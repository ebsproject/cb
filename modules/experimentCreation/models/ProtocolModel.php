<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for Experiment Creation Protocol
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

// interfaces
use app\interfaces\modules\experimentCreation\models\IProtocolModel;

// models
use app\models\Api;
use app\models\Config;
use app\models\Crop;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentProtocol;
use app\models\Item;
use app\models\Lists;
use app\models\OccurrenceData;
use app\models\PlatformListAccess;
use app\models\Protocol;
use app\models\Scale;
use app\models\ScaleValue;
use app\models\User;
use app\models\Variable;

use Yii;
class ProtocolModel implements IProtocolModel{

    protected $api;
    protected $formModel;
    protected $experimentModel;
    protected $scale;
    protected $scaleValue;
    protected $config;
    protected $experimentProtocol;
    protected $lists;
    protected $experimentData;
    protected $item;
    protected $experiment;
    protected $crop;
    protected $variable;
    protected $occurrenceData;
    protected $protocol;

    public function __construct(
        Api $api, 
        FormModel $formModel, 
        ExperimentModel $experimentModel,
        ScaleValue $scaleValue,
        Scale $scale,
        Config $config,
        ExperimentProtocol $experimentProtocol,
        Lists $lists,
        ExperimentData $experimentData,
        Item $item,
        Experiment $experiment,
        Crop $crop,
        Variable $variable,
        OccurrenceData $occurrenceData,
        PlatformListAccess $platformListAccess,
        Protocol $protocol)
    {
        $this->api = $api;
        $this->formModel = $formModel;
        $this->experimentModel = $experimentModel;
        $this->scale = $scale;
        $this->scaleValue = $scaleValue;
        $this->config = $config;
        $this->experimentProtocol = $experimentProtocol;
        $this->lists = $lists;
        $this->experimentData = $experimentData;
        $this->item = $item;
        $this->experiment = $experiment;
        $this->crop = $crop;
        $this->variable = $variable;
        $this->occurrenceData = $occurrenceData;
        $this->platformListAccess = $platformListAccess;
        $this->protocol = $protocol;
    }

    /**
     * Retrieve planting protocol data
     * 
     * @param Integer $id experiment identifier
     * @param String $program program code
     * @param Integer $processId process identifier
     * @param Array $model list of experiment info
     * @param Integer $dashProgram dashboard program id
     * @param String $isViewOnly flag for view-only mode
     * @param Integer $occurrenceDbId occurrence record id
     * @param String $previewOnly flag for view-only mode
     * 
     * @return Array $data planting protocol data
     */
    public function getPlantingProtocolData($id, $program, $processId, $model, $dashProgram, $isViewOnly = '', $occurrenceDbId = null, $previewOnly = '', $shouldUpdateOccurrenceProtocols = '', $hasMultipleSelectedOccurrences = ''){

        //Get the configuration
        $headActionId = 'specify-protocols';
        $activities = $this->formModel->prepare($headActionId,$program,$id,$processId);
        $index = array_search($activities['active'] ?? '', array_column($activities['children'] ?? [],'url'));
        $parentActivityChildren = $activities['children'][$index]['children'] ?? [];
        $plantingProcAbbrev = 'PLANTING_PROTOCOL'; 
        $childActivityAbbrev = '';
        $dataProcessAbbrev = null;
        
        $experimentResponse = $this->api->getApiResults(
            'POST',
            'experiments-search',
            json_encode([ 'experimentDbId' => "$id", ]),
            'limit=1&sort=experimentDbId:asc',
            true
        );
        
        // This retrieves the data process abbrev (in this case, the experiment type)...
        // ...of the current experiment
        if (
            $experimentResponse['success'] &&
            isset($experimentResponse['data']) &&
            !empty($experimentResponse['data'])
        ) {
            $dataProcessAbbrev = $experimentResponse['data'][0]['dataProcessAbbrev'];
        }

        // This allows rendering of config for Planting Protocols of Phase II Cross Parent Nurseries...
        // ...in View Occurrence Protocols tab
        if (
            $isViewOnly &&
            $dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'
        ) {
            array_unshift($parentActivityChildren, [
                'abbrev' => 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT',
            ]);
        }

        if(!empty($parentActivityChildren) && sizeof($parentActivityChildren) > 0){
            foreach($parentActivityChildren as $value){
                if(strpos($value['abbrev'], $plantingProcAbbrev) !== false){
                    $childActivityAbbrev = $value['abbrev'].'_VAL';
                }
            }
        }

        $configTypeOptions = [
            'activityAbbrev' => ($hasMultipleSelectedOccurrences) ? 'OBSERVATION_' . $plantingProcAbbrev . '_ACT_VAL' : $childActivityAbbrev
        ];

        $retrievedConfigData = $this->experimentModel->retrieveConfiguration(Yii::$app->controller->action->id, $program, $id, $processId,'child', $configTypeOptions);

        $defaultFieldArr = [];
        $variableArr = [];
        $variableNameArr = [];
        
        $plantInstruction = 'PLANTING_INSTRUCTIONS';
        $varMod = $this->variable->getVariableByAbbrev('YEAR');
        $plantInstructionId = $varMod['variableDbId'] ?? '';

        $mainApiResourceEndpoint = '';
        $mainApiResourceFilter = [];

        // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
        if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
            $mainApiResourceEndpoint = 'occurrence-data-search';
            $mainApiResourceFilter = ["occurrenceDbId" => $occurrenceDbId];
        } else {
            $mainApiResourceEndpoint = 'experiments/'.$id.'/data-search';
            $mainApiResourceFilter = ["experimentDbId"=>"$id"];
        }

        $defaultWidgetOptions = [
            'mainModel' => 'Experiment',
            'config' => $retrievedConfigData,
            'mainApiResourceMethod' => 'POST',
            'mainApiResourceEndpoint' => $mainApiResourceEndpoint,
            'mainApiSource' => 'metadata',
            'mainApiResourceFilter' => $mainApiResourceFilter,
            'program' => $dashProgram,
            'targetStep' => 'protocol',
            'id' => $id,
            'isViewOnly' => $isViewOnly,
            'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
            'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
            'previewOnly' => $previewOnly,
        ];

        return [
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'model' => $model,
            'configValues' => $retrievedConfigData,
            'defaultFieldArr' => $defaultFieldArr,
            'protocolChildren' => $parentActivityChildren,
            'variableArr' => $variableArr,
            'plantInstruction' => $plantInstruction,
            'plantInstructionId' => $plantInstructionId,
            'plantingActivity' => $childActivityAbbrev,
            'variableNameArr' => $variableNameArr,
            'defaultWidgetOptions' => $defaultWidgetOptions
        ];
            

    }

    /**
     * Retrieve plot types
     * 
     * @param Integer $id experiment identifier
     * @param String $program program identifier
     * @param Array $postData list of posted data
     * @param String $isViewOnly flag for view-only mode
     * @param String $previewOnly flag for view-only mode
     * 
     * @return Array $data plot type data
     */
    public function getPlotType($id, $program, $postData, $isViewOnly = '', $previewOnly = ''){
        $data = [];
        $selected = $postData['selected'];
        $variable = strtoupper($postData['memtype']);
        $varRecord = $this->variable->getVariableByAbbrev($variable);
        // flags for use in Experiment Manager
        $isViewOnly = $postData['isViewOnly'];
        $shouldUpdateOccurrenceProtocols = $postData['shouldUpdateOccurrenceProtocols'];
        $hasMultipleSelectedOccurrences = $postData['hasMultipleSelectedOccurrences'];
        
        $scaleValueParam = ["value"=>"$selected"];
        
        $selectedActivityAbbrev = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
        
        $requiredConfig = $this->config->getConfigByAbbrev($selectedActivityAbbrev[0]['abbrev']);  
        
        $requiredFields = isset($requiredConfig['Values']) ? $requiredConfig['Values'] : '';
        
        //Check if the selected type exists in the database
        $varModel = $this->variable->getVariableByAbbrev(strtoupper($variable));
        
        //check in the experiment data
        $varId = isset($varModel['variableDbId']) ? $varModel['variableDbId'] : 0;
        
        // Check if the new input exists in the database
        $inputDataCheckParam = ["variableDbId"=>"$varId", "dataValue"=>"$selected"];
        
        // This flag determines whether to display Occurrence-level data or Experiment-level data
        if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
            $exptData = $this->occurrenceData->getOccurrenceDataRecord($id,$inputDataCheckParam);
        } else {
            $exptData = $this->experimentData->getExperimentDataRecord($id,$inputDataCheckParam);
        }

        $checkExistingDataParam = ["variableDbId"=>"$varId"];

        if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
            $existingtData = $this->occurrenceData->getOccurrenceDataRecord($id,$checkExistingDataParam);
        } else {
            $existingtData = $this->experimentData->getExperimentDataRecord($id,$checkExistingDataParam);
        }

        $existingDataVal = '';
        if(!empty($existingtData)){
            $existingDataVal = $existingtData['dataValue'];
        }

        if(!empty($existingDataVal) && ($selected == $existingDataVal)){ // Input already exists
            //Do nothing
        }else{
            // If new, then replace the existing saved value
            $scaleValueParam = ["value"=>"$selected"];
            $scaleValueMod = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
            $scaleValueAbbrev = $scaleValueMod[0]['abbrev'];  
                
            $getPlotTypeConfig = $this->config->getConfigByAbbrev($scaleValueAbbrev); 
            
            $plotTypeConfigValues = $getPlotTypeConfig['Values'] ?? [];
            $plotTypeVaribleCond = '';

            // Add plot types detail
            $plotTypeVaribleCond = "equals ".$varRecord['variableDbId'];
            
            foreach($plotTypeConfigValues as $plotTypeValTemp){
                $varModelTemp = $this->variable->getVariableByAbbrev(strtoupper($plotTypeValTemp['variable_abbrev']));
                $varTempId = $varModelTemp['variableDbId'];
                                    
                if(empty($plotTypeVaribleCond)){
                    $plotTypeVaribleCond = "equals ".$varTempId;
                }else{
                    $plotTypeVaribleCond = $plotTypeVaribleCond . "|". "equals ".$varTempId;
                }
            }

            $plotMetDataArr = ["variableDbId"=>"$plotTypeVaribleCond"];


            // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
            if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
                //Search all the occurrence data id of the selected config variables
                $savedExptData = $this->occurrenceData->searchAllRecords($id,$plotMetDataArr);
            } else {
                //Search all the experiment data id of the selected config variables
                $savedExptData = $this->experimentData->searchAllRecords($id,$plotMetDataArr);
            }
    
            
            $exptDataIds = [];

            if(count($savedExptData) > 0){
                foreach($savedExptData as $eachDataVal){

                    if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
                        $exptDataIds[] = $eachDataVal['occurrenceDataDbId'];
                    } else {
                        $exptDataIds[] = $eachDataVal['experimentDataDbId'];
                    }
            
                }
                //Delete the exptDataIds
                $this->experimentData->deleteMany($exptDataIds);
            }
        }

        $exptDataValue = $exptData['dataValue'] ?? '';
        $scaleValueParam = ["value"=>"$exptDataValue"];
        if(!empty($exptDataValue)){
            $scaleValModel = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
        }
    
        if(!empty($requiredFields)){
            $variableYear = $this->variable->getVariableByAbbrev('YEAR');
            $yearScale = $this->scale->getVariableScale($variableYear['variableDbId']);  
            $yearMinValue = $yearScale['minValue'] ?? '';
            $yearMaxValue = $yearScale['maxValue'] ?? '';

            $mainApiResourceEndpoint = '';
            $mainApiResourceFilter = '';

            // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
            if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
                $mainApiResourceEndpoint = 'occurrence-data-search';
                $mainApiResourceFilter = ["occurrenceDbId" => $id];
            } else {
                $mainApiResourceEndpoint = 'experiments/'.$id.'/data-search';
                $mainApiResourceFilter = ["experimentDbId"=>"$id"];
            }

            $widgetOptions = [
                'mainModel' => 'Experiment',
                'config' => $requiredConfig['Values'],
                'mainApiResourceMethod' => 'POST',
                'mainApiResourceEndpoint' => $mainApiResourceEndpoint,
                'mainApiSource' => 'metadata',
                'mainApiResourceFilter' => $mainApiResourceFilter,
                'program' => $program,
                'id' => $id,
                'targetStep' => 'protocol',
                'isViewOnly' => $isViewOnly,
                'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
                'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
                'previewOnly' => isset($postData['previewOnly']) && $postData['previewOnly'] == '1' ?
                    $postData['previewOnly'] : '',
                'configTag' => 'plotTypeFields',
            ];
            
            $data = [
                'id' => $id,
                'program' => $program,
                'year_min_value' => $yearMinValue,
                'year_max_value' => $yearMaxValue,
                'widgetOptions' => $widgetOptions
            ];
        } else {
            $data = [
                'id' => $id,
                'program' => $program,
                'year_min_value' => null,
                'year_max_value' => null,
                'widgetOptions' => []
            ];
        }

        return $data;
    }

    /**
     * Retrieve protocol data stored in the experiment data
     * 
     * @param Integer $id experiment identifier
     * @param String $program program code
     * @param Integer $processId process identifier
     * @param Array $model list of experiment info
     * @param String Protocol abbrev to save
     * @param String $isViewOnly flag for view-only mode
     * @param Integer $occurrenceDbId occurrence record id
     * @param String $previewOnly flag for view-only mode
     * @return Array $data planting protocol data
     */
    public function getExperimentProtocolData($id, $program, $processId, $model, $protocolAbbrev, $isViewOnly = '', $occurrenceDbId = null, $previewOnly = '', $shouldUpdateOccurrenceProtocols = '', $hasMultipleSelectedOccurrences = ''){

        //Get the configuration
        $headActionId = 'specify-protocols';
        $activities = $this->formModel->prepare($headActionId,$program,$id,$processId);
        $index = array_search($activities['active'] ?? '', array_column($activities['children'] ?? [],'url'));

        $parentActivityChildren = $activities['children'][$index]['children'] ?? [];

        $childActivityAbbrev = '';

        if(!empty($parentActivityChildren) && sizeof($parentActivityChildren) > 0){
            foreach($parentActivityChildren as $value){
                if(strpos($value['abbrev'], $protocolAbbrev) !== false){
                    $childActivityAbbrev = $value['abbrev'].'_VAL';
                }
            }
        }

        $configTypeOptions = [
            'activityAbbrev' => ($hasMultipleSelectedOccurrences) ? 'OBSERVATION_' . $protocolAbbrev . '_ACT_VAL' : $childActivityAbbrev
        ];

        $retrievedConfigData = $this->experimentModel->retrieveConfiguration(Yii::$app->controller->action->id, $program, $id, $processId,'child', $configTypeOptions);

        $defaultFieldArr = [];
        $variableArr = [];
        $variableNameArr = [];

        $mainApiResourceEndpoint = '';
        $mainApiResourceFilter = [];

        // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
        if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
            $mainApiResourceEndpoint = 'occurrence-data-search';
            $mainApiResourceFilter = ["occurrenceDbId" => $occurrenceDbId];
        } else {
            $mainApiResourceEndpoint = 'experiments/'.$id.'/data-search';
            $mainApiResourceFilter = ["experimentDbId"=>"$id"];
        }

        $defaultWidgetOptions = [
            'mainModel' => 'Experiment',
            'config' => $retrievedConfigData,
            'mainApiResourceMethod' => 'POST',
            'mainApiResourceEndpoint' => $mainApiResourceEndpoint,
            'mainApiSource' => 'metadata',
            'mainApiResourceFilter' => $mainApiResourceFilter,
            'program' => $program,
            'targetStep' => 'protocol',
            'id' => $id,
            'isViewOnly' => $isViewOnly,
            'shouldUpdateOccurrenceProtocols' => $shouldUpdateOccurrenceProtocols,
            'hasMultipleSelectedOccurrences' => $hasMultipleSelectedOccurrences,
            'previewOnly' => $previewOnly,
        ];

        return [
            'id' => $id,
            'program' => $program,
            'processId' => $processId,
            'model' => $model,
            'configValues' => $retrievedConfigData,
            'defaultFieldArr' => $defaultFieldArr,
            'protocolChildren' => $parentActivityChildren,
            'variableArr' => $variableArr,
            'activity' => $childActivityAbbrev,
            'variableNameArr' => $variableNameArr,
            'defaultWidgetOptions' => $defaultWidgetOptions
        ];


   }


    /**
     * Auto compute of plot fields
     * 
     * @param Array $postData list of posted data
     *
     * @return Integer computed value
     */
    public function computePlotFieldValue($postData){

        $sample = [];
        parse_str($postData['formData'], $sample);

        $paramsArr = [];
        
        $keys = array_keys($sample['Experiment']);
        
        $varAbbrev = 'equals '.implode('|equals ', $keys);
        $variableData = $this->variable->searchAll(['abbrev'=>$varAbbrev])['data'];

        $variableIdsArray = array_column($variableData, 'abbrev');
        foreach($sample['Experiment'] as $key => $value){

            $fields = strtolower($key);
            $index = array_search(strtoupper($key), $variableIdsArray);
            $variableModel = $variableData[$index];
            $var_id = $variableModel['variableDbId'];
            
            if(isset($value['metadata']['no_new_val'][$var_id])){
                $paramsArr[$fields] = $value['metadata']['no_new_val'][$var_id];
            }elseif(isset($value['metadata'][$var_id])){
                $paramsArr[$fields] = $value['metadata'][$var_id];
            }elseif(isset($value['metadata']["'disabled'"][$var_id])){
                $paramsArr[$fields] = $value['metadata']["'disabled'"][$var_id];
            }else{
                $paramsArr[$fields] = $value;
            }
        }

        if($paramsArr != null){
                extract($paramsArr);
        }

        $computed = 0;
        $computedArr = [];
        $computedVariable = $postData['formulaVariable'];

        if( ($computedVariable == 'PLOT_WIDTH_RICE' || $computedVariable == 'PLOT_WIDTH') && (!empty($dist_bet_rows) && !empty($rows_per_plot_cont))){
            $computed = ($dist_bet_rows/100) * $rows_per_plot_cont;
            $computedArr[$computedVariable] = $computed;
        }else if($computedVariable == 'PLOT_WIDTH_WHEAT_FLAT' && (!empty($rows_per_plot_cont) && !empty($dist_bet_rows) && !empty($distance_bet_plots_in_m))){
            $computed = ($rows_per_plot_cont - 1) * $dist_bet_rows + $distance_bet_plots_in_m;
        }else if($computedVariable == 'PLOT_WIDTH_MAIZE_BED' && (!empty($no_of_beds_per_plot) && !empty($bed_width))){
            $computed = $no_of_beds_per_plot * $bed_width;   
        }else if($computedVariable == 'PLOT_LN_1' && (!empty($hills_per_row_cont) && !empty($dist_bet_hills))){
            $computed = ($hills_per_row_cont * $dist_bet_hills)/100;
            $plot_ln_1 = $computed;
        }else if($computedVariable == 'PLOT_AREA_1' && (!empty($plot_ln_1) && !empty($plot_width_rice))){
            $computed = $plot_ln_1 * $plot_width_rice;
        }else if($computedVariable == 'PLOT_AREA_2' && (!empty($plot_ln) && !empty($plot_width))){
            $computed = $plot_ln * $plot_width;
        }else if($computedVariable == 'PLOT_AREA_2' && (!empty($plot_ln_1) && !empty($plot_width))){
            $computed = $plot_ln_1 * $plot_width;
        }else if($computedVariable == 'PLOT_AREA_3' && (!empty($plot_ln) && !empty($plot_width_wheat_flat))){
            $computed = $plot_ln * $plot_width_wheat_flat;
        }else if($computedVariable == 'PLOT_AREA_4' && (!empty($plot_ln) && !empty($plot_width_maize_bed))){
            $computed = $plot_ln * $plot_width_maize_bed;
        }else if($computedVariable == 'PLOT_AREA_4' && (!empty($plot_ln) && !empty($plot_width))){
            $computed = $plot_ln * $plot_width;
        }
        
        return round($computed,3);
    }

    /**
     * Retrieve plot variable values
     * 
     * @param Integer $id experiment identifier
     * @param Array $defaultVariables list of default variables
     * @param String $previewOnly flag for view-only mode
     * @param String $isViewOnly flag for view-only mode
     */
    public function getPlotVariableValue($id, $defaultVariables, $isViewOnly = '', $previewOnly = '', $shouldUpdateOccurrenceProtocols = ''){
        $result = [];
            
        foreach($defaultVariables as $variable){
            $varModel = $this->variable->getVariableByAbbrev(strtoupper($variable['variable_abbrev']));
            $varId = $varModel['variableDbId'];

            $dataParam = ["variableDbId"=>$varId.""];
            
            // This flag determines whether to retrieve Occurrence-level data or Experiment-level data
            if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
                $experimentData = $this->occurrenceData->getOccurrenceDataRecord($id,$dataParam);
            } else {
                $experimentData = $this->experimentData->getExperimentDataRecord($id,$dataParam);
            }

            if(isset($experimentData['dataValue']) && !empty($experimentData['dataValue'])){
                $exptDataVal = $experimentData['dataValue'];
                $scaleParam = ["value"=>"$exptDataVal"];
                
                $scaleValModel = $this->scaleValue->getVariableScaleValues($experimentData['variableDbId'], $scaleParam);
            
                if(!empty($scaleValModel)){
                    $result[strtolower($varModel['abbrev'])] = ['key'=>$scaleValModel[0]['value'],'value'=>$scaleValModel[0]['value']];
                }else{
                    $result[strtolower($varModel['abbrev'])] = $exptDataVal;
                }
                
            }      
        }
        
        return $result;
    }

    /**
     * Validate if required protocol values were properly filled
     * 
     * @param Integer $id experiment identifier
     * @param String $program program code
     * @param Integer $processId process identifier
     * @param Array $protocolChildren list of protocol sub tabs
     */
    public function validateProtocol($id, $program, $processId, $protocolChildren){
        
        $protocolReqFields = [];
        $noRecordProtocolVal = [];
        $listOfNoRecordVal = [];

        // Checking for planting protocol: planting required variables
        // for trait protocol: may list of selected traits, $listId = $this->experimentProtocol->getListId($experimentId);
        // for  process path: process_path_abbrevs
         
        foreach($protocolChildren as $value){
            $procVar = $value['abbrev'];
            $procVarOptions = [
                'activityAbbrev' => $procVar.'_VAL'
            ];
            
            $retrievedConfigData = $this->experimentModel->retrieveConfiguration('specify-protocols', $program, $id, $processId,'child', $procVarOptions);
            $specificProtocolReqItems = [];

            if(isset($retrievedConfigData) && !empty($retrievedConfigData)){
                $varConfigValues = $retrievedConfigData;
                
                //Drill down the required fields of the protocol
                foreach($varConfigValues as $reqValItem){
                
                    if(isset($reqValItem['required']) && strtolower($reqValItem['required']) == 'required'){
                        $reqFieldItem = strtoupper($reqValItem['variable_abbrev']);
                        
                        //check if field label has value, if none then get the label in the variable table
                        $varMod = $this->variable->getVariableByAbbrev($reqFieldItem);

                        $label = ucwords(strtolower($varMod['name']));
                      
                        $specificProtocolReqItems[] = ['abbrev'=>$reqFieldItem,'label'=>$label];
                    }
                }

            }else{ //No specified required variables in the config but still has required list (minimum)
                //This is for traits specific, it should atleast have a selected trait lists
                
                if(strpos(strtolower($value['name']),'traits') !== false){
                    
                    $listId = $this->experimentProtocol->getListId($id,'TRAITS');
                    $listMemberInfo = $this->lists->searchAllMembers($listId);
                    $listMembers = isset($listMemberInfo['data'][0]['listMemberDbId']) ? $listMemberInfo['data'] : [];

                    $countOfTraits = count($listMembers); 
                    $specificProtocolReqItems[] = $countOfTraits;
                    
                }else if(strpos(strtolower($value['name']),'management') !== false){
                    
                    $listId = $this->experimentProtocol->getListId($id,'MANAGEMENT');
                    $listMemberInfo = $this->lists->searchAllMembers($listId);
                    $listMembers = isset($listMemberInfo['data'][0]['listMemberDbId']) ? $listMemberInfo['data'] : [];

                    $countOfMgt = count($listMembers); 
                    $specificProtocolReqItems[] = $countOfMgt;
                    
                }else if(strpos($value['name'],'process_path') !== false){

                }
            }
           
            //List of protocols and its associated required variables
            
            $protocolReqFields[$value['abbrev']] = $specificProtocolReqItems; 
        }

        //Check if every required fields in a protocol has a value or is saved in the DB
        foreach($protocolReqFields as $key=>$value){
            $configAbbrev = $key.'_VAL';
            //Check if Value is an array
            $noRecordProtocolVal = [];
            
            if(is_array($value) && !empty($value)){
        
                //This part is planting protocol specific (plot type)
                foreach($value as $valItem){
                    
                    $itemAbbrev = isset($valItem['abbrev']) ? $valItem['abbrev'] : '';
                 
                    //Check first the plot type
                    if(!empty($itemAbbrev)){
                        
                        //Check it in the Experiment data and get the saved value
                        $varModel = $this->variable->getVariableByAbbrev($itemAbbrev);
                        $varAbbrev = $varModel["abbrev"];
                        $plantingFilter = ["abbrev" => "$varAbbrev"];
                        $exptDataModel = $this->experimentData->getExperimentData($id,$plantingFilter);
                        
                        if(isset($exptDataModel[0]['dataValue']) && !empty($exptDataModel[0]['dataValue'])){
                            
                            //Check the associated plot type required values     
                            $configTypeOptions = [
                                'activityAbbrev' => $configAbbrev
                            ];
                            
                            // $retrievedConfigData_1 = $this->experimentModel->retrieveConfiguration('specify-protocols', $program, $id, $processId,'child', $configTypeOptions);
                            
                            $plotTypeVal = $exptDataModel[0]['dataValue'];
                            $scaleValues = $this->scaleValue->getVariableScaleValues($varModel['variableDbId'],["value"=>"$plotTypeVal"]);
                            
                            $getConfigVals = $this->config->getConfigByAbbrev($scaleValues[0]['abbrev']); 
                            
                            if(isset($getConfigVals['Values'])){
                                $varConfigValues_1 = $getConfigVals['Values'];
                            
                                //Drill down the required fields of the protocol
                                foreach($varConfigValues_1 as $reqValItem){
                                   
                                    if(isset($reqValItem['required']) && strtolower($reqValItem['required']) == 'required'){
                                        $specificProtocolReqItems_1[] = $reqValItem['variable_abbrev'];
                                        //Check experiment data for saved values
                                        $varModel_1 = $this->variable->getVariableByAbbrev(strtoupper($reqValItem['variable_abbrev'])); 
                                        $abbrevTemp = $varModel_1['abbrev'];
                                        $addtlFilter = ["abbrev"=>"$abbrevTemp"];
                                        
                                        $exptDataModel1 = $this->experimentData->getExperimentData($id,$addtlFilter);

                                        if(isset($exptDataModel1[0]['dataValue']) && !empty($exptDataModel1[0]['dataValue'])){
                                            //This is ok 
                                        }else{
                                            //No record found in the experiment data table
                                            //check if field label has value, if none then get the label in the variable table
                                            $varMod = $this->variable->getVariableByAbbrev(strtoupper($reqValItem['variable_abbrev']));
                                            $label = $varMod['name'];
                                            
                                            $noRecordProtocolVal[] = ['abbrev'=>$reqValItem['variable_abbrev'],'label'=>$label];
                                        }
                                    }
                                }
                                
                            }
                        }else{
                            //then the required plot type field has not been saved
                            $noRecordProtocolVal[] = ['abbrev'=>$itemAbbrev,'label'=>ucwords(strtolower($varModel['name']))];
                        }
                    }
                    //PLS do not remove yet, this is for the management and trait protocol validation 
                    // else{
                    //     //This is for traits protocol, the array contains the count of members assigned in a list
                    //     if($valItem > 0){
                    //         //List have members
                    //     }else{
                    //         //List have no members
                    //         if(strpos(strtolower($key),'management') !== false){
                    //             $noRecordProtocolVal[] = 'No management protocol list specified';
                    //         }elseif(strpos(strtolower($key),'traits') !== false){
                    //             $noRecordProtocolVal[] = 'No traits protocol list specified';
                    //         }
                    //     }
                    // }
                }
            }   
            if(!empty($noRecordProtocolVal)){
                $listOfNoRecordVal[] = $noRecordProtocolVal;
            }
        }
    
        $arr = [];
        
        foreach($listOfNoRecordVal as $key => $value){
            if(!empty($value)){
                if(!empty($value)){
                    $configMod = $this->item->getItem($key);
                    $displayName = $configMod['displayName'];
                    $arr[$key] = ['display_name' => $displayName, 'value'=>$value];
                }
            }
        }

        $experimentModel = $this->experiment->getExperiment($id);
        $status = $experimentModel['experimentStatus'];
        $status = explode(';',$status);
        
        if(!empty($listOfNoRecordVal)){
        
            //To update the status of the experiment
            if(in_array("protocol specified", $status)){    
                $index = array_search('protocol specified', $status);
                
                if(isset($status[$index])){
                    unset($status[$index]);

                    $newStatus = implode(';', $status);
                    $this->experiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
                }
            }
        }else{
            //Update the experiment status
            if(!in_array('protocol specified', $status)){
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'."protocol specified" : "protocol specified";
           
                $this->experiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
            }
        }
        
        $flag = false;
        if(!empty($arr)){
            $flag = true;
        }

        return [
            'records' => $arr,
            'flag' => $flag
        ];


    }

    /**
     * Retrieve plot field filter tags
     * 
     * @param Integer $id experiment identifier
     * @param Array $postData list of posted data
     */
    public function getPlotFieldFilterTag($id, $postData){

        $establishment = strtoupper($postData['establishment']);
        $plantingType = strtoupper($postData['plantingType']);
        $variable = strtoupper($postData['memType']);
        $tags = [];

        $activities = '';
        $activityAbbrev = '';
       
        $crop = '';
        $activCheck = $variable;
        $varRecord = $this->variable->getVariableByAbbrev(strtoupper($variable));
        
        /*
        
        23.06 Temporarily removed this since this is not being used in the code anyway

        $experimentModel = $this->experiment->getExperiment($id);

        if($variable == 'ESTABLISHMENT'){
            $activCheck = 'PLANTING_TYPE';
            $activities = 'planting_type_abbrevs';
       
            if(strpos($establishment,'SINGLE SEED') !== false){
                $scaleValueParam = ["value"=>"direct seeded"];
       
                $newEstabAbbrev = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
       
                $establishment = (isset($newEstabAbbrev[0]['abbrev']) && !empty($newEstabAbbrev[0]['abbrev'])) ? $newEstabAbbrev[0]['abbrev'] : $establishment;
            }
        }else if($variable == 'PLANTING_TYPE'){
            $activCheck = 'PLOT_TYPE';
            $activities = 'plot_type_abbrevs';
            //Get the establishment abbrev
            if(!empty($establishment)){
                if(strpos($establishment,'SINGLE SEED') !== false){
                    $scaleValueParam = ["value"=>"direct seeded"];
            
                    $newEstabAbbrev = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
                    $establishment = (isset($newEstabAbbrev[0]['abbrev']) && !empty($newEstabAbbrev[0]['abbrev'])) ? $newEstabAbbrev[0]['abbrev'] : $establishment;
                }

                $establishmentAbbrev = $establishment;
              
            }else if(empty($establishment)){
               
                $establishmentAbbrev = '';
            }
        }else if($variable == 'PLOT_TYPE'){
            $activCheck = $variable;
            $activities = 'plot_type_abbrevs';
        }
        
        if(!empty($experimentModel['cropDbId'])){
            $cropModel = $this->crop->getCrop($experimentModel['cropDbId']); 
            
            $crop = (isset($cropModel[0]['cropCode'])) ? $cropModel[0]['cropCode'] : '';
            
            if(strtoupper($crop) == 'RICE' && $variable == 'PLANTING_TYPE'){
                if(strpos($plantingType,'BED') !== false){
                    $prefix = !empty($establishmentAbbrev) ? $establishmentAbbrev.'_'.$plantingType : $crop;
                }else{
                    $prefix = !empty($establishmentAbbrev) ? $establishmentAbbrev : $crop;
                }
                $activityAbbrev = $prefix.'_'.'PLOT_TYPE';
            }else if(strtoupper($crop) != 'RICE' && $variable == 'PLANTING_TYPE'){ //For Wheat and Maize
                if(!empty($plantingType)){
                    $scaleParam = ["abbrev"=>"$plantingType"];
                    $scaleValues = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleParam);
                  
                    if(isset($scaleVal[0]['dataValue'])){
                        $scaleVal = $scaleValues[0];
                        $activityAbbrev = $crop.'_'.strtoupper($scaleVal['dataValue']).'_'.'PLOT_TYPE';
                    }
                }else{
                    $activityAbbrev = $crop.'_'.$activCheck;
                }
            }else if($variable == 'PLOT_TYPE' && strtoupper($crop) == 'RICE'){
                if(strpos($establishment,'SINGLE SEED') !== false){
                    $scaleValueParam = ["value"=>"direct seeded"];
                  
                    $newEstabAbbrev = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
                    $establishment = (isset($newEstabAbbrev[0]['abbrev']) && !empty($newEstabAbbrev[0]['abbrev'])) ? $newEstabAbbrev[0]['abbrev'] : $establishment;
                }
                if(!empty($establishment) && empty($plantingType)){
                    $activityAbbrev = $establishment.'_'.$variable;
                }elseif(empty($establishment) && !empty($plantingType)){
                    $plantingType = ($plantingType != 'PLANTING_TYPE_FLAT') ? 'PLANTING_TYPE_FLAT' : $plantingType;
                    $activityAbbrev = $plantingType.'_'.$variable;
                }elseif(empty($establishment) && empty($plantingType)){
                    $cropDbId = $experimentModel['cropDbId'];
                  
                    $cropModel = $this->crop->getCrop($cropDbId);

                    if(isset($cropModel[0]['cropCode'])){ 
                        $crop = $cropModel[0]['cropCode'];
                      
                        $activityAbbrev = $crop.'_'.$variable;
                    }
                }elseif(!empty($establishment) && !empty($plantingType)){
                    //both establishment and planting type is not empty
                    if(strpos($plantingType,'BED') !== false){
                        $prefix = !empty($establishment) ? $establishment.'_'.$plantingType : $crop;
                        $activityAbbrev = $prefix.'_'.'PLOT_TYPE';
                    }else{
                        $activityAbbrev = $establishment.'_'.$variable;
                    }
                }
            }else{
                $activityAbbrev = $crop.'_'.$activCheck;
            }      

            if(strpos(strtolower($establishment),'not_applicable') !== false){
                  $activityAbbrev = $crop.'_'.$activCheck;
            }

            $requiredConfig = $this->config->getConfigByAbbrev($activityAbbrev); 
            $requiredFields = $requiredConfig['Values'] ?? null;
           
            $activities_config = $requiredFields[0][$activities] ?? null;
            
            if(!empty($activities_config) && isset($activities_config)){
                foreach($activities_config as $val){
                    
                    $scaleValueParam = ["abbrev"=>"$val"];
                    
                    $scaleValueMod = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);
                    
                    if(isset($scaleValueMod[0]['value']) && !empty($scaleValueMod[0]['value'])){
                        $tags[$val] = $scaleValueMod[0]['value'];
                    }
                }
            }
        }*/

        $scaleValueParam = $configAbbrev = $plotTypeAbbrevs = null;
        // display list of plot types based on selected crop establishment from config
        if($variable == 'PLOT_TYPE'){
            if(strpos($establishment,'DIRECT SEEDED') !== false){
                $configAbbrev = 'ESTABLISHMENT_DIRECT_SEEDED_PLOT_TYPE';
            }else if(strpos($establishment,'TRANSPLANTED') !== false){
                $configAbbrev = 'ESTABLISHMENT_TRANSPLANTED_PLOT_TYPE';
            }

            // get plot types from the config
            if($configAbbrev != null){
                $plotTypeValuesConfig = $this->config->getConfigByAbbrev($configAbbrev); 
                $plotTypeAbbrevs = isset( $plotTypeValuesConfig['Values'][0]['plot_type_abbrevs'] ) ? $plotTypeValuesConfig['Values'][0]['plot_type_abbrevs'] : [];
            }

            if(!empty($plotTypeAbbrevs)){    
                $scaleValueParam['abbrev'] = 'equals '.implode('|equals ', $plotTypeAbbrevs);
            }
            
            $plotTypesArr = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], $scaleValueParam);

            foreach($plotTypesArr as $plotType){
                if(isset($plotType['value'])){
                    $value = $plotType['value'];
                    $tags[$value] = $value;
                }
            }
        }

        return $tags;
    }


    /**
     * Save Management Target Level -- Occurrence or Location
     * 
     * @param $id Integer Experiment ID
     * @param $targetLevel String Management target level
     */
    public function saveMgtTargetLevel($id,$targetLevel){
        //Check if Target level for management protocol exists in the table experiment.experiment_data
        $mgtVariable = Variable::searchAll(['abbrev' => 'equals PROTOCOL_TARGET_LEVEL'])['data'][0]['variableDbId'];
        $mgtDataParam = ['variableDbId'=>"equals $mgtVariable"];
        $experimentDataRes = ExperimentData::searchAllRecords($id,$mgtDataParam);
        
        if(empty($experimentDataRes)){
            //By default, the management type is occurrence
            $dataRequestBodyParam[] = [
                'variableDbId' => "$mgtVariable",
                'dataValue' => "$targetLevel"
            ];
            
            $dataResult = Experiment::createExperimentData($id,$dataRequestBodyParam);
        }else{
            //Update the target level
            //Get the experiment data id
            $experimentDataDbId = $experimentDataRes[0]['experimentDataDbId'];
            $updateDataParam = [ 'variableDbId' => "$mgtVariable",'dataValue' => "$targetLevel"];
            $dataResult = ExperimentData::updateExperimentData($experimentDataDbId,$updateDataParam);
        }
        return $dataResult;
    }
    
    // /**
    //  * TODO: To be refactored
    //  * Retrieve process path tags
    //  *
    //  * @param $id integer experiment identifier
    //  * @param $program integer program identifier
    //  * @param $processId integer processId identifier
    //  * 
    //  * @return $listTags array list of saved list tags
    //  */
    // public function getProcessPathTags($program,$id,$processId){

    // 	//Get the configuration
    // 	// $headActionId = 'specify-protocols';
    // 	// $activities = $this->formModel->prepare($headActionId,$program,$id,$processId);
    // 	// $index = array_search($activities['active'], array_column($activities['children'],'url'));

    // 	// $parentActivityAbbrev = $activities['children'][$index]['abbrev'];
    // 	// $parentActivityValue = $activities['children'][$index]['abbrev'].'_VAL';
    // 	// $parentActivityChildren = $activities['children'][$index]['children'];
        
    // 	//  $processPathProcAbbrev = 'PROCESS_PATH'; 
    // 	//  $childActivityAbbrev = '';
        
    // 	//  if(!empty($parentActivityChildren) && sizeof($parentActivityChildren) > 0){
    // 	//     foreach($parentActivityChildren as $value){
    // 	//         if(strpos($value['abbrev'], $processPathProcAbbrev) !== false){
    // 	//             $childActivityAbbrev = $value['abbrev'].'_VAL';
    // 	//         }
    // 	//     }
    // 	//  }

    // 	// $requiredConfig = $this->config->find()->where(['abbrev'=>$childActivityAbbrev, 'is_void'=>false])->one();
                
    // 	// $requiredFields = $requiredConfig['config_value'];
        
    // 	// $activitiesConfig = $requiredFields['Values'][0]['process_path_abbrevs'];
        
    // 	// $processPathAbbrevs = "'".implode("','", $activitiesConfig)."'";

    // 	// $sql = "
    // 	// 	select id, display_name
    // 	// 	from master.item 
    // 	// 	where abbrev in ($processPathAbbrevs)
    // 	// 	order by display_name
    // 	// ";

    // 	// $tags = Yii::$app->db->createCommand($sql)->queryAll();

    // 	// $listTags = []; // variable set tags
        
    // 	// foreach ($tags as $key => $value) {
    // 	//     $listTags[$value['id']] = $value['display_name'];
    // 	// }
        
    // 	// return $listTags;
    // }

    // /**
    //  * TODO: To be refactored
    //  * Save process path id in experiment data
    //  * 
    //  * @param $id integer experiment identifier
    //  * @param $processPathId integer process path identifier
    //  */
    // public function saveProcessPath($id, $processPathId){
        
    // 	// $userId = User::getUserId();
    // 	// $processPathData = null;
    // 	// $processPathAbbrev = 'PROCESS_PATH';
    // 	// $processPathVarId = $this->variable->find()->where(['abbrev'=> $processPathAbbrev, 'is_void'=> false])->one()['id'];
    // 	// $processPathData = $this->experimentData->find()->where(['experiment_id'=>$id, 'variable_id'=>$processPathVarId, 'is_void'=>false])->one();
        
    // 	// //save in experiment data
    // 	// if($processPathData == NULL){
    // 	// 	$sql = "insert into operational.experiment_data (experiment_id, variable_id, value, creator_id)
    // 	// 	values (
    // 	// 		$id,
    // 	// 		$processPathVarId,
    // 	// 		$processPathId,
    // 	// 		$userId
    // 	// 	);";
    // 	// 	$message = "Successfuly saved.";
    // 	// } else {
    // 	// 	$sql = "
    // 	// 		update operational.experiment_data 
    // 	// 		set value = $processPathId 
    // 	// 		where experiment_id = $id 
    // 	// 			and variable_id = $processPathVarId
    // 	// 			and is_void = false";
    // 	// 	$message = "Successfuly updated.";
    // 	// }

    // 	// Yii::$app->db->createCommand($sql)->execute();
        
    // 	// //save in operational.study
    // 	// $tempStudyCheckSql = "select operational_study_id from operational.temp_study where experiment_id = $id and is_void = false";
    // 	// $studyId = Yii::$app->db->createCommand($tempStudyCheckSql)->queryScalar();

    // 	// if(!empty($studyId)){
    // 	// 	$updateStudyProcessPathSql = "update operational.study set process_path_id = $processPathId where experiment_id = $id and is_void = false";
    // 	// 	Yii::$app->db->createCommand($updateStudyProcessPathSql)->execute();
    // 	// }

    // 	// return $message;
    // }

    // /**
    //  * TODO: To be refactored
    //  * Retrieve process path experiment data
    //  * 
    //  * @param $id integer experiment identifier
    //  */
    // public function getProcessPathData($id){

    // 	// $processPathData = null;
    // 	// $processPathAbbrev = 'PROCESS_PATH';
    // 	// $processPathVarId = $this->variable->find()->where(['abbrev'=> $processPathAbbrev, 'is_void'=> false])->one()['id'];
    // 	// $processPathData = $this->experimentData->find()->where(['experiment_id'=>$id, 'variable_id'=>$processPathVarId, 'is_void'=>false])->one();

    // 	// return $processPathData;
    // }

    // /**
    //  * TODO: To be refactored
    //  * Returns the next order number to be used in inserting list member records 
    //  * 
    //  * @param $listId integer list identifier
    //  */
    // public function getOrderNumber($listId){
    // 	// //get max order number
    // 	// $sql = "select max(order_number) from platform.list_member where list_id = $listId and is_void = false";
    // 	// $maxNo = Yii::$app->db->createCommand($sql)->queryScalar();
    // 	// $orderNumber = isset($maxNo) ? $maxNo : 0;

    // 	// return $orderNumber;
    // }

    /**
     * Save as new variable list
     *
     * @param $id Integer experiment identifier
     * @param $idList Array list of variable identifiers
     * @param $attributes Array list attributes
     * @param $protocolType String Type of protocol - either traits or management
     * @return $errorMsg Text error message
     */
    public function saveVariableList($id, $idList, $attributes,$protocolType){
        $errorMsg = '';

        extract($attributes);

        $listInfo =  $this->lists->searchAll(['abbrev' => "equals $abbrev"],'showWorkingList=true&limit=1&sort=listDbId:asc', false);

        if(empty($listInfo['success'])){
            return 'Unable to retrieve list info';
        }
    
        $abbrevExists = isset($listInfo['data'][0]['abbrev']) ? $listInfo['data'][0]['abbrev'] : '';
    
        if(!empty($abbrevExists)){
            $errorMsg = 'Abbrev already exists';
        }

        $listInfo = $this->lists->searchAll(['abbrev' => "equals $displayName"],'showWorkingList=true&limit=1&sort=listDbId:asc', false);
        
        $displayNameExists = isset($listInfo['data'][0]['displayName']) ? $listInfo['data'][0]['displayName'] : '';
        
        if(!empty($displayNameExists)){
            $errorMsg = 'Display name already exists';
        }

        // if no error encountered
        if(empty($errorMsg)){
            if($protocolType == 'MANAGEMENT'){
                // create list
                $remarks = 'created using Experiment Creation tool';
                $requestData['records'][] = [
                    'abbrev' => $abbrev,
                    'name' => $name,
                    'displayName' => $displayName,
                    'remarks' => $remarks,
                    'type' => 'variable',
                    'subType' => 'management',
                    'listUsage' => 'final list',
                    'status' => 'created',
                    'isActive' => 'true'
                ];

                // save list basic info
                $newListInfo = $this->lists->create($requestData);
                $listId =  isset($newListInfo['data'][0]['listDbId']) ? $newListInfo['data'][0]['listDbId'] : '';
               
                if($listId && $newListInfo['success']){
                    $experimentInfo = $this->experiment->getOne($id);
                    $experiment = isset($experimentInfo['data']) ? $experimentInfo['data'] : [];
                    $experimentCode = isset($experiment['experimentCode']) ? $experiment['experimentCode'] : '';

                    $protocolCode = strtoupper($protocolType)."_PROTOCOL_".$experimentCode;
        
                    $protocol = $this->protocol->searchAll(["protocolCode"=>"$protocolCode"],'limit=1&sort=protocolDbId:asc',false);
                    $protocolDbId = isset($protocol['data'][0]['protocolDbId']) ? $protocol['data'][0]['protocolDbId'] : '';
                      

                    //add to experimentData
                    $variable = $this->variable->searchAll(['abbrev' => 'equals MANAGEMENT_PROTOCOL_LIST_ID'],'limit=1&sort=variableDbId:asc',false);

                    $variableDbId = $variable['data'][0]['variableDbId'];
                    $insertData[] = [
                        'variableDbId' => "$variableDbId",
                        'dataValue' => "$listId",
                        'protocolDbId' => "$protocolDbId",
                    ];

                    $checkExpDataArr = ["variableDbId"=>"equals $variableDbId", "protocolDbId"=>"equals $protocolDbId"];

                    //Search all the experiment data for the protocol
                    $savedExptData = $this->experimentData->searchAllRecords($id,$checkExpDataArr);
                        
                    if(count($savedExptData) == 0){
                         $this->experiment->createExperimentData($id, $insertData);
                    }
                 
                  
                    //add list access
                    $this->platformListAccess->addAccess($listId, [], [$experiment['programDbId']], 'read_write');

                    //Add the list members
                    foreach($idList as $varId){
                        $variable =  $this->variable->getOne($varId);
                        $requestDataMem[] = [
                            'id' => $varId,
                            'displayValue' => $variable['data']['label']
                        ];
                    }
                    
                     $this->lists->createMembers($listId, $requestDataMem);
                }
            }
        }

        return $errorMsg;
    }
    
    /**
     * Get saved experiment protocols
     * @param Integer $id experiment identifier
     * @param Integer $occurenceIds Array of occurrence ids
     */
    public function getExperimentProtocols($id, $occurrences){
        //Get the protocol ids of planting, pollination, and harvest protocols
        $experimentProtocols = $this->experimentProtocol->searchAll(["experimentDbId"=>"equals $id","protocolType"=>"equals planting|equals pollination|equals harvest|equals trait|equals management"]);
        
        $experimentInfo = $this->experiment->getOne($id);
        $experiment = isset($experimentInfo['data']) ? $experimentInfo['data'] : [];

        $protocolCond = '';
        foreach($experimentProtocols['data'] as $val){
            $protocolCond = empty($protocolCond) ? 'equals '.$val['protocolDbId'] : $protocolCond.'|equals '.$val['protocolDbId'];
        }

        $path = "experiments/".$id."/data-search";
        $method = 'POST';
     
        $protocolArr = [];

        if(!empty($protocolCond)){
            $response = Yii::$app->api->getParsedResponse($method, $path, json_encode(["protocolDbId"=>$protocolCond]),null,true);
            
            //Build the protocol data array
            if(!empty($response['data'][0]['data'])){
                foreach($occurrences as $occurrence){
                    foreach($response['data'][0]['data'] as $val){
                        $requestDataMem = [];
                        if($val['abbrev'] == "TRAIT_PROTOCOL_LIST_ID" || $val['abbrev'] == "MANAGEMENT_PROTOCOL_LIST_ID"){
                            //create list and list members for each of the occurrences
                            $listMemberInfo = $this->lists->searchAllMembers($val['dataValue']);

                            if($val['abbrev'] == "TRAIT_PROTOCOL_LIST_ID") {
                                $initAbbrev = 'TRAIT_PROTOCOL';
                                $type = 'trait';
                                $initName = 'Trait Protocol';
                            } else {
                                $initAbbrev = 'MANAGEMENT_PROTOCOL';
                                $type = 'variable';
                                $initName = 'Management Protocol';
                            }
                            $occurrenceCode = $occurrence['occurrenceCode'];
                            $abbrev = $initAbbrev."_".$occurrenceCode;

                            //create lists
                            $name = $displayName = $occurrence['occurrenceName']." $initName ($occurrenceCode)";
                            $remarks = 'created using Experiment Creation tool';
                            $requestData = null;
                            $requestData['records'][] = [
                                'abbrev' => $abbrev,
                                'name' => $name,
                                'displayName' => $displayName,
                                'remarks' => $remarks,
                                'type' => $type,
                                'listUsage' => 'working list',
                            ];
                            
                            // save list basic info
                            $newListInfo = $this->lists->create($requestData);
                            
                            $listId = $newListInfo['data'][0]['listDbId'] ?? '';
               
                            if($listId && $newListInfo['success']){
                                //add list access
                                $this->platformListAccess->addAccess($listId, [], [$experiment['programDbId']], 'read_write');

                                //Add the list members
                                if($listMemberInfo['totalCount'] > 0){
                                    foreach($listMemberInfo['data'] as $listMember){
                                        $requestDataMem[] = [
                                            'id' => $listMember['variableDbId'],
                                            'displayValue' => $listMember['displayValue'],
                                            'remarks' => $listMember['remarks'],
                                        ];
                                    }
                                    
                                    $this->lists->createMembers($listId, $requestDataMem);
                                }
                            }
                            $protocolArr[] = [
                                'occurrenceDbId' => $occurrence['occurrenceDbId'],
                                'variableDbId' => $val['variableDbId'],
                                'dataValue' => $listId,
                                'protocolDbId' => $val['protocolDbId']
                            ];
                        } else {
                            $protocolArr[] = [
                                'occurrenceDbId' => $occurrence['occurrenceDbId'],
                                'variableDbId' => $val['variableDbId'],
                                'dataValue' => $val['dataValue'],
                                'protocolDbId' => $val['protocolDbId']
                            ];
                        }
                    }
                }
            }
        }  
       
        return $protocolArr; 
    }

    /**
     * Returns saved trait list members
     *
     * @param int $experimentDbId experiment identifier
     * @param string $level data-level of the protocol
     */
    public function getSavedTraits($experimentDbId, $level='EXPERIMENT')
    {
        $output = [];
        if ($level === 'EXPERIMENT') {
            $result = $this->experimentProtocol->searchAll([
                'experimentDbId' => "equals $experimentDbId",
                'protocolType' => 'equals trait'
            ], 'limit=1&sort=experimentDbId:asc');
            $protocolDbId = $result['data'][0]['protocolDbId'] ?? '';

            if (!empty($protocolDbId)) {
                $method = 'POST';
                $path = "experiments/$experimentDbId/data-search";
                $result = Yii::$app->api->getParsedResponse($method, $path, json_encode(['protocolDbId' => "equals $protocolDbId"]), null, true);

                $result = $this->lists->searchAllMembers($result['data'][0]['data'][0]['dataValue']);
                foreach ($result['data'] as $listMember){
                    $output[] = [
                        'id' => $listMember['variableDbId'],
                        'abbrev' => $listMember['abbrev'],
                        'remarks' => $listMember['remarks'],
                    ];
                }
            }
        }

        return $output;
    }


    // /**
    //  * TODO: To be refactored
    //  * Reorder variables
    //  *
    //  * @param $id integer experiment identifier
    //  * @param $idList array list of variable identifiers
    //  */
    // public function reorderVariables($id,$idList){
    // 	// $listId = $this->experimentProtocol->getListId($id);

    // 	// $sql = "
    // 	// 	update platform.list_member n
    // 	// 		set order_number = t.order_number
    // 	// 	from (
    // 	// 		select
    // 	// 			row_number() over(order by position(data_id::text in '{$idList}')) as order_number,
    // 	// 			data_id,
    // 	// 			list_id
    // 	// 		from
    // 	// 		platform.list_member
    // 	// 		where list_id = $listId
    // 	// 		and is_void = false
    // 	// 	) as t
    // 	// 	where
    // 	// 		t.data_id = n.data_id
    // 	// 		and n.list_id = t.list_id
    // 	// 		and is_void = false";
                
    //     // Yii::$app->db->createCommand($sql)->execute();
    // }

}