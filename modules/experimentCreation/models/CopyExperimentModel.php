<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Copy Controller
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Config;
use app\models\Cross;
use app\models\CrossSearch;
use app\models\Entry;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Item;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\Variable;

use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;

use app\modules\experimentCreation\models\BrowserModel;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\FormModel;
use app\modules\experimentCreation\models\PlantingArrangement;
use app\modules\experimentCreation\models\VariableComponent;
use Yii;
class CopyExperimentModel {
    protected $browserModel;
    protected $configModel;
    protected $entry;
    protected $entryList;
    protected $entryOrderModel;
    protected $experiment;
    protected $experimentData;
    protected $experimentBlock;
    protected $formModel;
    protected $item;
    protected $occurrence;
    protected $planTemplate;
    protected $plantingArrangement;
    protected $variable;
    protected $variableComponent;

    public function __construct(
        BrowserModel $browserModel,
        Config $configModel,
        Entry $entry, 
        EntryList $entryList, 
        EntryOrderModel $entryOrderModel,
        Experiment $experiment,
        ExperimentData $experimentData,
        ExperimentBlock $experimentBlock,
        ExperimentModel $experimentModel,
        FormModel $formModel,
        Item $item,
        Occurrence $occurrence,
        PlanTemplate $planTemplate,
        PlantingArrangement $plantingArrangement,
        UserDashboardConfig $userDashboardConfig,
        Variable $variable,
        VariableComponent $variableComponent)
    {
        $this->browserModel = $browserModel;
        $this->configModel = $configModel;
        $this->entry = $entry;
        $this->entryList = $entryList;
        $this->entryOrderModel = $entryOrderModel;
        $this->experiment = $experiment;
        $this->experimentData = $experimentData;
        $this->experimentBlock = $experimentBlock;
        $this->experimentModel = $experimentModel;
        $this->formModel = $formModel;
        $this->item = $item;
        $this->occurrence = $occurrence;
        $this->planTemplate = $planTemplate;
        $this->plantingArrangement = $plantingArrangement;
        $this->userDashboardConfig = $userDashboardConfig;
        $this->variable = $variable;
        $this->variableComponent = $variableComponent;
    }
    /**
     * Save the default arrangement for entry order
     * 
     * @param Integer $id experiment identifier
     * @param string $program name of the current program
     * @param Array $copyInformation array list of experiment steps to be copied
     * 
     */
    public function getPreviewInformation($id, $program, $copyInformation)
    {
        $model = $this->experiment->getExperiment($id);
        $processId = !empty($model) ? $model['dataProcessDbId'] : "";
        $activities = [];
        $basicInformation = [];
        $copyDesign = NULL;
        if(in_array('specify-basic-info', $copyInformation)){ //get information for basic information
            $basicInformation = [
              'data'=>$this->getBasicInformation($id, $program, $model),
              'label'=>'Basic Information',
              'action'=>'specify-basic-info'
            ];
        }
        if(in_array('specify-entry-list', $copyInformation)){
            $data = $this->getEntryListInformation($id, $model);
            
            $activities[] = [
                'label'=>Yii::t('app', $data['displayName']),
                'data'=>$data,
                'action'=>'specify-entry-list'
            ];
        }
        if(in_array('manage-crosses', $copyInformation)){
            $activities[] = [
                'label'=>Yii::t('app','Crosses'),
                'data' => $this->getCrossesInformation($id, $model),
                'action'=>'manage-crosses'
            ];
        }
        if(in_array('specify-design', $copyInformation)){

            $param = ['entityType'=>'equals parameter_sets', 'entityDbId'=> "equals $id"];
            $designInfo = $this->planTemplate->getPlanTemplateByEntity($param);

            $dataDesign = [];
            if($designInfo != null){
                $dataDesign = $this->getDesignInformationParamSet($id, $model);
                $dataDesign['isParameterSet'] = true;
            } else {
                $dataDesign = $this->getDesignInformation($id, $model);
                $dataDesign['isParameterSet'] = false;
            }

            $copyDesign = $dataDesign['copyDesign'];
            $activities[] = [
                'label'=>Yii::t('app','Design'),
                'data' => $dataDesign,
                'action'=>'specify-design',
            ];
        }
        if(in_array('specify-cross-design', $copyInformation)){
            if(strtolower($model['experimentDesignType']) == 'systematic arrangement'){
                $data = $this->getPlantingArrangementSA($id, $model);
            } else if(strtolower($model['experimentDesignType']) == 'entry order'){
              $data = $this->getPlantingArrangementEO($id, $model);
            }
            $activities[] = [
                'label' => Yii::t('app','Planting Arrangement'),
                'data' => $data,
                'action'=>'specify-cross-design'
            ];
        }
        if(in_array('specify-protocols', $copyInformation)){
            $activities[] = [
                'label'=>Yii::t('app','Protocols'),
                'data' => $this->getProtocolInformation($id, $program),
                'action'=>'specify-protocols'
            ];
        }
        if(in_array('specify-occurrences', $copyInformation)){
            $activities[] = [
                'label'=>Yii::t('app','Sites'),
                'data' => $this->getOccurrencesInformation($id, $model),
                'action'=>'specify-occurrences'
            ];
        }

        return [
            'activities' => $activities,
            'basicInformation' => $basicInformation,
            'copyDesign' => $copyDesign
        ];
    }

    /**
     * Get the basic information of the experiment
     * 
     * @param Integer $id experiment identifier
     * @param string $program name of the current program
     */
    public function getBasicInformation($id, $program, $model){

        $dataProcessDbId = !empty($model) ? $model['dataProcessDbId'] : "";
        $activities = $this->formModel->prepare('specify-basic-info', $program, $id, $dataProcessDbId);

        $requiredConfigValues = [];
        if(!empty($activities['children'])){
            $index = array_search($activities['active'], array_column($activities['children'], 'action_id'));
            $activityAbbrev = $activities['children'][$index]['abbrev'].'_VAL';
      
            //Get All required variables for basic info
            $requiredConfigValues = $this->configModel->getConfigByAbbrev($activityAbbrev)['Values']; 
        }
      
        return [
            'mainModel' => 'Experiment',
            'config' => $requiredConfigValues,
            'templateId' => $dataProcessDbId,
            'type' => isset($model['experimentType']) ? $model['experimentType']:"",
            'processType' => 'experiment',
            'mainApiResourceMethod' => 'POST',
            'mainApiResourceEndpoint' => 'experiments/'.$id.'/data-search',
            'mainApiSource' => 'metadata',
            'mainApiResourceFilter' => ["experimentDbId"=>"$id"],
            'program' => $program,
            'id' => $id,
            'collapsedDefault' => true
        ];
        
    
    }

    /**
     * Get the data for entry list of the experiment
     * 
     * @param Integer $id experiment identifier
     * @param Array $model array information of the experiment
     */
    public function getEntryListInformation($id, $model){

        $dataProcessAbbrev = isset($model['dataProcessAbbrev']) ? $model['dataProcessAbbrev'] : "";
        $parentActAbbrev = str_replace('_DATA_PROCESS', '', $dataProcessAbbrev);
        $entryListActAbbrev = $parentActAbbrev.'_ENTRY_LIST_ACT';


        // get entries/parents
        $entryListAct = $this->item->getAll("abbrev=$entryListActAbbrev");
        $displayName = isset($entryListAct['data'][0]['displayName']) ? strtolower($entryListAct['data'][0]['displayName']) : 'entry list';
        $entryListActValues = $this->configModel->getAll("abbrev=$entryListActAbbrev".'_VAL');
        $varConfigValues = !empty($entryListActValues['data']) ? $entryListActValues['data'][0]['configValue']['Values']:[];

        //For the entry/parent list browser 
        // get params for data browser
        $paramPage = '';
        $paramSort = '';
        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        } else if (isset($_GET['grid-entry-list-grid-page'])) {
            $paramPage = '&page=' . $_GET['grid-entry-list-grid-page'];
        }
        $paramLimit = '?limit=10';

        // get sorting
        if (isset($_GET['sort'])) {
            $paramSort = 'sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        $pageParams = $paramLimit.$paramPage;
        // append sorting condition
        $pageParams .= empty($pageParams) ? '?'.$paramSort : '&'.$paramSort;
        $entryDataProvider = $this->entry->getDataProvider($id, $varConfigValues, null, true, null,$pageParams);
            
        $configReviewColumns = $this->getReviewColumns($varConfigValues);
        $entryCount = $entryDataProvider['dataProvider']->getTotalCount();

        $entryGridColumns = [
            [
                'attribute'=>'entryNumber',
                'label' => '<span title="Sequential numbering of the entries">'.Yii::t('app', 'Entry No.').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
            ],     
            [
                'attribute'=>'seedName',
                'label' => '<span title="Seed code">'.Yii::t('app', 'Seed name').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
            ],
            [
                'attribute'=>'designation',
                'label' => '<span title="Gemplasm">'.Yii::t('app', 'Germplasm Name').'</span>',
                'format' => 'raw',
                    'format' => 'raw',                
                'format' => 'raw',
                'encodeLabel'=>false,
            ],
        ];
    
        $entryGridColumns = array_merge($entryGridColumns,$configReviewColumns);
        return [
            'dataProvider' => $entryDataProvider['dataProvider'],
            'columns' => $entryGridColumns,
            'displayName' => $displayName
        ];
    }

    /**
     * Get the data for basic information of the experiment
     * 
     * @param Integer $id experiment identifier
     * @param Array $model array information of the experiment
     */
    public function getCrossesInformation($id, $model){

        // get crosses
        $dataProcessAbbrev = $model['dataProcessAbbrev'];
        $parentActAbbrev = str_replace('_DATA_PROCESS', '', $dataProcessAbbrev);
        $crossesActAbbrev = $parentActAbbrev.'_CROSSES_ACT';

        $crossesAct = $this->item->searchAll(["abbrev"=>"equals $crossesActAbbrev"])['totalCount'] > 0;
        $crossDataProvider = null;
        if($crossesAct){
            $searchModel = new CrossSearch($this->browserModel, $this->userDashboardConfig); 
            $paramFilter = Yii::$app->request->queryParams;

            $mainParam = ["experimentDbId"=>"equals $id"];
            
            $crossDataProvider = $searchModel->search($mainParam, $paramFilter, true, null, true);

        }

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'crossName',
                'format' => 'raw',
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['crossName']) ? $data['crossName'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'crossFemaleParent',
                'label' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'red lighten-4'],
                'contentOptions'=>['class'=>'red lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['crossFemaleParent']) ? $data['crossFemaleParent'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'femaleParentage',
                'label' => '<span title="Female Parentage"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENTAGE').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'red lighten-4'],
                'contentOptions'=>['class'=>'red lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['femaleParentage']) ? $data['femaleParentage'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'crossMaleParent',
                'label' => '<span title="Male Parent">'.Yii::t('app', '<i class="fa fa-mars"></i> MALE PARENT').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'blue lighten-4'],
                'contentOptions'=>['class'=>'blue lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['crossMaleParent']) ? $data['crossMaleParent'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'maleParentage',
                'label' => '<span title="Male Parentage"><i class="fa fa-mars "></i>'.Yii::t('app', ' MALE PARENTAGE').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'blue lighten-4'],
                'contentOptions'=>['class'=>'blue lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['maleParentage']) ? $data['maleParentage'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'crossMethod',
                'label' => Yii::t('app','METHOD'),
                'format' => 'raw',
                'encodeLabel' => false,
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['crossMethod']) ? $data['crossMethod'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'crossRemarks'
            ],
            [
                'attribute' => 'femaleSourceEntry',
                'label' => '<span title="Female Source Entry"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE SOURCE ENTRY').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'red lighten-4'],
                'contentOptions'=>['class'=>'red lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['femaleSourceEntry']) ? $data['femaleSourceEntry'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'maleSourceEntry',
                'label' => '<span title="Male Source Entry"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE SOURCE ENTRY').'</span>',
                'encodeLabel' => false,
                'format' => 'raw',
                'headerOptions'=>['class'=>'blue lighten-4'],
                'contentOptions'=>['class'=>'blue lighten-5'],
                'vAlign'=>'middle',
                'value' => function($data){
                    return !empty($data['maleSourceEntry']) ? $data['maleSourceEntry'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'femaleParentSeedSource',
                'label' => '<span title="Female Seed Source"><i class="fa fa-venus"></i>'.Yii::t('app', 'FEMALE SEED SOURCE').'</span>',
                'format' => 'raw',
                'encodeLabel' => false,
                'vAlign'=>'middle',
                'headerOptions'=>['class'=>'red lighten-4'],
                'contentOptions'=>['class'=>'red lighten-5'],
                'value' => function($data){
                    return !empty($data['femaleParentSeedSource']) ? $data['femaleParentSeedSource'] : '<span class="not-set">(not set)</span>';
                }
            ],
            [
                'attribute' => 'maleParentSeedSource',
                'label' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', 'MALE SEED SOURCE').'</span>',
                'format' => 'raw',
                'encodeLabel' => false,
                'vAlign'=>'middle',
                'headerOptions'=>['class'=>'blue lighten-4'],
                'contentOptions'=>['class'=>'blue lighten-5'],
                'value' => function($data){
                    return !empty($data['maleParentSeedSource']) ? $data['maleParentSeedSource'] : '<span class="not-set">(not set)</span>';
                }
            ]
        ];

        return [
            'dataProvider' => $crossDataProvider,
            'columns' => $columns,
            'displayName' => 'Cross List'
        ];
    }

    /**
     * Get overview of the design for trials
     *
     * @param string $program - Program identifier
     * @param Array $model array information of the experiment
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function getDesignInformation($id, $model)
    {
    
          
        $entryListId = $this->entryList->getEntryListId($id);
        $entryListCheckArray = $this->entry->searchAll(['entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check'], 'limit=1', false);
        $entryListEntryArray = $this->entry->searchAll(['entryListDbId'=>"equals $entryListId", 'entryType'=>'equals test'], 'limit=1', false);
        $entryListTotalArray = $this->entry->searchAll(['entryListDbId'=>"equals $entryListId"], 'limit=1', false);

        $entryInfo  = array();
        
        $entryInfo['check'] = $entryListCheckArray['totalCount'];
        $entryInfo['entry'] = $entryListEntryArray['totalCount'];
        $entryInfo['total'] = $entryListTotalArray['totalCount'];
        $designFields = array();
        $layoutFields = array();

        //POST-search call
        $param = ['entityType'=>'equals experiment', 'entityDbId'=> "equals $id"];

        $jsonInput = $this->planTemplate->getPlanTemplateByEntity($param);
        $showLayout = FALSE;
        $paramValues = [];
        $upload = FALSE;
        if(isset($jsonInput) && $jsonInput != NULL){

            $jsonArr = json_decode($jsonInput['mandatoryInfo'],true);

            if(isset($jsonArr['design_ui'])){
                $parameter = $jsonArr['design_ui'];
                $indexArr = array_search('genLayout', array_column($jsonArr['design_ui']['randomization'], 'code'));
                $showLayout = (isset($jsonArr['design_ui']['randomization'][$indexArr]['value']) && $jsonArr['design_ui']['randomization'][$indexArr]['value'] == 'true') ? TRUE : FALSE;
                $designAF = isset($jsonArr['design_id']) ? $jsonArr['design_id'] : NULL;
            } else {
                $upload = TRUE;
            }

            if(isset($parameter['randomization'])){

                foreach($parameter['randomization'] as $key=>$var){
                  
                    if(isset($var['ui']['visible']) && $var['ui']['visible']){
                        $designFld = $this->variableComponent->generateDesignField($var, 'randomization', $key);
                        $designFields[] = $designFld;
                        $paramValues[$var['code']] = $designFld['defaultValue'];
                    }
                }
            }

            if(isset($parameter['layout'])){

              foreach($parameter['layout'] as $key=>$var){
                
                  if(isset($var['ui']['visible']) && $var['ui']['visible']){
                      $layoutFld = $this->variableComponent->generateDesignField($var, 'layout', $key);
                      $layoutFields[] = $layoutFld;
                      $paramValues[$var['code']] = $layoutFld['defaultValue'];
                  }
              }
            }
        }
        //create entry info data provider
        $entryInfoDataProvider = new ArrayDataProvider([
          'key'=>'total',
          'allModels' => [$entryInfo]
        ]);

        $copyDesign = TRUE;
        if($jsonInput == NULL || $upload){
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> There is no saved design parameters for this experiment. This will only copy the basic information and entry list of the experiment.');
            $copyDesign = FALSE;
        }
        
        return [
            'experimentDesign' => $model['experimentDesignType'],
            'designFields' => $designFields,
            'layoutFields' => $layoutFields,
            'planDesign' => $jsonInput,
            'withChecks' =>  $entryInfo['check'] > 0 ? TRUE:FALSE,
            'showLayout' => $showLayout,
            'entryInfo' => json_encode($entryInfo),
            'entryInfoDataProvider' => $entryInfoDataProvider,
            'paramValues' => json_encode($paramValues),
            'copyDesign' => $copyDesign
        ];
    }


    /**
     * Get overview of the design for trials using the Parameter set UI
     *
     * @param string $program - Program identifier
     * @param Array $model array information of the experiment
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function getDesignInformationParamSet($id, $model)
    {
    
        $entryListId = $this->entryList->getEntryListId($id);
        $entryListCheckArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check'], '', true);
        $entryListLocalArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId|entry.entry_role as entryRole','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check', 'entryRole'=>'equals local check'], '', true);

        $entryListEntryArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals test'], 'limit=1', false);
        $entryListTotalArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId"], 'limit=1', false);

        $entryInfo  = array();
        $entryInfo['local'] = $entryListLocalArray['totalCount'];
        $entryInfo['check'] = $entryListCheckArray['totalCount'] - $entryListLocalArray['totalCount'];
        $entryInfo['test'] = $entryListEntryArray['totalCount'];
        $entryInfo['total'] = $entryListTotalArray['totalCount'];

        //create entry info data provider
        $entryInfoDataProvider = new ArrayDataProvider([
          'key'=>'total',
          'allModels' => [$entryInfo]
        ]);

        //if there's no workflow saved in experiment remarks, default is null
        $workflow = $model['remarks'] ?? null;

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id"], 'sort=planTemplateDbId:ASC');

        $experimentJson = isset($experimentJsonArray['data'][0]) ? $experimentJsonArray['data'][0]:null;

        $planTemplateDbId = isset($experimentJson['planTemplateDbId']) ? $experimentJson['planTemplateDbId'] : [];

        $variableInfo = [];
        if(isset($experimentJson['planTemplateDbId']) && $experimentJson['notes']!= null){
            $variableInfo = json_decode($experimentJson['notes'], true);
            
        }

        //check if there's saved across env design variable, if none proceed with old UI
        $designRecord = $this->variable->getVariableByAbbrev('ACROSS_ENV_DESIGN');
       
        $designVariable = $designRecord['variableDbId'] ?? null;

        $acrossEnvDesign = $this->experimentData->getExperimentData($id,[
            'variableDbId' => 'equals '.$designVariable
        ]);

        $acrossEnvDesign = (empty($acrossEnvDesign) || count($acrossEnvDesign) == 0) ? 'none':'random';

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'not equals incomplete'], 'sort=planTemplateDbId:ASC');
        

        $addDesignFields = [];
        $paramValues = [];
        $totalOccurrences = 0;
        foreach($variableInfo as $key=>$var){
            if(isset($var['ui']['visible']) && $var['ui']['visible']){
                $designFld = $this->variableComponent->generateDesignField($var, 'additional', $key, true, 'Additional');
                $addDesignFields[] = $designFld;
                $paramValues[$var['code']] = $designFld['defaultValue'];
            }
            if($var['code'] == 'nExptTrial'){
                $totalOccurrences = isset($var['value']) ? (int)$var['value'] : (int)$var['ui']['defaultValue'];
            }
        }
        //create parameter sets table
        $parameterSets = [];
        $columns = [];
        $firstRec = false; 
        $setNo = 1;
        $assignedOccurrences = 0;
        $planStatus = 'draft';
        foreach($experimentJsonArray['data'] as $record){
            $tempRow = [];
            $jsonInput = json_decode($record['mandatoryInfo'],true);
            $planStatus = $record['status'];

            $tempRow['status'] = $record['status'];
            $tempRow['remarks'] = $record['remarks'];
            $tempRow['planTemplateDbId'] = $record['planTemplateDbId'];
            $tempRow['design'] = $record['design'];
            $tempRow['setNumber'] = $setNo++;
            $tempRow['addtlData'] = $jsonInput;
            if(!$firstRec){
                $columns[] = [
                    'label' => "",
                    'format' => 'raw',
                    'value' => function ($data) {
                    
                        $setNumber = $data["planTemplateDbId"];
                        $checked='';
                        $addClass = '';
                        return '<input type="radio" id="option-param-'.$setNumber.'" name="option" class="option-paramsets" value="option-'.$setNumber.'"> <label for="option-param-'.$setNumber.'"></label>';
                      
                    }
                ];
                $columns[] = [
                    'attribute'=>'setNumber',
                    'label' => '<span title="Parameter sets">'.Yii::t('app', 'Set').'</span>',
                    'format' => 'raw',
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
                
                $columns[] = [
                    'attribute'=>'status',
                    'label' => '<span title="Parameter set status">'.Yii::t('app', 'Status').'</span>',
                    'format' => 'raw',
                    'value' => function($data){
                        $status = '';
                        if($data['status'] == 'randomization in progress' || $data['status'] == 'deletion in progress'){
                            $status = '<span class="new badge center amber darken-2">'.strtoupper($data['status']).'</span>';
                        } else if ($data['status'] == 'randomized'){
                            $status = '<span class="new badge blue darken-2">'.strtoupper($data['status']).'</span>';
                        } else if ($data['status'] == 'randomization failed'){
                            $status = '<span class="new badge red darken-2">'.strtoupper($data['status']).'</span>';
                        } 
                        else {
                            $status = '<span class="new badge grey">'.strtoupper($data['status']).'</span>';
                        }
                        return $status;
                    },
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
                $columns[] = [
                    'attribute'=>'design',
                    'label' => '<span title="Parameter set design">'.Yii::t('app', 'Design').'</span>',
                    'format' => 'raw',
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
            }

            $strLimit = 20;
            if(isset($jsonInput['design_ui']['randomization'])){
                foreach($jsonInput['design_ui']['randomization'] as $ui){
                    if($ui['ui']['visible']){
                        $tempRow[$ui['code']] = isset($ui['value']) ? $ui['value'] : null;
                        if(!$firstRec){
                            $code =  (strlen($ui['variableLabel']) > $strLimit ) ? substr($ui['variableLabel'],0,$strLimit ).'...' : $ui['variableLabel'];
                            $columns[] = [
                                'attribute'=>$ui['code'],
                                'label' =>'<span title="'.$ui['variableLabel'].'">'.Yii::t('app', $code).'</span>',
                                'format' => 'raw',
                                'enableSorting' => false,
                                'encodeLabel'=>false,
                            ];
                        }

                        if($ui['code'] == 'nTrial'){
                            $assignedOccurrences += isset($ui['value']) ? $ui['value'] : null;
                        }
                    }
                }
            }

            if(isset($jsonInput['design_ui']['layout'])){
                foreach($jsonInput['design_ui']['layout'] as $ui){
                    if($ui['ui']['visible']){
                        $tempRow[$ui['code']] = isset($ui['value']) ? $ui['value'] : null;
                        if(!$firstRec){
                            $code =  (strlen($ui['variableLabel']) > $strLimit ) ? substr($ui['variableLabel'],0,$strLimit ).'...' : $ui['variableLabel'];
                            $columns[] = [
                                'attribute'=>$ui['code'],
                                'label' => '<span title="'.$ui['variableLabel'].'">'.Yii::t('app', $code).'</span>',
                                'format' => 'raw',
                                'enableSorting' => false,
                                'encodeLabel'=>false,
                            ];
                        }
                    }
                }
            }

            if(isset($jsonInput['design_ui']['rules'])){
                $rules = $jsonInput['design_ui']['rules'];
            }

            if(isset($jsonInput['design_ui']['occurrenceDbIds']) && $workflow == 'upload'){
                $tempRow['occurrenceCount'] = count($jsonInput['design_ui']['occurrenceDbIds']);
                if(!$firstRec){
                    $columns[] = [
                        'attribute'=>'occurrenceCount',
                        'label' => '<span title="No. of Occurrences">'.Yii::t('app', 'No. of Occurrences').'</span>',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'encodeLabel'=>false,
                    ];
                }
            }
            $parameterSets[] = $tempRow;
            $firstRec = true;
        }

        //create entry info data provider
        $parameterSetsProvider = new ArrayDataProvider([
          'key'=>'planTemplateDbId',
          'allModels' => $parameterSets
        ]);

        $model = $this->experiment->getExperiment($id);

        $copyDesign = TRUE;
        if($jsonInput == NULL || $workflow == 'upload'){
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> There is no saved design parameters for this experiment. This will only copy the basic information and entry list of the experiment.');
            $copyDesign = FALSE;
        }
        
        return [
            'entryInfo' => json_encode($entryInfo),
            'entryInfoDataProvider' => $entryInfoDataProvider,
            'model' => $model,
            'id' => $id,
            'acrossEnvDesign' => $acrossEnvDesign,
            'addDesignFields'=>$addDesignFields,
            'parameterSetsProvider' => $parameterSetsProvider,
            'columns' => $columns,
            'copyDesign' => $copyDesign,
            'workflow' => $workflow
        ];
    }

    /**
     * Get overview of the planting arrangement for Entry Order
     *
     * @param string $program - Program identifier
     * @param Array $model array information of the experiment
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function getPlantingArrangementEO($id, $model)
    {
        //get all records of experiment blocks
        $params = ["experimentDbId" => "equals $id"];
        $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params);
        
        //Get experiment record
        $experiment = $this->experiment->searchAll(['experimentDbId' => "equals ".$id]);
        $experimentType = $experiment['data'][0]['experimentType'];

        //populate entryListIdList, layoutOrderData

        if(count($experimentBlockData) > 0){
            $blockData = $this->plantingArrangement->getBlockInformation($experimentBlockData[0]['experimentBlockDbId'], $experimentType);
            if($experimentBlockData[0]['entryListIdList'] == NULL){
                $this->entryOrderModel->saveBlockParams($id, $experimentBlockData[0]['experimentBlockDbId'], $blockData);
                $blockData = $this->plantingArrangement->getBlockInformation($experimentBlockData[0]['experimentBlockDbId'], $experimentType);
            }
        }
        

        $checkInformation = [];
        $blockData = [];
        //For Observation experiment type
        if($experimentType == 'Observation'){

            //Get occurrence count
            $varAbbrev = 'OCCURRENCE_COUNT';
            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
            $blockData['no_of_occurrences'] = (isset($expData) && !empty($expData))?$expData[0]['dataValue']:1;

            //get check information
            $entryListId = $this->entryList->getEntryListId($id);
            $checkRecords = $this->entry->searchAll(['fields'=>"entry.entry_list_id as entryListDbId|entry.id as entryDbId|entry.entry_number as entryNumber|germplasm.designation|entry.entry_type as entryType",'entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check']);
            
            $checkArray = array();
            if($checkRecords['totalCount'] > 0){

                foreach($checkRecords['data'] as $checks){
                    $checkArray[$checks['entryDbId']] =  $checks['entryNumber'].': '.$checks['designation'];
                }
            }
            $checkInformation = $this->entryOrderModel->getCheckInformation($id);
            $checkInformation['check_array'] = $checkArray;
        }
        
        $startingCornerValues = $this->plantingArrangement->getScaleValuesByAbbrev('STARTING_CORNER');
        $plotNumberingOrderValues = $this->plantingArrangement->getScaleValuesByAbbrev('PLOT_NUMBERING_ORDER');

        $experiment = $this->experiment->searchAll(['experimentDbId' => $id]);
        return [
            'id' => $id,
            'startingCornerValues' => $startingCornerValues,
            'plotNumberingOrderValues' => $plotNumberingOrderValues,
            'experimentType' => $experimentType,
            'experiment' => $experiment['data'][0],
            'blockData' => $blockData,
            'subBlockId' => isset($experimentBlockData[0]) ? $experimentBlockData[0]['experimentBlockDbId'] : null,
            'totalPlots' => isset($blockData['total_no_of_plots_in_block']) ? $blockData['total_no_of_plots_in_block'] : 0,
            'checkInformation' => $checkInformation
        ];


    }

    /**
     * Get overview of the planting arrangement for Systematic Arrangement
     *
     * @param string $program - Program identifier
     * @param Array $model array information of the experiment
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function getPlantingArrangementSA($id, $model)
    {
        $dataProvider = $this->plantingArrangement->getBlocksByExperimentId($id);
        $entryCountAssigned = $this->plantingArrangement->getTotalEntryCountAssignedToBlock($id);
        $plotCountAssigned = $this->plantingArrangement->getTotalPlotCountAssignedToBlock($id);
        $entryCount = $this->plantingArrangement->getTotalEntryCount($id);

        // blocks browser columns
        $columns = [
          [
              'header'=>'',
              'class' => 'yii\grid\SerialColumn',
          ],
          [
              'attribute'=>'experimentBlockCode',
              'label' => 'Block Code',
          ],
          [
              'attribute'=>'experimentBlockName',
              'label' => 'Block Name',
          ],
          [
              'attribute'=>'experimentType',
              'format'=>'raw',
              'contentOptions'=>function($data){
                  if($data['experimentType'] == 'Parent block'){
                      return ['class' => 'amber lighten-4'];
                  }else{
                      return ['class' => 'green lighten-4'];
                  }
              }
          ],
          [
              'attribute'=>'startingCorner',
              'contentOptions'=>function($data){
                  if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-4'];
                  }else{
                      return ['class' => 'teal lighten-4'];
                  }
              },
          ],
          [
              'attribute'=>'plotNumberingOrder',
              'contentOptions'=>function($data){
                  if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-4'];
                  }else{
                      return ['class' => 'teal lighten-4'];
                  }
              },
          ],
          [
              'header'=>'<span title="No. of rows in a block">Rows</div>',
              'attribute'=>'noOfRanges',
              'contentOptions'=>function($data){
                  if($data['noOfRanges'] == 0){
                      return ['class' => 'red lighten-4'];
                  }
                  else if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-4'];
                  }else{
                      return ['class' => 'teal lighten-4'];
                  }
              },
          ],
          [
              'header'=>'<span title="No. of columns in a block">Cols</div>',
              'attribute'=>'noOfCols',
              'contentOptions'=>function($data){
                  if($data['noOfCols'] == 0){
                      return ['class' => 'red lighten-4'];
                  }
                  else if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-4'];
                  }else{
                      return ['class' => 'teal lighten-4'];
                  }
              },
          ],
          [
              'attribute'=>'entryCount',
              'format'=>'raw',
              'contentOptions'=>function($data){
                  if($data['entryCount'] == 0){
                      return ['class' => 'red lighten-3'];
                  }
                  else if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-3'];
                  }else{
                      return ['class' => 'teal lighten-3'];
                  }
              },
              'label' => 'No. of entries',
          ],
          [
              'attribute'=>'plotCount',
              'format'=>'raw',
              'label' => 'No. of plots',
              'contentOptions'=>function($data){
                  if($data['plotCount'] == 0){
                      return ['class' => 'red lighten-3'];
                  }
                  else if($data['rowgroup'] % 2){
                      return ['class' => 'light-green lighten-3'];
                  }else{
                      return ['class' => 'teal lighten-3'];
                  }
              }
          ]
        ];

        $entryCountAssigned = (empty($entryCountAssigned)) ? 0 : $entryCountAssigned;
        $percent = round(($entryCountAssigned / $entryCount) * 100, 2);
        $percentClass = ($percent == 100) ? 'green-text' : 'red-text';
        $panel = [
          'heading'=> false,
          'after'=> false,
          'before' => '<table><tr>
                  <td style="padding:0px 10px 0px 0px">
                      <div class="progress">
                      <div class="determinate" style="width: '.$percent.'%"></div>
                      </div>
                      <div class="pull-left"> <b>'.$entryCountAssigned.'</b> of <b>'.$entryCount.'</b> <b class="'.$percentClass.' text-darken-2">('.$percent.'%)</b> entries are assigned to blocks</div>
                      <div class="pull-right">Total no. of plots: <b>'.$plotCountAssigned.'</b></div>
                  </td>
              </tr></table>',
          'footer' => false
        ];
        return [
            'id' => $id,
            'dataProvider' => $dataProvider,
            'entryCountAssigned' => $entryCountAssigned,
            'plotCountAssigned' => $plotCountAssigned,
            'entryCount' => $entryCount,
            'panel' => $panel,
            'columns' => $columns
        ];


    }
    /**
     * Get the data for occurrences of the experiment
     * 
     * @param Integer $id experiment identifier
     * @param Array $model array information of the experiment
     */
    public function getOccurrencesInformation($id, $model)
    {
        // get occurrences
        $occurrenceDataProvider = $this->occurrence->getDataProvider($id, null, [], true, ['occurrenceName'=>SORT_ASC]);
        $occurrenceRecord = $this->occurrence->searchAll(['experimentDbId' => "equals ".$id]);
        


        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'occurrenceName',
                'enableSorting' => false
            ],
            [
                'attribute'=>'occurrenceCode',
                'label' => '<span title="Occurrence Code">'.Yii::t('app', 'Code').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
                'enableSorting' => false
            ],
            [
                'attribute' => 'site',
                'enableSorting' => false
            ],
            [
                'attribute' => 'field',
                'enableSorting' => false
            ],
            [
                'attribute'=>'experimentStageCode',
                'label' => '<span title="Stage Code">'.Yii::t('app', 'Stage').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
                'enableSorting' => false
            ],
            [
                'attribute'=>'experimentYear',
                'label' => '<span title="Experiment Year">'.Yii::t('app', 'Year').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
                'enableSorting' => false
            ],
            [
                'attribute'=>'experimentSeason',
                'label' => '<span title="Experiment Season">'.Yii::t('app', 'Season').'</span>',
                'format' => 'raw',
                'encodeLabel'=>false,
                'enableSorting' => false
            ],
            [
                'attribute' => 'entryCount',
                'enableSorting' => false
            ],
            [
                'attribute' => 'plotCount',
                'enableSorting' => false
            ]
        ];

        return[
            'dataProvider' => $occurrenceDataProvider['dataProvider'],
            'columns' => $columns,
            'displayName' => 'Occurrence/s'
        ];
    }

    /**
     * View protocols of an occurrence
     *
     * @param string $program - Program identifier
     * @param integer $id - Unique Occurrence identifier
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function getProtocolInformation ($id, $program)
    {
        // Retrieve experimentDbId and dataProcessDbId
        $response = Yii::$app->api->getParsedResponse(
            'POST',
            'experiments-search',
            json_encode([
                'experimentDbId' => (string) $id,
            ]),
            'limit=1&sort=experimentDbId:asc',
            true
        );
        
        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        ) {
            $experimentDbId = $response['data'][0]['experimentDbId'];
            $experimentType = $response['data'][0]['experimentType'];
            $dataProcessDbId = $response['data'][0]['dataProcessDbId'];
            $dataProcessAbbrev = $response['data'][0]['dataProcessAbbrev'];
        }

        // Retrieve protocol parameters and config
        $protocolParams = $this->specifyProtocols(
            $experimentDbId,
            $experimentType,
            $dataProcessDbId,
            $dataProcessAbbrev,
            $program
        );

        $protocolParams = array_merge($protocolParams, [
            'experimentDbId' => $experimentDbId,
            'dataProcessDbId' => $dataProcessDbId,
            'program' => $program,
        ]);
        
        return $protocolParams;
    }
    

    /**
     * Specify Protocols of an Occurrence
     *
     * @param integer $experimentDbId Unique Experiment identifier
     * @param string $experimentType Experiment type identifier
     * @param integer $dataProcessDbId Unique Data Process identifier
     * @param string $dataProcessAbbrev Data Process abbreviation identifier
     * @param string $program Program identifier
     * 
     * @return mixed a rendered view with layout (if applicable)
     */
    public function specifyProtocols ($experimentDbId, $experimentType, $dataProcessDbId, $dataProcessAbbrev, $program)
    {
        if (
            (
                $dataProcessAbbrev !== 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS' ||
                (
                    stripos($experimentType, 'nursery') === false &&
                    !empty($entryCountAssignedToBlock)
                )
            ) ||
            stripos($experimentType, 'trial') === false
        ) {
            $activities = $this->formModel->prepare(
                'specify-protocols',
                $program,
                $experimentDbId,
                $dataProcessDbId
            );
            
            $index = array_search(
                $activities['active'],
                array_column($activities['children'],'url')
            );
        
            $activityChildren = $activities['children'][$index]['children'];
          
            $viewArr = [];
            
            if (!empty($activityChildren) && sizeof($activityChildren) > 0) {
              foreach ($activityChildren as $value) {
                    $viewArr []= [
                        'display_name' => $value['display_name'],
                        'action_id' => $value['action_id'],
                        'item_icon' => $value['item_icon']
                    ];
              }
            }

            return [
                'viewArr' => $viewArr,
                'protocolChildren' => $activityChildren
            ];
        }
    }
    /**
     * Get the config grid view columns for display in the review tab
     *  
     *  @param Array $varConfigFields variable fields in the configuration
     */
    public function getReviewColumns($varConfigFields)
    {
        $gridColumns = [];
        foreach($varConfigFields as $varConfigField){
            if(!(isset($varConfigField['is_hidden']) && $varConfigField['is_hidden'])){          
                $varAbbrev = $varConfigField['variable_abbrev'];
                $varEntry = $this->variable->getVariableByAbbrev($varAbbrev);
                $varLabel = isset($varConfigField['display_name']) ? $varConfigField['display_name'] : $varEntry['label'];
                $varDescription = isset($varEntry['description']) ? $varEntry['description'] : '';
                $attribute = ($varAbbrev == 'PARENT_TYPE') ? 'parent_type' : $this->experimentModel->setIndex($varAbbrev);

                $temp = [
                    'attribute' => $attribute,
                    'label' => '<span title="'.Yii::t('app', $varDescription).'">'.Yii::t('app', $varLabel).'</span>',
                    'format' => 'raw',
                    'encodeLabel'=>false,
                    'value'=> function($model) use ($attribute){
                        return isset($model[$attribute]) && !empty($model[$attribute])? ucwords($model[$attribute]) : NULL;
                    },
                ];
                $gridColumns[] = $temp;
            }
        }
        return $gridColumns;
    }

    /**
     * Copy and create a new experiment
     * 
     * @param integer $id record id of the experiment to be copied
     * @param array $model list of attributes of the experiment to be copied
     * 
     * @return integer $experimentDbId record id of the new copied experiment
     * 
     */
    public function copyNewExperiment($id, $model){

        $initialAttrs = [
            'experimentType','cropDbId','programDbId','stageDbId','experimentYear','seasonDbId','stewardDbId','experimentStatus',
            'pipelineDbId','experimentObjective','experimentType','experimentStatus','plantingSeason','projectDbId','dataProcessDbId',
            'experimentSubType','experimentSubSubType','experimentDesignType','description'
        ];

        $createRecord = [];
        foreach($initialAttrs as $attr){
            if(isset($model[$attr]) && $model[$attr] != null){
                $createRecord[$attr] = $model[$attr]."";
            }
        }
        $createRecord['experimentStatus'] = 'draft';

        //create experiment record
        $result = $this->experiment->createExperiment($createRecord);
        $newId = null;
        if($result['status']){
            $newId = $result['experimentId'];
            
            //copy experiment data
            $oldExpData = $this->experimentData->getExperimentData($id, ["protocolDbId" => "is null"]);
            $abbrevArray = [
                'CHECK_GROUPS',
                'BEGINS_WITH_CHECK_GROUP',
                'ENDS_WITH_CHECK_GROUP',
                'CHECK_GROUP_INTERVAL'
            ];
            $requestData = [];
            foreach($oldExpData as $data){
                if(!in_array($data['abbrev'], $abbrevArray)){
                    $requestData[]=[
                        'dataValue' => $data['dataValue']."",
                        'variableDbId' => $data['variableDbId'].""
                    ];
                }
            }
            
            if(!empty($requestData)){
                $this->experiment->createExperimentData($newId,$requestData);
            }
        }
        return $newId;
    }

}
