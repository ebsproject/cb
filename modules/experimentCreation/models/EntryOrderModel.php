<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Entry Order Controller
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Config;
use app\models\Entry;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Variable;

use yii\helpers\ArrayHelper;

use app\modules\experimentCreation\models\DesignModel;
use app\modules\experimentCreation\models\PlantingArrangement;
use Yii;
class EntryOrderModel {

    public function __construct(
        public Config $config, 
        public DesignModel $designModel,
        public Entry $entry, 
        public EntryList $entryList, 
        public Experiment $experiment,
        public ExperimentData $experimentData,
        public ExperimentBlock $experimentBlock,
        public PlantingArrangement $plantingArrangement,
        public Variable $variable)
    { }

    /**
     * Save the default arrangement for entry order
     * 
     * @param Integer $id experiment identifier
     * @param Integer $blockId block identifier
     * @param Array $blockData array list of block params
     * 
     */
    public function saveBlockParams($id, $blockId, $blockData)
    {

        $data = array_change_key_case($blockData, CASE_UPPER);

        //get entry list id
        $entryListId = $this->entryList->getEntryListId($id);
        $rawData = ([
            'fields'=>'entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber',
            'entryListDbId' => "equals $entryListId"
        ]);
        $entryData = $this->entry->searchAll($rawData,'sort=entryNumber:ASC');

        $entryIds = array_column($entryData['data'], 'entryDbId');
        $entryListIdList = [];
        foreach($entryIds as $entryId){
            $entryListIdList[] = [
                'entryDbId' => $entryId,
                'repno' => 1
            ];
        }
        $assignBlockStatus = 'overwrite';

        $this->plantingArrangement->saveBlockParams($id, $data, $entryIds, [$blockId], json_encode($entryListIdList),$assignBlockStatus);
    }
    

    /**
     * Check design type of the block
     * @param Integer $id  experiment identifier
     * @param Array $parameters  information
     */
    public function checkDesignType($id, $parameters)
    {

        $experiment = $this->experiment->getOne($id);

        extract($parameters);

        $check = false;
        $confirmFlag = false;
        
        if($designType == 'design-add-blocks'){
            if(isset($experiment['experimentDesignType']) && $experiment['experimentDesignType'] != 'Systematic Arrangement'){
                $check = true;
            }
        } else if($designType == 'design-entry-order'){
            if(isset($experiment['experimentDesignType']) && $experiment['experimentDesignType'] != 'Entry Order'){
                $check = true;
            }
        }

        if($check){

            $params = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                    experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.entry_list_id_list AS entryListIdList',
                "experimentDbId" => "equals $id"
            ];
            $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params, '?sort=orderNumber:DESC');

            $experimentBlockCount = isset($experimentBlockData[0]['experimentBlockDbId']) ? count($experimentBlockData) : 0;

            $confirmFlag = $this->confirmDesignType($experimentBlockData, $experimentBlockCount, $designType);
        }
        
        return $confirmFlag;
    }

    /**
     * Get the confirmation flag value based on the experiment block information
     * 
     * @param Array $experimentBlockData experiment block information array
     * @param Integer $experimentBlockCount total count of experiment blocks 
     * @param String $designType design type value
     */
    public function confirmDesignType($experimentBlockData, $experimentBlockCount, $designType)
    {
        $confirmFlag = false;
        //Check if change in tab needs confirmation
        if($experimentBlockCount == 0){
            $confirmFlag = false;
        } else {
            if($experimentBlockCount > 1){  //delete created default arrangement
                
                $confirmFlag = true;
            } else if($experimentBlockCount == 1){
                if($designType == 'design-add-blocks'){
                    if(strpos($experimentBlockData[0]['experimentBlockName'], 'Entry order') !== false){
                      
                        $confirmFlag = true;
                    }
                } else if($designType == 'design-entry-order'){
                  
                    if(strpos($experimentBlockData[0]['experimentBlockName'], 'Entry order') === false){
                    
                        $confirmFlag = false;
                    }

                    if(!empty($experimentBlockData[0]['entryListIdList'])){
                        $confirmFlag = true;
                    }
                }
            }
        }
        
        return $confirmFlag;
    }

     /**
     * Delete blocks for changes in entry list
     * 
     * @param integer $id Experiment ID
     * 
     */
    public function deleteBlocksUpdate($id){
        //delete all records of experiment blocks
        $params = [
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.order_number AS orderNumber',
            "experimentDbId" => "equals $id"
        ];
        $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params);

        $experimentBlockCount = isset($experimentBlockData[0]['orderNumber']) ? count($experimentBlockData) : 0;

        if($experimentBlockCount > 0){

            $blockData = ArrayHelper::map($experimentBlockData, 'experimentBlockDbId', 'experimentBlockDbId');
            $listOfExptBlockIds = array_keys($blockData);

            $this->experimentBlock->deleteMany($listOfExptBlockIds);

            // update status of experiment
            $this->plantingArrangement->updateExperimentStatus($id);
        }
    }

    /**
     * Update experiment when entry list is updated
     * @param integer $id record id of the experiment
     * @param array $newEntryIds list of new entry id
     * 
     */
    public function entryListAfterUpdate($id, $newEntryIds = null){
        //Get experiment record
        $model = $this->experiment->getOne($id);
            
        $experimentDesignType = "";
        $experimentStatus = "";
        $status = "";
        
        if(!empty($model)){
            $experimentDesignType = !empty($model['data']) ? $model['data']['experimentDesignType'] : "";
            $experimentStatus = !empty($model['data']) ? $model['data']['experimentStatus'] :"";
            $status = explode(';',$experimentStatus);
        }
        
        //update experiment status
        if(in_array($experimentDesignType, ['Entry Order'])){
          
            //check if design is already generated
            if(in_array('design generated', $status)){
                //check if nursery
                $this->experiment->updateOne($id, ["experimentDesignType"=>"Systematic Arrangement"]);

                //delete blocks
                $params = [
                    'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                        experimentBlock.order_number AS orderNumber',
                    "experimentDbId" => "equals $id"
                ];
                $experimentBlockData = $this->experimentBlock->searchExperimentBlocks($params);

                $experimentBlockCount = isset($experimentBlockData[0]['orderNumber']) ? count($experimentBlockData) : 0;

                if($experimentBlockCount > 0){

                    $blockData = ArrayHelper::map($experimentBlockData, 'experimentBlockDbId', 'experimentBlockDbId');
                    $listOfExptBlockIds = array_keys($blockData);

                    $this->experimentBlock->deleteMany($listOfExptBlockIds);

                    // update status of experiment
                   $this->plantingArrangement->updateExperimentStatus($id);
                }

                $this->experiment->updateOne($id, ['notes' => 'plantingArrangementChanged']);
            }
        }

        //call creation of entry data records if there's times_rep values within entry list
        $params = [
            "fields" => "entryData.id AS entryDataDbId | variable.abbrev AS variableAbbrev",
            "variableAbbrev" => "equals TIMES_REP"
        ];
        $expData = $this->experiment->searchAllEntryData($id,$params, null,false);

        if($newEntryIds != null && isset($expData) && count($expData['data']) > 0){
            $this->designModel->populateEntryData($id, initial:false, currentCount:0,newEntryIds:$newEntryIds);
        }

    }

     /**
     * Check and create first plot pos
     * @param integer $id record id of the experiment
     */
    public function checkFirstPlotPos($id){
        //Save FIRST_PLOT_POSITION_VIEW
        $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
        $variableInfo = $this->variable->searchAll([
            'fields' => 'variable.id AS variableDbId|variable.abbrev',
            'abbrev'=>'equals '.$varAbbrev
        ]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;

        $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);

        if(isset($expData) && count($expData) == 0){

            // get default first plot position in config
            $config = $this->config->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

            $varValue = (isset($config['value']) && !empty($config['value'])) ? $config['value'] : 'Top Left';

            $dataRequestBodyParam[] = [
                'variableDbId' => "$variableDbId",
                'dataValue' => "$varValue"
            ];
            $this->experiment->createExperimentData($id,$dataRequestBodyParam);
        }
    }

    /**
     * Get check information of an experiment
     * 
     * @param integer $id record id of the experiment
     * 
     */
    public function getCheckInformation($id){

        $abbrevArray = [
            'CHECK_GROUPS',
            'BEGINS_WITH_CHECK_GROUP',
            'ENDS_WITH_CHECK_GROUP',
            'CHECK_GROUP_INTERVAL'
        ];

        $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
        $expData = $this->experimentData->getExperimentData($id, ["abbrev" => $abbrevStr]);
        
        $checkInformation = [
            'check_groups' => [],
            'begins_with_check_group'=>[],
            'ends_with_check_group'=>[],
            'check_group_interval' => 0
        ];
        if(isset($expData) && count($expData) > 0){
          
            foreach($expData as $data){
                $checkInformation[strtolower($data['abbrev'])] = json_decode($data['dataValue'], true);
            }
        }
        return $checkInformation;
    }

    /**
     * Update plots with the specified check information
     * 
     * @param integer $id record id of the experiment
     * @param array $expData contains check specific information
     */
    public function updatePlotsWithChecks($id, $expData){

        $existingRec = [];
        if(isset($expData) && !empty($expData)){
            $existingRec = array_column($expData, 'abbrev');
            $checkGroups = [];
            $checkGroupInterval = NULL;
            $endsWithCheckGroup = [];
            $beginsWithCheckGroup = [];
            $checksUsed = [];
            $checkEntries = [];

            if(in_array('CHECK_GROUPS',$existingRec)){
                $varIndex = array_search('CHECK_GROUPS', array_column($expData, 'abbrev'));
                $checkGroups = json_decode($expData[$varIndex]['dataValue'], true);
                foreach($checkGroups as $checks){
                    $checksUsed = array_merge($checksUsed, $checks['members']);
                }
                
            }
            if(in_array('CHECK_GROUP_INTERVAL',$existingRec)){
                $varIndex = array_search('CHECK_GROUP_INTERVAL', array_column($expData, 'abbrev'));
                $checkGroupInterval = json_decode($expData[$varIndex]['dataValue']);
            }
            if(in_array('ENDS_WITH_CHECK_GROUP',$existingRec)){
                $varIndex = array_search('ENDS_WITH_CHECK_GROUP', array_column($expData, 'abbrev'));
                $endsWithCheckGroup = json_decode($expData[$varIndex]['dataValue']);
                $checksUsed = array_merge($checksUsed, $endsWithCheckGroup);
            }
            if(in_array('BEGINS_WITH_CHECK_GROUP',$existingRec)){
                $varIndex = array_search('BEGINS_WITH_CHECK_GROUP', array_column($expData, 'abbrev'));
                $beginsWithCheckGroup = json_decode($expData[$varIndex]['dataValue']);
                $checksUsed = array_merge($checksUsed, $beginsWithCheckGroup);
            }

            $checksUsed = array_unique($checksUsed);
            $checksUsed = array_map('intval', $checksUsed);
            
            // get maximum plot no of previous blocks
            $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                        parentExperimentBlock.id AS parentExperimentBlockDbId|parentExperimentBlock.experiment_block_name AS parentExperimentBlockName|
                        experimentBlock.entry_list_id_list AS entryListIdList|experimentBlock.order_number AS orderNumber',
                'experimentDbId' => "equals "."$id"
            ]);

            if ($experimentBlockArray['totalCount'] > 0) {
                //build plots without the checks used in check_groups
                $currentBlock = $experimentBlockArray['data'][0];
                if (!empty($currentBlock['entryListIdList'])) {
                    // $currentBlockEntries = implode(',', $currentBlock['entryListIdList']);
                    $blockEntriesCond = '';
                    $currentBlockTemp = $currentBlock['entryListIdList'];
                    $currentBlockTempLength = count($currentBlockTemp);

                    for($i = 0; $i < $currentBlockTempLength; $i++){
                        $blockEntriesCond = empty($blockEntriesCond) ? 'equals '. $currentBlockTemp[$i]['entryDbId'] : $blockEntriesCond. '|equals '. $currentBlockTemp[$i]['entryDbId'];
                    }
                    
                    $entries = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.entry_name AS entryName','entryDbId' => $blockEntriesCond]);
                }
                
                if (isset($entries['totalCount']) && $entries['totalCount'] > 0) {
                      // Get entry order
                      if($currentBlock['entryListIdList'] != null){
                        $entriesOrder = array_column($currentBlock['entryListIdList'], 'entryDbId');
                        $entriesRecord = array_fill(0, count($entries['data']), 1); // fill array with temp values to be replaced by sort
                        foreach($entries['data'] as $entry) { // sort entries from API by entryListIdList
                            $entriesRecord[array_search($entry['entryDbId'], $entriesOrder)] = $entry;
                        }
                        
                        $newArr = ArrayHelper::map($currentBlock['entryListIdList'],'entryDbId','repno');
                        if(!empty($currentBlock['parentExperimentBlockDbId'])){
                            //Block is a sublock
                            $blockNo = !empty($currentBlock['parentExperimentBlockName']) ? explode('nursery block ',strtolower($currentBlock['parentExperimentBlockName'])) : null;
                            $blockNo = !empty($blockNo) && isset($blockNo[1]) ? (int)$blockNo[1] : null;
                        }else{
                            //Block is a parent
                            $blockNo = $currentBlock['orderNumber'];
                        }

                        $batchThreshold = 500;
                        $counter = 0;
                        $spliceThreshold = 0;
                        $firstAppend = true;
                        $startPlotno = 0;
                        $totalPrevRows = 0;
                        foreach ($entriesRecord as $entry) {
                            if(!in_array($entry['entryDbId'], $checksUsed)){
                                $timesRep = $newArr[$entry['entryDbId']];                            
                                
                                //get previous entry times_rep
                                if(isset($maxEntryRepno[$entry['entryDbId']])){
                                    $maxRepNo = $maxEntryRepno[$entry['entryDbId']]+1;
                                    $maxTimes = $maxEntryRepno[$entry['entryDbId']]+$timesRep;
                                } else {
                                    $maxEntryRepno[$entry['entryDbId']] = 0;
                                    $maxRepNo = 1;
                                    $maxTimes = $timesRep;
                                }

                                for ($i = $maxRepNo; $i <= $maxTimes; $i++) {
                                    $temp = [
                                        'parent_id' => isset($entry['parentExperimentBlockDbId']) ?? null,
                                        'block_id' => intval($currentBlock['experimentBlockDbId']),
                                        'block_no' => $blockNo,
                                        'entry_id' => $entry['entryDbId'],
                                        'entno' => $entry['entryNumber'],
                                        'plotno' => $startPlotno++,
                                        'designation' => $entry['entryName'],
                                        'times_rep' => $timesRep,
                                        'repno' => $i,
                                        'prev_block_no_of_rows' => $totalPrevRows
                                    ];

                                    $layoutOrderData[] = $temp;
                                    $counter++;
                                    if($counter == $batchThreshold){
                                        $updatedData = [
                                            "layoutOrderData" => json_encode($layoutOrderData)
                                        ];

                                        if(!$firstAppend){
                                            $updatedData['appendLayout'] = true;
                                        } else $firstAppend = false;

                                        \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($currentBlock['experimentBlockDbId']), $updatedData);
                                        unset($layoutOrderData);
                                        $counter = 0;
                                        $spliceThreshold+=$batchThreshold;
                                    }
                                    $maxEntryRepno[$entry['entryDbId']] = $maxEntryRepno[$entry['entryDbId']] + 1;
                                }
                            } else {
                                $checkEntries[$entry['entryDbId']] = $entry;
                            }
                        }
                        
                        if(!empty($layoutOrderData) && count($layoutOrderData) > 0){
                            $updatedData = [
                                "layoutOrderData" => json_encode($layoutOrderData)
                            ];

                            if(!$firstAppend){
                                $updatedData['appendLayout'] = true;
                            } else $firstAppend = false;

                            $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($currentBlock['experimentBlockDbId']), $updatedData);
                            unset($layoutOrderData);
                            $layoutOrderData = null;
                        }
                        $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                                experimentBlock.entry_list_id_list AS entryListIdList|experimentBlock.layout_order_data AS layoutOrderData|
                                experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.no_of_cols AS noOfCols|
                                experimentBlock.experiment_block_name AS experimentBlockName',
                            'experimentDbId' => "equals $id"
                        ], 'sort=experimentBlockName:ASC');
                        
                        if ($experimentBlockArray['totalCount'] > 0) {
                            $blockRecords = $experimentBlockArray['data'][0];

                            if ($blockRecords['entryListIdList'] != NULL) {
                                
                                if ($blockRecords['layoutOrderData'] != NULL) {
                                    $layoutOrderDataTemp = $blockRecords['layoutOrderData'];
                                    $blockDbId = $blockRecords['experimentBlockDbId'];
                                    $layoutOrderDataTempLength = count($layoutOrderDataTemp);

                                    $newLayout = [];
                                    $startPlotno = 1;
                                    //insert checks
                                    if(!empty($beginsWithCheckGroup)){
                                        foreach($beginsWithCheckGroup as $beginCheck){
                                            
                                            $entry = $checkEntries[$beginCheck];
                                            if(!isset($checkEntries[$beginCheck]['repno'])){
                                                $checkEntries[$beginCheck]['repno'] = 1;
                                            }
                                            $temp = [
                                                'parent_id' => isset($entry['parentExperimentBlockDbId']) ?? null,
                                                'block_id' => intval($currentBlock['experimentBlockDbId']),
                                                'block_no' => $blockNo,
                                                'entry_id' => $entry['entryDbId'],
                                                'entno' => $entry['entryNumber'],
                                                'plotno' => $startPlotno++,
                                                'designation' => $entry['entryName'],
                                                'times_rep' => $checkEntries[$beginCheck]['repno'],
                                                'repno' => $checkEntries[$beginCheck]['repno']++,
                                                'prev_block_no_of_rows' => $totalPrevRows
                                            ];
                                            $newLayout[] = $temp;
                                        }
                                    }

                                    $checkIntervalCount = 0;
                                    $currentCheckGroup = 0;
                                    if(!empty($checkGroups)){
                                        for ($l = 0; $l < $layoutOrderDataTempLength; $l++) {
                                            if($checkIntervalCount < $checkGroupInterval){
                                                $temp = $layoutOrderDataTemp[$l];
                                                $temp['plotno'] = $startPlotno++;
                                                $newLayout[] = $temp;
                                                $checkIntervalCount++;
                                            } else {
                                                if($currentCheckGroup == count($checkGroups)){
                                                    $currentCheckGroup = 0;
                                                }

                                                $entriesCheck = $checkGroups[$currentCheckGroup]['members'];

                                                foreach($entriesCheck as $entryDbId){

                                                    $entry = $checkEntries[$entryDbId];
                                                    if(!isset($checkEntries[$entryDbId]['repno'])){
                                                        $checkEntries[$entryDbId]['repno'] = 1;
                                                    }
                                                    $temp = [
                                                        'parent_id' => isset($entry['parentExperimentBlockDbId']) ?? null,
                                                        'block_id' => intval($currentBlock['experimentBlockDbId']),
                                                        'block_no' => $blockNo,
                                                        'entry_id' => $entry['entryDbId'],
                                                        'entno' => $entry['entryNumber'],
                                                        'plotno' => $startPlotno++,
                                                        'designation' => $entry['entryName'],
                                                        'times_rep' => $checkEntries[$entryDbId]['repno'],
                                                        'repno' => $checkEntries[$entryDbId]['repno']++,
                                                        'prev_block_no_of_rows' => $totalPrevRows
                                                    ];
                                                    $newLayout[] = $temp;
                                                }
                                                $currentCheckGroup++;
                                                $checkIntervalCount=0;

                                                $temp = $layoutOrderDataTemp[$l];
                                                $temp['plotno'] = $startPlotno++;
                                                $newLayout[] = $temp;
                                                $checkIntervalCount++;
                                    
                                            }
                                        }
                                    } else {
                                        for ($l = 0; $l < $layoutOrderDataTempLength; $l++) {
                                            $temp = $layoutOrderDataTemp[$l];
                                            $temp['plotno'] = $startPlotno++;
                                            $newLayout[] = $temp;
                                        }
                                    }

                                    if(!empty($endsWithCheckGroup)){
                                        foreach($endsWithCheckGroup as $endCheck){
                                            
                                            $entry = $checkEntries[$endCheck];
                                            if(!isset($checkEntries[$endCheck]['repno'])){
                                                $checkEntries[$endCheck]['repno'] = 1;
                                            }
                                            $temp = [
                                                'parent_id' => isset($entry['parentExperimentBlockDbId']) ?? null,
                                                'block_id' => intval($currentBlock['experimentBlockDbId']),
                                                'block_no' => $blockNo,
                                                'entry_id' => $entry['entryDbId'],
                                                'entno' => $entry['entryNumber'],
                                                'plotno' => $startPlotno++,
                                                'designation' => $entry['entryName'],
                                                'times_rep' => $checkEntries[$endCheck]['repno'],
                                                'repno' => $checkEntries[$endCheck]['repno']++,
                                                'prev_block_no_of_rows' => $totalPrevRows
                                            ];
                                            $newLayout[] = $temp;
                                        }
                                    }
                                    
                                    $entryIdData = $blockRecords['entryListIdList'];
                                    $entryIdsArr = array_column($entryIdData, 'entryDbId');

                                    $newEntryListData = $entryIdData;
                                    foreach($checkEntries as $check){
                                        $index = array_search($check['entryDbId'], $entryIdsArr);
                                        $newEntryListData[$index]['repno'] = $check['repno'];
                                    }

                                    // Build the layout order data of a block through appending if it exceeds the threshold
                                    $needToAppend = false;
                                    $totalPlots = count($newLayout);
                                    $batchThreshold = 500;   // threshold per batch
                                    if($totalPlots > $batchThreshold) $needToAppend = true;

                                    $updatedData = [
                                        "entryListIdList" => json_encode($newEntryListData),
                                        "layoutOrderData" => json_encode(array_slice($newLayout, 0, $batchThreshold))
                                    ];

                                    $layoutTotal = intval($blockRecords['noOfRanges']) * intval($blockRecords['noOfCols']);

                                    if($layoutTotal < $totalPlots){
                                        $updatedData['noOfRanges'] = "0";
                                        $updatedData['noOfCols'] = "0";
                                    }
                                    //update layout order data of the block
                                    $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockDbId, $updatedData);

                                    if($needToAppend) {
                                        for($i = $batchThreshold; $i < $totalPlots; $i += $batchThreshold){
                                            $appendData = [
                                                "appendLayout" => true,
                                                "layoutOrderData" => json_encode(array_slice($newLayout, $i, $batchThreshold))
                                            ];
                                            
                                            $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockDbId, $appendData);
                                            unset($appendData);
                                        }
                                        $appendData = null;
                                    }

                                    $this->plantingArrangement->updateBlockCoordinates($id, $blockDbId, 'updatePlots', null, 0);
                                }
                            }
                            
                            
                        }
                    }
                }
            }
        }
    }

    /**
     * Get used checks
     * 
     * @param integer $id record id of experiment
     */
    public function getChecksUsed($id){
        $abbrevArray = [
            'CHECK_GROUPS',
            'BEGINS_WITH_CHECK_GROUP',
            'ENDS_WITH_CHECK_GROUP',
            'CHECK_GROUP_INTERVAL'
        ];
        $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
        $expData = ExperimentData::getExperimentData($id, ["abbrev" => $abbrevStr]);

        $existingRec = array_column($expData, 'abbrev');
        $checksUsed = [];
        if(in_array('CHECK_GROUPS',$existingRec)){
            $varIndex = array_search('CHECK_GROUPS', array_column($expData, 'abbrev'));
            $checkGroups = json_decode($expData[$varIndex]['dataValue'], true);
            foreach($checkGroups as $checks){
                $checksUsed = array_merge($checksUsed, $checks['members']);
            }
            
        }
        if(in_array('ENDS_WITH_CHECK_GROUP',$existingRec)){
            $varIndex = array_search('ENDS_WITH_CHECK_GROUP', array_column($expData, 'abbrev'));
            $endsWithCheckGroup = json_decode($expData[$varIndex]['dataValue']);
            $checksUsed = array_merge($checksUsed, $endsWithCheckGroup);
        }
        if(in_array('BEGINS_WITH_CHECK_GROUP',$existingRec)){
            $varIndex = array_search('BEGINS_WITH_CHECK_GROUP', array_column($expData, 'abbrev'));
            $beginsWithCheckGroup = json_decode($expData[$varIndex]['dataValue']);
            $checksUsed = array_merge($checksUsed, $beginsWithCheckGroup);
        }

        $checksUsed = array_unique($checksUsed);
        $checksUsed = array_map('intval', $checksUsed);
        
        return $checksUsed;
    }

    /**
     * Reset planting arrangement due to entry list change
     * 
     * @param integer $experimentId Experiment ID
     */
    public function resetDesign($id){

        //delete blocks
        $this->deleteBlocksUpdate($id);
        $this->experiment->updateOne($id, ['notes' => 'plantingArrangementChanged']);

         //Check if there are checks metadata
            $abbrevArray = [
                'CHECK_GROUPS',
                'BEGINS_WITH_CHECK_GROUP',
                'ENDS_WITH_CHECK_GROUP',
                'CHECK_GROUP_INTERVAL',
                'ENTRY_LIST_TIMES_REP',
                'PA_ENTRY_WORKING_LIST'
            ];
            $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
            $expData = $this->experimentData->getExperimentData($id, ["abbrev" => $abbrevStr]);

            if(isset($expData) && count($expData) > 0){
                $existingRec = array_column($expData, 'experimentDataDbId');
                $this->experimentData->deleteMany($existingRec);
            }
  
    }
}