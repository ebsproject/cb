<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for Experiment Planting Arrangement
 */

namespace app\models;

namespace app\modules\experimentCreation\models;

use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\ExperimentModel;
use app\models\BackgroundJob;
use app\models\Entry;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Occurrence;
use app\models\Variable;
use app\models\VariableSet;
use app\models\VariableSetMember;
use app\models\Plot;
use app\models\Scale;
use app\models\ScaleValue;
use app\models\Item;

use Yii;

class PlantingArrangement extends Experiment
{
    public EntryList $entryList;

    public function __construct (
        EntryList $entryList,
        Item $item,
        $config = []
    ) {
        $this->entryList = $entryList;
        parent::__construct($entryList, $item, $config);
    }

    /**
     * Retrieves nursery blocks of an experiment
     * @param $id integer experiment identifier
     * @param $sessionCheckFlag boolean flag for hard reload checking of session saved
     * @param $savingFlag boolean flag for checking of session saved
     */
    public function getNurseryBlocks($id, $sessionCheckFlag, $savingFlag)
    {
        // if not pjax reload, load what's saved in the database

        $experimentCode = Experiment::getOne($id);

        if (!$sessionCheckFlag && !$savingFlag && isset($experimentCode['experimentDesignType']) && $experimentCode['experimentDesignType'] != 'Entry Order') {

            //Insert inactive blocks to the experiment block table  
            $experimentCode = $experimentCode['data']['experimentCode'] . '-NB-1';

            //Check if the initially loaded blocks has a record in the database
            $checkParam = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.experiment_block_code AS experimentBlockCode',
                "experimentDbId" => "equals $id",
                "experimentBlockCode" => "equals $experimentCode"
            ];

            $checkData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($checkParam);

            if (isset($checkData['data']) && empty($checkData['data'])) {
                $createParams[] = [
                    "orderNumber" => "1",
                    "experimentDbId" => "$id",
                    "experimentBlockCode" => "$experimentCode",
                    "experimentBlockName" => 'Nursery block 1',
                    "noOfBlocks" => "0",
                    "blockType" => 'parent',
                    "isActive" => false
                ];
                $processedParams = [
                    'records' => $createParams
                ];

                \Yii::$container->get('app\models\ExperimentBlock')->create($processedParams);
            }
        }

        $params = ["experimentDbId" => "$id", "parentExperimentBlockDbId" => "is null"];
        $data = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);

        return new ArrayDataProvider([
            'allModels' => $data['data'],
            'key' => 'experimentBlockCode',
            'pagination' => false,
            'sort' => [
                'attributes' => ['orderNumber'],
                'defaultOrder' => ['orderNumber' => SORT_ASC]
            ]
        ]);


    }


    /**
     * Adding of new row of nursery blocks
     * @param $id integer Experiment ID
     * @param $rows integer no of rows to add
     * @param $designType flag if nursery block or entry order block
     */
    public function addNewRows($id, $rows, $designType = 'default')
    {
        $experimentCode = Experiment::getOne($id);
        $experimentCode = $experimentCode['data']['experimentCode'];

        // get max order number in temp table
        $params = [
            'fields' => 'experiment.id AS experimentDbId|parentExperimentBlock.id AS parentExperimentBlockDbId|
                experimentBlock.order_number AS orderNumber',
            "experimentDbId" => "equals $id",
            "parentExperimentBlockDbId" => "is null"
        ];
        $apiSort = '?sort=orderNumber:DESC';
        $experimentBlockData = \Yii::$container->get('app\models\ExperimentBlock')->searchExperimentBlocks($params, $apiSort);

        $nurseryBlockCount = isset($experimentBlockData[0]['orderNumber']) ? $experimentBlockData[0]['orderNumber'] : 0;

        $valuesStr = '';
        $createParams = [];

        if($designType == 'default'){ // default add blocks design
            $codeStr = '-NB-';
            $nameStr = 'Nursery block ';

            for ($i = 0; $i < $rows; $i++) {
                $row = $nurseryBlockCount + ($i + 1);
                $code = $experimentCode . $codeStr . $row;
                $name = $nameStr . $row;
                $ant = empty($valuesStr) ? '' : ', ';
                $valuesStr .= $ant . "({$row},'{$code}','{$name}',0,0,false)";

                $createParams[] = [
                    "orderNumber" => "$row",
                    "experimentDbId" => "$id",
                    "experimentBlockCode" => "$code",
                    "experimentBlockName" => $nameStr . $row,
                    "noOfBlocks" => "0",
                    "blockType" => 'parent',
                    "isActive" => true
                ];
            }
        } else if ($designType == 'entry order' || $designType == 'cross pattern'){ //Entry Order design
            $codeStr = '-EO-';
            $nameStr = 'Entry order block ';

            for ($i = 0; $i < $rows; $i++) {
                $row = $nurseryBlockCount + ($i + 1);
                $code = $experimentCode . $codeStr . $row;
                $name = $nameStr . $row;
                $ant = empty($valuesStr) ? '' : ', ';
                $valuesStr .= $ant . "({$row},'{$code}','{$name}',0,0,false)";

                $tempArr = [
                    "orderNumber" => "$row",
                    "experimentDbId" => "$id",
                    "experimentBlockCode" => "$code",
                    "experimentBlockName" => $nameStr . $row,
                    "noOfBlocks" => "0",
                    "blockType" => 'parent',
                    "isActive" => true,
                    "plotNumberingOrder" => "Column serpentine",
                    "startingCorner" => "Top Left"
                ];

                if($designType == 'cross pattern'){
                    $tempArr['notes'] = 'cross pattern';
                }

                $createParams[] = $tempArr;
            }
        }

        $processedParams = [
            'records' => $createParams
        ];

        //update experiment design type to Entry Order for cross pattern
        if($designType == 'cross pattern'){
            \Yii::$container->get('app\models\Experiment')->updateOne($id, ["experimentDesignType"=>"Entry Order"]);
        }

        \Yii::$container->get('app\models\ExperimentBlock')->create($processedParams);
    }

    /**
     * Removes a row in the    experiment block record
     * @param $id integer Experiment ID
     * @param $rowId integer block id of the record
     */
    public function removeRow($id, $rowId)
    {
        \Yii::$container->get('app\models\ExperimentBlock')->DeleteOne($rowId);
    }


    /** 
     * Saves newly added blocks
     * @param $id integer Experiment ID
     */
    public function saveBlocks($id)
    {
        // insert new nursery blocks
        // This would now be update nursery blocks as it has been previously added

        $params = [
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.experiment_block_code AS experimentBlockCode|experimentBlock.experiment_block_name AS experimentBlockName|
                experimentBlock.no_of_blocks AS noOfBlocks',
            "experimentDbId" => "equals $id"
        ];
        $experimentBlockData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);

        $experimentBlocks = $experimentBlockData['data'];

        foreach ($experimentBlocks as $value) {
            $experimentBlockDbId = $value['experimentBlockDbId'];
            $newNurseryBlocksParam = [
                "isActive" => true
            ];

            // Update the records of the nursery block
            $params2 = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId',
                "experimentDbId" => "equals $id",
                "experimentBlockDbId" => "equals $experimentBlockDbId"
            ];
            $experimentBlockData2 = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params2);
            $experimentSubBlocks = isset($experimentBlockData2['data']) ? $experimentBlockData2['data'] : null;

            if (!empty($experimentSubBlocks)) {
                \Yii::$container->get('app\models\ExperimentBlock')->updateOne($experimentBlockDbId, $newNurseryBlocksParam);
            }

            $noOfBlocks = $value['noOfBlocks'];

            //Check if the are existing sub-block records
            $parentExperimentBlockDbId = $value['experimentBlockDbId'];
            $params2 = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                    parentExperimentBlock.id AS parentExperimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
                "experimentDbId" => "equals $id",
                "parentExperimentBlockDbId" => "equals $parentExperimentBlockDbId"
            ];

            $experimentBlockData2 = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params2);
            $experimentSubBlocks = ($experimentBlockData2['totalCount'] > 0 && isset($experimentBlockData2['data'])) ? $experimentBlockData2['data'] : null;

            //Remove existing sub-blocks
            if ($experimentSubBlocks != null && empty($experimentSubBlocks[0]['layoutOrderData'])) {

                $blockData = ArrayHelper::map($experimentSubBlocks, 'experimentBlockDbId', 'experimentBlockDbId');
                $listOfExptBlockIds = array_keys($blockData);
                \Yii::$container->get('app\models\ExperimentBlock')->deleteMany($listOfExptBlockIds);
            }

            if ($noOfBlocks > 0) {
                //For insert of new sub-block records
                $experimentBlockCode = $value['experimentBlockCode'];
                $newSubBlocksParam = [];

                for ($i = 0; $i < $noOfBlocks; $i++) {
                    $newExperimentBlockCode = $experimentBlockCode . '-SB-' . ($i + 1);
                    $newExperimentBlockName = $value['experimentBlockName'] . ' Sub-block ' . ($i + 1);
                    $subBlockOrderNumber = $i + 1;

                    $params["experimentBlockCode"] = "$newExperimentBlockCode";
                    $experimentBlockData2 = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);

                    if (empty($experimentBlockData2['data'])) {
                        // insert new sub-blocks
                        $newSubBlocksParam[] = [
                            "experimentDbId" => "$id",
                            "orderNumber" => "$subBlockOrderNumber",
                            "experimentBlockCode" => "$newExperimentBlockCode",
                            "experimentBlockName" => "$newExperimentBlockName",
                            "parentExperimentBlockDbId" => "$parentExperimentBlockDbId",
                            "blockType" => "sub-block",
                            "isActive" => true,
                            "startingCorner" => "Top Left",
                            "plotNumberingOrder" => "Row order",
                            "noOfBlocks" => "0",
                            "noOfRanges" => "0",
                            "noOfCols" => "0"
                        ];
                    }
                }

                if (!empty($newSubBlocksParam)) {
                    $processedParams = [
                        'records' => $newSubBlocksParam
                    ];

                    // Create new records for the sublock
                    \Yii::$container->get('app\models\ExperimentBlock')->create($processedParams);
                }
            }
        }
    }

    /**
     * Adds sub-block
     * @param $id integer Experiment ID
     * @param $rowId integer block ID
     * @param $newVal integer user provided no of sub-blocks
     */
    public function actionAddSubBlockSingle($id, $rowId, $newVal)
    {
        if (!empty($newVal)) {
            $params = ["noOfBlocks" => "$newVal"];

            $searchParams = [
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                    experimentBlock.no_of_blocks AS noOfBlocks',
                "experimentDbId" => "equals $id",
                "experimentBlockDbId" => "equals $rowId"
            ];
            $experimentBlockData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($searchParams);


            if (!empty($experimentBlockData['data'])) {
                $prevNoOfBlocks = $experimentBlockData['data'][0]['noOfBlocks'];

                if ($prevNoOfBlocks != intval($newVal)) {
                    $updateParams = ["layoutOrderData" => "null", "entryListIdList" => "null"];

                    \Yii::$container->get('app\models\ExperimentBlock')->updateOne($rowId, $updateParams);
                }
            }
            \Yii::$container->get('app\models\ExperimentBlock')->updateOne($rowId, $params);
        }
    }

    /**
     * Retrieves the select2 tags
     * @param $attr string input field attribute
     * @param $id experiment ID
     * @param $variableAbbrev string variable abbreviation
     */
    public function getSearchTags ($attr, $id, $variableAbbrev = null)
    {
        $result = [];
        
        $entryListId = $this->entryList->getEntryListId($id);
        $data = \Yii::$container->get('app\models\Entry')->searchAll([
            'fields' => 'entry.id AS "entryDbId"|entry.entry_list_id AS "entryListDbId"|experiment.id AS "experimentDbId"|entry.entry_role AS "entryRole"|entry.description AS "description"',
            'entryListDbId' => "equals $entryListId",
            'experimentDbId'=> "equals ".$id,
            'distinctOn'=> "$attr"
        ]);
        
        $entriesData = isset($data['data']) && !empty($data['data']) ? $data['data'] : null;

        $trackArr = [];

        //attr is present in the entry data table
        if (!isset($entriesData[0][$attr]) && ($variableAbbrev != 'entryNumber' && $variableAbbrev != 'designation')) {

            $varRecord = \Yii::$container->get('app\models\Variable')->getVariableByAbbrev(strtoupper($variableAbbrev));
            $entryIdList = ArrayHelper::map($data['data'], 'entryDbId', 'entryDbId');
            $entryIdListKeys = array_keys($entryIdList);

            $varRecId = $varRecord['variableDbId'];
            $apiResourceFilter = ["variableDbId" => "equals $varRecId"];

            foreach ($entryIdListKeys as $entryId) {
                $dataRes = Yii::$app->api->getResponse('POST', 'entries/' . $entryId . '/data-search', json_encode($apiResourceFilter), true);

                if (!empty($dataRes['body']['result']['data'][0]['data'][0]['dataValue']) && !in_array($dataRes['body']['result']['data'][0]['data'][0]['dataValue'], $trackArr)) {
                    $trackArr[] = $dataRes['body']['result']['data'][0]['data'][0]['dataValue'];
                    $result[] = ["id" => $entryId, "text" => $dataRes['body']['result']['data'][0]['data'][0]['dataValue']];
                }
            }
        } else {
            foreach ($entriesData as $value) {
                if (!empty($value[$attr]) && !in_array($value[$attr], $trackArr)) {
                    $trackArr[] = $value[$attr];
                    $result[] = ["id" => $value["entryDbId"], "text" => $value[$attr]];
                }
            }
        }

        if (empty($result)) { //if no program
            $result = 0;
        }

        return $result;
    }

    /**
     * Retrieves entries based from search filters
     * @param $id integer experiment identifier
     * @param $entryListVars object Entry config
     * @param $filters array filter parameters
     * @param $urlParams string url get parameters
     * @param $entryModel string entry model
     * 
     */
    public function searchEntries($id, $entryListVars = null, $filters = null, $urlParams = null, $entryModel = null, $pageParams = null)
    {
        $vars = isset($entryListVars) ? $entryListVars : [];
        
        $filterCond = [];
        
        // if filters are set
        if (!empty($filters) && $filters != null && $filters != 'empty') {
            foreach ($filters as $key => $value) {
                $key = strtolower($key);

                if ($key == 'entrynumber') {
                    $operator = isset($value['operator']) ? $value['operator'] : '';

                    // if range
                    if ($operator == 'range' && (!empty($value['from']) || !empty($value['to']))) {
                        $fromVal = $value['from'];
                        $toVal = $value['to'];
                      
                        if(!empty($toVal)){
                           $addCond = "range: " . $fromVal . '-' . $toVal;
                           $filterCond["entryNumber"] = "$addCond";
                        }else{
                           $addCond = ''; 
                        }
                   
                    } else if ($operator == '') { // if equal
                        if (!empty($value)) {
                            $values = implode("','", $value);
                            $addCond = '';
                            foreach ($value as $val) {
                                $addCond = empty($addCond) ? "equals " . $val : $addCond . "|equals " . $val;
                            }
                            $filterCond["entryNumber"] = "$addCond";
                        }
                    } elseif ($operator == '<') {
                        if (!empty($value)) {
                            $value = $value['val'];
                            $addCond = "less than " . $value;
                            $filterCond["entryNumber"] = "$addCond";
                        }
                    } elseif ($operator == '<=') {
                        if (!empty($value)) {
                            $value = $value['val'];
                            $addCond = "less than or equal to " . $value;
                            $filterCond["entryNumber"] = "$addCond";
                        }
                    } elseif ($operator == '>') {
                        if (!empty($value)) {
                            $value = $value['val'];
                            $addCond = "greater than " . $value;
                            $filterCond["entryNumber"] = "$addCond";
                        }
                    } elseif ($operator == '>=') {
                        if (!empty($value)) {
                            $value = $value['val'];
                            $addCond = "greater than or equal to " . $value;
                            $filterCond["entryNumber"] = "$addCond";
                        }
                    }
                } elseif($key == 'notincludedentries' && !empty($value)){
                    $valueStr = implode(',', $value);
                    $addCond = 'not equals ' . str_replace(',', '|not equals ', $valueStr);
                    $filterCond['entryDbId'] = "$addCond";
                }else {

                    if (strpos($key, '_') !== false && $key != 'already_used') {
                        $key = ucwords(strtolower($key), '_');
                        $key = str_replace('_', '', $key);
                        $key = lcfirst($key);
                    } elseif ($key == 'already_used') {
                        if (!$value) {
                            $params = ["fields" => "experiment.id AS experimentDbId|experimentBlock.entry_list_id_list AS entryListIdList",
                                "experimentDbId" => "$id", "entryListIdList" => "is not null "];
                            $allBlocksData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params);
                            $blocksArr = [];
                            
                            foreach ($allBlocksData['data'] as $blocks) {
                                if (empty($blocksArr)) {
                                    $blocksArr = $blocks['entryListIdList'];
                                    $blocksArr = ArrayHelper::map($blocksArr,'entryDbId','entryDbId');
                                } else {
                                    foreach ($blocks['entryListIdList'] as $val) {
                                        if (!in_array($val['entryDbId'], $blocksArr)) {
                                            $blocksArr[] = $val['entryDbId'];
                                        }
                                    }
                                }
                            }
                            
                            $key = "entryDbId";
                            $value = $blocksArr;
                        }
                    }

                    if (!empty($value) && !is_bool($value)) {
                        
                        $addCond = '';
                        if (gettype($value) == 'array' && !empty($value)) {

                            $valueStr = implode(',', $value);
                    
                            if ($key == "entryDbId") {
                                $addCond = empty($addCond) ? 'not equals ' . str_replace(',', '|not equals ', $valueStr) : $addCond . str_replace(',', '|not equals ', $valueStr);
                            } else if(in_array($key, ['entrytype', 'entryclass'])){
                                $transform = [
                                    'entryclass' => 'entryClass',
                                    'entrytype' => 'entryType'
                                ];
                                $key = $transform[$key];
                                $addCond = empty($addCond) ? 'equals ' . str_replace(',', '|equals ', $valueStr) : $addCond . str_replace(',', '|equals ', $valueStr);
                            }
                            else {
                                $addCond = empty($addCond) ? 'equals ' . str_replace(',', '|equals ', $valueStr) : $addCond . str_replace(',', '|equals ', $valueStr);
                            }

                            $filterCond[$key] = "$addCond";
                        }
                    }
                }
            }
        }
        
        $entryListId = $this->entryList->getEntryListId($id);
        $filterCond["entryListDbId"] = "$entryListId";

        return $entryModel->search($urlParams, $vars, $filterCond, false, 'planting-arrangement',$pageParams);
   

    }

    /**
     * Retrieve blocks by experiment
     *
     * @param $id integer experiment identifier
     * @param $sort string sort parameter
     * @return $dataProvider array list of experiment blocks
     */
    public function getBlocksByExperimentId($id, $sort=null)
    {

        $searchParams = ["experimentDbId" => "$id", "noOfBlocks" => "equals 0", "isActive" => "true"];

        if(empty($sort)){
            $sort = 'sort=experimentBlockCode';
        }
        $experimentBlocksData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($searchParams, $sort);

        return new ArrayDataProvider([
            'allModels' => $experimentBlocksData['data'],
            'key' => 'experimentBlockDbId',
            'pagination' => false
        ]);


    }

    public function getBlockVariables()
    {
        $result = [];

        $variableSetParam = [
            'fields' => 'variableSet.abbrev|variableSet.usage|variableSet.id AS "variableSetDbId"',
            "abbrev" => "equals PLANTING_ARRANGEMENT",
            "usage"=>"application"
        ];
        $variableSetData = \Yii::$container->get('app\models\VariableSet')->searchAll($variableSetParam);

        //Variable set ID of planting arrangement
        $variableSetId = isset($variableSetData['data'][0]) && !empty($variableSetData['data'][0]) ? $variableSetData['data'][0]['variableSetDbId'] : 0;

        //Get variable set members
        $variableSetMembers = \Yii::$container->get('app\models\VariableSetMember')->getVariableSetMembers($variableSetId);

        $varSetMembers = ArrayHelper::map($variableSetMembers, 'abbrev', 'variableDbId');

        $varSetMemKeys = array_keys($varSetMembers);

        if (in_array('NO_OF_BLOCKS', $varSetMemKeys)) {
            unset($varSetMembers['NO_OF_BLOCKS']);
        }

        if (in_array('BLOCK_TYPE', $varSetMemKeys)) {
            unset($varSetMembers['BLOCK_TYPE']);
        }


        $result = [];
        $count = 0;
        foreach ($varSetMembers as $key => $value) {
            $scaleValStr = '';
            $result[$count] = \Yii::$container->get('app\models\Variable')->getVariableByAbbrev(strtoupper($key));
            $scaleValuesData = Scale::getVariableScale($value);
            $scaleValHolder = !empty($scaleValuesData['scaleValues']) ? ArrayHelper::map($scaleValuesData['scaleValues'], 'value', 'value') : '';

            $scaleVals = !empty($scaleValHolder) ? array_keys($scaleValHolder) : '';
            if (!empty($scaleVals)) {
                $scaleValStr = implode(',', $scaleVals);
            }

            $result[$count]['scaleValues'] = $scaleValStr;
            $count++;
        }

        return $result;
    }

    /**
     * Save block parameters
     *
     * @param $id experiment identifier
     * @param $data array list of block params
     * @param $entryIds array list of entry identifiers
     * @param $blockIds array list of blocks
     * @param $entryListIdList json entry list id list
     */
    public function saveBlockParams($id, $data, $entryIds, $blockIds, $entryListIdList, $assignBlockStatus)
    {
        $exp = Experiment::getOne($id);
        $entryIdsStr = implode(',', $entryIds);

        //Update experiment block record
        //Assign entries to blocks
        foreach ($data as $key => $value) {
            if ($key == 'NO_OF_REPS_IN_BLOCK') {
                $key = 'NO_OF_REPS';
            }

            if ($key == 'NO_OF_ROWS_IN_BLOCK') {
                $key = 'NO_OF_RANGES';
            }

            if ($key == 'NO_OF_COLS_IN_BLOCK') {
                $key = 'NO_OF_COLS';
            }

            if (strpos($key, '_') !== false) {
                $column = ucwords(strtolower($key), '_');
                $column = str_replace('_', '', $column);
                $column = lcfirst($column);
            } else {
                $column = strtolower($key);
            }


            $value = !empty($value) ? $value : 0;
            $updateParams[$column] = "$value";
        }
        $updateParams['entryListIdList'] = $entryListIdList;
        
        if(strtolower($assignBlockStatus) == 'append'){
            // Get experiment block ids
            $blockIdCond = '';
            foreach($blockIds as $idRec){
                $blockIdCond = empty($blockIdCond) ? 'equals '. $idRec : $blockIdCond . '|equals '.$idRec;
            }

            $params = [
                'fields' => 'experiment.id as experimentDbId|experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list as entryListIdList|experimentBlock.no_of_cols as noOfCols',
                'experimentDbId' => "equals ".$id,
                "experimentBlockDbId"=>"$blockIdCond"
            ];

            $filters = 'sort=experimentBlockDbId';
            $coBlocksInExperiment = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params, $filters, true);
            $coBlocksInExperiment = $coBlocksInExperiment['data'];
            $newEntryListIdList = [];

            foreach($coBlocksInExperiment as $existingBlock){ 
                if(!empty($existingBlock['entryListIdList'])){
                    if($exp['data']['experimentDesignType'] == 'Systematic Arrangement') {
                        $newEntryListIdList = is_string($existingBlock['entryListIdList'])? json_decode($existingBlock['entryListIdList'], true) : $existingBlock['entryListIdList'];
                        $entryListIdListTemp = is_string($entryListIdList)? json_decode($entryListIdList, true) : $entryListIdList;
                        foreach($entryListIdListTemp as $index => $entry) {
                            $entryDbIdColumn = array_column($newEntryListIdList, 'entryDbId');
                            if(in_array($entry['entryDbId'], $entryDbIdColumn)) {
                                // get index of entryDbId from $newEntryListIdList
                                $i = array_search($entry['entryDbId'], $entryDbIdColumn);
                                $newEntryListIdList[$i] = [
                                    'entryDbId' => $entry['entryDbId'],
                                    'repno' => $newEntryListIdList[$i]['repno'] + $entry['repno'],
                                    'replicates' => json_encode(
                                        array_merge(
                                            json_decode($newEntryListIdList[$i]['replicates'], true),
                                            json_decode($entry['replicates'], true))
                                    )
                                ];
                            } else {
                                $newEntryListIdList[] = [
                                    'entryDbId' => $entry['entryDbId'],
                                    'repno' => $entry['repno'],
                                    'replicates' => $entry['replicates']
                                ];
                            }
                        }
                    } else {
                        $newEntryListIdList = ArrayHelper::merge($existingBlock['entryListIdList'],json_decode($entryListIdList, true));
                        $newEntryListIdList = json_encode(array_values(array_unique($newEntryListIdList, SORT_REGULAR)));
                    }
                }else{
                    $newEntryListIdList = $entryListIdList;
                }
                $updateParams['entryListIdList'] = is_string($newEntryListIdList)? $newEntryListIdList : json_encode($newEntryListIdList);

                // Retain no of cols and recompute for no of rows
                $updateParams['noOfCols'] = strval($existingBlock['noOfCols']);
                $totalPlotsInBlock = $this->getTotalPlotsInBlock(is_string($newEntryListIdList) ? json_decode($newEntryListIdList, true) : $newEntryListIdList);
                $newNoOfRows = ceil($totalPlotsInBlock/$existingBlock['noOfCols']);
                $updateParams['noOfRanges'] = strval($newNoOfRows);

                \Yii::$container->get('app\models\ExperimentBlock')->updateOne($existingBlock['experimentBlockDbId'], $updateParams);
            }
        }else{
            //overwrite
            \Yii::$container->get('app\models\ExperimentBlock')->updateMany($blockIds, $updateParams);
        }
        // update entry list replicate data
        if($exp['data']['experimentDesignType'] == 'Systematic Arrangement') {
            \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($id, $entryListIdList, $blockIds, $assignBlockStatus);
        }

        //limit for no. of entries
        $entryListId = $this->entryList->getEntryListId($id);
        $entries = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.entry_list_id AS entryListDbId','entryListDbId' =>  "equals $entryListId"], 'limit=1&sort=entryListDbId:asc', false);
        if($entries['totalCount'] > 500){
            //call background process for updating experiment blocks
            $addtlParams = ["description"=>"Creating of design records","entity"=>"EXPERIMENT_BLOCK","entityDbId"=>$id,"endpointEntity"=>"EXPERIMENT_BLOCK", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'processName' => 'create-pa-records',
                'experimentDbId' => $id."",
                'blockIds' => $blockIds
            ];

            $create = Experiment::create($dataArray, true, $addtlParams);

            $experiment = Experiment::getExperiment($id);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }

            $bgStatus = Experiment::formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);
            if($bgStatus !== 'done' && !empty($bgStatus)){

                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                Experiment::updateOne($id, ["experimentStatus"=>"$newStatus"]);

                //update experiment
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The experiment block records for <strong>'.$experiment['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$id]);
            }  else {
                //update worker status; seen and reflected
                 $params = [
                    "jobIsSeen" => "true",
                    "jobRemarks" => "REFLECTED"
                ];
                \Yii::$container->get('app\models\BackgroundJob')->update(['backgroundJobDbId'=>$create['data'][0]], $params);

                // update status of experiment
                PlantingArrangement::updateExperimentStatus($id);
            }
        } else {
            // update temporary plots
            PlantingArrangement::updateTemporaryPlotsOfExperiment($id, $blockIds);

            // update status of experiment
            PlantingArrangement::updateExperimentStatus($id);
        }
    }

    /**
     * Save initial block parameters
     *
     * @param $id experiment identifier
     * @param $data array list of block params
     * @param $blockIds array list of blocks
     * @param $entryListIdList json entry list id list
     * @param $initialLayoutOrderData json layout order data of block
     * @param $assignBlockStatus string append or overwrite reps to block
     */
    public function saveInitialBlockParams($id, $data, $blockIds, $entryListIdList, $initialLayoutOrderData, $assignBlockStatus)
    {
        $exp = Experiment::getOne($id);
        $entryListIdList = is_string($entryListIdList)? json_decode($entryListIdList, true) : $entryListIdList;
        $initialLayoutOrderData = is_string($initialLayoutOrderData)? json_decode($initialLayoutOrderData, true) : $initialLayoutOrderData;

        //Update experiment block record
        //Assign entries to blocks
        foreach ($data as $key => $value) {
            if ($key == 'NO_OF_REPS_IN_BLOCK') {
                $key = 'NO_OF_REPS';
            } else if ($key == 'NO_OF_ROWS_IN_BLOCK') {
                $key = 'NO_OF_RANGES';
            } else if ($key == 'NO_OF_COLS_IN_BLOCK') {
                $key = 'NO_OF_COLS';
            }

            if (strpos($key, '_') !== false) {
                $column = ucwords(strtolower($key), '_');
                $column = str_replace('_', '', $column);
                $column = lcfirst($column);
            } else {
                $column = strtolower($key);
            }

            if($key == 'REMARKS'){
                $value = !empty($value) ? $value : '';
            } else $value = !empty($value) ? $value : 0;
            $updateParams[$column] = "$value";
        }

        // temporary: since only updating one block at a time, no need for loop
        $params = [
            'fields' => 'experiment.id as experimentDbId|experimentBlock.id as experimentBlockDbId|
                experimentBlock.entry_list_id_list as entryListIdList|experimentBlock.layout_order_data as layoutOrderData|
                parentExperimentBlock.id as parentExperimentBlockDbId|experimentBlock.order_number AS orderNumber|
                experimentBlock.no_of_cols as noOfCols',
            'experimentDbId' => "equals ".$id,
            'experimentBlockDbId'=>"".$blockIds[0]
        ];

        $existingBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params, 'sort=experimentBlockDbId', true);
        $existingBlock = $existingBlock['data'][0];
        $newEntryListIdList = [];
        $newLayoutOrderData = [];

        if(strtolower($assignBlockStatus) == 'append' && !empty($existingBlock['entryListIdList'])) {   // add initial layoutOrderData to existing
            $newEntryListIdList = is_string($existingBlock['entryListIdList'])? json_decode($existingBlock['entryListIdList'], true) : $existingBlock['entryListIdList'];
            $newLayoutOrderData = is_string($existingBlock['layoutOrderData'])? json_decode($existingBlock['layoutOrderData'], true) : $existingBlock['layoutOrderData'];
        }

        // update entryListIdList
        foreach($entryListIdList as $index => $entry) {
            $entryDbIdColumn = array_column($newEntryListIdList, 'entryDbId');
            if(in_array($entry['entryDbId'], $entryDbIdColumn)) {   // entry already exists in entryListIdList
                // get index of entryDbId from $newEntryListIdList
                $i = array_search($entry['entryDbId'], $entryDbIdColumn);
                $newEntryListIdList[$i] = [
                    'entryDbId' => $entry['entryDbId'],
                    'repno' => $newEntryListIdList[$i]['repno'] + $entry['repno'],
                    'replicates' => json_encode(
                        array_merge(
                            json_decode($newEntryListIdList[$i]['replicates'], true),
                            json_decode($entry['replicates'], true))
                    )
                ];
            } else {
                $newEntryListIdList[] = [
                    'entryDbId' => $entry['entryDbId'],
                    'repno' => $entry['repno'],
                    'replicates' => $entry['replicates']
                ];
            }
        }

        // prepare values for updating newLayoutOrderData
        $entriesOrder = array_column($newEntryListIdList, 'entryDbId');
        $paramEntryDbIdListIds = array_column($newEntryListIdList, 'entryDbId');
        $paramEntryDbIdListIds = isset($paramEntryDbIdListIds) && !empty($paramEntryDbIdListIds) ? 'equals '.implode('|equals ', $paramEntryDbIdListIds) : null;
        $entries = \Yii::$container->get('app\models\Entry')->searchAll([
            'fields'=>'entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.entry_name AS entryName',
            'entryDbId' => $paramEntryDbIdListIds
        ]);
        $entriesRecord = [];
        foreach ($entries['data'] as $entry) {
            $entryIndex = array_search($entry['entryDbId'], $entriesOrder);
            $entriesRecord[$entry['entryDbId']] = [
                'entryNumber' => $entry['entryNumber'],
                'entryName' => $entry['entryName'],
                'times_rep' => $newEntryListIdList[$entryIndex]['repno']
            ];
        }

        // get $totalNoOfPreviousPlots to preserve correct plot number when adding entries
        $params = [
            'fields' => 'experiment.id as experimentDbId|experimentBlock.layout_order_data as layoutOrderData|
            experimentBlock.order_number AS orderNumber',
            'experimentDbId' => "equals ".$id
        ];
        $allBlocks = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params, 'sort=orderNumber', true);
        $allBlocks = $allBlocks['data'];
        $totalNoOfPreviousPlots = 0;
        foreach ($allBlocks as $block) {
            if($block['orderNumber'] < $existingBlock['orderNumber']) {
                if(isset($block['layoutOrderData'])) $totalNoOfPreviousPlots += count($block['layoutOrderData']);
            }
        }

        $startPlotno = $totalNoOfPreviousPlots + count($newLayoutOrderData) + 1;

        // get $totalPrevBlockNoOfRows value
        $totalPrevBlockNoOfRows = 0;
        $experimentBlocks = $this->getArrangedExpBlocks($id);   // get experiment blocks arranged by order no of parent then sub block
        $currBlockId = $existingBlock['experimentBlockDbId'];
        foreach($experimentBlocks as $key => $expBlock) {
            if($expBlock['experimentBlockDbId'] == $currBlockId) {
                for ($i=$key-1; $i >= 0; $i--) {
                    if(!is_null($experimentBlocks[$i]['noOfRanges']) && is_int($experimentBlocks[$i]['noOfRanges'])) {
                        $totalPrevBlockNoOfRows += intval($experimentBlocks[$i]['noOfRanges']);
                    }
                }
                break;
            }
        }

        // update layoutOrderData
        foreach ($initialLayoutOrderData as $key => $plot) {
            $newLayoutOrderData[] = [
                'entry_id' => $plot['entry_id'],    // entryDbId
                'repno' => $plot['repno'],  // rep no
                'entno' => $entriesRecord[$plot['entry_id']]['entryNumber'],    // entry no
                'designation' => $entriesRecord[$plot['entry_id']]['entryName'],    // entry designation
                'times_rep' => $entriesRecord[$plot['entry_id']]['times_rep'],  // total no of rep of the entry
                'plotno' => $startPlotno++, // get total count of existing layoutOrderData
                'parent_id' => $existingBlock['parentExperimentBlockDbId'], // id of parent block
                'block_id' => $existingBlock['experimentBlockDbId'],    // block id
                'block_no' => $existingBlock['orderNumber'],    // block no
                'prev_block_no_of_rows' => $totalPrevBlockNoOfRows, // total noOfRanges of prev blocks
            ];
        }


        $updateParams['entryListIdList'] = is_string($newEntryListIdList)? $newEntryListIdList : json_encode($newEntryListIdList);

        // Retain no of cols and recompute for no of rows
        if($existingBlock['noOfCols'] > 0) {
            $updateParams['noOfCols'] = strval($existingBlock['noOfCols']);
            $totalPlotsInBlock = $this->getTotalPlotsInBlock(is_string($newEntryListIdList) ? json_decode($newEntryListIdList, true) : $newEntryListIdList);
            $newNoOfRows = ceil($totalPlotsInBlock/$existingBlock['noOfCols']);
            $updateParams['noOfRanges'] = strval($newNoOfRows);
        }

        \Yii::$container->get('app\models\ExperimentBlock')->updateOne($existingBlock['experimentBlockDbId'], $updateParams);

        // save newLayoutOrderData
        $updateParams['layoutOrderData'] = is_string($newLayoutOrderData)? $newLayoutOrderData : json_encode($newLayoutOrderData);


        $batchThreshold = 500;
        $counter = 0;
        $firstAppend = true;
        $layoutTemp = [];

        foreach ($newLayoutOrderData as $layout) {
            $layoutTemp[] = $layout;
            $counter++;
            if($counter == $batchThreshold){
                $updatedData = [
                    'layoutOrderData' => json_encode($layoutTemp)
                ];

                if(!$firstAppend){
                    $updatedData['appendLayout'] = true;

                } else $firstAppend = false;

                $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($existingBlock['experimentBlockDbId'], $updatedData);
                unset($layoutTemp);
                $counter = 0;
            }
        }

        if(!empty($layoutTemp)) {
            $updatedData = [
                'layoutOrderData' => json_encode($layoutTemp)
            ];

            if(!$firstAppend){
                $updatedData['appendLayout'] = true;

            } else $firstAppend = false;

            $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($existingBlock['experimentBlockDbId'], $updatedData);
            unset($layoutTemp);
        }

        // update entry list replicate data
        if($exp['data']['experimentDesignType'] == 'Systematic Arrangement') {
            \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($id, $entryListIdList, $blockIds, $assignBlockStatus);
        }

        // update succeeding rows
        $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll(['experimentDbId' => "$id"], 'sort=experimentBlockName:ASC');

        if ($experimentBlockArray['totalCount'] > 0) {
            $blockRecords = $experimentBlockArray['data'];
            $blockRecordDbId = array_column($blockRecords, 'experimentBlockDbId');
            $arrayIndex = array_search($blockIds[0], $blockRecordDbId);

            PlantingArrangement::updateBlockCoordinates($id, $blockIds[0], '', null,  $totalPrevBlockNoOfRows);

            for ($i = $arrayIndex + 1; $i < $experimentBlockArray['totalCount']; $i++) {
                if ($blockRecords[$i]['entryListIdList'] != NULL) {
                    if ($blockRecords[$i]['layoutOrderData'] != NULL) {
                        $layoutOrderDataTemp = $blockRecords[$i]['layoutOrderData'];
                        $blockDbId = $blockRecords[$i]['experimentBlockDbId'];
                        $layoutOrderDataTempLength = count($layoutOrderDataTemp);

                        $batchThreshold = 500;
                        $counter = 0;
                        $firstAppend = true;
                        for ($l = 0; $l < $layoutOrderDataTempLength; $l++) {
                            $layoutOrderDataTemp[$l]['plotno'] = $startPlotno++;
                            $xx = 1;
                            if(isset($layoutOrderDataTemp[$l]) && isset($layoutOrderDataTemp[$l]['entry_id']) && isset($maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']])) {
                                $xx = $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']]+1;
                            } else {
                                $xx = 1;
                            }
                            $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']] = $xx;
                            $layoutOrderDataTemp[$l]['repno'] = $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']];
                            $layoutOrderDataTemp[$l]['prev_block_no_of_rows'] = $totalPrevBlockNoOfRows;

                            $counter++;
                            if($counter == $batchThreshold){
                                $updatedData = [
                                    "layoutOrderData" => json_encode($layoutOrderDataTemp)
                                ];

                                if(!$firstAppend){
                                    $updatedData['appendLayout'] = true;
                                } else $firstAppend = false;

                                $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($blockDbId), $updatedData);
                                unset($layoutOrderDataTemp);
                                $counter = 0;
                            }
                        }
                        if(!empty($layoutOrderDataTemp) && !empty($layoutOrderDataTemp)){
                            $updatedData = [
                                "layoutOrderData" =>  json_encode($layoutOrderDataTemp)
                            ];

                            if(!$firstAppend){
                                $updatedData['appendLayout'] = true;
                            } else $firstAppend = false;

                            $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($blockDbId), $updatedData);
                            unset($layoutOrderDataTemp);
                            $layoutOrderDataTemp = null;
                        }
                        PlantingArrangement::updateBlockCoordinates($id, $blockDbId, '', null, $totalPrevBlockNoOfRows);

                        $totalPrevBlockNoOfRows = intval($blockRecords[$i]['noOfRanges']);
                    }
                }
            }
        }

        // update status of experiment
        PlantingArrangement::updateExperimentStatus($id);
    }

    /**
     * Update experiment status
     *
     * @param $id integer experiment identifier
     */
    public function updateExperimentStatus($id, $isParentBlock = null)
    {

        // check first if experiment has experiment blocks
        $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.experiment_block_name AS experimentBlockName|
                parentExperimentBlock.id AS parentExperimentBlockDbId|experimentBlock.no_of_blocks AS noOfBlocks|
                experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.no_of_cols AS noOfCols',
            'experimentDbId' => "equals $id"
        ], 'sort=experimentBlockName:ASC');

        $experiment = Experiment::searchAll([
            'fields' => 'experiment.id AS experimentDbId|experiment.experiment_design_type AS experimentDesignType',
            'experimentDbId' => "equals $id"
        ]);
        $experimentDesignType = $experiment['data'][0]['experimentDesignType'];

        // if has experiment blocks
        if ($experimentBlockArray['totalCount'] > 0) {

            // get total entry count of experiment
            $entryCount = PlantingArrangement::getTotalEntryCount($id);

            // get total entry count assigned to block
            $entryCountAssignedToBlock = PlantingArrangement::getTotalEntryCountAssignedToBlock($id);

            // check if there are blocks that have no assigned rows and columns
            $allBlocksAreComplete = true;
            foreach ($experimentBlockArray['data'] as $block) {
                if ((isset($block['parentExperimentBlockDbId']) && $block['parentExperimentBlockDbId'] != NULL) || ((!isset($block['parentExperimentBlockDbId']) || is_null($block['parentExperimentBlockDbId'])) && $block['noOfBlocks'] == 0)) {
                    if (($block['noOfRanges'] == 0 || $block['noOfCols'] == 0) && $experimentDesignType != 'Entry Order') {
                        $allBlocksAreComplete = false;
                        break;
                    }
                }
            }

            // if all are completed
            if ($entryCount == $entryCountAssignedToBlock && $allBlocksAreComplete) {
                // add design generated to status
                PlantingArrangement::addDesignGeneratedStatus($id);
            } else { // if incomplete information
                // remove design generated to status
                PlantingArrangement::removeDesignGeneratedStatus($id);
            }
        }
    }

    /**
     * Remove design generated sin experiment status
     *
     * @param $id integer experiment identifier
     */
    public function removeDesignGeneratedStatus($id)
    {
        $model = Experiment::getExperiment($id);

        $status = explode(';', $model['experimentStatus']);

        // already in status, remove in status
        if (in_array('design generated', $status)) {
            $index = array_search('design generated', $status);

            if (isset($status[$index])) {
                unset($status[$index]);

                $updatedStatus = implode(';', $status);

                PlantingArrangement::saveExperimentStatus($id, $updatedStatus);
                Experiment::updateOne($id, ['notes' => 'plantingArrangementChanged']);
            }
        }
    }

    /**
     * Add design generated sin experiment status
     *
     * @param $id integer experiment identifier
     */
    public function addDesignGeneratedStatus($id)
    {
        $model = Experiment::getExperiment($id);

        $status = explode(';', $model['experimentStatus']);

        // if not in array, add in status
        if (!in_array('design generated', $status)) {
            $updatedStatus = implode(';', $status) . ';design generated';

            PlantingArrangement::saveExperimentStatus($id, $updatedStatus);
        }

        // delete existing studies created if any, to be refactored
        // ExperimentModel::deleteExistingStudies($id);
    }

    /**
     * Add design generated sin experiment status
     *
     * @param $id integer experiment identifier
     * @param $status text experiment status
     */
    public function saveExperimentStatus($id, $status)
    {
        Experiment::updateOne($id, ["experimentStatus" => "$status"]);
    }

    /**
     * Generate temporary plots for the experiment
     *
     * @param $id integer experiment identifier
     * @param $blockIds array list of block identifiers
     */
    public function updateTemporaryPlotsOfExperiment($id, $blockIds, $deleteBlock = false, $resetBlockLayout = true)
    {
        // get first block
        $firstBlockId = isset($blockIds[0]) ? $blockIds[0] : 0;

        if (!empty($blockIds)) {

            foreach ($blockIds as $firstBlockId) {
                // check if temp table exists
                $currentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                    'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                        parentExperimentBlock.id AS parentExperimentBlockDbId|parentExperimentBlock.experiment_block_name AS parentExperimentBlockName|
                        experimentBlock.entry_list_id_list AS entryListIdList|experimentBlock.order_number AS orderNumber|
                        experimentBlock.no_of_ranges AS noOfRanges',
                    'experimentBlockDbId' => "equals "."$firstBlockId"
                ]);

                $existingLayoutOrderData = [];
                if(!$resetBlockLayout) {
                    $existingLayoutOrderData = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                        'fields' => 'experimentBlock.id AS experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
                        'experimentBlockDbId' => "equals "."$firstBlockId"
                    ]);
                }

                $currentBlock = [];
                if ($currentBlockArray['totalCount'] > 0) {
                    $currentBlock = $currentBlockArray['data'][0];
                }

                $maxPlotNo = 0;

                $parentId = !empty($currentBlock['parentExperimentBlockDbId']) ? $currentBlock['parentExperimentBlockDbId'] : null;

                // if nursery block, remove only succeeding nursery blocks
                if (empty($parentId)) {

                    if ($currentBlockArray['totalCount'] > 0) {
                        $currentBlockData = $currentBlockArray['data'];
                        $allIds = array_column($currentBlockData, 'experimentBlockDbId');
                        //update all created plots
                        $updatedData = [
                            "layoutOrderData" => "[]"
                        ];

                        \Yii::$container->get('app\models\ExperimentBlock')->updateMany($allIds, $updatedData);
                    }
                } else {
                    //update all created plots
                    $updatedData = [
                        "layoutOrderData" => "[]"
                    ];
                    $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($firstBlockId, $updatedData);
                }
                unset($currentBlockArray);


                // get maximum plot no of previous blocks
                $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                    'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                        experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.entry_list_id_list AS entryListIdList|
                        experimentBlock.no_of_ranges AS noOfRanges',
                    'experimentDbId' => "equals "."$id"
                ], 'sort=experimentBlockName:ASC');

                $totalPlotsBeforeBlock = 0;
                $totalPrevRows = 0;
                $maxEntryRepno = [];
               
                if ($experimentBlockArray['totalCount'] > 0) {
    
                    $blockRecordDbId = array_column($experimentBlockArray['data'], 'experimentBlockDbId');
                    $blockRecords = $experimentBlockArray['data'];
                    $arrayIndex = array_search($firstBlockId, $blockRecordDbId);

                    for ($i = 0; $i < $arrayIndex; $i++) {
                        if ($blockRecords[$i]['entryListIdList'] != NULL) {
                          
                            $entryListIdListTemp = $blockRecords[$i]['entryListIdList'];
                            $entryListIdListTempLength = count($entryListIdListTemp);

                            for($j = 0; $j < $entryListIdListTempLength; $j++){
                                $totalPlotsBeforeBlock = $totalPlotsBeforeBlock + $entryListIdListTemp[$j]['repno']; 
                                $maxEntryRepno[$entryListIdListTemp[$j]['entryDbId']] = isset($maxEntryRepno[$entryListIdListTemp[$j]['entryDbId']]) ? $maxEntryRepno[$entryListIdListTemp[$j]['entryDbId']]+$entryListIdListTemp[$j]['repno'] : $entryListIdListTemp[$j]['repno'];
                            }
                            
                        }
                        if ($blockRecords[$i]['noOfRanges'] != NULL) {
                            $totalPrevRows += intval($blockRecords[$i]['noOfRanges']);
                        }
                    }
                    unset($blockRecords);
                    $maxPlotNo = $totalPlotsBeforeBlock;
                }
                unset($experimentBlockArray);

                $startPlotno = $maxPlotNo + 1;
                
                if (!$deleteBlock) {
                    //Create layout order data
                    $totalPlots = 0;
                    if ($currentBlock['entryListIdList'] != NULL) {
                        $currEntryListIdListTemp = $currentBlock['entryListIdList'];
                        $currEntryListIdListTempLength = count($currEntryListIdListTemp);

                        for($j = 0; $j < $currEntryListIdListTempLength; $j++){
                            $totalPlots = $totalPlots + $currEntryListIdListTemp[$j]['repno'];     
                        }
                    }

                    if (!empty($currentBlock['entryListIdList'])) {
                        $blockEntriesCond = '';
                        $currentBlockTemp = $currentBlock['entryListIdList'];
                        
                        $entryDbIdListIds = array_column($currentBlockTemp, 'entryDbId');
                        
                        $blockEntriesCond = isset($entryDbIdListIds) && !empty($entryDbIdListIds) ? 'equals '.implode('|equals ', $entryDbIdListIds) : null;
                        $entries = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.entry_name AS entryName','entryDbId' => $blockEntriesCond]);
                    }

                    $layoutOrderData = [];

                    if($resetBlockLayout) {
                        if (isset($entries['totalCount']) && $entries['totalCount'] > 0) {
                        
                            // Get entry order
                            if($currentBlock['entryListIdList'] != null){
                                $entriesOrder = array_column($currentBlock['entryListIdList'], 'entryDbId');
                                $entriesRecord = array_fill(0, count($entries['data']), 1); // fill array with temp values to be replaced by sort
                                foreach($entries['data'] as $entry) { // sort entries from API by entryListIdList
                                    $entriesRecord[array_search($entry['entryDbId'], $entriesOrder)] = $entry;
                                }
                                
                                $newArr = ArrayHelper::map($currentBlock['entryListIdList'],'entryDbId','repno');
                                if(!empty($currentBlock['parentExperimentBlockDbId'])){
                                    //Block is a sublock
                                    $blockNo = !empty($currentBlock['parentExperimentBlockName']) ? explode('nursery block ',strtolower($currentBlock['parentExperimentBlockName'])) : null;
                                    $blockNo = !empty($blockNo) && isset($blockNo[1]) ? (int)$blockNo[1] : null;
                                }else{
                                    //Block is a parent
                                    $blockNo = $currentBlock['orderNumber'];
                                }
                                
                                $batchThreshold = 500;
                                $counter = 0;
                                $spliceThreshold = 0;
                                $firstAppend = true;
                                foreach ($entriesRecord as $entry) {
                                    $timesRep = $newArr[$entry['entryDbId']];                            
                                    
                                    //get previous entry times_rep
                                    if(isset($maxEntryRepno[$entry['entryDbId']])){
                                        $maxRepNo = $maxEntryRepno[$entry['entryDbId']]+1;
                                        $maxTimes = $maxEntryRepno[$entry['entryDbId']]+$timesRep;
                                    } else {
                                        $maxEntryRepno[$entry['entryDbId']] = 0;
                                        $maxRepNo = 1;
                                        $maxTimes = $timesRep;
                                    }
        
                                    for ($i = $maxRepNo; $i <= $maxTimes; $i++) {
                                        $temp = [
                                            'parent_id' => $parentId,
                                            'block_id' => intval($firstBlockId),
                                            'block_no' => $blockNo,
                                            'entry_id' => $entry['entryDbId'],
                                            'entno' => $entry['entryNumber'],
                                            'plotno' => $startPlotno++,
                                            'designation' => $entry['entryName'],
                                            'times_rep' => $timesRep,
                                            'repno' => $i,
                                            'prev_block_no_of_rows' => $totalPrevRows
                                        ];
        
                                        $layoutOrderData[] = $temp;
                                        $counter++;
                                        if($counter == $batchThreshold){
                                            $updatedData = [
                                                "layoutOrderData" =>  json_encode($layoutOrderData)
                                            ];

                                            if(!$firstAppend){
                                                $updatedData['appendLayout'] = true;
                                                
                                            } else $firstAppend = false;
                                            
                                            $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($firstBlockId), $updatedData);
                                            unset($layoutOrderData);
                                            $counter = 0;
                                            $spliceThreshold+=$batchThreshold;
                                        }
                                        $maxEntryRepno[$entry['entryDbId']] = $maxEntryRepno[$entry['entryDbId']] + 1;
                                    }
        
                                }
                            }
                        }
                    } else {    // retain layoutOrderData
                        $layoutOrderData = $existingLayoutOrderData['data'][0]['layoutOrderData'];
                        $firstAppend = true;
                        $startPlotno += count($layoutOrderData);
                    }

                    $afterPrevRows = intval($currentBlock['noOfRanges']) + $totalPrevRows;
                    //update layout order data of the block
                    if(!empty($layoutOrderData) && !empty($layoutOrderData)){
                        $updatedData = [
                            "layoutOrderData" => json_encode($layoutOrderData)
                        ];
                        
                        if(!$firstAppend){
                            $updatedData['appendLayout'] = true;
                        } else $firstAppend = false;
                        
                        $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($firstBlockId), $updatedData);
                        unset($layoutOrderData);
                        $layoutOrderData = null;
                    }
                }

                $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                    'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                        experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.entry_list_id_list AS entryListIdList|
                        experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.no_of_ranges AS noOfRanges',
                    'experimentDbId' => "equals $id"
                ], 'sort=experimentBlockName:ASC');

                if ($experimentBlockArray['totalCount'] > 0) {
                    $blockRecords = $experimentBlockArray['data'];
                    $blockRecordDbId = array_column($blockRecords, 'experimentBlockDbId');
                    $arrayIndex = array_search($firstBlockId, $blockRecordDbId);

                    if (!$deleteBlock) {
                        PlantingArrangement::updateBlockCoordinates($id, $firstBlockId, '', null,  $totalPrevRows);
                    }

                    for ($i = $arrayIndex + 1; $i < $experimentBlockArray['totalCount']; $i++) {

                        if ($blockRecords[$i]['entryListIdList'] != NULL) {
                            
                            if ($blockRecords[$i]['layoutOrderData'] != NULL) {
                                $layoutOrderDataTemp = $blockRecords[$i]['layoutOrderData'];
                                $blockDbId = $blockRecords[$i]['experimentBlockDbId'];
                                $layoutOrderDataTempLength = count($layoutOrderDataTemp);

                                $batchThreshold = 500;
                                $counter = 0;
                                $spliceThreshold = 0;
                                $firstAppend = true;
                                for ($l = 0; $l < $layoutOrderDataTempLength; $l++) {
                                    $layoutOrderDataTemp[$l]['plotno'] = $startPlotno++;
                                    $xx = 1;
                                    if(isset($layoutOrderDataTemp[$l]) && isset($layoutOrderDataTemp[$l]['entry_id']) && isset($maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']])) {
                                        $xx = $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']]+1;
                                    } else {
                                        $xx = 1;
                                    }
                                    $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']] = $xx;
                                    $layoutOrderDataTemp[$l]['repno'] = $maxEntryRepno[$layoutOrderDataTemp[$l]['entry_id']];
                                    $layoutOrderDataTemp[$l]['prev_block_no_of_rows'] = $totalPrevRows;

                                    $counter++;
                                    if($counter == $batchThreshold){
                                        $updatedData = [
                                            "layoutOrderData" => json_encode($layoutOrderDataTemp)
                                        ];
                                        
                                        if(!$firstAppend){
                                            $updatedData['appendLayout'] = true;
                                        } else $firstAppend = false;

                                        $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($blockDbId), $updatedData);
                                        unset($layoutOrderDataTemp);
                                        $counter = 0;
                                    }
                                }
                                if(!empty($layoutOrderDataTemp) && !empty($layoutOrderDataTemp)){
                                    $updatedData = [
                                        "layoutOrderData" =>  json_encode($layoutOrderDataTemp)
                                    ];

                                    if(!$firstAppend){
                                        $updatedData['appendLayout'] = true;
                                    } else $firstAppend = false;
                                    
                                    $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne(intval($blockDbId), $updatedData);
                                    unset($layoutOrderDataTemp);
                                    $layoutOrderDataTemp = null;
                                }
                                PlantingArrangement::updateBlockCoordinates($id, $blockDbId, '', null, $totalPrevRows);

                                $totalPrevRows = intval($blockRecords[$i]['noOfRanges']);
                            }
                        }
                    }
                    
                }
            }
        }
    }

    /**
     * Get block layout data
     *
     * @param $id integer block identifier
     */
    public function getBlockLayout($id)
    {

        $result = [
            'no_of_rows_in_block' => null,
            'no_of_cols_in_block' => null,
            'notes' => null
        ];
        $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.no_of_cols AS noOfCols|
                experimentBlock.notes',
            'experimentBlockDbId' => "equals $id"
        ]);

        if ($blockInformation['totalCount'] > 0) {
            $result = [
                'no_of_rows_in_block' => $blockInformation['data'][0]['noOfRanges'],
                'no_of_cols_in_block' => $blockInformation['data'][0]['noOfCols'],
                'notes' => $blockInformation['data'][0]['notes']
            ];
        }

        return $result;
    }

    /**
     * Get temp layout of block
     */
    public function getTempLayoutByBlock($id, $blockId, $values)
    {
        return PlantingArrangement::updateBlockCoordinates($id, $blockId, null, $values);

    }

    public function getStartingPlotNo($id, $blockId)
    {
        $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.layout_order_data AS layoutOrderData',
            'experimentBlockDbId' => "equals $blockId"
        ]);
        if ($blockInformation['totalCount'] > 0) {
            $record = $blockInformation['data'][0]['layoutOrderData'];
            if ($record != NULL) {
                $result = $record[0];
            }
        }

        return (isset($result)) ? $result['plotno'] : 1;
    }

    public function generateDummyPlots($id, $blockId, $plotCount, $sorting)
    {
        //get starting plot no
        $startingPlotNo = PlantingArrangement::getStartingPlotNo($id, $blockId);

        //generate integer values
        $endVal =  ($plotCount + $startingPlotNo) - 1;
        $plotRange = range($startingPlotNo, $endVal, 1);
        
        if($sorting == "desc"){
            rsort($plotRange);
        }

        $results = [];
        for($i=0; $i<count($plotRange); $i++){
            $results[] = [
              'plotno' => $plotRange[$i]
            ];
        }
        return $results;
    }

    /**
     * Gets blocks in experiment and arranges them according to order number and sub blocks
     *
     * @param $experimentId integer experiment identifier
     */
    public function getArrangedExpBlocks($experimentId) {
        $experimentBlocksArranged = [];

        // get parent blocks
        $parentBlocks = [
            'fields' => 'experimentBlock.id AS experimentBlockDbId|experiment.id AS experimentDbId|
                experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.order_number AS orderNumber|
                experimentBlock.block_type AS blockType|parentExperimentBlock.id AS parentExperimentBlockDbId',
            'blockType' => 'parent',
            'experimentDbId' => "$experimentId"
        ];
        $parentBlocks = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($parentBlocks, 'sort=orderNumber:ASC');
        $parentBlocks = $parentBlocks['data'];

        // get sub-blocks
        $subBlocks = [
            'fields' => 'experimentBlock.id AS experimentBlockDbId|experiment.id AS experimentDbId|
                experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.order_number AS orderNumber|
                experimentBlock.block_type AS blockType|parentExperimentBlock.id AS parentExperimentBlockDbId',
            'blockType' => 'sub-block',
            'experimentDbId' => "$experimentId"
        ];
        $subBlocks = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($subBlocks, 'sort=orderNumber:ASC');
        $subBlocks = $subBlocks['data'];

        foreach ($parentBlocks as $parentKey => $parentBlock) {
            // add first to arranged array
            $experimentBlocksArranged[] = $parentBlock;
            foreach ($subBlocks as $subKey => $subBlock) {
                // check if it has subblock then add subblocks to arranged array
                if($subBlock['parentExperimentBlockDbId'] == $parentBlock['experimentBlockDbId']) {
                    $experimentBlocksArranged[] = $subBlock;
                }
            }
        }

        return $experimentBlocksArranged;
    }

    /**
     * Update coordinates of block
     *
     * @param $id integer experiment identifier
     * @param $blockId integer block identifier
     * @param $checkFlag text flag for specifying checks
     * @param $values array block parameters
     * @param $prevBlockNoOfRows integer no of columns of previous block
     * @param $toUpdate boolean flag for updating
     */
    public function updateBlockCoordinates($id, $blockId, $checkFlag='', $values = null, $prevBlockNoOfRows = 0, $toUpdate = false)
    {
        $experiment = Experiment::getExperiment($id);
        $isPreview = empty($values) ? false : true;
        //Get all the checks settings 
        $abbrevArray = [
            'CHECK_GROUPS',
            'BEGINS_WITH_CHECK_GROUP',
            'ENDS_WITH_CHECK_GROUP',
            'CHECK_GROUP_INTERVAL'
        ];
        $abbrevStr = 'equals '.implode('|equals ', $abbrevArray);
        $expData = ExperimentData::getExperimentData($id, ["abbrev" => $abbrevStr]);

        if(!$isPreview && $checkFlag == '' && isset($expData) && count($expData) > 0 && $experiment['experimentType'] == 'Observation'){
            \Yii::$container->get('app\modules\experimentCreation\models\EntryOrderModel')->updatePlotsWithChecks($id, $expData);
        } else {
            // get block parameters
            $isPreview = empty($values) ? false : true;

            $experimentBlocks = $this->getArrangedExpBlocks($id);

            $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                    experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.starting_corner AS startingCorner|
                    experimentBlock.no_of_ranges AS noOfRanges|experimentBlock.no_of_cols AS noOfCols|
                    experimentBlock.plot_numbering_order AS plotNumberingOrder',
                'experimentBlockDbId' => "equals $blockId"
            ]);

            // get $prevBlockNoOfRows value
            $currBlockId = $blockInformation['data'][0]['experimentBlockDbId'];
            foreach($experimentBlocks as $key => $expBlock) {
                if($expBlock['experimentBlockDbId'] == $currBlockId) {
                    $prevBlockNoOfRows = 0;
                    for ($i=$key-1; $i >= 0; $i--) {
                        if(!is_null($experimentBlocks[$i]['noOfRanges']) && is_int($experimentBlocks[$i]['noOfRanges'])) {
                            $prevBlockNoOfRows += intval($experimentBlocks[$i]['noOfRanges']);
                        }
                    }
                    break;
                }
            }

            // if preview layout
            if ($isPreview) {
                $result[] = $values;
                $noOfEntries = $result[0]['no_of_entries_in_block'];
                $plotCount = $result[0]['total_no_of_plots_in_block'];
            } else {
                if ($blockInformation['totalCount'] > 0) {
                    $result[] = [
                        'no_of_rows_in_block' => $blockInformation['data'][0]['noOfRanges'],
                        'no_of_cols_in_block' => $blockInformation['data'][0]['noOfCols'],
                        'starting_corner' => $blockInformation['data'][0]['startingCorner'],
                        'plot_numbering_order' => $blockInformation['data'][0]['plotNumberingOrder']
                    ];
                }
            }

            $rows = intval($result[0]['no_of_rows_in_block']);
            $cols = intval($result[0]['no_of_cols_in_block']);
            $order = $result[0]['plot_numbering_order'];
            $starting = $result[0]['starting_corner'];

            $orderCond = 'asc';
            if (in_array($starting, ['Top Right', 'Bottom Right']) && $order == 'Column serpentine') {
                $orderCond = 'desc';
            }
            if (in_array($starting, ['Bottom Left', 'Bottom Right']) && (in_array($order, ['Row serpentine', 'Row order']))) {
                $orderCond = 'desc';
            }
            if (in_array($starting, ['Bottom Right', 'Top Right']) && $order == 'Column order') {
                $orderCond = 'desc';
            }

            if (!$isPreview) {
                // retrieve plots for the block
                $plots = PlantingArrangement::getPlotsByBlock($blockId, '', $orderCond);
            } else {
                // generate dummy plots for preview
                $plots = PlantingArrangement::generateDummyPlots($id, $blockId, $plotCount, $orderCond);
            }

            $origPlotCount = !empty($plots) ? count($plots) : 0;

            $dummyPlotArr = [];
            // if needed to create dummy plots
            if ((in_array($starting, ['Bottom Right', 'Bottom Left']) && in_array($order, ['Row order', 'Row serpentine'])) || (in_array($starting, ['Top Right', 'Bottom Right']) && in_array($order, ['Column order', 'Column serpentine']))) {
                $totalCells = ($rows * $cols);
                $additionalCells = ($totalCells - $origPlotCount);

                // add dummy plots
                for ($i = 0; $i < $additionalCells; $i++) {
                    $dummyPlotArr[] = [
                        'plotno' => null
                    ];
                }

                $plotCount = $origPlotCount + $additionalCells;
            } else {
                $plotCount = $origPlotCount;
            }

            if (empty($plots)) {
                $plots = $dummyPlotArr;
            } else {
                $plots = array_merge($dummyPlotArr, $plots);
            }

            
            $plotIndexes = [];
            if ($blockInformation['data'][0]['layoutOrderData'] != NULL) {
                $layoutOrderData = $blockInformation['data'][0]['layoutOrderData'];
                $plotIndexes = array_column($layoutOrderData, 'plotno');
            }
            $matrixArr = [];
            $dataArr = [];

            if ($order == 'Column serpentine') {
                $plots = in_array($starting, ['Top Right', 'Bottom Right'])? array_reverse($plots): $plots; // Reverse because of dummy plots
                for ($i = 1, $iterator = 0; $i <= $cols; $i++) {
                    if (!($i & 1)) {    // even
                        for ($j = $rows; $j >= 1 && $iterator < $plotCount; $j--, $iterator++) {
                            $plotno = $plots[$iterator]["plotno"];

                            if (($i <= 10 && $j <= 10)) {
                                $value = (($i == 10 && $i != $cols) || ($j == 10 && $j != $rows)) ? '...' : $plotno;

                                $matrixArr = [
                                    'x' => intval($i),
                                    'y' => intval($j),
                                    'value' => $value
                                ];

                                $dataArr[] = $matrixArr;
                            }

                            if (!$isPreview && !empty($plotno)) {
                                if (!empty($layoutOrderData)) {
                                    $currentPlotIdx = array_search($plotno, $plotIndexes);
                                    $layoutOrderData[$currentPlotIdx]['field_x'] = $i;
                                    $layoutOrderData[$currentPlotIdx]['field_y'] = $j;
                                    $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                    $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                                }
                            }
                        }
                    } else {    // odd
                        for ($j = 1; $j <= $rows && $iterator < $plotCount; $j++, $iterator++) {
                            $plotno = $plots[$iterator]["plotno"];
                            $matrixArray[$j][$i] = $plots[$iterator]["plotno"];

                            if (($i <= 10 && $j <= 10)) {
                                $value = (($i == 10 && $i != $cols) || ($j == 10 && $j != $rows)) ? '...' : $plotno;

                                $matrixArr = [
                                    'x' => intval($i),
                                    'y' => intval($j),
                                    'value' => $value
                                ];

                                $dataArr[] = $matrixArr;
                            }

                            if (!$isPreview && !empty($plotno)) {
                                if (!empty($layoutOrderData)) {
                                    $currentPlotIdx = array_search($plotno, $plotIndexes);
                                    $layoutOrderData[$currentPlotIdx]['field_x'] = $i;
                                    $layoutOrderData[$currentPlotIdx]['field_y'] = $j;
                                    $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                    $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                                }
                            }
                        }
                    }
                }
            } else if ($order == 'Row serpentine') {
                $plots = in_array($starting, ['Bottom Left', 'Bottom Right'])? array_reverse($plots): $plots;   // Reverse because of dummy plots
                for ($i = 1, $iterator = 0; $i <= $rows; $i++) {
                    if (!($i & 1)) {    // even
                        for ($j = $cols; $j >= 1 && $iterator < $plotCount; $j--, $iterator++) {
                            $plotno = $plots[$iterator]["plotno"];

                            if (($i <= 10 && $j <= 10)) {
                                $value = (($i == 10 && $i != $rows) || ($j == 10 && $j != $cols)) ? '...' : $plotno;

                                $matrixArr = [
                                    'x' => intval($j),
                                    'y' => intval($i),
                                    'value' => $value
                                ];

                                $dataArr[] = $matrixArr;
                            }

                            if (!$isPreview && !empty($plotno)) {
                                if (!empty($layoutOrderData)) {
                                    $currentPlotIdx = array_search($plotno, $plotIndexes);
                                    $layoutOrderData[$currentPlotIdx]['field_x'] = $j;
                                    $layoutOrderData[$currentPlotIdx]['field_y'] = $i;
                                    $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                    $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                                }
                            }
                        }
                    } else {    // odd
                        for ($j = 1; $j <= $cols && $iterator < $plotCount; $j++, $iterator++) {
                            $plotno = $plots[$iterator]["plotno"];

                            if (($i <= 10 && $j <= 10)) {
                                $value = (($i == 10 && $i != $rows) || ($j == 10 && $j != $cols)) ? '...' : $plotno;

                                $matrixArr = [
                                    'x' => intval($j),
                                    'y' => intval($i),
                                    'value' => $value
                                ];
                                $dataArr[] = $matrixArr;
                            }

                            if (!$isPreview && !empty($plotno)) {
                                if (!empty($layoutOrderData)) {
                                    $currentPlotIdx = array_search($plotno, $plotIndexes);
                                    $layoutOrderData[$currentPlotIdx]['field_x'] = $j;
                                    $layoutOrderData[$currentPlotIdx]['field_y'] = $i;
                                    $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                    $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                                }
                            }
                        }
                    }
                }
            } else if ($order == 'Row order') {
                $plots = in_array($starting, ['Bottom Left', 'Bottom Right'])? array_reverse($plots): $plots;   // Reverse because of dummy plots
                for ($i = 1, $iterator = 0; $i <= $rows; $i++) {
                    for ($j = 1; $j <= $cols && $iterator < $plotCount; $j++, $iterator++) {
                        $plotno = $plots[$iterator]["plotno"];

                        if (($i <= 10 && $j <= 10)) {
                            $value = (($i == 10 && $i != $rows) || ($j == 10 && $j != $cols)) ? '...' : $plotno;

                            $matrixArr = [
                                'x' => intval($j),
                                'y' => intval($i),
                                'value' => $value
                            ];

                            $dataArr[] = $matrixArr;
                        }

                        if (!$isPreview && !empty($plotno)) {
                            if (!empty($layoutOrderData)) {
                                $currentPlotIdx = array_search($plotno, $plotIndexes);
                                $layoutOrderData[$currentPlotIdx]['field_x'] = $j;
                                $layoutOrderData[$currentPlotIdx]['field_y'] = $i;
                                $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                            }
                        }
                    }
                }
            } else if ($order == 'Column order') {
                $plots = in_array($starting, ['Top Right', 'Bottom Right'])? array_reverse($plots): $plots; // Reverse because of dummy plots
                for ($i = 1, $iterator = 0; $i <= $cols; $i++) {
                    for ($j = 1; $j <= $rows && $iterator < $plotCount; $j++, $iterator++) {
                        $plotno = $plots[$iterator]["plotno"];

                        if (($i <= 10 && $j <= 10)) {
                            $value = (($i == 10 && $i != $cols) || ($j == 10 && $j != $rows)) ? '...' : $plotno;

                            $matrixArr = [
                                'x' => intval($i),
                                'y' => intval($j),
                                'value' => $value
                            ];

                            $dataArr[] = $matrixArr;
                        }

                        if (!$isPreview && !empty($plotno)) {
                            if (!empty($layoutOrderData)) {
                                $currentPlotIdx = array_search($plotno, $plotIndexes);
                                $layoutOrderData[$currentPlotIdx]['field_x'] = $i;
                                $layoutOrderData[$currentPlotIdx]['field_y'] = $j;
                                $layoutOrderData[$currentPlotIdx]['actual_field_y'] = $prevBlockNoOfRows + $layoutOrderData[$currentPlotIdx]['field_y'];
                                $layoutOrderData[$currentPlotIdx]['prev_block_no_of_rows'] = $prevBlockNoOfRows;
                            }
                        }
                    }
                }
            }

            if (!$isPreview) {

                // Build the layout order data of a block through appending if it exceeds the threshold
                $needToAppend = false;
                $totalPlots = isset($layoutOrderData)? count($layoutOrderData) : 0;
                $batchThreshold = 500;   // threshold per batch
                if($totalPlots > $batchThreshold) $needToAppend = true;

                // update matrix array of a block
                $updatedData = [
                    "notes" => json_encode($dataArr),
                    "layoutOrderData" => isset($layoutOrderData)? json_encode(array_slice($layoutOrderData, 0, $batchThreshold)) : json_encode(null)
                ];
                \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $updatedData);

                if($needToAppend) {
                    for($i = $batchThreshold; $i < $totalPlots; $i += $batchThreshold){
                        $appendData = [
                            "appendLayout" => true,
                            "layoutOrderData" => json_encode(array_slice($layoutOrderData, $i, $batchThreshold))
                        ];
                        $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $appendData);
                        unset($appendData);
                    }
                    $appendData = null;
                }
            } else {

                return [
                    'data' => $dataArr,
                    'rows' => $rows,
                    'cols' => $cols
                ];
            }
        }
    }


    /**
     * Update matrix data of a block
     *
     * @param $blockId integer block identifier
     * @param $dataArr array matrix data of block
     */
    public function updateMatrixDataByBlock($blockId, $dataArr)
    {
        $data = json_encode($dataArr);

        $updatedData = [
            "notes" => $data
        ];
        \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $updatedData);
    }

    /**
     * Get plots by block
     * @param $id integer block identifier
     */
    public function getPlotsByBlock($id, $tableName, $order)
    {

        $result = NULL;
        $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
            'experimentBlockDbId' => "$id"
        ]);
        if ($blockInformation['totalCount'] > 0) {
            $record = $blockInformation['data'][0]['layoutOrderData'];
            if ($record != NULL) {
                if ($order == 'asc') {
                    usort($record, function ($first, $second) {
                        return $first['plotno'] > $second['plotno'] ? 1 : -1;
                    });
                } else {
                    usort($record, function ($first, $second) {
                        return $first['plotno'] < $second['plotno'] ? 1 : -1;
                    });
                }
                $result = $record;
            }
        }
        return $result;
    }

    /**
     * Update block parameters of a block
     *
     * @param $data array block parameters
     * @param $id integer bock identifier
     */
    public function updateBlockParams($data, $id, $experimentId)
    {
        $labelName = [
            'no_of_reps_in_block' => 'noOfReps',
            'no_of_rows_in_block' => 'noOfRanges',
            'no_of_cols_in_block' => 'noOfCols',
            'starting_corner' => 'startingCorner',
            'plot_numbering_order' => 'plotNumberingOrder',
            'remarks' => 'remarks'
        ];
        $updatedData = [];
        $blockInformation = [];
        $entriesCount = 0;
        foreach ($data as $key => $value) {
            if($key == 'no_of_occurrences'){
                //Save FIRST_PLOT_POSITION_VIEW
                $varAbbrev = 'OCCURRENCE_COUNT';
                $variableInfo = \Yii::$container->get('app\models\Variable')->searchAll([
                    'fields' => 'variable.id AS variableDbId|variable.abbrev',
                    "abbrev"=>"equals $varAbbrev"
                ]);
                $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;

                $expData = ExperimentData::getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);
                if(isset($expData) && !empty($expData)){
                    if($value != $expData[0]['dataValue']){
                        $dataRequestBodyParam = [
                            'dataValue' => "$value"
                        ];
                        ExperimentData::updateExperimentData($expData[0]['experimentDataDbId'], $dataRequestBodyParam);
                    }
                } else {   //create variable
                                  
                    $dataRequestBodyParam[] = [
                        'variableDbId' => "$variableDbId",
                        'dataValue' => "$value"
                    ];
                    $dataResult = Experiment::createExperimentData($experimentId,$dataRequestBodyParam);
                }

                //check if there are existing occurrences without plots
                $occurrenceRecord = \Yii::$container->get('app\models\Occurrence')->searchAll([
                    'fields' => 'occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId',
                    'experimentDbId' => "equals ".$experimentId
                ]);
                if($occurrenceRecord['totalCount'] > 0){
                    $occurenceIds = array_column($occurrenceRecord['data'], 'occurrenceDbId');
                    $totalPlots = \Yii::$container->get('app\models\Plot')->searchAll(["fields"=>"plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurenceIds)], 'limit=1&sort=occurrenceDbId:asc', false)['totalCount'];

                    if($totalPlots == 0){ //delete occurrence records
                        \Yii::$container->get('app\models\Occurrence')->deleteMany($occurenceIds);
                    } else { //update notes

                    }
                }
            } else {
                if($key == 'no_of_reps_in_block'){
                    //get entries
                    $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                        'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList',
                        'experimentBlockDbId' => "$id"
                    ]);
                    $entryIdData = $blockInformation['data'][0]['entryListIdList'];
                    $entryIdsArr = array_column($entryIdData, 'entryDbId');
                    $entriesCount = count($entryIdData);
                    $newEntryListData = [];
                    foreach($entryIdData as $entry){
                        if(!isset($entry['entryDbId'])) continue;
                        $newEntryListData[] = [
                            'entryDbId' => $entry['entryDbId'],
                            'repno' => $value
                        ];
                    }
                    
                    $updatedData['entryListIdList'] = json_encode($newEntryListData);
                }
                if(isset($labelName[$key])) $updatedData[$labelName[$key]] = "$value";
            }
        }

        if(!empty($updatedData)){
            \Yii::$container->get('app\models\ExperimentBlock')->updateOne($id, $updatedData);
        }

        // limit for no. of entries
        if(empty($blockInformation)){
            $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList',
                'experimentBlockDbId' => "$id"
            ]);
            $entryIdData = $blockInformation['data'][0]['entryListIdList'];
            $entriesCount = count($entryIdData);
        }
        
        if($entriesCount > 500){

            //call background process for updating experiment blocks
            $addtlParams = ["description"=>"Creating of design records","entity"=>"EXPERIMENT_BLOCK","entityDbId"=>$experimentId,"endpointEntity"=>"EXPERIMENT_BLOCK", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'processName' => 'create-pa-records',
                'experimentDbId' => $experimentId."",
                'blockIds' => [$id],
                'resetBlockLayout' => false
            ];
            
            $create = Experiment::create($dataArray, true, $addtlParams);
            
            $experiment = Experiment::getExperiment($experimentId);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
            
            $bgStatus = Experiment::formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

            if($bgStatus !== 'done' && !empty($bgStatus)){
                
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
            
                Experiment::updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

                //update experiment
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The experiment block records for <strong>'.$experiment['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentId]);
            } else {
                //update worker status; seen and reflected
                $params = [
                    "jobIsSeen" => "true",
                    "jobRemarks" => "REFLECTED"
                ];
                \Yii::$container->get('app\models\BackgroundJob')->update(['backgroundJobDbId'=>$create['data'][0]], $params);

                // update status of experiment
                PlantingArrangement::updateExperimentStatus($experimentId);
            }
        } else {
            PlantingArrangement::updateTemporaryPlotsOfExperiment($experimentId, [$id], false, false);

            // update status of experiment
            PlantingArrangement::updateExperimentStatus($experimentId);
        }
    
    }

    /**
     * Retrieve all block details of an experiment
     *
     * @param $id integer block identifier
     */
    public function getBlocksDataByExperimentId($id)
    {

        //search all experiment-blocks
        $experimentBlocks = \Yii::$container->get('app\models\ExperimentBlock')->searchAll(['experimentDbId' => "$id", "isActive"=>"true"], 'sort=orderNumber:ASC');
        $parentBlock = array();
        $parentIdArray = array();
        $arrayParents = array();
        if ($experimentBlocks['totalCount'] > 0) {
            $arrayParents = array_values(array_column($experimentBlocks['data'], 'parentExperimentBlockDbId'));
        }

        foreach ($experimentBlocks['data'] as $block) {
            if ($block['parentExperimentBlockDbId'] === null) {

                if (!in_array($block['experimentBlockDbId'], $arrayParents)) {

                    $tempParent = [
                        'id' => $block['experimentBlockDbId'],
                        'code' => $block['experimentBlockCode'],
                        'name' => $block['experimentBlockName'],
                        'block_ids' => [$block['experimentBlockDbId']],
                        'block_codes' => [$block['experimentBlockCode']],
                        'block_names' => [$block['experimentBlockName']],
                        'block_reps' => [$block['noOfReps']],
                        'block_rows' => [$block['noOfRanges']],
                        'block_cols' => [$block['noOfCols']],
                        'block_plot_numbering_order' => [$block['plotNumberingOrder']],
                        'block_starting_corner' => [$block['startingCorner']],
                        'block_entry_counts' => !empty($block['entryListIdList']) ? [count($block['entryListIdList'])] : [0],
                        'total_no_of_plots_in_block' => [$block['plotCount']]
                    ];
                } else {

                    $tempParent = [
                        'id' => $block['experimentBlockDbId'],
                        'code' => $block['experimentBlockCode'],
                        'name' => $block['experimentBlockName'],
                        'block_ids' => [],
                        'block_codes' => [],
                        'block_names' => [],
                        'block_reps' => [],
                        'block_rows' => [],
                        'block_cols' => [],
                        'block_plot_numbering_order' => [],
                        'block_starting_corner' => [],
                        'block_entry_counts' => [],
                        $tempParent['total_no_of_plots_in_block'] = [0]
                    ];
                }

                $parentBlock[] = $tempParent;
                $parentIdArray[] = $block['experimentBlockDbId'];
            }
        }

        foreach ($experimentBlocks['data'] as $block) {
            if ($block['parentExperimentBlockDbId'] != null) {
                $index = array_search($block['parentExperimentBlockDbId'], $parentIdArray);

                

                $parentBlock[$index]['block_ids'][] = $block['experimentBlockDbId'];
                $parentBlock[$index]['block_codes'][] = $block['experimentBlockCode'];
                $parentBlock[$index]['block_names'][] = $block['experimentBlockName'];
                $parentBlock[$index]['block_reps'][] = $block['noOfReps'];
                $parentBlock[$index]['block_rows'][] = $block['noOfRanges'];
                $parentBlock[$index]['block_cols'][] = $block['noOfCols'];
                $parentBlock[$index]['block_plot_numbering_order'][] = $block['plotNumberingOrder'];
                $parentBlock[$index]['block_starting_corner'][] = $block['startingCorner'];
                $parentBlock[$index]['block_entry_counts'][] = !empty($block['entryListIdList']) ? count($block['entryListIdList']) : 0;
                $parentBlock[$index]['total_no_of_plots_in_block'][] = $block['plotCount'];
            }
        }

        return $parentBlock;
    }

    /**
     * Compute the total number of plots in a block from the no of reps per entry
     * @param $entryListIdList array containing the entries per block
     * @return $result integer total number of plots in block
     */
    public function getTotalPlotsInBlock($entryListIdList)
    {
        $result = 0;
       
        if(isset($entryListIdList) && !empty($entryListIdList)) {
            foreach($entryListIdList as $key => $val) {
                $result += $val['repno'];
            }
        }
        return $result;
    }

    /**
     * Retrieve variable scale values by abbrev
     * 
     * @param $abbrev text variable abbreviation
     * @return $result array list of variable scale values 
     */
    public function getScaleValuesByAbbrev($abbrev)
    {

        $variable = \Yii::$container->get('app\models\Variable')->getVariableByAbbrev($abbrev);
        $scaleValues = ScaleValue::getVariableScaleValues($variable['variableDbId'], null);

        return ArrayHelper::map($scaleValues, 'value', 'value');


    }

    /**
     * Get total entry count of entries assigned to block
     *
     * @param $id integer experiment identifier
     * @return $result integer entry count
     */
    public function getTotalEntryCountAssignedToBlock($id)
    {
        $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.experiment_block_name AS experimentBlockName|
                experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentDbId' => "equals $id"
        ], 'sort=experimentBlockName:ASC');

        $totalEntries = [];
        foreach ($experimentBlockArray['data'] as $block) {
            if ($block['entryListIdList'] != NULL) {
                $totalEntries = array_merge($totalEntries, array_column($block['entryListIdList'], 'entryDbId'));
            }
        }

        return count(array_unique($totalEntries));
    }

    /**
     * Get total replicate count of entries assigned to block
     *
     * @param $id integer experiment identifier
     * @return $result integer replicate count
     */
    public function getTotalReplicateCountAssignedToBlock($id) {
        $experimentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.experiment_block_name AS experimentBlockName|
                experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentDbId' => "equals $id"
        ], 'sort=experimentBlockName:ASC');

        $totalReplicates = 0;
        foreach ($experimentBlockArray['data'] as $block) {
            if ($block['entryListIdList'] != NULL) {
                $totalReplicates += array_sum(array_column($block['entryListIdList'], 'repno'));
            }
        }

        return $totalReplicates;
    }

    /**
     * Get total plot count of entries assigned to block
     *
     * @param $id integer experiment identifier
     * @return $result integer plot count
     */
    public function getTotalPlotCountAssignedToBlock($id)
    {
        $params = [
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.layout_order_data AS layoutOrderData',
            'experimentDbId' => "equals $id"
        ];

        $data = \Yii::$container->get('app\models\ExperimentBlock')->searchExperimentBlocks($params, '');

        $result = 0;
        foreach ($data as $block) {
            if (isset($block['layoutOrderData']) && !is_null($block['layoutOrderData']) && !empty($block['layoutOrderData'])) {
                $result += count($block['layoutOrderData']);
            }
        }

        return $result;
    }

    /**
     * Get total entry count of experiment
     *
     * @param $id integer experiment identifier
     * @return $result integer entry count
     */
    public function getTotalEntryCount($id)
    {
        //get entry list id
        $entryListId = $this->entryList->getEntryListId($id);
        $entries = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.entry_list_id AS entryListDbId','entryListDbId' => "equals ".$entryListId], 'limit=1&sort=entryListDbId:asc', false);

        return $entries['totalCount'];
    }

    /**
     * Get total replicate count of experiment
     *
     * @param $id integer experiment identifier
     * @return $result integer replicate count
     */
    public function getTotalReplicateCount($id) {
        $varAbbrev = 'ENTRY_LIST_TIMES_REP';
        $variableInfo = \Yii::$container->get('app\models\Variable')->searchAll([
            'fields' => 'variable.id AS variableDbId|variable.abbrev',
            "abbrev"=>"equals $varAbbrev"
        ]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        $expData = \Yii::$container->get('app\models\ExperimentData')->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);

        $entryReplicateCount = 0;
        if(isset($expData[0])) {
            $entryReplicates = json_decode($expData[0]['dataValue'], true);
            $entryReplicates = array_column($entryReplicates, 'repno');
            $entryReplicateCount = array_sum($entryReplicates);
        }
        return $entryReplicateCount;
    }

    /**
     * Remove a block
     *
     * @param $experimentId integer experiment identifier
     * @param $id ineteger block identifier
     */
    public function deleteBlock($experimentId, $id)
    {
        $isParentBlock = false;

        //get current block
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                parentExperimentBlock.id AS parentExperimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentBlockDbId' => "equals $id"
        ]);
        $parentId = isset($currentBlock['data'][0])? $currentBlock['data'][0]['parentExperimentBlockDbId'] : [];

        //If null, then block type is parent block
        if (empty($parentId)) {
            $isParentBlock = true;
        }

        if (!$isParentBlock) {
            $otherBlocks = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
                'fields' => 'experiment.id AS experimentDbId|parentExperimentBlock.id AS parentExperimentBlockDbId',
                'experimentDbId' => 'equals '.$experimentId,
                'parentExperimentBlockDbId' => 'equals '.$parentId
            ]);
            $updatedCount = $otherBlocks['totalCount'];
            if($updatedCount > 0){
                $updatedCount = $otherBlocks['totalCount'] - 1;
            }
            //update parent block
            \Yii::$container->get('app\models\ExperimentBlock')->updateOne($parentId, ["noOfBlocks" => $updatedCount.""]);
        }

        //limit for no. of entries
        $entryListId = $this->entryList->getEntryListId($experimentId);
        $entries = \Yii::$container->get('app\models\Entry')->searchAll(['fields'=>'entry.entry_list_id AS entryListDbId','entryListDbId' => "equals ".$entryListId], 'limit=1&sort=entryListDbId:asc', false);

        // update entry list replicate data (set reps in block to null)
        if(!empty($currentBlock['data']) && isset($currentBlock['data'][0]['entryListIdList'])){
            \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($experimentId, $currentBlock['data'][0]['entryListIdList'], [null]);
        }

        if($entries['totalCount'] > 500){
            //call background process for creating entries
            $addtlParams = ["description"=>"Creating of design records","entity"=>"EXPERIMENT_BLOCK","entityDbId"=>$experimentId,"endpointEntity"=>"EXPERIMENT_BLOCK", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'processName' => 'create-pa-records',
                'experimentDbId' => $experimentId."",
                'blockIds' => [$id],
                'deleteBlock' => true
            ];
            
            $create = Experiment::create($dataArray, true, $addtlParams);
            
            $experiment = Experiment::getExperiment($experimentId);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
            
            $bgStatus = Experiment::formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

            if($bgStatus !== 'done' && !empty($bgStatus)){
                
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
            
                Experiment::updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

                //update experiment
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The experiment block records for <strong>'.$experiment['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentId]);
            } else {
                //update worker status; seen and reflected
                $params = [
                    "jobIsSeen" => "true",
                    "jobRemarks" => "REFLECTED"
                ];
                \Yii::$container->get('app\models\BackgroundJob')->update(['backgroundJobDbId'=>$create['data'][0]], $params);

                // update status of experiment
                PlantingArrangement::updateExperimentStatus($experimentId);

            }
        } else {
            PlantingArrangement::updateTemporaryPlotsOfExperiment($experimentId, [$id], true);

            //update block
            \Yii::$container->get('app\models\ExperimentBlock')->deleteOne($id);

            // update status of experiment
            PlantingArrangement::updateExperimentStatus($experimentId);
        }
    }

    /**
     * Retrieve entries of a block in manage entries
     *
     * @param $id integer block identifier
     * @param $varCols array list of entry list dynamic variables
     * @return $dataProvider array manage entries data provider
     */
    public function getEntriesByBlock($id, $varCols = [])
    {

        $entries = [];
        $currentBlockArray = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experimentBlock.id AS experimentBlockDbId|experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentBlockDbId' => "equals $id",
            "isActive"=>"equals true"
        ]);
        if ($currentBlockArray['totalCount'] > 0) {
            $entryIds = $currentBlockArray['data'][0]['entryListIdList'];
            $entryListIdListTempCount = count($entryIds);
            $entryListIdListTemp = $currentBlockArray['data'][0]['entryListIdList'];
            $entryCond = '';

            for($i = 0; $i < $entryListIdListTempCount; $i++){
                $entryCond = empty($entryCond) ? 'equals ' . $entryListIdListTemp[$i]['entryDbId'] : $entryCond. '|equals '.$entryListIdListTemp[$i]['entryDbId'];
            }

            if(!empty($entryCond)){
                $entries = \Yii::$container->get('app\models\Entry')->searchAll(['entryDbId' => $entryCond]);
                $entries = $entries['data'];
            }
        }
        $entryColumns = [
            'entryDbId',
            'entryNumber',
            'entryCode',
            'entryName',
            'entryType',
            'entryRole',
            'entryClass',
            'entryStatus',
            'description',
            'entryListDbId',
            'germplasmDbId',
            'designation',
            'parentage',
            'generation',
            'seedDbId',
            'seedCode',
            'seedName',
            'sourceEntryDbId',
            'sourceOccurrenceDbId',
            'notes',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier',
            'availability'
        ];

        return new ArrayDataProvider([
            'allModels' => $entries,
            'key' => 'entryDbId',
            'pagination' => false,
            'sort' => false
        ]);


    }

    /**
     * TO BE REFACTORED
     * Remove entry in a block
     *
     * @param $id integer experiment identifier
     * @param $blockId integer block identifier
     * @param $entryIds array list of entry ids to be removed
     */
    public function removeEntryInBlock($id, $blockId, $entryIds)
    {
        $entryIds = implode(',', $entryIds);
        $sql = "
            update 
                operational.experiment_block 
            set 
                entry_list_ids = (
                    select array (
                        with s as(
                            (
                                select unnest (
                                    (select 
                                        array (
                                            with t as (
                                                select unnest(entry_list_ids) as entry_ids
                                            ) 
                                            select
                                                entry_ids 
                                            from 
                                                t 
                                            order by position(entry_ids::text in (select trim(entry_ids::text, '{}') ))    
                                    ) 
                                    from 
                                        operational.experiment_block 
                                    where 
                                        id = {$blockId}
                                    )
                                ) as entry_ids
                            ) 
                            except 
                            ( 
                                select unnest('{" . $entryIds . "}'::INT[]) as entry_ids
                            ) 
                        ) select entry_ids from s order by position(entry_ids::text in    (select trim ((select entry_list_ids::text from operational.experiment_block where id = {$blockId}),'{}') ) )
                    )
                ),
                no_of_rows_in_block = 0,
                no_of_cols_in_block = 0
            where
                id = {$blockId}
        ";
        Yii::$app->db->createCommand($sql)->execute();

        // regenerate plots to succeeding blocks
        PlantingArrangement::updateTemporaryPlotsOfExperiment($id, [$blockId]);
        // update status of experiment
        PlantingArrangement::updateExperimentStatus($id);
    }

    /**
     * TO BE REFACTORED
     * Reorder entries in a block
     *
     * @param $id integer experiment identifier
     * @param $blockId integer block identifier
     * @param $entryIds array list of ordered entry ids
     */
    public function reorderEntriesInBlock($id, $blockId, $entryIds)
    {
        $entryIds = implode(',', $entryIds);

        $sql = "
            update
                operational.experiment_block
            set
                entry_list_ids = '{" . $entryIds . "}'
            where
                id = {$blockId}
        ";

        Yii::$app->db->createCommand($sql)->execute();

        //get starting plot no
        $startingPlotNo = PlantingArrangement::getStartingPlotNo($id, $blockId);

        // update plot numbers
        $sql = "
            with s as (
                with t as (
                    select 
                        id,
                        unnest(entry_list_ids) as entry_id
                    from 
                        operational.experiment_block 
                        where id = {$blockId} and is_void = false
                ) 
                select id as block_id, t.entry_id, row_number() over (partition by id order by id) as plotno from t
            ) 
            update
                z_admin.planting_arrangement_{$id} pa
            set
                plotno = (s.plotno + ({$startingPlotNo} - 1))
            from
                s
            where
                s.block_id = pa.block_id
                and s.entry_id = pa.entry_id
                and is_void = false
        ";

        Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * Remove plots in a block
     *
     * @param $id integer experiment identifier
     * @param $blockId integer block identifier
     * @param $plotNos array list of ordered plot nos
     */
    public function removePlotsInBlock($id, $blockId, $plotNos)
    {
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.entry_list_id_list AS entryListIdList',
            'experimentBlockDbId' => "equals $blockId"
        ]);
        $layoutOrderData = $currentBlock['data'][0]['layoutOrderData'];
        $entryListIdList = $currentBlock['data'][0]['entryListIdList'];
        $layoutOrderPlotNos = array_column($layoutOrderData, 'plotno');
        $startingPlotNo = min($layoutOrderPlotNos);
        $entryListIdListToRemove = [];

        // Get indexes of plots to remove
        $indexes = array_map(function($val) use($startingPlotNo) {return intval($val) - $startingPlotNo;}, $plotNos);

        // Get tally to decrease repno in entryListIdList
        $tally = [];
        foreach($indexes as $index) {
            $entryId = $layoutOrderData[$index]['entry_id'];
            $tally[$entryId] = [
                'replicates' => !isset($tally[$entryId]) || empty($tally[$entryId]) ? [$layoutOrderData[$index]['repno']] : array_merge($tally[$entryId]['replicates'], [$layoutOrderData[$index]['repno']])
            ];
        }
        $entryDbIds = array_column($entryListIdList, 'entryDbId');
        foreach($tally as $key => $val) {
            $i = array_search($key, $entryDbIds);
            $entryListIdListToRemove[] = [
                'entryDbId' => $key,
                'replicates' => $val['replicates']
            ];
            $entryListIdList[$i]['repno'] = $entryListIdList[$i]['repno'] - count($val['replicates']);
        }

        // remove entry from entryListIdList if repno becomes 0
        for ($i=count($entryListIdList) - 1; $i >= 0; $i--) {
            if($entryListIdList[$i]['repno'] <= 0) {
                unset($entryListIdList[$i]);
            }
        }
        $entryListIdList = array_values($entryListIdList);

        // update entry list replicate data (set deleted reps in block to null)
        \Yii::$container->get('app\modules\experimentCreation\models\PAEntriesModel')->updateEntryListReplicate($id, $entryListIdListToRemove, [null]);

        // Remove plots based on indexes
        $indexes = array_flip($indexes);
        $layoutOrderData = array_diff_key($layoutOrderData, $indexes);
        $layoutOrderData = array_values($layoutOrderData);
        foreach($layoutOrderData as $key => $val) { // Update plot nos
            $layoutOrderData[$key]['plotno'] = $key + $startingPlotNo;
        }

        // Build the layout order data of a block through appending if it exceeds the threshold
        $needToAppend = false;
        $totalPlots = count($layoutOrderData);
        $batchThreshold = 500;   // threshold per batch
        if($totalPlots > $batchThreshold) $needToAppend = true;

        // Update block
        $param = [
            'entryListIdList' => json_encode($entryListIdList),
            "layoutOrderData" => json_encode(array_slice($layoutOrderData, 0, $batchThreshold))
        ];
        \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $param);

        if($needToAppend) {
            for($i = $batchThreshold; $i < $totalPlots; $i += $batchThreshold){
                $appendData = [
                    "appendLayout" => true,
                    "layoutOrderData" => json_encode(array_slice($layoutOrderData, $i, $batchThreshold))
                ];
                $updateRecord = \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $appendData);
                unset($appendData);
            }
            $appendData = null;
        }

        PlantingArrangement::updateBlockCoordinates($id, $blockId, ''); // Update notes

        // Get experiment block ids
        $params = [
            'fields' => 'experiment.id as experimentDbId|experimentBlock.id as experimentBlockDbId',
            'experimentDbId' => $id
        ];
        $filters = 'sort=experimentBlockDbId';
        $coBlocksInExperiment = \Yii::$container->get('app\models\ExperimentBlock')->searchAll($params, $filters, true);
        $coBlocksInExperiment = $coBlocksInExperiment['data'];

        // Get array of succeeding block ids
        $succeedingBlockIds = [];
        foreach($coBlocksInExperiment as $key => $val) {
            $currBlockId = $val['experimentBlockDbId'];
            // If blockId is not succeeding block, then skip
            if($currBlockId <= $blockId) {
                continue;
            }
            $succeedingBlockIds[] = $currBlockId;
        }

        // regenerate plots of succeeding blocks
        PlantingArrangement::updateTemporaryPlotsOfExperiment($id, $succeedingBlockIds);
        // update status of experiment
        PlantingArrangement::updateExperimentStatus($id);
    }

    /**
     * Reorder plots in a block
     *
     * @param $id integer experiment identifier
     * @param $blockId integer block identifier
     * @param $plotNos array list of ordered plot nos
     */
    public function reorderPlotsInBlock($id, $blockId, $plotNos)
    {
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId|
                experimentBlock.layout_order_data AS layoutOrderData',
            'experimentBlockDbId' => "equals $blockId"
        ]);
        $layoutOrderData = $currentBlock['data'][0]['layoutOrderData'];
        $startingPlotNo = min($plotNos);

        // Update order by plot no then sort layoutOrderData
        foreach($plotNos as $key => $val) {
            $layoutOrderData[intval($val) - $startingPlotNo]['plotno'] = $key + $startingPlotNo;
        }
        usort($layoutOrderData, function ($first, $second) {
            return $first['plotno'] > $second['plotno'] ? 1 : -1;
        });

        // Build the layout order data of a block through appending if it exceeds the threshold
        $needToAppend = false;
        $totalPlots = count($layoutOrderData);
        $batchThreshold = 500;   // threshold per batch
        if($totalPlots > $batchThreshold) $needToAppend = true;

        // Update order layout data of block
        $param = [
            "layoutOrderData" => json_encode(array_slice($layoutOrderData, 0, $batchThreshold))
        ];

        PlantingArrangement::updateBlockCoordinates($id, $blockId, ''); // Update notes
        \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $param);

        if($needToAppend) {
            for($i = $batchThreshold; $i < $totalPlots; $i += $batchThreshold){
                $appendData = [
                    "appendLayout" => true,
                    "layoutOrderData" => json_encode(array_slice($layoutOrderData, $i, $batchThreshold))
                ];
                \Yii::$container->get('app\models\ExperimentBlock')->updateOne($blockId, $appendData);
                unset($appendData);
            }
            $appendData = null;
        }
    }

    /**
     * Retrieve column name and description of a variable for the form displays
     */
    public function getFormDisplay($variableAbbrev)
    {
        $variableModel = \Yii::$container->get('app\models\Variable')->getVariableByAbbrev(strtoupper(trim($variableAbbrev)));
        $variableName = ucwords(strtolower(isset($variableModel['name'])? $variableModel['name'] : ''));
        return [
            'name' => $variableName,
            'description' => !empty($variableModel['description']) ? $variableModel['description'] : $variableName
        ];



    }

    /**
     * Generates the dataprovider for the working list
     * @param $entries Array of selected entries
     */
    public function getWorkingListProvider($entries){
       
        $totalCount = $entries['totalCount'];

        $providerDetails = [
            'allModels' => $entries['data'],
            'totalCount' => $totalCount,
            'key' => 'entryDbId',
            'pagination' => false,
            'sort' => false
          ];
          
          return new \yii\data\ArrayDataProvider($providerDetails);


    }

    /**
     * Checks if there are succeeding experiment status/es and unsets them and adds a flag
     * to trigger deletion of previously created plots and planting instructions
     *
     * @param $experimentId integer experiment identifier
     */
    public function checkExperimentPAUpdate($experimentId)
    {
        $experiment = Experiment::getExperiment($experimentId);
        $status = explode(';', $experiment['experimentStatus']);

        // If has design generated status and there are succeeding statuses
        if(in_array('design generated', $status) && (in_array('protocol specified', $status) || in_array('occurrences created', $status))) {
            // Remove succeeding status/es
            $status = array_filter($status, function($s) {
                if($s != 'protocol specified' && $s != 'occurrences created') return $s;
            });
            $updatedStatus = implode(';', $status);

            Experiment::updateOne($experimentId, ['experimentStatus' => $updatedStatus, 'notes' => 'plantingArrangementChanged']);
        }
    }

    /**
     * Compute the experiment total number of plots in a block from the no of reps per entry
     * @param $entryListIdList array containing the entries per block
     * @return $result integer total number of plots in block
     */
    public function getExperimentTotalPlots($id)
    {
        $result = 0;

        $model = Experiment::getOne($id)['data'];
       
        if(strpos($model['dataProcessAbbrev'], '_TRIAL_DATA_PROCESS') === false){
            //Get the total plots in the experiment 
            $experimentBlockRecords = \Yii::$container->get('app\models\ExperimentBlock')->searchAll(['fields'=>'experimentBlock.id AS experimentBlockDbId|experimentBlock.experiment_block_name AS experimentBlockName|
            experimentBlock.experiment_id AS experimentDbId|experimentBlock.entry_list_id_list AS entryListIdList','experimentDbId' => "$id"] , 'sort=experimentBlockName:ASC');
            
            $experimentBlocks = $experimentBlockRecords['data'];

            foreach($experimentBlocks as $experimentBlock){
                if(isset($experimentBlock['entryListIdList']) && !empty($experimentBlock['entryListIdList'])){
                    foreach($experimentBlock['entryListIdList'] as $key => $val) {
                        $result += $val['repno'];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get information of a block
     * 
     * @param integer $currentBlockId record if of the experiment block
     * @param string $experimentType value of experiment type
     */
    public function getBlockInformation($currentBlockId, $experimentType = NULL){

      if($experimentType != 'Observation'){
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => '
                experimentBlock.id AS experimentBlockDbId|
                experimentBlock.entry_list_id_list AS entryListIdList|
                experimentBlock.no_of_ranges AS noOfRanges|
                experimentBlock.no_of_cols AS noOfCols|
                experimentBlock.plot_numbering_order AS plotNumberingOrder|
                experimentBlock.remarks AS remarks|
                experimentBlock.starting_corner AS startingCorner',
                'experimentBlockDbId' => "$currentBlockId",
        ]);
      } else {
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => '
                experimentBlock.id AS experimentBlockDbId|
                experimentBlock.entry_list_id_list AS entryListIdList|
                experimentBlock.no_of_ranges AS noOfRanges|
                experimentBlock.no_of_cols AS noOfCols|
                experimentBlock.plot_numbering_order AS plotNumberingOrder|
                experimentBlock.no_of_reps AS noOfReps|
                experimentBlock.remarks AS remarks|
                experimentBlock.starting_corner AS startingCorner',
                'experimentBlockDbId' => "$currentBlockId",
        ]);
      }
      
      $currentBlock = $currentBlock['data'][0];
      $entryListIdList = $currentBlock['entryListIdList'];
      $entryCount = is_array($entryListIdList)? count($entryListIdList): 0;
      $entryListIdList = is_array($entryListIdList)? array_column($entryListIdList, 'repno'): [];
      $plotCount = is_array($entryListIdList)? array_sum($entryListIdList): 0;

      $sBlockParams = [
          'no_of_entries_in_block'=>$entryCount,
          'total_no_of_plots_in_block'=>$plotCount,
          'no_of_rows_in_block'=>$currentBlock['noOfRanges'],
          'no_of_cols_in_block'=>$currentBlock['noOfCols'],
          'starting_corner'=>$currentBlock['startingCorner'],
          'plot_numbering_order'=>$currentBlock['plotNumberingOrder'],
          'remarks'=>$currentBlock['remarks']
      ];

      if($experimentType == 'Observation'){
          $sBlockParams['no_of_reps_in_block'] = empty($currentBlock['noOfReps'])? 1 : $currentBlock['noOfReps'];
      }

      return $sBlockParams;
    }

    /**
     * Update the entry role for Augmented Design
     * 
     * @param integer $entryListId record id of the entry list
     * @param string $update value of entry role
     * @param array $entryIds record ids of the entries
     */
    public function updateEntryRole($entryListId, $update, $entryIds = []){
        if($entryListId != null){
            $fields = [
                'fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId|entry.entry_role as entryRole',
                'entryListDbId'=>"equals $entryListId", 
                'entryType'=>'check'
            ];
            if($update == 'spatial check'){
                $fields['entryRole'] = 'null';
            }
            if($update == 'null'){
                $fields['entryRole'] = 'spatial check';
            }
            $entryListCheckArray =  \Yii::$container->get('app\models\Entry')->searchAll($fields, '', true)['data'];
            $entryIds = array_column($entryListCheckArray, 'entryDbId');
        }

        if(count($entryIds) > 0){
            $requestedData =[
                "entryRole" => $update,
            ];
            
            foreach ($entryIds as $entryId) {
                \Yii::$container->get('app\models\Entry')->updateOne($entryId, $requestedData);
            }
        }
    }

    /**
     * Check if block has assigned replicates
     *
     * @param integer $currentBlockId record if of the experiment block
     */
    public function blockHasAssignReplicates($currentBlockId) {
        // check if sub blocks entryListIdList are not empty
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => '
                experimentBlock.id AS experimentBlockDbId|
                experimentBlock.entry_list_id_list AS entryListIdList|
                parentExperimentBlock.id as parentExperimentBlockDbId',
                'parentExperimentBlockDbId' => "$currentBlockId",
        ]);
        foreach ($currentBlock['data'] as $currentBlock) {
            $entryListIdList = $currentBlock['entryListIdList'];
            if(!empty($entryListIdList)) {
                return true;
            }
        }

        // check if current block entryListIdList id not empty
        $currentBlock = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => '
                experimentBlock.id AS experimentBlockDbId|
                experimentBlock.entry_list_id_list AS entryListIdList',
                'experimentBlockDbId' => "$currentBlockId",
        ]);
        $currentBlock = $currentBlock['data'][0];
        $entryListIdList = $currentBlock['entryListIdList'];
        if(!empty($entryListIdList)) {
            return true;
        }



        return false;
      }
}