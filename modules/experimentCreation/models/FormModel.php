<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model for getting the process path information
 */

namespace app\models;
namespace app\modules\experimentCreation\models;

use Yii;
use app\models\Program;
use app\models\Experiment;
use app\models\Item;
use app\models\Variable;
use app\models\ItemRelation;
use app\models\ExperimentData;

use kartik\dynagrid\Module;
use app\models\ItemModule;
use app\models\PlatformModule;

use app\modules\experimentCreation\controllers\CreateController;

class FormModel extends \yii\base\BaseObject{

    public function __construct (
        public Experiment $experiment,
    ) { }

    public $activities;
    
    /**
    * Get the children items for the process path
    * @param $id integer item ID
    * @param $rootId integer item root ID
    * @param $endLevel integer Type of the item in the table
    * @return $nodes object containing the node information of an item
    */
    public function getChildrenItems($id, $rootId, $endLevel = 0) {
        $nodes = array();
        $childrenItems = ItemRelation::getItemRelations(['rootDbId'=>"equals $rootId", 'parentDbId'=>"equals $id"]);
        
        if (!empty($childrenItems)) {
            
            foreach ($childrenItems as $i => $child) {
                $tempChildId = $child['childDbId'];
                $childItem = Item::searchItemRecord(['itemDbId'=>"equals $tempChildId"]);
                
                $childItem = !empty($childItem['data']) ? $childItem['data'] : null;
                
                if (!empty($childItem) && $childItem['type'] >= $endLevel) {
                    $nodes[] = [
                        'item' => $childItem,
                        'id' => $childItem['itemDbId'],
                        'abbrev' => $childItem['abbrev'],
                        'name' => $childItem['name'],
                        'display_name' => $childItem['displayName'],
                        'description' => $childItem['description'],
                        'order_number' => $i + 1,
                        'item_icon' => $childItem['itemIcon'],
                        'item_group' => $childItem['itemGroup'],
                        'children' => FormModel::getChildrenItems($childItem['itemDbId'], $rootId, $endLevel),
                    ];
                }
            }
        }

        return $nodes;
    }

    /**
     * Get the items related to the process path
     * @param $id integer item ID
     * @param $rootId integer item root ID
     * @param $endLevel integer Type of the item in the table
     * @return $array object containing the node information of an item
     */
    public function getItemsInTree($id, $rootId = NULL, $endLevel = 0) {
        
        $item = Item::searchItemRecord(['itemDbId'=>"equals $id"]);
        $item = !empty($item['data']) ? $item['data'] : null;   
        if (empty($item)) {
            return NULL;
        }
        if (empty($rootId)) {
            $rootId = $item->id;
        } elseif (!is_numeric($rootId)) {
            $root = Item::searchItemRecord(['itemDbId'=>"equals $rootId"]);
            $root = !empty($root['data']) ? $root['data'] : null;
            if (!empty($root)) {
                $rootId = $root['itemDbId'];
            } else {
                $rootId = 0;
            }
        }
        
        return [
            'item' => $item,
            'id' => $item['itemDbId'],
            'abbrev' => $item['abbrev'],
            'name' => $item['name'],
            'display_name' => $item['displayName'],
            'description' => $item['description'],
            'order_number' => 0,
            'item_icon' => $item['itemIcon'],
            'item_group' => $item['itemGroup'],
            'children' => FormModel::getChildrenItems($item['itemDbId'], $rootId, $endLevel),
        ];
    }

    /**
     * Get the activities related to the process path
     * @param $processId integer item process ID
     * @param $dataProcess object item information
     * @return $activities object containing the node of activities of an item
    */
    public function getActivities($processId, $dataProcess = NULL) {
        if(!empty($dataProcess)){
            $activities = [
                'id' => $dataProcess['itemDbId'],
                'abbrev' => $dataProcess['abbrev'],
                'name' => $dataProcess['name'],
                'display_name' => $dataProcess['displayName'],
                'description' => $dataProcess['description'],
                'order_number' => 0,
                'item_icon' => $dataProcess['itemIcon'],
                'item_group' => $dataProcess['itemGroup'],
                'children' => FormModel::getChildrenItems($processId, $processId, getenv('ITEM_ACTV')),
            ];
        }else{
            $activities = FormModel::getItemsInTree($processId, $processId, getenv('ITEM_ACTV'));
        }
        
        if(!empty($activities['children'])){
            for($i = 0; $i<count($activities['children']); $i++){
                $childUrl = [];
                if(count($activities['children'][$i]['children']) > 0){
                    for($j = 0; $j<count($activities['children'][$i]['children']); $j++){
                        $tempChild = $activities['children'][$i]['children'][$j];
                        $activities['children'][$i]['children'][$j] = FormModel::getItemModuleInfo($tempChild, $activities['children'][$i]['children'][$j]['id']);
                        $childUrl[] = $activities['children'][$i]['children'][$j]['url'];
                    }
                }
                $tempAct = $activities['children'][$i];
                $tempAct['child_url'] = $childUrl;
               
                $activities['children'][$i] = FormModel::getItemModuleInfo($tempAct, $activities['children'][$i]['id']);
            }
            return $activities;
        }
    }

    /**
     * Get the item module information
     * @param $tempAct object item module info
     * @param $itemId integer item ID
     * @return $temptAct object containing info of the item module
    */
    public function getItemModuleInfo($tempAct, $itemId){
      Yii::info('Item ID ' . json_encode(['itemId'=>$itemId]), __METHOD__);

      $itemModParam = ['itemId'=>''.$itemId.''];
      $itemModule= ItemModule::getItemModule($itemModParam);  

      Yii::info('Get item module info ' . json_encode(['itemModule'=>$itemModule]), __METHOD__);

      $module = PlatformModule::getModule($itemModule['moduleDbId']); 
      
      $tempAct['url']='/'.$module['moduleId'].'/'.$module['controllerId'].'/'.$module['actionId'];
      $tempAct['action_id']=$module['actionId'];
      $tempAct['status']=$module['required_status'];
      return $tempAct;
    }

    /**
     * Get the tasks related to the activity
     * @param $activityId integer item activity ID
     * @param $processId integer item process ID
    */
    public function getTasks($activityId, $processId) {
        return FormModel::getItemsInTree($activityId, $processId, getenv('ITEM_TASK'));
    }

    /**
     * Get the steps related to the tasks
     * @param $taskId integer item task ID
     * @param $processId integer item process ID
     */
    public function getSteps($taskId, $processId) {
        return FormModel::getItemsInTree($taskId, $processId, getenv('ITEM_STEP'));
    }

    /**
     * Prepares the process tabs for experiment creation
     *
     * @param string $active current action for the experiment creation step
     * @param string $program name of the current program
     * @param integer $id record id of the experiment
     * @param $processId integer item process ID
     *
     */
    public function prepare($active, $program, $id = NULL, $processId = NULL){
        
        if($processId != NULL){

            $id = !empty($id) ? $id : 0;
            $abbrev = 'HAS_EXPERIMENT_GROUP';

                   
            $experimentData = [];

            $hasGroups = false;
         
            if(isset($experimentData['value'])){

               
                if(in_array(strtolower($experimentData['value']), ['true', '1', true])){
                    $hasGroups = true;
                }
            }

            if($processId == 'undefined'){
                $experiment = Experiment::getExperiment($id);
                $dataProcess = Item::getItem('BREEDING_TRIAL_DATA_PROCESS');

                $dataProcItemDbId = isset($dataProcess['itemDbId']) && !empty($dataProcess['itemDbId']) ? $dataProcess['itemDbId'] : null;

                $experiment->data_process_id = $dataProcItemDbId;
                $experiment->save();

                $_GET['processId'] = $dataProcess['itemDbId'];
                $processId = $dataProcess['itemDbId'];
            }
          
            $variableProcess = Item::searchItemRecord(['itemDbId'=>"equals $processId"]);
            
            $variableProcessDbId = null;
            if(!empty($variableProcess['data'])){
                $variableProcessDbId = $variableProcess['data']['itemDbId'];
            }
            $activities = FormModel::getActivities($variableProcessDbId);

            //remove tab for experiment groups
            if(!$hasGroups && !empty($activities['children'])){
                
                foreach($activities['children'] as $key=>$value) {

                    if (strpos($value['abbrev'],"EXPT_GROUP_ACT")) {

                        unset($activities['children'][$key]);
                    }
                }
            }
        }else{
            $dataProcess = Item::getItem('EXPT_DEFAULT_DATA_PROCESS');
            if(!empty($dataProcess)){
                $processId = $dataProcess['itemDbId'];
                $activities = FormModel::getActivities($processId, $dataProcess);
            }
        }
      
        if(!empty($activities['children'])){
            $activities['children'] = array_values($activities['children']);
        
            if(!in_array($active, array_column($activities['children'], 'url'))){
                for($i = 0; $i<count($activities['children']); $i++){
                    
                    if(strpos($active, $activities['children'][$i]['action_id']) !== false){
                        $active = $activities['children'][$i]['url'];
                    
                        break;
                    }
                }
            }
            $activities['active'] = $active;
            $activities['program'] = $program;
    
            return FormModel::getTabs($id, $activities);

        }
     
    }

    /**
     * Get tabs for the data process
     *
     *
     */
    public function getTabs($id, $activities){
        if($id != NULL){
            $experiment = $this->experiment->getExperiment($id);
            if(isset($experiment)){
                $exptStatusArr = explode(';', $experiment['experimentStatus']);
                $latestStatus = $exptStatusArr[count($exptStatusArr)-1];
            }else{
                $latestStatus = 'draft';
            }
        } else{
            $latestStatus = 'draft';
        }

        $orderedStatus = array_column($activities['children'], 'status');
        $statusIndex = array_search($latestStatus, $orderedStatus);
        
        foreach($activities['children'] as $key=>$value) {

            if($key <= $statusIndex || $key == ($statusIndex + 1)){
                $activities['children'][$key]['disabled'] = '';
            } else {
                $activities['children'][$key]['disabled'] = 'disabled';
            }
        }
        return $activities;
    }

     /**
     * Check if data process has design activitiy
     *
     */
    public function checkDataProcess($processId){
      
        $activities = FormModel::getActivities($processId);
        if(!empty($activities)){
            $activityAbbrev = array_column($activities['children'], 'abbrev');
      
            return array_filter($activityAbbrev, function($value) {
                return strpos($value, 'TRIAL_DESIGN') !== false;
            });
    

        } 
    }
}