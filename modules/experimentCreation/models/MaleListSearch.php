<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Entry;
use yii;

use yii\helpers\ArrayHelper;

// interfaces
use app\interfaces\modules\experimentCreation\models\IMaleListSearch;

use app\modules\experimentCreation\models\ExperimentModel;

/**
 * This is the ActiveQuery class for Male List Search.
 */
class MaleListSearch extends Entry implements IMaleListSearch
{

    public $entryDbId;
    public $entryNumber;
    public $entryCode;
    public $entryRole;
    public $designation;
    public $parentage;
    public $germplasmDbId;
    public $femaleCount;
    public $maleCount;

    protected $experimentModel;

    /**
     * Returns the validation rules for attributes.
     * This method is overridden
     * 
     * @return array validation rules
     */
    public function rules() {
        
        return [
            [['entryDbId','entryNumber','germplasmDbId','femaleCount','maleCount'],'integer'],
            [['entryRole', 'designation', 'parentage', 'entryCode'],'string']
        ];
    }

    /**
     * Search model for male parents
     *
     * @param Array $params list of query parameters
     * @param String $filters set text of existing filters
     * @param String $currentPage current page parameter
     * @param String $type input use for creating crosses
     *
     * @return Array $dataProvider male browser data provider
     */
    public function search($params, $filters = '', $currentPage = null, $type='designation'){
        $experimentModel = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel');
        extract($params);
        $this->load($params);

        $params = $params['MaleListSearch'];
        $params['includeParentCount'] = true;

        $results = $experimentModel->getParentList($params, $filters, 'male', $currentPage);

        Yii::$app->session->set('male_entry_ids_params', $params);

        return $results;
    }
}
?>