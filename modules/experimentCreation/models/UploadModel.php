<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\modules\experimentCreation\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\models\Entry;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\User;
use app\models\Scale;
use app\models\Variable;
use yii\web\UploadedFile;
use yii\httpclient\Client;
use yii\httpclient\Request;



use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\models\DesignModel;
/**
 * Upload model for design array
 */
class UploadModel
{
    protected $entry;
    protected $experiment;
    protected $experimentModel;
    protected $occurrence;
    protected $planTemplate;
    protected $variable;
    protected $scale;

    public function __construct(
        DesignModel $designModel,
        Entry $entry, 
        Experiment $experiment,
        ExperimentModel $experimentModel,
        Occurrence $occurrence,
        PlanTemplate $planTemplate,
        Variable $variable,
        Scale $scale
    )
    {
        $this->designModel = $designModel;
        $this->entry = $entry;
        $this->experiment = $experiment;
        $this->experimentModel = $experimentModel;
        $this->occurrence = $occurrence;
        $this->planTemplate = $planTemplate;
        $this->variable = $variable;
        $this->scale = $scale;
    }

    public function preValidateFile($fileName, $id, $experimentDesign, $occurrenceDbIds=null){

        $error['success'] = false;

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads';
            $file = UploadedFile::getInstanceByName($fileName);
            $oldFileName =  $file->name;
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false, 
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $rawFileName = \Yii::$app->security->generateRandomString();
            $origTmpFileName = $rawFileName . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $origTmpFileName;

            $file->saveAs($tmpFileName);
           
            //Copy if successfully validated
            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);
            $headers = $csv->getHeader();
            
            //initial creation of file with headers updated
            $root=Yii::getAlias('@webroot').'/analytics/output/'.$origTmpFileName;
            if(file_exists($root)){
                chmod($root, 777);
                unlink($root);
            }
            $headerFlag = 0;
            $handle = fopen($root, 'x');

            $inputData = [];
            $parameterOccurrence = [];
            foreach($csv as $line){
                $inputData[] = $line;
            }
                
            fclose($handle);

            $parameterSetCount = null;
            if(isset($inputData[0]['parameter set'])){
                $parameterSetCount = max(array_column($inputData, 'parameter set'));
            }
            $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

            //send to validation first
            $curl = curl_init();

            $type = $file->type;
            $data = array( 
                'file' => curl_file_create($tmpFileName, $type,$tmpFileName)
            ); 

            if($parameterSetCount != null && $parameterSetCount > 0){
                $designArray = array_fill(0, $parameterSetCount, $experimentDesign);
                $designVarStr = implode(",", $designArray);
                $designStr = "&experimentDesign=".rawurlencode($designVarStr);
            } else {
                $designStr = "&experimentDesign=".rawurlencode($experimentDesign);
            }
           
            curl_setopt_array($curl, [
                CURLOPT_URL => getenv('BA_API2_URL')."v1/designs/validate?experimentDbId=".$id.$designStr,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true, 
                CURLOPT_VERBOSE => 1, 
                CURLOPT_POST => 1, 
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer ". $accessToken,
                    "content-type: multipart/form-data"
                ],  
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                $error['error'] = $err;
                return $error;
            } else {
                $response = json_decode($response, true);

                if(isset($response['result']['data'][0]) && $response['metadata']['additional']['isValid']){
                    $experimentDesign = strtolower($experimentDesign);
                    //copy file to output folder with the correct names
                    $csv = new \app\components\CsvReader($tmpFileName);
                    $csv->setHasHeader(true);

                    $headers = $csv->getHeader();

                    //required headers per design
                    $reqHeadersPerDesign = [
                        'rcbd' => [
                            'headers' => ['occurrence', 'plot_number', 'replicate', 'entry_id', 'field_row', 'field_col'],
                            'optional' => ['field_row', 'field_col', 'remark']
                        ],
                        'augmented rcbd'=> [
                            'headers' => ['occurrence', 'plot_number', 'block','entry_type', 'entry_id', 'field_row', 'field_col'],
                            'optional' => ['field_row', 'field_col', 'remark']
                        ],
                        'partially replicated' => [
                            'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate'],
                            'optional' => ['field_row', 'field_col', 'remark']
                        ],
                        'alpha-lattice' => [
                            'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate', 'block'],
                            'optional' => ['field_row', 'field_col', 'remark']
                        ],
                        'unspecified' => [
                            'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate', 'block'],
                            'optional' => ['field_row', 'field_col','entry_type','replicate', 'block', 'remark']
                        ],
                        'row-column' => [
                            'headers' => ['occurrence', 'plot_number', 'entry_id', 'field_row', 'field_col', 'colblock', 'rowblock'],
                            'optional' => ['field_row', 'field_col', 'remark']
                        ]
                    ];
                    foreach($csv as $line){
                        $headers = array_keys($line);
                        break;
                    }

                    $finalList = $reqHeadersPerDesign[$experimentDesign]['headers'];
                    $optionalFields = $reqHeadersPerDesign[$experimentDesign]['optional'];

                    $finalHeaders = [];
                    foreach($finalList as $val){
                        $finalHeaders[$val] = null;
                    }

                    if($experimentDesign == 'unspecified'){
                       //allowed naming of headers in file
                        $mappedHeaders = [
                            "occurrence" => ['exp', 'trial', 'occurrence no'],
                            "plot_number"=>['plot', 'plotnum', 'plot no'],
                            "rep"=>['replicate', 'replication'],
                            "block"=>['block', 'blk', 'block no'],
                            "entry_id"=>['entry', 'treatment', 'entry_number', 'entry no'],
                            "entry_type" => ["entry_type", 'entry type'],
                            "field_row" => ['fieldrow', 'row', 'y', 'design y'],
                            "field_col" => ['fieldcol', 'column', 'col', 'x', 'design x'],
                            "remark" => ['remark']
                        ];
                    } else {
                        //allowed naming of headers in file
                        $mappedHeaders = [
                            "occurrence" => ['exp', 'trial', 'occurrence no'],
                            "plot_number"=>['plot', 'plotnum', 'plot no'],
                            "rep"=>['replicate', 'replication'],
                            "block"=>['block', 'blk', 'block no'],
                            "entry_id"=>['entry', 'treatment', 'entry_number', 'entry no'],
                            "entry_type" => ["entry_type", 'entry type'],
                            "field_row" => ['fieldrow', 'row', 'y', 'design y'],
                            "field_col" => ['fieldcol', 'column', 'col', 'x', 'design x'],
                            "colblock" => ['colblock', 'col block no'],
                            "rowblock" => ['rowblock', 'row block no'],
                            "remark" => ['remark']
                        ];
                    }
                    
                    $withLayout = false;
                    $orderedHeaders = [];
                    $errorHeaders = [];
                    
                    foreach($headers as $index){
                        foreach($mappedHeaders as $key => $value){
                            $inAllowedHeaders = false;
                            $addHeader = '';

                            if(in_array(strtolower($index), $value)){
                                $finalHeaders[$key] = $index;
                                $addHeader = $key;
                                $inAllowedHeaders = true;
                                break;
                            }
                        }

                        if($inAllowedHeaders){
                            $orderedHeaders[] = $addHeader;
                        } else {
                            $orderedHeaders[] = strtolower($index);
                        }
                    }

                    //Copy if successfully validated
                    $csv->setHeader($orderedHeaders);
                    $rawFileName = \Yii::$app->security->generateRandomString();
                    $origTmpFileName = $rawFileName . '.' . $file->extension;
                    
                    //initial creation of file with headers updated
                    $root=Yii::getAlias('@webroot').'/analytics/output/'.$origTmpFileName;
                    if(file_exists($root)){
                        chmod($root, 777);
                        unlink($root);
                    }
                    $handle = fopen($root, 'x');
                    $headerFlag = 0;

                    $inputData = [];
                    foreach($csv as $line){
                        if($headerFlag == 0){
                            fputcsv($handle, $orderedHeaders,',', '"');
                            $headerFlag = 1;
                        }
                        fputcsv($handle, $line, ',', '"');
                        $inputData[] = $line;
                    }
                    
                    fclose($handle);
                    
                    if($occurrenceDbIds == null){
                        $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"equals $id"]);
                    } else {
                        $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'occurrence', 'occurrenceApplied'=>"equals ".implode(";", $occurrenceDbIds)]);
                    }
                    if($planTemplateArray['totalCount'] > 0){
                        $experimentJson = $planTemplateArray['data'][0];
                        $uiInfo = json_decode($experimentJson['mandatoryInfo'], true);
                        $designId = $uiInfo['design_id'];
                        $design = $uiInfo['design_name'];
                    } 
                    //preview information if successful
                    return [
                        'finalHeaders' => $finalHeaders,
                        'information' => [
                            'totalOccurrences' => [
                                'label' => 'No. of Occurrences',
                                'value' => max(array_column($inputData, 'occurrence'))
                            ],
                            'filename' => [
                                'label' => 'Uploaded file name',
                                'value' => $oldFileName
                            ],
                            'design_id' => $designId,
                            'design_name' => $design
                        ],
                        'success' => true,
                        'fileName' => $rawFileName                    
                    ];
                } else {
                  
                    $error['error'] = 'Encountered problems during validation. Kindly check the uploaded file.';

                    if(isset($response['metadata']['additional']['failedRules'])){
                            
                        $rules = $response['metadata']['additional']['failedRules'];
                        $error['error'] = "Uploaded file failed in the following validation rules: <br />";
                        
                        foreach($rules as $rule){
                            foreach($rule as $r){
                                $msg = $r['description'];
                                unset($r['description']);
                                $errStr = [];
                                foreach($r as $key=>$e){
                                    $errStr[] = $key.":".json_encode($e, true);
                                }
                                $error['error'] .= "<b>&bull; ".$msg."</b> (".implode(", ", $errStr).")<br />";
                            }
                        }
                    } else if(isset($response['detail'])){

                        $rules = $response['detail'];
                        $error['error'] = "Uploaded file failed in the following validation rules: <br />";
                        
                        foreach($rules as $rule){
                            $msg = $rule['msg'];
                            unset($rule['msg']);
                            $errStr = [];
                            foreach($rule as $key=>$e){
                                $errStr[] = $key.":".json_encode($e, true);
                            }
                            $error['error'] .= "<b>&bull; ".$msg."</b> (".implode(", ", $errStr).")<br />";
                        }
                    }
                    return $error;
                }
            }
        } catch (\Exception $e) {

            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }
            
            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Validate file to be uploaded for design array
     *
     * @param String $fileName name of the uploaded file
     * @param Integer $id record id of the experiment
     */
    public function validateDesignArray($fileName, $id){
        $error['success'] = false;
        //get entry information 
        $totalEntriesRecords = $this->entry->searchAll(['fields'=>'experiment.id as experimentDbId|entry.entry_number as entryNumber','experimentDbId'=>"equals ".$id]);
        $totalEntries = $totalEntriesRecords['totalCount'];
        
        $actualEntries = array_column($totalEntriesRecords['data'], 'entryNumber');

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads';
            
            $file = UploadedFile::getInstanceByName($fileName);
            $oldFileName =  $file->name;
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false, 
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $rawFileName = \Yii::$app->security->generateRandomString();
            $origTmpFileName = $rawFileName . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $origTmpFileName;

            $file->saveAs($tmpFileName);

            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);
            $headers = $csv->getHeader();

            $mappedHeaders = [
                "occurrence" => ['exp', 'trial'],
                "plot_number"=>['plot', 'plotnum'],
                "replicate"=>['rep'],
                "block"=>['block'],
                "entry_number"=>['entry', 'treatment'],
                "field_row" => ['fieldrow'],
                "field_col" => ['fieldcol'],
            ];

            $finalHeaders = [
                "occurrence" => null,
                "plot_number"=>null,
                "replicate"=> null,
                "block"=>null,
                "entry_number"=>null,
                "field_row" => 'fieldrow',
                "field_col" =>'fieldcol',
            ];

            $noChecking = ['rep', 'block', 'fieldrow', 'fieldcol' ];
            $optionalFields  = ['fieldrow', 'fieldcol'];
            $withLayout = false;
            $orderedHeaders = [];
            $errorHeaders = [];
            foreach($csv as $line){
              
                $headers = array_keys($line);
                
                foreach($headers as $index){
                    //check occurrence header
                    $flagHeader = true;
                    if(in_array(strtolower($index), $mappedHeaders['occurrence'])){ 
                        $finalHeaders['occurrence'] = $index;
                        $flagHeader = false;
                        $orderedHeaders[] = 'occurrence';
                    }
                    //check plot number header
                    else if(in_array(strtolower($index), $mappedHeaders['plot_number'])){ 
                        $finalHeaders['plot_number'] = $index;
                        $flagHeader = false;
                        $orderedHeaders[] = 'plot_number';
                    }
                    //check entry number header
                    else if(in_array(strtolower($index), $mappedHeaders['entry_number'])){ 
                        $finalHeaders['entry_number'] = $index;
                        $flagHeader = false;
                        $orderedHeaders[] = 'entry_id';
                    }

                    //check replicate header
                    else if(in_array(strtolower($index), $mappedHeaders['replicate'])){
                        $orderedHeaders[] = 'replicate';
                        $finalHeaders['replicate'] = $index;
                    }
                    //check block header
                    else if(in_array(strtolower($index), $mappedHeaders['block'])){ 
                        $orderedHeaders[] = 'block';
                        $finalHeaders['block'] = $index;
                    }
                    //check field_row header
                    else if(in_array(strtolower($index), $mappedHeaders['field_row'])){ 
                        $orderedHeaders[] = 'field_row';
                    }
                    //check field_col header
                    else if(in_array(strtolower($index), $mappedHeaders['field_col'])){ 
                        $orderedHeaders[] = 'field_col';
                    }
                    //for those not in the required columns
                    else {
                        $orderedHeaders[] = strtolower($index);
                    }

                    //check if there's field_row and field_col
                    if(in_array(strtolower($index), $noChecking)){
                        $flagHeader = false;
                    }

                    if(in_array(strtolower($index), $optionalFields)){ 
                        $idx = array_search($index, $optionalFields);
                        unset($optionalFields[$idx]);
                        $optionalFields = array_values($optionalFields);
                    }
                }
                //check if headers are all there
                if(!empty($optionalFields) && count($optionalFields) == 1){
                    $errorHeaders[] = 'Missing header for FIELD_ROW or FIELD_COL at line 1.';
                } else if(empty($optionalFields)){
                    $withLayout = true;
                }

                //check if all required headers are in place
                foreach($finalHeaders as $key=>$value){
                    if($value == null){
                        $errorHeaders[] = 'Missing header for '.strtoupper($key).' at line 1.';
                    }
                }
                break;
            }

            //check if there are errors in the headers
            if(!empty($errorHeaders)){
                $error['error'] = implode("<br>", $errorHeaders);
                return $error;
            }

            $count = 2;
            $filePlotIds = [];
            $occurrenceHeader = '';
            $plotIdHeader = '';
            $plotIdField = 'plot_id';
            $occField = 'occurrence_id';
            $fieldArr = [];
            $fieldTemp = [];
            $finalValues = [];
            $occEntryRepBlockPlot = [];
            $occRowCol = [];
            $occBlock = [];
            $repBlockEqualCnt = 2;
            $repPerOccurrence = [];
            $occurrencePrev = '';
            $occurrenceLine = 2;
            $firstOccRep = [];
            $firstFlag = false;
            $entryPerRep = [];
            $blocksInRep = [];

            //preview information
            $occurrenceVal = [];
            $entriesVal = [];
            $plotsVal = [];
            $blocksVal = [];
            $repsVal = [];

            foreach ($csv as $line) {

                $allValid = true;
                foreach ($finalHeaders as $key => $value) {
                    
                    if(!$withLayout && in_array($key, ['field_row', 'field_col']) ){
                        continue;
                    }
                    if (isset($line[strtolower($value)]) && empty($line[strtolower($value)])) {
                        $allValid = false;
                    }

                    // if value is not numeric and not float
                    if ( 
                        $key != 'entry_number' &&
                        isset($line[strtolower($value)]) &&
                        !empty($line[strtolower($value)]) && 
                        !is_numeric($line[strtolower($value)]) ||
                        strpos($line[strtolower($value)], '.')
                    ) {
                        $error['error'] = 'Invalid value for ' . $value . ' at line '. $count . '.';
                        return $error;
                    }

                    if(!in_array($key, ['field_row', 'field_col']) && empty($line[strtolower($value)])){
                        $error['error'] = 'Empty value for ' . $value . ' at line '. $count . '.';
                        return $error;
                    }
                }

                if(!in_array($line[$finalHeaders['entry_number']], $actualEntries)){
                    $error['error'] = 'Entry number value '.$line[$finalHeaders['entry_number']].' at line '. $count . ' is not in the entry list.';
                    return $error;
                }
                //form combination
                if($withLayout){
                    $occRowColCurrent = $line[$finalHeaders['occurrence']].'-'.$line[$finalHeaders['field_row']].'-'.$line[$finalHeaders['field_col']];
                    // check if the FIELD combination does not exist yet
                    if(in_array($occRowColCurrent, $occRowCol)){
                        $error['error'] = 'Duplicate FIELD ROW and FIELD COL values at line '. $count . '.';
                        return $error;
                    }
                    $occRowCol[] = $occRowColCurrent;

                    if(empty($line[$finalHeaders['field_row']])){
                        $error['error'] = 'Empty value for fieldrow at line '. $count . '.';
                        return $error;
                    }
                    if(empty($line[$finalHeaders['field_col']])){
                        $error['error'] = 'Empty value for fieldcol at line '. $count . '.';
                        return $error;
                    }
                }

                $occEntryRepBlockPlotCurr = $line[$finalHeaders['occurrence']].'-'.$line[$finalHeaders['plot_number']];
                // check if the occurrence and plot number combination does not exist yet
                if(in_array($occEntryRepBlockPlotCurr,$occEntryRepBlockPlot)){
                    $error['error'] = 'Duplicate combination of OCCURRENCE and PLOT NUMBER value at line '. $count . '.';
                    return $error;
                }
                $occEntryRepBlockPlot[] = $occEntryRepBlockPlotCurr;
                
                // check if the rep and block does not have the same value within occurrence
                if($line[$finalHeaders['replicate']] == $line[$finalHeaders['block']]){
                    if($occurrencePrev != '' & ($occurrencePrev != $line[$finalHeaders['occurrence']])){
                        //check if equal
                        if($repBlockEqualCnt == $occurrenceLine){
                            $error['error'] = 'Same values for REP and BLOCK within OCCURRENCE '. $occurrencePrev . '.';
                            return $error;
                        }
                        $repBlockEqualCnt = 0;
                    }
                    $repBlockEqualCnt++;
                }

                if($occurrencePrev != '' & ($occurrencePrev != $line[$finalHeaders['occurrence']])){
                    $occurrenceLine = 0;
                    $firstFlag = true;

                    //check entries per occurrence
                    if(!empty($entryPerRep)){
                        foreach($entryPerRep as $idx => $perRep){
                            if(count($perRep) != $totalEntries){
                                $error['error'] = 'The no. of entries did not match for OCCURRENCE '. $occurrencePrev.'.';
                                return $error;
                            }
                        }
                        
                    }
                    $entryPerRep = [];

                    //check rep per occurrence
                    if(!empty($repPerOccurrence) && count(array_unique($repPerOccurrence)) != count(array_unique($firstOccRep))){
                        $error['error'] = 'The no. of replicates did not match for OCCURRENCE '. $line[$finalHeaders['occurrence']] . '.';
                        return $error;
                    }
                    $repPerOccurrence = [];

                    //check if block values are not equal to 1 and if the max value is not equal to totalEntries
                    if(!empty($occBlock)){
                        $newOccBlock = array_unique($occBlock);
                        if(count($newOccBlock) == 1 && $newOccBlock[0] == 1){
                            $error['error'] = 'The block value should not all be equal to 1 for OCCURRENCE '. $occurrencePrev . '.';
                            return $error;
                        } 
                        foreach($blocksInRep as $bir){
                            if(count(array_unique($bir)) == $totalEntries){
                                $error['error'] = 'The no. of blocks in a rep should not be equal to total entries for OCCURRENCE '. $occurrencePrev . '.';
                                return $error;
                            }
                        }
                    }
                    $occBlock = [];


                }

                if ($allValid) {

                    $uploadedData[] = $line;
                }

                $count++;
                $occurrenceLine++;
                $occurrencePrev = $line[$finalHeaders['occurrence']];
                $repPerOccurrence[] = $line[$finalHeaders['replicate']];
                $occBlock[] = $line[$finalHeaders['block']];
                if(!$firstFlag){
                    $firstOccRep[] = $line[$finalHeaders['replicate']];
                }
                if(empty($entryPerRep[$line[$finalHeaders['replicate']]])){
                    $entryPerRep[$line[$finalHeaders['replicate']]][] =  $line[$finalHeaders['entry_number']];
                } else if(!in_array($line[$finalHeaders['entry_number']], $entryPerRep[$line[$finalHeaders['replicate']]])){
                    $entryPerRep[$line[$finalHeaders['replicate']]][] =  $line[$finalHeaders['entry_number']];
                }

                $blocksInRep[$line[$finalHeaders['replicate']]][] = $line[$finalHeaders['block']];
                //preview information
                $occurrenceVal[] = $line[$finalHeaders['occurrence']];
                $entriesVal[] = $line[$finalHeaders['entry_number']];
                $plotsVal[] = $line[$finalHeaders['plot_number']];
                $repsVal[] = $line[$finalHeaders['replicate']];
                $blocksVal[] = $line[$finalHeaders['block']];
            }

            // check if the rep and block does not have the same value within occurrence
            if($repBlockEqualCnt == $occurrenceLine){
                $error['error'] = 'Same values for REP and BLOCK within OCCURRENCE '. $occurrencePrev . '.';
                return $error;
            }

            //check entries per occurrence
            if(!empty($entryPerRep)){
                foreach($entryPerRep as $idx => $perRep){
                    if(count($perRep) != $totalEntries){
                        $error['error'] = 'The no. of entries does not match for OCCURRENCE '. $occurrencePrev . ' REP '.$idx.'.';
                        return $error;
                    }
                }
                
            }
            //check if block values are not equal to 1 and if the max value is not equal to totalEntries
            if(!empty($occBlock)){
                $newOccBlock = array_unique($occBlock);
                if(count($newOccBlock) == 1 && $newOccBlock[0] == 1){
                    $error['error'] = 'The block value should not all be equal to 1 for OCCURRENCE '. $occurrencePrev . '.';
                    return $error;
                } 
            }
            //check rep per occurrence
            if(count(array_unique($repPerOccurrence)) != count(array_unique($firstOccRep))){
                $error['error'] = 'The no. of replicates did not match for OCCURRENCE '. $line[$finalHeaders['occurrence']] . '.';
                return $error;
            }

            //Copy if successfully validated
            $csv->setHeader($orderedHeaders);
            
            $fileName = $rawFileName;
            //initial creation of file
            $root=Yii::getAlias('@webroot').'/analytics/output/'.$fileName.'.csv';

            if(file_exists($root)){
                chmod($root, 777);
                unlink($root);
            }
            $handle = fopen($root, 'x');
            $headerFlag = 0;
            foreach($csv as $line){
                if($headerFlag == 0){
                    fputcsv($handle, $orderedHeaders,',', '"');
                    $headerFlag = 1;
                }
                fputcsv($handle, $line, ',', '"');
            }
            
            fclose($handle);

            $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"equals $id"]);

            if($planTemplateArray['totalCount'] > 0){
                $experimentJson = $planTemplateArray['data'][0];
                $uiInfo = json_decode($experimentJson['mandatoryInfo'], true);
                $designId = $uiInfo['design_id'];
                $design = $uiInfo['design_name'];

            } 

            //preview information if successful
            return [
                'finalHeaders' => $finalHeaders,
                'information' => [
                    'totalOccurrences' => [
                        'label' => 'No. of Occurrences',
                        'value' => count(array_unique($occurrenceVal))
                    ],
                    'totalEntries' => [
                        'label' => 'No. of Entries',
                        'value' => count(array_unique($entriesVal))
                    ],
                    'totalBlocks' => [
                        'label' => 'No. of Blocks',
                        'value' => count(array_unique($blocksVal))
                    ],
                    'totalPlots' => [
                        'label' => 'No. of Plots',
                        'value' => count(array_unique($plotsVal))
                    ],
                    'withLayout' => [
                        'label' => 'With Layout',
                        'value' => $withLayout
                    ],
                    'filename' => [
                        'label' => 'Uploaded file name',
                        'value' => $oldFileName
                    ],
                    'design_id' => $designId,
                    'design_name' => $design
                ],
                'success' => true,
                'fileName' => $fileName,

            ];
        } catch (\Exception $e) {
            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }
            
            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Pre-Validate file to be uploaded for planting arrangement
     *
     * @param String $fileName name of the uploaded file
     * @param String $experimentType type of the experiment
     */
    public function preValidatePaFile($fileName, $experimentType){
        $error['success'] = false;

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads';
            $file = UploadedFile::getInstanceByName($fileName);
            $oldFileName = $file->name;

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $rawFileName = \Yii::$app->security->generateRandomString();
            $tmpFileName = $rawFileName . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);

            //copy file to output folder with the correct names
            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            $headers = $csv->getHeader();

            //required headers per experiment type
            $experimentType = $experimentType == 'Observation'? 'observation' : 'others';
            $reqHeadersPerExpType = [
                'observation' => [
                    'headers' => ['entry_number', 'plot_number', 'germplasm_code', 'entry_type', 'replicate', 'design_x', 'design_y'],
                    'optional' => ['germplasm_code', 'entry_type', 'replicate', 'design_x', 'design_y']
                ],
                'others'=> [
                    'headers' => ['entry_number', 'plot_number', 'germplasm_code', 'parent_role', 'replicate', 'design_x', 'design_y'],
                    'optional' => ['germplasm_code', 'parent_role', 'replicate', 'design_x', 'design_y']
                ]
            ];
            foreach($csv as $line){
                $headers = array_keys($line);
                break;
            }

            $finalList = $reqHeadersPerExpType[$experimentType]['headers'];
            $optionalFields = $reqHeadersPerExpType[$experimentType]['optional'];

            $finalHeaders = [];
            foreach($finalList as $val){
                $finalHeaders[$val] = null;
            }

            //allowed naming of headers in file
            $mappedHeaders = [
                'entry_number' => ['entry', 'entrynum', 'entryno'],
                'plot_number' => ['plot', 'plotnum', 'plotno'],
                'germplasm_code' => ['germplasm', 'germplasmcode', 'germplasm_code'],
                'entry_type' => ['entrytype', 'entry_type'],
                'parent_role' => ['parentrole', 'parent_role'],
                'replicate' => ['rep', 'replicate', 'replication'],
                'design_x' => ['designx', 'design_x', 'x'],
                'design_y' => ['designy', 'design_y', 'y'],
            ];

            $orderedHeaders = [];
            $errorHeaders = [];

            foreach($headers as $index){
                $inAllowedHeaders = false;
                $addHeader = '';
                foreach($mappedHeaders as $key => $value){
                    if(in_array(strtolower($index), $value)){
                        $finalHeaders[$key] = $index;
                        $addHeader = $key;
                        unset($mappedHeaders[$key]);
                        $inAllowedHeaders = true;
                        break;
                    } else if(in_array(strtolower($index), $optionalFields)){   //for those not in the required columns
                        $finalHeaders[$key] = $index;
                        $addHeader = $key;
                        unset($mappedHeaders[$key]);
                        $inAllowedHeaders = true;
                        break;
                    }
                }
                if($inAllowedHeaders){
                    $orderedHeaders[] = $addHeader;
                } else {
                    $orderedHeaders[] = strtolower($index);
                }
            }

            //Copy if successfully validated
            $csv->setHeader($orderedHeaders);

            $headerFlag = 0;
            $inputData = [];
            foreach($csv as $line){
                if($headerFlag == 0){
                    $headerFlag = 1;
                }
                $inputData[] = $line;
            }

            //preview information if successful
            return [
                'finalHeaders' => $finalHeaders,
                'information' => [
                    'totalPlots' => [
                        'label' => 'No. of Plots',
                        'value' => max(array_column($inputData, 'plot_number'))
                    ],
                    'filename' => [
                        'label' => 'Uploaded file name',
                        'value' => $oldFileName
                    ],
                    'design_id' => 1,
                    'design_name' => $experimentType
                ],
                'success' => true,
                'fileName' => $rawFileName
            ];
        } catch (\Exception $e) {
            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }

            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Validate file to be uploaded for planting arangement
     *
     * @param String $fileName name of the uploaded file
     * @param Object $preValidationResult object from prevalidation
     * @param Integer $id record id of the experiment
     */
    public function validatePaFile($fileName, $preValidationResult, $id) {
        $error['success'] = false;

        //get entry information
        $totalEntriesRecords = $this->entry->searchAll([
            'fields'=>'experiment.id AS experimentDbId|entry.entry_number AS entryNumber|germplasm.germplasm_code AS germplasmCode|
                entry.entry_type AS entryType|entry.entry_role AS entryRole',
            'experimentDbId'=>"equals ".$id
        ]);
        $totalEntries = $totalEntriesRecords['totalCount'];

        $actualEntries = array_column($totalEntriesRecords['data'], 'entryNumber');

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads';

            $file = UploadedFile::getInstanceByName($fileName);
            $oldFileName =  $file->name;
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $origTmpFileName = $preValidationResult['fileName'] . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $origTmpFileName;

            $file->saveAs($tmpFileName);

            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);
            $headers = $csv->getHeader();

            $mappedHeaders = [
                'entry_number' => ['entry', 'entrynum', 'entry_number', 'entryno'],
                'plot_number' => ['plot', 'plotnum', 'plot_number', 'plotno'],
                'germplasm_code' => ['germplasm', 'germplasmcode', 'germplasm_code'],
                'entry_type' => ['entrytype', 'entry_type'],
                'parent_role' => ['parentrole', 'parent_role'],
                'replicate' => ['rep', 'replicate', 'replication'],
                'design_x' => ['designx', 'design_x', 'x'],
                'design_y' => ['designy', 'design_y', 'y'],
            ];

            $finalHeaders = [
                'entry_number' => null,
                'plot_number' => null,
                'germplasm_code' => 'germplasm_code',
                'entry_type' => 'entry_type',
                'parent_role' => 'parent_role',
                'replicate' => 'replicate',
                'design_x' => 'design_x',
                'design_y' => 'design_y',
            ];

            $optionalFields  = ['designx', 'designy'];
            $withLayout = false;
            $orderedHeaders = [];
            $errorHeaders = [];
            foreach($csv as $line){
                $headers = array_keys($line);

                // check if headers exist
                foreach($headers as $index){
                    // entry_number header
                    if(in_array(strtolower($index), $mappedHeaders['entry_number'])){
                        $finalHeaders['entry_number'] = $index;
                        $orderedHeaders[] = 'entry_number';
                    }
                    // plot number header
                    else if(in_array(strtolower($index), $mappedHeaders['plot_number'])){
                        $finalHeaders['plot_number'] = $index;
                        $orderedHeaders[] = 'plot_number';
                    }
                    // germplasm_code header
                    else if(in_array(strtolower($index), $mappedHeaders['germplasm_code'])){
                        $orderedHeaders[] = 'germplasm_code';
                    }
                    // entry_type header
                    else if(in_array(strtolower($index), $mappedHeaders['entry_type'])){
                        $orderedHeaders[] = 'entry_type';
                    }
                    // parent_role header
                    else if(in_array(strtolower($index), $mappedHeaders['parent_role'])){
                        $orderedHeaders[] = 'parent_role';
                    }
                    // replicate header
                    else if(in_array(strtolower($index), $mappedHeaders['replicate'])){
                        $orderedHeaders[] = 'replicate';
                    }
                    // design_x header
                    else if(in_array(strtolower($index), $mappedHeaders['design_x'])){
                        $orderedHeaders[] = 'design_x';
                    }
                    // design_y header
                    else if(in_array(strtolower($index), $mappedHeaders['design_y'])){
                        $orderedHeaders[] = 'design_y';
                    }
                    //for those not in the required columns
                    else {
                        $orderedHeaders[] = strtolower($index);
                    }

                    if(in_array(strtolower($index), $optionalFields)){
                        $idx = array_search($index, $optionalFields);
                        unset($optionalFields[$idx]);
                        $optionalFields = array_values($optionalFields);
                    }
                }

                //check if headers for both design_x and design_y exists at the same time because one cannot live without the other
                if(!empty($optionalFields) && count($optionalFields) == 1){
                    $errorHeaders[] = 'Missing header for DesignX or DesignY at line 1.';
                }

                //check if all required headers are in place
                foreach($finalHeaders as $key=>$value) {
                    if($value == null){
                        $errorHeaders[] = 'Missing header for '.strtoupper($key).' at line 1.';
                    }
                }
                break;
            }

            //check if there are errors in the headers
            if(!empty($errorHeaders)){
                $error['error'] = implode("<br>", $errorHeaders);
                return $error;
            }

            // Validation - per input row checking
            $count = 2;
            $uploadedData = [];
            $uploadedDesignXY = [];
            $uploadedRepEntry = [];
            $uploadedPlotnos = [];
            $actualEntryParentRole = array_map(function($entry) {
                return $entry['entryRole'] . '-' . $entry['entryNumber'];
            }, $totalEntriesRecords['data']);
            $actualEntryEntryType = array_map(function($entry) {
                return $entry['entryType'] . '-' . $entry['entryNumber'];
            }, $totalEntriesRecords['data']);
            foreach($csv as $line) {
                $data = $line;
                $error = true;

                // Check if numeric inputs are numeric
                if(is_numeric($data['entryno'])) $data['entryno'] = intval($data['entryno']);
                else return ['success'=>false, 'error'=>'Entry number value '.$line['entryno'].' at line '. $count . ' is not a number.'];

                if(is_numeric($data['plotno'])) $data['plotno'] = intval($data['plotno']);
                else return ['success'=>false, 'error'=>'Plot number value '.$line['plotno'].' at line '. $count . ' is not a number.'];

                if(isset($data['rep']) && $data['rep'] != '') {
                    if(is_numeric($data['rep'])) $data['rep'] = intval($data['rep']);
                    else return ['success'=>false, 'error'=>'Rep number value '.$line['rep'].' at line '. $count . ' is not a number.'];
                }

                if(isset($data['designx']) && $data['designx'] != '') {
                    if(is_numeric($data['designx'])) $data['designx'] = intval($data['designx']);
                    else return ['success'=>false, 'error'=>'DesignX value '.$line['designx'].' at line '. $count . ' is not a number.'];
                }

                if(isset($data['designy']) && $data['designy'] != '') {
                    if(is_numeric($data['designy'])) $data['designy'] = intval($data['designy']);
                    else return ['success'=>false, 'error'=>'DesignY value '.$line['designy'].' at line '. $count . ' is not a number.'];
                }

                // Check if entryno is in Entry List
                if(!in_array($data['entryno'], $actualEntries))
                    return ['success'=>false, 'error'=>'Entry number value '.$line['entryno'].' at line '. $count . ' is not in the entry list.'];

                // Check if duplicate design x and y combination (there should be no duplicate design x and y combination)
                if(isset($data['designx'], $data['designy']) && $data['designx'] != '' && $data['designy'] != '') {
                    $withLayout = true;
                    if(in_array($data['designx'].'-'.$data['designy'], $uploadedDesignXY))
                        return ['success'=>false, 'error'=>'Duplicate Design X and Design Y value: '.$line['designx'].' and '.$line['designy'].' at line '. $count . '.'];
                    else $uploadedDesignXY[] = $data['designx'].'-'.$data['designy'];
                }

                // Check if duplicate rep number for an entry (there should be no duplicate rep number for an entry)
                if(isset($data['rep']) && $data['rep'] != '') {
                    if(in_array($data['rep'].'-'.$data['entryno'], $uploadedRepEntry))
                        return ['success'=>false, 'error'=>'Duplicate Rep value '.$line['rep'].' for Entry Number '.$line['entryno'].' at line '. $count . '.'];
                    else $uploadedRepEntry[] = $data['rep'].'-'.$data['entryno'];
                }

                // Check if duplicate plot number (there should be no duplicate plot number)
                if(in_array($data['plotno'], $uploadedPlotnos))
                    return ['success'=>false, 'error'=>'Duplicate Plot number value '.$line['plotno'].' at line '. $count . '.'];
                else $uploadedPlotnos[] = $data['plotno'];

                // Check if Parent role and Entry number combination are correct (Parent role and Entry number combination should be correct)
                if(isset($data['parentrole']) && $data['parentrole'] != '') {
                    if(!in_array($data['parentrole'].'-'.$data['entryno'], $actualEntryParentRole))
                        return ['success'=>false, 'error'=>'Incorrect Parent Role value '.$line['parentrole'].' for Entry Number '.$line['entryno'].' at line '. $count . '.'];
                    else $actualEntryParentRole[] = $data['parentrole'].'-'.$data['entryno'];
                }

                // Check if Entry type and Entry number combination are correct (Entry type and Entry number combination should be correct)
                if(isset($data['entrytype']) && $data['entrytype'] != '') {
                    if(!in_array($data['entrytype'].'-'.$data['entryno'], $actualEntryEntryType))
                        return ['success'=>false, 'error'=>'Incorrect Entry Type value '.$line['entrytype'].' for Entry Number '.$line['entryno'].' at line '. $count . '.'];
                    else $actualEntryEntryType[] = $data['entrytype'].'-'.$data['entryno'];
                }

                // Add data
                foreach ($data as $key => $value) {
                    if($value == '') unset($data[$key]);    // prevent empty values from being added
                }
                $uploadedData[] = $data;
                $count++;
            }

            // All entries should be complete
            $uploadedEntryNos = array_column($uploadedData, 'entryno');
            $uploadedEntryNos = array_unique($uploadedEntryNos);
            $entriesNotUsed = array_diff($actualEntries,$uploadedEntryNos);
            if(!empty($entriesNotUsed))
                return ['success'=>false, 'error'=>'Missing entry numbers: '.implode(',', $entriesNotUsed).'.'];

            // All plot numbers should be complete
            $uploadedPlotNos = array_column($uploadedData, 'plotno');
            $uploadedPlotNos = array_unique($uploadedPlotNos);
            $maxPlotNo = count($uploadedPlotNos);
            for ($x=1; $x <= $maxPlotNo; $x++) {
                if(!in_array($x, $uploadedPlotNos)) {
                    return ['success'=>false, 'error'=>'Missing plot number value: '.$x.'.'];
                }
            }

            // If layout is included, all design x and design y should be complete (no spaces between plots)
            if($withLayout) {
                $uploadedLayout = array_map(function($data) {
                    $x = $data['designx'] ?? '';
                    $y = $data['designy'] ?? '';
                    return $x . '-' . $y;
                }, $uploadedData);
                $maxX = max(array_column($uploadedData, 'designx'));
                $maxY = max(array_column($uploadedData, 'designy'));
                for ($x=1; $x < $maxX; $x++) {
                    for ($y=1; $y < $maxY; $y++) {
                        if(!in_array("$x-$y", $uploadedLayout)) {
                            return ['success'=>false, 'error'=>'Missing Design X and Design Y value: '.$x.' and '.$y.'.'];
                        }
                    }
                }

                // Check the edges for inconsistencies if there is an empty plot between plots
                // Test for row
                $firstPlotExist = false;
                $secondChecker = false;
                $useSecondChecker = false;
                for ($x=1; $x < $maxX; $x++) {
                    if($x == 1) {
                        if(in_array("$x-$maxY", $uploadedLayout)) {
                            $firstPlotExist = true;
                            $secondChecker = false;
                        } else {
                            $firstPlotExist = false;
                            $secondChecker = true;
                        }
                        continue;
                    }

                    if($useSecondChecker) {
                        if((in_array("$x-$maxY", $uploadedLayout) && !$secondChecker)
                            || !in_array("$x-$maxY", $uploadedLayout) && $secondChecker) {
                                return ['success'=>false, 'error'=>'Missing Design X and Design Y value: '.--$x.' and '.$maxY.'.'];
                        }
                    } else {    // if value still exists (or not exists) from the beginning of the column
                        if((in_array("$x-$maxY", $uploadedLayout) && !$firstPlotExist)
                            || !in_array("$x-$maxY", $uploadedLayout) && $firstPlotExist) {
                            $useSecondChecker = true;
                        }
                    }
                }

                // Test for column
                $firstPlotExist = false;
                $secondChecker = false;
                $useSecondChecker = false;
                for ($y=1; $y < $maxY; $y++) {
                    if($y == 1) {
                        if(in_array("$maxX-$y", $uploadedLayout)) {
                            $firstPlotExist = true;
                            $secondChecker = false;
                        } else {
                            $firstPlotExist = false;
                            $secondChecker = true;
                        }
                        continue;
                    }

                    if($useSecondChecker) {
                        if((in_array("$maxX-$y", $uploadedLayout) && !$secondChecker)
                            || !in_array("$maxX-$y", $uploadedLayout) && $secondChecker) {
                                return ['success'=>false, 'error'=>'Missing Design X and Design Y value: '.$maxX.' and '.--$y.'.'];
                        }
                    } else {    // if value still exists (or not exists) from the beginning of the row
                        if((in_array("$maxX-$y", $uploadedLayout) && !$firstPlotExist)
                            || !in_array("$maxX-$y", $uploadedLayout) && $firstPlotExist) {
                            $useSecondChecker = true;
                        }
                    }
                }
            }

            // Preview information if successful
            return [
                'finalHeaders' => $finalHeaders,
                'information' => [
                    'totalEntries' => [
                        'label' => 'No. of Entries',
                        'value' => count(array_unique($actualEntries))
                    ],
                    'totalPlots' => [
                        'label' => 'No. of Plots',
                        'value' => count(array_unique($uploadedPlotnos))
                    ],
                    'withLayout' => [
                        'label' => 'With Layout',
                        'value' => $withLayout
                    ],
                    'filename' => [
                        'label' => 'Uploaded file name',
                        'value' => $oldFileName
                    ]
                ],
                'success' => true,
                'fileName' => $fileName,
                'fileContent' => $uploadedData
            ];
        } catch (\Exception $e) {
            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }

            $error['error'] = $e->getMessage();
            return $error;
        }
    }

    /**
     * Get upload information
     * 
     * @param Integer $id record id of the experiment
     * @param Integer $occurrenceCount total no. of occurrence
     * @param Array $occurrenceDbIds array of occurrence record ids
     */
    public function getUploadInformation($id, $occurrenceCount, $occurrenceDbIds=null){
        $totalEntriesRecord = $this->entry->searchAll(['experimentDbId'=>"equals $id"], 'sort=entryNumber:ASC');
        $metadata = [];
        //Get entry_ids
        $metadata['entryIds'] = array_column($totalEntriesRecord['data'], 'entryDbId');

        $experiment = $this->experiment->getOne($id);
        
        if($occurrenceDbIds == null){

            //generate occurrences
            $occurrenceExperiment = $this->occurrence->searchAll(['experimentDbId'=>"equals ".$id], 'sort=occurrenceName:DESC&limit=1');

            if($occurrenceExperiment['totalCount'] < $occurrenceCount){

                $newCount = $occurrenceCount - $occurrenceExperiment['totalCount'];
                $totalCount = $occurrenceExperiment['totalCount'];
                
                for($i=0; $i<$newCount; $i++){
                    $occNum = $totalCount + ($i+1);
                    $occName = ($occNum < 10) ? '0'.$occNum : $occNum; 
                    $occurrenceName = $experiment['data']['experimentName'].'#OCC-'.$occName;//to be replaced with occurrence browser
                    $occurrenceStatus = 'draft';
                    $records = [
                        'occurrenceStatus' => $occurrenceStatus,
                        'experimentDbId' => "$id"
                    ];
                    $requestData['records'][] = $records;
                }
                
                //Debug occurrence creation
                $insertedRecord = $this->occurrence->create($requestData);

            } else if($occurrenceExperiment['totalCount'] > $occurrenceCount){

                $toBeDeletedCnt = $occurrenceExperiment['totalCount'] - $occurrenceCount;
                $occurrencesIds = array_column($occurrenceExperiment['data'], 'occurrenceDbId');
                $toBeDeleted = array();

                for($i=0; $i<$toBeDeletedCnt; $i++){
                    $toBeDeleted[] = $occurrencesIds[$i];
                }

                //delete excess occurrences
                $deleteRecords =$this->occurrence->deleteMany($toBeDeleted);
            }
           
            $occurrenceExperiment = $this->occurrence->searchAll(['experimentDbId'=>"equals ".$id], 'sort=occurrenceDbId:ASC');
            
        } else {
            $occurrenceIds = explode("|", $occurrenceDbIds);
            $occurrenceExperiment = $this->occurrence->searchAll(['occurrenceDbId'=>"equals ".implode("|equals ", $occurrenceIds)], 'sort=occurrenceDbId:ASC');
        }
        
        $metadata['occurrenceIds'] = array_column($occurrenceExperiment['data'], 'occurrenceDbId');

        return $metadata;
    }

    /** 
     * Create plan template records
     * 
     * @param Integer $id record id of the experiment
     * @param Integer $designId record id of the design
     * @param Integer $design name of the design
     * @param Integer $information array containing information about the uploaded file
    */
    public function createPlanTemplate($id, $designId, $design, $information){

        $parameterArray = $this->experimentModel->getJsonConfig($designId, 'ui');
        
        //generate template json for selected design
        $designRecord = $this->variable->getVariableByAbbrev('DESIGN');
        $designVariable = $designRecord['variableDbId'];
        $information['design_id'] = $designId;
        $information['design_name'] = $design;
        //get scale values for design
        $scaleRecords = $this->scale->getVariableScale($designVariable)['scaleValues'];
        $scaleValues = array_column($scaleRecords, 'value');
        
        $scaleValuesKey = array_map('strtoupper', $scaleValues);

        $searchword = trim($design);
        $matches = array_filter($scaleValues, function($var) use ($searchword) { return preg_match("/($searchword)/i", $var); });

        $matches = array_values($matches);
        $experimentDesign = $matches[0];
        $userModel = new User();
        $userId = $userModel->getUserId();

        //Update experiment design
        $this->experiment->updateOne($id, ['experimentDesignType'=>$experimentDesign]);
        //check if there's existing json input for the experiment design
        //POST search plan-templates
        $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"$id"]);
        $model = $this->experiment->getExperiment($id);
        
        if($planTemplateArray['totalCount'] == 0){

            //create plan template record
            $experimentJson = [];
            $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$model['experimentName']))).'_'.$model['experimentDbId'];
            $experimentJson['entityType'] = "experiment";
            $experimentJson['entityDbId'] = $id;

            //create record in experiment data
            $experimentJson['design'] = $experimentDesign;
            $programDbId = $model['programDbId'];
            $experimentJson['programDbId'] = "$programDbId";
            $experimentJson['status'] = 'upload in progress';
            $experimentJson['mandatoryInfo'] = json_encode($information);
            $experimentJson['entryCount'] = "0";
            $experimentJson['plotCount'] = "0";
            $experimentJson['checkCount'] = "0";
            $experimentJson['repCount'] = "0";
            $experimentJson['tempFinalCabinetDbId'] = "0";
            
            $planTemplateId = $this->planTemplate->createPlanTemplate($experimentJson);
            return $planTemplateId;

        } else {
            $experimentJson = $planTemplateArray['data'][0];

            //update plan template
            $experimentJsonTemp = array();

            $experimentJsonTemp['design'] = $experimentDesign;
            $experimentJsonTemp['mandatoryInfo'] = json_encode($information);
            $experimentJsonTemp['status'] = 'uploading in progress';

            // Update experiment design
            $this->planTemplate->updateOne($experimentJson['planTemplateDbId'], $experimentJsonTemp);
            return $experimentJson['planTemplateDbId'];
        }
    }

     /** 
     * Create plan template records
     * 
     * @param Integer $id record id of the experiment
     * @param Array $information array containing information about the uploaded file
     * @param Array $uploadInfo array containing upload information
     * @param String $paramDesignstr designs for each parameter
    */
    public function createPlanTemplateForParamSet($id, $information, $uploadInfo, $paramDesignstr){

        //check if there's existing plan template for the experiment design, delete if there's existing
        //POST search plan-templates
        $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"$id"]);

        $model = $this->experiment->getExperiment($id);
        if($planTemplateArray['totalCount'] > 0){
            $planTemplateIds = array_column($planTemplateArray['data'], 'planTemplateDbId');
            $this->planTemplate->deleteMany($planTemplateIds);
        }
        
        //create new records every upload
        $paramDesign = explode(',', $paramDesignstr);
        $paramCount = 0;        

        $newPlanTemplateIds = [];
        
        $occurrenceCount=0;
        foreach($information['parameterOccurrence'] as $numOccurrence){    
            $experimentJson = [];
            $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$model['experimentName']))).'_'.$model['experimentDbId'].'_'.($paramCount+1);
            $experimentJson['entityType'] = "parameter_sets";
            $experimentJson['entityDbId'] = $id;

            //create record in experiment data
            $experimentJson['design'] = $paramDesign[$paramCount++];
            $programDbId = $model['programDbId'];
            $experimentJson['programDbId'] = "$programDbId";
            $experimentJson['status'] = 'upload in progress';

            $paramSetOccIds = [];
            for($i=0; $i<count($numOccurrence); $i++){
                $paramSetOccIds[] = $uploadInfo['occurrenceIds'][$occurrenceCount++];
            }
            $information['design_ui']['occurrenceDbIds'] = $paramSetOccIds;
            $experimentJson['mandatoryInfo'] = json_encode($information);
            $experimentJson['entryCount'] = "0";
            $experimentJson['plotCount'] = "0";
            $experimentJson['checkCount'] = "0";
            $experimentJson['repCount'] = "0";
            $experimentJson['tempFinalCabinetDbId'] = "0";
            
            $planTemplateId = $this->planTemplate->createPlanTemplate($experimentJson);

            //update occurrences
            $this->occurrence->updateMany($paramSetOccIds, ["occurrenceDesignType"=>$experimentJson['design']]);
            $newPlanTemplateIds[] = $planTemplateId;
        }

        return $newPlanTemplateIds;
    }

    /**
     * Pre-validate design array file
     *
     * @param String $fileName name of the uploaded file
     * @param Integer $id record id of the experiment
     * @param String $experimentDesign object from prevalidation
     * @param Array $occurrenceDbIds list of occurrence record ids
     */
    public function prevalidateUploadFile($fileName, $id, $experimentDesign, $occurrenceDbIds=null){
        $error['success'] = false;

        try {
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads';
            $experimentDesign = strtolower($experimentDesign);
            $file = UploadedFile::getInstanceByName($fileName);
            $oldFileName =  $file->name;
            $type = $file->type;
            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false, 
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $rawFileName = \Yii::$app->security->generateRandomString();
            $origTmpFileName = $rawFileName . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $origTmpFileName;

            $file->saveAs($tmpFileName);


            //copy file to output folder with the correct names
            $csv = new \app\components\CsvReader($tmpFileName);

            //get headers information
            $headersInfoArr = $this->getOrderedHeaders($tmpFileName, $experimentDesign, $csv);

            extract($headersInfoArr);

            //Copy if successfully validated
            $csv->setHeader($orderedHeaders);
            $rawFileName = \Yii::$app->security->generateRandomString();
            $origTmpFileName = $rawFileName . '.' . $file->extension;
            
            //initial creation of file with headers updated
            $root=Yii::getAlias('@webroot').'/analytics/output/'.$origTmpFileName;
            if(file_exists($root)){
                chmod($root, 777);
                unlink($root);
            }
            $headerFlag = 0;
            $handle = fopen($root, 'x');

            $inputData = [];
            $parameterOccurrence = [];
            foreach($csv as $line){
                if($headerFlag == 0){
                    fputcsv($handle, $orderedHeaders,',', '"');
                    $headerFlag = 1;
                }
                fputcsv($handle, $line, ',', '"');
                $inputData[] = $line;
                $parameterOccurrence[$line['parameter set']][] = $line['occurrence'];
            }
                    
            fclose($handle);

            $totalOccurrences = 0;
            foreach($parameterOccurrence as $key => $value){
                $parameterOccurrence[$key] = array_values(array_unique($value));
                $totalOccurrences += count($parameterOccurrence[$key]);
            }

            if($occurrenceDbIds == null){
                $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'experiment', 'entityDbId'=>"equals $id"]);
            } else {
                $planTemplateArray = $this->planTemplate->searchAll(['entityType' => 'occurrence', 'occurrenceApplied'=>"equals ".implode(";", $occurrenceDbIds)]);
            }
            //return selection of design per parameters
            if($planTemplateArray['totalCount'] > 0){
                $experimentJson = $planTemplateArray['data'][0];
                $uiInfo = json_decode($experimentJson['mandatoryInfo'], true);
                $designId = $uiInfo['design_id'];
                $design = $uiInfo['design_name'];
            } 

            //get available designs, Unspecified only for this 1st implementation
            $analysisModel = new AnalysisModel();
            
            $designValue = 'Unspecified';

            $availDesigns = $analysisModel->getDesigns();
           
            $designArray = [];
            foreach($availDesigns['designArray'] as $availDesign){
                if($availDesign != "Sparse Testing"){
                    $designArray[] = [
                        'id' => $availDesign,
                        'value' => $availDesign
                    ];
                }
            }
            
            $dataArray = [
                'information' => [
                    'totalOccurrences' => [
                        'label' => 'No. of Occurrences',
                        'value' => $totalOccurrences
                    ],
                    'parameter_sets' => [
                        'label' => 'Parameter sets',
                        'value' => max(array_column($inputData, 'parameter set'))
                    ],
                    'filename' => [
                        'label' => 'Uploaded file name',
                        'value' => $oldFileName
                    ],
                    'parameterOccurrence' => $parameterOccurrence
                ],
                'parameter_set' => max(array_column($inputData, 'parameter set')),
                'success' => true,
                'fileName' => $rawFileName ,
                'designArray' => $designArray,
                'tmpFileName' => $tmpFileName,
                'type'=>$type,
                'parameterOccurrence'=> $parameterOccurrence
            ];

            return $dataArray;
            
        } catch (\Exception $e) {

            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $error['error'] = 'Please check if file is not empty.';
                return $error;
            }
            
            $error['error'] = $e->getMessage();
            return $error;
        } 
    }

    /**
     * Update headers
     *
     * @param String $tmpFileName name of the uploaded file
     * @param String $experimentDesign object from prevalidation
     * @param Object $csv 
     */
    public function getOrderedHeaders($tmpFileName, $experimentDesign, $csv){
        //copy file to output folder with the correct names

        $csv->setHasHeader(true);

        $headers = $csv->getHeader();

        //required headers per design
        $reqHeadersPerDesign = [
            'rcbd' => [
                'headers' => ['occurrence', 'plot_number', 'replicate', 'entry_id', 'field_row', 'field_col'],
                'optional' => ['field_row', 'field_col']
            ],
            'augmented rcbd'=> [
                'headers' => ['occurrence', 'plot_number', 'block','entry_type', 'entry_id', 'field_row', 'field_col'],
                'optional' => ['field_row', 'field_col']
            ],
            'partially replicated' => [
                'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate'],
                'optional' => ['field_row', 'field_col']
            ],
            'alpha-lattice' => [
                'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate', 'block'],
                'optional' => ['field_row', 'field_col']
            ],
            'unspecified' => [
                'headers' => ['occurrence', 'plot_number','entry_type', 'entry_id', 'field_row', 'field_col', 'replicate', 'block', 'parameter_set'],
                'optional' => ['field_row', 'field_col','entry_type','replicate', 'block']
            ]
        ];
        foreach($csv as $line){
            $headers = array_keys($line);
            break;
        }

        $finalList = $reqHeadersPerDesign[$experimentDesign]['headers'];
        $optionalFields = $reqHeadersPerDesign[$experimentDesign]['optional'];

        $finalHeaders = [];
        foreach($finalList as $val){
            $finalHeaders[$val] = null;
        }

        if($experimentDesign == 'unspecified'){
           //allowed naming of headers in file
            $mappedHeaders = [
                "occurrence" => ['exp', 'trial', 'occurrence no'],
                "plot_number"=>['plot', 'plotnum', 'plot no'],
                "rep"=>['replicate', 'replication'],
                "block"=>['block', 'blk', 'block no'],
                "entry_id"=>['entry', 'treatment', 'entry_number', 'entry no'],
                "entry_type" => ["entry_type", 'entry type'],
                "field_row" => ['fieldrow', 'row', 'y', 'design y'],
                "field_col" => ['fieldcol', 'column', 'col', 'x', 'design x'],
                "remark" => ['remark']
            ];
        } else {
            //allowed naming of headers in file
            $mappedHeaders = [
                "occurrence" => ['exp', 'trial', 'occurrence no'],
                "plot_number"=>['plot', 'plotnum', 'plot no'],
                "rep"=>['replicate', 'replication'],
                "block"=>['block', 'blk', 'block no'],
                "entry_id"=>['entry', 'treatment', 'entry_number', 'entry no'],
                "entry_type" => ["entry_type", 'entry type'],
                "field_row" => ['fieldrow', 'row', 'y', 'design y'],
                "field_col" => ['fieldcol', 'column', 'col', 'x', 'design x'],
                "colblock" => ['colblock', 'col block no'],
                "rowblock" => ['rowblock', 'row block no'],
                "remark" => ['remark']
            ];
        }
        
        $withLayout = false;
        $orderedHeaders = [];
        $errorHeaders = [];
        
        foreach($headers as $index){
            foreach($mappedHeaders as $key => $value){
                $inAllowedHeaders = false;
                $addHeader = '';

                if(in_array(strtolower($index), $value)){
                    $finalHeaders[$key] = $index;
                    $addHeader = $key;
                    $inAllowedHeaders = true;
                    break;
                }
            }

            if($inAllowedHeaders){
                $orderedHeaders[] = $addHeader;
            } else {
                $orderedHeaders[] = strtolower($index);
            }
        }

        return [
            'withLayout' => $withLayout,
            'orderedHeaders' => $orderedHeaders,
            'errorHeaders' => $errorHeaders,
            'finalHeaders' => $finalHeaders,
            'csv' => $csv
        ];
    }   

    /**
     * Update headers
     *
     * @param String $tmpFileName name of the uploaded file
     * @param String $designStr design per parameter set from prevalidation
     * @param String $type file format
     * @param Integer $id record id of experiment
     */
    public function validateViaApi($tmpFileName, $designStr, $type, $id){

        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

        //send to validation first
        $curl = curl_init();

        $data = array( 
            'file' => curl_file_create($tmpFileName, $type,$tmpFileName)
        ); 

        if(empty($designStr)){
            $designStr = "&experimentDesign=Unspecified";
        } else {
            $designStr = "&experimentDesign=".rawurlencode($designStr);
        }

        $acrossEnvDesign = $this->designModel->getAcrossEnvDesign($id);
        if($acrossEnvDesign == 'random'){
            $designStr .= '&acrossEnvironmentDesign=random';
        }
        
        curl_setopt_array($curl, [
            CURLOPT_URL => getenv('BA_API2_URL')."v1/designs/validate?experimentDbId=".$id.$designStr,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true, 
            CURLOPT_VERBOSE => 1, 
            CURLOPT_POST => 1, 
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer ". $accessToken,
                "content-type: multipart/form-data"
            ],  
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        return [
            'err' => $err,
            'response' => $response
        ];
    }

    /**
    * Download and replaces the tempfile before submitting the upload request
    * @param string $filename name of the tempfile
    * @param integer $experimentDbId record id of the experiment
    * */
    public function downloadValidatedFile($filename, $experimentDbId){

        //Download validated file and saved in uploaded file
        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');   
        set_time_limit(0);
        $uploadPath = Yii::getAlias('@webroot').'/analytics/output';

        //This is the file where we save the    information
        $fp = fopen ($uploadPath .'/'.$filename, 'w+');

        //Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(getenv('BA_API2_URL')."v1/designs/validate/output?experimentDbId=".$experimentDbId);

        // make sure to set timeout to a high enough value
        // if this is too low the download will be interrupted
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        // write curl response to file
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                "Authorization: Bearer ". $accessToken,
                "content-type: multipart/form-data"
        ]);

        // get curl response
        curl_exec($ch); 
        curl_close($ch);
        fclose($fp);

        return true;
    }

}