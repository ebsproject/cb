<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\experimentCreation\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;
use app\models\UserDashboardConfig;

/**
 * Model for searching an experiment's entry data of an occurrence
 */
class ExperimentEntryModel extends BaseModel
{

    public $entryDbId;
    public $entryNumber;
    public $entryName;
    public $entryType;
    public $entryRole;
    public $entryClass;

    /**
     * Set dependencies
     * This overrides construct
     */
    public function __construct (protected UserDashboardConfig $userDashboardConfig, $config = [])
    { parent::__construct($config); }

    /**
    * {@inheritdoc}
    */
    public function rules ()
    {
        return [
            [[
                'entryName',
                'entryType',
                'entryClass',
                'entryRole',
            ], 'string'],
            [[
                'entryDbId',
                'entryNumber',
            ], 'integer'],
        ];
    }

    /**
     * Get experiment's entry list's data provider for entry data
     *
     * @param array $params list of search parameters
     * @param string|integer $id unique experiment identifier
     *
     * @return ArrayDataProvider data provider for the entry data of an experiment's entry list
     */
    public function search ($params, $id)
    {
        Yii::info(
            'Searching for experiment entry with arguments: ' . json_encode(compact('params', 'id')),
            __METHOD__
        );

        $filters = [];
        $id = $params['id'] ?? $id;
        $dataProviderId = 'data-provider-experiment-entry';

        $paramLimit = '';
        $paramPage = '';
        $currPage = 1;
        $paramSort = '';
        $sortParams = '';

        if (
            isset($_GET["$dataProviderId-sort"]) &&
            !empty($_GET["$dataProviderId-sort"])
        ) {
            $paramSort = 'sort=';
            $sortParams = $_GET["$dataProviderId-sort"];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach ($sortParams as $column) {
                if ($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if ($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        } else { // set default order
            $paramSort = 'sort=entryNumber';
        }

        // get filters
        if (isset($params['ExperimentEntryModel'])) {
            foreach ($params['ExperimentEntryModel'] as $key => $value) {
                $val = trim($value);

                if ($val != '' && $key != 'fields') {
                    $filters[$key] = (str_contains($val, '%') || str_contains($val, 'equals')) ? $val : "equals $val";
                } elseif ($key === 'fields') {
                    $filters[$key] = $val;
                }
            }
        }

        $filters = empty($filters) ? null : $filters;

        // get current page
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $pagination = $paramLimit . $paramPage;

        $paramSortOriginal = $paramSort;

        // check if page is out of range
        $filters['experimentDbId'] = "equals ".$id;
        $totalPagesArr = Yii::$app->api->getParsedResponse(
            'POST',
            "entries-search?$pagination&$paramSort",
            json_encode($filters)
        );

        // if out of range, redirect to page 1
        if ($currPage != 1 && count($totalPagesArr['data']) == 0) {
            $pagination = "$paramLimit&page=1";
            $_GET["$dataProviderId-page"] = 1; // return browser to page 1
        }

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            "entries-search?$pagination&$paramSort",
            json_encode($filters)
        );
        Yii::$app->session->set('experiment-'.$id.'-entry-api_filter', $filters);

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'entryDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'attributes' => [
                    'entryDbId',
                    'entryNumber',
                    'entryName',
                    'entryType',
                    'entryRole',
                    'entryClass',
                ],
                'defaultOrder' => [
                    'entryNumber' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);

        return [
            $dataProvider,
            $output['totalCount'],
            $paramSortOriginal,
            $filters,
            $output['data']
        ];
    }
}