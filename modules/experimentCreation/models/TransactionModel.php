<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Study;
use app\models\User;
use app\models\Program;
use app\models\Variable;
use app\models\StudyMetadata;
use app\models\TerminalBackgroundProcess;
use app\models\TerminalTransaction;
use app\models\UserDashboardConfig;
use app\controllers\Dashboard;
use app\controllers;
use Yii;

use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use app\models\PlanTemplate;
use Alchemy\Zippy\Zippy;
use yii\helpers\Url;
use app\modules\experimentCreation\models\ExperimentModel;
use app\modules\experimentCreation\controllers\CreateController;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 *  Model class for Randomization transaction
 */
class TransactionModel
{
    /**
     * Create plot records via rabbitmq worker
     *
     * @param $experimentDbId integer experiment identifier
     * @param $planTemplateDbId integer plan template identifier
     * @param $design string experiment design value
     * @param $rep integer number of replications
     * @param $entryListDbId integer entry list identifier
     * @param $row integer number of field rows
     */
    public static function createPlotRecords($experimentDbId, $planTemplateDbId, $design, $rep, $entryListDbId, $row){

        $host = getenv('CB_RABBITMQ_HOST');
        $port = getenv('CB_RABBITMQ_PORT');
        $user = getenv('CB_RABBITMQ_USER');
        $password = getenv('CB_RABBITMQ_PASSWORD');

        $connection = new AMQPStreamConnection($host, $port, $user, $password);

        $channel = $connection->channel();

        $channel->queue_declare(
            'PopulatePlotRecords', //queue - Queue names may be up to 255 bytes of UTF-8 characters
            false,  //passive - can use this to check whether an exchange exists without modifying the server state
            true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
            false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
            false   //auto delete - queue is deleted when last consumer unsubscribes
        );

        $msg = new AMQPMessage(
            json_encode([
                "experimentDbId" => "$experimentDbId",
                "planTemplateDbId" => "$planTemplateDbId",
                "accessToken" => Yii::$app->session->get('user.b4r_api_v3_token'),
                "design"=>$design,
                "entryListDbId"=> "$entryListDbId",
                "rep"=>"$rep",
                "row"=>$row
            ]),
            array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
        );

        $channel->basic_publish(
            $msg,   //message 
            '', //exchange
            'PopulatePlotRecords'  //routing key (queue)
        );
        $channel->close();
        $connection->close();

        return json_encode(array("success" => true));

    }
}
?>