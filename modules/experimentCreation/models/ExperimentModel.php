<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for Experiment
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\components\GenericFormWidget;

use app\models\Config;
use app\models\Crop;
use app\models\CropProgram;
use app\models\Cross;
use app\models\CrossAttribute;
use app\models\CrossParent;

use app\models\Entry;
use app\models\EntryData;
use app\models\EntryList;
use app\models\EntryListSearch;
use app\models\Experiment;
use app\models\ExperimentBlock;
use app\models\ExperimentData;

use app\models\GermplasmName;
use app\models\Germplasm;

use app\models\Item;

use app\models\Occurrence;
use app\models\OccurrenceLayout;

use app\models\Package;
use app\models\Person;
use app\models\PlanTemplate;
use app\models\PlantingInstruction;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\models\Plot;
use app\models\Program;
use app\models\Protocol;
use app\models\ScaleValue;
use app\models\Seed;
use app\models\User;
use app\models\Variable;

use app\interfaces\models\IWorker;

use app\modules\experimentCreation\models\AnalysisModel;
use app\modules\experimentCreation\models\CrossPatternModel;
use app\modules\experimentCreation\models\EntryOrderModel;
use app\modules\experimentCreation\models\FormModel;

use PhpOffice\PhpSpreadsheet\Chart\Exception;

use app\dataproviders\ArrayDataProvider;
use yii\data\SqlDataProvider;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\dynagrid\DynaGrid;
use kartik\widgets\Select2;
use kartik\grid\GridView;

use DateTime;
use Yii;

class ExperimentModel extends Experiment
{
    
    public function __construct (   
        public Cross $cross,
        public CrossAttribute $crossAttribute,
        public CrossParent $crossParent,
        public CrossPatternModel $crossPatternModel,
        public Entry $mainEntry,     
        public EntryData $entryData,     
        public EntryList $mainEntryList,
        public EntryOrderModel $mainEntryOrderModel,
        public Experiment $mainExperiment,
        public ExperimentBlock $mainExperimentBlock,
        public ExperimentData $mainExperimentData,
        public Variable $variable,
        public Item $item,
        public FormModel $formModel,
        public Germplasm $germplasm,
        public GenericFormWidget $genericFormWidget,
        public Config $configModel,
        public Occurrence $occurrenceModel,
        public Package $mainPackage,
        public Person $person,
        public PlantingInstruction $plantingInstruction,
        public PlatformList $platformlist,
        public PlatformListMember $platformListMember,
        public Plot $plotModel,
        public Protocol $protocolModel,
        public ScaleValue $scaleValue,
        public Seed $mainSeed,
        public IWorker $worker,
    ) { }
    /**
     * Save basic information of the Experimnent
     *
     * @param string $program program name
     * @param array $experiment contains experiment basic information to be saved
     * @param integer $id record id of the experiment
     * @return array $tags list of filter tags
    */
    public function saveExperiment($experiment, $program, $dataLevel, $id, $targetStep=null, $protocolType=null, $model=null, $controller=null){
        $metadata = [];
        $identification = [];
        $metadataIds = [];
        $metId = '';
        $plotTypeVariables = [];
        $plotTypeVariableIds = [];
        $dataProcessId = '';
        $defaultArr = [];
        $experimentRecord = null;

        // for those variables not for updates
        $colsForUpdatesVal = [];
        
        foreach($experiment['Experiment'] as $key => $value){

            $varModel = $this->variable->getVariableByAbbrev(strtoupper($key));

            $key = ucwords(strtolower($key),'_');

            if(!empty($varModel)){
                if(!empty($varModel['targetModel'])){
                    $key = strtolower($key).'DbId';
                }

                if(isset($varModel['abbrev']) && $varModel['abbrev'] == 'EXPERIMENT_STEWARD'){
                    $key = 'stewardDbId';
                }

                $key = str_replace('_','',$key);
                $key = lcfirst($key);
        
                if(is_array($value) === false){
                    $metadata[] = [
                        'id' => $varModel['variableDbId'],
                        'value' => $value
                    ];
                    $metadataIds[] = $varModel['variableDbId'];

                }else{
                    if($varModel['abbrev'] != 'EXPERIMENT_CODE'){
                        $source = array_keys($value);
                        $newVal = array_keys($value[$source[0]]);
                        
                        if(strpos($newVal[0], "'disabled'") !== false){
                            $idVal = array_keys($value[$source[0]][$newVal[0]]);
                            $metId = $idVal[0];
                            $value_temp = $value[$source[0]][$newVal[0]][$idVal[0]];
                        }else{
                            $metId = $newVal[0];
                            $value_temp = $value[$source[0]][$newVal[0]];
                            if(!empty($value_temp)){
                                $colsForUpdatesVal[$key] = $value_temp;
                            }

                            if($key == 'stageDbId'){
                                //split value to get id
                                $stageValArray = explode('-', $value_temp);
                                $colsForUpdatesVal[$key] = $stageValArray[1];
                            }
                        }
                       
                        if($source[0] == "identification"){
                            $identification[$key] = $value_temp;
                        } else{
                            //This is to classify the plot type variables
                            if($varModel['abbrev'] !== 'PLANTING_TYPE' && $varModel['abbrev'] !== 'ESTABLISHMENT'){
                                $plotTypeVariables[] = [
                                    'variableDbId' => $metId,
                                    'dataValue' => $value_temp
                                ];
                                $plotTypeVariableIds[] = $metId;
                            }else{
                                //This is for Planting Type and Establishment variable
                                $defaultArr[] = [
                                    'variableDbId' => $metId,
                                    'dataValue' => $value_temp
                                ];
                            }
                            $metadata[] = [
                                'id' => $metId,
                                'value' => $value_temp
                            ];
                            $metadataIds[] = $metId;
                        }
                    }
                }
            }
        } // End of foreach loop

        //Check if experiment exists
        if(!empty($id)){
            $experimentRecord = $this->mainExperiment->getExperiment($id);
        }
        
        $tableColumnParams = [];
        $xptTemplate = '';
        // Assign the identification variable and value to the request body parameter
        // This is variables that exists as columns in the experiment table
        foreach($identification as $key=>$val){
            $val = empty($val) ? null : $val;
            if(!empty($val)){
                $tableColumnParams[$key] = $val;
                if($key == 'experimentType'){
                    if(strpos($val,'Cross Parent Nursery') !== false){
                        $xptTemplate = 'CROSS_PARENT_NURSERY'.'_PHASE_I_DATA_PROCESS'; 
                    }else{
                        $xptTemplate = str_replace(' ','_',$val);
                        $xptTemplate = strtoupper($xptTemplate).'_DATA_PROCESS';
                    }
                   
                    $item = $this->item->searchAll(["abbrev"=>"$xptTemplate"]);
                    $dataProcessId = isset($item['data'][0]['itemDbId']) ? $item['data'][0]['itemDbId'] : '';
                } else if($key == 'stageDbId'){
                    //split value to get id
                    $stageValArray = explode('-', $val);
                    $tableColumnParams[$key] = $stageValArray[1];
                }
            }
        }

        //Build request body param for experimeent data variables
        $dataRequestBodyParam = [];
        $searchDataParamCond = '';
        $voidValSearchCond = '';
        
        foreach($metadata as $metaVal){
            $variableId = $metaVal['id'];
            $variableValue = $metaVal['value'];

            $dataRequestBodyParam[] = [
                'variableDbId' => "$variableId",
                'dataValue' => "$variableValue"
            ];
            
            if(!empty($variableValue)){
                if(empty($searchDataParamCond)){
                    $searchDataParamCond = "equals ".$metaVal['id'];
                }else{
                    $searchDataParamCond = $searchDataParamCond . "|". "equals ".$metaVal['id'];
                }
            }

            //Build the search parameter for voiding of existing values to replace
            if(empty($voidValSearchCond)){
                $voidValSearchCond = "equals ".$metaVal['id'];
            }else{
                $voidValSearchCond = $voidValSearchCond . "|". "equals ".$metaVal['id'];
            }
        }
        $errorMsg = '';
        
        //Build the request body parameters
        if($experimentRecord == NULL){
            // Create new record
            // Defaults
            
            if(strpos($xptTemplate, "_TRIAL") !== false){
                $defaultRequestBodyParam = [
                    'experimentStatus' => 'draft',
                    'dataProcessDbId' => "$dataProcessId"
                ];
            } else {

                //check if there's default value for DESIGN
                $configData = ExperimentModel::retrieveConfiguration($controller, $program, $id, $dataProcessId);
                $varIndex = array_search('DESIGN', array_column($configData, 'variable_abbrev'));
                if($varIndex === false){
                    $experimentDesignType = 'Systematic Arrangement';
                } else {
                    $experimentDesignType = $configData[$varIndex]['default'];
                }
                $defaultRequestBodyParam = [
                    'experimentStatus' => 'draft',
                    'experimentDesignType' => $experimentDesignType,
                    'dataProcessDbId' => "$dataProcessId"
                ];
            }
            
            //Merge default params and dynamic params
            //Consists of variables that exists as columns in the experiment table
            $mainRequestBodyParam = ArrayHelper::merge($defaultRequestBodyParam,$tableColumnParams);
            $result = Experiment::createExperiment($mainRequestBodyParam);
            
            //Check if experiment record was successfully created (value is true or false)
            if($result['status']){
                //If true then save data record 
                $id = $result['experimentId'];

                if(!empty($dataRequestBodyParam)) {
                    $dataResult = Experiment::createExperimentData($id,$dataRequestBodyParam);
    
                    if(!$dataResult['status']){
                        $errorMsg = $result['error'];
                    }
                }
            }else{
                $errorMsg = $result['error'];
            
            }
        }else if($experimentRecord['dataProcessAbbrev'] != 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
            // This condition works for the BASIC information input form
            if($targetStep != 'protocol' && !empty($colsForUpdatesVal)){
                //Update experiment record
                //Consists of variables that exists as columns in the experiment table
                $result = Experiment::updateExperiment($id,$colsForUpdatesVal);
            
                //Check if experiment record was successfully created (value is true or false)
                if($result['status']){
                    if(!empty($dataRequestBodyParam)) {
                        //If true then save data record 
                        $experimentDataRes = ExperimentData::getExperimentData($id);
            
                        foreach($experimentDataRes as $eachExperimentData){
                            $dataResult = ExperimentData::updateExperimentData($eachExperimentData['experimentDataDbId'],$dataRequestBodyParam);
            
                            if(!$dataResult['status']){
                                $errorMsg = $result['error'];
                            } 
                        }
                    }
                }else{
                    $errorMsg = $result['error'];
                }
            }else{
                //This update is for PROTOCOLS input form
                //Check if the variable exists in the experiment data
                //Create a new protocol record first
                //Check if the protocol type for the experiment already exists
                $experimentCode = $experimentRecord['experimentCode'];
                $protocolCode = strtoupper($protocolType)."_PROTOCOL_".$experimentCode;
            
                $protocol = $this->protocolModel->searchAll(["protocolCode"=>"equals $protocolCode"]);
                
                $protocolId = isset($protocol['data'][0]['protocolDbId']) ? $protocol['data'][0]['protocolDbId'] : '';
                
                // If protocol id is empty then create a protocol and experiment protocol record
                if(empty($protocolId) || $protocolId == 0){ 
                    $protocolId = $this->protocolModel->createExperimentProtocols($id,$protocolType);
                }

                $createDataParam = [];
                
                //Values from the input fields
                foreach($dataRequestBodyParam as $metData){
                    $varId = $metData['variableDbId'];
                    $dataVal = $metData['dataValue'];

                    if(!empty($dataVal)){
                        $createDataParam[] = ["variableDbId"=>"$varId","dataValue"=>"$dataVal","protocolDbId"=>"$protocolId"];
                    }
                }        
                
                $updateDataParam = ['variableDbId'=>"$voidValSearchCond", "protocolDbId"=>"equals $protocolId"];
                $experimentDataRes = $this->mainExperimentData->searchAllRecords($id,$updateDataParam);
                
                //Update
                if(!empty($experimentDataRes)){
                    //Remove the previously saved records of the protocol
                    $toRemExptDataArr = ArrayHelper::map($experimentDataRes,'experimentDataDbId','experimentDataDbId');
                    if(count($toRemExptDataArr) > 0){
                        $expDataIds = array_keys($toRemExptDataArr);
                        //Remove all saved variables of a protocol not part of the input form
                        $this->mainExperimentData->deleteMany($expDataIds);
                    }
                } 

                //Create a new experiment data record here
                $dataResult = $this->mainExperiment->createExperimentData($id,$createDataParam);
                
            }
        } 
        return ['experimentId'=>$id,'errorMsg'=>$errorMsg];
    }

    /**
     * Get ids of filtered entry list
     *
     * @param array $filters array list of entry list filters
     * @param integer $id record id of the experiment
     *
     * @return array $entryIds list of entry ids
     */
    public function getFilteredEntryList($filters, $id, $varConfigValues){
        $model = Experiment::findOne($id);
        $programId = $model->program_id;

        $searchModel = new EntryListSearch();
        $params['EntryListSearch'] = $filters;
        $entryIds = $searchModel->search($params, $id, $programId, 'id', $varConfigValues);

        return $entryIds;
    }

    /**
     * Populate entry of the experiment with the saved list
     *
     * @param Integer $experimentId record id of the experiment
     * @param Integer $savedListId record id of the selected saved list
     * @param Array $configData list of configuration variables
     * @param String $program Program Abbrev
     */
    public function populateEntryViaList($experimentId, $savedListId, $configData,$program){
        
        // retrieve default values
        foreach($configData as $config){
            if(isset($config['default']) && !empty($config['default'])){
                switch($config['variable_abbrev']){
                    case 'ENTRY_TYPE':
                        $entryType = $config['default'];
                        break;
                    case 'ENTRY_CLASS':
                        $entryClass = $config['default'];
                        break;
                    case 'ENTRY_ROLE':
                        $entryRole = $config['default'];
                        break;
                    default:
                        break;
                }
            }
        }
        //Get the working experiment info
        $experiment = $this->mainExperiment->getExperiment($experimentId);

        //Get entry list
        $entryListId = $this->mainEntryList->getEntryListId($experimentId, true);

        $param = ["listDbId"=>"equals ".$savedListId];
        $listRecord = $this->platformlist->searchList($param, true);
        $listMemberCount = $listRecord[0]['memberCount'];
        
        $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;
        
        //add notes for BT experiment
        $this->addDesignNoteForUpdate($experimentId, "updated entries");

        if($listMemberCount > $bgThreshold){
            $userModel = new User();
            $userId = $userModel->getUserId();

            $token = \Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = \Yii::$app->session->get('user.refresh_token');
            //call background process for creating entries
            $addtlParams = ["description"=>"Creating of entry records","entity"=>"ENTRY","entityDbId"=>$experimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            $token = \Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = \Yii::$app->session->get('user.refresh_token');
            $dataArray = [
                'entity'=>'list',
                'entityId' => $savedListId,
                'defaultValues' => [
                    'entryListDbId' => $entryListId,
                    'entryType' => isset($entryType) ? $entryType : null,
                    'allowedEntryType' => null,
                    'entryClass' => isset($entryClass) ? $entryClass : null,
                    'entryRole' => isset($entryRole) ? $entryRole : null
                ],
                'processName' => 'create-entry-records',
                'experimentDbId' => $experimentId."",
                'tokenObject' => [
                    'token' => $token,
                    'refreshToken' => $refreshToken
                ],
                'personDbId' => $userId
            ];
            $create = $this->mainExperiment->create($dataArray, true, $addtlParams);

            $experiment = $this->mainExperiment->getExperiment($experimentId);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }

            if(!empty($create['data'])){
                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    
                    $updatedStatus = implode(';', $status);
                    $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                
                    $this->mainExperiment->updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

                    //update experiment
                    Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentId]);
                }
            } else {
                 Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentId]);
            }
        } else {
      
            //get platformlist members
            $list = $this->platformListMember->getListMembers($savedListId, true);

            $listMembers = $list[0]['members'];
            $programId = isset($experiment['programDbId']) ? $experiment['programDbId'] : "";
            $seedFilterIds = array();
         
            $createRecords = [];
            foreach($listMembers as $members){
                if($members['isActive']){
                    $germplasmId = $members['germplasmDbId'];
                    $temp = [
                        'entryName' => $members['designation'],
                        'entryType' => !empty($entryType) ? $entryType : 'test',
                        'entryStatus' => 'active',
                        'entryListDbId' => "$entryListId",
                        'germplasmDbId' => "$germplasmId"
                    ];

                    if(!empty($entryClass)){
                        $temp['entryClass'] = $entryClass;
                    }

                    if(!empty($experiment['experimentType']) && ($experiment['experimentType'] != 'Breeding Trial' && $experiment['experimentType'] != 'Observation')){
                        if(!empty($entryRole)){
                            $temp['entryRole'] = $entryRole;
                        }
                    }
                  
                    if(isset($members['seedDbId'])){
                        $seedDbId = $members['seedDbId'];
                        $temp['seedDbId'] = "$seedDbId";
                    }
                    if(isset($members['packageDbId']) && $members['programDbId'] == $programId){
                        $packageDbId = $members['packageDbId'];
                        $temp['packageDbId'] = "$packageDbId";
                    }
                    
                    $createRecords[] = $temp;
                }
            }
            
            $createdEntryIds = $this->mainEntry->create(["records" =>$createRecords]);

            //update experiment status
            $this->mainEntryOrderModel->entryListAfterUpdate($experimentId, $createdEntryIds);

            //Update seedDbId for entry if list is product
            if(in_array($list[0]['type'], ['designation', 'germplasm'])){
                $this->autoSelectSinglePackage($entryListId, $experimentId);
            }
        }
        return true;
        
    }

    /**
     * Gets the configuration for the selected design
     *
     * @param string $design selected value for the design
     * @param string $type data to retrieve
     */
    public function getJsonConfig($design, $type){

        $jsonConfig = NULL;

        $analysisModel = new AnalysisModel();
        $designParams = $analysisModel->getDesignProperty($design);

        if(isset($designParams['data']['designModel']['children'])){

            $properties = $designParams['data']['designModel']['children'];
            $propertiesArray = array();
            foreach($properties as $property){
                $propertiesArray[$property['type']][] = $property;
            }
            
            if($type == 'ui'){

                // Asc sort
                $jsonConfig = $propertiesArray['input'];
                if($jsonConfig != NULL){
                    usort($jsonConfig,function($first,$second){
                        return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                    });

                }

                $randomization = array();
                $layout = array();
                $rules = array();
                foreach($jsonConfig as $params){
                    if($params['layoutVariable']){
                        $layout[] = $params;
                    } else {
                        $randomization[] = $params;
                    }

                    //get rules per parameters
                    if($params['rules'] != null){
                        foreach($params['rules'] as $rule){
                            $rules[$rule['type']][$params['code']][] = $rule;
                        }

                        //sort rules per params based on orderNumber
                        usort($rules[$rule['type']][$params['code']],function($first,$second){
                            return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                        });
                    }
                }

                $jsonConfig = [
                    'randomization' => $randomization,
                    'layout' => $layout,
                    'rules' => $rules
                ];

            } else if($type == 'metadata'){
                
                if(isset($designParams['data']['designModel']['meta'])){
                    $jsonConfig = $designParams['data']['designModel']['meta'];
                }

            } else {
                $jsonConfig = NULL;
            }
        }
        
        return $jsonConfig;
    }

    /**
     * Gets the configuration for the selected design
     *
     * @param string $design selected value for the design
     * @param string $type data to retrieve
     */
    public function getJsonConfigFromAnalytics($design, $type){

        $analysisModel = new AnalysisModel();
        $designs = $analysisModel->getDesignCatalogue();
        
        $designMethod = null;
        if(isset($designs['data']['designModel']['children'])){
            $arrayIndexes = array_column($designs['data']['designModel']['children'], 'id');
            $index = array_search($design, $arrayIndexes);
            $designMethod = $designs['data']['designModel']['children'][$index]['code'];
        }

        return $designMethod;
    }



    /**
     * Creates the json data input based on the user input
     *
     * @param integer $id selected value for the design
     * @param array $value information to be saved
     * @param string $design selected value for the design
     * @param array $jsonInput json array of parameters to be filled
     * @param array $jsonUIParameters json array of parameters to be filled
     * @param integer $occurrenceCount number of occurrences to be generated
     */
    public function createJsonInputAnalytics($id, $value, $design, $jsonInput, $jsonUIParameters, $occurrenceCount, $occurrenceDbIds = null){

        $designId = $jsonUIParameters['design_id'];
        $design = $jsonUIParameters['design_name'];
        $method = ExperimentModel::getJsonConfigFromAnalytics($designId, 'json');
        
        $inputKeysArray = array_column($jsonUIParameters['design_ui']['randomization'], 'code');
        
        $metadataArray = $jsonUIParameters['metadata'];
        
        $metadata = [];
        $experiment = $this->mainExperiment->getExperiment($id);

        if($occurrenceDbIds == null){
            $param = ['entityType'=>'experiment', 'entityDbId'=> $id];
        } else {
            $param = ['entityType'=>'occurrence', 'occurrenceApplied'=> "equals ".implode(";",$occurrenceDbIds)];
        }
        
        $experimentJson = PlanTemplate::getPlanTemplateByEntity($param);

        $date = new DateTime();
        $timestamp = $date->format('YmdHis');

        //Get crop and institute
        $cropId = $experiment['cropDbId'];
        $programId = $experiment['programDbId'];

        $programModel = new Program(new CropProgram());
        $program = $programModel->getProgram($programId);
        $cropProgramId = $program['cropProgramDbId'];

        $cropProgramModel = new CropProgram();
        $programCrop = $cropProgramModel->searchAll(['cropProgramDbId'=>$cropProgramId.""]);

        $cropModel = new Crop();
        $cropArray = $cropModel->searchAll(['cropDbId'=> $cropId.""]);
        
        //build metadata
        $metadataArrayReq = [
            ['code'=>'crop'], 
            ['code'=>'experiment_id'], 
            ['code'=>'requestorId'], 
            ['code'=>'design'], 
            ['code'=>'method'],
            ['code'=>'category'],
            ['code'=>'type'],
            ['code'=>'engine'],
            ['code'=>'program'],
            ['code'=>'organization_code']
        ];
        
        $metadataKeys = array_column($metadataArrayReq, 'code');
        
        if($metadataArray != NULL){
            foreach($metadataArray as $meta){
                if(in_array($meta['code'], $metadataKeys)){
                    $metadata[$meta['code']] = $meta['value'];
                }
            }
        }
        
        $metadataKeysValue = array_keys($metadata);
        //check if required metadata are all there
        foreach($metadataArrayReq as $meta){
            if(!in_array($meta['code'], $metadataKeysValue)){
                // $metadata[$meta['code']] = $meta['name'];
                if($meta['code'] == 'crop'){
                    $metadata[$meta['code']] = $cropArray['data'][0]['cropCode'];
                }
                if($meta['code'] == 'experiment_id'){
                    $metadata[$meta['code']] = $id;
                }
                if($meta['code'] == 'requestorId'){
                    $userModel = new User();
                    $metadata[$meta['code']] = $userModel->getUserId();
                }
                if($meta['code'] == 'design'){
                    $metadata[$meta['code']] = $design;
                }
                if($meta['code'] == 'method'){
                    $metadata[$meta['code']] = $method;
                }
                if($meta['code'] == 'category'){
                    $metadata[$meta['code']] = "Design";
                }
                if($meta['code'] == 'type'){
                    $metadata[$meta['code']] = "Trial Design";
                }
                if($meta['code'] == 'engine'){
                    $metadata[$meta['code']] = "NA";
                }
                if($meta['code'] == 'program'){
                    $metadata[$meta['code']] = $experiment['programCode'];
                }
                if($meta['code'] == 'organization_code'){
                    $metadata[$meta['code']] = $programCrop['totalCount'] > 0 ? $programCrop['data'][0]['cropProgramCode'] : "NA";
                }
            }
        }

        //build parameters
        $filename = $experimentJson['templateName'].'_SD_'.$cropArray['data'][0]['cropCode'].'_'.$timestamp;

        $currentInputKeys = array_keys($jsonInput);

        foreach($jsonUIParameters['design_ui']['randomization'] as $input){
            if(!in_array($input['code'], $currentInputKeys)){
                if($input['ui']['defaultValue'] == NULL){
                    // needs update for the API side
                } else {
                    $jsonInput[$input['code']] = $input['ui']['defaultValue'];
                }
            }
        }

        foreach($jsonUIParameters['design_ui']['layout'] as $input){
            if(!in_array($input['code'], $currentInputKeys)){
                if($input['ui']['defaultValue'] == NULL){
                    // needs update for the API side
                } else {
                    $jsonInput[$input['code']] = $input['ui']['defaultValue'];
                }
            }
        }

        //build entryList
        if(in_array('entryList', $inputKeysArray)){

            //create input json array
    
            $entryListId = $this->mainEntryList->getEntryListId($id);
            $totalEntriesRecord = $this->mainEntry->searchAll(['entryListDbId'=>"equals ".$entryListId], 'sort=entryNumber:ASC');

            //Get crop
            $entryList['entry_id'] = array_column($totalEntriesRecord['data'], 'entryDbId');
            $entryList['entry_number'] = array_column($totalEntriesRecord['data'], 'entryNumber');
            $entryList['entry_type'] = array_column($totalEntriesRecord['data'], 'entryType');
            $entryList['entry_role'] = array_column($totalEntriesRecord['data'], 'entryRole');
            $entryList['entry_status'] = array_column($totalEntriesRecord['data'], 'entryStatus');
            $entryList['entry_name'] = array_column($totalEntriesRecord['data'], 'entryName');
            $entryList['entry_class'] = array_column($totalEntriesRecord['data'], 'entryClass');
            $entryList['germplasm_id'] = array_column($totalEntriesRecord['data'], 'germplasmDbId');

            if(isset($jsonUIParameters['design_ui']['groups']) && !empty($jsonUIParameters['design_ui']['groups'])){
                if(strtolower($design) == "augmented design"){
                    $entryReps = [];
                    $uiGroups = $jsonUIParameters['design_ui']['groups'];
                    foreach ($entryList['entry_id'] as $entryId) {
                        $entryReps[] = isset($uiGroups[$entryId]) ? intval($uiGroups[$entryId]) : 1;
                    }
                    $entryList['nrep'] = $entryReps;
                } else {
                    $groupEntries = [];
                    $entryReps = [];
                    foreach ($jsonUIParameters['design_ui']['groups'] as $group) {
                        for($i=0; $i<count($group['entryIds']); $i++){
                            $idx = array_search($group['entryIds'][$i], $entryList['entry_id']);
                            $totalEntriesRecord['data'][$idx]['repno'] = intval($group['repno']);
                        }   
                    }
                    $entryList['nrep'] = array_column($totalEntriesRecord['data'], 'repno');
                }
            }

            $jsonInputArray['entryList']=$entryList;
        }
        
        //generate occurrences
        if($occurrenceDbIds == null){
            $occurrenceExperiment = $this->occurrenceModel->searchAll(['experimentDbId'=>"equals ".$id], 'sort=occurrenceName:DESC&limit=1');
            if($occurrenceExperiment['totalCount'] < $occurrenceCount){

                $newCount = $occurrenceCount - $occurrenceExperiment['totalCount'];
                $totalCount = $occurrenceExperiment['totalCount'];

                for($i=0; $i<$newCount; $i++){
                    $occNum = $totalCount + ($i+1);
                    $occName = ($occNum < 10) ? '0'.$occNum : $occNum; 
                    $occurrenceName = $experiment['experimentName'].'#OCC-'.$occName;//to be replaced with occurrence browser
                    $occurrenceStatus = 'draft';
                    $records = [
                        'occurrenceStatus' => $occurrenceStatus,
                        'experimentDbId' => "$id"
                    ];
                    $requestData['records'][] = $records;
                }
                //Debug occurrence creation
                $this->occurrenceModel->create($requestData);

            } else if($occurrenceExperiment['totalCount'] > $occurrenceCount){

                $toBeDeletedCnt = $occurrenceExperiment['totalCount'] - $occurrenceCount;
                $occurrencesIds = array_column($occurrenceExperiment['data'], 'occurrenceDbId');
                $toBeDeleted = array();

                for($i=0; $i<$toBeDeletedCnt; $i++){
                    $toBeDeleted[] = $occurrencesIds[$i];
                }

                //delete excess occurrences
                $this->occurrenceModel->deleteMany($toBeDeleted);
            }
            $occurrenceExperiment = $this->occurrenceModel->searchAll(['experimentDbId'=>"equals ".$id]);
        } else {
            $occurrenceExperiment = $this->occurrenceModel->searchAll(['occurrenceDbId'=>"equals ".implode("|equals ", $occurrenceDbIds)]);
        }
        
        $metadata['occurrence_id'] = array_column($occurrenceExperiment['data'], 'occurrenceDbId');
        
        $jsonInputArray['metadata'] = $metadata;
        $jsonInputArray['parameters']=$jsonInput;

        return $jsonInputArray;


    }

    /**
     * Get factors of n with limit m
     *
     * @param integer $min
     * @param integer $n
     */
    public function getFactorsWithLimit($min, $n) {
        $factors_array = array();
        for ($x = 1;$x <= sqrt(abs($n)); $x++) {
            if ($n % $x === 0) {
                $z = $n / $x;
                if ($z > $min) {
                    if(!array_key_exists($z, $factors_array)){
                        $factors_array[$z] = $z;
                    }
                }
                if ($x > $min) {
                    if(!array_key_exists($x, $factors_array)){
                        $factors_array[$x] = $x;
                    }
                }
            }
        }
        return $factors_array;
    }

    /**
     * Get all factors of integer n
     *
     * @param integer $n
     */
    public function getAllFactorsOfInteger($n) {
        $factors_array = array();
        for ($x = 1; $x <= sqrt(abs($n)); $x++) {
            if ($n % $x === 0) {
                $z = $n / $x;
                if (array_search($z, $factors_array) === false) {
                    $factors_array[]=$z;
                }
                if (array_search($x, $factors_array) === false) {
                    $factors_array[]=$x;
                }
            }
        }
        return $factors_array;
    }

    /***
     * Get all nurseries
     * 
     * @param integer $id primary key of the experiment
     */
    public function getNurseries($id){

            $experimentRecord = Experiment::getExperiment($id);
            $programId = !empty($experimentRecord) ? $experimentRecord['programDbId']:null;
            
            $experimentRecords = ExperimentModel::getExperimentsWithPackages([], $programId);
           
            
            $dataProvider = new ArrayDataProvider([
                'allModels' => $experimentRecords['experiments'],
                'key' => 'experimentDbId',
                'sort' => [
                    'defaultOrder' => ['experimentYear'=>SORT_DESC,'experimentDbId'=>SORT_ASC],
                    'attributes' => ['experimentDbId', 'stageCode', 'projectCode','experimentCode', 'experiment', 'experimentYear','entryCount']
                
                ],
                'id' => 'from-nursery-modal-browser',
                'pagination' => false,
            ]);
            
            return [
                'dataProvider' => $dataProvider,
                'partitionView' => $experimentRecords['partitionView']
            ];
    }

    /**
     * Retrieve all occurrences based on entry list type
     * 
     * @param Integer $id primary key of the experiment
     * @param String $entryListType valid values - parent list, entry list, cross-list
     * 
     */
    public function getOccurrences($id, $entryListType){

        $occurrenceRecords = $entryListRecords = [];
        $experimentRecord = $this->mainExperiment->getExperiment($id);
        $partition = false;

        $entryListRecords = $this->mainEntryList->searchAll(['fields'=>'entryList.experiment_id as experimentDbId|entryList.entry_list_type as entryListType',
            'entryListType' => $entryListType, 'experimentDbId' => "not equals $id"])['data'];

        if(isset($entryListRecords[0]['experimentDbId'])){

            $experimentIdList = implode('|equals ',array_column($entryListRecords, 'experimentDbId'));

            $requestData = [
                'occurrenceStatus' => 'planted%', //planted and planted;crossed
                'experimentDbId' => "equals $experimentIdList",
                'programDbId' => "equals ".$experimentRecord['programDbId'],
                'experimentSeasonDbId' => "equals ".$experimentRecord['seasonDbId'].'',
                'experimentYear' => "equals ".$experimentRecord['experimentYear'].'',
            ];

            $dataProcessAbbrev = $experimentRecord['dataProcessAbbrev'];

            if($dataProcessAbbrev == 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'){
                $occurrenceRecord = $this->occurrenceModel->searchAll(['experimentDbId' => "equals $id"]);
                

                if(isset($occurrenceRecord['data'][0]['siteDbId']) && !empty($occurrenceRecord['data'][0]['siteDbId']) && $occurrenceRecord['data'][0]['siteDbId'] != NULL){
                    $requestData['siteDbId'] =  "equals ".$occurrenceRecord['data'][0]['siteDbId'].'';
                }
            }
            
            $configData = ExperimentModel::retrieveConfiguration('specify-occurrences', $experimentRecord['programCode'], $id, $experimentRecord['dataProcessDbId']);
            foreach($configData as $config){
                if($config['variable_abbrev'] == 'SITE' && isset($config['default']) && !empty($config['default'])){
                    $requestData['siteCode'] = $config['default'];
                }
            }
        
            $occurrenceRecords = $this->occurrenceModel->searchAll($requestData)['data'];
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $occurrenceRecords,
            'key' => 'experimentDbId',
            'sort' => [
                'defaultOrder' => ['experimentYear'=>SORT_DESC,'experimentDbId'=>SORT_ASC],
                'attributes' => [
                    'occurrenceDbId',
                    'occurrenceCode',
                    'occurrenceName',
                    'programDbId',
                    'programCode',
                    'program',
                    'projectDbId',
                    'projectCode',
                    'project',
                    'siteDbId',
                    'siteCode',
                    'site',
                    'fieldDbId',
                    'fieldCode',
                    'field',
                    'geospatialObjectId',
                    'geospatialObjectCode',
                    'geospatialObject',
                    'geospatialObjectType',
                    'experimentDbId',
                    'experimentCode',
                    'experiment',
                    'experimentType',
                    'experimentStageDbId',
                    'experimentStageCode',
                    'experimentStage',
                    'experimentYear',
                    'experimentSeasonDbId',
                    'experimentSeasonCode',
                    'experimentSeason',
                    'experimentDesignType',
                    'experimentStewardDbId',
                    'experimentSteward',
                    'repCount',
                    'entryCount',
                    'plotCount',
                    'locationDbId',
                    'occurrenceStatus',
                    'description',
                    'contactPerson',
                    'creationTimestamp',
                    'creatorDbId',
                    'creator',
                    'modificationTimestamp',
                    'modifierDbId',
                    'modifier',
                    'occurrenceDocument',	
                ] 
            ],
            'pagination' => false,
            'id' => 'from-occurrences-modal-browser'
        ]);
        
        return [
            'dataProvider' => $dataProvider,
            'partitionView' => $partition
        ];

    }

    /**
     * Get no. of germplasm with packages within an experiment
     *
     * @param integer $id primary key of the experiment
     */
    public function getNoOfGermplasmWithPackages($id){
        
        $filter = [
            "fields" => "experiment.id AS \"experimentDbId\"| \"entry\".entry_number AS \"seedSourceEntryNumber\"",
        	"distinctOn" => "experimentDbId",
            "experimentDbId" => "equals ".$id
        ];
        
        $packages = Package::searchAll($filter, "limit=1");
     
        $uniqueEntriesCnt = 0;
        if($packages['totalCount'] > 0){
            $entryNumbers = array_unique(array_column($packages['data'], 'seedSourceEntryNumber')); 
            
            $uniqueEntriesCnt = count($entryNumbers);
        }

        return $uniqueEntriesCnt;
    }

    /**
     * Get no. of germplasm with packages within an experiment
     *
     * @param integer $id primary key of the experiment
     * @param integer $programDbId primary key of the program
     */
    public function getExperimentsWithPackages($experimentIds, $programDbId = null){
        
        $experiments = [];
       
        $sort = 'sort=experimentYear:desc&experimentDbId:asc';
        $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'renderAdvancedNursery')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'renderAdvancedNursery') : 100;

        $sqlData = [
            "fields" => "experiment.id AS experimentDbId|stage.stage_code AS stageCode|project.project_code AS projectCode|experiment.experiment_code AS experimentCode|
            experiment.experiment_name AS experimentName|experiment.experiment_year AS experimentYear|season.season_code AS seasonCode|experiment.experiment_type AS experimentType|experiment.experiment_status AS experimentStatus|experiment.program_id as programDbId",
            "experimentType" => "%nursery%",
            "experimentStatus" => "equals completed|equals planted|equals planted;crossed"
        ];
        if($programDbId != null){
            $sqlData['programDbId'] = "equals ".$programDbId;
        }
        
        $experimentsData = $this->mainExperiment->searchAll($sqlData, 'limit=1&sort=experimentDbId:desc', false);
        $partition = false;
        if($experimentsData['totalCount'] > $limit){
            $partition = true;
        }
        $path = 'experiments-search';
        if($limit < 100){
            $path = $path . '?limit='.$limit;
        }
        $result = Yii::$app->api->getResponseWithLimit('POST',$path, json_encode($sqlData), $sort, true, null, $limit);
        $experiments = $result['body']['result']['data'];   

        return [
            'experiments' => $experiments,
            'partitionView' => $partition
        ];
    }

    /**
     * Get harvested plots from the experiment
     *
     * @param integer $id primary key of the experiment
     * @param integer $programDbId primary key of the program
     */
    public function getHarvestedPlots($id, $programDbId=null, $withPagination=false){
    
        $filter = [
            "fields"=>"seed.id AS seedDbId|germplasm.id AS germplasmDbId|germplasm.parentage AS parentage|germplasm.generation AS generation|experiment.experiment_year AS year|germplasm.designation AS designation|experiment.id AS experimentDbId| entry.entry_number AS seedSourceEntryNumber| seed.source_entry_id AS seedSourceEntryDbId| plot.plot_number AS seedSourcePlotNumber 
                       | package.package_label AS label | package.id AS packageDbId | package.package_code AS packageCode|germplasm.germplasm_state AS germplasmState|germplasm.germplasm_type AS germplasmType|package.program_id as programDbId",
            "experimentDbId" => "equals ".$id
        ];
        
        if($programDbId != null){
            $filter['programDbId'] = "equals ".$programDbId;
        }
        
        if($withPagination){
            $packagesArray = $this->mainPackage->searchAll($filter, 'sort=packageCode:ASC');
        } else {
            $packagesArray = $this->mainPackage->searchAll($filter, 'sort=packageCode:ASC', $withPagination);
        }

        return [
            'entries' => $packagesArray['data'],
            'count' => $packagesArray['totalCount']
        ];
    }

    /**
     * Get harvested plot count from the experiment
     *
     * @param integer $id primary key of the experiment
     * @param integer $programDbId primary key of the program
     */
    public function getHarvestedPlotsCount($id, $programDbId=null){
    
        $filter = [
            "fields"=>"experiment.id AS experimentDbId|package.program_id as programDbId",
            "experimentDbId" => "equals ".$id
        ];

        if($programDbId != null){
            $filter['programDbId'] = "equals ".$programDbId;
        }
        
        $packagesArray = $this->mainPackage->searchAll($filter, 'limit=1', false);

        return $packagesArray['totalCount'];
    }

    /**
     * Get harvested records from the experiment
     *
     * @param Integer $selectedExperimentId indentifier of selected experiment
     * @param Integer $experimentId primary key of the experiment
     * @param Array $entries list of entry ids
     * @param Array $configData list of configuration variables
     * @param Integer $bgThreshold Background process limit
     * @param String $program Program Abbrev indetifier
     * @param String $experimentType Working experiment type
     */
    public function createEntriesFromNurseries($selectedExperimentId, $experimentId, $entries, $configData, $bgThreshold, $program, $totalCount, $experimentType = null) {
        // retrieve default values
        $entryType = null;
        $entryClass = null;
        $entryRole = null;
        foreach($configData as $config){
            if(isset($config['default']) && !empty($config['default'])){
                switch($config['variable_abbrev']){
                    case 'ENTRY_TYPE':
                        $entryType = $config['default'];
                        break;
                    case 'ENTRY_CLASS':
                        $entryClass = $config['default'];
                        break;
                    case 'ENTRY_ROLE':
                        $entryRole = $config['default'];
                        break;
                    default:
                        break;
                }
            }
        }
        //Get entry list
        $currentEntryListId = $this->mainEntryList->getEntryListId($experimentId, true);
        $experiment = $this->mainExperiment->getExperiment($experimentId);
        
        //add notes for BT experiment
        $this->addDesignNoteForUpdate($experimentId, "updated entries");

        if($totalCount > $bgThreshold){
            $userModel = new User();
            $userId = $userModel->getUserId();

            $token = \Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = \Yii::$app->session->get('user.refresh_token');
            //call background process for creating entries
            $addtlParams = ["description"=>"Creating of entry records","entity"=>"ENTRY","entityDbId"=>$experimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'entity'=>'nursery',
                'entityId' => $selectedExperimentId,
                'defaultValues' => [
                    'entryListDbId' => $currentEntryListId,
                    'entryType' => $entryType,
                    'allowedEntryType' => null,
                    'entryClass' => $entryClass,
                    'entryRole' => $entryRole,
                    'entries' => $entries
                ],
                'processName' => 'create-entry-records',
                'experimentDbId' => $experimentId,
                'tokenObject' => [
                    'token' => $token,
                    'refreshToken' => $refreshToken
                ],
                'personDbId' => $userId
            ];
            $create = $this->mainExperiment->create($dataArray, true, $addtlParams);

            $experimentStatus = !empty($experiment) ? $experiment['experimentStatus'] : null;
            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            } else {
                $status = [];
            }

            if(!empty($create['data'])){
                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    
                    $updatedStatus = implode(';', $status);
                    $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                
                    $this->mainExperiment->updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

                    //update experiment
                    Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentId]);
                }
            } else {
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentId]);
            }
        } else {
            $createEntries = array();
            $dataEntries = $this->getHarvestedPlots($selectedExperimentId, $experiment['programDbId'], true);

            $entries = $dataEntries['entries'];
            foreach($entries as $record){
                $entryType = !empty($entryType) ? $entryType : 'test';
                $entryStatus = 'active';
                $entryName = $record['designation'];
                $seedDbId = isset($record['seedDbId']) ? $record['seedDbId'] : '';
                $germplasmDbId = $record['germplasmDbId'];
                $packageDbId = isset($record['packageDbId']) ? $record['packageDbId'] : '';

                $temp = [
                    "entryName" => "$entryName",
                    "entryType" => "$entryType",
                    "entryStatus" => "$entryStatus",
                    "entryListDbId" => "$currentEntryListId",
                    "germplasmDbId" => "$germplasmDbId"
                ];
                if(!empty($entryClass)){
                    $temp['entryClass'] = $entryClass;
                }
                if(!empty($experimentType) && ($experimentType != 'Breeding Trial' && $experimentType != 'Observation')){
                    if(!empty($entryRole)){
                        $temp['entryRole'] = $entryRole;
                    }
                }
              
                if(!empty($seedDbId)){
                    $temp['seedDbId'] = "$seedDbId";
                }
                if(!empty($packageDbId)){
                    $temp['packageDbId'] = "$packageDbId";
                }

                $createEntries[] = $temp;
            }
            //Create records
            $createdEntryIds = $this->mainEntry->create(["records"=>$createEntries]);
            //update experiment status
            $this->mainEntryOrderModel->entryListAfterUpdate($experimentId, $createdEntryIds);

        }
        return true;
    }
    
    /**
     * Get all created experiments within the program
     *
     * @param integer $id primary key of the study
     */
    public function getExistingExperimentsDataProvider($id, $programId=NULL){
        
        if($programId != NULL){
            $filter = ["fields"=>'entryCount|experiment.id as experimentDbId|
                experiment.experiment_code AS experimentCode|
                experiment.experiment_name AS experimentName|
                experiment.experiment_year AS experimentYear|
                experiment.experiment_status AS experimentStatus|
                project.project_name AS projectName|
                experiment.program_id AS programDbId|
                stage.stage_code AS stageCode',"experimentDbId"=>"not equals ".$id,"experimentStatus" => "in: ('created', 'planted', 'planted;crossed', 'mapped','completed')", "programDbId"=>"equals ".$programId];
        } else {
            $filter = ["fields"=>'entryCount|experiment.id as experimentDbId|
                experiment.experiment_code AS experimentCode|
                experiment.experiment_name AS experimentName|
                experiment.experiment_year AS experimentYear|
                experiment.experiment_status AS experimentStatus|
                project.project_name AS projectName|
                experiment.program_id AS programDbId|
                stage.stage_code AS stageCode',"experimentDbId"=>"not equals ".$id,"experimentStatus" => "in: ('created', 'planted', 'planted;crossed', 'mapped','completed')"];
        }

        $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'renderCopyEntryList')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'renderCopyEntryList') : 500;
        $experimentsData = $this->mainExperiment->searchAll($filter, 'limit=1&sort=experimentDbId:desc', false);
        $partition = false;
        if($experimentsData['totalCount'] > $limit){
            $partition = true;
        }

        $path = 'experiments-search';
        if($limit < 100){
            $path = $path . '?limit='.$limit;
        }
        $result = Yii::$app->api->getResponseWithLimit('POST',$path, json_encode($filter), '', true, null, $limit);
        $result = $result['body']['result']['data'];
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'key' => 'experimentDbId',
            'sort' => [
                'defaultOrder' => ['experimentYear'=>SORT_DESC,'experimentDbId'=>SORT_DESC],
                'attributes' => ['experimentDbId', 'stageCode', 'projectCode','experimentCode', 'experimentName', 'experimentYear','entryCount']
            ],
            'id' => 'experiment-data-provider'
        ]);
        return [
            'dataProvider' => $dataProvider,
            'partitionView' => $partition
        ];
    }

    /**
     * Save selected packages for the entry/entries
     *
     * @param string $command sql command
     * @param array $data array of data
     */
    public function autoSelectSinglePackage($entryListDbId, $experimentId){

        $entries = Entry::searchRecords([
            'fields'=>"entry.entry_list_id as entryListDbId|entry.id as entryDbId|entry.germplasm_id as germplasmDbId|entry.seed_id as seedDbId|entry.package_id as packageDbId",
            'entryListDbId'=>"equals $entryListDbId", 
            'packageDbId'=>'null']);
        $experimentModel = Experiment::searchAll(["fields"=>"experiment.id AS experimentDbId| program.program_code AS programCode",'experimentDbId'=>"equals ".$experimentId]);  

        $germplasmIds = [];
        $noPackagesEntry = [];
        $noGermplasmPackages = [];
        foreach($entries as $entry){
            if($entry['packageDbId'] == null){
                $germplasmIds[] = $entry['germplasmDbId'];
                $noPackagesEntry[$entry['entryDbId']] = $entry['germplasmDbId'];
                $noGermplasmPackages[$entry['germplasmDbId']][]=$entry['entryDbId'];
            }
        }
        
        if(!empty($germplasmIds)){

            foreach($germplasmIds as $gId){
                $filterPackages = [
                    "fields"=>"seed.id AS seedDbId|germplasm.id AS germplasmDbId|program.program_code AS programCode|package.id as \"packageDbId\"",
                    "germplasmDbId" => "equals ".$gId,
                    "programCode" => "equals ".$experimentModel['data'][0]['programCode'],
                    "distinctOn"=>"germplasmDbId",
                    "addPackageCount"=>true
                ];
                $dataLevel = 'dataLevel=germplasm';
                $results = Yii::$app->api->getResponse('POST', 'seed-packages-search?'.$dataLevel, json_encode($filterPackages), 'limit=1&sort=seedDbId:desc', false);
                $packageInfo = $results['body']['result']['data'];
                $totalCount  = $results['body']['metadata']['pagination']['totalCount'];
               
                if($totalCount > 0){
                    $germplasmDbId = $packageInfo[0]['germplasmDbId'];
                    foreach($noGermplasmPackages[$germplasmDbId] as $key){
                        if($packageInfo[0]['packageCount'] == 1){
                            $packageDbId = $packageInfo[0]['packageDbId'];
                            $seedDbId = $packageInfo[0]['seedDbId'];
                            Entry::updateRecord($key, ["seedDbId"=>"$seedDbId", "packageDbId"=>"$packageDbId"]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Save selected seed lot for the entry/entries
     *
     * @param Integer $entryListDbId record id of entry list
     * @param Integer $experimentId recordd id of the experiment
     * @param Array $entries list of entries without seeds
     */
    public function autoSelectSinglePackageInit($entryListDbId, $experimentId, $entries){

        $experimentModel = $this->mainExperiment->searchAll(["fields"=>"experiment.id AS experimentDbId| program.program_code AS programCode",'experimentDbId'=>"equals ".$experimentId]);  

        $germplasmIds = [];
        $noPackagesEntry = [];
        foreach($entries as $entry){
            $germplasmIds[] = $entry['germplasmDbId'];
            $noPackagesEntry[$entry['entryDbId']] = $entry['germplasmDbId'];
        }
        
        if(!empty($germplasmIds) && !empty($germplasmIds)){
            $filterPackages = [
                "fields"=>"seed.id AS seedDbId|germplasm.id AS germplasmDbId|germplasm.designation AS designation|program.program_code AS programCode|package.id as \"packageDbId\"",
                "germplasmDbId" => "equals ".implode("|equals ", array_unique($germplasmIds)),
                "programCode" => "equals ".$experimentModel['data'][0]['programCode'],
                "distinctOn"=>"germplasmDbId",
                "addPackageCount"=>true
            ];
            $dataLevel = 'dataLevel=germplasm';
            $results = Yii::$app->api->getResponse('POST', 'seed-packages-search?'.$dataLevel, json_encode($filterPackages), '', true);
            $packageInfo = $results['body']['result']['data'];
            $totalCount  = $results['body']['metadata']['pagination']['totalCount'];
       
            if($totalCount > 0){
                $germplasmIdArray = array_column($packageInfo, 'germplasmDbId');
                foreach($noPackagesEntry as $key => $value){
                    $idx = array_search($value, $germplasmIdArray);
                    if($idx !== false && isset($packageInfo[$idx]) && (int)$packageInfo[$idx]['packageCount'] == 1){
                        $packageDbId = (int)$packageInfo[$idx]['packageDbId'];
                        $seedDbId = (int)$packageInfo[$idx]['seedDbId'];
                        Entry::updateRecord($key, ["seedDbId"=>"$seedDbId", "packageDbId"=>"$packageDbId"]);
                    }
                }
            }
        }
    }

    /**
     * Sql for searching product list
     *
     * @param string $command sql command
     * @param array $data array of data
     */
    public function searchProductList($command, $data){

        extract($data);

        if($command == 'normalize'){

            return GermplasmName::getStandardizedGermplasmName($inputListSplitArr);
            
        }
        else if($command == 'search'){

            //search for each germplasm
            $notFoundProductArray = [];

            $orderNumber = 0;
            $inputListSplitArr = $normInputProductNameResults;

            $searchNames = 'equals '.implode('|equals ', $inputListSplitArr);

            $filters['names'] = $searchNames;
            $data = $this->germplasm->searchAll($filters);
            $foundProducts = $data['data'];

            $arrangedArray = [];
            $normalizedNameArray = array_column($foundProducts, 'germplasmNormalizedName');
            $designationArray = array_column($foundProducts, 'designation');
            $otherNamesArray = array_column($foundProducts, 'otherNames');
            $otherNamesArray = array_map('strtoupper',$otherNamesArray);
            $productFound = false;
            $germplasmDbIds = array();
            $germplasmCounter = array();
            $duplicateGermplasm = false;

            foreach($inputListSplitArr as $inputProduct){

                $productFound = false;
                $orderNumber+=1;

                // normalize product name
                $normalizedProductName = GermplasmName::getStandardizedGermplasmName([$inputProduct]);
                
                $normalizedProductName = isset($normalizedProductName) && !empty($normalizedProductName) ? $normalizedProductName[0]['norm_input_product_name'] : $inputProduct;

                if (in_array($normalizedProductName, $normalizedNameArray) || in_array($inputProduct, $designationArray)) {
                    
                    $index = array_search($normalizedProductName, $normalizedNameArray);
                    $index = $index === false ? array_search($normalizedProductName, $designationArray): $index;
                    
                    if(!in_array($foundProducts[$index]['germplasmDbId'], $germplasmDbIds)){
                        $duplicateGermplasm = true;
                    }
                    $temp = [
                        'germplasmDbId' => $foundProducts[$index]['germplasmDbId'],
                        'germplasmNormalizedName' => $foundProducts[$index]['germplasmNormalizedName'],
                        'inputProductName' => $inputProduct,
                        'designation' =>$foundProducts[$index]['designation'],
                        'orderNumber' => $orderNumber
                    ];
                    
                    if(strpos($dataProcessAbbrev, '_TRIAL_DATA_PROCESS') !== false){
                        $germplasmDbIds[] = $foundProducts[$index]['germplasmDbId'];
                    }
                    $arrangedArray[] = $temp;

                    if(isset($germplasmCounter[$foundProducts[$index]['germplasmDbId']])){
                        $germplasmCounter[$foundProducts[$index]['germplasmDbId']]++;
                    } else {
                        $germplasmCounter[$foundProducts[$index]['germplasmDbId']]=1;
                    }
                    $productFound = true;
                } else {
                    foreach ($otherNamesArray as $index => $otherNames) {
                        $splitNames = explode(';', $otherNames);

                        // normalize other names
                        $normOtherNames = GermplasmName::getStandardizedGermplasmName($splitNames);
                        $normOtherNames = array_column($normOtherNames,'norm_input_product_name');

                        if(in_array(strtoupper($normalizedProductName), $normOtherNames)){
                            if(!in_array($foundProducts[$index]['germplasmDbId'], $germplasmDbIds)){
                                $temp = [
                                    'germplasmDbId' => $foundProducts[$index]['germplasmDbId'],
                                    'germplasmNormalizedName' => $foundProducts[$index]['germplasmNormalizedName'],
                                    'inputProductName' => $inputProduct,
                                    'designation' =>$foundProducts[$index]['designation'],
                                    'orderNumber' => $orderNumber
                                ];

                                if(strpos($dataProcessAbbrev, '_TRIAL_DATA_PROCESS') !== false){
                                    $germplasmDbIds[] = $foundProducts[$index]['germplasmDbId'];
                                }
                                $arrangedArray[] = $temp;

                                if(isset($germplasmCounter[$foundProducts[$index]['germplasmDbId']])){
                                    $germplasmCounter[$foundProducts[$index]['germplasmDbId']]++;
                                } else {
                                    $germplasmCounter[$foundProducts[$index]['germplasmDbId']]=1;
                                }
                            }

                            $productFound = true;
                            break;
                        }
                    }
                }

                if (!$productFound) {
                    $notFoundProductArray[] = $inputProduct;
                }
            }

            return [
                'arrangedArray' => $arrangedArray,
                'foundProductArray' => $arrangedArray,
                'notFoundProductArray' => $notFoundProductArray,
                'germplasmCounter' => $germplasmCounter
            ];
        }
    }

    /**
     * Search package label list
     *
     * @param array $data array of data
     * @param int $programDbId record id of program
     */
    public function searchPackageLabels($data, $programDbId){
        //search for each germplasm
        extract($data);
        $notFoundArray = [];

        $orderNumber = 0;
        $inputListSplitArr = $packageLabels;
        $searchNames = 'equals '.implode('|equals ', $inputListSplitArr);

        $filters['fields']= "seed.id AS seedDbId|germplasm.id AS germplasmDbId|germplasm.designation|germplasm.parentage AS parentage|germplasm.generation AS generation|package.package_label AS packageLabel | package.id AS packageDbId | package.package_code AS packageCode|germplasm.germplasm_type AS germplasmType|package.program_id as programDbId";
        $filters['packageLabel'] = $searchNames;
        $filters['programDbId'] = "equals $programDbId";
        $data = $this->mainPackage->searchAll($filters)['data'];
        $foundRecords = $data;

        $arrangedArray = [];
        $packageLabelArray = array_column($foundRecords, 'packageLabel');
        $packageLabelArray = array_map('trim',$packageLabelArray);
        $designationArray = array_column($foundRecords, 'designation');
        $recordFound = false;
        $packageDbIds = array();
        $packageCounter = array();
        $duplicatePackage = array();
        $packageCountArr = array_count_values(array_column($foundRecords, 'packageLabel'));

        //check duplicates in input
        $duplicates = [];
        $results = [];
        foreach ($inputListSplitArr as $item) {
            if (in_array($item, $results)) {
                $duplicates[] = $item;
            }

            $results[] = $item;
        }
        $duplicateLabels = array_unique($duplicates);
        
        //check duplicates in search records
        $duplicatePackageRec = [];
        $results = [];
        foreach ($packageLabelArray as $item) {
            if (in_array($item, $results)) {
                $duplicatePackageRec[] = $item;
            }

            $results[] = $item;
        }
        $duplicatePackageArr = array_unique($duplicatePackageRec);
        
        foreach($inputListSplitArr as $input){
            $recordFound = false;
            $orderNumber+=1;

            $index = array_search(trim($input), $packageLabelArray);
            
            if($index !== false && !in_array($foundRecords[$index]['packageDbId'], $packageDbIds)   && !in_array($foundRecords[$index]['packageDbId'], $duplicatePackage)){
                $temp = [
                    'germplasmDbId' => $foundRecords[$index]['germplasmDbId'],
                    'packageDbId' => $foundRecords[$index]['packageDbId'],
                    'seedDbId' => $foundRecords[$index]['seedDbId'],
                    'designation' => $foundRecords[$index]['designation'],
                    'packageLabel' => $foundRecords[$index]['packageLabel'],
                    'orderNumber' => $orderNumber,
                    'inputLabel' => $input
                ];

                if(strpos($dataProcessAbbrev, '_TRIAL_DATA_PROCESS') !== false){
                    $packageDbIds[] = $foundRecords[$index]['packageDbId'];
                }
                
                if(in_array($foundRecords[$index]['packageLabel'], $duplicatePackageArr)){
                    $idx = array_search($foundRecords[$index]['packageDbId'], $packageDbIds);
                    unset($packageDbIds[$idx]);
                    $packageDbIds = array_values($packageDbIds);
                    $recordFound = false;
                    $duplicatePackage[] = $foundRecords[$index]['packageDbId'];
                }else if(in_array($foundRecords[$index]['packageLabel'], $duplicateLabels)){
                    $arrangedArray[] = $temp;
                    $recordFound = true;
                } else {
                    $arrangedArray[] = $temp;
                    $recordFound = true;
                }
                
            }

            if (!$recordFound) {
                $notFoundArray[] = $input;
            }
        }
        
        return [
            'arrangedArray' => $arrangedArray,
            'foundProductArray' => $arrangedArray,
            'notFoundProductArray' => $notFoundArray,
            'duplicatePackage' => $duplicatePackage
        ];
    }

    /**
     * Search package code list
     *
     * @param array $data array of data
     * @param int $programDbId record id of program
     */
    public function searchPackageCodes($data, $programDbId){
        //search for each germplasm
        extract($data);
        $notFoundArray = [];

        $orderNumber = 0;
        $inputListSplitArr = $packageCodes;
        $searchNames = 'equals '.implode('|equals ', $inputListSplitArr);

        $filters['fields']= "seed.id AS seedDbId|germplasm.id AS germplasmDbId|germplasm.designation|germplasm.parentage AS parentage|germplasm.generation AS generation|package.package_label AS packageLabel | package.id AS packageDbId | package.package_code AS packageCode|germplasm.germplasm_type AS germplasmType|package.program_id as programDbId";
        $filters['packageCode'] = $searchNames;
        $filters['programDbId'] = "equals $programDbId";
        $data = $this->mainPackage->searchAll($filters)['data'];
        $foundRecords = $data;

        $arrangedArray = [];
        $packageCodeArray = array_column($foundRecords, 'packageCode');
        $packageCodeArray = array_map('trim',$packageCodeArray);
        $designationArray = array_column($foundRecords, 'designation');
        $recordFound = false;
        $packageDbIds = array();
        $packageCounter = array();
        $duplicatePackage = array();
        $packageCountArr = array_count_values(array_column($foundRecords, 'packageCode'));

        //check duplicates in input
        $duplicates = [];
        $results = [];
        foreach ($inputListSplitArr as $item) {
            if (in_array($item, $results)) {
                $duplicates[] = $item;
            }

            $results[] = $item;
        }
        $duplicateCodes = array_unique($duplicates);

        //check duplicates in search records
        $duplicatePackageRec = [];
        $results = [];
        foreach ($packageCodeArray as $item) {
            if (in_array($item, $results)) {
                $duplicatePackageRec[] = $item;
            }

            $results[] = $item;
        }
        $duplicatePackageArr = array_unique($duplicatePackageRec);

        foreach($inputListSplitArr as $input){
            $recordFound = false;
            $orderNumber+=1;

            $index = array_search(trim($input), $packageCodeArray);

            if($index !== false){
                $temp = [
                    'germplasmDbId' => $foundRecords[$index]['germplasmDbId'],
                    'packageDbId' => $foundRecords[$index]['packageDbId'],
                    'seedDbId' => $foundRecords[$index]['seedDbId'],
                    'designation' => $foundRecords[$index]['designation'],
                    'packageCode' => $foundRecords[$index]['packageCode'],
                    'orderNumber' => $orderNumber,
                    'inputCode' => $input
                ];

                if(strpos($dataProcessAbbrev, '_TRIAL_DATA_PROCESS') !== false){
                    $packageDbIds[] = $foundRecords[$index]['packageDbId'];
                }

                if(in_array($foundRecords[$index]['packageCode'], $duplicatePackageArr)){
                    $idx = array_search($foundRecords[$index]['packageDbId'], $packageDbIds);
                    unset($packageDbIds[$idx]);
                    $packageDbIds = array_values($packageDbIds);
                    $recordFound = false;
                    $duplicatePackage[] = $foundRecords[$index]['packageDbId'];
                }else if(in_array($foundRecords[$index]['packageCode'], $duplicateCodes)){
                    $duplicatePackage[] = $foundRecords[$index]['packageCode'];
                    $arrangedArray[] = $temp;
                    $recordFound = true;
                } else {
                    $arrangedArray[] = $temp;
                    $recordFound = true;
                }
            }

            if (!$recordFound) {
                $notFoundArray[] = $input;
            }
        }

        return [
            'arrangedArray' => $arrangedArray,
            'foundProductArray' => $arrangedArray,
            'notFoundProductArray' => $notFoundArray,
            'duplicatePackage' => $duplicatePackage
        ];
    }

    /**
     * Adding entries from experiment
     *
     * @param array $data array of data
     */
    public function addEntriesExperiment($data){

        extract($data);
        // retrieve default values
        $allowedEntryType = [];
        $entryType = null;
        $entryRole = null;
        $entryClass = null;
        $newEntryIds = [];
        $entryConfigColumns = [];
        foreach($configData as $config){
            if(isset($config['default']) && !empty($config['default'])){
                switch($config['variable_abbrev']){
                    case 'ENTRY_TYPE':
                        $entryType = $config['default'];
                        $variable = $this->variable->getVariableByAbbrev('ENTRY_TYPE');

                        $scales = isset($scales) ?? null;
                        $scaleValues = $this->scaleValue->getVariableScaleValues($variable['variableDbId'], $scales);

                        $allowedValues = $config['allowed_values'];
                        $scaleValueAbbrev = array_column($scaleValues, 'abbrev');
                        $allowedEntryType = [];
                        foreach($allowedValues as $allowed){
                            $index = array_search($allowed, $scaleValueAbbrev);
                            $allowedEntryType[]=$scaleValues[$index]['value'];
                        }
                        break;
                    case 'ENTRY_CLASS':
                        $entryClass = $config['default'];
                        break;
                    case 'ENTRY_ROLE':
                        $entryRole = $config['default'];
                        break;
                    default:
                        break;
                }
            }

            $entryConfigColumns[] = $config['variable_abbrev'];
        }

        $experiment = $this->mainExperiment->getExperiment($currentExperimentId);
    
        //Copy the entry list of the selected experiment to the current one
        $entryListData = $this->mainEntryList->getEntryList($experimentId);
        
        //Get existing entry list record
        $newEntryListDbId =  $this->mainEntryList->getEntryListId($currentExperimentId, true);
        
        $entryListDbId = !empty($entryListData) ? $entryListData['entryListDbId'] : null;

        //Copy the entries of the selected experiment to the current one
        $paramFilter = ["fields"=>"entry.entry_list_id as entryListDbId|entry.id as entryDbId","entryListDbId"=>"equals $entryListDbId"];

        //These are the entries of the experiment selected
        $entrySearchData =  $this->mainEntry->searchAll($paramFilter, 'limit=1&sort=entryDbId:asc', false);

        //These are the entries of the entry list of the experiment selected

        $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;

        //add notes for BT experiment
        $this->addDesignNoteForUpdate($currentExperimentId, "updated entries");

        if($entrySearchData['totalCount'] > $bgThreshold){
            $userModel = new User();
            $userId = $userModel->getUserId();
                
            $token = \Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = \Yii::$app->session->get('user.refresh_token');
            //call background process for creating entries
            $addtlParams = ["description"=>"Creating of entry records","entity"=>"ENTRY","entityDbId"=>$currentExperimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'entity'=>'experiment',
                'entityId' => $experimentId,
                'defaultValues' => [
                    'entryListDbId' => $newEntryListDbId,
                    'entryType' => $entryType,
                    'allowedEntryType' => $allowedEntryType,
                    'entryClass' => $entryClass,
                    'entryRole' => $entryRole,    
                    'entryConfigColumns' => $entryConfigColumns
                ],
                'processName' => 'create-entry-records',
                'experimentDbId' => $currentExperimentId."",
                'tokenObject' => [
                    'token' => $token,
                    'refreshToken' => $refreshToken
                ],
                'personDbId' => $userId
            ];
            
            $create = $this->mainExperiment->create($dataArray, true, $addtlParams);

            $experiment = $this->mainExperiment->getExperiment($currentExperimentId);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
            
            if(!empty($create['data'])){
                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    
                    $updatedStatus = implode(';', $status);
                    $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                
                    $this->mainExperiment->updateOne($currentExperimentId, ["experimentStatus"=>"$newStatus"]);

                    //update experiment
                    Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$currentExperimentId]);
                }
            } else {
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$currentExperimentId]);   
            }
            $newEntryIds['success'] = true;
            
        } else {

            $entryDataValues = [];
            $createEntries = null;
            $toCopyEntryData = [];
            $paramFilter['fields'] = 'entry.entry_number AS entryNumber|entry.entry_type AS entryType|entry.entry_name AS entryName|entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|entry.entry_role AS entryRole|entry.entry_class AS entryClass|entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.package_id AS packageDbId';
            $entrySearchData = $this->mainEntry->searchAll($paramFilter, 'sort=entryNumber:asc', true)['data'];

            foreach($entrySearchData as $entries){
                
                if(in_array($entries['entryType'], $allowedEntryType)){
                    $entryType = $entries['entryType'];
                } else { $entryType = ($entryType != null) ? $entryType : $entries['entryType']; }
                $entryName = $entries['entryName'];
                $entryStatus = !empty($entries['entryStatus']) ? $entries['entryStatus'] : 'active';
                $germplasmDbId = $entries['germplasmDbId'];
                $entryRole = !empty($entries['entryRole']) ? $entries['entryRole'] : $entryRole;
                $entryClass = !empty($entries['entryClass']) ? $entries['entryClass'] : $entryClass;
                $seedDbId = $entries['seedDbId'];
                $packageDbId = $entries['packageDbId'];

                $temp = [
                    'entryName' => $entryName,
                    'entryType' => $entryType,
                    'entryStatus' => $entryStatus,
                    'entryListDbId' => "$newEntryListDbId",
                    'germplasmDbId' => "$germplasmDbId"
                ];

                if(!empty($entryClass)){
                    $temp['entryClass'] = $entryClass;
                }

                if(!empty($experiment['experimentType']) && ($experiment['experimentType'] != 'Breeding Trial' && $experiment['experimentType'] != 'Observation' ) && !empty($entryRole)){
                    $temp['entryRole'] = $entryRole;
                }
              
                if(!empty($seedDbId)){
                    $temp['seedDbId'] = "$seedDbId";
                }

                if(!empty($packageDbId)){
                    $temp['packageDbId'] = "$packageDbId";
                }
                
                $toCopyEntryData = [];

                //Normal way
                $getEntryDataRec = $this->entryData->getEntryDataRecordsFiltered($entries['entryDbId'], $entryConfigColumns);
                if(!empty($getEntryDataRec)){
                    $entryDataValues[$entries['entryDbId']] = $getEntryDataRec;
                }
                
                $createEntries[] = $temp;
            }
        
            //Create record to entry tablecreateEntries
            if(!empty($toCopyEntryData)){
                $createAddtlParams = ["dependents"=>['entry-data'],"description"=>"Creation of entries","entity"=>"EXPERIMENT","entityDbId"=>$currentExperimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            }else{
                $createAddtlParams = ["description"=>"Creation of entries","entity"=>"EXPERIMENT","entityDbId"=>$currentExperimentId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            }
            
            //Normal saving of entry records
            $newEntryIds = $this->mainEntry->create(["records"=>$createEntries]);

            if($newEntryIds['success']){
                $newEntriesArray = $newEntryIds;
                $count = 0;
                foreach($entryDataValues as $key => $entryDataVal){
                    if(!empty($entryDataVal)){
                        $newEntryDbId = isset($newEntriesArray['data'][$count]['entryDbId']) ? $newEntriesArray['data'][$count]['entryDbId'] : null;
                        if(!empty($entryDataVal) && !empty($newEntryDbId)){
                            
                            foreach($entryDataVal as $eachEntryData){
                                $variableDbId = $eachEntryData["variableDbId"];
                                $dataValue = $eachEntryData["dataValue"];
                                $toCopyEntryData[] = ["entryDbId"=>"$newEntryDbId","variableDbId"=>"$variableDbId","dataValue"=>"$dataValue"];
                            }
                        }
                        $count++;
                    }
                }
                $this->autoSelectSinglePackage($newEntryListDbId, $experimentId);
                
                //update experiment status
                $this->mainEntryOrderModel->entryListAfterUpdate($currentExperimentId, $newEntryIds);
                
                if(!empty($toCopyEntryData)){
                    //Create the entry list data record
                    $entryDataInfo = $this->entryData->addRecord($toCopyEntryData);
                    if(!empty($entryDataInfo)){
                        $newEntryIds['success'] = true;
                    } else {
                        $newEntryIds['success'] = false;
                    }
                }  
            }
            return $newEntryIds;       
        }

        return $newEntryIds;
    }

    /**
    * Get the different status in the experiment table
    * TODO: refactor this function to use API
    * @param array $filter list of filter condition
    * @return array $options list of the current status of the experiments
    */
    public function getExperimentStatus($filter=NULL){
        if($filter == null){
            $experimentsData = Experiment::searchAll(["fields"=>'experiment.id as experimentDbId|experiment.experiment_status as "experimentStatus"',"experimentStatus"=>"not null",	"distinctOn" => "experimentStatus"]);
            $statusList = ArrayHelper::map($experimentsData['data'],'experimentStatus','experimentStatus');
        }else{
            $filter['fields'] = 'experiment.id as experimentDbId|experiment.experiment_status as "experimentStatus"';
            $experimentsData = Experiment::searchAll($filter);
            $statusList = ArrayHelper::map($experimentsData['status'],'experimentStatus','experimentStatus');
        }
        
        $options = [];
        foreach ($statusList as $value) {
            $tempVal = explode(';',$value);

            foreach($tempVal as $val){
                if(!in_array($val,$options)){
                    $options[$val] = $val; 
                }
            }
        }
    
        return $options;
    }

    

    /**
     * Generate plots of the experiment
     * 
     * @param Integer $experimentId experiment identifier
     * @param Integer $getExperimentTotalPlots Total count of plots in an experiment
     */
    public function generatePlots($experimentDbId, $getExperimentTotalPlots=null){

        $occurrenceRecords = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals ".$experimentDbId]);
        $occurrenceIds = '';
        $occurrences = $occurrenceRecords['data'];
        if($occurrenceRecords['totalCount'] > 0){
            $occurrenceIds = array_column($occurrenceRecords['data'], 'occurrenceDbId');
        }

        $experimentRecord = $this->mainExperiment->getExperiment($experimentDbId);

        $plotRecordscheck = $this->plotModel->searchAll(["fields"=>"plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], 'limit=1&sort=occurrenceDbId:desc',false)['totalCount'];

        if($plotRecordscheck == 0){
            $experimentModel = $experimentRecord;
            $program = isset($experimentModel['programCode']) ? $experimentModel['programCode'] : null;
            $designType = isset($experimentModel['experimentDesignType']) ? $experimentModel['experimentDesignType'] : null;
        
            $experimentBlockRecords = $this->mainExperimentBlock->searchAll(["fields"=>"experimentBlock.experiment_id AS experimentDbId|experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.no_of_ranges AS noOfRows",'experimentDbId' =>  "equals ".$experimentDbId] , 'sort=experimentBlockName:ASC');
            $insertList = $plotRecords = [];
            $experimentBlocks = $experimentBlockRecords['data'];
    
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences') : 250;
            $createAddtlParams = ["description"=>"Creation of plots and/or planting instruction records","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"OCCURRENCE", "application"=>"EXPERIMENT_CREATION"];
            
            $totalPlotsCount = 0;
                
            foreach($experimentBlocks as $experimentBlock){
                if(!isset($experimentBlock['layoutOrderData']) && empty($experimentBlock['layoutOrderData'])){
                    continue;
                }
    
                $layoutOrderDataCnt = count($experimentBlock['layoutOrderData']);
                $totalPlotsCount += $layoutOrderDataCnt;
            }
            $totalPlotsCount = $totalPlotsCount * count($occurrences);

            $crossInfoCount = 0;
            if($experimentModel['dataProcessAbbrev'] == 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'){
                //check cross record count
                $crosses = $this->cross->searchAll(['fields'=>'germplasmCross.id as crossDbId|experiment.id as experimentDbId','experimentDbId' => 'equals '.$experimentDbId], 'limit=1&sort=crossDbId:desc', false);
                $crossInfoCount = ($crosses['totalCount'] * 3) + $totalPlotsCount; //updating of cross records and cross parents(cross count*2)
            }

            if($totalPlotsCount >= $bgThreshold || $crossInfoCount >= $bgThreshold){  //Use the background process
          
                //send request to worker
                $addtlParams = ["description"=>"Generating plots and planting instruction records","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"OCCURRENCE", "application"=>"EXPERIMENT_CREATION"];
                $dataArray = [
                    'records'=>$experimentRecord,
                    'processName' => 'generate-plots-plant_inst',
                    'experimentDbId' => $experimentDbId."",
                    'tokenObject' => [
                        'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                        'refreshToken' => Yii::$app->session->get('user.refresh_token')
                    ],
                ];
                $plotRecords = $this->mainExperiment->create($dataArray, true, $addtlParams);
        
                $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($plotRecords['data'][0]['backgroundJobDbId']);

                //Generate Plot records 1st
                if($bgStatus !== 'done' && !empty($bgStatus)){
                    $experimentStatus = isset($experimentModel['experimentStatus']) ? $experimentModel['experimentStatus'] : '';
            
                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }
            
                    $updatedStatus = implode(';', $status);
                    $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
                
                    $this->mainExperiment->updateOne($experimentDbId, ["experimentStatus"=>"$newStatus"]);

                    return [
                        'success' => true,
                        'message' => 'redirect'
                    ];

                } else {
                    //After plot records have been generated, generate the planting instruction records next
                    
                    if($plotRecords['success']){
                        return $plotRecords;
                    }else{
                        return [
                            'success' => false,
                            'message' => 'Problem in generating the planting instruction records'
                        ];

                    }
                }
            }else{
                foreach($occurrences as $occurrence){
                    $occurrenceId = $occurrence['occurrenceDbId'];
                    
                    foreach($experimentBlocks as $experimentBlock){
                        if(!isset($experimentBlock['layoutOrderData']) && empty($experimentBlock['layoutOrderData'])){
                            continue;
                        }
            
                        $layoutOrderData = $experimentBlock['layoutOrderData'];
                  
                        if($designType == "Entry Order" && $experimentBlock['noOfRows'] == 0){
                            foreach($layoutOrderData as $layout){
                                  
                                $entryDbId = isset($layout['entry_id']) ? $layout['entry_id'] : 0;
                                
                                $requestData = [
                                    'occurrenceDbId' => "$occurrenceId",
                                    'entryDbId' => "$entryDbId",
                                    'plotType' => 'plot',
                                    'plotNumber' => isset($layout['plotno']) ? $layout['plotno'].'' : '0',
                                    'rep' => isset($layout['repno']) ? $layout['repno'].'' : '0',
                                    'plotStatus' => 'active',
                                    'plotQcCode' => 'G',
                                    'blockNumber' => isset($layout['block_no']) ? $layout['block_no'].'' : '0'
                                ];

                                $insertList[] = $requestData;
                            }
                        } else {
                            foreach($layoutOrderData as $layout){
                                  
                                $entryDbId = isset($layout['entry_id']) ? $layout['entry_id'] : 0;

                                $requestData = [
                                    'occurrenceDbId' => "$occurrenceId",
                                    'entryDbId' => "$entryDbId",
                                    'plotType' => 'plot',
                                    'plotNumber' => isset($layout['plotno']) ? $layout['plotno'].'' : '0',
                                    'rep' => isset($layout['repno']) ? $layout['repno'].'' : '0',
                                    'plotStatus' => 'active',
                                    'designX' => isset($layout['field_x']) ? $layout['field_x'].'' : '0',
                                    'designY' => isset($layout['actual_field_y']) ? $layout['actual_field_y'].'' : '0',
                                    'plotQcCode' => 'G',
                                    'blockNumber' => isset($layout['block_no']) ? $layout['block_no'].'' : '0'
                                ];

                                $insertList[] = $requestData;
                            }
                        }
                    }
                }

                //Normal Creation of plot records
                if($experimentRecord['experimentType'] == 'Observation'){
                    $plotRecords = $this->plotModel->create(['autoGeneratePlotNumber'=>false,'records' => $insertList]);
                } else {
                    $plotRecords = $this->plotModel->create(['records' => $insertList]);
                }
                
                if($plotRecords['success']){
                    //Normal Creation of planting instruction records
                    return ExperimentModel::generatePlantingInstructions($experimentDbId);

                }else{
                    return [
                        'success' => false,
                        'message' => 'Problem in generating the planting instruction records'
                    ];

                }
              
            }
        }else{
            //Generate planting instruction only
            return ExperimentModel::generatePlantingInstructions($experimentDbId);

        }
    }
   


    /**
     * Generate planting instructions of the experiment
     * 
     * @param Integer $experimentDbId experiment identifier
     */
    public function generatePlantingInstructions($experimentDbId){
        $insertedRecord = $insertList = [];

        $occurrenceRecords = $this->occurrenceModel->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals ".$experimentDbId]);
        
        $occurrenceIds = '';
        $occurrences = $occurrenceRecords['data'];
        if($occurrenceRecords['totalCount'] > 0){
            $occurrenceIds = array_column($occurrenceRecords['data'], 'occurrenceDbId');
        }
        $plotRecordscheck = $this->plotModel->searchAll(["fields"=>"plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], 'limit=1&sort=occurrenceDbId:desc',false)['totalCount'];
        
        $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences') : 250;
        $createAddtlParams = ["description"=>"Creation of planting instruction records","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"OCCURRENCE", "application"=>"EXPERIMENT_CREATION"];

        $experimentModel = $this->mainExperiment->searchAll(["fields"=>"experiment.id AS experimentDbId|program.program_code AS programCode|experiment.experiment_status AS experimentStatus|experiment.experiment_name AS experimentName|experiment.experiment_type as experimentType|experiment.program_id as programDbId|item.abbrev AS dataProcessAbbrev",'experimentDbId'=>"equals ".$experimentDbId]);
        $program = isset($experimentModel['data'][0]['programCode']) ? $experimentModel['data'][0]['programCode'] : null;
        $programId = isset($experimentModel['data'][0]['programDbId']) ? $experimentModel['data'][0]['programDbId'] : null;
        if($plotRecordscheck>= $bgThreshold){
            
            $addtlParams = ["description"=>"Generating planting instruction records","entity"=>"EXPERIMENT","entityDbId"=>$experimentDbId,"endpointEntity"=>"OCCURRENCE", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'records'=>$experimentModel,
                'processName' => 'generate-plant_inst',
                'experimentDbId' => $experimentDbId."",
                'tokenObject' => [
                    'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                    'refreshToken' => Yii::$app->session->get('user.refresh_token')
                ],
            ];
            $insertedRecord = $this->mainExperiment->create($dataArray, true, $addtlParams);

            //Redirect to main page
            $experimentStatus = isset($experimentModel['data'][0]['experimentStatus']) ? $experimentModel['data'][0]['experimentStatus'] : '';
          
            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }

            $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($insertedRecord['data'][0]['backgroundJobDbId']);
            
            if($bgStatus !== 'done' && !empty($bgStatus)){
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;
            
                $this->mainExperiment->updateOne($experimentDbId, ["experimentStatus"=>"$newStatus"]);
                
                return [
                        'success' => true,
                        'message' => 'redirect'
                ];
            }else{
                return $insertedRecord;
            }
        }else{
            $plotRecords = $this->plotModel->searchAll(["fields"=>"plot.id AS plotDbId|plot.entry_id AS entryDbId|plot.occurrence_id AS occurrenceDbId","occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)]);
           
            // Retrieve all the planting instruction records of the occurrence
            $plantingInsRecord = $this->plantingInstruction->searchAll(["fields"=>"plantingInstruction.id AS plantingInstructionDbId|plot.occurrence_id as occurrenceDbId", "occurrenceDbId"=>"equals ".implode('|equals ', $occurrenceIds)], null, true);
            if(isset($plantingInsRecord['data']) && !empty($plantingInsRecord['data'])){
                $plantingInsIdsArr = ArrayHelper::map($plantingInsRecord['data'],'plantingInstructionDbId','plantingInstructionDbId');
                $plantingInsIdsKey = array_keys($plantingInsIdsArr);
                $plantingDelRecord = $this->plantingInstruction->deleteMany($plantingInsIdsKey);
            }

            $entryRecord = $this->mainEntry->searchAll(['fields' => 'entry.entry_code AS entryCode|entry.entry_number AS entryNumber|entry.entry_type AS entryType
                            |entry.entry_name AS entryName|entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|
                            entry.entry_role AS entryRole|entry.entry_class AS entryClass|entry.id AS entryDbId|experiment.id AS experimentDbId|entry.package_id AS packageDbId',
                            'experimentDbId' => "equals ".$experimentDbId])['data'];
            
            $entryArr = [];

            foreach($entryRecord as $entryRec){
                $entryArr[$entryRec['entryDbId']] = $entryRec; 
            }
            
            //replace with occurrence api call
            foreach($plotRecords['data'] as $index => $plotRecord){
                $plotDbId = $plotRecord['plotDbId'];
                $entryDbId = $plotRecord['entryDbId'];

                $requestData = [
                    'entryDbId' => "$entryDbId", 
                    'plotDbId' => "$plotDbId",
                    'entryCode' => $entryArr[$entryDbId]['entryCode'].'',
                    'entryNumber' => $entryArr[$entryDbId]['entryNumber'].'',  
                    'entryName' => $entryArr[$entryDbId]['entryName'].'', 
                    'entryType' => $entryArr[$entryDbId]['entryType'].'',
                    'entryStatus' => $entryArr[$entryDbId]['entryStatus'].'',
                    'germplasmDbId' => $entryArr[$entryDbId]['germplasmDbId'].''
                ];
                
                if(isset($entryArr[$entryDbId]['seedDbId']) && !empty($entryArr[$entryDbId]['seedDbId'])){
                    $requestData['seedDbId'] = $entryArr[$entryDbId]['seedDbId']."";
                }
                if(isset($entryArr[$entryDbId]['packageDbId']) && !empty($entryArr[$entryDbId]['packageDbId'])){
                    $requestData['packageDbId'] = $entryArr[$entryDbId]['packageDbId']."";
                }
                      
                //entry role
                if(isset($entryArr[$entryDbId]['entryRole']) && !empty($entryArr[$entryDbId]['entryRole'])){
                    $requestData['entryRole'] = $entryArr[$entryDbId]['entryRole'];
                }
                //entry class
                if(isset($entryArr[$entryDbId]['entryClass']) && !empty($entryArr[$entryDbId]['entryClass'])){
                    $requestData['entryClass'] = $entryArr[$entryDbId]['entryClass'];
                }
                $insertList[] = $requestData; 
            }
            $insertedRecord = $this->plantingInstruction->create(['records' => $insertList]);
            //Experiment Status Update
            try{
                foreach($occurrences as $occurrence){
                    $occurrenceDbId = $occurrence['occurrenceDbId'];
                    $plotRecord = $this->plotModel->searchAll(["fields"=>"plot.id AS plotDbId|plot.occurrence_id AS occurrenceDbId","occurrenceDbId"=>"equals ".$occurrenceDbId], '', false)['totalCount'];
                    $plantingInsRecord = $this->plantingInstruction->searchAll(['occurrenceDbId' => "equals ".$occurrenceDbId], '', false)['totalCount'];
    
                    $status = explode(';', $experimentModel['data'][0]['experimentStatus']);
                    $status = array_filter($status);
                    
                    if($plotRecord > 0 && $plantingInsRecord > 0){
                    
                        if(in_array('draft', $status)){
                            $index = array_search('draft', $status);
                            unset($status[$index]);
                        }

                        //Reset the experiment statsus
                        if(!in_array('occurrences created', $status)){
                            $updatedStatus = implode(';', $status);
                            $model['experimentStatus'] = $updatedStatus;
                            $newStatus = !empty($model['experimentStatus']) ? $model['experimentStatus'].';'."occurrences created" : "occurrences created";
                            $model['experimentStatus'] = $newStatus;
                        }
                    }
                    
                    $statusTemp = !empty($model['experimentStatus']) ? $model['experimentStatus'] : 'draft';
                    
                    $this->mainExperiment->updateOne($experimentDbId, ["experimentStatus"=>"$statusTemp"]);

                    //update occurrence info
                    $this->occurrenceModel->populateSingleOccurrenceInfo($occurrenceDbId, $programId);
                }
                
                // Update cross parents for ICN
                if(in_array($experimentModel['data'][0]['dataProcessAbbrev'],['INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS', 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'])){
                    
                    if($experimentModel['data'][0]['dataProcessAbbrev'] == 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'){
                        $params = [
                            'fields' => 'crossParent.id as crossParentDbId|crossParent.experiment_id as experimentDbId',
                            'experimentDbId' => "equals $experimentDbId"
                        ];
              
                        $crossParents = $this->crossParent->searchAll($params, '', true)['data'];

                        //update with occurrence_id
                        $idList = array_column($crossParents, 'crossParentDbId');

                        $this->crossParent->updateMany($idList, ['occurrenceDbId' => $occurrences[0]['occurrenceDbId'].""]);

                    }
                    //check if there are cross records
                    $crosses = $this->cross->searchAll(['fields'=>'germplasmCross.id as crossDbId|experiment.id as experimentDbId','experimentDbId' => 'equals '.$experimentDbId], '', true);

                    //Update cross records with occurrenceDbIds
                    if(isset($crosses['totalCount']) && $crosses['totalCount'] > 0){
                        $crossIds = array_column($crosses['data'], 'crossDbId');
                        $this->cross->updateMany($crossIds, ['occurrenceDbId'=>$occurrences[0]['occurrenceDbId'].""]); //assumming that experiment types with crosses has only one occurrence
                    }
                    
                }
                return $insertedRecord;
            } catch (Exception $error){
                $return = [
                    'success' => false,
                    'message' => 'Problem in updating experiment status.<br> <em>Additional Information: ' .$error.'</em>',
                    'currentProcess' => 'experiment status'
                ];

                return json_encode($return);
            }
            return json_encode(['success' => true, "message" => "Successfully generated plots and/or planting instruction records."]);
        }
    }
    
    // /**
    // * TODO: To be refactored
    // * Get the scale values of the variable
    // */
    // public function getVariableScaleValue($abbrev){

    //     $scale_val_arr = [];
    //     $keyScale = [];

    //     $varEntry = Variable::find()->where(['abbrev'=> strtoupper($abbrev), 'is_void'=> false])->one();
    //     $data_type = $varEntry->data_type;
    //     //Retrieve accepted values if any
    //     $sql = "select value from master.scale_value where scale_id=".$varEntry->scale_id." and is_void=false order by value asc";

    //     $scaleValues = Yii::$app->db->createCommand($sql)->queryAll();

    //     foreach ($scaleValues as $scale_val) {

    //         if($scale_val['value'] != null){

    //             $scale_val_arr[$scale_val['value']] = ucwords($scale_val['value']);

    //         }
    //     }

    //     $keys = array_keys($scale_val_arr);
    //     $ret_key = (sizeof($keys) > 0) ? $keys[0] : '';

    //     $return = ['scale_val_arr'=>$scale_val_arr];
        
    //     return $return;
    // }

    // /**
    // * TODO: To be refactored
    // * Update Entry List Data
    // */
    // public function updateEntryListData($experiment_id, $entry_list_id, $variable_id, $input_val){


    //     $checkSql = "select value from operational.entry_list_data where entry_list_id={$entry_list_id} and variable_id={$variable_id} and is_void=false";

    //     $entryListDataRec = Yii::$app->db->createCommand($checkSql)->queryScalar();

    //     $userId = User::getUserId();

    //             //Check if the record is new or not
    //     if(empty($entryListDataRec)){
    //         //New record
    //         $entryListData = new EntryListData();
    //         $entryListData->entry_list_id = $entry_list_id;
    //         $entryListData->variable_id = $variable_id;
    //         $entryListData->value = strval($input_val);
    //         $entryListData->creator_id = $userId;
    //         $entryListData->creation_timestamp = new Expression('NOW()');
    //         $entryListData->save(false);

    //     }else{
    //         $updateSql = "update operational.entry_list_data set value='".$input_val."' where entry_list_id={$entry_list_id} and variable_id={$variable_id} and is_void=false";

    //         Yii::$app->db->createCommand($updateSql)->execute();
    //     }
    // }

    /**
     * Retrive experiment configuration
     * 
     * @param string $controller controller name
     * @param string $program program name
     * @param integer $id experiment record identifier
     * @param integer $processId process path id
     * @param string $configType configuration type ex. 'child' 
     * @param array $configTypeOptions configuration type options ex. activity abbrev
     */
    public function retrieveConfiguration ($controller, string $program=null, $id, $processId, string $configType = null, array $configTypeOptions = null)
    {
        
        $activityAbbrev = $varConfigValues ='';

        if($configType === null){
            $activities = $this->formModel->prepare($controller, $program, $id, $processId);
            if(!empty($activities)){
                $index = array_search($activities['active'], array_column($activities['children'], 'url'));
                $activityAbbrev = $activities['children'][$index]['abbrev'].'_VAL';
            }

        }else{ //this is for children of the config activity (tabs inside a tab)

            if($configTypeOptions !== null){
                extract($configTypeOptions);
            }
        }
        
        $requiredConfig = $this->configModel->getConfigByAbbrev($activityAbbrev);
        return isset($requiredConfig['Values']) ? $requiredConfig['Values'] : [];


    }

    /***
     * Format string index to camelCase
     * 
     * @param $index string attribute index
     */
    public function setIndex($index){
        return preg_replace_callback('/_([a-z]?)/', function($match) {return strtoupper($match[1]);}, strtolower($index));
    }

    
    /**
     * Build column attributes based on the configuration
     * 
     * @param $experimentId integer experiment record identifier
     * @param $configFields array list of fields in the configuration
     */
    public function buildColumns($experimentId, $configFields, $columns, $targetStep = 'entry'){
        
        /**
         * TODO: make this dynamic and more generic next sprint
         */
        
        $entryColumns = ['ENTRY_ROLE', 'ENTRY_CLASS', 'DESCRIPTION', 'ENTRY_TYPE', 'PACKAGE_QUANTITY', 'PACKAGE_UNIT'];

        foreach($configFields as $configField){
            if(!(isset($configField['is_hidden']) && $configField['is_hidden'])){    
                $column = strtoupper($configField['variable_abbrev']);
                
                //set attribute index
                if(in_array($column, $entryColumns)){
                    $index = ExperimentModel::setIndex($column);
                    $targetTable = 'entry';
                }else{
                    $index = strtolower($column);
                    $targetTable = 'entry_data';
                }
                
                if(isset($configField['is_shown']) && !$configField['is_shown']){
                    $visible = false;
                }else{
                    $visible = true;
                }
            
                //set other information
                $defaultValue = (isset($configField['default']) && !empty($configField['default'])) ? $configField['default'] : '';
                $variable = $this->variable->getVariableByAbbrev($column);
                $tooltip = $variable['description'] ?? '';
                $header = isset($configField['display_name']) && !empty($configField['display_name']) ? $configField['display_name'] : ($variable['label'] ?? '');
                $dataType = $variable['dataType'] ?? '';

                //Check if the field is required
                $isRequiredLabel = $isRequiredClass = '';
                $isRequired = false;
                $allowClear = true;
                if(isset($configField['required']) && $configField['required'] == 'required'){
                    $isRequiredLabel = '<span class="required">*</span>';
                    $isRequiredClass = ' required-field';
                    $isRequired = $configField['required'];
                    $allowClear = false;
                }

                //Check is the field is disabled
                $isDisabled = isset($configField['disabled']) ? $configField['disabled'] : false;

                if($targetStep == 'entry'){
                    /**
                     * TODO: Use generic form in building entry list grid form
                     */
                    $allowedValues = false;
                    $additionalFilter = '';
                    if(isset($configField['allowed_values']) && count($configField['allowed_values']) > 0){
                        $allowedValues = true;

                        foreach($configField['allowed_values'] as $allowedVal){
                            if(empty($additionalFilter)){
                                $additionalFilter = "equals ".$allowedVal;
                            }else{
                                $additionalFilter = $additionalFilter . "|". "equals ".$allowedVal;
                            }
                        }
                    }

                    if(isset($additionalFilter) && !empty($additionalFilter)){
                        $scales = ['abbrev' => $additionalFilter];
                        $scaleValues = ScaleValue::getVariableScaleValues($variable['variableDbId'], $scales);
                    }
                    else{
                        //Get scale values
                        $scales = $this->variable->getVariableScales($variable['variableDbId']);
                        $scaleValues = isset($scales['scaleValues']) ? $scales['scaleValues'] : [];
                    }

                    $fieldValue = function($data) use ($dataType, $targetTable, $scales, $scaleValues, $header, $isDisabled, $isRequiredClass, $index, $defaultValue, $additionalFilter, $allowClear){
                        $entryId = $data['entryDbId'];
                        $value = !empty($data[$index]) ? $data[$index] : $defaultValue;
    
                        if(!empty($scaleValues)){//display dropdown list
                            $variableTags = ArrayHelper::map($scaleValues, 'value', 'value');
                            
                            $field = Select2::widget([
                                'name' => $index.'_value',
                                'data' => $variableTags,
                                'value' => $value,
                                'options' => [
                                    'placeholder' => "Select $header",
                                    'id' => "$index-$targetTable-$entryId",
                                    'class' => "editable-field $index".$isRequiredClass,
                                    'data-name' => $index,
                                    'data-label' => $index,
                                    'data-subMem' => $index.'-'.$entryId,
                                    'multiple' => false,
                                    'disabled' => $isDisabled
                                ],
                                'pluginOptions' => [
                                    'allowClear' => $allowClear,
                                ]
                            ]);
    
                            return '<div class="'.$index.'">'.$field.'</div>';
                        }
                        else if($dataType === 'integer' || $dataType === 'float' || $dataType === 'double'){ // display numeric field
                
                            $min = isset($scaleValues['minValue']) ? $scaleValues['minValue'] : 0;
                            $max = isset($scaleValues['maxValue']) ? $scaleValues['maxValue'] : '';
                            $step = ($dataType === 'integer') ? 1 : 0.1;
    
                            return Html::input('number', $index.'_value', $value,
                                [
                                    'disabled' => $isDisabled,
                                    'id' => "$index-$targetTable-$entryId",
                                    'data-label' => $index,
                                    'class'=> "editable-field $index".$isRequiredClass,
                                    'min' => $min,
                                    'max' => $max,
                                    'step' => $step,
                                ]);
    
                        }
                        else {// display text field
                            return Html::input('text', $index.'_value', $value,
                                [
                                    'disabled' => $isDisabled,
                                    'id' => "$index-$targetTable-$entryId",
                                    'data-label' => $index,
                                    'class'=> "editable-field $index".$isRequiredClass,
                                ]);
                        }
                    };
                    // $order = !empty($isRequiredLabel) ? DynaGrid::ORDER_FIX_RIGHT : DynaGrid::ORDER_MIDDLE; //Temporarily commented
                    $order = DynaGrid::ORDER_FIX_RIGHT;
                }
                else{
                    $index = isset($configField['target_column']) && empty($configField['secondary_target_column'] && $configField['variable_type'] !== 'metadata') ? $configField['target_column'] : ExperimentModel::setIndex($column);

                    $apiResourceMethod = isset($configField['api_resource_method']) ? $configField['api_resource_method'] : '';
                    $apiResourceEndpoint = isset($configField['api_resource_endpoint']) ? $configField['api_resource_endpoint'] : '';
                    $apiResourceFilter = isset($configField['api_resource_filter']) ? $configField['api_resource_filter'] : '';
                    $apiResourceSort = isset($configField['api_resource_sort']) ? $configField['api_resource_sort'] : '';
                    $dataResults = null;

                    //check if data will be retrieved from CRM
                    $apiDomainEndpt = isset($configField['api_domain_endpoint']) ? $configField['api_domain_endpoint'] : null;

                    // This is for the variables with select2 options
                    if($apiResourceMethod == 'GET'){
                        $path = $apiResourceEndpoint.'?'.$apiResourceSort;
                    }else{
                        $path = $apiResourceEndpoint;
                    }
                    if(in_array($index,['site', 'field', 'contactPerson'])){
                        if(in_array($index,['site', 'field'])){
                            $filterValue = $apiResourceFilter[array_keys($apiResourceFilter)[0]];
                            $apiResourceFilter[array_keys($apiResourceFilter)[0]] = 'equals '.$filterValue;
                        }
                      
                        if($index != 'contactPerson' && $apiDomainEndpt != 'cs'){
                            $result = Yii::$app->api->getResponse($apiResourceMethod, $path, json_encode($apiResourceFilter), '', true);

                            if (isset($result['status']) && $result['status'] == 200 && isset($result['body']['result']['data'])){
                                $dataResults = $result['body']['result']['data'];
                            }
                        } else {
                            $dataResults = $this->person->getPersonContacts();
                        }
                    }

                    $fieldValue = function($data) use ($targetStep, $defaultValue, $configField, $index, $dataResults){
                        if($index == 'field'){
                            if(isset($data['siteDbId'])){
                                $configField['api_resource_filter'] = [
                                    'geospatialObjectType' => 'equals field',
                                    'parentGeospatialObjectDbId' => 'equals '.$data['siteDbId'],
                                ];
                                $dataResults = [];
                            }
                        }

                        $configField['default'] = isset($data[$index]) && !empty($data[$index]) ? $data[$index] : $defaultValue;
                     
                        $rowId = $data[$targetStep.'DbId'];

                        $genericForm = $this->genericFormWidget;
                        $optionsArray = [
                            'config' => [$configField],
                            'targetStep' => $targetStep,
                            'rowId' => $rowId,
                            'rowClass' => 'editable-field'
                        ];
                        if(in_array($index,['site', 'field', 'contactPerson'])){
                            $optionsArray['presetData'] = [
                                $configField['variable_abbrev'] => $dataResults
                            ];
                        }
                        $genericForm->options = $optionsArray;
                        $placeHolder = $genericForm->generateFields();

                        return $placeHolder['requiredFields'][0]['rawValue'];
                    };
                    

                    $order = !empty($isRequiredLabel) ? DynaGrid::ORDER_FIX_LEFT : DynaGrid::ORDER_MIDDLE;
                }

                $formattedField =
                    [
                        'attribute'=> $index,
                        'label' => '<span title="'.$tooltip.'">'.Yii::t('app', $header).' '.$isRequiredLabel,
                        'format' => 'raw',
                        'encodeLabel'=> false,
                        'vAlign' => 'middle',
                        'visible' => true,
                        'value' => $fieldValue,
                        'order'=> $order
                    ];

                    if($index == 'site'){
                        $formattedField['filterType'] = GridView::FILTER_SELECT2;
                        $formattedField['headerOptions']=['style'=>'min-width:150px !important'];
                        $formattedField['filterWidgetOptions'] = [
                            'pluginOptions'=>[
                                'allowClear'=>true,
                                'ajax' => [
                                    'url' => '/index.php/experimentCreation/create/get-filter-occurrences?id='.$experimentId,
                                    'dataType' => 'json',
                                    'delay' => 150,
                                    'type'=>'POST',
                                    'data' => new JsExpression(
                                        'function (params) {
                                            var value = (params.term === "")? undefined: params.term

                                            return {
                                                "filter": {
                                                    "'.$index.'": value
                                                },
                                                "column": "'.$index.'"
                                            }
                                        }'
                                    ),
                                    'processResults' => new JSExpression(
                                        'function (data, params) {
                                            params.page = params.page || 1;

                                            return {
                                                results: data.items
                                            };
                                        }'
                                    ),
                                    'cache' => true
                                ]
                            ],
                            'options'=>[
                                'id'=>$index.'FilterId'
                            ]
                        ];
                        $formattedField['filterInputOptions'] = [
                            'placeholder'=>''
                        ];
                    }
                    $columns[] = $formattedField;
                   
            }
        }

        return $columns;
        
    }

    // /**
    // * TODO: To be refactored
    // * View of dynamic columns of the configuration (Entry)
    // * @param integer experimentId Id of the experiment to view
    // * @param array $configVariables Array of the dynamic variables
    // */
    // public function viewEntryDynamicColsById($experimentId, $configVariables=null){

    //     $requiredColumns = [];

    //     if($configVariables != null){
    //         foreach ($configVariables as $value) {
    //             $variableModel = Variable::find()->where(['abbrev'=>$value['variable_abbrev'],'is_void'=>false])->one();
    //             $label = !empty($variableModel->name) ? $variableModel->name : $value['field_label'];
                
    //             $requiredColumns[] = [
    //                 'attribute' => strtolower($value['variable_abbrev']),
    //                 'label' => Yii::t('app',$label),
    //                 'format' => 'raw',
    //                 'value' => function($data) use ($value){
    //                     return $data[strtolower($value['variable_abbrev'])];
    //                 },
    //                 'enableSorting' => false,
    //             ];
    //         }
    //     }
    //     return $requiredColumns;
    // }

    /**
     * Returns all valid entry ids
     * @param $id integer experiment identifier
     * @param $entIdList string of entry ids separated by ','
     * @param $entIdType string either male or female
     * @param $parentCount integer total number of parent combinations
     */
    public function getEntIdList($experimentDbId, $entryIds, $crossName, $parentRole){

        $parentRole = $parentRole.'EntryDbId'; 
        $entIdList = '';
        $entryIds = explode(',', $entryIds);
        foreach($entryIds as $entryId){

            $params = [
                $parentRole => "equals $entryId",
                'crossName' => $crossName,
                'experimentDbId' => "equals $experimentDbId"
            ];

            $crossParentData = Cross::searchAll($params);

            //if entryDbId is not yet used in cross parents
            if($crossParentData['totalCount'] == 0 && $crossParentData['status'] == 200){
                $entIdList = $entIdList !== '' ? "$entryId,$entIdList" : "$entryId";
            }
        }
        
        return $entIdList;
    }


    /**
     * Retrive duplicate germplasm with multiple entry list numbers
     * 
     * @param Array $duplicateList  of crosses with duplicate germplasm
     * 
     * @return Array $dataProvider list of duplicate data
     */
    public function getDuplicateListData($duplicateList){
        return new ArrayDataProvider([
            'allModels' => $duplicateList,
            'pagination' => false
        ]);


    }

    /**
     * Retrieve male/female parents
     * 
     * @param Array $params list of search parameters
     * @param String $filters set of search filters (e.g. limit,sort,page, etc.)
     * @param String $gridName name of the data browser
     * @param String $currentPage current page parameter
     */
    public function getParentList($params, $filters, $gridName, $currentPage=null){
        
        if($filters == NULL){
            $filters = '?sort=entryNumber:asc';
        }else if(strpos($filters, 'sort') !== false){
            $sortStr = explode('sort', $filters);
            if(strpos($sortStr[1], 'entryNumber') === false){
                $filters .= '|entryNumber:asc';
            }
        } else {
            $filters .= '&sort=entryNumber:asc';
        }
        
        $entries = $this->mainEntry->searchAll($params, $filters, false);

        if(isset($currentPage) && $entries['totalCount'] == 0){
            $filters = str_replace($currentPage,'', $filters);
            $entries = $this->mainEntry->searchAll($params, $filters, false);
            $_GET["$gridName-grid-page"] = 1;
        }

        return new ArrayDataProvider([
            'key'=>'entryDbId',
            'allModels' => $entries['data'],
            'sort' => [
                'defaultOrder' => ['entryNumber' => SORT_ASC],
                'attributes' => ['entryDbId', 'entryNumber', 'entryRole', 'designation', 'parentage', 'germplasmDbId','parentage', 'femaleCount', 'maleCount', 'entryCode']   
            ],
            'restified' => true,
            'id' => "$gridName-grid",
            'totalCount' => $entries['totalCount']
        ]);


    }

    /**
    * Get method tags
    * 
    * @param $mode string update mode on whether by bulk or not
    */
    public function getTags($mode=null){

        $variable = $this->variable->getVariableByAbbrev('CROSS_METHOD');
        $scaleValues = $this->scaleValue->getVariableScaleValues($variable['variableDbId'], null);
 
        $tags = ArrayHelper::map($scaleValues, 'value', 'value');

        if(isset($tags['selfing'])){
            unset($tags['selfing']);
        }
    
        return $tags;
    }

    /**
     * Save crosses in the database
     *
     * @param integer $id experiment identifier
     * @param string $column variable to update
     * @param string|integer $value variable value
     * @param string $command update/delete action
     * @param string $mode single/bulk update/delete
     * @param string $selected selected value/s
     */
    public function saveBulkCrosses ($id, $column, $value, string $command = null, $mode, $selected=[])
    {

        if($command == 'update'){


            if(strpos($column,"_")!==false){
                $column = ucwords(strtolower($column),'_');
                $column = str_replace('_','',$column);
                $column = lcfirst($column); 
            } else {
                $column = strtolower($column);
            }

            $requestBody = ["$column"=>"$value"];
            if(!empty($selected)){
                $filters['crossDbId'] = 'equals '.implode('|equals ', $selected);
            }
            $filters['experimentDbId'] = "equals $id";
            $filters['fields'] = 'germplasmCross.id AS crossDbId|experiment.id AS experimentDbId|germplasmCross.is_method_autofilled AS isMethodAutofilled';
            $getAllCrossRecord = $this->cross->getCrosses(json_encode($filters));
            $crossIds = [];
            foreach($getAllCrossRecord as $crossRec){
                if($column == 'crossMethod' && $crossRec['isMethodAutofilled']){
                    continue;
                }else{
                    $crossIds[] = $crossRec['crossDbId'];
                }
            }

            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateCrossList')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateCrossList') : 1000;

            if(count($crossIds) < $bgThreshold) {
                $this->cross->updateMany($crossIds,$requestBody);
            } else { // bg worker
                $result = $this->worker->invoke(
                    'ExperimentCreationProcessor',
                    'Updating cross list records',
                    'EXPERIMENT',
                    $id,
                    'CROSS',
                    'EXPERIMENT_CREATION',
                    'PUT',
                    [
                        'experimentDbId' => $id,
                        'requestBody' => [
                            "httpMethod" => "PUT",
                            "endpoint" => 'v3/crosses',
                            "dbIds" => implode("|", $crossIds),
                            "requestData" => $requestBody
                        ],
                    ],
                    null,
                    ['processName' => 'update-cross-list']
                );

                $experiment = $this->mainExperiment->getExperiment($id);

                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The cross list records for <strong>'.$experiment['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$id]);
            }

        } elseif($command == 'delete'){
            if($mode == 'all'){
                //retrieve crosses count
                $crosses = $this->cross->searchAll([
                    'fields'=> 'germplasmCross.id AS crossDbId|experiment.id AS experimentDbId',
                    'experimentDbId'=>"equals $id",
                ], 'limit=1&sort=crossDbId:asc', false);
                $selectedCount = $crosses['totalCount'];
            } else {
                $selectedCount = count($selected);
            }
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteCrossList')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'deleteCrossList') : 500;

            // If cross pattern is used, get total batch count of selected ids
            $crossesBatchCount = $this->crossPatternModel->getCrossesBatchCount($id,$selected);
            if($crossesBatchCount > 0) $selectedCount = $crossesBatchCount;

            if($selectedCount < $bgThreshold){

                if($mode == 'selected'){ 
                    
                    //remove selected cross nos
                    $this->cross->deleteSelectedCrosses($id,$selected); 
                    //delete experiment block if cross pattern is used
                    $this->crossPatternModel->deleteArrangement($id, $selected);
                    
                }elseif($mode == 'all'){
                    $this->cross->deleteCrosses($id);
                    $this->crossPatternModel->deleteCrossPatternInfo($id);
                }
            } else {
                $idList = ($mode == 'selected') ? implode("|", $selected) : [];
                //invoke worker
                $result = $this->worker->invoke(
                    'EcCrossRecordsProcessor',
                    'Deleting cross list records',
                    'EXPERIMENT',
                    $id,
                    'CROSS',
                    'EXPERIMENT_CREATION',
                    'DELETE',
                    [
                        "experimentDbId" => $id,
                        "idList" => $idList,
                        "processName" => "delete-cross-records",
                        "selection" => $mode
                        
                    ]
                );
                
                $experiment = $this->mainExperiment->getExperiment($id);
                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The cross list records for <strong>'.$experiment['experimentName'].'</strong> are being deleted in the background. You may proceed with other tasks. You will be notified in this page once done.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$id]);
            }
        }
    }

    // /**
    //  * TODO: To be refactored
    //  * Check if there's an existing cross
    //  * @param $id integer experiment identifier
    //  * @return $result boolean true if there are crosses
    //  */
    // public function checkCrosses($id){

    //     $sql = "select exists (select 1 from operational.crossing_list where crossing_method_id is not null and is_void = false and experiment_id ={$id})";
    //     $result = Yii::$app->db->createCommand($sql)->queryOne();

    //     return $result['exists'];
    // }

    
    /**
    * Checks if the required attributes/columns are filled
    * @param $id integer experiment identifier
    * @param $reqVarConfig object contains the configuration variables
    * @param $apiParams array API resources
    */
    public function genericCheckRequiredData($id, $reqVarConfig, $apiParams){
        $reqVarsArr = [];
        $reqVarsLabel = null;
        $entryIdCond = '';

        extract($apiParams); 
        
        if(!empty($reqVarConfig)){

            foreach($reqVarConfig as $key => $value){
                if(isset($value['required']) && !empty($value['required'])){
                    $variable_abbrev = strtoupper($value['variable_abbrev']);
                    $varParam['fields'] = 'variable.id as variableDbId|variable.abbrev as abbrev|variable.label as label';
                    $varRecord = $this->variable->getVariableByAbbrev(strtoupper($variable_abbrev));
                    $targetColumn = isset($variable['target_column']) ? $variable['target_column'] : ''; 
                 
                    $abbrev = strtolower($varRecord['abbrev']);
                    $label = $varRecord['label'];

                    $requiredColNullCounter = 0;
    
                    if(empty($targetColumn)){ 
                        $column = ucwords(strtolower($abbrev),'_');
                        $column = str_replace('_','',$column);
                        $column = lcfirst($column); 
                    }else{
                        $column = $targetColumn;
                    }
                    
                    if($column == 'crossMethod'){
                        $additionalFilter = ["$column" => "equals "];
                    }else{
                        $additionalFilter = ["$column" => "is null"];
                    }
                    
                    $apiResourceFilter = ArrayHelper::merge($apiResourceFilter, $additionalFilter);
    
                    if($apiResourceMethod == 'GET'){
                        if(!empty($apiResourceSort)){
                            $path = $apiResourceEndpoint.'?'.$apiResourceSort;
                        }else{
                            $path = $apiResourceEndpoint;
                        }
                    }else{
                        $path = $apiResourceEndpoint;
                    }
                    
                    if($varRecord['abbrev'] == 'PARENT_TYPE'){
                        $varRecId = $varRecord['variableDbId'];
                        $apiResourceFilter = ["fields"=>"entry_data.data_value AS dataValue","variableDbId"=>"equals ".$varRecId];
                            
                        foreach($entryIdList as $entryId){
                            $result = Yii::$app->api->getResponse($apiResourceMethod,'entries/'.$entryId.'/data-search', json_encode($apiResourceFilter), true);
                            
                            $requiredColNullCounter = isset($result['body']['result']['data']['0']['data']) ? count($result['body']['result']['data']['0']['data']) : 1;
                            
                            if(empty($requiredColNullCounter) || $requiredColNullCounter == 0){
                                if(!in_array($label, $reqVarsArr)){
                                    $reqVarsArr[] = $label;
                                    $reqVarsLabel[] = sizeof($reqVarsArr).'.'.$label;
                                }
                            }
                        }
                    }else{
                        if(!empty($apiResourceMethod) && !empty($apiResourceEndpoint)){
                            if(!empty($apiResourceFilter)){ 
                                $result = Yii::$app->api->getResponse($apiResourceMethod, $path, json_encode($apiResourceFilter), true);
                                
                            }else{
                                $result = Yii::$app->api->getResponse($apiResourceMethod, $path);
                            }
                        }

                        if (isset($result['status']) && $result['status'] == 200 && isset($result['body']['metadata']['pagination']['totalCount'])){
                            $requiredColNullCounter = $result['body']['metadata']['pagination']['totalCount'];
                            if($requiredColNullCounter > 0){
                                if(!in_array($label, $reqVarsArr)){
                                    $reqVarsArr[] = $label;
                                    $reqVarsLabel[] = sizeof($reqVarsArr).'.'.$label;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $reqVarsLabel;
    }

     /**
     * Delete the occurrences, occurrence layout, and plot records of an experiment
     * @param $experiment_id integer Database ID of the record
     */
    public function deleteExistingOccurrences($id){
        //delete occurrences
        $occurrencesArray = Occurrence::searchAll(['experimentDbId'=>$id]);
        
        if($occurrencesArray['totalCount'] > 0){
            $occurrences = $occurrencesArray['data'];
            $occurrenceIds = array_column($occurrences, 'occurrenceDbId');

            $plotsArray = Plot::searchAll(['occurrenceDbId'=>'equals '.implode('|equals ', $occurrenceIds)]);
            if($plotsArray['totalCount'] > 0){
                $plots = $plotsArray['data'];
                $plotIds = array_column($plots, 'plotDbId');
                Plot::deleteMany($plotIds);
            }

            $occurrenceLayoutArray = OccurrenceLayout::searchAll(['occurrenceDbId'=>'equals '.implode('|equals ', $occurrenceIds)]);
            if($occurrenceLayoutArray['totalCount'] > 0){
                $occurrenceLayout = $occurrenceLayoutArray['data'];
                $occurrenceLayoutIds = array_column($occurrenceLayout, 'occurrenceLayoutDbId');
                OccurrenceLayout::deleteMany($occurrenceLayoutIds);
            }

            Occurrence::deleteMany($occurrenceIds);
        }

    }

    /**
    * Generate colors for layout
    *
    * @param $hex integer original color
    * @param $percent integer darkens color
    * @param $darken boolean flag if color will be darken
    */
    public function adjustBrightness($hex, $percent, $darken = true) {
        $brightness = $darken ? -255 : 255;
        $steps = round($percent*$brightness/100);
        
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

    /**
     * Form layout of the occurrence
     * 
     * @param Integer $occurrenceDbId record id of the occurrence
     * @param Integer $experimentDbId record id of the experiment
     * @param Boolean $fullScreen flag to show full layout
     * @param Text $type Type of Layout (design, field, etc.)
     *
     * @return mixed contains layout information
     * 
     */
    public function generateLayout($occurrenceDbId, $experimentDbId, $fullScreen=false, $type='design'){
        // get color for each rep
        $repColors = array();
        $plotLayout = array();
        $entryLayout = array();
        $codeLayout = array();
        $blkLayout = array();
        $xAxis = array();
        $yAxis = array();
        $lowestFieldXDisplay = $lowestFieldYDisplay= 10;
        $lowestFieldX = $lowestFieldY = 1;
        $xAttribute = 'designX';
        $yAtrribute = 'designY';

        // if field layout, retrieve field x and y values
        if($type == 'field'){
            $xAttribute = 'fieldX';
            $yAtrribute = 'fieldY';

            // get lowest field X and Y values
            $lowestFieldXRes = $this->plotModel->searchAll([
                'occurrenceDbId'=>"equals ".$occurrenceDbId,
                'fields'=>'plot.field_x AS fieldX|plot.occurrence_id AS occurrenceDbId'
            ], 'sort=fieldX&limit=1', false);

            $lowestFieldXDisplay = isset($lowestFieldXRes['data'][0]['fieldX']) ? $lowestFieldXRes['data'][0]['fieldX'] + ($lowestFieldXDisplay - 1) : $lowestFieldXDisplay;
            $lowestFieldX = isset($lowestFieldXRes['data'][0]['fieldX']) ? $lowestFieldXRes['data'][0]['fieldX'] : $lowestFieldX;
            
            $lowestFieldYRes = $this->plotModel->searchAll([
                'occurrenceDbId'=>"equals ".$occurrenceDbId,
                'fields'=>'plot.field_y AS fieldY|plot.occurrence_id AS occurrenceDbId'
            ], 'sort=fieldY&limit=1', false);

            $lowestFieldYDisplay = isset($lowestFieldYRes['data'][0]['fieldY']) ? $lowestFieldYRes['data'][0]['fieldY'] + ($lowestFieldYDisplay - 1) : $lowestFieldYDisplay;
            $lowestFieldY = isset($lowestFieldYRes['data'][0]['fieldY']) ? $lowestFieldYRes['data'][0]['fieldY'] : $lowestFieldY;
        }

        // get records within 1-10 of x and y attributes
        $plotDesignY = $this->plotModel->searchAll([$yAtrribute=>"less than or equal to $lowestFieldYDisplay", $xAttribute=>"less than or equal to $lowestFieldXDisplay", 'occurrenceDbId'=>"equals ".$occurrenceDbId]);
        $plotDesignXMax = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,"sort=$xAttribute:DESC&limit=1", false);
        $plotDesignYMax = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,"sort=$yAtrribute:DESC&limit=1", false);
        $plotRepMax = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,'sort=rep:DESC&limit=1', false);
        $plotBlkMax = $this->occurrenceModel->searchAllPlots($occurrenceDbId, null,'sort=blockNumber:DESC&limit=1', false);
        
        // get max row and col of layout
        $maxRows = $plotDesignYMax['data'][0]['plots'][0][$yAtrribute] ?? 0;
        $maxCols = $plotDesignXMax['data'][0]['plots'][0][$xAttribute] ?? 0;

        $maxRep = $plotRepMax['data'][0]['plots'][0]['rep'] ?? 0;
        $maxBlk = $plotBlkMax['data'][0]['plots'][0]['blockNumber'] ?? 0;

        if($maxRows > 1){
            $plotsCombined = $plotDesignY['data'];

        } else {
            $plotRecords = $this->plotModel->searchAll(['occurrenceDbId'=>"equals ".$occurrenceDbId],'sort=plotNumber:ASC', true);

            $plotsCombined = $plotRecords['data'];
        }
        
        if($maxRows != NULL || $maxRows > 0){
            $repColors[1]='#c5e1a5';
            for($i=2; $i<=$maxRep; $i++){

                
                $repColors[$i] = ExperimentModel::adjustBrightness($repColors[1], ($i/(int) $maxRep)*65, true);
            }
            
            if($fullScreen){
                $maxRowLimit = $maxRows;
                $maxColLimit = $maxCols;
                
                $plotRecords = $this->plotModel->searchAll(['occurrenceDbId'=>"equals ".$occurrenceDbId],'sort=plotNumber:ASC', true);

                foreach($plotRecords['data'] as $plots){
                    
                    $repValue = $plots['rep'];
                    $plotValue = $plots['plotNumber'];
                    $codeValue = $plots['plotCode'];
                    $entryValue = $plots['entryNumber'];

                    if($plots['entryType'] == 'check'){
                        $color = '#e67e00';
                    } else {
                        $color = $repColors[$repValue];
                    }

                    $plotLayout[] = [
                        'x' => $plots[$xAttribute],
                        'y' => $plots[$yAtrribute],
                        'value' => $plotValue,
                        'color'=> $color
                    ];

                    $entryLayout[] = [
                        'x' => $plots[$xAttribute],
                        'y' => $plots[$yAtrribute],
                        'value' => $entryValue,
                        'color'=> $color
                    ];

                    $codeLayout[] = [
                        'x' => $plots[$xAttribute],
                        'y' => $plots[$yAtrribute],
                        'value' => $codeValue != '...' ? (int) $codeValue : $codeValue,
                        'color'=> $color
                    ];

                    if(!empty($maxBlk) || $maxBlk != NULL){
                        $blkValue = $plots['blockNumber'];
                        $blkLayout[] = [
                            'x' => $plots[$xAttribute],
                            'y' => $plots[$yAtrribute],
                            'value' => $blkValue != '...' ? (int) $blkValue : $blkValue,
                            'color'=> $color
                        ];
                    }
                }
                $xAxis = range(0,$maxCols);
                $yAxis = range(0,$maxRows);

            }else{
                $maxRowLimit = $lowestFieldY + 9;
                $maxColLimit = $lowestFieldX + 9;

                foreach($plotsCombined as $plots){                    
                    if (($plots[$xAttribute] <= $maxColLimit && $plots[$yAtrribute] <= $maxRowLimit)) {

                        // Display '...' on Layout UI if over max limit
                        $repValue = (($plots[$xAttribute] == $maxColLimit && $plots[$xAttribute] != $maxCols) || ($plots[$yAtrribute] == $maxRowLimit && $plots[$yAtrribute] != $maxRows)) ? '...' : $plots['rep'];
                        $plotValue = (($plots[$xAttribute] == $maxColLimit && $plots[$xAttribute] != $maxCols) || ($plots[$yAtrribute] == $maxRowLimit && $plots[$yAtrribute] != $maxRows)) ? '...' : $plots['plotNumber'];
                        $codeValue = (($plots[$xAttribute] == $maxColLimit && $plots[$xAttribute] != $maxCols) || ($plots[$yAtrribute] == $maxRowLimit && $plots[$yAtrribute] != $maxRows)) ? '...' : $plots['plotCode'];
                        $entryValue = (($plots[$xAttribute] == $maxColLimit && $plots[$xAttribute] != $maxCols) || ($plots[$yAtrribute] == $maxRowLimit && $plots[$yAtrribute] != $maxRows)) ? '...' : $plots['entryNumber'];
                       
                        if($plots['entryType'] == 'check'){
                            $color = '#e67e00';
                        } else {
                            $color = $repColors[$repValue] ?? null;
                        }
                        
                        $plotLayout[] = [
                            'x' => $plots[$xAttribute],
                            'y' => $plots[$yAtrribute],
                            'value' => $plotValue,
                            'color'=> $color
                        ];
    
                        $entryLayout[] = [
                            'x' => $plots[$xAttribute],
                            'y' => $plots[$yAtrribute],
                            'value' => $entryValue,
                            'color'=> $color
                        ];
    
                        $codeLayout[] = [
                            'x' => $plots[$xAttribute],
                            'y' => $plots[$yAtrribute],
                            'value' => $codeValue != '...' ? (int) $codeValue : $codeValue,
                            'color'=> $color
                        ];

                        if(!empty($maxBlk) || $maxBlk != NULL){
                            $blkValue = (($plots[$xAttribute] == $maxColLimit && $plots[$xAttribute] != $maxCols) || ($plots[$yAtrribute] == $maxRowLimit && $plots[$yAtrribute] != $maxRows)) ? '...' : $plots['blockNumber'];
                            $blkLayout[] = [
                                'x' => $plots[$xAttribute],
                                'y' => $plots[$yAtrribute],
                                'value' => $blkValue != '...' ? (int) $blkValue : $blkValue,
                                'color'=> $color
                            ];
                        }
                    }
                }

                // set dynamic x and y axes
                $colsArr = range(0, $maxColLimit - 1);
                $maxColsArr = [$maxCols];
                $xAxis = array_merge($colsArr, $maxColsArr);

                $rowsArr = range(0, $maxRowLimit - 1);
                $maxRowsArr = [$maxRows];
                $yAxis = array_merge($rowsArr, $maxRowsArr);
                
            }
            
        }

        return [
          'dataArrays' =>[
              'entno' => $entryLayout,
              'plotcode' => $codeLayout,
              'plotno' => $plotLayout,
              'block' => $blkLayout
          ],
          'maxRows' => $maxRows,
          'maxCols' => $maxCols,
          'maxBlk' => $maxBlk,
          'xAxis' => $xAxis,
          'yAxis' => $yAxis
        ];
    }

    /**
     * Reorder all entries in background process
     * @param Integer $experimentId experiment id
     * @param String $paramSort current sorting
     */
    public function reorderAllEntriesBgProcess($experimentId,$paramSort, $entryInfo = null, $entryCount = null){

        $token = Yii::$app->session->get('user.b4r_api_v3_token');
        $refreshToken = Yii::$app->session->get('user.refresh_token');

        if($entryInfo != null){
            $requestDataParams = [
                'processName' => "reorder-all-entries",
                'experimentDbId' => "".$experimentId,
                'insertSortArray' => $entryInfo
                
            ];
        } else {
            $requestDataParams = [
                'processName' => "reorder-all-entries",
                'experimentDbId' => "".$experimentId,
                'sort' => "".$paramSort
                
            ];
        }

        $params = [
            'httpMethod' => 'PUT',
            'endpoint' => 'v3/entries',
            'entity' => 'ENTRY_LIST',
            'entityDbId' => ''.$experimentId,
            'endpointEntity' => 'ENTRY_LIST',
            'application' => 'EXPERIMENT_CREATION',
            'dbIds' => ''.$experimentId,
            'descpription' => 'Update entry list',
            'requestData' => $requestDataParams,
            'tokenObject' => [
                "token" => $token,
                "refreshToken" => $refreshToken
            ],
        ];

        $updateBgProcess = Yii::$app->api->getParsedResponse('POST', 'processor', json_encode($params));

        // update experiment status
        $experiment = $this->mainExperiment->getExperiment($experimentId);
        $experimentStatus = $experiment['experimentStatus'];

        if(!empty($experimentStatus)){
            $status = explode(';',$experimentStatus);
            $status = array_filter($status);
        }else{
            $status = [];
        }
       
        $bgStatus = $this->mainExperiment->formatBackgroundWorkerStatus($updateBgProcess['data'][0]['backgroundJobDbId']);

        if($bgStatus !== 'done' && !empty($bgStatus)){          
            $updatedStatus = implode(';', $status);
            $newStatus = !empty($updatedStatus) ? $updatedStatus.';' . 'updating of entries in progress' : 'updating of entries in progress';
        
            $this->mainExperiment->updateOne($experimentId, ["experimentStatus"=>"$newStatus"]);

            Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.');
        }
        
        return $updateBgProcess['data'][0]['backgroundJobDbId'];
    }

    /**
     * Update seedDbId values of cross parent records
     * @param Integer $experimentId experiment id
     * 
     * @return Integer number of records updated
     */
    public function updateCrossParents($experimentId){
        $crossParents = $this->crossParent->searchAll([
            'fields'=> 'crossParent.id as crossParentDbId|crossParent.seed_id as seedDbId|crossParent.experiment_id as experimentDbId|crossParent.entry_id as entryDbId',
            'experimentDbId'=> "equals $experimentId",
            'seedDbId' => 'is null'
        ]);
        if($crossParents['totalCount'] > 0){

            $entryIds = array_column($crossParents['data'], 'entryDbId');

            $entryRecords = $this->mainEntry->searchAll([
                'fields'=>'entry.id as entryDbId|entry.seed_id as seedDbId',
                'seedDbId' => 'is not null',
                'entryDbId'=> 'equals '.implode('|equals ', $entryIds)
            ]);
           
            if($entryRecords['totalCount'] > 0){
                $entryIds = array_column($entryRecords['data'], 'entryDbId');
                $seedDbIds = array_column($entryRecords['data'], 'seedDbId');
                $combinedArray = array_combine($entryIds, $seedDbIds);
                foreach ($crossParents['data'] as $record) {
                    $this->crossParent->updateOne($record['crossParentDbId'], ['seedDbId'=>$combinedArray[$record['entryDbId']].""]);
                }
            }
        }

        return $crossParents['totalCount'];   
    }

    /**
     * Get info from entry code to designation
     * @param array $crosses array of paired input crosses
     * 
     * @return array transformed array using the designation
     */
    public function getEntryDesignation($crossList, $experimentId){
        $updatedCrossList = [];

        $inputString = implode('|', $crossList);
        $crossListArray = array_unique(explode('|', $inputString));
    
        $entryRecords = $this->mainEntry->searchAll([
            'fields'=>'entry.entry_code as entryCode|germplasm.designation|experiment.id AS experimentDbId',
            'experimentDbId' => "equals $experimentId",
            'entryCode'=> 'equals '.implode('|equals ', $crossListArray)
        ]);

        if($entryRecords['totalCount'] > 0){
            $designations = array_column($entryRecords['data'], 'designation');
            $entryCodes = array_column($entryRecords['data'], 'entryCode');
            $combinedArray = array_combine($entryCodes, $designations);
            return [
                'combinedArray' => $combinedArray,
                'entryCodeArray' => $crossListArray
            ];
        }

        return [];
    }

    /**
     * Add note in experiment creation if design needs to be updated
     * @param integer $experimentId record ID of the experiment
     * 
     * 
     */
    public function addDesignNoteForUpdate($experimentId, $note){

        $update = $this->mainExperiment->updateOne($experimentId, ["notes"=>"$note"]);
    }

  }