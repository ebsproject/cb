<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for Experiment Planting Arrangement
 */

namespace app\models;

namespace app\modules\experimentCreation\models;

use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use app\models\Experiment;
use app\models\ExperimentData;
use app\modules\experimentCreation\models\PAEntriesModel;
use app\models\Variable;

use Yii;

class PAWorkingListModel extends \yii\base\Model {
    public $entryNumber;
    public $replicate;
    public $designation;

    protected $experiment;
    protected $experimentData;
    protected $paEntriesModel;
    protected $variable;

    public function __construct(
        Experiment $experiment,
        ExperimentData $experimentData,
        PAEntriesModel $paEntriesModel,
        Variable $variable
    ) {
        $this->experiment = $experiment;
        $this->experimentData = $experimentData;
        $this->paEntriesModel = $paEntriesModel;
        $this->variable = $variable;
    }

    /**
     * Generates rules for filtering
     */
    public function rules() {
        return [
            [['entryNumber', 'replicate'], 'integer'],
            [['designation'], 'string'],
            [['entryNumber', 'replicate', 'designation'], 'safe'],
        ];
    }

    /**
     * Search model for germplasm
     * @param $params array list of parameters
     * @param $experimentId experiment identifier
     *
     * @return $dataProvider array germplasm browser dataprovider
     */
    public function search($params, $experimentId) {
        $filter = isset($params['PAWorkingListModel'])? $params['PAWorkingListModel'] : [];
        $sort = isset($params['grid-entries-working-list-sort'])? $params['grid-entries-working-list-sort'] : '';

        $expData = $this->experimentData->getExperimentData($experimentId, ['abbrev' => "equals PA_ENTRY_WORKING_LIST"]);
        if(!isset($expData[0])) {   // working list does not exist yet
            return new ArrayDataProvider([]);
        }
        $result = json_decode($expData[0]['dataValue'], true);

        for ($i=0; $i < count($result); $i++) { // add index to each replicate
            $result[$i]['index'] = $i;
        }

        if($sort != '') {
            if(!str_starts_with($sort, '-')) {  // ascending
                usort($result, function($a, $b) use ($sort) {
                    return $a[$sort] <=> $b[$sort];
                });
            } else {    // descending
                $sort = substr($sort, 1);
                usort($result, function($a, $b) use ($sort) {
                    return $b[$sort] <=> $a[$sort];
                });
            }
        }

        if(!empty($filter)) {
            $result = array_filter($result, function($rep) use ($filter) {
                return ($filter['entryNumber'] != ''? $rep['entryNumber'] == $filter['entryNumber'] : 1) &&
                    ($filter['replicate'] != ''? $rep['replicate'] == $filter['replicate'] : 1) &&
                    ($filter['designation'] != ''? $rep['designation'] == $filter['designation'] : 1)
                ;
            });
        }

        $result = array_values($result);

        Yii::$app->session->set('PA-working-list-all_filtered_index', array_column($result, 'index'));
        $this->saveToWorkingListExperimentData($experimentId, array_column($result, 'index'), 'PA_WL_ALL_FILTERED_INDEX');

        if(json_encode($filter) != Yii::$app->session['PA-working-list-filter']) {  // reset selection if new filters
            Yii::$app->session->set('PA-working-list-filter', json_encode($filter));
            Yii::$app->session->set('PA-working-list-selected_index', []);
            $this->saveToWorkingListExperimentData($experimentId, [], 'PA_WL_SELECTED_INDEX');
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'totalCount' => count($result),
            'id' => 'grid-entries-working-list',
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'entryNumber',
                    'replicate',
                    'designation'
                ],
                'defaultOrder' => [
                    'entryNumber' => SORT_ASC
                ],
            ]
        ]);

        //validate results
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    /**
     * Get working list variable data to experiment data
     *
     * @param integer $experimentId record id of the experiment
     * @param string $varAbbrev variable abbrev of data to be saved (ex. 'PA_WL_ALL_FILTERED_INDEX', 'PA_WL_SELECTED_INDEX')
     */
    public function getWorkingListExperimentData($experimentId, $varAbbrev) {
        //get working list variable data to experiment data value
        $expData = $this->experimentData->getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);

        if(!isset($expData[0])) {   // working list does not exist yet
            return null;
        }
        $result = json_decode($expData[0]['dataValue'], true);
        $result = array_values($result);

        return $result;
    }

    /**
     * Save working list variable data to experiment data
     *
     * @param integer $experimentId record id of the experiment
     * @param array $data entry information with replicates
     * @param string $varAbbrev variable abbrev of data to be saved (ex. 'PA_WL_ALL_FILTERED_INDEX', 'PA_WL_SELECTED_INDEX')
     */
    public function saveToWorkingListExperimentData($experimentId, $data, $varAbbrev) {
        //get working list variable data to experiment data value
        $variableInfo = $this->variable->searchAll(["abbrev"=>"$varAbbrev"]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        $expData = $this->experimentData->getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);

        if(isset($expData[0])){
            $dataRequestBodyParamUpdate = [
                'dataValue' => json_encode($data)
            ];
            $exptDataDbId = $expData[0]['experimentDataDbId'];
            $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);

        } else {  //create variable
            $dataRequestBodyParam[] = [
                'variableDbId' => "".$variableDbId,
                'dataValue' => json_encode($data)
            ];
            $test = $this->experiment->createExperimentData($experimentId,$dataRequestBodyParam);
        }
    }

    /**
     * Remove replicates from working list
     * @param $experimentId Experiment identifier
     * @param $selectedIds Array of indices of selected replicates
     */
    public function removeFromWorkingList($experimentId, $selectedIds) {
        $expData = $this->experimentData->getExperimentData($experimentId, ['abbrev' => "equals PA_ENTRY_WORKING_LIST"]);
        $workingList = json_decode($expData[0]['dataValue'], true);

        usort($selectedIds, function($a, $b) {
            return $b <=> $a;
        });

        foreach ($selectedIds as $index) {
            unset($workingList[$index]);
        }
        $workingList = array_values($workingList);

        $this->paEntriesModel->saveWorkingList($experimentId, $workingList);
    }

    /**
     * Reorder replicates in working list
     * @param $experimentId Experiment identifier
     * @param $reorderMethod Indicates the type of reorder to be done (swap of cascade)
     * @param $originalOrderNo Array of original order number of selected replicates
     * @param $newOrderNo Array of new order number for selected replicates
     */
    public function reorderSelectedInWorkingList($experimentId, $reorderMethod, $originalOrderNo, $newOrderNo) {
        $result = [ // default values
            'success' => false,
            'data' => '',
            'message' => 'There was a problem in loading content. Please report using "Feedback".'
        ];

        $expData = $this->experimentData->getExperimentData($experimentId, ['abbrev' => "equals PA_ENTRY_WORKING_LIST"]);
        $workingList = json_decode($expData[0]['dataValue'], true);
        $totalReps = count($workingList);

        $originalOrderNo = array_map('intval', $originalOrderNo);
        $newOrderNo = array_map('intval', $newOrderNo);

        if($reorderMethod == 'Swap') {
            // check if values for new order number are valid (if they all exists in old order number)
            if(!empty(array_diff($originalOrderNo,$newOrderNo))) {
                $result['message'] = 'You have entered an invalid input. For Swap, please only choose values from the original order numbers.';
                return $result;
            } else if(count($originalOrderNo) < 2) {
                $result['message'] = 'For Swap, please select at least 2 replicates to swap.';
                return $result;
            }

            // copy existing working list to new working list then swap values
            $newWorkingList = $workingList;
            for ($i=0; $i < count($newOrderNo); $i++) {
                $newWorkingList[$newOrderNo[$i]-1] = $workingList[$originalOrderNo[$i]-1];
            }
        } else if($reorderMethod == 'Cascade') {
            // check if all values are within 0 to total number of replicates in working list
            foreach($newOrderNo as $orderNo) {
                if($orderNo < 1 || $orderNo > $totalReps) {
                    $result['message'] = 'You have entered an invalid input. For Cascade, please only enter values between 1 up to the max number of replicates in the working list.';
                    return $result;
                }
            }

            // copy existing working list to new working list then swap values
            $newWorkingList = $workingList;
            $tempWorkingList = [];

            // get all replicates to be transfered to new positions
            foreach($originalOrderNo as $orderNo) {
                $tempWorkingList[] = $workingList[$orderNo-1];
            }

            // arrange $originalOrderNo by descending
            usort($originalOrderNo, function($a, $b) {
                return $b <=> $a;
            });

            // remove replicates in $newWorkingList starting from last
            foreach($originalOrderNo as $orderNo) {
                unset($newWorkingList[$orderNo-1]);
            }
            $newWorkingList = array_values($newWorkingList);

            // insert the replicates from the temp to the $newWorkingList from the beginning
            for ($i=0; $i < count($newOrderNo); $i++) {
                array_splice($newWorkingList, $newOrderNo[$i]-1, 0, [$tempWorkingList[$i]]);
            }
        }

        // save new working list
        $this->paEntriesModel->saveWorkingList($experimentId, $newWorkingList);

        $result['success'] = true;
        $result['message'] = 'Success';

        return $result;
    }

    /**
     * Generates the dataprovider for the selected items in the working list
     * @param $replicates Array of selected items
        */
    public function getSelectedWorkingListProvider($replicates) {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $replicates,
            'totalCount' => count($replicates),
            'pagination' => false,
            'sort' => false
        ]);

        return $dataProvider;
    }

    /**
     * Generates the dataprovider for the working list
     * @param $entries Array of selected entries
     */
    public function getWorkingListProvider($entries) {
        $totalCount = $entries['totalCount'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $entries['data'],
            'totalCount' => $totalCount,
            'key' => 'entryDbId',
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'entryNumber',
                    'replicate',
                    'designation'
                ],
                'defaultOrder' => [
                    'entryNumber' => SORT_ASC
                ],
            ]
        ]);

        return $dataProvider;
    }
}