<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for Analysis
*/

namespace app\models;
namespace app\modules\experimentCreation\models;
use yii\web\UploadedFile;
use app\models\User;

use app\modules\experimentCreation\models\UploadModel;


use Yii;

class AnalysisModel
{

  /**
   * Get design catalogue
   *
   */
  public function getDesignCatalogue(){
    //get design catalogue
    $graphQLquery = 'query {designModel:findCatalog(class:DESIGN_MODEL){ id code children { id code name variableLabel orderNumber parentPropertyId type description}}}';
    
    try {
      $client = new \GuzzleHttp\Client();
    
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $response = $client->request('POST', getenv('EBS_SG_AF_API_URL'), 
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json',
          ],
          'body' => json_encode(
            ['query' => $graphQLquery]
            )
        ]
      );
      
      return json_decode($response->getBody()->getContents(), true);

    } catch (\GuzzleHttp\Exception\ClientException $e){
      return null;
    }

  }

  /**
   * Get design catalogue from new framework
   * @param string $design name of design to be filtered
   *
   */
  public function getDesigns($design = null){
    try {

      $designs = [];
      $designInfo = [];
      $designDesc = [];

      $client = new \GuzzleHttp\Client();
  
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $urlResource = 'v1/designs?designName='.$design;
      if($design == null){
        $urlResource = 'v1/designs';
      }
      
      $response = $client->request('GET', getenv('BA_API2_URL').$urlResource,
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json',
          ]
        ]
      );
      
      $results = json_decode($response->getBody()->getContents(), true);

      if(isset($results['result']['data'][0]) && count($results['result']['data']) > 0){
        foreach($results['result']['data'] as $key=>$var){
          $designs[$var['designDbId']] = $var['designName'];

          $designInfo[$var['designDbId']]  = [
              'designCode' => $var['designCode'],
              'designName' => $var['designName']
          ];

          $designDesc[$var['designDbId']] = $var['description'];
        } 
      }

      return [
        'designArray' => $designs,
        'designInfo' => $designInfo,
        'designDesc' => $designDesc
      ];
    } catch (\Exception $e) {
      Yii::error($e);
      
      $return['message']  = $e->getMessage();
      return [
        'designArray' => $designs,
        'designInfo' => $designInfo,
        'designDesc' => $designDesc
      ];
    }

  }


   /**
   * Get design catalogue from new framework
   * @param string $scriptCode code of design to be filtered
   *
   */
  public function getDesignScripts($scriptCode = null, $stageCode = null){
    try {

      $designs = [];
      $designInfo = [];
      $designDesc = [];

      $client = new \GuzzleHttp\Client();

      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $urlResource = "v1/design-scripts?scriptCode=".$scriptCode;
      if($scriptCode == null){
        $urlResource = "v1/design-scripts";
        if($stageCode != null){
          $urlResource .= "?stageCode=".$stageCode;
        }
      }
      
      $response = $client->request('GET', getenv('BA_API2_URL').$urlResource,
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json',
          ]
        ]
      );

      $results = json_decode($response->getBody()->getContents(), true);

      if(isset($results['result']['data'][0]) && count($results['result']['data']) > 0){
        foreach($results['result']['data'] as $key=>$var){
          if($var['name'] != 'Sparse Testing'){
            $designs[$var['scriptDbId']] = $var['name'];

            $designInfo[$var['scriptDbId']]  = [
                'designCode' => $var['design']['designName'],
                'designName' => $var['name']
            ];

            $designDesc[$var['designDbId']] = $var['design']['description'];
          }
        } 
      }
      
      return [
        'designArray' => $designs,
        'designInfo' => $designInfo,
        'designDesc' => $designDesc
      ];
    } catch (\Exception $e) {
      Yii::error($e);
      
      $return['message']  = $e->getMessage();
      return [
        'designArray' => $designs,
        'designInfo' => $designInfo,
        'designDesc' => $designDesc
      ];
    }

  }

  /**
   * Get the design properties
   * @param $designId integer record id of the selected design
   * 
   */
  public function getDesignProperty($designId){
    
    $graphQLquery = 'query {designModel: findProperty(propertyId:'.$designId.', parentPropertyId:4){id code name type dataType parentPropertyId orderNumber ui { visible minimum maximum multiple defaultValue disabled} meta{ code value } children{ id code name type dataType description variableLabel orderNumber parentPropertyId layoutVariable ui{ id visible minimum maximum multiple defaultValue disabled catalogue} rules{id propertyId type expression group notification action orderNumber}}}}';

    $client = new \GuzzleHttp\Client();
    
    $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
  
    $response = $client->request('POST', getenv('EBS_SG_AF_API_URL'), 
      [
        'headers' => [
          'Authorization' => 'Bearer ' . $accessToken,  
          'Content-Type' => 'application/json'
        ],
        'body' => json_encode(
          ['query' => $graphQLquery]
          )
      ]
    );       
    
    return $designProperties = json_decode($response->getBody()->getContents(), true);
    

  }

  /**
   * Submit randomization request
   * @param $inputArray array list of information to be submitted
   * 
   */
  public function submitRequest($inputArray){
    
    $requestString = 'mutation {createRequest(request:{ ';
    foreach($inputArray['metadata'] as $key => $value){

      if($key == 'experiment_id'){
        $requestString .= $key.':'.$value.' ';
      } else {
        if(is_array($value)){
          if(strpos($key, '_id') || strpos($key, 'Id') || strpos($key, 'number')){
            $value = '['.implode(',',$value).']';
          } else {
            $value = '["'.implode('","',$value).'"]';
          }
          $requestString .= $key.':'.$value.' ';
        } else {
          $requestString .= $key.':"'.$value.'" ';
        }
      }
      
    }

    $requestString .=  'parameters: [';
    foreach($inputArray['parameters'] as $key => $value){
      if(is_array($value)){
        if(strpos($key, '_id') || strpos($key, 'Id') || strpos($key, 'number')){
          $value = '['.implode(',',$value).']';
        } else {
          $value = '["'.implode('","',$value).'"]';
        }
        $requestString .= '{code:"'.$key.'", val:'.$value.'} ';
      } else {
        $requestString .= '{code:"'.$key.'", val:"'.$value.'"} ';
      }

    }

    $requestString .=  '] entryList: {';
    $entryListArray = array();
    $specialCharList = '/[\'\/\\`\\\"]/';
    foreach($inputArray['entryList'] as $key => $value){
      if(is_array($value)){
        if(strpos($key, '_id') || strpos($key, 'Id') || strpos($key, 'number') || $key == 'nrep'){
          $value = '['.implode(',',$value).']';
        } else {
          if($key == 'entry_name'){
            $tempValue = [];
            foreach($value as $temp){
              if(preg_match($specialCharList, $temp)){
                $escapeBackslash = preg_replace("/\\\/",'\\\\\\\\',$temp);
                $temp = $escapeBackslash;
              }
              $tempValue[] = $temp;
            }
            $value = '["'.implode('","',$tempValue).'"]';
          } else {
            $value = '["'.implode('","',$value).'"]';
          }
        }
        $entryListArray[] =$key.':'.$value;
      } else {
        $entryListArray[] =$key.':"'.$value.'"';
      }
    }

    $entryListStr = $requestString.implode(',', $entryListArray);

    $requestString =  $entryListStr.'}}){metadata {id timestamp}}}';

    $graphQLquery = $requestString;

    $client = new \GuzzleHttp\Client();
        
    $return['success'] = false;
    $return['requestId'] = NULL;

    try{
      
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $response = $client->request('POST', getenv('EBS_SG_AF_API_URL'), 
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json'
          ],
          'body' => json_encode([
              'query' => $graphQLquery,
              'variables' => null
            ])
        ]
      );       
      
      $results = json_decode($response->getBody()->getContents(), true);
      
      if(isset($results['data']['createRequest']['metadata']['id'])){
        $return['success'] = true;
        $return['requestId'] = $results['data']['createRequest']['metadata']['id'];

        return $return;
      }
    } catch (Exception $e){
      
      $return['success'] = false;
      $return['requestId'] = NULL;
      return $return;
    }

    return $return;
  }

  /**
   * Get design catalogue
   *
   */
  public function getRequestStatus($requestId, $design){

    if(!in_array(strtolower($design), ['unspecified', 'sparse'])){
      $parsedId = explode('_', $requestId);
      //get design catalogue
      $graphQLquery = 'query{findRequest(id:"'.$parsedId[0].'"){status}}';
      
      $client = new \GuzzleHttp\Client();
      
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
      
      $response = $client->request('POST', getenv('EBS_SG_AF_API_URL'), 
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json',
          ],
          'body' => json_encode(
            ['query' => $graphQLquery]
            )
        ]
      );
      
      $results = json_decode($response->getBody()->getContents(), true);
    } else {

      $results['data']['findRequest']['status'] = 'error';
      $results['errors'] = "There's a problem submitting the request.";
      $client = new \GuzzleHttp\Client();
    
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $response = $client->request('GET', getenv('BA_API2_URL').'v1/design-requests/'.$requestId, 
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json',
          ]
        ]
      );
      $response = json_decode($response->getBody()->getContents(),true);

      if(isset($response['result']['data'][0]['requestStatus']) && in_array($response['result']['data'][0]['requestStatus'], ['open', 'closed'])){
        $results['data']['findRequest']['status'] = ($response['result']['data'][0]['requestStatus'] == 'closed') ? 'completed':'submitted';
      } else if(isset($response['result']['data'][0]['requestStatus']) && in_array($response['result']['data'][0]['requestStatus'], ['canceled'])){
        $results['data']['findRequest']['status'] = 'failed';
        $results['errors'] = $response['result']['data'][0]['message'] ?? "Error encountered while running the process.";
      } else {
        $results['data']['findRequest']['status'] = 'error';
        $results['errors'] = "There's a problem submitting the request.";
      }
    }

    return $results;
    
  }

  /**
   * Submit design file upload
   * @param $inputArray array list of information to be submitted
   * 
   */
  public function submitDesignFile($inputArray){
    $requestString = 'mutation {createFileDesignRequest(request:{ ';
    
    $requestString .=  ' occurrenceIds: [';
    $occurrenceIdStr = implode(',', $inputArray['occurrenceIds']);
    $requestString .= $occurrenceIdStr.']';
    
    $requestString .=  ' entryIds: [';
    $entryIdsStr = implode(',', $inputArray['entryIds']);
    $requestString .= $entryIdsStr.']';

    $requestString .=  ' designFileName: "'.$inputArray['designFileName'].'.csv"'.'}){status metadata{id timestamp occurrence_id experiment_id}}}';
    
    $graphQLquery = $requestString;
    
    $client = new \GuzzleHttp\Client();
        
    $return['success'] = false;
    $return['requestId'] = NULL;

    try{
      
      $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

      $response = $client->request('POST', getenv('EBS_SG_AF_API_URL'), 
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-Type' => 'application/json'
          ],
          'body' => json_encode([
              'query' => $graphQLquery,
              'variables' => null
            ])
        ]
      );       
      
      $results = json_decode($response->getBody()->getContents(), true);
      
      if(isset($results['data']['createFileDesignRequest']['metadata']['id'])){
        $return['success'] = true;
        $return['requestId'] = $results['data']['createFileDesignRequest']['metadata']['id'];

        return $return;
      }
    } catch (Exception $e){
      
      $return['success'] = false;
      $return['requestId'] = NULL;
      return $return;
    }

    return $return;
  }

  /**
   * Submit design file upload for Unspecified
   * 
   * @param integer $id record id of the plan template
   * @param array $inputArray list of information to be submitted
   * 
   */
  public function submitUnspecifiedDesign($id, $inputArray){
      try {

          $uploadPath = Yii::getAlias('@webroot').'/analytics/output';
          $filename = $inputArray['designFileName'].'.csv';
          $tmpFileName = $uploadPath . '/' . $inputArray['designFileName'].'.csv';

          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          //download validated file first
          \Yii::$container->get('app\modules\experimentCreation\models\UploadModel')->downloadValidatedFile($filename, $id);

          $return['success'] = false;
          $return['requestId'] = NULL;
          $return['message'] = "";
          //send to validation first
          $curl = curl_init();

          $data = array( 
              'file' => curl_file_create($tmpFileName, 'text/csv', $filename)
          ); 
          
          $occurrenceIdStr = implode('&occurrenceDbId=', $inputArray['occurrenceIds']);

          $userModel = new User();
          $userId = $userModel->getUserId();
          $params = "&occurrenceDbId=".$occurrenceIdStr."&creatorDbId=".$userId;

          curl_setopt_array($curl, [
              CURLOPT_URL => getenv('BA_API2_URL')."v1/design-requests/upload?experimentDbId=".$id.$params,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_FOLLOWLOCATION => true, 
              CURLOPT_VERBOSE => 1, 
              CURLOPT_POST => 1, 
              CURLOPT_POSTFIELDS => $data,
              CURLOPT_HTTPHEADER => [
                  "Authorization: Bearer ". $accessToken,
                  "content-type: multipart/form-data"
              ],  
          ]);

          $response = curl_exec($curl);
          $err = curl_error($curl);
        
          curl_close($curl);

          if ($err) {
              $return['success'] = false;
              $return['message'] = $err;
              $return['requestId'] = NULL;
             
          } else {
              $response = json_decode($response, true);
              if(isset($response['result']['data'][0]['requestStatus']) && in_array($response['result']['data'][0]['requestStatus'], ['open', 'closed'])){
                  $return['success'] = $response['result']['data'][0]['requestStatus'];
                  $return['requestId'] = $response['result']['data'][0]['requestDbId'];
              } else {
                  $return['success'] = false;
                  $return['message'] = $response['status']['message'] ?? 'Upload failed. Please try again.';
                  $return['requestId'] = NULL;
              }
          }

          return $return;
      } catch (\Exception $e) {
        
          if ($e->getMessage() == 'max(): Array must contain at least one element') {
              $return['message']  = 'Please check if file is not empty.';
              return $return;
          }
          
          $return['message']  = $e->getMessage();
          return $return;
      }
  }

  /**
   * Submit design file upload new workflow
   * 
   * @param integer $id record id of the plan template
   * @param array $inputArray list of information to be submitted
   * 
   */
  public function submitDesignNew($id, $inputArray){
      try {

          $uploadPath = Yii::getAlias('@webroot').'/analytics/output';
          $filename = $inputArray['designFileName'].'.csv';
          $tmpFileName = $uploadPath . '/' . $inputArray['designFileName'].'.csv';

          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          //download validated file first
          \Yii::$container->get('app\modules\experimentCreation\models\UploadModel')->downloadValidatedFile($filename, $id);

          $return['success'] = false;
          $return['requestId'] = NULL;
          $return['message'] = "";
          //send to validation first
          $curl = curl_init();

          $data = array( 
              'file' => curl_file_create($tmpFileName, 'text/csv', $filename)
          ); 
          
          $occurrenceIdStr = implode('&occurrenceDbId=', $inputArray['occurrenceIds']);

          $userModel = new User();
          $userId = $userModel->getUserId();
          $params = "&occurrenceDbId=".$occurrenceIdStr."&creatorDbId=".$userId;

          curl_setopt_array($curl, [
              CURLOPT_URL => getenv('BA_API2_URL')."v1/design-requests/upload?experimentDbId=".$id.$params,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_FOLLOWLOCATION => true, 
              CURLOPT_VERBOSE => 1, 
              CURLOPT_POST => 1, 
              CURLOPT_POSTFIELDS => $data,
              CURLOPT_HTTPHEADER => [
                  "Authorization: Bearer ". $accessToken,
                  "content-type: multipart/form-data"
              ],  
          ]);

          $response = curl_exec($curl);
          $err = curl_error($curl);
          
          curl_close($curl);

          if ($err) {
              $return['success'] = false;
              $return['message'] = $err;
              $return['requestId'] = NULL;
             
          } else {
              $response = json_decode($response, true);
              if(isset($response['result']['data'][0]['requestStatus']) && in_array($response['result']['data'][0]['requestStatus'], ['open', 'closed'])){
                  $return['success'] = $response['result']['data'][0]['requestStatus'];
                  $return['requestId'] = $response['result']['data'][0]['requestDbId'];
              } 
          }

          return $return;
      } catch (\Exception $e) {
        
          if ($e->getMessage() == 'max(): Array must contain at least one element') {
              $return['message']  = 'Please check if file is not empty.';
              return $return;
          }
          
          $return['message']  = $e->getMessage();
          return $return;
      }
  }

  /**
   * Get parameters of selected design
   * 
   * @param integer $designId id of selected design
   * 
   */
  public function getDesignParameters($designId){
      try {

          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          $client = new \GuzzleHttp\Client();
          
          $response = $client->request('GET', getenv('BA_API2_URL')."v1/design-scripts?scriptDbId=".$designId,
            [
              'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json',
              ]
            ]
          );
          $results = [];
          $results = json_decode($response->getBody()->getContents(), true);

          return $results;
      } catch (\Exception $e) {
          Yii::error($e);
          
          $return['message']  = $e->getMessage();
          return $return;
      }
  }

  /**
   * Get parameters of selected design via name
   * 
   * @param string $designName name of selected design
   * 
   */
  public function getDesignParametersbyName($designName){
      try {

          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          $client = new \GuzzleHttp\Client();
      
          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          $response = $client->request('GET', getenv('BA_API2_URL')."v1/designs?".urlencode($designName),  
            [
              'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json',
              ]
            ]
          );
          
          $results = json_decode($response->getBody()->getContents(), true);

          return $results;
      } catch (\Exception $e) {
          Yii::error($e);
          
          $return['message']  = $e->getMessage();
          return $return;
      }
  }


  /**
   * Convert format of parameters
   * 
   * @param array $parameterArray list of parameters
   * @param string $type identifier for info type
   */
  public function convertDesignParameters($parameterArray,$type){

      $convertedArray = [];
      if(in_array($type, ['randomization', 'layout'])){
          $orderNumber = 1;
          foreach($parameterArray as $parameter){

              $temp['id'] = $parameter['variable']['variableDbId'] ?? null;
              $temp['code'] = $parameter['variable']['code'] ?? null;
              $temp['name'] = $parameter['variable']['name'] ?? null;
              $temp['type'] = "input";
              $temp['dataType'] = $parameter['variable']['dataType'] ?? null;
              $temp['description'] = $parameter['variable']['description'] ?? null;
              $temp['variableLabel'] = $parameter['variable']['name'] ?? null;
              $temp['orderNumber'] = $parameter['variable']['name'] ?? $orderNumber++;
              $temp['parentPropertyId'] = null;
              $temp['layoutVariable'] = $parameter['isLayout'] ?? false;

              // $tempUi['id'] = null;
              $tempUi['visible'] = !$parameter['isHidden'] ?? false;
              $tempUi['minimum'] = $parameter['minimumValue'] ?? null;
              $tempUi['maximum'] = $parameter['maximumValue'] ?? null;
              // $tempUi['multiple'] = null;
              $tempUi['defaultValue'] = $parameter['defaultValue'] ?? null;
              $tempUi['disabled'] = $parameter['isReadOnly'] ?? false;
              $tempUi['catalogue'] = (count($parameter['scriptInputScales']) > 0 && $temp['dataType'] != "boolean")? true : false;
              $temp['ui'] = $tempUi;

              $values = [];
              if(count($parameter['scriptInputScales']) > 0){
                  $catalogue = $parameter['scriptInputScales'];
                  //order values
                  usort($catalogue, function ($first, $second) {
                      return $first['orderNumber'] > $second['orderNumber'] ? 1 : -1;
                  });
                  foreach($catalogue as $val){
                      $values[$val['dataValue']] = $val['label'];
                  }
              } 
              $temp['ui']['values'] = $values;

              $rules = [];
              $ruleOrder = 1;
              $allowedValueFlag = false;

              if($parameter['scriptInputRules'] != null && count($parameter['scriptInputRules']) > 0){

                  $rulesArray = $parameter['scriptInputRules'];

                  if(count($parameter['scriptInputRules']) > 0){

                      //order values
                      usort($rulesArray, function ($first, $second) {
                          return $first['scriptInputRuleDbId'] > $second['scriptInputRuleDbId'] ? 1 : -1;
                      });
                  } 

                  foreach($rulesArray as $val){
                      // $tempRules['id'] = null;
                      // $tempRules['propertyId'] = null;
                      $tempRules['type'] = $val['type'];
                      $tempRules['expression'] = $val['expression'];
                      $tempRules['notification'] = $val['message'];
                      $tempRules['action'] = $val['severity'];
                      $tempRules['orderNumber'] = $ruleOrder++;
                      $rules[] = $tempRules;
                      if($val['type'] == 'allowed-value') {
                        $allowedValueFlag = true;
                      }
                  }
              } 

              if($allowedValueFlag){
                $temp['ui']['catalogue'] = true;
              }

              $temp['rules'] = $rules;

              $convertedArray[] = $temp;
          } 
      } 

      return $convertedArray;
  }

  /**
   * Submit design file upload for Unspecified
   * @param $id record id of experiment
   * @param $inputArray array list of information to be submitted
   */
  public function submitNewRequest($id, $inputArray){
      try {

          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

          $return['success'] = false;
          $return['requestId'] = NULL;
          $return['status'] = "failed";
          $return['message'] = "";
          $return['requestInfo'] = null;

          $client = new \GuzzleHttp\Client();
    
          $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
          
          $response = $client->request('POST', getenv('BA_API2_URL').'v1/design-requests', 
            [
              'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json'
              ],
              'body' => json_encode($inputArray)
            ]
          );       
          
          $response = json_decode($response->getBody()->getContents(), true);
          
          if(isset($response['result']['data'][0]['requestStatus']) && in_array($response['result']['data'][0]['requestStatus'], ['open', 'closed'])){
              $return['status'] = $response['result']['data'][0]['requestStatus'];
              $return['success'] = true;
              $return['requestId'] = $response['result']['data'][0]['requestDbId'];
              $return['requestInfo'] = $response;
          } else if(isset($response['detail'])){
              $return['success'] = false;
              $return['message'] = json_encode($response);
          }
          
      } catch (Exception $e){
      
        $return['success'] = false;
        $return['requestId'] = NULL;
        $return['message'] = $e;
      }

      return $return;
  }
}