<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* TODO: to be refactored
* Model class for Experiment
*/

// namespace app\models;
// namespace app\modules\experimentCreation\models;

// use app\models\Experiment;
// use app\models\User;
// use yii\data\SqlDataProvider;
// use app\modules\experimentCreation\models\ExperimentModel;

// use Yii;

// class CrossingMatrix extends Experiment
// {
// 	/**
// 	 * Get crossing matrix for the experiment
// 	 * @param $id integer experiment identifier
// 	 * @param $type text whether reset or reload
// 	 * @return mixed
// 	 */
// 	public static function getCrossingMatrixData($id, $type=''){
// 		$session = Yii::$app->session;

// 		if($type == 'reload' || $type == 'reset'){
// 			// if reload
// 			$xCategories = $session->get('xLabels-'.$id);
// 			$yCategories = $session->get('yLabels-'.$id);
// 			$maleCount = $session->get('xCount-'.$id);
// 			$femaleCount = $session->get('yCount-'.$id);

// 			if($type == 'reload'){
// 				$dataArr = CrossingMatrix::retrieveCrossingMatrixTempData($id);
// 			}else{
// 				$dataArr = $session->get('dataArr-'.$id);
// 			}
// 		}else{
// 			// if on first load
// 			$femaleParents = CrossingMatrix::getParents($id,'female');
// 			$maleParents = CrossingMatrix::getParents($id,'male');

// 			$femaleCount = count($femaleParents);
// 			$maleCount = count($maleParents);

// 			$dataArr = [];

// 			for ($i = 0; $i < $maleCount; $i++) {

// 				$xCategories[] = [
// 					'val' => $maleParents[$i]['designation'],
// 					'entno' => $maleParents[$i]['entno'],
// 					'id' => $maleParents[$i]['id'],
// 					'product_id' => $maleParents[$i]['product_id']
// 				];
				
// 				for ($j = 0; $j < $femaleCount; $j++) {
// 					$maleId = $maleParents[$i]['id'];
// 					$femaleId = $femaleParents[$j]['id'];
// 					$maleProductId = $maleParents[$i]['product_id'];
// 					$femaleProductId = $femaleParents[$j]['product_id'];

// 					$inMatrix = CrossingMatrix::checkIfInMatrix($femaleId, $maleId, $id);

// 					$plotArr = [
// 						'x' => $i,
// 						'y' => $j,
// 						'femaleentryid' => $femaleId,
// 						'maleentryid' => $maleId,
// 						'femaleproductid' => $femaleProductId,
// 						'maleproductid' => $maleProductId
// 					];

// 					if($maleId == $femaleId || $maleProductId == $femaleProductId){
// 						//if invalid pair
// 						$plotArr = array_merge($plotArr,['value'=>null]);
// 					}else if($inMatrix){
// 						// already in matrix
// 						$plotArr = array_merge($plotArr,['color'=>'#00838f','value'=>'✓']);
// 					}

// 					$dataArr[] = $plotArr;
// 				}
// 			}

// 			for ($j = 0; $j < $femaleCount; $j++) {
// 				$yCategories[] = [
// 					'val' => $femaleParents[$j]['designation'],
// 					'entno' => $femaleParents[$j]['entno'],
// 					'id' => $femaleParents[$j]['id'],
// 					'product_id' => $femaleParents[$j]['product_id']
// 				];
// 			}

// 			CrossingMatrix::updateTempTable(json_encode($dataArr), $id);

// 			$xCategories = isset($xCategories) && !empty($xCategories) ? $xCategories : null;
// 			$yCategories = isset($yCategories) && !empty($yCategories) ? $yCategories : null;

// 			$session->set('dataArr-'.$id, $dataArr);
// 			$session->set('xLabels-'.$id, $xCategories);
// 			$session->set('yLabels-'.$id, $yCategories);
// 			$session->set('xCount-'.$id, $maleCount);
// 			$session->set('yCount-'.$id, $femaleCount);
// 		}

// 		// get maximum length of string
// 		$maxLength = CrossingMatrix::getMaxMaleLength($id);

// 		return [
// 			'data'=>$dataArr,
// 			'xLabels'=>$xCategories,
// 			'yLabels'=>$yCategories,
// 			'yCount'=>$femaleCount,
// 			'xCount'=>$maleCount,
// 			'xMaxLength'=>$maxLength
// 		];

// 	}

// 	/**
// 	 * Get maximum male parent string length
// 	 *
// 	 * @param $id integer experiment identifier
// 	 * @return integer max count
// 	 */
// 	public static function getMaxMaleLength($id){
// 		$sql = "
// 			select 
// 				max(length(el.product_name))
// 			from 
// 				operational.entry_list_data ed,
// 				operational.entry_list el,
// 				master.variable v
// 			where
// 				ed.variable_id = v.id
// 				and v.abbrev = 'PARENT_TYPE'
// 				and ed.is_void = false
// 				and el.id = ed.entry_list_id
// 				and el.is_void = false
// 				and el.experiment_id = {$id}
// 				and (lower(value) = 'male' or lower(value) = 'female-and-male')
// 		";

// 		$result = Yii::$app->db->createCommand($sql)->queryScalar();

// 		return $result;
// 	}

// 	/**
// 	 * Reset crossing matrix
// 	 * @param $id integer experiment identifier
// 	 * @param $userId integer user identifier
// 	 */
// 	public static function resetCrossingMatrix($id, $userId){
// 		$tempTable = "temporary_data.crossing_matrix_".$id."_".$userId;

// 		// update selection
// 		$updateSql = "
// 			update 
// 				{$tempTable} 
// 			set 
// 				color = '', 
// 				value = case when male_entry_id = female_entry_id then null else '' end
// 			";
// 		Yii::$app->db->createCommand($updateSql)->execute();

// 		// set selected crosses
// 		$selectCrossSql = "
// 			update
// 				{$tempTable} td
// 			set 
// 				value = '✓',
// 				color = '#00838f'
// 			from
// 				operational.crossing_list cl
// 			where
// 				cl.experiment_id = {$id}
// 				and cl.female_entry_id = td.female_entry_id
// 				and cl.male_entry_id = td.male_entry_id
// 				and td.male_entry_id != td.female_entry_id
// 		";
// 		Yii::$app->db->createCommand($selectCrossSql)->execute();
// 	}

// 	/**
// 	 * Store crossing matrix in temporary table
// 	 *
// 	 * @param $data array list of crosses in crossing matrix
// 	 * @param $id integer experiment identifier
// 	 */
// 	public static function updateTempTable($data, $id){
// 	    $userId = User::getUserId();

// 		Yii::$app->db->createCommand("SELECT * from operational.save_crossing_matrix_data('".$data."',$id, $userId);")->queryScalar();
// 	}

// 	/**
// 	 * Update data in crossing matrix temporary table
// 	 *
// 	 * @param $experimentId integer experiment identifier
// 	 * @param $type integer whether x or y axis, 0 = x, 1 = y
// 	 * @param $pos integer position to be updated for bulk
// 	 * @param $femaleId integer female entry identifier
// 	 * @param $maleId integer male enntry identifier
// 	 * @param $method text whether bulk or single
// 	 * @param $value text value whether selected or not
// 	 */
// 	public static function updateCrossingMatrix($experimentId, $type, $pos, $femaleId, $maleId, $method, $value){
// 	    $userId = User::getUserId();
// 	    $tempTable = "temporary_data.crossing_matrix_".$experimentId."_".$userId;
// 	    $selectedColor = '#00838f'; // selected color
// 		$unselectedColor = ''; // unselected color
// 		$selectedVal = '✓';
// 		$unselectedVal = '';

// 		if($method == 'bulk'){
// 			// if method is bulk

// 			$axis = ($type == 0) ? 'x' : 'y';
// 			// check if all selected or not
// 			$checkCount = "select count(1) from ".$tempTable." where ".$axis." = ". $pos ." and female_entry_id != male_entry_id and female_product_id != male_product_id";
// 			$axisCount = Yii::$app->db->createCommand($checkCount)->queryScalar();

// 			$checkIfHasSelectedSql = "select count(1) from ".$tempTable." where ".$axis." = ". $pos. " and value = '✓' and female_entry_id != male_entry_id and female_product_id != male_product_id";

// 			$selectedCount = Yii::$app->db->createCommand($checkIfHasSelectedSql)->queryScalar();

// 			if($axisCount != $selectedCount){
// 				// if not all are selected
// 				$color = $selectedColor;
// 				$value = $selectedVal;
// 			}else{
// 				// if has selected, unselect all
// 				$color = $unselectedColor;
// 				$value = $unselectedVal;
// 			}

// 			$sql = "
// 				update 
// 					".$tempTable." 
// 				set
// 					color = '".$color."',
// 					value = '".$value."'
// 				where 
// 					".$axis." = ". $pos ."
// 					and female_entry_id != male_entry_id
// 					and female_product_id != male_product_id";
// 		}else{
// 			// if single selection
// 			if($value == $unselectedVal){
// 				// if already selected, unselect cell
// 				$newVal = $unselectedVal;
// 				$newColor = $unselectedColor;
// 			}else{
// 				// if not yet selected, select cell
// 				$newVal = $selectedVal;
// 				$newColor = $selectedColor;
// 			}

// 			$sql = "
// 				update
// 					".$tempTable."
// 				set
// 					color = '".$newColor."',
// 					value = '".$newVal."'
// 				where
// 					female_entry_id = {$femaleId}
// 					and male_entry_id = {$maleId}
// 					and female_entry_id != male_entry_id
// 					and female_product_id != male_product_id
// 			";
// 		}

// 		$res = Yii::$app->db->createCommand($sql)->execute();

// 	}

// 	/**
// 	* Retrieve crossing matrix data
// 	* @param $experimentId integer experiment identifier
// 	* @param defaultCols boolean whether to retrieve default columns or not
// 	*/
// 	public static function retrieveCrossingMatrixTempData($experimentId, $defaultCols = false){
// 		$userId = User::getUserId();
// 		$tempTable = "temporary_data.crossing_matrix_".$experimentId."_".$userId;

// 		$select = "
// 			select
// 				x,
// 				y,
// 				female_entry_id as femaleEntryId,
// 				male_entry_id as maleEntryId,
// 				color,
// 				value
// 		";

// 		$whereCond = '';
// 		if($defaultCols){
// 			$select = "
// 			select
// 				female_entry_id as femaleEntryId,
// 				male_entry_id as maleEntryId
// 			";
// 			$whereCond = " where value = '✓' ";
// 		}

// 		$from = "
// 			from
// 				{$tempTable}
// 				{$whereCond}
// 			order by
// 			x, y
// 		";

// 		$res = Yii::$app->db->createCommand($select . $from)->queryAll();

// 		return $res;
// 	}

// 	/**
// 	* Update crossing list using crossing matrix
// 	*
// 	* @param $data list of crosses to be added
// 	* @param $id integer experiment identifier
// 	* @param $userId integer user identifier
// 	*/
// 	public static function updateCrossingListByMatrix($data, $id, $userId){
// 		$data = json_encode($data);

// 		$voidSql = "delete from operational.crossing_list where experiment_id = {$id}";
// 		Yii::$app->db->createCommand($voidSql)->execute();

// 		$insertSql = "SELECT * from operational.get_cross_method('".$data."',$id, $userId);";
// 		Yii::$app->db->createCommand($insertSql)->queryScalar();

// 		//delete existing studies
// 		ExperimentModel::deleteExistingStudies($id);
// 		// update experiment status
// 		$model = Experiment::findOne($id);
// 		if(strpos($model->experiment_status,'cross list created') === false){
//             $experimentModel = Experiment::findOne($id);
//             $status = explode(';', $experimentModel->experiment_status);
//             $crossExists = ExperimentModel::checkCrosses($id);

//             if(!in_array('cross list created', $status) && $crossExists){

//                 $updatedStatus = !empty($status) ? implode(';', $status).';'.'cross list created' : 'cross list created';
//                 $experimentModel->experiment_status = $updatedStatus;
//                 $experimentModel->save();
//             }
//         }
// 	}

// 	/**
// 	 * Get parents of experiment
// 	 *
// 	 * @param $id integer experiment identifier
// 	 * @param $type text whether male or female, defaults to male
// 	 * @return $parent array list of parents
// 	 */
// 	public static function getParents($id,$type='male'){

// 		$parentCond = "and (lower(value) = 'male' or lower(value) = 'female-and-male')";
// 		$order = '';
// 		if($type == 'female'){
// 			$order = 'DESC';
// 			$parentCond = "and (lower(value) = 'female' or lower(value) = 'female-and-male')";
// 		}

// 		$sql = "
// 			select
// 				el.id,
// 				el.entlistno as entno,
// 				el.product_name as designation,
// 				el.product_id,
// 				p.generation
// 			from 
// 				operational.entry_list_data ed,
// 				operational.entry_list el,
// 				master.variable v,
// 				master.product p
// 			where
// 				ed.variable_id = v.id
// 				and v.abbrev = 'PARENT_TYPE'
// 				and ed.is_void = false
// 				and el.id = ed.entry_list_id
// 				and el.is_void = false
// 				and p.id = el.product_id
// 				and experiment_id = {$id}
// 				{$parentCond}
// 			order by entlistno {$order}
// 		";

// 		$result = Yii::$app->db->createCommand($sql)->queryAll();

// 		return $result;
// 	}

// 	/**
// 	 * Check if already in crossing list
// 	 *
// 	 * @param $femaleId integer female entry identifier
// 	 * @param $maleId integer male entry identifier
// 	 * @param $expId integer exoeriment identifier
// 	 * @return $res boolean whether in crossing list or not
// 	 */
// 	public static function checkIfInMatrix($femaleId, $maleId, $expId){
// 		$sql = "
// 			select 
// 				count(1) 
// 			from 
// 				operational.crossing_list
// 			where
// 				female_entry_id = {$femaleId}
// 				and male_entry_id = {$maleId}
// 				and experiment_id = {$expId}
// 				and is_void = false
// 			";

// 		$result = Yii::$app->db->createCommand($sql)->queryScalar();
// 		return $result;
// 	}

// 	/**
// 	 * Process crossing matrix data for preview
// 	 * @param $id integer crossing matrix identifier
// 	 * @param $userId integer user identifier
// 	 * @return $data array preview cross list data provider
// 	 */
// 	public static function proccessCrossingMatrixDataPreview($id, $userId){
// 		$tempTable = "temporary_data.crossing_matrix_".$id."_".$userId;

// 		$sql = "
// 			select
// 				td.id,
// 				female.product_name || '/' || male.product_name as cross_name,
// 				female.entlistno as female_entno,
// 				male.entlistno as male_entno,
// 				female_product.generation as female_generation,
// 				male_product.generation as male_generation
// 			from 
// 				{$tempTable} td
// 				left join
// 					operational.entry_list female
// 				on
// 					female.id = td.female_entry_id
// 				left join
// 					operational.entry_list male
// 				on
// 					male.id = td.male_entry_id
// 				left join
// 					master.product female_product
// 				on
// 					female_product.id = female.product_id
// 				left join
// 					master.product male_product
// 				on
// 					male_product.id = male.product_id
// 			where
// 				td.value = '✓'
// 		";

// 		$dataProvider = new SqlDataProvider([
// 			'sql' => $sql,
// 			'pagination' => false,
// 			'sort' => [
// 				'attributes' => ['female_entno','male_entno'],
// 				'defaultOrder' => ['female_entno'=> SORT_ASC,'male_entno'=>SORT_ASC]
// 			]
// 		]);

// 		return $dataProvider;
// 	}

// 	/**
// 	 * Get total number of selected crosses in crossing matrix
// 	 * @param $id integer experiment identifier
// 	 * @return $count integer total number of selected crosses
// 	 */
// 	public static function getNoOfSelectedCrosses($id){
// 		$userId = User::getUserId();

// 		$tempTable = "temporary_data.crossing_matrix_".$id."_".$userId;
// 		$sql = "select count(1) from {$tempTable} where value = '✓'";

// 		$count = Yii::$app->db->createCommand($sql)->queryScalar();

// 		return $count;
// 	}
// }