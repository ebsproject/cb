<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * TODO: Unused Model - remove when refactoring is done
 * Model for getting the process path information
 */

// namespace app\models;
// namespace app\modules\experimentCreation\models;

// use app\controllers\Dashboard;
// use Yii;
// use app\models\User;
// use app\models\Program;
// use app\models\Place;
// use app\models\Season;
// use app\models\Phase;
// use app\models\Experiment;
// use app\models\Item;
// use app\models\Variable;
// use app\models\Study;
// use app\models\CrossingList;
// use app\models\CrossList;
// use app\models\Entry;
// use app\models\Cross;
// use app\models\CrossMethod;
// use app\models\Product;
// use app\models\TempPlot;
// use app\models\TempEntry;
// use app\models\PlanTemplate;
// use app\models\EntryList;
// use app\models\RandomizationTransaction;
// use app\models\TempPlotMetadata;
// use app\modules\experimentCreation\models\ExperimentModel;

// use linslin\yii2\curl;
// use yii\data\SqlDataProvider;

// use yii\data\ActiveDataProvider;
// use yii\db\Expression;
// use DateTime;
// use ChromePhp;


// class GenerateRecordModel extends \yii\base\BaseObject{

  
//     /**
//      * Use HTTP requests to GET, PUT, POST and DELETE data
//      * 
//      * @param method string request method(POST, GET, PUT, etc)
//      * @param path string url path request
//      * @param rawData json optional request content
//      * 
//      * @return array list of requested information 
//      */
//     public static function getApiResponse($method, $path, $rawData = ['body'=>null]){

//         $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
//         $return = NULL;
//         try {

//             $client = new \GuzzleHttp\Client([
//                 'base_uri' => getenv('CB_API_V3_URL'), 
//                 'headers' => [
//                     'Authorization' => 'Bearer ' . $accessToken,
//                     'Content-Type' => 'application/json'
//                 ]
//             ]);

//             $response = $client->request($method, $path, $rawData);

//         }
//         catch (\GuzzleHttp\Exception\ClientException $e) {
//             $response = $e->getResponse();
//         }
//         $return = $response->getBody()->getContents();
//         $response->getBody()->close();
        
//         return json_decode($return,true);
          
//     }

//     /**
//      * Get cross parents
//      *
//      * @param $experimentId integer experiment identifier
//      * @return $results array  
//      */
//     public static function getParentsId($experimentId){
//         $source = "
//             select 
//               distinct on (el.entlistno)
//               el.id,
//               el.experiment_id,
//               el.entlistno
//               --(select p.designation from master.product p where p.id = el.product_id and p.is_void = false) as designation,
//               --(select eld.value from operational.entry_list_data eld where eld.entry_list_id = el.id and is_void = false) as parent_role,
//               --(select p.parentage from master.product p where p.id = el.product_id and p.is_void = false) as parentage
//             from
//               operational.crossing_list cl,
//               operational.entry_list el
//             where
//               el.experiment_id = {$experimentId}
//               and cl.experiment_id = el.experiment_id
//               and (cl.female_entry_id = el.id or cl.male_entry_id = el.id)
//               and el.is_void = false
//               and cl.is_void = false
//           ";
//         $results = Yii::$app->db->createCommand($source)->queryAll();

//         return array_column($results, 'id');
//     }

//     /**
//      * Get record information
//      *
//      * @param $table text table identifier
//      * @param $cond text query condition
//      * @return $record array record information
//      */
//     public function getRecord($table, $cond){

//         $sql = "select * from $table
//                 where $cond;";
//         $record = Yii::$app->db->createCommand($sql)->queryAll();

//         return $record;
//     }

//     /**
//      * Create service request for loc rep where process path is a service
//      * 
//      * @param $studyId integer study identifier
//      * @param $processPathId integer process path identifier
//      * @param $programId integer program identifier of the study
//      * @param $userId integer user identifier
//      *
//      */
//     public static function createServiceRequest($studyId, $processPathId, $programId, $userId){
//       // void previous study service request if any
//       $sql = "
//         update 
//           operational.service_request 
//         set 
//           is_void = true 
//         where
//           data_id = {$studyId}
//           and status = 'NEW'
//           and notes = 'created via experiment creation tool'
//           and is_void = false 
//       ";

//       Yii::$app->db->createCommand($sql)->execute();

//       // create service request transaction
//       $sql = "
//         insert into operational.service_request (entity_id, data_id, service_id, status,provider_program_id,requestor_program_id,requestor_user_id,creator_id,creation_timestamp, modification_timestamp,notes)
//         select 
//           (select id from dictionary.entity where abbrev = 'STUDY' and is_void = false limit 1) as entity_id,
//           (select id from operational.study where id = {$studyId}) as data_id,
//           service_id,
//           'NEW' as status,
//           (select program_id from master.program_team where team_id = ist.team_id) as provider_program_id,
//           {$programId} as requestor_program_id,
//           {$userId} as requestor_user_id,
//           {$userId} as creator_id,
//           now() as creation_timestamp,
//           now() as modification_timestamp,
//           'created via experiment creation tool' as notes
//         from
//           master.item_service_team ist
//         where
//           proc_path_id = {$processPathId}       
//       ";

//       Yii::$app->db->createCommand($sql)->execute();
//     }

//     /**
//      * Create crosses
//      *
//      * @param $crossListId integer cross list identifier
//      * @param $id integer experiment identifier
//      * @param $parentStudy integer parent study identifier
//      */
//     public function createCrosses($crosslistId, $id, $parentStudy=NULL){
//         //get crossing list
//         $femaleMale = <<<EOD
//             female.source_entry_id as "femaleEntryDbId",
//             male.source_entry_id as "maleEntryDbId"
// EOD;

//         if(!empty($parentStudy)){
//             $femaleMale = <<<EOD
//                 (select id from operational.entry where study_id = {$parentStudy} and product_id = female.product_id and is_void = false limit 1) as "femaleEntryDbId",
//                 (select id from operational.entry where study_id = {$parentStudy} and product_id = male.product_id and is_void = false limit 1) as "maleEntryDbId"
// EOD;
//         }

//         $sql = <<<EOD
//             select 
//                 {$crosslistId} as "crossListDbId",
//                 crossing_method_id as "crossMethodDbId",
//                 ROW_NUMBER () over (order by cl.id) + 1000 as crossno,
//                 ROW_NUMBER () over (order by cl.id) as entno,
//                 female.product_name || '/' || male.product_name as "crossName",
//                 {$femaleMale},
//                 cl.remarks
//             from
//                 operational.crossing_list cl
//                 left join
//                     operational.entry_list as female
//                 on female.id = cl.female_entry_id
//                 left join
//                     operational.entry_list as male
//                 on male.id = cl.male_entry_id
//             where
//                 cl.experiment_id = {$id}
//                 and cl.is_void = false
//             order by cl.id
// EOD;
//         $result = Yii::$app->db->createCommand($sql)->queryAll();

//         $data['records'] = $result;

//         $curl = new curl\Curl();
//         $url = getenv('CB_API_V3_URL').'crosses';
//         $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
//         $auth = 'Authorization: Bearer '.$accessToken;

//         $curl->setOption(CURLOPT_HTTPHEADER, array('Content-type:application/json'));
//         $curl->setOption(CURLOPT_HTTPHEADER, array(''.$auth.''));
//         $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//         $curl->setOption(CURLOPT_FORBID_REUSE, true);
//         $curl->setOption(CURLOPT_FRESH_CONNECT, true);
//         $crossesResponse = $curl->setRequestBody(json_encode($data))
//         ->setHeaders([
//             'Content-Type' => 'application/json',
//             'Content-Length' => strlen(json_encode($data))
//         ])
//         ->post($url);
//         $crossesResponse = json_decode($crossesResponse,true);

//         if(count($crossesResponse['result']['data']) > 0){
//             $crossDbIds = array_column($crossesResponse['result']['data'], 'crossDbId');
//             $success = true;
//         }else{
//             $crossDbIds = [];
//             $success = false;
//         }

//         return [
//             'success' => $success,
//             'crossDbIds' => $crossDbIds
//         ];

//     }

//     /**
//      * Old version of create crosses function
//      * This is retained for reference
//      * Please don't remove
//      */
//     public function createCrossesOld($crosslist, $id, $parentStudy=NULL){
//         //get crossing list count
//         $crosses = GenerateRecordModel::getRecord('operational.crossing_list', 'experiment_id='.$id.' and is_void=false order by id asc');
//         $crossNames = [];
//         $success = false;
//         $crossDbIds = [];

//         $repeatCount = ceil(count($crosses) / 5);
//         $repeat = 1;
//         $i = 0;

//         while($repeat <= $repeatCount){
//             $rawData = [];
//             $total = $i + 5;
//             while($i < $total){
//                 if(isset($crosses[$i])){
//                     $temp = [];
//                     $femaleEntry = NULL;
//                     $maleEntry = NULL;
//                     $temp['crossListDbId'] = $crosslist;
                    
//                     $temp['crossno'] = 1000+($i + 1);
//                     $temp['entno'] = $i+1;

//                     if(!isset($crossNames[$crosses[$i]['id']])){
//                         $femaleEntry = CrossingList::getDesignation((int)$crosses[$i]['female_entry_id']);
//                         $maleEntry = CrossingList::getDesignation((int)$crosses[$i]['male_entry_id']);
//                         $femaleName = isset($femaleEntry[0]['designation']) ? $femaleEntry[0]['designation'] : '';
//                         $maleName = isset($maleEntry[0]['designation']) ? $maleEntry[0]['designation'] : '';
//                         $crossname = $femaleName.'/'.$maleName;
//                         $crossNames[$crosses[$i]['id']] = $crossname;
//                     } else {
//                         $crossname = $crossNames[$crosses[$i]['id']];
//                     }
//                     $temp['crossName'] = $crossname;

//                     if(isset($parentStudy) && $parentStudy != NULL){
//                         if($femaleEntry == NULL){
//                             $femaleEntry = CrossingList::getDesignation((int)$crosses[$i]['female_entry_id']);
//                             $maleEntry = CrossingList::getDesignation((int)$crosses[$i]['male_entry_id']);
//                         }
//                         $entryFemale = GenerateRecordModel::getRecord('operational.entry', 'study_id = '.$parentStudy.' and is_void = false and product_id='.$femaleEntry[0]['product_id'].' limit 1');
//                         $entryMale = GenerateRecordModel::getRecord('operational.entry', 'study_id = '.$parentStudy.' and is_void = false and product_id='.$maleEntry[0]['product_id'].' limit 1');
                    
//                         $temp['femaleEntryDbId'] = $entryFemale[0]['id'];
//                         $temp['maleEntryDbId'] = $entryMale[0]['id'];
//                     } else {
            
//                         $entryFemale = GenerateRecordModel::getRecord('operational.entry_list', 'id='.$crosses[$i]['female_entry_id']);
//                         $entryMale = GenerateRecordModel::getRecord('operational.entry_list', 'id='.$crosses[$i]['male_entry_id']);
                    

//                         $temp['femaleEntryDbId'] = $entryFemale[0]['source_entry_id'];
//                         $temp['maleEntryDbId'] = $entryMale[0]['source_entry_id'];
//                     }
                    
//                     $temp['crossMethodDbId'] = 1;
//                     if(isset($crosses[$i]['remarks'])){
//                         $temp['remarks'] = $crosses[$i]['remarks'];
//                     }
                    
//                     $rawData['records'][] = $temp;
//                     $i++;
                    
//                 } else {
//                     break;
//                 }
//             }

//             $repeat++;
//             try{
//                 if(count($rawData) > 0){
                   
//                     $curl = new curl\Curl();
//                     $url = getenv('CB_API_V3_URL').'crosses';
//                     $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
//                     $auth = 'Authorization: Bearer '.$accessToken;

//                     $curl->setOption(CURLOPT_HTTPHEADER, array('Content-type:application/json'));
//                     $curl->setOption(CURLOPT_HTTPHEADER, array(''.$auth.''));
//                     $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//                     $curl->setOption(CURLOPT_FORBID_REUSE, true);
//                     $curl->setOption(CURLOPT_FRESH_CONNECT, true);
//                     $crossesResponse = $curl->setRequestBody(json_encode($rawData))
//                     ->setHeaders([
//                        'Content-Type' => 'application/json',
//                        'Content-Length' => strlen(json_encode($rawData))
//                     ])
//                         ->post($url);
//                     $crossesResponse = json_decode($crossesResponse,true);

//                     if(count($crossesResponse['result']['data']) > 0){

//                         $crossDbIds = array_merge($crossDbIds, array_column($crossesResponse['result']['data'], 'crossDbId'));
//                         $success = true;
                       
//                     } else {
//                         $crossDbIds = [];
//                         $success = false;
//                     }
//                 } else break;

//             } catch (Exception $error){
//                 $success = false;
//                 $crossDbIds = [];
//                 return [
//                     'success' => false,
//                     'crossDbIds' => [],
//                 ];
//             }
//         }

//         return[
//             'success' => $success,
//             'crossDbIds' => $crossDbIds,
//         ];
//     }

//     /**
//      * Delete products
//      * 
//      * @param $crossesId array crosses identifiers
//      */
//     public function deleteProducts($crossesId){
//         //get crosses
//         $sql = "select * from master.product where cross_id IN (".implode(',', $crossesId).")";
        
//         $products = Yii::$app->db->createCommand($sql)->queryAll();

//         if(count($products) > 0){
//             $productsId = array_column($products, 'id');
//             //delete entries
//             $sql = "delete from operational.temp_entry where product_id IN (".implode(',', $productsId).")";
//             $products = Yii::$app->db->createCommand($sql)->execute();

//             //delete products
//             $sql = "delete from master.product where cross_id IN (".implode(',', $crossesId).")";
            
//             Yii::$app->db->createCommand($sql)->execute();
//             return true;
//         }
//     }

//     /**
//      * Delete cross list
//      * @param $id integer cross list identifier
//      * @return boolean wether successful or not
//      */
//     public function deleteCrossList($id){
//         //get crosses
//         $sql = "select * from operational.cross where cross_list_id=".$id;
  
//         $crosses = Yii::$app->db->createCommand($sql)->queryAll();

//         if(count($crosses) > 0){

//             $crossesId = array_column($crosses, 'id');

//             $response=GenerateRecordModel::deleteProducts($crossesId);
//         }
        
//         //get crosses
//         $sql = "delete from operational.cross_list where id=".$id;

//         Yii::$app->db->createCommand($sql)->execute();
//         return true;
//     }

//     /**
//      * Create cross list
//      *
//      * @param $locationRepIds array list of location rep identifiers
//      * @param $id integer experiment identifier
//      */
//     public function createCrossList($locationRepIds, $id){
        
//         //get crossing list count
//         $crosses = GenerateRecordModel::getRecord('operational.crossing_list', 'experiment_id='.$id.' and is_void=false');
//         $crosslistIds = [];
//         $success = false;
//         $rawData = [];
//         //create cross list per location rep study
//         for($i=0; $i<count($locationRepIds); $i++){

//             //get study
//             $study = Study::findOne($locationRepIds[$i]);
//             // count max exp. number
//             $sql = "select
//                 max(number)
//               from
//                 operational.cross_list
//               where
//               program_id = {$study->program_id}
//               and place_id = {$study->place_id}
//               and season_id = {$study->season_id}
//               -- and phase_id = {$study->phase_id}
//               and year = {$study->year};";
  
//             $maxStudyNumber = Yii::$app->db->createCommand($sql)->queryScalar();
//             $number = $maxStudyNumber + 1;

//             $temp = [];
//             $temp['studyDbId'] = $study->id;
            
//             $temp['name'] = $study->name;
//             $temp['title'] = $study->study;
//             $temp['crossCount'] = count($crosses);
//             $temp['remarks'] = "created via experiment creation tool";

//             $rawData['records'][] = $temp;

          
//             //Check if there's no duplicate cross list for a study
//             $crossListExist = CrossList::find()->where(['study_id'=>$study->id, 'is_void'=>false])->all();

//             if(!empty($crossListExist)){
//                 //delete cross list
//                 foreach($crossListExist as $clexist){
//                     $response=GenerateRecordModel::deleteCrossList($clexist['id'],null);
//                 }
//             }
//             $cond = "name='$study->name' ";
//             //Delete previously created crosslist
//             $crossListLeft = GenerateRecordModel::getRecord('operational.cross_list', $cond.' and is_void=false');

//             if( !empty($crossListLeft) ){
//                 //delete calculhmac(clent, data)ross list
//                 foreach($crossListLeft as $clexist){
//                     $response=GenerateRecordModel::deleteCrossList($clexist['id'],null);
//                 }
//             }

//             $studyId = $study->id;
//         }

//         $crossCount = count($crosses);
//         $sql = "
//             insert into operational.cross_list (study_id, program_id, place_id, year, season_id, number, name, title, remarks, creator_id, cross_count, status, notes)
//             select 
//                 {$studyId}, program_id, place_id, year, season_id, number, name, title, remarks, creator_id, {$crossCount}, 'draft', 'created from Experiment creation tool'
//             from
//                 operational.study s
//             where
//                 id = {$studyId}
//             returning id;
//         ";

//         $crosslistIds = Yii::$app->db->createCommand($sql)->queryColumn();

//         if(!empty($crosslistIds)){
//             $success = true;
//         }

//         return [
//             'crosslistIds'=>$crosslistIds,
//             'success' => $success
//         ];
//     }

//     /**
//      * Create products
//      *
//      * @param $crossesDbIds array list of crosses identifiers
//      * @return array list of product identifiers
//      */
//     public function createProducts($crossesDbIds){
//         $crossesIdsStr = implode(',', $crossesDbIds);
//         $sql = <<<EOD
//             select 
//                 c.id as "crossDbId",
//                 cross_name as designation,
//                 case 
//                     when cm.name = 'backross' then 'BC1F1'
//                     else 'F1' end as generation,
//                 'cross_name' as "nameType",
//                 '?/?' as parentage,
//                 'progeny' as "productType",
//                 season_id as "seasonDbId",
//                 year
//             from
//                 operational.cross c,
//                 master.cross_method cm
//             where
//                 c.id in ($crossesIdsStr)
//                 and c.cross_method_id = cm.id
// EOD;
//         $result = Yii::$app->db->createCommand($sql)->queryAll();

//         $data['records'] = $result;

//         $curl = new curl\Curl();
//         $url = getenv('CB_API_V3_URL').'products';
//         $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
//         $auth = 'Authorization: Bearer '.$accessToken;

//         $curl->setOption(CURLOPT_HTTPHEADER, array('Content-type:application/json'));
//         $curl->setOption(CURLOPT_HTTPHEADER, array(''.$auth.''));
//         $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//         $curl->setOption(CURLOPT_FORBID_REUSE, true);
//         $curl->setOption(CURLOPT_FRESH_CONNECT, true);
//         $productsResponse = $curl->setRequestBody(json_encode($data))
//         ->setHeaders([
//             'Content-Type' => 'application/json',
//             'Content-Length' => strlen(json_encode($data))
//         ])
//         ->post($url);
//         $productsResponse = json_decode($productsResponse,true);

//         if(count($productsResponse['result']['data'])  > 0){
//             $productDbIds = array_column($productsResponse['result']['data'], 'productDbId');
//             $success = true;
//         } else {
//             $productDbIds = [];
//             $success = false;
//         }

//         return [
//             'productDbIds'=>$productDbIds,
//             'success' => $success
//         ];
//     }

//     /**
//      * Old version of create products
//      * Dont remove this for reference
//      */
//     public function createProductsOld($crossesDbIds){
//         $crosses = GenerateRecordModel::getRecord('operational.cross', 'id IN ('.implode(',', $crossesDbIds).') and is_void=false');
//         $rawData = [];
//         $success = false;
//         $productDbIds = [];

//         $repeatCount = ceil(count($crosses) / 5);
//         $repeat = 1;
//         $i = 0;

//         while($repeat <= $repeatCount){
//             $rawData = [];
//             $total = $i + 5;
//             while($i < $total){
//                 if(isset($crosses[$i])){
//                     $temp = [];
//                     $femaleEntry = NULL;
//                     $maleEntry = NULL;
//                     $temp['designation'] = $crosses[$i]['cross_name'];
//                     $temp['nameType'] = 'cross_name';
//                     $temp['productType'] = 'progeny';
//                     $temp['year'] = $crosses[$i]['year'];
//                     $temp['seasonDbId'] = $crosses[$i]['season_id'];
//                     $temp['parentage'] = '?/?';
//                     $temp['crossDbId'] = $crosses[$i]['id'];

//                     $generation = 'F1';
//                     $crossMethod = CrossMethod::findOne($crosses[$i]['cross_method_id']);
//                     if(strtolower($crossMethod['name']) == 'backcross'){
//                         $generation = 'BC1F1';
//                     }
//                     $temp['generation'] = $generation;

//                     if(isset($crosses[$i]['remarks'])){
//                         $temp['remarks'] = $crosses[$i]['remarks'];
//                     }

//                     $rawData['records'][] = $temp;
//                     $i++;
//                 } else {
//                     break;
//                 }
//             }
//             $repeat++;
//             try{
//                 if(count($rawData) > 0){

//                     $curl = new curl\Curl();
//                     $url = getenv('CB_API_V3_URL').'products';
//                     $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
//                     $auth = 'Authorization: Bearer '.$accessToken;

//                     $curl->setOption(CURLOPT_HTTPHEADER, array('Content-type:application/json'));
//                     $curl->setOption(CURLOPT_HTTPHEADER, array(''.$auth.''));
//                     $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//                     $curl->setOption(CURLOPT_FORBID_REUSE, true);
//                     $curl->setOption(CURLOPT_FRESH_CONNECT, true);
//                     $productsResponse = $curl->setRequestBody(json_encode($rawData))
//                     ->setHeaders([
//                        'Content-Type' => 'application/json',
//                        'Content-Length' => strlen(json_encode($rawData))
//                     ])
//                         ->post($url);
//                     $productsResponse = json_decode($productsResponse,true);
                    
//                     if(count($productsResponse['result']['data'])  > 0){
//                         $productDbIds = array_merge($productDbIds, array_column($productsResponse['result']['data'], 'productDbId'));
//                         $success = true;
//                     } else {
//                         $productDbIds = [];
//                         $success = false;
//                     }
//                 } else break;
//             } catch (Exception $error){
//                 $success = false;
//                 $productDbIds = [];
//                 return [
//                     'success' => false,
//                     'productDbIds' => [],
//                 ];
//             }
//         }

//         return [
//             'productDbIds'=>$productDbIds,
//             'success' => $success
//             ];
        
//     }

//     /**
//      * Update product id in operational.cross
//      *
//      * @param $productDbIds array list of product identifiers
//      */
//     public function updateCrosses($productDbIds){
//         $success = false;
//         $productDbIdsStr = implode(',', $productDbIds);
//         $sql = "
//             with t as(
//                 select id, cross_id from master.product where id in ($productDbIdsStr)
//             ) 
//             update
//                 operational.cross c
//             set
//                 product_id = t.id
//             from
//                 t
//             where 
//                 c.id = t.cross_id
//         ";

//         $result = Yii::$app->db->createCommand($sql)->execute();

//         if(!empty($result)){
//             $success = true;
//         }

//         return [
//           'success' => $success
//         ];
//     }

//     /**
//      * Old update crosses information function
//      * Please dont remove this for reference
//      */
//     public function updateCrossesOld($crossesDbIds, $productDbIds, $experimentId){
//         $success = true;

//         for($i=0; $i<count($productDbIds); $i++){
//             $productId = $productDbIds[$i];

//             $product =  GenerateRecordModel::getApiResponse('GET', "products/$productId", ['body'=>NULL]);
           
//             $product2 = Product::findOne($productId);
//             if(isset($product['result']['data'][0]['productDbId']) && $product['result']['data'][0]['productDbId'] == $productId){
//                 $crossId = $product['result']['data'][0]['crossDbId'];

//                 $temp['product_id'] = $productId;
//                 $rawData['records'][] = $temp;
//                 $cross = Cross::findOne($crossId);
//                 $cross->product_id = $productId;
//                 $cross->save();
//             } else {
//                 $success = false;
//             }
//         }

//         return [
//           'success' => $success
//         ];

//     }

//     /**
//      * Populate cross metadata
//      * 
//      */
//     public function populateStudyCrossMetadata($study){
//         $id = $study->experiment_id;
//         $studyId = $study->id;
//         /**For old implementation of cross list creation */

//         $userId = User::getUserId();
//         //delete and update parent type of the source entry id
//         $parentTypeVar = Variable::find()->where(['abbrev'=>'PARENT_TYPE', 'is_void'=>false])->one()['id'];
//         $deleteSql = "
//             delete from operational.entry_metadata em
//                 where 
//                     em.entry_id in (select el.source_entry_id from operational.entry_list el where experiment_id={$id})
//                     and em.variable_id = {$parentTypeVar} 
//         ";
//         Yii::$app->db->createCommand($deleteSql)->execute();

//         $insertParentTypeSql = "
//             insert into operational.entry_metadata(study_id, entry_id, variable_id, value, creator_id, notes)
//             select 
//                 el.source_study_id, el.source_entry_id, {$parentTypeVar}, eld.value, {$userId}, 'created via experiment creation tool'
//             from 
//                 operational.entry_list el,
//                 operational.entry_list_data eld
//             where el.experiment_id = {$id}
//                 and eld.entry_list_id = el.id
//                 and eld.variable_id = {$parentTypeVar}
//                 and el.is_void = false
//                 and eld.is_void = false;
//         ";

//         Yii::$app->db->createCommand($insertParentTypeSql)->execute();

//         $entrySourceSql = "
//             select el.source_entry_id from operational.entry_list el where experiment_id={$id};
//         ";
//         $entrySourceIds = Yii::$app->db->createCommand($entrySourceSql)->queryAll();
//         $entrySourceStr = json_encode(array_column($entrySourceIds, 'source_entry_id'));

//         $studySourceSql = "
//             select distinct (el.source_study_id) from operational.entry_list el where experiment_id={$id};
//         ";
//         $studySourceIds = Yii::$app->db->createCommand($studySourceSql)->queryAll();
//         $studySourceStr = json_encode(array_column($studySourceIds, 'source_study_id'));

//         //form selected crosses json
//         $crossSelectedSql = "
//             select     
//                 female.source_entry_id || '-' || male.source_entry_id as cross
//             from
//                 operational.crossing_list cl
//                 left join
//                     operational.entry_list as female
//                 on female.id = cl.female_entry_id
//                 left join
//                     operational.entry_list as male
//                 on male.id = cl.male_entry_id
//             where
//                 cl.experiment_id = {$id}
//                 and cl.is_void = false
//             order by cl.id
//         ";

//         $crossSelectedIds = Yii::$app->db->createCommand($crossSelectedSql)->queryAll();
//         $crossSelectedArray['selectedCrosses'] = array_column($crossSelectedIds, 'cross');
//         $crossSelectedArray['inputDesignation'] = [];
//         $crossSelectedArray['errorArr'] = [];
//         $crossSelectedStr = json_encode($crossSelectedArray);

//         $crossParentListVar = Variable::find()->where(['abbrev'=>'CROSS_PARENT_LIST', 'is_void'=>false])->one()['id'];
//         $parentStudiesVar = Variable::find()->where(['abbrev'=>'CROSS_PARENT_STUDIES', 'is_void'=>false])->one()['id'];
//         $crossSelectedVar = Variable::find()->where(['abbrev'=>'CROSS_SELECTED_CROSS_LIST', 'is_void'=>false])->one()['id'];
        
//         //delete existing values for metadata
//         $deleteMetaSql = "
//             delete from operational.study_metadata
//                 where study_id = {$studyId}
//                     and variable_id in ({$crossParentListVar}, {$parentStudiesVar}, {$crossSelectedVar});
//         ";

//         Yii::$app->db->createCommand($deleteMetaSql)->execute();

//         $insertStudyMetaSql = "
//             insert into operational.study_metadata(study_id, variable_id,value, creator_id, notes)
//             values 
//                 ({$studyId}, {$crossParentListVar}, '{$entrySourceStr}', {$userId}, 'created via experiment creation tool1'),
//                 ({$studyId}, {$parentStudiesVar}, '{$studySourceStr}', {$userId}, 'created via experiment creation tool1'),
//                 ({$studyId}, {$crossSelectedVar}, '{$crossSelectedStr}', {$userId}, 'created via experiment creation tool1');
//         ";

//         Yii::$app->db->createCommand($insertStudyMetaSql)->execute();
//         /****END***/

//     }

//     /**
//      * Populate cross metadata
//      * @param $study text study identifier
//      */
//     public function populateStudyCrossMetadata1($study){
//         //get crossing list 
//         $crossList = GenerateRecordModel::getRecord('operational.cross_list', 'study_id='.$study->id.' and is_void=false');
//         $userId = User::getUserId();
//         $timeStamp = date('Y-m-d H:m:s', time());

//         //update source entry for the parent type
       
//         $designVariable = Variable::find()->where(['abbrev'=>'PARENT_TYPE'])->one()['id'];
			
//         //update source entry of the roles
//         //set parent role
//         $experimentId = $study->experiment_id;
        
//         $sql = "
//             update operational.entry_metadata em
//             set value = i.value
//             from
//                 (
//                 select el.source_entry_id as entry_id, eld.value from 
//                 operational.entry_list el,
//                 operational.entry_list_data eld
//                 where
//                 el.experiment_id = {$experimentId}
//                 and el.id = eld.entry_list_id
//                 and eld.variable_id = 1904
//                 and el.is_void = false
//                 and eld.is_void = false
//                 ) i
//             where i.entry_id = em.entry_id
//         ";
//         Yii::$app->db->createCommand($sql)->execute();
		
//         if(isset($crossList[0])){
//             $crosses =  GenerateRecordModel::getRecord('operational.cross', 'cross_list_id='.$crossList[0]['id'].' and is_void=false');
//             $femaleEntries = array_column($crosses, 'female_entry_id');
            
//             $maleEntries = array_column($crosses, 'male_entry_id');
//             $mergedEntries = array_merge($femaleEntries, $maleEntries);
//             $mergedEntries = array_unique($mergedEntries, SORT_REGULAR);
//             $parentValStr = json_encode(array_map('strval',array_values($mergedEntries)));

//             $sql = "
//                 select female_entry_id ||'-'|| male_entry_id as selected
//                 from operational.cross
//                 where cross_list_id = {$crossList[0]['id']}
//                 and is_void = false;
//             ";

//             $selectedCrosses = Yii::$app->db->createCommand($sql)->queryAll();

//             $crossesInfo['selectedCrosses'] = array_column($selectedCrosses, 'selected');
//             $crossesInfo['inputDesignation'] = [];
//             $crossesInfo['errorArr'] = [];
//             $crossesInfoVal = json_encode($crossesInfo);

//             $studies = GenerateRecordModel::getRecord('operational.entry', 'id IN('.implode(',', $mergedEntries).') and is_void=false');
//             $studyIds = array_unique(array_column($studies, 'study_id'), SORT_REGULAR);
//             $studyValStr = json_encode(array_map('strval',array_values($studyIds)));

//             //Get variable_ids 
//             $parentsVar = GenerateRecordModel::getRecord('master.variable', "abbrev='CROSS_PARENT_LIST' and is_void=false");
//             $parentsVarId =$parentsVar[0]['id'];
//             $studiesVar = GenerateRecordModel::getRecord('master.variable', "abbrev='CROSS_PARENT_STUDIES' and is_void=false");
//             $studiesVarId = $studiesVar[0]['id'];
//             $crossSelectedVar = GenerateRecordModel::getRecord('master.variable', "abbrev='CROSS_SELECTED_CROSS_LIST' and is_void=false");
//             $crossSelectedVarId = $crossSelectedVar[0]['id'];
//             $studyId = $study->id;
//             $parentStr = "($studyId,$parentsVarId,'$parentValStr',$userId,'$timeStamp')";
//             $studyStr = "($studyId,$studiesVarId,'$studyValStr',$userId,'$timeStamp')";
//             $crossSelectedStr = "($studyId,$crossSelectedVarId,'$crossesInfoVal',$userId,'$timeStamp')";
//             $metaInsertStr = "
//                 insert into
//                     operational.study_metadata
//                 (study_id,variable_id,value,creator_id,creation_timestamp)
//                 values
//                 {$parentStr},
//                 {$studyStr},
//                 {$crossSelectedStr}
//             ";

//             Yii::$app->db->createCommand($metaInsertStr)->query();
//         }
//     }

//     /**
//      * Modify generated plot records
//      * Needs to be refactored
//      * @param $studyId integer database identifier of the study
//      * @param $blkExists boolean
//      * @param $creatorId integer database identifier of the user
//      */
//     public static function modifyPlotnos($studyId, $blkExists, $creatorId = null){

//         $plots = Yii::$app->db->createCommand("select * from operational.temp_plot where study_id = '$studyId' and is_void = false order by plotno asc")->queryAll();

//         $plotStart = 1;
//         $plotCodePatArr = range($plotStart, ($plotStart + count($plots)-1));

//         $iterator = 0;
//         $blkVar = 750;
//         $entries = array();
//         foreach ($plots as $plot){
//             $plotNo = $plotCodePatArr[$iterator++];
//             $plotId = $plot['id'];

//             Yii::$app->db->createCommand("update operational.temp_plot set plotno = '$plotNo' where id = '$plotId'")->execute();

//             if($blkExists){
//                 $valueBlk = $plot['rep'];
//                 $plotEntryId = $plot['entry_id'];
//                 $id = $plot['id'];

//                 //Update block value
//                 $blkSql = " 
//                     UPDATE 
//                     operational.temp_plot set block_no = '$valueBlk'
//                     where
//                     study_id = '$studyId'
//                     and entry_id = '$plotEntryId'
//                     and id = '$id';";
//                 Yii::$app->db->createCommand($blkSql)->execute();

//                 //Change rep value for the plot
//                 $entries[$plot['entry_id']] = isset($entries[$plot['entry_id']]) ? ++$entries[$plot['entry_id']] : 1;
//                 $repVal = $entries[$plot['entry_id']];
//                 Yii::$app->db->createCommand("update operational.temp_plot set rep = '$repVal' where id = '$plotId'")->execute();

//             }
//         }
//     }

//     /**
//      * Generate occurrences
//      * 
//      * @param $id integer database identifier of the experiment
//      * @param $occurrencesCount integer number of occurrences to be created
//      */
//     public static function generateOccurrences($id, $occurrenceCount){

//         $createRecords = array();
//         for($i=0;$i<$occurrenceCount; $i++){
//             $data['experimentDbId'] = $id;
//             $data['occurrenceName'] = 'sample ';
//             $data['occurrenceCode'] = 'test';
//             $data['status'] = 'draft';

//             $createRecords[] = $data;
//         }
//         $postData['records'][] = $createRecords;
//         $records = Occurrence::create($postData);

//         $newIds = [];
//         if($records['totalcount'] > 0){
//             $newIds = array_column($records['data'], 'occurrenceDbId');
//         }

//         return $newIds;
//     }
// }
