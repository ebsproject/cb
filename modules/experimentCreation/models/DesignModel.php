<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for Analysis
*/

namespace app\models;
namespace app\modules\experimentCreation\models;
use app\models\Config;
use app\models\Crop;
use app\models\CropProgram;
use app\models\BackgroundJob;
use app\models\Entry;
use app\models\EntryData;
use app\models\EntryList;
use app\models\EntrySearch;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\Occurrence;
use app\models\PlanTemplate;
use app\models\Plot;
use app\models\Program;
use app\models\Scale;
use app\models\User;
use app\models\Variable;

use Yii;
use stdClass;
use DateTime;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\modules\experimentCreation\models\VariableComponent;
use app\modules\experimentCreation\models\ExperimentModel;
class DesignModel
{
    
    /**
     * Model constructor
     */
    public function __construct(
        public Config $config,
        public Crop $crop,
        public CropProgram $cropProgram,
        public BackgroundJob $backgroundJob,
        public Entry $entry,
        public EntryData $entryData,
        public EntryList $entryList,
        public Experiment $experiment,
        public ExperimentData $experimentData,
        public Occurrence $occurrence,
        public PlanTemplate $planTemplate,
        public Plot $plot,
        public Program $program,
        public Scale $scale,
        public User $user,
        public Variable $variable,
        public VariableComponent $variableComponent,
    )
    { }

    /**
     * Prepare info for new UI design
     * 
     * @param integer $id record id of the experiment
     * @param string $program name of the current program
     * @param integer $processId record id of the data process
     * @param string $acrossEnvDesign if design is sparse or not
     */
    public function generateDesignInfo($id, $program, $processId, $acrossEnvDesign = 'random'){

        $model = $this->experiment->getExperiment($id);
        $entryListId = $this->entryList->getEntryListId($id);
        $entryListCheckArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check'], '', true);
        $entryListLocalArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId|entry.entry_role as entryRole','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check', 'entryRole'=>'equals local check'], '', true);

        $entryListSpatialArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType|entry.id as entryDbId|entry.entry_role as entryRole','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals check', 'entryRole'=>'equals spatial check'], '', true);

        $entryListEntryArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId", 'entryType'=>'equals test'], 'limit=1', false);
        $entryListTotalArray = $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.entry_type as entryType','entryListDbId'=>"equals $entryListId"], 'limit=1', false);

        $entryInfo  = array();
        $entryInfo['local'] = $entryListLocalArray['totalCount'];
        $entryInfo['spatial'] = $entryListSpatialArray['totalCount'];
        $entryInfo['check'] = $entryListCheckArray['totalCount'] - $entryListLocalArray['totalCount'];
        $entryInfo['test'] = $entryListEntryArray['totalCount'];
        $entryInfo['total'] = $entryListTotalArray['totalCount'];
        
        //create entry info data provider
        $entryInfoDataProvider = new ArrayDataProvider([
            'key'=>'total',
            'allModels' => [$entryInfo]
        ]);

        //if there's no workflow saved in experiment remarks, default is null
        $workflow = $model['remarks'] ?? null;

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id"], 'sort=planTemplateDbId:ASC');

        //check if there's already a created draft plan template
        $planRecordCreated = isset($experimentJsonArray['data'][0]) ? 'true':'false';
        
        $experimentJson = isset($experimentJsonArray['data'][0]) ? $experimentJsonArray['data'][0]:null;

        $planTemplateDbId = isset($experimentJson['planTemplateDbId']) ? $experimentJson['planTemplateDbId'] : [];

        if(isset($experimentJson['planTemplateDbId']) && $experimentJson['notes']!= null){
            $variableInfo = json_decode($experimentJson['notes'], true);
            
        } else{
            
            // fields for additional metadata for sparse testing
            $variableInfo = [
                [
                    "id" => "1",
                    "code" => "nExptTrial",
                    "name" => "TOTAL_OCCURRENCES",
                    "type" => "input",
                    "dataType" => "integer",
                    "description" => "Total number of occurrences within the experiment",
                    "variableLabel" => "No. of Experiment Occurrences",
                    "orderNumber" => 1,
                    "parentPropertyId" => 9,
                    "layoutVariable" => false,
                    "ui"=> [
                        "id"=> "1",
                        "visible"=> true,
                        "minimum"=> 1,
                        "maximum"=> null,
                        "multiple"=> false,
                        "defaultValue"=> "1",
                        "disabled"=> false,
                        "catalogue"=> false
                    ],
                    "rules"=> []
                ]                
            ];
            
            if($acrossEnvDesign == 'random'){
                $variableInfo[] = [
                    "id"=> "2",
                    "code"=> "noOfIterations",
                    "name"=> "NO_ITERATIONS",
                    "type"=> "input",
                    "dataType"=> "integer",
                    "description"=> "Number of Iterations",
                    "variableLabel"=> "No. of Iterations",
                    "orderNumber"=> 2,
                    "parentPropertyId"=> 9,
                    "layoutVariable"=> false,
                    "ui"=> [
                        "id"=> "2",
                        "visible"=> true,
                        "minimum"=> 1,
                        "maximum"=> null,
                        "multiple"=> false,
                        "defaultValue"=> "1",
                        "disabled"=> false,
                        "catalogue"=> false
                    ],
                    "rules"=> []
                ];
            }
        }

        $addDesignFields = [];
        $paramValues = [];
        $totalOccurrences = 0;
        foreach($variableInfo as $key=>$var){
            if(isset($var['ui']['visible']) && $var['ui']['visible']){
                $designFld = $this->variableComponent->generateDesignField($var, 'additional', $key, true, 'Additional');
                $addDesignFields[] = $designFld;
                $paramValues[$var['code']] = $designFld['defaultValue'];
            }
            if($var['code'] == 'nExptTrial'){
                $totalOccurrences = isset($var['value']) ? (int)$var['value'] : (int)$var['ui']['defaultValue'];
            }
        }

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'not equals incomplete'], 'sort=planTemplateDbId:ASC');
        
        //create parameter sets table
        $parameterSets = [];
        $columns = [];
        $firstRec = false; 
        $setNo = 1;
        $assignedOccurrences = 0;
        $planStatus = 'draft';
        foreach($experimentJsonArray['data'] as $record){
            $tempRow = [];
            $jsonInput = json_decode($record['mandatoryInfo'],true);
            $planStatus = $record['status'];

            $tempRow['status'] = $record['status'];
            $tempRow['remarks'] = $record['remarks'];
            $tempRow['planTemplateDbId'] = $record['planTemplateDbId'];
            $tempRow['design'] = $record['design'];
            $tempRow['setNumber'] = $setNo++;
            $tempRow['addtlData'] = $jsonInput;
            if(!$firstRec){
                
                $columns[] = [
                    'attribute'=>'setNumber',
                    'label' => '<span title="Parameter sets">'.Yii::t('app', 'Set').'</span>',
                    'format' => 'raw',
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
                $columns[] = [
                    'class'=>'kartik\grid\ActionColumn',
                    'header' => false,
                    'noWrap' => true,
                    'template' => '{delete}{view}{update}{error}',
                    'buttons' => [
                        'delete' => function ($url,$data) {
                            if($data['status'] != 'randomization in progress' && $data['status'] != 'randomized' && $data['status'] != 'uploaded'){
                                return Html::a('<i class="material-icons">delete</i>',
                                    '#',
                                    [
                                        'class'=>'delete-param-set',
                                        'title' => Yii::t('app','Delete parameter set'),
                                        'data-plan-template-id' => $data['planTemplateDbId'],
                                    ]
                                );
                            }
                        },
                        'view' => function ($url,$data) use ($id, $program, $processId)  {
                            $withLayout = false;
                            if($data['status'] == 'uploaded'){
                                $occurrences = $data['addtlData']['design_ui']['occurrenceDbIds'] ?? [];
                               
                                if(!empty($occurrences)){
                                    $plot = $this->plot->searchAll(["occurrenceDbId"=>"equals ".$occurrences[0]], "limit=1", false);
                                    
                                    if(isset($plot['data'][0]['designX']) && $plot['data'][0]['designX'] != NULL){
                                        $withLayout = true;
                                    }
                                }
                            }
                            if(($data['status'] == 'randomized' && isset($data['genLayout']) && $data['genLayout'] == "true") || ($data['status'] == 'uploaded' && $withLayout)){
                                return Html::a('<i class="material-icons">remove_red_eye</i>',
                                    Url::toRoute(['design/view-layout',
                                                  'id'=>$id, 'program'=>$program, 'processId'=>$processId, 'planTemplateDbId'=> $data['planTemplateDbId']]),
                                    [
                                        'class'=>'view-param-set',
                                        'title' => Yii::t('app','View parameter set layout')
                                    ]
                                );
                            }
                        },
                        'update' => function ($url,$data) use ($id, $program, $processId)  {
                            if($data['status'] == 'draft' || $data['status'] == 'randomization failed'){
                                return  Html::a('<i class="material-icons">edit</i>',
                                    '#',
                                    [
                                        'class'=>'update-param-set',
                                        'title' => Yii::t('app','Update parameter set'),
                                        'data-plan-template-id' => $data['planTemplateDbId'],
                                    ]
                                );
                            }
                        },
                        'error' => function ($url,$data) use ($id, $program, $processId)  {
                            if($data['status'] == 'randomization failed'){
                                return  Html::a('<i class="material-icons">error</i>',
                                    '#',
                                    [
                                        'class'=>'tooltipped',
                                        'data-position'=>'top',
                                        'data-tooltip'=>$data['remarks']
                                    ]
                                );
                            }
                        },
                    ]
                ];
                $columns[] = [
                    'attribute'=>'status',
                    'label' => '<span title="Parameter set status">'.Yii::t('app', 'Status').'</span>',
                    'format' => 'raw',
                    'value' => function($data){
                        $status = '';
                        if($data['status'] == 'randomization in progress' || $data['status'] == 'deletion in progress'){
                            $status = '<span class="new badge center amber darken-2">'.strtoupper($data['status']).'</span>';
                        } else if ($data['status'] == 'randomized'){
                            $status = '<span class="new badge blue darken-2">'.strtoupper($data['status']).'</span>';
                        } else if ($data['status'] == 'randomization failed'){
                            $status = '<span class="new badge red darken-2">'.strtoupper($data['status']).'</span>';
                        } 
                        else {
                            $status = '<span class="new badge grey">'.strtoupper($data['status']).'</span>';
                        }
                        return $status;
                    },
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
                $columns[] = [
                    'attribute'=>'design',
                    'label' => '<span title="Parameter set design">'.Yii::t('app', 'Design').'</span>',
                    'format' => 'raw',
                    'enableSorting' => false,
                    'encodeLabel'=>false,
                ];
            }

            $strLimit = 20;
            if(isset($jsonInput['design_ui']['randomization'])){
                foreach($jsonInput['design_ui']['randomization'] as $ui){
                    if($ui['ui']['visible']){
                        $tempRow[$ui['code']] = isset($ui['value']) ? $ui['value'] : null;
                        if(!$firstRec){
                            $code =  (strlen($ui['variableLabel']) > $strLimit ) ? substr($ui['variableLabel'],0,$strLimit ).'...' : $ui['variableLabel'];
                            $columns[] = [
                                'attribute'=>$ui['code'],
                                'label' =>'<span title="'.$ui['variableLabel'].'">'.Yii::t('app', $code).'</span>',
                                'format' => 'raw',
                                'enableSorting' => false,
                                'encodeLabel'=>false,
                            ];
                        }

                        if($ui['code'] == 'nTrial'){
                            $assignedOccurrences += isset($ui['value']) ? $ui['value'] : null;
                        }
                    }
                }
            }

            if(isset($jsonInput['design_ui']['layout'])){
                foreach($jsonInput['design_ui']['layout'] as $ui){
                    if($ui['ui']['visible']){
                        $tempRow[$ui['code']] = isset($ui['value']) ? $ui['value'] : null;
                        if(!$firstRec){
                            $code =  (strlen($ui['variableLabel']) > $strLimit ) ? substr($ui['variableLabel'],0,$strLimit ).'...' : $ui['variableLabel'];
                            $columns[] = [
                                'attribute'=>$ui['code'],
                                'label' => '<span title="'.$ui['variableLabel'].'">'.Yii::t('app', $code).'</span>',
                                'format' => 'raw',
                                'enableSorting' => false,
                                'encodeLabel'=>false,
                            ];
                        }
                    }
                }
            }

            if(isset($jsonInput['design_ui']['rules'])){
                $rules = $jsonInput['design_ui']['rules'];
            }

            if(isset($jsonInput['design_ui']['occurrenceDbIds']) && $workflow == 'upload'){
                $tempRow['occurrenceCount'] = count($jsonInput['design_ui']['occurrenceDbIds']);
                if(!$firstRec){
                    $columns[] = [
                        'attribute'=>'occurrenceCount',
                        'label' => '<span title="No. of Occurrences">'.Yii::t('app', 'No. of Occurrences').'</span>',
                        'format' => 'raw',
                        'enableSorting' => false,
                        'encodeLabel'=>false,
                    ];
                }
            }
            $parameterSets[] = $tempRow;
            $firstRec = true;
        }

        //create entry info data provider
        $parameterSetsProvider = new ArrayDataProvider([
          'key'=>'planTemplateDbId',
          'allModels' => $parameterSets
        ]);

        $model = $this->experiment->getExperiment($id);
        $experimentStatus = $model['experimentStatus'];

        return [
            'entryInfo' => json_encode($entryInfo),
            'entryInfoDataProvider' => $entryInfoDataProvider,
            'model' => $model,
            'program' => $program,
            'id' => $id,
            'processId' => $processId,
            'acrossEnvDesign' => $acrossEnvDesign,
            'addDesignFields'=>$addDesignFields,
            'paramValues'=> $paramValues,
            'parameterSetsProvider' => $parameterSetsProvider,
            'columns' => $columns,
            'assignedOccurrences' => $assignedOccurrences,
            'totalOccurrences' => $totalOccurrences,
            'planStatus' => $planStatus,
            'expStatus' => $experimentStatus,
            'workflow' => $workflow,
            'planRecordCreated' => $planRecordCreated
        ];
    }

    /**
     * 
     * Create entry data records for no. of replications
     * 
     * @param integer $id record id of the experiment
     * @param boolean $initial first population of the entry list
     * @param integer $currentCount current count of entry data records
     * @param array $newEntryIds list of ids for newly created entries
     *
     * */
    public function populateEntryData($id, $initial = true, $currentCount = 0, $newEntryIds=null){

        $limit = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'updateEntries') : 500;
        $useWorker = false;
        $designRecord = $this->variable->getVariableByAbbrev('TIMES_REP');
        $variableDbId = isset($designRecord['variableDbId']) ? $designRecord['variableDbId'] : 0;
        $entryListId = $this->entryList->getEntryListId($id);
        $varValue = 1; //default value
        $experiment = $this->experiment->getExperiment($id);
        $program = isset($experiment['programCode']) ? $experiment['programCode'] : '';

        if($initial){
            
            $entryRecords = $this->entry->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.entry_number as entryNumber",
                "entryListDbId"=>"$entryListId"
            ], 'sort=entryNumber:DESC&limit=1', false);

            $recordsToCreate = $entryRecords['totalCount'] - $currentCount;
            $useWorker = ($recordsToCreate > $limit) ? true : false;

            if(!$useWorker){
                $entries = $this->experiment->searchAllEntries($id)['data'];
            }
        } else if($newEntryIds == null){

            //get all entryDbIds
            $entryRecords = $this->entry->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.id as entryDbId",
                "entryListDbId"=>"equals $entryListId"
            ], null, true);

            //get all entryDbIds from entryData
            $params = [
                "fields" => "variable.abbrev AS variableAbbrev | entryData.entry_id as entryDbId",
                "variableAbbrev" => "equals TIMES_REP"
            ];
            $entryDataRecords = $this->experiment->searchAllEntryData($id,$params, null,true);
            
            $entries = [];

            $entryDbIds = array_column($entryRecords['data'], 'entryDbId');
            $entryDbIdsData = array_column($entryDataRecords['data'], 'entryDbId');
            $diff = $diff2 = [];
            
            $diff = array_diff($entryDbIds, $entryDbIdsData);
            $entries = [];

            foreach($diff as $entryId){
                $entries[] = ["entryDbId"=>$entryId];
            }
        }  
        else {
            $useWorker = (count($newEntryIds) > $limit) ? true : false;
            $entries = $newEntryIds['data'];
        }

        if(!$useWorker && count($entries) > 0){
            foreach($entries as $entry){
                // insert new entry data
                $insertList[] = [
                    "entryDbId" => $entry['entryDbId']."",
                    "variableDbId" => "$variableDbId",
                    "dataValue" => "$varValue",
                ];
            }
            $insertedList = $this->entryData->create(['records'=> $insertList]); 

        } else if($useWorker && count($entries) > 0){

            $userModel = new User();
            $userId = $userModel->getUserId();

            $backgroundJobParams = [
                "workerName" => "CreateEntryRecords",
                "entity" => "ENTRY_DATA",
                "entityDbId" => $id,
                "endpointEntity" => "ENTRY_DATA",
                "application" => "EXPERIMENT_CREATION",
                "method" => "POST",
                "jobStatus" => "IN_QUEUE",
                "startTime" => "NOW()",
                "creatorDbId" => $userId
            ];
            
            $redirectUrl = ['experimentCreation/create/index?program='.$program.'&id='.$id];

            //create background transactions
            try {
                $transaction = $this->backgroundJob->create(["records" => [$backgroundJobParams]]);
            } catch (\Exception $e) {

                $notif = 'error';
                $icon = 'times';
                $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);         
            }

            // if creating transaction is successful
            try {
                if (isset($transaction['status']) && $transaction['status'] == 200) {

                    $host = getenv('CB_RABBITMQ_HOST');
                    $port = getenv('CB_RABBITMQ_PORT');
                    $user = getenv('CB_RABBITMQ_USER');
                    $password = getenv('CB_RABBITMQ_PASSWORD');

                    $connection = new AMQPStreamConnection($host, $port, $user, $password);
        
                    $channel = $connection->channel();
        
                    $channel->queue_declare(
                        'CreateEntryRecords', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                        false,  //passive - can use this to check whether an exchange exists without modifying the server state
                        true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                        false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                        false   //auto delete - queue is deleted when last consumer unsubscribes
                    );

                    $msg = new AMQPMessage(
                        json_encode([
                            'tokenObject' => [
                                'token' => Yii::$app->session->get('user.b4r_api_v3_token'),
                                'refreshToken' => ''
                            ],
                            'requestData' => [
                                'experimentDbId' => $id,
                                'processName' => 'create-entry-data-records',
                                'defaultValues'=>[
                                    'entryListDbId'=> $entryListId,
                                    'variableDbId' => $variableDbId,
                                    'variableValue' => $varValue
                                ]
                            ],
                            'backgroundJobDbId' => $transaction["data"][0]["backgroundJobDbId"],
                            'description' => 'Creation of entry data records',
                            'personDbId' => $userId
                        ]),
                        array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
                    );
        
                    $channel->basic_publish(
                        $msg,   //message 
                        '', //exchange
                        'CreateEntryRecords'  //routing key (queue)
                    );
                    $channel->close();
                    $connection->close();
                    
                    $bgStatus = $this->experiment->formatBackgroundWorkerStatus($transaction['data'][0]['backgroundJobDbId']);

                    if($bgStatus !== 'done' && !empty($bgStatus)){
                        $experiment = $this->experiment->getExperiment($id);
                        $experimentStatus = isset($experiment['experimentStatus']) ? $experiment['experimentStatus'] : '';

                        if(!empty($experimentStatus)){
                            $status = explode(';',$experimentStatus);
                            $status = array_filter($status);
                        }else{
                            $status = [];
                        }

                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                        $this->experiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);

                        //update experiment
                        Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                        Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$id]);
                    } else {
                        $notif = 'error';
                        $icon = 'times';
                        $message = 'There was a problem in background processing. You may file a report using the "Send feedback" form. Thank you.';
                        Yii::$app->session->setFlash($notif, '<i class="fa fa-' . $icon . '"></i> ' . $message);
                    }
                }
            } catch (\Exception $e) {
                \ChromePhp::log('e:', $e);
            }

        }
    }
    
    /**
     * Get json parameters for design
     * 
     * @param integer $designId
     * @param boolean $sparse
     */
    public function getDesignFields($designId, $isSparse){
        $jsonConfig = NULL;

        $analysisModel = new AnalysisModel();
        $designParams = $analysisModel->getDesignParameters($designId);

        // if($isSparse){
        //     $sparseUi = $analysisModel->getDesignParameters(14); //sparse design additional 
        //     $designParams['result']['data'][0]['scriptInputs'] = array_merge($sparseUi['result']['data'][0]['scriptInputs'], $designParams['result']['data'][0]['scriptInputs']);
        // }

        return $designParams;
    }


    /**
     * Get design parameters
     * 
     * @param integer $designId id of the selected design
     * @param string $type type of parameters to be returned
     * @param array $designParams
     * @param boolean $isSparse
     */
    public function getDesignParametersOld($designId, $type, $designParams, $isSparse){
        $jsonConfig = NULL;

        if(isset($designParams['result']['data'][0]['script'][0]['scriptInputs'])){
            $properties = $designParams['result']['data'][0]['script'][0]['scriptInputs'];
            $designCode = $designParams['result']['data'][0]['script'][0]['code'];
            $propertiesArray = array();

            $randomization = array();
            $layout = array();
            $rules = array();

            if($type == 'ui'){

                foreach($properties as $params){

                    if(!in_array($params['variable']['code'], ['outputFile', 'outputPath'])){
                        // if(($params['isSparse'] && $isSparse) || !$params['isSparse']){
                        if(($isSparse && $params['method'] == 'sparse') || (!$isSparse && $params['method'] == 'non-sparse')){
                            if($params['isLayout']){
                                $layout[] = $params;
                            } else {

                                if($params['variable']['code'] == 'nExptTrial'){
                                    $params['isHidden'] = true;
                                }

                                if($isSparse && ($params['variable']['code'] == 'repTest' || $params['variable']['code'] == 'repCheck')){
                                    $params['scriptInputRules'] = null;
                                }

                                $randomization[] = $params;

                            }

                            if($params['variable']['code'] == 'maxPlot'){
                                $params['default'] = null;
                            }

                            //get rules per parameters
                            if($params['scriptInputRules'] != null){
                                $ruleOrder = 1;

                                usort($params['scriptInputRules'],function($first,$second){
                                    return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                                });
                                
                                foreach($params['scriptInputRules'] as $rule){

                                    $tempRule['expression'] = $rule['expression'];
                                    $tempRule['notification'] = $rule['message'];
                                    $tempRule['action'] = $rule['severity'];
                                    $tempRule['orderNumber'] = $ruleOrder++;

                                    $rules[$rule['type']][$params['variable']['code']][] = $tempRule;
                                }
                            }
                        }
                    }
                }

                usort($randomization,function($first,$second){
                    return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                });

                usort($layout,function($first,$second){
                    return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                });


                $jsonConfig = [
                    'randomization' => $randomization,
                    'layout' => $layout,
                    'rules' => $rules,
                    'designCode' =>$designCode
                ];

            } else if($type = 'design-info'){

            } else {
                $jsonConfig = NULL;
            }
        }
        
        return $jsonConfig;

    }

    /**
     * Get design parameters via design-scripts call
     * 
     * @param integer $designId id of the selected design
     * @param string $type type of parameters to be returned
     * @param array $designParams
     * @param boolean $isSparse
     */
    public function getDesignParameters($designId, $type, $designParams, $isSparse){
        $jsonConfig = NULL;

        if(isset($designParams['result']['data'][0]['scriptInputs'])){
            $properties = $designParams['result']['data'][0]['scriptInputs'];
            $designCode = $designParams['result']['data'][0]['code'];
            $propertiesArray = array();

            $randomization = array();
            $layout = array();
            $rules = array();

            if($type == 'ui'){

                foreach($properties as $params){

                    if(!in_array($params['variable']['code'], ['outputFile', 'outputPath'])){

                        if(($isSparse && ($params['method'] == 'sparse' || $params['method'] == 'sparse,non-sparse')) || (!$isSparse && ($params['method'] == 'non-sparse' || $params['method'] == 'sparse,non-sparse'))){
                            if($params['isLayout']){
                                $layout[] = $params;
                               
                                if($designParams['result']['data'][0]['design']['designName'] == "Partially Replicated" && $params['variable']['code'] == 'nFieldRow'){
                                    $params['scriptInputRules'][0]['expression'] = 'factor(totalPlots)';
                                }
                            } else {
                                if($params['variable']['code'] == 'nExptTrial'){
                                    $params['isHidden'] = true;
                                }

                                if($designParams['result']['data'][0]['design']['designName'] == "Partially Replicated" && $params['variable']['code'] == 'maxPlot'){
                                    $params['scriptInputRules'][0]['expression'] = 'value >= totalPlots';
                                }
                                
                                // formula format and computation should be improved
                                if($isSparse && $designParams['result']['data'][0]['design']['designName'] == "Partially Replicated" && ($params['variable']['code'] == 'repTest' || $params['variable']['code'] == 'repCheck')){
                                    $params['scriptInputRules'] = null;
                                }

                                if(!$isSparse && $designParams['result']['data'][0]['design']['designName'] == "Partially Replicated" && $params['variable']['code'] == 'repLocalCheck'){
                                    $params['scriptInputRules'][0]['expression'] = 'value >= 2';
                                }

                                $randomization[] = $params;
                            }

                            if($params['variable']['code'] == 'maxPlot'){
                                $params['default'] = null;
                            }

                            //get rules per parameters
                            if($params['scriptInputRules'] != null){

                                $ruleOrder = 1;
                                //order values
                                $rulesArray = $params['scriptInputRules'];
                                usort($rulesArray, function ($first, $second) {
                                    return $first['scriptInputRuleDbId'] > $second['scriptInputRuleDbId'] ? 1 : -1;
                                });
                                
                                foreach($rulesArray as $rule){

                                    if($designParams['result']['data'][0]['design']['designName'] == "Partially Replicated"){
                                        if(str_contains($rule['expression'], "'")) {
                                            $tempRule['expression'] = str_replace("'","",$rule['expression']);
                                        } else $tempRule['expression'] = $rule['expression'];
                                    } else {
                                        $tempRule['expression'] = $rule['expression'];
                                    }

                                    $tempRule['notification'] = $rule['message'];
                                    $tempRule['action'] = $rule['severity'];
                                    $tempRule['orderNumber'] = $ruleOrder++;
                                    
                                    $rules[$rule['type']][$params['variable']['code']][] = $tempRule;
                                }
                            }
                        }
                    }
                }

                usort($randomization,function($first,$second){
                    return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                });

                usort($layout,function($first,$second){
                    return ($first['orderNumber'] > $second['orderNumber'])? 1 : -1;
                });


                $jsonConfig = [
                    'randomization' => $randomization,
                    'layout' => $layout,
                    'rules' => $rules,
                    'designCode' =>$designCode
                ];

            } else if($type = 'design-info'){

            } else {
                $jsonConfig = NULL;
            }
        }

        return $jsonConfig;

    }

    /**
     * Save additional details for sparse testing
     * 
     * @param Array $valuesArray contains the additional info to be saved
     * @param Integer $id record id of the experiment
     * @param String $acrossEnvDesign if design is sparse or not
     * 
     * */
    public function saveAdditionalInfo($valuesArray, $id, $acrossEnvDesign='random'){

        $variableInfo = [
            [
                "id" => "1",
                "code" => "nExptTrial",
                "name" => "TOTAL_OCCURRENCES",
                "type" => "input",
                "dataType" => "integer",
                "description" => "Total number of occurrences within the experiment",
                "variableLabel" => "No. of Experiment Occurrences",
                "orderNumber" => 1,
                "parentPropertyId" => 9,
                "layoutVariable" => false,
                "ui"=> [
                    "id"=> "1",
                    "visible"=> true,
                    "minimum"=> 1,
                    "maximum"=> null,
                    "multiple"=> false,
                    "defaultValue"=> "1",
                    "disabled"=> false,
                    "catalogue"=> false
                ],
                "rules"=> []
            ]
        ];

        if($acrossEnvDesign == "random"){
            $variableInfo[] = [
                "id"=> "2",
                "code"=> "noOfIterations",
                "name"=> "NO_ITERATIONS",
                "type"=> "input",
                "dataType"=> "integer",
                "description"=> "Number of Iterations",
                "variableLabel"=> "No. of Iterations",
                "orderNumber"=> 2,
                "parentPropertyId"=> 9,
                "layoutVariable"=> false,
                "ui"=> [
                    "id"=> "2",
                    "visible"=> true,
                    "minimum"=> 1,
                    "maximum"=> null,
                    "multiple"=> false,
                    "defaultValue"=> "1",
                    "disabled"=> false,
                    "catalogue"=> false
                ],
                "rules"=> []
            ];
        }
        
        $indexRandIds = array_column($variableInfo, 'code');

        if(!is_array($valuesArray['Additional'])){
            $arrayValues = json_decode($valuesArray['Additional'], true);
        } else {
            $arrayValues = $valuesArray['Additional'];
        }
        
        foreach($arrayValues as $key=>$value){
            $indexRand = array_search($key,$indexRandIds);

            if($indexRand !== false && isset($variableInfo[$indexRand])){
                $variableInfo[$indexRand]['value'] = $value;
            }
        }

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status' => 'incomplete']);

        if($experimentJsonArray['totalCount'] == 0){
            $model = $this->experiment->getExperiment($id);
            
            //create initial plan template record
            $experimentJson = [];
            $experimentJson['templateName'] = str_replace('#','_',(str_replace(' ', '_',$model['experimentCode']))).'_'.$model['experimentDbId'];
            $experimentJson['entityType'] = "parameter_sets";
            $experimentJson['entityDbId'] = $id;
            $experimentJson['programDbId'] = $model['programDbId']."";
            $experimentJson['status'] = "incomplete";
            $plantTemplateDbId = $this->planTemplate->createPlanTemplate($experimentJson);
        }

        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id"]);
        $experimentJson = $experimentJsonArray['data'];
        $plantTemplateDbIds = array_column($experimentJson,'planTemplateDbId');

        //save in notes
        // Update experiment design
        $experimentJsonTemp['notes'] = json_encode($variableInfo);
        $test = $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);

    }

    /**
     * Get layout per parameter set
     * 
     * @param integer $id record id of the experiment
     * @param integer $planTemplateDbId record id of the plan template
     * @param string $label attributes to be shown in layout
     * @param integer $occurrenceDbId record id of the experiment
     * 
     * */
    public function getLayoutParamSet($id, $planTemplateDbId, $label, $occurrenceDbId=NULL){

        $experimentJsonArray =  $this->planTemplate->getPlanTemplateById($planTemplateDbId);
        
        $jsonArray = json_decode($experimentJsonArray['mandatoryInfo'], true);
        
        $inputFile = $experimentJsonArray['randomizationInputFile'];

        $occurrenceIds = $jsonArray['design_ui']['occurrenceDbIds'];
        $occurrences = $this->occurrence->searchAll([
            'fields'=>'occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName|occurrence.id AS occurrenceDbId',
            'occurrenceDbId' => "equals ".implode("|equals ", $occurrenceIds)
        ],'sort=occurrenceName:ASC');
        
        $occurrencesList = array();
        $layoutData = [];
        if($occurrences['totalCount'] > 0){

            $occurrencesList = $occurrences['data'];
            if($occurrenceDbId == NULL){
                $occurrenceDbId = $occurrences['data'][0]['occurrenceDbId'];
                $occurrenceName = $occurrences['data'][0]['occurrenceName'];
            } else {
                $occurrenceIdx = array_search($occurrenceDbId, array_column($occurrencesList, 'occurrenceDbId'));
                $occurrenceName = $occurrencesList[$occurrenceIdx]['occurrenceName'];
            }

            $occurrenceNameArr = array_column($occurrencesList, 'occurrenceName');
            $occurrenceIdArr = array_column($occurrencesList, 'occurrenceDbId');
            $occurrenceArray = array_combine($occurrenceIdArr, $occurrenceNameArr);
            //checked plot records and view layout
            $plotRecords = $this->occurrence->searchAllPlots($occurrenceDbId, null,'sort=plotNumber:ASC');

            if($plotRecords['totalCount'] > 0){

                //Save FIRST_PLOT_POSITION_VIEW
                $varAbbrev = 'FIRST_PLOT_POSITION_VIEW';
                $variableInfo = $this->variable->searchAll(["abbrev"=>"equals $varAbbrev"]);
                $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
                
                $expData = $this->experimentData->getExperimentData($id, ["abbrev" => "equals "."$varAbbrev"]);
                
                if(isset($expData) && !empty($expData)){
                    if($expData[0]['dataValue'] == "Top Left"){
                        $flipped = true;
                    } else {
                        $flipped = false;
                    }   
                } else {   //create variable

                    // if no saved in experiment data, get default first plot position in config
                    $config = $this->config->getConfigByAbbrev('FIRST_PLOT_POSITION_VIEW_DEFAULT');

                    $varValue = (isset($config['value']) && !empty($config['value'])) ? $config['value'] : 'Top Left';

                    $dataRequestBodyParam[] = [
                        'variableDbId' => "$variableDbId",
                        'dataValue' => "$varValue"
                    ];

                    $this->experiment->createExperimentData($id,$dataRequestBodyParam);
                    $flipped = ($varValue == 'Bottom Left') ? false : true;
                }
                $layoutArray =\Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel')->generateLayout($occurrenceDbId, $id);
                
                $panelWidth = 0;
                $layoutData = [
                    'experimentId' => $id,
                    'data' => $layoutArray['dataArrays'],
                    'label'=> $label,
                    'startingPlotNo' => 1,
                    'panelWidth' => $panelWidth,
                    'noOfRows' => $layoutArray['maxRows'] == NULL ? 0:$layoutArray['maxRows'],
                    'noOfCols' => $layoutArray['maxCols'] == NULL ? 0:$layoutArray['maxCols'],
                    'statusDesign' => 'completed',
                    'deletePlots' => 'true',
                    'planStatus' => 'randomized',
                    'flipped' => $flipped,
                    'occurrencesList' => $occurrencesList,
                    'occurrenceName' => $occurrenceName,
                    'occurrenceDbId' => $occurrenceDbId,
                    'xAxis' => $layoutArray['xAxis'],
                    'yAxis' => $layoutArray['yAxis'],
                    'maxBlk' => $layoutArray['maxBlk'],
                    'design'=> 'Partially Replicated',
                    'occurrenceArray' => $occurrenceArray,
                    'design' => $experimentJsonArray['design']
                ];
                
            } else {
                $layoutData = [
                    'data' => [],
                    'statusDesign' => $experimentJsonArray['status']
                ];
            }
        }
        
        return $layoutData;
    }

    /**
     * Generate request parameters
     * 
     * @param Integer $integer record id of the experiment
     * 
     * */
    public function generateRequestParameters($id){

        $experiment = $this->experiment->getExperiment($id);

        //get all draft parameter sets
        $experimentJsonArray = $this->planTemplate->searchAll(['entityType' => 'equals parameter_sets', 'entityDbId'=>"equals $id", 'status'=>'not equals incomplete'], 'sort=planTemplateDbId:ASC');
 
        $experimentJson = isset($experimentJsonArray['data'][0]) ? $experimentJsonArray['data'][0]:null;
        
        //Get crop and institute
        $cropId = $experiment['cropDbId'];
        $programId = $experiment['programDbId'];

        $program = $this->program->getProgram($programId);
        $cropProgramId = $program['cropProgramDbId'];

        $programCrop = $this->cropProgram->searchAll(['cropProgramDbId'=>$cropProgramId.""]);

        $cropArray = $this->crop->searchAll(['cropDbId'=> $cropId.""]);

        $userId = $this->user->getUserId();
        $requestParam['requestType'] = "generate";
        $requestParam['requesterDbId'] = $userId;
        $requestParam['creatorDbId'] = $userId;
        $requestParam['experimentDbId'] = $id;
        $requestParam['experimentName'] = $experiment['experimentName'] ?? "";
        $requestParam['organizationCode'] = $programCrop['totalCount'] > 0 ? $programCrop['data'][0]['cropProgramCode'] : "NA";
        $requestParam['cropCode'] = $cropArray['data'][0]['cropCode'] ?? null;
        $requestParam['programCode'] = $program['programCode'];
        
        $acrossEnvDesign = $this->getAcrossEnvDesign($id);
        if($acrossEnvDesign == 'random'){
            $requestParam['acrossEnvironmentDesign'] = $acrossEnvDesign;
        }
       
        $occurrenceCount = 0;
        if(isset($experimentJson['planTemplateDbId']) && $experimentJson['notes']!= null){
            $variableInfo = json_decode($experimentJson['notes'], true);

            if($experiment['stageCode'] == 'OFT'){ //use nFarm for no. of occurrences
                $jsonInput = json_decode($experimentJson['mandatoryInfo'],true);

                $indexRand = array_column($jsonInput['design_ui']['randomization'], 'code');
                $indexLayout = array_search('nFarm', $indexRand);

                if(isset($jsonInput['design_ui']['randomization'][$indexLayout]['value'])){
                    $occurrenceCount = $jsonInput['design_ui']['randomization'][$indexLayout]['value'];
                }
            } else {
                $indexNtrial = array_search('nExptTrial',array_column($variableInfo, 'code'));
                $occurrenceCount = $variableInfo[$indexNtrial]['value'];
            }
            
        }

        //creation of occurrence records
        $occurrenceExperiment = $this->occurrence->searchAll(['fields'=>'occurrence.id as occurrenceDbId|occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName','experimentDbId'=>"equals ".$id], 'sort=occurrenceName:DESC&limit=1');
        if($occurrenceExperiment['totalCount'] < $occurrenceCount){

            $newCount = $occurrenceCount - $occurrenceExperiment['totalCount'];
            $totalCount = $occurrenceExperiment['totalCount'];

            for($i=0; $i<$newCount; $i++){
                $occNum = $totalCount + ($i+1);
                $occName = ($occNum < 10) ? '0'.$occNum : $occNum; 
                $occurrenceName = $experiment['experimentName'].'#OCC-'.$occName;//to be replaced with occurrence browser
                $occurrenceStatus = 'draft';
                $records = [
                    'occurrenceStatus' => $occurrenceStatus,
                    'experimentDbId' => "$id"
                ];
                $requestData['records'][] = $records;
            }
            //Debug occurrence creation
            $this->occurrence->create($requestData);

        } else if($occurrenceExperiment['totalCount'] > $occurrenceCount){

            $toBeDeletedCnt = $occurrenceExperiment['totalCount'] - $occurrenceCount;
            $occurrencesIds = array_column($occurrenceExperiment['data'], 'occurrenceDbId');
            $toBeDeleted = array();

            for($i=0; $i<$toBeDeletedCnt; $i++){
                $toBeDeleted[] = $occurrencesIds[$i];
            }

            //delete excess occurrences
            $this->occurrence->deleteMany($toBeDeleted);
        }

        $occurrenceExperiment = $this->occurrence->searchAll(['fields'=>'occurrence.id as occurrenceDbId|occurrence.experiment_id as experimentDbId|occurrence.occurrence_name as occurrenceName','experimentDbId'=>"equals ".$id], 'sort=occurrenceName:ASC&limit=1');
        $occurrencesIds = array_column($occurrenceExperiment['data'], 'occurrenceDbId');

        $requestParam['occurrenceDbId'] = $occurrencesIds;

        $paramSets = [];
        $setNumber =1;
        $occurrenceCount = 0;
        $methodCheck=false;
        $methodTest=false;

        $excludedAttributes = ['methodReplicateCheckEntries','nCheckListSet'];
        foreach($experimentJsonArray['data'] as $planRecord){

            $jsonInput = json_decode($planRecord['mandatoryInfo'],true);

            $indexRand = array_column($jsonInput['design_ui']['randomization'], 'code');
            $indexLayout = array_search('genLayout', $indexRand);

            $jsonMerged = $jsonInput['design_ui']['randomization'];
            if(isset($jsonInput['design_ui']['randomization'][$indexLayout]['value']) && $jsonInput['design_ui']['randomization'][$indexLayout]['value'] == "true"){
                $jsonMerged = array_merge($jsonInput['design_ui']['randomization'], $jsonInput['design_ui']['layout']);
            }

            $tempParams = null;
            $tempParams['set_number'] = $setNumber++;
            $tempParams['designScriptCode'] = isset($jsonInput['design_ui']['designCode']) ? $jsonInput['design_ui']['designCode'] :'randPREPirri'; //needs clarification where to get this value
            $tempParams['designCode'] = isset($jsonInput['design_code']) ? $jsonInput['design_code'] :'Unspecified';
            
            foreach($jsonMerged as $input){
                if(isset($input['value']) && !in_array($input['code'], $excludedAttributes)){
                    if($input['code'] == 'genLayout'){
                        if($input['value'] == "true"){
                            $input['value'] = true;
                        } else {
                            $input['value'] = false;
                        }
                    } 
                    if(is_numeric($input['value'])){
                        $input['value'] = (double) $input['value'];
                    }

                    $tempParams[$input['code']] = $input['value'];
                    
                }
                if($input['code'] == 'methodReplicateCheckEntries'){
                    $methodCheck=true;
                }
                if($input['code'] == 'methodReplicateTestEntries'){
                    $methodTest=true;
                }
            }

            $occurrenceParam = 'nTrial';
            if(!isset($tempParams['nTrial'])){
                $occurrenceParam = 'nFarm';
            }
            for($i = 0; $i < $tempParams[$occurrenceParam];$i++){

                //update design type of occurrence record
                $tempOccId = $occurrencesIds[$occurrenceCount++];
                $update = $this->occurrence->updateOne($tempOccId, ['occurrenceDesignType'=>$tempParams['designCode']]);
                $tempParams['occurrenceDbIds'][] = $tempOccId;
            }

            $jsonInput['design_ui']['occurrenceDbIds'] = $tempParams['occurrenceDbIds'];

            if(isset($jsonInput['design_ui']['repTest']) && $methodTest){
                $tempParams['repTest'] = array_values($jsonInput['design_ui']['repTest']);
            }
            if(isset($jsonInput['design_ui']['repCheck']) && $methodCheck){
                $tempParams['repCheck'] = array_values($jsonInput['design_ui']['repCheck']);
            }
            if(strtolower($tempParams['designCode']) == 'alpha-lattice'){
                $tempParams['nRep'] = $tempParams['repTest'];
            }
            if(strtolower($tempParams['designCode']) == 'augmented rcbd'){
                $tempParams['nRep'] = $tempParams['repCheck'];
            }

            //save occurrences
            $experimentJsonTemp = array();
            $experimentJsonTemp['mandatoryInfo'] = json_encode($jsonInput);
            $this->planTemplate->updateOne($planRecord['planTemplateDbId'], $experimentJsonTemp);

            $paramSets[] = $tempParams;
        }

        $requestParam['parameters']['set'] = $paramSets;

        //create input json array
    
        $entryListId = $this->entryList->getEntryListId($id);
        $totalEntriesRecord = $this->entry->searchAll(['entryListDbId'=>"equals ".$entryListId], 'sort=entryNumber:ASC');

        //Get crop
        $entryList['entryDbId'] = array_column($totalEntriesRecord['data'], 'entryDbId');
        $entryList['entryNumber'] = array_column($totalEntriesRecord['data'], 'entryNumber');
        $entryList['entryType'] = array_column($totalEntriesRecord['data'], 'entryType');
        $entryList['entryRole'] = array_column($totalEntriesRecord['data'], 'entryRole');
        $entryList['entryStatus'] = array_column($totalEntriesRecord['data'], 'entryStatus');
        $entryList['entryName'] = array_column($totalEntriesRecord['data'], 'entryName');
        $entryList['entryClass'] = array_column($totalEntriesRecord['data'], 'entryClass');
        $entryList['germplasmDbId'] = array_column($totalEntriesRecord['data'], 'germplasmDbId');

        //get entry data
        $filter = [
            "variableAbbrev"=>"equals TIMES_REP",
            "fields"=>"entryData.data_value as dataValue|variable.abbrev as variableAbbrev"
        ];
        $entryListData = $this->entryData->getEntryDataByExperiment($id, $filter);
        if(count($entryListData) > 0){
            $entryList['nRep'] = array_column($entryListData, 'dataValue');
        } else {
            $entryList['nRep'] = array_fill(0, count($entryList['entryDbId']), 1);
        }

        $requestParam['entryList'] = $entryList;

        //save to randomizationInputfile
        $experimentJsonTemp = array();
        $experimentJsonTemp['randomizationInputFile'] = json_encode($requestParam, JSON_UNESCAPED_SLASHES);

        // Update experiment design
        $plantTemplateDbIds = array_column($experimentJsonArray['data'], 'planTemplateDbId');
        $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);

        $analysisModel = new AnalysisModel();
        $submitRequest = $analysisModel->submitNewRequest($id, $requestParam);
     
        if($submitRequest['success']){
            $experimentJsonTemp['status'] = 'randomization in progress';
            $experimentJsonTemp['randomizationResults'] = json_encode($submitRequest['requestInfo'], JSON_UNESCAPED_SLASHES);

            //store request id
            $experimentJsonTemp['requestDbId'] = $submitRequest['requestId'];

            // Update experiment design
            $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);
            
            //Update experiment design
            $entryStatus = "entry list created";
            $statusArray = explode(';', $experiment['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $statusArray)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus.';design requested';
            
            $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            $submitRequest['requestInfo'] = "";
        } else {
            $experimentJsonTemp['status'] = 'randomization failed';
            $experimentJsonTemp['randomizationResults'] = '{}';

            // Update experiment design
            $this->planTemplate->updateMany($plantTemplateDbIds, $experimentJsonTemp);
        }
        return $submitRequest;
    } 


    /**
     * Get design response
     * 
     * @param integer $id record id of the experiment
     * @param string $type status of the request
     * @param array $experimentJson info list of the transaction
     * @param array $experiment record of the experiment
     * @param array $plantTemplateDbIds list of record id of the plan template
     * */
    public function getDesignResponse($id, $type, $experimentJson, $experiment, $plantTemplateDbIds){

        if($type == 'in progress'){
            $statusMessage = 'Randomization in progress';
            if(strpos($experimentJson['status'], 'upload') !== false){
                $statusMessage = 'Upload in progress';
            }
            //check status
            $entryStatus = "entry list created";
            $statusArray = explode(';', $experiment['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $statusArray)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus.';design requested';

            return [
                'expStatus' => $experimentStatus,
                'success'=> 'in progress',
                'planStatus' => $experimentJson['status'],
                'deletePlots' => 'true',
                'message' => $statusMessage
            ];
        } else if($type == 'finished'){
            //check status
            $entryStatus = "entry list created";
            $statusArray = explode(';', $experiment['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $statusArray)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus.';design generated';

            $statusStr = 'randomized';

            if($experimentJson['status'] !== 'randomized' || $experimentJson['status'] !== 'uploaded'){
                //check if randomization or upload
                
                if(strpos($experimentJson['status'], 'upload') !== false){
                    $statusStr = 'uploaded';
                }
                 
                $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>$statusStr]);
                
                $status = explode(';',$experiment['experimentStatus']);
    
                //Update experiment design
                //check status
                $entryStatus = "entry list created";
                $statusArray = explode(';', $experiment['experimentStatus']);
                if(in_array("entry list created", $statusArray)){
                    $entryStatus = "entry list created";
                } else if(in_array("entry list incomplete seed sources", $statusArray)){
                    $entryStatus = "entry list incomplete seed sources";
                }
                $experimentStatus = $entryStatus.';design generated';

                //Update experiment status
                $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            }
            
            return [
                'expStatus' => $experimentStatus,
                'success'=> 'success',
                'deletePlots' => 'true',
                'planStatus' => $statusStr,
                'message' => 'Successfully '.$statusStr.'.'
            ];
        } else if($type == 'failed'){
            $statusStr = 'randomization failed';
            $statusMessage = 'Randomization Failed. Please try again.';
            if(strpos($experimentJson['status'], 'upload') !== false){
                $statusStr = 'upload failed';
                $statusMessage = 'Upload Failed. Please try again.';
            }
            
            $this->planTemplate->updateMany($plantTemplateDbIds, ['status'=>$statusStr]);
                
            $status = explode(';',$experiment['experimentStatus']);
    
            //check status
            $entryStatus = "entry list created";
            $statusArray = explode(';', $experiment['experimentStatus']);
            if(in_array("entry list created", $statusArray)){
                $entryStatus = "entry list created";
            } else if(in_array("entry list incomplete seed sources", $statusArray)){
                $entryStatus = "entry list incomplete seed sources";
            }
            $experimentStatus = $entryStatus;
                    
            //Update experiment status
            $this->experiment->updateOne($id, ['experimentStatus'=>$experimentStatus]);
            return [
                'expStatus' => $experimentStatus,
                'success'=> 'failed',
                'planStatus' => $statusStr,
                'deletePlots' => 'false',
                'message' => $statusMessage
            ];
        }
    }   

    /**
     * 
     * Get old design information from AF
     * @param string $experimentDesign design of the experiment record
     * 
     * */
    public function getOldDesignInformation($experimentDesign){
        //get old design information from AF framework
        $analysisModel = new AnalysisModel();
        $designSG = $analysisModel->getDesignCatalogue();
        $designId = 0;

        if(!isset($designSG['data']['designModel']['children'])){
            Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> The design catalogue cannot be accessed. Please try to refresh the page.');
            return $designId;
        } else {
           
            $designList = $designSG['data']['designModel']['children'];
           
            foreach($designList as $design){
                
                if($design['variableLabel'] == $experimentDesign){
                    return $design['id'];
                }
            }            
        }
    }


    /**
     * Get across env design value for experiment
     * @param integer $id record id of the experiment
     * 
     * */
    public function getAcrossEnvDesign($id){
        $designRecord = $this->variable->getVariableByAbbrev('ACROSS_ENV_DESIGN');
        $variableDbId = $designRecord['variableDbId'];

        $acrossEnvDesign = $this->experimentData->getExperimentData($id,[
            'variableDbId' => 'equals '.$variableDbId
        ]);

        return isset($acrossEnvDesign[0]['dataValue']) ? $acrossEnvDesign[0]['dataValue'] : "none";
        
    }

    /**
     * Get entry list description for design interface placeholder
     * @param integer $id record id of the experiment
     * 
     * */
    public function getDesignUiInfo($id){
        $entryListId = $this->entryList->getEntryListId($id);
        $entryList = $this->entryList->searchAll(['experimentDbId' => "equals $id"])['data'][0];

        if(empty($entryList['description'])){
            $this->entryList->updateOne($entryListId, ['description'=>'old']);
        }
        
        return $entryList['description']; 
    }   
}