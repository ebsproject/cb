<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Cross Pattern Controller
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Config;
use app\models\Cross;
use app\models\CrossAttribute;
use app\models\CrossParent;
use app\models\Entry;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\ExperimentBlock;
use app\models\Variable;

use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;

use app\modules\experimentCreation\models\PlantingArrangement;
use Yii;

class CrossPatternModel
{
    /**
     * Model constructor
     */
    public function __construct(
        public Config $config, 
        public Cross $cross,
        public CrossAttribute $crossAttribute,
        public CrossParent $crossParent,
        public Entry $entry,
        public EntryList $entryList,
        public Experiment $experiment,
        public ExperimentData $experimentData,
        public ExperimentBlock $experimentBlock,
        public PlantingArrangement $plantingArrangement,
        public Variable $variable)
    { }

    /**
     * Search model for plots generated from cross pattern in block
     *
     * @param Integer $blockId experiment block identifier
     */
    public function getPlotsFromCrossPatternInBlock($blockId) {
        $data = [];
        $blockInformation = \Yii::$container->get('app\models\ExperimentBlock')->searchAll([
            'fields' => 'experimentBlock.id as experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
            'experimentBlockDbId' => "equals $blockId"
        ]);

        if ($blockInformation['totalCount'] > 0) {
            $data = $blockInformation['data'][0]['layoutOrderData'];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'totalCount' => count($data),
            'id' => 'ec-pa-entry-order-cross-pattern-plots-grid',
            'sort' => [
                'attributes' => [
                    'plotno'
                ],
                'defaultOrder' => [
                    'plotno' => SORT_ASC
                ],
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Save the default arrangement for entry order
     * 
     * @param Integer $id experiment identifier
     * @param Array $newCrossRecords array list of crosses
     * @param String $crossPattern cross pattern value
     * @param Array $insertCrossList array list of inserted crosses
     * @param Array $currentAttributes array list of cross attribute records
     * 
     */
    public function saveCrossPatternRecords($id, $newCrossRecords, $crossPattern, $insertCrossList, $currentAttributes){
        // check first if this is the first time saving a cross pattern, if yes delete experiment block if systematic arrangement
        $params = ["fields"=>"experimentBlock.id as experimentBlockDbId|experimentBlock.entry_list_id_list as entryListIdList|experimentBlock.is_active AS isActive|experiment.id AS experimentDbId|experimentBlock.no_of_ranges as noOfRange|experimentBlock.notes|experimentBlock.experiment_block_name as experimentBlockName",
            "experimentDbId" => "equals $id"];
        $experimentBlockData = $this->experimentBlock->searchAll($params);

        if($experimentBlockData['totalCount'] == 0){ // create entry order row
            $this->plantingArrangement->addNewRows($id, 1, 'cross pattern');
        } else {
            $deleteIds = [];
            $crossPatternBlock = [];
            foreach($experimentBlockData['data'] as $blockData){
                if($blockData['notes'] != 'cross pattern'){
                    $deleteIds[] = $blockData['experimentBlockDbId'];
                } else if($blockData['notes'] == 'cross pattern' && $currentAttributes['totalCount'] == 0){
                    $deleteIds[] = $blockData['experimentBlockDbId'];
                } else {
                    $crossPatternBlock[] = $blockData['experimentBlockDbId'];
                }
            }
            if(count($deleteIds) > 0){
                $this->experimentBlock->deleteMany($deleteIds);
                //return a notification that the initial planting arrangement was deleted
                // remove design generated to status
                $this->plantingArrangement->removeDesignGeneratedStatus($id);

                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The previously created planting arrangement for this experiment was deleted to accommodate the cross pattern.');
            }
            
            if(count($crossPatternBlock) == 0){
                $this->plantingArrangement->addNewRows($id, 1, 'cross pattern');
            }
        }

        $variable = $this->variable->getVariableByAbbrev('CROSSING_PATTERN');
        $crossAttrList = [];
        $crossIds = [];

        foreach($insertCrossList as $key => $value){
            $crossDbId = $newCrossRecords['data'][$key]['crossDbId'].'';
            $crossIds[] = $crossDbId;
            $crossAttrList[] = [
                'crossDbId' => "$crossDbId",
                'dataValue' => $crossPattern,
                'variableDbId' => $variable['variableDbId'].''
            ];
        }
        if($crossPattern != null){
            $this->crossAttribute->create(['records' => $crossAttrList]);
        }

        //save in planting arrangement
        //Check if there's already saved experiment block
        $params = ["experimentDbId" => "$id", "notes"=>"cross pattern"];
        $experimentBlockData = $this->experimentBlock->searchAll($params);

        if($experimentBlockData['totalCount'] == 1){  
            $experimentBlock = $experimentBlockData['data'][0];

            $femaleEntryIds = [];
            $maleEntryIds = [];
            //get cross parents of the crosses
            $params = [
                'crossDbId' => 'equals '.implode("|equals ", $crossIds),
                'experimentDbId' => "equals $id",
                'crossParentRole' => "equals female"
            ];
          
            $femaleParents = $this->crossParent->searchAll($params, 'sort=entryNo')['data'];
            if(isset($femaleParents) && count($femaleParents) > 0){
                $femaleEntryIds = array_column($femaleParents, 'entryDbId');
            }

            $params = [
                'crossDbId' => 'equals '.implode("|equals ", $crossIds),
                'experimentDbId' => "equals $id",
                'crossParentRole' => "equals male"
            ];
          
            $maleParents = $this->crossParent->searchAll($params, 'sort=entryNo')['data'];

            if(isset($maleParents) && count($maleParents) > 0){
                $maleEntryIds = array_column($maleParents, 'entryDbId');
            }

            //check what's the repeated parent
            if($crossPattern != null){
                $repeated = [];
                $nonRepeated = [];
                if(count(array_unique($femaleEntryIds)) == 1){
                    $repeated = $femaleParents;
                    $nonRepeated = $maleParents;
                } else if(count(array_unique($maleEntryIds)) == 1){
                    $repeated = $maleParents;
                    $nonRepeated = $femaleParents;
                }
            } else {
                $params = [
                    'distinctOn' => 'entryNo',
                    'crossDbId' => 'equals '.implode("|equals ", $crossIds),
                    'experimentDbId' => "equals $id"
                ];

                $parents = $this->crossParent->searchAll($params, 'sort=entryNo')['data'];
                $nonRepeated = $parents;
            }

            $entryListIdList = ($experimentBlock['entryListIdList'] != NULL)? $experimentBlock['entryListIdList'] : [];
            $layoutOrderData = ($experimentBlock['layoutOrderData'] != NULL)? $experimentBlock['layoutOrderData'] : [];

            $batchNo = !empty($layoutOrderData) ? $layoutOrderData[count($layoutOrderData)-1]['batch_no']+1 : 1;
            $plotNo = !empty($layoutOrderData) ? $layoutOrderData[count($layoutOrderData)-1]['plotno']+1 : 1;

            for($i=0; $i<count($nonRepeated); $i++){
               
                $entno = $nonRepeated[$i]['entryNo'];
                $entryId = $nonRepeated[$i]['entryDbId'];
                $crossId = $nonRepeated[$i]['crossDbId'];
                $entIdx = array_search($entryId, array_column($entryListIdList, 'entryDbId'));

                if($entIdx !== false){
                    $repno = $entryListIdList[$entIdx]['repno']+1;
                    $entryListIdList[$entIdx]['repno'] = $repno;
                } else {
                    $repno = 1;
                    $entryListIdList[] = [
                        'repno' => $repno,
                        'entryDbId' => $entryId
                    ];
                }
                
                $crossType = "normal";
                if($crossPattern != null){
                    $crossType = "nonrepeated";
                }
                $layoutOrderData[] = [
                    "entno" => $entno,
                    "repno" => $repno,
                    "plotno" => $plotNo++,
                    "block_id" => $experimentBlock['experimentBlockDbId'],
                    "block_no" => 1,
                    "entry_id" => $entryId,
                    "parent_id" => null,
                    "times_rep" => null,
                    "designation" => $nonRepeated[$i]['germplasmDesignation'],
                    "prev_block_no_of_rows" => 0,
                    "pattern" => $crossPattern,
                    "batch_no" => $batchNo,
                    "cross_id" => $crossId,
                    "crossType" => $crossType
                ];

                if($crossPattern != null){
                    //for repeated parent
                    $entno = $repeated[0]['entryNo'];
                    $entryId = $repeated[0]['entryDbId'];
                    $crossId = $repeated[0]['crossDbId'];

                    $entIdx = array_search($entryId, array_column($entryListIdList, 'entryDbId'));

                    if($entIdx !== false){
                        $repno = $entryListIdList[$entIdx]['repno']+1;
                        $entryListIdList[$entIdx]['repno'] = $repno;
                    } else {
                        $repno = 1;
                        $entryListIdList[] = [
                            'repno' => $repno,
                            'entryDbId' => $entryId
                        ];
                    }
                    
                    $layoutOrderData[] = [
                        "entno" => $entno,
                        "repno" => $repno,
                        "plotno" => $plotNo++,
                        "block_id" => $experimentBlock['experimentBlockDbId'],
                        "block_no" => 1,
                        "entry_id" => $entryId,
                        "parent_id" => null,
                        "times_rep" => null,
                        "designation" => $repeated[0]['germplasmDesignation'],
                        "prev_block_no_of_rows" => 0,
                        "pattern" => $crossPattern,
                        "batch_no" => $batchNo,
                        "cross_id" => $crossId,
                        "crossType" => "repeated"
                    ];
                    
                    //if triplet pattern
                    if($crossPattern == 'Triplet'){
                        $i = $i+1;
                        if($i < count($nonRepeated)){
                            $entno = $nonRepeated[$i]['entryNo'];
                            $entryId = $nonRepeated[$i]['entryDbId'];
                            $crossId = $nonRepeated[$i]['crossDbId'];
                            
                            $entIdx = array_search($entryId, array_column($entryListIdList, 'entryDbId'));

                            if($entIdx !== false){
                                $repno = $entryListIdList[$entIdx]['repno']+1;
                                $entryListIdList[$entIdx]['repno'] = $repno;
                            } else {
                                $repno = 1;
                                $entryListIdList[] = [
                                    'repno' => $repno,
                                    'entryDbId' => $entryId
                                ];
                            }
                            
                            $layoutOrderData[] = [
                                "entno" => $entno,
                                "repno" => $repno,
                                "plotno" => $plotNo++,
                                "block_id" => $experimentBlock['experimentBlockDbId'],
                                "block_no" => 1,
                                "entry_id" => $entryId,
                                "parent_id" => null,
                                "times_rep" => null,
                                "designation" => $nonRepeated[$i]['germplasmDesignation'],
                                "prev_block_no_of_rows" => 0,
                                "pattern" => $crossPattern,
                                "batch_no" => $batchNo,
                                "cross_id" => $crossId,
                                "crossType" => "nonrepeated"
                            ];
                        }
                    }
                }
                
            }

            $updatedData = [
                "entryListIdList" => json_encode($entryListIdList),
                "layoutOrderData" => json_encode($layoutOrderData)
            ];
                
            $this->experimentBlock->updateOne($experimentBlock['experimentBlockDbId'], $updatedData);
        }
    }

    /**
     * Get cross attribute values
     * 
     * @param Integer $id experiment identifier
     * 
     */
    public function getCrossAttributes($id) 
    {
        //check if there's existing cross pattern data
        $variable = $this->variable->getVariableByAbbrev('CROSSING_PATTERN');
        $sqlData = [
            'experimentDbId'=> "equals $id",
            'variableDbId' => "equals ".$variable['variableDbId']
        ];
        $crossAttributes = $this->crossAttribute->searchAll($sqlData); 

        $combiArr = [];
        if($crossAttributes['totalCount'] > 0){
            $crossIds = array_column($crossAttributes['data'], 'crossDbId');
            $attrValue = array_column($crossAttributes['data'], 'dataValue');

            $combiArr = array_combine($crossIds, $attrValue);
        }

        return $combiArr;
    }

    /**
     * Delete cross pattern info
     * 
     * @param Integer $experimentDbId experiment identifier
     * 
     */
    public function deleteCrossPatternInfo($experimentDbId){
        //retrieve cross attributes
        $crossAttr = $this->crossAttribute->searchAll([
            'experimentDbId'=>"equals $experimentDbId"
        ]);

        $crossAttrDbIds = isset($crossAttr['data'][0]['crossAttributeDbId']) ? array_column($crossAttr['data'],'crossAttributeDbId') : [];           
        
        //delete cross attributes
        $test = $this->crossAttribute->deleteMany($crossAttrDbIds);
        
        //delete experiment block with cross pattern
        //retrieve experiment block
        $expBlock = $this->experimentBlock->searchAll([
            'experimentDbId'=>"equals $experimentDbId"
        ]);
        $expBlockDbIds = isset($expBlock['data'][0]['experimentBlockDbId']) ? array_column($expBlock['data'],'experimentBlockDbId') : [];      
        //delete experiment block
        $this->experimentBlock->deleteMany($expBlockDbIds);
        // remove design generated to status
        $this->plantingArrangement->removeDesignGeneratedStatus($experimentDbId);
    }

    /**
     * Get crosses batch count
     *
     * @param Integer $experimentDbId experiment identifier
     * @param Array $crossIds list of cross identifiers
     */
    public function getCrossesBatchCount($experimentDbId, $crossIds){
        $experimentBlockData = $this->experimentBlock->searchAll([
            'experimentDbId' => "equals $experimentDbId",
            'notes' => 'cross pattern'
        ]);

        $batchCrossIds = [];
        if($experimentBlockData['totalCount'] == 1){
            $experimentBlock = $experimentBlockData['data'][0];
            $layoutOrderData = ($experimentBlock['layoutOrderData'] != NULL)? $experimentBlock['layoutOrderData'] : [];

            $newLayoutOrder = [];
            $newEntryListId = [];
            for($i=0; $i < count($crossIds); $i++){
                $idx = array_search($crossIds[$i], array_column($layoutOrderData, 'cross_id'));

                if($idx !== false && isset($layoutOrderData[$idx])){
                    if($layoutOrderData[$idx]['crossType'] != 'normal'){
                        $batchNo = $layoutOrderData[$idx]['batch_no'];
                        $newLayoutOrder = [];

                        for($y=0; $y<count($layoutOrderData);$y++){
                            if($batchNo != $layoutOrderData[$y]['batch_no']){
                                $newLayoutOrder[] = $layoutOrderData[$y];
                            } else {
                                $batchCrossIds[] =  $layoutOrderData[$y]['cross_id'];
                            }
                        }

                        $layoutOrderData = $newLayoutOrder;
                    } else {
                        unset($layoutOrderData[$idx]);
                        $layoutOrderData = array_values($layoutOrderData);
                    }
                }

            }
            $batchCrossIds = array_unique($batchCrossIds);
        }

        return count($batchCrossIds);
    }

    /**
     * Delete planting arrangement info
     * 
     * @param Integer $experimentDbId experiment identifier
     * @param Array $crossIds list of cross identifiers
     */
    public function deleteArrangement($experimentDbId, $crossIds){
        //retrieve cross attributes
        $crossAttr = $this->crossAttribute->searchAll([
            'experimentDbId'=>"equals $experimentDbId",
        ]);

        //check if there's cross pattern saved
        if($crossAttr['totalCount'] > 0){

            //retrieve cross attributes
            $crossAttr = $this->crossAttribute->searchAll([
                'experimentDbId'=>"equals $experimentDbId",
                'crossDbId' => 'equals '.implode('|equals ', $crossIds)
            ]);
            $crossAttrDbIds = isset($crossAttr['data'][0]['crossAttributeDbId']) ? array_column($crossAttr['data'],'crossAttributeDbId') : [];      

            if(count($crossAttrDbIds) > 0){
                //delete cross attributes
                $this->crossAttribute->deleteMany($crossAttrDbIds); 
            } 

            //delete plots
            //retrieve experiment block
            $experimentBlockData = $this->experimentBlock->searchAll([
                'experimentDbId'=>"equals $experimentDbId",
                'notes' => 'cross pattern'
            ]);

            $batchCrossIds = [];
            if($experimentBlockData['totalCount'] == 1){
                $experimentBlock = $experimentBlockData['data'][0];
                $entryListIdList = ($experimentBlock['entryListIdList'] != NULL)? $experimentBlock['entryListIdList'] : [];
                $layoutOrderData = ($experimentBlock['layoutOrderData'] != NULL)? $experimentBlock['layoutOrderData'] : [];

                $newLayoutOrder = [];
                $newEntryListId = [];
                $crossCounter = 0;
                for($i=0; $i < count($crossIds); $i++){
                    $idx = array_search($crossIds[$i], array_column($layoutOrderData, 'cross_id'));
                   
                    if($idx !== false && isset($layoutOrderData[$idx])){
                        if($layoutOrderData[$idx]['crossType'] != 'normal'){
                            $batchNo = $layoutOrderData[$idx]['batch_no'];
                            $newLayoutOrder = [];
                            
                            for($i=0; $i<count($layoutOrderData);$i++){
                                if($batchNo != $layoutOrderData[$i]['batch_no']){
                                    $newLayoutOrder[] = $layoutOrderData[$i];
                                } else {
                                    $batchCrossIds[] =  $layoutOrderData[$i]['cross_id'];
                                }
                            }

                            $layoutOrderData = $newLayoutOrder;
                        } else {
                            unset($layoutOrderData[$idx]);
                            $layoutOrderData = array_values($layoutOrderData);
                        }
                    }

                }

                $newLayoutOrder = [];
                $newEntryListIdList = [];
                $plotNo = 1;
                foreach($layoutOrderData as $layout){
                    $layout['plotno'] = $plotNo++;
                    $entryId = $layout['entry_id'];
                    $entIdx = array_search($entryId, array_column($newEntryListIdList, 'entryDbId'));

                    if($entIdx !== false){
                        $repno = $newEntryListIdList[$entIdx]['repno']+1;
                        $newEntryListIdList[$entIdx]['repno'] = $repno;
                    } else {
                        $repno = 1;
                        $newEntryListIdList[] = [
                            'repno' => $repno,
                            'entryDbId' => $entryId
                        ];
                    }
                    $layout['repno'] = $repno;

                    $newLayoutOrder[] = $layout;
                }
            }
            
            //delete additional crosses in the batch
            if(count($batchCrossIds) > 0){
                $this->cross->deleteSelectedCrosses($experimentDbId,$batchCrossIds); 
            }
            $updatedData = [
                "entryListIdList" => json_encode($newEntryListIdList),
                "layoutOrderData" => json_encode($newLayoutOrder)
            ];
                
            $this->experimentBlock->updateOne($experimentBlock['experimentBlockDbId'], $updatedData);
            // remove design generated to status
            $this->plantingArrangement->removeDesignGeneratedStatus($experimentDbId);
        } else if($crossAttr['totalCount'] == 0){
            //delete experiment block with cross pattern
            //retrieve experiment block
            $experimentBlock = $this->experimentBlock->searchAll([
                'experimentDbId'=>"equals $experimentDbId",
                'notes' => "cross pattern"
            ]);
            $expBlockDbIds = isset($experimentBlockData['data'][0]['experimentBlockDbId']) ? array_column($experimentBlock['data'],'experimentBlockDbId') : [];

            if(count($expBlockDbIds) > 0){
                //delete experiment block
                $this->experimentBlock->deleteMany($expBlockDbIds);

                // remove design generated to status
                $this->plantingArrangement->removeDesignGeneratedStatus($experimentDbId);
            }
        }
    }

    /**
     * Auto generate planting arrangement for normal parents
     * 
     * @param Integer $experimentDbId experiment identifier
     */
    public function autoGenerateNormalParents($experimentId){

        //retrieve experiment block
        $experimentBlockData = $this->experimentBlock->searchAll([
            'experimentDbId'=>"equals $experimentDbId",
            'notes' => "cross pattern"
        ]);

        if($experimentBlockData['totalCount'] > 0){
            $experimentBlock = $experimentBlockData['data'][0];
            $entryListIdList = ($experimentBlock['entryListIdList'] != NULL)? $experimentBlock['entryListIdList'] : [];
            $layoutOrderData = ($experimentBlock['layoutOrderData'] != NULL)? $experimentBlock['layoutOrderData'] : [];
            $entryListId = $this->entryList->getEntryListId($id);
            $layoutEntryIds = array_column($entryListIdList, 'entry_id');

            $entryRecords = $this->entry->searchAll([
                "fields" => "entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.id AS entryDbId|entry.germplasm_id as germplasmDbId",
                "entryDbId"=>"not equals ".implode("|not equals ", $layoutEntryIds),
                "entryListDbId"=>"equals $entryListId",
            ], 'sort=entryNumber:ASC', true);

            if($entryRecords['totalCount'] > 0){
                $entries = $entryRecords['data'];
                $crossId = null;
                $batchNo = !empty($layoutOrderData) ? $layoutOrderData[count($layoutOrderData)-1]['batch_no']+1 : 1;
                $plotNo = !empty($layoutOrderData) ? $layoutOrderData[count($layoutOrderData)-1]['plotno']+1 : 1;

                for($i=0; $i<count($entries); $i++){
                    $entno = $entries[$i]['entryNumber'];
                    $entryId = $entries[$i]['entryDbId'];
                    
                    $repno = 1;
                    $entryListIdList[] = [
                        'repno' => $repno,
                        'entryDbId' => $entryId
                    ];
                    
                    $crossType = "normal";
                    
                    $layoutOrderData[] = [
                        "entno" => $entno,
                        "repno" => $repno,
                        "plotno" => $plotNo++,
                        "block_id" => $experimentBlock['experimentBlockDbId'],
                        "block_no" => 1,
                        "entry_id" => $entryId,
                        "parent_id" => null,
                        "times_rep" => null,
                        "designation" => $entries[$i]['designation'],
                        "prev_block_no_of_rows" => 0,
                        "pattern" => null,
                        "batch_no" => $batchNo,
                        "cross_id" => $crossId,
                        "crossType" => $crossType
                    ];

                }
            }
        }
    }
}