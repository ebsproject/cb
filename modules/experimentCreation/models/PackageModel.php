<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for Seed lot
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use Yii;
use app\models\Entry;
use app\models\EntryList;
use app\models\Seed;
use app\models\Package;

use yii\data\ArrayDataProvider;

class PackageModel extends \yii\base\Model 
{
    protected $mainEntryModel;
    protected $mainEntryListModel;
    protected $mainSeedModel;

    public $entryNumber;
    public $packageQuantity;
    public $seedManager;
    public $germplasmName;
    public $experimentName;
    public $experimentYear;
    public $experimentStageCode;
    public $experimentSeason;
    public $programName;
    public $seedName;
    public $packageLabel;
    public $packageUnit;
    public $experimentType;
    public function __construct(
        Entry $mainEntryModel, 
        EntryList $mainEntryListModel, 
        Seed $mainSeedModel
    )
    {
        $this->mainEntryModel = $mainEntryModel;
        $this->mainEntryListModel = $mainEntryListModel;
        $this->mainSeedModel = $mainSeedModel;
    }

    /**
     * Generates rules for filtering
     */
    public function rules() {
        return [
            [['entryNumber', 'packageQuantity'], 'integer'],
            [['seedManager','germplasmName', 'experimentName','experimentYear','experimentStageCode','experimentSeason','programName','seedName', 'packageLabel', 'packageUnit', 'experimentType'], 'string'],
            [['entryNumber', 'packageQuantity','programName', 'seedManager','germplasmName', 'experimentName','experimentYear','experimentStageCode','experimentSeason', 'seedName', 'packageLabel', 'packageUnit', 'experimentType'], 'safe'],
        ];
    }

    /**
     * Gets the filters for bulk package selection.
     *
     * @param integer $id experiment record identifier
     * @param array $filter associative values used for searching packages
     */
    public function getFilterBulkPackage($id, $filter=array()) {
        extract($filter);

        $entryIdArray = isset($entryIdArray) ? $entryIdArray : '[]';
        $entries = $this->getEntries($entryIdArray, $id);
        
        $germplasmIds = array_column($entries, 'germplasmDbId');
        $packages = $this->getPackages($germplasmIds, $filter);

        $experiment = array_filter(array_unique(array_column($packages, 'experimentName')));
        $year = array_filter(array_unique(array_column($packages, 'experimentYear')));
        $season = array_filter(array_unique(array_column($packages, 'experimentSeason')));
        $stage = array_filter(array_unique(array_column($packages, 'experimentStageCode')));
      
        asort($experiment);
        asort($year);
        asort($season);
        asort($stage);
        return [
            'experiment' => array_combine($experiment, $experiment),
            'year' => array_combine($year, $year),
            'season' => array_combine($season, $season),
            'stage' => array_combine($stage, $stage),
        ];
    }

    /**
     * Gets package browser data for bulk package selection.
     *
     * @param int $id experiment record identifier
     * @param array $filter associative values used for searching packages
     * @param int $limit optional maximum number of records to return, and
     *   flags the return value if number of records exceed this threshold;
     *   providing a $limit disables auto-pagination by the ArrayDataProvider in the response
     * @param array $pagination optional value to paginate response data : { limit => int, page => int }
     *   $pagination takes precedence over $limit if both are given
     */
    public function getFilterBulkRecords($id, $filter=array(), $limit=null, $pagination=null) {
        extract($filter);

        $entryIdArray = $entryIdArray ?? '[]';
        $entries = $this->getEntries($entryIdArray, $id);

        $entryDbIds = array_column($entries, 'entryDbId');
        // Get data browser records
        $packages = $this->getPackages($entryDbIds, $filter,
            fieldName: 'entryDbId', dataLevel: 'entry', sort: 'entryNumber:ASC', pagination: $pagination);
        // Create a counter for { entryNumber => size }
        $entryNumberCounter = array_count_values(array_column($packages, 'entryNumber'));

        $dataProvider = $this->getDataProvider('dynagrid-package-browser', $packages, paginated: empty($limit));

        return $this->partition($dataProvider, $entryNumberCounter, $limit, paginated: empty($limit));
    }


    /*************************** HELPER FUNCTIONS ***************************/

    /**
     * Gets Entry data given their ids. If ids not given,
     * gets data associated with an Experiment instead.
     *
     * @param string $entryIdArray list of Entry primary key
     * @param integer $experimentId primary key of the Experiment
     * @return array list of Entry data
     */
    protected function getEntries($entryIdArray, $experimentId) {
        $entryIdArray = json_decode($entryIdArray, true);

        if(isset($entryIdArray) && !empty($entryIdArray)){
            $filterEntries = [
                'fields' => 'entry.entry_list_id as entryListDbId|'.
                    'entry.id as entryDbId|'.
                    'entry.germplasm_id as germplasmDbId|'.
                    'entry.seed_id as seedDbId|'.
                    'package.id AS packageDbId|'.
                    'entry.entry_number as entryNumber',
                'entryDbId'=> 'equals '.implode('|equals ', $entryIdArray)
            ];
        } else {
            $entryList = $this->mainEntryListModel->searchRecords(['experimentDbId'=>"equals $experimentId"]);
            if(!empty($entryList)){
                $entryListId = $entryList[0]['entryListDbId'];
            } else {
                // Create a new entry list
                $entryListId = $this->mainEntryListModel->getEntryListId($experimentId, true);
            }
            $filterEntries = [
                'fields' => 'entry.entry_list_id as entryListDbId|'.
                    'entry.id as entryDbId|'.
                    'entry.germplasm_id as germplasmDbId|'.
                    'entry.seed_id as seedDbId|'.
                    'package.id AS packageDbId|'.
                    'entry.entry_number as entryNumber',
                'entryListDbId'=>"equals $entryListId"
            ];
        }
        $entries = $this->mainEntryModel->searchRecords($filterEntries);

        return $entries;
    }

    /**
     * Gets seed packages data of the given field/s.
     *
     * @param array $fieldIds list of entity primary key
     * @param array $filter associative values used for searching packages
     * @param string $fieldName entity name for the field associated with $fieldIds used for searching packages
     * @param string $dataLevel optional granularity of seed packages data : all|germplasm|seed|entry
     * @param string $sort optional value to sort results by field
     * @param array $pagination optional value to paginate response data : { limit => int, page => int }
     *
     * @return array list of seed package data
     */
    public function getPackages($fieldIds, $filter, $fieldName='germplasmDbId', $dataLevel='all', $sort=null, $pagination=null) {
        extract($filter);

        $filterSeedPackages = [
            'fields' => 'seed.id AS seedDbId|'.
                'seed.germplasm_id AS germplasmDbId|'.
                'seed.seed_code AS seedCode|'.
                'seed.seed_name AS seedName|'.
                'program.program_code AS seedManager|'.
                'program.program_name AS programName|'.
                'germplasm.designation AS germplasmName|'.
                'package.id AS packageDbId|'.
                'package.package_code AS packageCode|'.
                'package.package_label AS packageLabel|'.
                'package.package_quantity AS packageQuantity|'.
                'package.package_unit AS packageUnit|'.
                'experiment.experiment_year AS experimentYear|'.
                'experiment.experiment_type AS experimentType|'.
                'experiment.experiment_name AS experimentName|'.
                'season.season_code AS experimentSeason|'.
                'stage.stage_code AS experimentStageCode|'.
                'entry.id AS entryDbId|'.
                'entry.entry_number AS entryNumber|'.
                'entry.package_id AS currentPackageDbId'  // identifies the package assigned to the same entry group
        ];

        $filterSeedPackages[$fieldName] = 'equals '.implode('|equals ', $fieldIds);
        if (isset($yearFilter) && $yearFilter != '') {
            $filterSeedPackages['experimentYear'] = "equals ".$yearFilter;
        }
        if (isset($experimentFilter) && $experimentFilter != '') {
            $filterSeedPackages['experimentName'] = "equals ".$experimentFilter;
        }
        if(isset($seasonFilter) && $seasonFilter != '') {
            $filterSeedPackages['experimentSeason'] = "equals ".$seasonFilter;
        }
        if(isset($stageFilter) && $stageFilter != '') {
            $filterSeedPackages['experimentStageCode'] = "equals ".$stageFilter;
        }
        if(isset($programFilter) && $programFilter != '') {
            $filterSeedPackages['seedManager'] = "equals ".$programFilter;
        }
        
        foreach($filter as $key=>$value){
            if(!in_array($key, ['yearFilter', 'experimentFilter', 'seasonFilter','stageFilter','programFilter']) && $value != "" && $key !='entryIdArray'){
                $filterSeedPackages[$key] = "equals ".$value;
            }
        }

        return $this->mainSeedModel->searchPackageRecords($filterSeedPackages,
            dataLevel: $dataLevel, sort: $sort, pagination: $pagination);
    }

    /**
     * Gets an ArrayDataProvider object that represents package entries.
     *
     * @param string $id data provider identifier
     * @param array $records data provider models
     * @param bool $paginated sets ArrayDataProvider pagination value
     *
     * @return ArrayDataProvider data with package information
     */
    protected function getDataProvider($id, $records, $paginated=false) {
        $config = [
            'allModels' => $records,
            'key' => 'entryDbId',
            'sort' => [
                'defaultOrder' => ['entryNumber'=>SORT_ASC],
                'attributes' => ['entryNumber', 'packageQuantity','programName', 'seedManager','germplasmName', 'experimentName','experimentYear','experimentStageCode','experimentSeason', 'seedName', 'packageLabel', 'packageUnit', 'experimentType']
            ],
            'id' => $id
        ];

        if ($paginated == false) $config['pagination'] = false;
        return new ArrayDataProvider($config);
    }

    /**
     * Modifies the response if package records need to be partitioned.
     *
     * @param ArrayDataProvider $dataProvider package browser data
     * @param array $entryNumbers associative array of entry number => count
     * @param int $limit partition size
     * @param bool $paginated sets ArrayDataProvider pagination value
     */
    protected function partition($dataProvider, $entryNumbers, $limit, $paginated=false) {
        $partitionFlag = false;
        $packageRecords = $dataProvider->allModels;

        if (($limit != null) && (count($packageRecords) > $limit)) {
            $partitionFlag = true;

            // Sort records ascending by entryNumber
            usort($packageRecords, function ($record1, $record2) {
                return $record1['entryNumber'] <=> $record2['entryNumber'];
            });
            ksort($entryNumbers, SORT_NUMERIC);

            // Partition the records then remove any entry number from finalEntries not in the new partition
            $allEntryNumbers = array_unique(array_column($packageRecords, 'entryNumber'));
            $packageRecords = array_slice($packageRecords, 0, $limit);
            $goodEntryNumbers = array_unique(array_column($packageRecords, 'entryNumber'));
            $badEntryNumbers = array_diff($allEntryNumbers, $goodEntryNumbers);
            $entryNumbers = array_diff_key($entryNumbers, array_flip($badEntryNumbers));

            // Update the last entry number counter in finalEntries to the correct count
            $idx = array_key_last($entryNumbers);
            $finalEntriesRecordCount = array_sum($entryNumbers);
            $newEntryCount = $entryNumbers[$idx] - ($finalEntriesRecordCount - $limit);
            // NOTE: if newEntryCount is 1, it may be auto-selected downstream
            $entryNumbers[$idx] = $newEntryCount;
        }

        return [
            'dataProvider' => $this->getDataProvider($dataProvider->id, $packageRecords, paginated: $paginated),
            'entryNumbers' => $entryNumbers,
            'partitionView' => $partitionFlag
        ];
    }

    /**
     * Search model for package browser
     *
     * @param integer $id experimen record identifier
     * @param array $filter list of query parameters
     * @param array $params data browser parameters
     * @param integer $limit limit parameter
     * @param integer $pagination current page parameter
     * @return array $dataProvider experiment browser data provider
     */
    public function search($id, $filter, $params,$limit=null, $pagination=null)
    {

        $dataProvider = [];
        
        $dataProvider = $this->getFilterBulkRecords($id, $filter, $limit, $pagination);
        
        //validate results
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        //no results found
        return $dataProvider;
    }

}