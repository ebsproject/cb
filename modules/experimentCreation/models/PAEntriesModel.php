<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Planting Arrangement:Assign Entries
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Entry;
use app\models\EntryList;
use app\models\Experiment;
use app\models\ExperimentData;
use app\models\Variable;

use yii\helpers\ArrayHelper;
use Yii;
class PAEntriesModel {

    protected $entry;
    protected $entryList;
    protected $experiment;
    protected $experimentData;
    protected $variable;

    public function __construct(
        Entry $entry, 
        EntryList $entryList, 
        Experiment $experiment,
        ExperimentData $experimentData,
        Variable $variable)
    {
        $this->entry = $entry;
        $this->entryList = $entryList;
        $this->experiment = $experiment;
        $this->experimentData = $experimentData;
        $this->variable = $variable;
    }

    /**
     * Get replicates of the entries
     * 
     * @param integer $experimentId record id of the experiment
     * @param integer $entryListId record id of the entry list
     */
    public function getEntryReplicatesInfo($experimentId, $entryListId){

        //get entry list no. of replicates value
        $varAbbrev = 'ENTRY_LIST_TIMES_REP';
        $variableInfo = $this->variable->searchAll(["abbrev"=>"$varAbbrev"]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        $expData = $this->experimentData->getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);
        
        $entryReplicates = [];
        $entryListArray =  $this->entry->searchAll(['fields'=>'entry.entry_list_id as entryListDbId|entry.id as entryDbId|entry.entry_number as entryNumber','entryListDbId'=>"equals $entryListId"], 'sort=entryNumber:asc', true)['data'];
        if(isset($expData[0]) && !empty(json_decode($expData[0]['dataValue'],true))){
            
            $entryReplicates = $this->generateReplicates($entryListArray, json_decode($expData[0]['dataValue'],true));
            $dataRequestBodyParamUpdate = [
                'dataValue' => json_encode($entryReplicates)
            ];
            $exptDataDbId = $expData[0]['experimentDataDbId'];
            $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);
        } else {   //create variable
            $entryReplicates = $this->generateReplicates($entryListArray);              
            $dataRequestBodyParam[] = [
                'variableDbId' => "".$variableDbId,
                'dataValue' => json_encode($entryReplicates)
            ];
            $test = $this->experiment->createExperimentData($experimentId,$dataRequestBodyParam);
        }
        
        return $entryReplicates;
    }

    /**
     * Generate default replicates of the entries
     * 
     * @param array $entryListArray record id of the entry list
     * @param array $oldArray record id of the entry list with replicates
     */
    public function generateReplicates($entryListArray, $oldArray = null){

        $entryReplicates = [];
        if(!empty($entryListArray) && $oldArray == null){
            $entryIds = array_column($entryListArray, 'entryDbId');

            for($i = 0; $i < count($entryIds); $i++){
                $entryReplicates[$entryIds[$i]] = [
                    "repno" => 1,
                    "replicates" => [
                        1 => null
                    ]
                ];
            }
        } else {
            $entryIds = array_column($entryListArray, 'entryDbId');
            $entryReplicates = $oldArray;
            for($i = 0; $i < count($entryIds); $i++){
                if(!isset($entryReplicates["".$entryIds[$i]])){
                    $temp = [
                        "repno" => 1,
                        "replicates" => [
                            1 => null
                        ]
                    ];
                    $entryReplicates[$entryIds[$i]] = $temp;
                }
            }
        }

        return $entryReplicates;
    }

    /**
     * Save entry list replicate data
     *
     * @param integer $experimentId record id of the experiment
     * @param array $data entry information with replicates
     */
    public function saveEntryListReplicate($experimentId, $data) {
        //get entry list replicate list value
        $varAbbrev = 'ENTRY_LIST_TIMES_REP';
        $variableInfo = $this->variable->searchAll(["abbrev"=>"$varAbbrev"]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        $expData = $this->experimentData->getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);

        if(isset($expData[0])){
            $dataRequestBodyParamUpdate = [
                'dataValue' => json_encode($data)
            ];
            $exptDataDbId = $expData[0]['experimentDataDbId'];
            $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);

        } else {  //create variable
            $dataRequestBodyParam[] = [
                'variableDbId' => "".$variableDbId,
                'dataValue' => json_encode($data)
            ];
            $test = $this->experiment->createExperimentData($experimentId,$dataRequestBodyParam);
        }
    }

    /**
     * Update entry list replicate data for when adding, overwriting, or appending reps to a block
     * Can also be used when deleting reps from a block (just pass reps to be deleted in entryListIdList format then set $blockIds to [null])
     *
     * @param integer $experimentId record id of the experiment
     * @param array $entryListIdList entry and repno information of a block
     * @param array $blockIds record ids of affected blocks
     * @param string $assignBlockStatus mode to do with reps on a block (whether to overwrite, append, or others)
     */
    public function updateEntryListReplicate($experimentId, $entryListIdList, $blockIds, $assignBlockStatus = 'not overwrite') {
        // get existing entry list replicate data value
        $expData = $this->experimentData->getExperimentData($experimentId, ['abbrev' => 'equals ENTRY_LIST_TIMES_REP']);
        $entryReplicates = (isset($expData[0]) && !empty(json_decode($expData[0]['dataValue'],true)))? json_decode($expData[0]['dataValue'],true) : [];

        // if overwrite, set existing entry reps in block ids to null
        if($assignBlockStatus == 'overwrite') {
            foreach ($entryReplicates as $entryDbId => $entry) {
                foreach ($entry['replicates'] as $repNo => $blockId) {
                    if($entryReplicates[$entryDbId]['replicates'][$repNo] == $blockIds[0] || is_null($entryListIdList)) {
                        $entryReplicates[$entryDbId]['replicates'][$repNo] = null;
                    }
                }
            }
        }

        if(is_null($entryListIdList)) {
            $entryListIdList = [];
        }

        if(is_string($entryListIdList)) {
            $entryListIdList = json_decode($entryListIdList,true);
        }

        // update entry reps according to $entryListIdList and $blockIds
        foreach ($entryListIdList as $entryIndex => $entry) {
            if(!isset($entry['replicates'])) continue;
            $repArray = is_string($entry['replicates'])? json_decode($entry['replicates'], true) : $entry['replicates'];
            // assign rep to only first block
            foreach ($repArray as $repIndex => $rep) {
                $entryReplicates[$entry['entryDbId']]['replicates'][$rep] = is_null($blockIds[0])? null : intval($blockIds[0]);
            }
        }

        // save entry rep
        $dataRequestBodyParamUpdate = [
            'dataValue' => json_encode($entryReplicates)
        ];
        if(isset($expData[0])) {
            $exptDataDbId = $expData[0]['experimentDataDbId'];
            $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);
        }
    }

    /**
     * Generate working list data
     * 
     * @param integer $experimentId record id of the experiment
     * @param integer $entryListId record id of the entry list
     * @param array $entryData entry information
     * @param array $entryReplicates entry replicates information
     */
    public function generateWorkingList($experimentId, $entryListId, $entryData, $entryReplicates){

        if(!empty($entryData['data'])){
            $temporaryPlots = [];
            foreach($entryData['data'] as $entry){
                for($i = 1; $i <= (int)$entryReplicates[$entry['entryDbId']]["repno"]; $i++){
                    if(is_null($entryReplicates[$entry['entryDbId']]['replicates'][$i])) {
                        $tempEntry = $entry;
                        $tempEntry['replicate'] = $i;
                        $temporaryPlots[] = $tempEntry;
                    }
                }
            }

            $entryData['data'] = $temporaryPlots;
        }
       
        return $entryData;
    }

    /**
     * Save working list data
     * 
     * @param integer $experimentId record id of the experiment
     * @param array $data entry information with replicates
     */
    public function saveWorkingList($experimentId, $data){
        //get working list value
        $varAbbrev = 'PA_ENTRY_WORKING_LIST';
        $variableInfo = $this->variable->searchAll(["abbrev"=>"$varAbbrev"]);
        $variableDbId = isset($variableInfo['data']['0']['variableDbId']) ? $variableInfo['data']['0']['variableDbId'] : 0;
        $expData = $this->experimentData->getExperimentData($experimentId, ["abbrev" => "equals "."$varAbbrev"]);
        
        if(isset($expData[0])){
            $dataRequestBodyParamUpdate = [
                'dataValue' => json_encode($data)
            ];
            $exptDataDbId = $expData[0]['experimentDataDbId'];
            $dataUpdate = $this->experimentData->updateExperimentData($exptDataDbId, $dataRequestBodyParamUpdate);

        } else {  //create variable            
            $dataRequestBodyParam[] = [
                'variableDbId' => "".$variableDbId,
                'dataValue' => json_encode($data)
            ];
            $test = $this->experiment->createExperimentData($experimentId,$dataRequestBodyParam);
        }
    }
}