<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for the Browser Controller
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\BackgroundJob;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\PlantingInstruction;

use app\modules\experimentCreation\models\EntryOrderModel;

//interfaces
use app\interfaces\modules\experimentCreation\models\IBrowserModel;

class BrowserModel implements IBrowserModel{

    public function __construct(
        public Occurrence $occurrenceRec, 
        public BackgroundJob $backgroundJob, 
        public PlantingInstruction $plantingInstruction,
        public Experiment $experiment,
        public EntryOrderModel $entryOrderModel)
    {
        
    }

    /**
     * Updates the status of the experiment after background process is done
     * @param Array $experimentStatus Status of the experiment
     * @param Array $excludedStatusesArr List of Experiment that should not be included in the update
     * @param String $method Experiment step of the process
     * @param Integer $entityDbId background job entity ID
     */
    public function updateNotifExperimentStatus($experimentStatus,$excludedStatusesArr,$method,$entityDbId){
        $createController = \Yii::$container->get('app\modules\experimentCreation\controllers\CreateController');
        if(strpos($method, 'entry') !== false && !in_array($experimentStatus, $excludedStatusesArr)){
            $action = 'specify-entry-list';
            $createController->actionCheckStatus($entityDbId, $action);

            //check entry list
            $this->entryOrderModel->entryListAfterUpdate($entityDbId);
        }elseif(strpos($method, 'cross') !== false && !in_array($experimentStatus, $excludedStatusesArr)){
            $action = 'manage-crosses';
            $createController->actionCheckStatus($entityDbId, $action);
        }elseif(strpos($method, 'experiment_block') !== false && !in_array($experimentStatus, $excludedStatusesArr)){
            //For planting arrangement
            $action = 'cross-design';
            $createController->actionCheckStatus($entityDbId, $action);
        }elseif(strpos($method,'plot') !== false && !in_array($experimentStatus, $excludedStatusesArr)){
            //Deletion of plots
            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);

                if(in_array("design generated", $status)){
                    $index = array_search('design generated', $status);
                    
                    if(isset($status[$index])){
                        unset($status[$index]);
                        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                    }
                    $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                }
            }
        }elseif(strpos($method,'occurrence') !== false && !in_array($experimentStatus, $excludedStatusesArr)){
            $occurrenceRecord = $this->occurrenceRec->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals ".$entityDbId]);
            $firstOccurrenceId = isset($occurrenceRecord['data'][0]['occurrenceDbId']) ? $occurrenceRecord['data'][0]['occurrenceDbId'] : 0;
            $plotRecord = $this->occurrenceRec->searchAllPlots($firstOccurrenceId, null, 'limit=1&sort=plotDbId:desc', false)['totalCount'];
            $plantingInsRecord = $this->plantingInstruction->searchAll(['occurrenceDbId' => "equals $firstOccurrenceId"], 'limit=1&sort=plantingInstructionDbId:desc', false)['totalCount'];
            
            //Creation of occurrrence records (plots and planting instructions)
            if($plotRecord != 0 && $plantingInsRecord != 0){
                //Reset the experiment statsus
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
                if(!in_array("occurrences created", $status)){
                    $updatedStatus = implode(';', $status);
                    $model['experimentStatus'] = $updatedStatus;
                    $newStatus = !empty($model['experimentStatus']) ? $model['experimentStatus'].';'."occurrences created" : "occurrences created";
                    $model['experimentStatus'] = $newStatus;
                    $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                }
            }else{
                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);
    
                    if(in_array("occurrences created", $status)){
                        $index = array_search('occurrences created', $status);
                        
                        if(isset($status[$index])){
                            unset($status[$index]);
                            
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', $status);
                            }
                        }
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                }
            }
        }
    }

    /**
     * Update the experiment the experiment status from the background job as it changes
     * @param Integer @userId User ID 
     */
    public function updateExptBackgroundStatus($userId){
        $params = [
            "creatorDbId" => "equals ".$userId,
            "application" => "equals EXPERIMENT_CREATION",
            "distinctOn"=> "entityDbId"
        ];

        $result = $this->backgroundJob->searchAll($params);
        if ($result["status"] == 200) {
            foreach($result['data'] as $record){
                //reflected = experiment status is updated based on bg status
                if($record['jobStatus'] == 'DONE' && $record['jobRemarks'] != 'REFLECTED'){
                   
                    $entityDbId = $record['entityDbId']; 
                    $method = $record['endpointEntity'];
                    $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$entityDbId"]);
                    $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';
                    
                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }
                   
                    //Remove the status related to background job process
                    $isUpdateFinalStat = false;
                    
                    if($this->partialArraySearch($status,'done') !== false){
                        $checkDoneStat = $this->partialArraySearch($status,'done');
                        unset($status[$checkDoneStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    
                    if($this->partialArraySearch($status,'in progress') !== false && $status[0] != 'creation in progress'){
                        //Remove the status related to background job process
                        $checkInProgressStat = $this->partialArraySearch($status,'in progress');
                        unset($status[$checkInProgressStat]);
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllInProgresStatus($newStatus);
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'failed') !== false){
                        $checkFailedIndex = $this->partialArraySearch($status,'failed');
                        unset($status[$checkFailedIndex]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllFailedStatus($newStatus);
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'in queue') !== false){
                        $checkInQueueIndex = $this->partialArraySearch($status,'in queue');
                        unset($status[$checkInQueueIndex]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllInQueueStatus($newStatus);
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'adding') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'adding');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'copying') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'copying');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'finalizing') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'finalizing');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = false;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"created"]);
                    }
                    if($this->partialArraySearch($status,'generation') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'generation');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            if($record['endpointEntity'] == 'OCCURRENCE'){
                                $addStatus = 'occurrences created';
                                if(!in_array($addStatus, $status)){
                                    $status[] = $addStatus;
                                }
                            }
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                      
                    if($isUpdateFinalStat){
                        $this->updateExperimentFinalStatus($record['entityDbId'], $method);
                    }
                    
                    //update the background, add REFLECTED to the remarks
                    $updateParam = [
                        "jobRemarks" => "REFLECTED",
                    ];
                    if($record['workerName'] == 'BuildCsvData') continue;
                    
                    $result = $this->backgroundJob->updateOne($record['backgroundJobDbId'], $updateParam);
                }else if($record['jobStatus'] != 'DONE' && $record['jobRemarks'] != 'REFLECTED'){
                    //Update the experiment 
                    $entityDbId = $record['entityDbId']; 
                    $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$entityDbId"]);
                    $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';
              
                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }
               
                    $backgroundStatus = $this->experiment->formatBackgroundWorkerStatus($record['backgroundJobDbId']);
                    
                    if(!in_array($backgroundStatus, $status) && ($backgroundStatus != 'DONE' || $backgroundStatus != 'FAILED' ) && !empty($backgroundStatus)){
                        $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$entityDbId"]);
                        $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';

                        if(!empty($experimentStatus)){
                            $status = explode(';',$experimentStatus);
                            $status = array_filter($status);
                        }else{
                            $status = [];
                        }

                        $updatedStatus = implode(';', array_unique($status));

                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $backgroundStatus : $backgroundStatus;

                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                        
                    } else if($record['jobStatus'] == 'FAILED'){
                        if($this->partialArraySearch($status,'failed') !== false){
                            $checkFailedIndex = $this->partialArraySearch($status,'failed');
                            unset($status[$checkFailedIndex]);
            
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', array_unique($status));
                            }
                            $isUpdateFinalStat = true;
                            $newStatus = $this->unsetAllFailedStatus($newStatus);
                            if($record['workerName'] == 'BuildCsvData') continue;
                            $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                        }

                        if($this->partialArraySearch($status,'in progress') !== false  && $status[0] != 'creation in progress'){
                            //Remove the status related to background job process
                            $checkInProgressStat = $this->partialArraySearch($status,'in progress');
                            unset($status[$checkInProgressStat]);
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', $status);
                            }
                            $isUpdateFinalStat = true;
                            $newStatus = $this->unsetAllInProgresStatus($newStatus);
                            if($record['workerName'] == 'BuildCsvData') continue;
                            $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                        }

                        if($this->partialArraySearch($status,'in queue') !== false){
                            //Remove the status related to background job process
                            $checkInProgressStat = $this->partialArraySearch($status,'in queue');
                            unset($status[$checkInProgressStat]);
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', $status);
                            }
                            $isUpdateFinalStat = true;
                            $newStatus = $this->unsetAllInQueueStatus($newStatus);
                            if($record['workerName'] == 'BuildCsvData') continue;
                            $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                        }


                        $updateParam = [
                            "jobRemarks" => "REFLECTED",
                        ];
                        if($record['workerName'] == 'BuildCsvData') continue;
                        
                        $result = $this->backgroundJob->updateOne($record['backgroundJobDbId'], $updateParam);
                    }
                    
                }else if($record['jobStatus'] == 'DONE' && $record['jobRemarks'] == 'REFLECTED'){ 
                    $entityDbId = $record['entityDbId']; 
                    $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$entityDbId"]);
                    $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';
                    
                    if(!empty($experimentStatus)){
                        $status = explode(';',$experimentStatus);
                        $status = array_filter($status);
                    }else{
                        $status = [];
                    }
                    
                    //Remove the status related to background job process
                    $isUpdateFinalStat = false;
                    
                    if($this->partialArraySearch($status,'done') !== false){
                        $checkDoneStat = $this->partialArraySearch($status,'done');
                        unset($status[$checkDoneStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    
                    if(($this->partialArraySearch($status,'in progress') !== false || $this->partialArraySearch($status,'in_progress') !== false)  && $status[0] != 'creation in progress'){
                        //Remove the status related to background job process
                        $checkInProgressStat = $this->partialArraySearch($status,'in progress');
                        unset($status[$checkInProgressStat]);
                        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllInProgresStatus($newStatus);
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'failed') !== false){
                        $checkFailedIndex = $this->partialArraySearch($status,'failed');
                        unset($status[$checkFailedIndex]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllFailedStatus($newStatus);
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    
                    if($this->partialArraySearch($status,'in queue') !== false){
                        $checkInQueueIndex = $this->partialArraySearch($status,'in queue');
                        unset($status[$checkInQueueIndex]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        $newStatus = $this->unsetAllInQueueStatus($newStatus);
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'adding') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'adding');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'copying') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'copying');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    if($this->partialArraySearch($status,'finalizing') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'finalizing');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = false;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"created"]);
                    }
                    if($this->partialArraySearch($status,'generation') !== false){
                        $checkAddStat = $this->partialArraySearch($status,'generation');
                        unset($status[$checkAddStat]);
        
                        if(empty($status)){
                            $newStatus = "draft";
                        }else{
                            if($record['endpointEntity'] == 'OCCURRENCE'){
                                $addStatus = 'occurrences created';
                                if(!in_array($addStatus, $status)){
                                    $status[] = $addStatus;
                                }
                            }
                            $newStatus = implode(';', $status);
                        }
                        $isUpdateFinalStat = true;
                        if($record['workerName'] == 'BuildCsvData') continue;
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                    }
                    
                    if($isUpdateFinalStat){
                        $this->updateExperimentFinalStatus($record['entityDbId'], $record['endpointEntity']);
                    }
                }
            }
        }
    }

    /** 
     * For a partial match you can iterate the array and use a string search function like strpos().
     * @param Array $arr String array
     * @param String $keyword keyword to search
    */  
    public function partialArraySearch($arr, $keyword) {
        $index = false;
        for($i=0; $i < count($arr); $i++){
            $checking = strpos($arr[$i], $keyword);
            if($checking !== false){
                $index = $i;
            }
        }
  
        return $index;
    }

    /**
     * Status update of the background job for the occurrence step
     * @param Integer $id Experiment ID
     * @param Array $result Api call for the background job endpoint
     */
    public function bgOccurrenceStatusUpdate($id, $result){
        if(isset($result['data'][0]) && $result['data'][0]['jobStatus'] == 'DONE'){
            $entityDbId = $result['data'][0]['entityDbId'];
            $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$id"]);
            $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
            

                    
            if($this->partialArraySearch($status,'done') !== false){
                $checkDoneStat = $this->partialArraySearch($status,'done');
                unset($status[$checkDoneStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    if($result['data'][0]['endpointEntity'] == 'OCCURRENCE' && $result['data'][0]['workerName'] == 'ExperimentCreationProcessor'){
                        $addStatus = 'occurrences created';
                        if(!in_array($addStatus, $status)){
                            $status[] = $addStatus;
                        }
                    }
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            
            if($this->partialArraySearch($status,'in progress') !== false){
                //Remove the status related to background job process
                $checkInProgressStat = $this->partialArraySearch($status,'in progress');
                unset($status[$checkInProgressStat]);
                
                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllInProgresStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'failed') !== false){
                $checkFailedIndex = $this->partialArraySearch($status,'failed');
                unset($status[$checkFailedIndex]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllFailedStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            
            if($this->partialArraySearch($status,'in queue') !== false){
                $checkInQueueIndex = $this->partialArraySearch($status,'in queue');
                unset($status[$checkInQueueIndex]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllInQueueStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'adding') !== false){
                $checkAddStat = $this->partialArraySearch($status,'adding');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'copying') !== false){
                $checkAddStat = $this->partialArraySearch($status,'copying');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'finalizing') !== false){
                $checkAddStat = $this->partialArraySearch($status,'finalizing');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"created"]);
            }
            if($this->partialArraySearch($status,'generation') !== false){
                $checkAddStat = $this->partialArraySearch($status,'generation');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    if($result['data'][0]['endpointEntity'] == 'OCCURRENCE' && $result['data'][0]['workerName'] == 'ExperimentCreationProcessor'){
                        $addStatus = 'occurrences created';
                        if(!in_array($addStatus, $status)){
                            $status[] = $addStatus;
                        }
                    }
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($isUpdateFinalStat = true){ //Deletion of plot and/or planting instruction records is successful
                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);

                    if(in_array("occurrences created", $status)){
                        $index = array_search('occurrences created', $status);
                        
                        if(isset($status[$index])){
                            unset($status[$index]);
                            
                            if(empty($status)){
                                $newStatus = "draft";
                            }else{
                                $newStatus = implode(';', $status);
                            }
                        }
                        //Update the experiment status since the removal of plot are planting instruction records have been successfully executed
                        $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);

                        //Update the experiment notes to remove plantingArrangementChanged, empty the notes
                        $this->experiment->updateOne($id, ["notes" => null]);
                        
                        //return is success=>true since there is no need the background job after
                        return json_encode(['success'=>true]);  
                    }else{
                        //return is true, which means that there's still a need to check the background job
                        return json_encode(true);
                    }
                }
            }
        }else{
            
            if($result['totalCount'] != 0){
                $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"$id"]);
                $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';
    
                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);
                }else{
                    $status = [];
                }

                if($this->partialArraySearch($status,'failed') !== false){
                    return json_encode(['success' => false, 'status'=>'failed' ,'message'=>$result['data'][0]['message']]);
                }else{
                    //return is true, which means that there's still a need to check the background job
                    return json_encode(true);
                }
            }else{
                return json_encode(false);
            } 
        }
    }

    /**
     * Status update of the background job for the generation of plot records
     * @param Integer $id Experiment ID
     * @param Array $result Api call for the background job endpoint
     */
    public function plotRecordStatusUpdate($id, $result){

        if(isset($result['data'][0]) && $result['data'][0]['jobStatus'] == 'DONE'){
            $entityDbId = $result['data'][0]['entityDbId']; 
            $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"equals $id"]);
            
            $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
                    
            if($this->partialArraySearch($status,'done') !== false){
                $checkDoneStat = $this->partialArraySearch($status,'done');
                unset($status[$checkDoneStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    if($result['data'][0]['endpointEntity'] == 'OCCURRENCE' && $result['data'][0]['workerName'] == 'ExperimentCreationProcessor'){
                        $addStatus = 'occurrences created';
                        if(!in_array($addStatus, $status)){
                            $status[] = $addStatus;
                        }
                    }
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            
            if($this->partialArraySearch($status,'in progress') !== false){
                //Remove the status related to background job process
                $checkInProgressStat = $this->partialArraySearch($status,'in progress');
                unset($status[$checkInProgressStat]);
                
                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllInProgresStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'failed') !== false){
                $checkFailedIndex = $this->partialArraySearch($status,'failed');
                unset($status[$checkFailedIndex]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllFailedStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            
            if($this->partialArraySearch($status,'in queue') !== false){
                $checkInQueueIndex = $this->partialArraySearch($status,'in queue');
                unset($status[$checkInQueueIndex]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $newStatus = $this->unsetAllInQueueStatus($newStatus);
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'adding') !== false){
                $checkAddStat = $this->partialArraySearch($status,'adding');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'copying') !== false){
                $checkAddStat = $this->partialArraySearch($status,'copying');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }
            if($this->partialArraySearch($status,'finalizing') !== false){
                $checkAddStat = $this->partialArraySearch($status,'finalizing');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"created"]);
            }
            if($this->partialArraySearch($status,'generation') !== false){
                $checkAddStat = $this->partialArraySearch($status,'generation');
                unset($status[$checkAddStat]);

                if(empty($status)){
                    $newStatus = "draft";
                }else{
                    if($result['data'][0]['endpointEntity'] == 'OCCURRENCE' && $result['data'][0]['workerName'] == 'ExperimentCreationProcessor'){
                        $addStatus = 'occurrences created';
                        if(!in_array($addStatus, $status)){
                            $status[] = $addStatus;
                        }
                    }
                    $newStatus = implode(';', $status);
                }
                $isUpdateFinalStat = true;
                $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
            }

            if($isUpdateFinalStat = true){ //Adding of plot and/or planting instruction records successful
                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);

                    $occurrences = $this->occurrenceRec->searchAll(["fields"=>"occurrence.id AS occurrenceDbId|occurrence.experiment_id AS experimentDbId",'experimentDbId' => "equals $id"]);
                    $firstOccurrenceId = isset($occurrences['data'][0]['occurrenceDbId']) ? $occurrences['data'][0]['occurrenceDbId'] : 0;
                    
                    $plotRecord = $this->occurrenceRec->searchAllPlots($firstOccurrenceId, null, 'limit=1&sort=plotDbId:desc', false)['totalCount'];
                    $plantingInsRecord = $this->plantingInstruction->searchAll(['occurrenceDbId' => "equals $firstOccurrenceId"], 'limit=1&sort=plantingInstructionDbId:desc', false)['totalCount'];

                    if($plotRecord > 0 && $plantingInsRecord > 0){
                        if(!in_array('occurrences created', $status)){
                            $updatedStatus = implode(';', $status);
                            $newStatus = !empty($updatedStatus) ? $updatedStatus.';'."occurrences created" : "occurrences created";
                            
                            $this->experiment->updateOne($entityDbId, ["experimentStatus"=>"$newStatus"]);
                            //This return means that the updates have been in-placed and no need to check the background job
                            return json_encode(['success'=>true]);  
                        }
                    }else if($plotRecord > 0 && $plantingInsRecord == 0){
                        //planting instruction needs to be created
                        return json_encode(['success'=>true,'createPlantingInstruction'=>true]);
                    }else{
                        //False means, the background job needs to be checked for updates
                        return json_encode(false);
                    }
                }else{
                    //False means, the background job needs to be checked for updates
                    return json_encode(false);
                }
            }
        }else{
            if($result['totalCount'] == 0){
                return json_encode(true);
            }else{
                $experimentRecord = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|experiment.experiment_status AS experimentStatus","experimentDbId"=>"equals $id"]);
                $experimentStatus = isset($experimentRecord['data'][0]['experimentStatus']) ? $experimentRecord['data'][0]['experimentStatus'] : '';
    
                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);
                }else{
                    $status = [];
                }
                
                if($this->partialArraySearch($status,'failed') !== false || $result['data'][0]['jobStatus'] == 'FAILED'){
                    $this->unsetAllInProgresStatus($status);
                    $backgroundStatus = $this->experiment->formatBackgroundWorkerStatus($result['data'][0]['backgroundJobDbId']);
                
                    if(!in_array($backgroundStatus, $status) && ($backgroundStatus != 'DONE' || $backgroundStatus != 'FAILED' ) && !empty($backgroundStatus)){
                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $backgroundStatus : $backgroundStatus;
                        
                        $this->experiment->updateOne($id, ["experimentStatus"=>"$newStatus"]);
                    }
                    return json_encode(['success' => false, 'status'=>'failed','message'=>$result['data'][0]['message']]);
                }else{
                    //False means, the background job needs to be checked for updates
                    return json_encode(false);
                }
            }
        }
    }


    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllInProgresStatus($experimentStatus){
        $experimentStatus = str_replace(';adding of entries in progress','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of entries in progress','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entries in progress','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of plots in progress','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of occurrences in progress','',$experimentStatus);
        $experimentStatus = str_replace(';randomization in progress','',$experimentStatus);
        $experimentStatus = str_replace(';adding of occurrences in progress','',$experimentStatus);
        $experimentStatus = str_replace(';copying of experiment in progress','',$experimentStatus);
        $experimentStatus = str_replace(';generation of occurrences in progress','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entry_lists in progress','',$experimentStatus);
        $experimentStatus = str_replace(';updating of crosses in progress','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of crosses in progress','',$experimentStatus);

        return $experimentStatus;
    }

    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllInQueueStatus($experimentStatus){ 
        $experimentStatus = str_replace(';deletion of entries in queue','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entries in queue','',$experimentStatus);
        $experimentStatus = str_replace(';adding of entries in queue','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of plots in queue','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of occurrences in queue','',$experimentStatus);
        $experimentStatus = str_replace(';adding of occurrences in queue','',$experimentStatus);
        $experimentStatus = str_replace(';copying of experiment in queue','',$experimentStatus);
        $experimentStatus = str_replace(';generation of occurrences in queue','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entry_lists in queue','',$experimentStatus);
        $experimentStatus = str_replace(';updating of crosses in queue','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of crosses in queue','',$experimentStatus);

        return $experimentStatus;
    }

    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllFailedStatus($experimentStatus){
        $experimentStatus = str_replace(';adding of entries failed','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of entries failed','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entries failed','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of plots in failed','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of occurrences in failed','',$experimentStatus);
        $experimentStatus = str_replace(';randomization failed','',$experimentStatus);
        $experimentStatus = str_replace(';adding of occurrences failed','',$experimentStatus);
        $experimentStatus = str_replace(';copying of experiment failed','',$experimentStatus);
        $experimentStatus = str_replace(';generation of occurrences failed','',$experimentStatus);
        $experimentStatus = str_replace(';updating of entry_lists failed','',$experimentStatus);
        $experimentStatus = str_replace(';updating of crosses failed','',$experimentStatus);
        $experimentStatus = str_replace(';deletion of crosses failed','',$experimentStatus);

        return $experimentStatus;
    }


    /**
     * Update the experiment status once the background process is done
     * @param integer $entityDbId Experiment ID
     * @param string $method Experiment step of the process
     */
    public function updateExperimentFinalStatus($entityDbId, $method){
        $method = strtolower($method);

        $experiment = $this->experiment->searchAll(['fields'=>'experiment.id as experimentDbId|experiment.experiment_status as experimentStatus','experimentDbId' => "equals ".$entityDbId]);
        $experimentStatus = isset($experiment['data'][0]) ? $experiment['data'][0]['experimentStatus'] : '';
                
        $excludedStatusesArr = ['mapped', 'created', 'planted', 'planted;crossed'];
        $this->updateNotifExperimentStatus($experimentStatus,$excludedStatusesArr,$method,$entityDbId);
    }

    /**
     * Set filters to api format
     * 
     * @param array $params search filter parameters e.g dashboard filters
     */
    public function formatFilters($params = null){
      
        $filters = []; // initialize filters
        $delimiter = "|";

        foreach ($params as $key => $value) {
            
            $value = ($value == "'") ? "''" : $value;
            if(isset($value) && $value !== ''){
                $value = is_array($value) ? implode($delimiter, array_map(function($string) use ($key){
                    if($key == 'experimentStatus'){
                        $string = $string == 'created' ? $string : "%$string";
                    }else{
                        $string = "%$string%";
                    }
                    return $string;
                }, $value)) : trim($value);

                if($key == 'occurrenceStatus'){
                    // occurrences status does not contain pack
                    if (strpos($value, 'pack') === false){
                        $semiColon = '';
                        $percent = '%';
                    }else{
                        $semiColon = ';';
                        $percent = '';
                    }
                    
                    $value = '%'.$semiColon.$value.$percent;
                }

                if($key == 'collaboratorPermissions'){
                    $value = '%'.$value.'%';
                }

                $key = ($key == 'year') ? 'experimentYear' : str_replace('_id', 'DbId' , $key);
                $filters[$key] = $value;

                if(strpos($key, 'DbId') !== false ){
                    if(str_contains($value, "|")){
                        $tempValue = explode("|", $value);
                    } else {
                        $tempValue = $value;
                    }
                    if (is_array($tempValue)) {
                        array_walk($tempValue, function ($valKey, $key) {
                            if (
                                $valKey != '' &&
                                !str_contains(strtolower($valKey), 'equals') &&
                                !str_contains(strtolower($valKey), '%')
                            ){
                                $valKey = "equals $valKey";
                            }
                        });
                        $val = implode('|', $tempValue);
                    }  else {
                        if (
                            $value != '' &&
                            !str_contains(strtolower($value), 'equals') &&
                            !str_contains(strtolower($value), '%')
                        ){
                            $val = "equals $value";
                        } else {
                            $val = $value;
                        }
                    }
                    $filters[$key] = $val;
                }
            }
        }
        return $filters;
    }

    /**
     * Api call for remove draft experiments
     * 
     * @param integer $id experiment record id
     */
    public function deleteDraftExperiment($id){
     
        $params = [
            "isDraftedExperiment" => true,
            "entity" => "EXPERIMENT",
            "endpointEntity" => "EXPERIMENT",
            "application" => "EXPERIMENT_CREATION"
        ];

        return $this->experiment->deleteMany([$id],true,$params);

    }

}
