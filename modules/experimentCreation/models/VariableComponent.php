<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
* Model class for variable component
*/

namespace app\models;
namespace app\modules\experimentCreation\models;

use app\models\Experiment;
use app\models\ExperimentGroupExperiment;
use app\models\ExperimentGroup;
use app\models\Season;
use app\models\Variable;
use app\models\Program;
use app\models\Project;


use yii\helpers\ArrayHelper;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\select2\Select2;
use Yii;


class VariableComponent extends \yii\base\BaseObject
{

    public function __construct (
        public Variable $variable,
    ) { }
    /**
     * Get field for specific variable
     * 
     * @param array $varRecord array containing variable information
     */
    public function generateDesignField($varRecord, $addtlClass, $key=null, $new=false, $formName=null){
       
        $variableRecord = $this->variable->getVariableByAbbrev($varRecord['name']);
        
        $variablesArr['id'] = ($variableRecord == NULL) ? NULL : $variableRecord['variableDbId'];
        $variablesArr['label'] = $varRecord['variableLabel'];

        $defVal = (isset($varRecord['value'])) ? $varRecord['value'] : $varRecord['ui']['defaultValue'];
        $variablesArr['abbrev'] = $varRecord['name'];
        $variablesArr['code'] = $varRecord['code'];
        $value = $defVal;

        $varId = $varRecord['code'];
        $name = ($formName == null) ? 'ExperimentDesign['.$varId.']':$formName.'['.$varId.']';
        $labelStr = ucwords(strtolower($varRecord['variableLabel']));
        $label = Html::label($labelStr,$varId, ['data-label'=>ucwords(strtolower($variablesArr['label']))]);
        $size = ($new == true) ?' col col-md-5':' col col-md-10';

        $size = ($formName == 'Additional') ? ' col col-md-3': $size;
        $disabled = (isset($varRecord['ui']['disabled'])) ? $varRecord['ui']['disabled'] : false;
        $readOnly = (isset($varRecord['ui']['readonly']) && in_array($varRecord['code'], ['nTrial', 'nFarm'])) ? $varRecord['ui']['readonly'] : false;
        $title = array();
        if ($varRecord['description'] != null) {
            $title[] = $varRecord['description'];
        }
        if ($varRecord['ui']['minimum'] != null) {

            $min = $varRecord['ui']['minimum'];
            $title[] = "Minimum value = ".$min;
        } else {

            $min = "null";
        }

        if ($varRecord['ui']['maximum'] != null) {

            $max = $varRecord['ui']['maximum'];
            $title[] = "Maximum value = ".$max;
        } else {

            $max = "null";
        }

        $titleStr = '';

        if(count($title) > 0){
        
            $titleStr = implode('; ', $title);
        } else{
        
            $titleStr = "Fill in the required field.";
        }

        
        if($varRecord['name'] == 'COL'){
        
            $disabled = true;
        }

        if($varRecord['ui']['catalogue'] == true){
          
            if($defVal == '' || $defVal == NULL || $defVal == 'false'){
        
                $defVal = NULL;
            }
            
            $classCompute = '';

            $allowedIndex = array_search('allowed-value,', array_column($varRecord['rules'], 'type'));

            if(isset($varRecord['rules'][$allowedIndex]['expression'])){
                $classCompute = 'computeFormula';
                $value = Select2::widget([
                    'name' => $name,
                    'id' => $varId,
                    'data' => isset($varRecord['ui']['values']) ? $varRecord['ui']['values']: [],
                    'value'=>$defVal,
                    'disabled'=>$disabled,
                    'options' => [
                        'placeholder' => 'Select a value',
                        'tags' => true,
                        'class'=> $classCompute.' '.$addtlClass,
                        'title' => $titleStr,
                    ],
                    'pluginEvents'=>[
                        "change" => "function() { $('#'+this.id+'-hidden').val($(this).val()); }",
                        "select2:opening" => "function() { computeFormula(this.id); }",
                    ]
                ]);
            } else {
                if(strtolower($varId) == 'serpentineprep' || strtolower($varId) == 'serpentine'){
                    $noImage = '<img src="../../../images/column-order.png" width="20px" height="20px"/>';
                    $yesImage = '<img src="../../../images/column-serpentine.png" width="20px" height="20px"/>';
                    $dataValues = [
                        "CS"=>" Column Serpentine",
                        "C0"=> " Column Order",
                        "RS"=> " Row Serpentine",
                        "RO"=> " Row Order"
                    ];
                } else if(strtolower($varId) == 'pcheck'){
                    $dataValues = [
                        "5" => "5",
                        "6" => "6",
                        "7" => "7",
                        "8" => "8",
                        "9" => "9",
                        "10" => "10",
                        "11" => "11",
                        "12" => "12",
                        "13" => "13",
                        "14" => "14",
                        "15" => "15",
                        "20" => "20"
                    ];
                } else {
                    $dataValues = isset($varRecord['ui']['values']) ? $varRecord['ui']['values']: [];
                    
                }

                $value = Select2::widget([
                    'name' => $name,
                    'id' => $varId,
                    'data' => $dataValues,
                    'value'=>$defVal,
                    'disabled'=>$disabled,
                    'options' => [
                        'placeholder' => 'Select a value',
                        'tags' => true,
                        'class'=> $classCompute.' '.$addtlClass,
                        'title' => $titleStr,
                    ],
                    'pluginEvents'=>[
                        "change" => "function() { $('#'+this.id+'-hidden').val($(this).val()); }",
                    ]
                ]);
            }
        
            if(in_array($varRecord['dataType'], ['integer', 'smallint', 'numeric', 'bigint'])){
                $hidden = Html::input('number',$name.'-hidden', !empty($defVal) ? $defVal:null, ['id'=>$varId.'-hidden','min'=>$min, 'max'=>$max,'class'=>'numeric-input-validate-hidden hidden '. $addtlClass, "readonly"=>$readOnly]);
            } else{
                $hidden = Html::input('text',$name.'-hidden', !empty($defVal) ? $defVal:null, ['id'=>$varId.'-hidden','class'=>'select2-input-validate-hidden hidden '. $addtlClass]);
            }
            
            $variablesArr['value']=Html::tag('div',$label.$value.$hidden, ['class'=>$size, 'style'=>'margin-bottom:5px;']);

        } else if(in_array($varRecord['dataType'], ['boolean'])){
            
            $noteLayout = '';
            if($varRecord['code'] == 'genLayout'){
                $noteLayout = '<p class="layout-note">NOTE: This will show the panel for specifying field layout details.</p>';
            }

            //get default values
            $noImage = '';
            $yesImage = '';
            if(isset($varRecord['ui']['values']) && count($varRecord['ui']['values']) > 0){
                $labelSwitch = array_values($varRecord['ui']['values']);
            } else if(strtolower($varId) == 'vserpentine'){
                $noImage = '&nbsp;<img src="../../../images/hserpentine.png" width="20px" height="20px">';
                $yesImage = '&nbsp;<img src="../../../images/vserpentine.png" width="20px" height="20px">';
                $labelSwitch = [
                  'Horizontal ',
                  'Vertical '
                ];
            } else if(strtolower($varId) == 'serpentine'){
                $noImage = '&nbsp;<img src="../../../images/column-order.png" width="20px" height="20px">';
                $yesImage = '&nbsp;<img src="../../../images/column-serpentine.png" width="20px" height="20px">';
                $labelSwitch = [
                  'Plot Order ',
                  'Serpentine '
                ];
            } else {
                $labelSwitch = [
                  'No',
                  'Yes'
                ];
            }

            if(empty($defVal) || in_array($defVal."", ["false", "FALSE", "F"])){
                $defVal = false;
                $arraySet = ['id'=>$varId, 'class'=>'boolean-input-validate '.$addtlClass, 'disabled'=>$disabled];
            } 
            else if(in_array($defVal."", ["true", "TRUE", "T"])){
                $arraySet = ['id'=>$varId, 'class'=>'boolean-input-validate '.$addtlClass, 'disabled'=>$disabled, 'checked'=>'checked'];
            } 
            // else if(in_array(ucwords($defVal), $labelSwitch)){

            // } 
            else {
                // $defVal = true;
                $arraySet = ['id'=>$varId, 'class'=>'boolean-input-validate '.$addtlClass, 'disabled'=>$disabled];
            }
            
            

            $checkedStr = '';
            if(strtolower($defVal) == strtolower($labelSwitch[1])){
                $arraySet["checked"] = "checked";
            }

            $value = Html::input('checkbox', $name, '',$arraySet );
            $span = $labelSwitch[0].'&nbsp;'.$noImage.$value.Html::tag('span', '', ['class'=>'lever ']).$labelSwitch[1].'&nbsp;'.$yesImage;
            $labelName = Html::label($label, $variablesArr['abbrev'],['for'=>$varId, 'class'=>'cbx-label ', 'title' => $titleStr, 'style'=>'font-size: 1rem;color: #9e9e9e;font-weight: bold;']);
            $label = '<div class="row switch">'.Html::tag('label',$span).$noteLayout.'</div>';
            $variablesArr['value']=Html::tag('div',$labelName.$label, ['class'=>$size, 'style'=>'margin-top:5px;']);

        } 
        else if(in_array($varRecord['dataType'], ['integer', 'smallint', 'numeric', 'bigint'])){
            if($min == "null"){
                $min = 0;
            }
            if($disabled || $readOnly){
                $disabled = true;                
            }
           
            $value=Html::input('number', $name, $defVal, ['id'=>$varId, 'class'=>' numeric-input-validate '. $addtlClass, "readonly"=>$disabled,'min'=>$min, 'max'=>$max, 'title' => $titleStr]);
            
            $variablesArr['value']=Html::tag('div',$label.$value, ['class'=>'input-field'.$size]);

        }
        else if(in_array($varRecord['dataType'], ['float'])){

            $value = Html::input('number', $name, $defVal, ['id'=>$varId,'min'=>'0', 'step'=>'any', 'class'=>'float-input-validate '. $addtlClass, 'title' => $titleStr,'disabled'=>$disabled,]);
            $variablesArr['value']=Html::tag('div',$value.$label, ['class'=>'input-field'.$size]);
        } 
        else if(in_array($varRecord['dataType'], ['name', 'character varying'])){
            
            if(isset($varRecord['values']) && count($varRecord['values']) > 1){
        
                $value = Select2::widget([
                    'name' => $name,
                    'id' => $varId,
                    'data' => $varRecord['values'],
                    'value'=>$defVal,
                    'options' => [
                        'tags' => true,
                        'class'=>'select2-Drop '. $addtlClass,
                        'title' => $titleStr
                    ],
                    'pluginEvents'=>[
                        "change" => "function() { $('#'+this.id+'-hidden').val($(this).val()); }",
                    ]
                ]);
                
                $hidden = Html::input('text',$name.'-hidden', !empty($defVal) ? $defVal:null, ['id'=>$varId.'-hidden','class'=>'select2-input-validate-hidden hidden '. $addtlClass]);
                $variablesArr['value'] = $label.$value;

            } else{

                $value = Html::input('text', $name, $defVal, ['id'=>$varId, 'class'=>'text-input-validate '. $addtlClass, 'title' => $titleStr,'disabled'=>$disabled,]);
                $variablesArr['value']=Html::tag('div',$value.$label, ['class'=>'input-field'.$size]);
            }
        }

        $variablesArr['rawValue'] = $value;
        $variablesArr['rawLabel'] = $label;
        $variablesArr['defaultValue'] =$defVal;
        return $variablesArr;
    }

    /**
    * Returns the array of variables that can be updated in augmented design
    */
    public function getDesignConfig(){
        //retrieve configuration: For now hardcoded since it will not change
        $configData = [
              [
                "default" => "test",
                "disabled" =>  false,
                "required" =>  "required",
                "target_value" =>  "",
                "target_column" =>  "",
                "variable_type" =>  "identification",
                "allowed_values" =>  [
                  "ENTRY_ROLE_REPEATED_CHECK",
                  "ENTRY_ROLE_SPATIAL_CHECK"
                ],
                "variable_abbrev" =>  "ENTRY_ROLE",
                "api_resource_sort" =>  "",
                "api_resource_filter" =>  "",
                "api_resource_method" =>  "",
                "api_resource_endpoint" =>  "entries",
                "secondary_target_column" =>  "",
                "required" => "required"
              ],
              [
                "disabled" =>  false,
                "target_value" =>  "",
                "target_column" =>  "",
                "variable_type" =>  "identification",
                "variable_abbrev" =>  "ENTRY_CLASS",
                "api_resource_sort" =>  "",
                "api_resource_filter" =>  "",
                "api_resource_method" =>  "",
                "api_resource_endpoint" =>  "entries",
                "secondary_target_column" =>  "",
                "required" => "required"
              ],
              [
                "disabled" =>  false,
                "target_value" =>  "",
                "target_column" =>  "",
                "variable_type" =>  "identification",
                "display_name" => "No. of Reps",
                "variable_abbrev" =>  "TIMES_REP",
                "api_resource_sort" =>  "",
                "api_resource_filter" =>  "",
                "api_resource_method" =>  "",
                "api_resource_endpoint" =>  "",
                "secondary_target_column" =>  "",
                "default" => 2,
                "min_value" => 2,
                "required" => "required"
              ]
        ];

        return $configData;
    }

    
    /**
     * FOR REFACTORING
     * Form experiment group UI
     * 
     * @param integer $exptGroupId primary key of the experiment group
     * @param integer $expId primary key of the experiment
     */
    public static function formExptGrpTable($exptGroupId, $expId){

        $tableInfo = array();
        $tableInfo['expGrpId'] = $exptGroupId;
        $exptGrpRec = ExperimentGroup::findOne($exptGroupId);
        $tableInfo['expGrpType'] = $exptGrpRec->experiment_group_type;
        $tableInfo['expGrpTypePattern'] = (isset($exptGrpRec->type_pattern) && $exptGrpRec->type_pattern != '') ? ' ('.Yii::t('app', $exptGrpRec->type_pattern).')' : '';
        $tableInfo['tableId'] = 'table-grid-'.$exptGroupId;
        $tableInfo['experimentGrp'] = ' ('.$exptGrpRec->abbrev.')';
        $tableInfo['experimentGrpName'] = $exptGrpRec->name;
        $experimentIdsRec = ExperimentGroupExperiment::find()->where(['experiment_group_id'=>$exptGroupId, 'is_void'=>false])->all();
        $experimentIds = array_column($experimentIdsRec, 'experiment_id');

        //form data provider for gridview
        $sql = "
            select 
                eg.id,
                eg.order_number,
                eg.experiment_role,
                eg.experiment_id,
                (select e.experiment_name from operational.experiment e where e.id = eg.experiment_id LIMIT 1) as experiment_name,
                (select e.abbrev from operational.experiment_group e where e.id = eg.experiment_group_id LIMIT 1) as experiment_group
                ";
        $from = " from
                operational.experiment_group_experiment eg
            where 
                eg.experiment_group_id = $exptGroupId
                and eg.is_void = false
                order by eg.id;
            ";
       
        $expGrpexpArr = Yii::$app->db->createCommand($sql.$from)->queryAll();
        $tempContentArr= ArrayHelper::map($expGrpexpArr, 'experiment_id', 'experiment_name');
        $contentArr = array();

        $data = [
            'test' => 'Test',
            'check' => 'Check'
        ];

        $label = '<div class="pull-right set-Field"><label class="label-role">Experiment Role</label>';

        foreach($tempContentArr as $key=>$value){

            $expGrpExp = ExperimentGroupExperiment::find()->where(['experiment_id'=>$key, 'experiment_group_id'=>$exptGroupId, 'is_void'=>false])->one();
        
            if($key == $expId){

                $contentArr[] = [
                    'content' => '<div id="'.$exptGroupId.'-'.$key.'" class="expId-class not-disabled">'.$value.$label.Editable::widget([
                        'name'=>'experiment_role_value', 
                        'value' =>isset($expGrpExp->experiment_role) ? $expGrpExp->experiment_role:null,
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => $data,
                        'displayValueConfig' => $data,
                        'id' => $exptGroupId.'-'.$key,
                        'size' => 'md',
                        'formOptions' => ['action' => ['/experimentCreation/create/update-record-column-expt-grp?type=experiment_role&id='.$key.'&expGrp='.$exptGroupId]],
                        'options' => ['class'=>'form-control experiment_role', 'style'=>'float:right;']
                    ]).'</div></div>',
                    'disabled' => true
                ];
            } else {
                
                $contentArr[] = [
                    'content' => '<div id="'.$exptGroupId.'-'.$key.'" class="expId-class">'.$value.$label.Editable::widget([
                        'name'=>'experiment_role_value', 
                        'value' => isset($expGrpExp->experiment_role) ? $expGrpExp->experiment_role:null,
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => $data,
                        'displayValueConfig' => $data,
                        'size' => 'md',
                        'formOptions' => ['action' => ['/experimentCreation/create/update-record-column-expt-grp?type=experiment_role&id='.$key.'&expGrp='.$exptGroupId]],
                        'options' => ['class'=>'form-control experiment_role']
                    ]).'</div></div>'
                ];
            }
        }
        $tableInfo['expGrpexp'] = $contentArr;
        
        $includedIds = array_column($expGrpexpArr, 'experiment_id');
        $includedStr = implode(',', $includedIds);
        $expRecord = Experiment::findOne($expId);

        $projectStr = '';
        
        if(isset($expRecord->project_id) && $expRecord->project_id != null){
        
            $projectStr = "and project_id = $expRecord->project_id";
        }

        $sql="
            SELECT
                id,
                experiment_name
            from
                operational.experiment
            where
                id not in ({$includedStr})
                {$projectStr}
                and year = $expRecord->year
                and season_id = $expRecord->season_id
                and is_void = false;
        ";

        $expNotIncluded = Yii::$app->db->createCommand($sql)->queryAll();
        $tempNotContentArr=  $tempContentArr= ArrayHelper::map($expNotIncluded, 'id', 'experiment_name');
        $notContentArr = [];
        $label = '<div class="pull-right set-Field hidden"><label class="label-role">Experiment Role</label>';

        foreach($tempNotContentArr as $key=>$value){

            $notContentArr[] = [
                'content' => '<div id="'.$exptGroupId.'-'.$key.'" class="expId-class">'.$value.$label.Editable::widget([
                    'name'=>'experiment_role_value', 
                    'value' => isset($expGrpExp->experiment_role) ? $expGrpExp->experiment_role:null,
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    'data' => $data,
                    'size' => 'md',
                    'formOptions' => ['action' => ['/experimentCreation/create/update-record-column-expt-grp?type=experiment_role&id='.$key.'&expGrp='.$exptGroupId]],
                    'options' => ['class'=>'form-control experiment_role']
                ]).'</div></div>'
            ];

        }
        
        $tableInfo['notExpGrpexp'] = $notContentArr;
        
        return $tableInfo;
        
    }

}