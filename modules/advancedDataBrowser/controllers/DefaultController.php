<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\advancedDataBrowser\controllers;

// Import Yii
use Yii;
// Import components
use app\components\B4RController;
// Import models

/**
 * Default controller for the `advancedDataBrowser` module
 */
class DefaultController extends B4RController
{
    /**
     * Controller constructor
     */
    public function __construct($id, $module,
        $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders a sample usage of the Advanced Data Browser Widget
     *
     * @return mixed render info
     */
    public function actionIndex() {

        // Renders browser
        return $this->render('index', [
        ]);
    }
}