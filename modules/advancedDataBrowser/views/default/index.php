<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* Renders browser and manage persons page
*/

use app\components\AdvancedDataBrowserWidget;

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

use app\dataproviders\ArrayDataProvider;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

use app\widgets\GermplasmInfo;

//all columns of germplasm catalog browser
$defaultColumns = [
    // [
    //     'label'=> "Checkbox",
    //     'header'=>'
    //         <input type="checkbox" class="filled-in" id="germplasm-select-all-id" ' . $selectAllButtonChecked . '/>
    //         <label for="germplasm-select-all-id"></label>
    //     ',
    //     'content'=>function($model) use ($selectedIds, $selectAllMode) {
    //         $checked = '';
    //         if($selectAllMode == 'include mode') {
    //             if(in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
    //         } else {    // exclude mode
    //             if(!in_array($model['germplasmDbId'], $selectedIds)) $checked = 'checked';
    //         }
    //         return '
    //             <input class="dynagrid-germplasm-grid_select filled-in" type="checkbox" id="'.$model['germplasmDbId'].'" '.$checked.'/>
    //             <label for="'.$model['germplasmDbId'].'"></label>
    //         ';
    //     },
    //     'hAlign' => 'center',
    //     'vAlign' => 'top',
    //     'hiddenFromExport' => true,
    //     'mergeHeader' => true,
    //     'order' => DynaGrid::ORDER_FIX_LEFT
    // ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => ' ',
        'template' => '{view}',
        'visibleButtons' => [
        ],
        'buttons' => [
            'view' => function ($url, $model) {
                return GermplasmInfo::widget([
                    'id' => $model['germplasmDbId'],
                    'label' => '<i class="material-icons view-germplasm-btn">remove_red_eye</i>',
                    'entityLabel' => $model['designation'],
                    'linkAttrs' => [
                        'id' => 'view-germplasm-widget-'.$model['germplasmDbId'],
                        'class' => 'blue-text text-darken-4 header-highlight',
                        'title' => \Yii::t('app', 'Click to view more information'),
                        'data-label' => $model['designation'],
                        'data-id' => $model['germplasmDbId'],
                        'data-target' => '#view-germplasm-widget-modal',
                        'data-toggle' => 'modal',
                        'style' => ['font-size' => '1rem']
                    ]
                ]);
            },
        ],
        'vAlign' => 'top'
    ],
    [
        'attribute'=>'designation',
        'label' => 'Germplasm',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute'=>'otherNames',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ],
        'value' => function($data) {
            $otherNames = $data['otherNames'];
            $otherNames = str_replace(";", "<br/>", $otherNames);
            return $otherNames;
        },
        'format' => 'raw'
    ],
    [
        'attribute'=>'parentage',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ]
    ],
    [
        'attribute'=>'generation',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmNameType',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmType',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmState',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'germplasmNormalizedName',
        'contentOptions'=>[
            'class' => 'germplasm-name-col'
        ],
        'visible' => false
    ],
    [
        'attribute'=>'germplasmCode',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
        'visible' => false
    ],
];

$otherColumns = [
    [
        'attribute'=>'creator',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'creationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
		'visible' => false,
		'format' => 'dateTime',
		'filterType' => GridView::FILTER_DATE,
		'filterWidgetOptions' => [
			'pluginOptions'=> [
				'format' => 'yyyy-mm-dd',
				'autoclose' => true,
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				'todayHighlight' => true,
				'showButtonPanel'=>true,
				'clearBtn' => true,
				'options'=>[
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				]
			],
			'pluginEvents' => [
				"clearDate" => 'function (e) {$(e.target).find("input").change();}',
			],
		]
    ],
    [
        'attribute'=>'modifier',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ]
    ],
    [
        'attribute'=>'modificationTimestamp',
        'contentOptions'=>[
            'style'=>'min-width:100px;'
        ],
		'visible' => false,
		'format' => 'dateTime',
		'filterType' => GridView::FILTER_DATE,
		'filterWidgetOptions' => [
			'pluginOptions'=> [
				'format' => 'yyyy-mm-dd',
				'autoclose' => true,
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				'todayHighlight' => true,
				'showButtonPanel'=>true,
				'clearBtn' => true,
				'options'=>[
				'type'=>DatePicker::TYPE_COMPONENT_APPEND,
				'removeButton'=>true,
				]
			],
			'pluginEvents' => [
				"clearDate" => 'function (e) {$(e.target).find("input").change();}',
			],
		],
        'value' => function($data) {
            if(is_null($data['modifier'])) return null;
            return $data['modificationTimestamp'];
        },
    ],
];

// set up columns for export and grid view
$otherGridColumns = [];
foreach ($otherColumns as $key => $value) {
    $visibleFalse = ['visible'=>false];

    $defaultOtherCols = array_merge($value,$visibleFalse);
    $otherGridColumns[] = $defaultOtherCols;
}

$columns = array_merge($defaultColumns, $otherGridColumns);

// $browserId = 'dynagrid-ec-experiment-browser';  // EC
// $browserId = 'dynagrid-em-occurrences'; // EM
// $browserId = 'dynagrid-cm-cross-list-grid'; // CM
// $browserId = 'dynagrid-pim-planting-job-transactions';  // PIM
// $browserId = 'dynagrid-harvest-manager-plot-hdata-grid';    // HM, sa loob pa
// $browserId = 'dynagrid-seed-search-search-grid';    // SS
// $browserId = 'dynagrid-im-grid';    // IM
// $browserId = 'dynagrid-list-manager-my-lists-grid'; // LM
// $browserId = 'dynagrid-germplasm-grid'; // GS
// $browserId = 'dynagrid-traits-grid';    // TS
$browserId = 'dynagrid-germplasm-sample-grid';
$endpoint = 'germplasm-search';

echo AdvancedDataBrowserWidget::widget([
    'baseUrl' => '/index.php/advancedDataBrowser/default/index?program=IRSEA',
    'browserId' => $browserId,
    'endpoint' => $endpoint,
    'columns' => $columns,
    'browserDescription' => 'sample browser',
    'defaultFilter' => ['names' => ['equals' => 'ir %']],
    'defaultSort' => ['designation' => SORT_ASC],
    // 'showPersonalize' => true,
]);

?>
index