<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\variable\controllers;
use app\components\B4RController;
use app\controllers\Dashboard;
use app\models\Variable;
use Yii;
/**
 * Default controller for the `Variable` module
 */
class DefaultController extends B4RController
{

    public function __construct($id, $module,
        protected Dashboard $dashboard,
        protected Variable $variable,
        $config = []) {

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders view more information of a variable in widget
     * @return mixed variable information
     */
    public function actionViewInfo(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;
        $entityId = isset($_POST['entity_id']) ? $_POST['entity_id'] : null;
        $entityType = isset($_POST['entity_type']) ? $_POST['entity_type'] : null;
        $entityLabel = isset($_POST['entity_label']) ? $_POST['entity_label'] : null;
        $program = isset($_POST['program']) ? $_POST['program'] : Yii::$app->userprogram->get('abbrev'); //current program

        if (empty($program)) {
            $defaultFilters = $this->dashboard->getFilters();
            $dashProgram = $defaultFilters->program_id;
        }

        $data = $this->variable->getVariableInfo($id,$entityId,$entityType);

        return Yii::$app->controller->renderAjax('@app/widgets/views/variable/tabs.php',[
            'data' => $data,
            'id' => $id,
            'entityLabel' => $entityLabel,
            'entityId' => $entityId,
            'program' => $program
        ]);
    }

}