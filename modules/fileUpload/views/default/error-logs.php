<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload error logs
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Define URLs
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);

// Data browser ID
$browserId = 'dynagrid-file-upload-error-log-grid';

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// Add breadcrumbs
echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/breadcrumbs.php',[
    "sourceToolName" => $sourceToolName,
    "fileUploadInfo" => $fileErrorLogInfo,
    "returnUrl" => $returnUrl,
    "title" => 'Error Logs'
]);

?>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
        // Add file info panel
        echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/file-info.php',[
            "fileUploadInfo" => $fileErrorLogInfo
        ]);

        // Render data browser
        $columns = $fileErrorLogInfo['columns'] ?? [];
        $dataProvider = $fileErrorLogInfo['dataProvider'] ?? [];

        // Render grid
        echo '<div style="padding-left:15px; padding-right:15px;">';
        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>'file-upload-error-log-grid-table'],
                'options'=>['id'=>'file-upload-error-log-grid-table-con'],
                'id' => 'file-upload-error-log-grid-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => ['id' => $browserId, 'enablePushState' => true,],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' => 'Review file upload error logs here. {summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [Yii::$app->controller->action->id, 'fileDbId'=>$fileDbId, 'returnUrl'=>$returnUrl],
                            [
                                'class' => 'btn btn-default',
                                'id' => 'file-upload-reset-plot-btn',
                                'style' => "margin-left:10px;",
                                'data-position' => 'top',
                                'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                'data-pjax' => true
                            ]
                        )
                        . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=> [
                'id' => $browserId
            ]
        ]);

        DynaGrid::end();
        echo '</div>';

    ?>
</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

    $("document").ready(function() {
        
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        refresh();

        // Reload grid on pjax success
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            refresh();
        });
    });

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);
        $('.dropdown-trigger').dropdown();
    }
JS;

$this->registerJs($script);

?>

