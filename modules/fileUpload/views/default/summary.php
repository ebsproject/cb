<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload summary
 */

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use app\components\PrintoutsWidget2;

// Define URLs
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$germplasmTabUrl = Url::to(['default/summary', 'fileDbId' => $fileDbId, 'returnUrl' => $returnUrl, 'sourceToolName' => $sourceToolName, 'configAbbrevPrefix' => $configAbbrevPrefix, 'entity' => 'germplasm']);
$seedTabUrl = Url::to(['default/summary', 'fileDbId' => $fileDbId, 'returnUrl' => $returnUrl, 'sourceToolName' => $sourceToolName, 'configAbbrevPrefix' => $configAbbrevPrefix, 'entity' => 'seed']);
$packageTabUrl = Url::to(['default/summary', 'fileDbId' => $fileDbId, 'returnUrl' => $returnUrl, 'sourceToolName' => $sourceToolName, 'configAbbrevPrefix' => $configAbbrevPrefix, 'entity' => 'package']);
$germplasmRelationTabUrl = Url::to(['default/summary', 'fileDbId' => $fileDbId, 'returnUrl' => $returnUrl, 'sourceToolName' => $sourceToolName, 'configAbbrevPrefix' => $configAbbrevPrefix, 'entity' => 'germplasm_relation']);
$germplasmAttributeTabUrl = Url::to(['default/summary', 'fileDbId' => $fileDbId, 'returnUrl' => $returnUrl, 'sourceToolName' => $sourceToolName, 'configAbbrevPrefix' => $configAbbrevPrefix, 'entity' => 'germplasm_attribute']);

// Check the source tab of the request
if (str_contains($returnUrl, 'create')) {
    $action = 'create';
} else if (str_contains($returnUrl, 'update')) {
    $action = 'update';
} else {
    $action = '';
}

// Data browser ID
$browserId = 'dynagrid-file-upload-summary-' . $action . '-' . $entity . '-grid';

// Browser configurations
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// Configure displayed tabs
$tabActive = [
    "germplasm" => $entity == "germplasm" ? "active" : "",
    "seed" => $entity == "seed" ? "active" : "",
    "package" => $entity == "package" ? "active" : "",
    "germplasm_relation" => $entity == "germplasm_relation" ? "active" : "",
    "germplasm_attribute" => $entity == "germplasm_attribute" ? "active" : ""
];
// Tabs array
$tabs = [
    "germplasm" => '<li class="tab col step s2 hm-tool-tab" id="search-tab">
        <a class="view-file-upload-entity-tab a-tab ' . $tabActive['germplasm'] . '" link="' . $germplasmTabUrl . '" href="' . $germplasmTabUrl . '" data-entity="germplasm">' . \Yii::t('app', 'Germplasm') . '</a>
    </li>',
    "seed" => '<li class="tab col step s2 hm-tool-tab" id="search-tab">
        <a class="view-file-upload-entity-tab a-tab ' . $tabActive['seed'] . '" link="' . $seedTabUrl . '" href="' . $seedTabUrl . '" data-entity="seed">' . \Yii::t('app', 'Seed') . '</a>
    </li>',
    "package" => '<li class="tab col step s2 hm-tool-tab" id="search-tab">
        <a class="view-file-upload-entity-tab a-tab ' . $tabActive['package'] . '" link="' . $packageTabUrl . '" href="' . $packageTabUrl . '" data-entity="package">' . \Yii::t('app', 'Package') . '</a>
    </li>',
    "germplasm_relation" => '<li class="tab col step s2 hm-tool-tab" id="search-tab">
        <a class="view-file-upload-entity-tab a-tab ' . $tabActive['germplasm_relation'] . '" link="' . $germplasmRelationTabUrl . '" href="' . $germplasmRelationTabUrl . '" data-entity="germplasm_relation">' . \Yii::t('app', 'Germplasm Relation') . '</a>
    </li>',
    "germplasm_attribute" => '<li class="tab col step s3 hm-tool-tab" id="search-tab">
    <a class="view-file-upload-entity-tab a-tab ' . $tabActive['germplasm_attribute'] . '" link="' . $germplasmAttributeTabUrl . '" href="' . $germplasmAttributeTabUrl . '" data-entity="germplasm_attribute">' . \Yii::t('app', 'Germplasm Attribute') . '</a>
    </li>',
];

// Add breadcrumbs
echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/breadcrumbs.php',[
    "sourceToolName" => $sourceToolName,
    "fileUploadInfo" => $fileUploadInfo,
    "returnUrl" => $returnUrl,
    "title" => 'File Upload Summary'
]);

// Initialize printouts component
$printouts = PrintoutsWidget2::widget([
    "product" => "Seed Inventory",
    "program" => $fileUploadInfo['programCode'],
    "entityName" => 'germplasm file upload',
    "entityDisplayName" => $fileUploadInfo['fileName'],
    "entityDbIds" => [ $fileDbId ],
    "buttonStyle" => [
        "margin-left" => "10px"
    ]
]);

?>

<div class="panel panel-default" style="min-height: 500px; margin-top: 60px;">
    <?php
        // Add file info panel
        echo Yii::$app->controller->renderPartial('@app/modules/fileUpload/views/file-info.php',[
            "fileUploadInfo" => $fileUploadInfo
        ]);

        // Render tabs
        echo '
        <div class="row" style="margin-top:0px; margin-bottom:0px">
            <ul id="tabs" class="tabs">'; 
        foreach($supportedTabs as $tab) {
            echo $tabs[$tab];
        }
        echo '    </ul>
            <br />
        </div>      
        ';

        // Render grid
        echo '<div style="padding-left:15px; padding-right:15px;">';
        $description = Yii::t('app', "Review " . str_replace("_", " ", $entity) . " records here.");
        $dynagrid = DynaGrid::begin([
            'columns'=>$columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme'=>'simple-default',
            'showPersonalize' => true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions'=>[
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'tableOptions'=>['class'=>$browserId.'-table'],
                'options'=>['id'=>$browserId.'-table-con'],
                'id' => $browserId.'-table-con',
                'striped'=>false,
                'responsive' => true,
                'responsiveWrap' => false,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                ],
                'hover' => true,
                'floatOverflowContainer' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => ['id' => $browserId, 'enablePushState' => true,],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'rowOptions'=> [],
                'panel'=> [
                    'heading'=> false,
                    'before' =>  $description . '{summary}',
                    'after'=> false,
                ],
                'toolbar' =>  [
                    [
                        'content' => 
                            $printouts . Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                [
                                    Yii::$app->controller->action->id,
                                    'fileDbId' => $fileDbId,
                                    'returnUrl' => $returnUrl,
                                    'configAbbrevPrefix' => $configAbbrevPrefix,
                                    'entity' => $entity,
                                    'sourceToolName' => $sourceToolName
                                ],
                                [
                                    'class' => 'btn btn-default',
                                    'id' => 'file-upload-reset-summary-grid-btn',
                                    'data-position' => 'top',
                                    'data-tooltip' => \Yii::t('app', 'Reset grid'),
                                    'data-pjax' => true
                                ]
                            )
                            . '{dynagridFilter}{dynagridSort}{dynagrid}'

                    ],
                ],
            ],
            'options'=> [
                'id' => $browserId
            ]
        ]);

        DynaGrid::end();
        echo '</div>';

    ?>


    

</div>

<?php

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
    \yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<< JS

    $("document").ready(function() {
        
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        
        refresh();

        // Reload grid on pjax success
        $(document).on('ready pjax:success', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            refresh();
        });
    });

    /**
    * Resize grid table upon loading 
    */
    function refresh() {
        var maxHeight = ($(window).height() - 350);
        $(".kv-grid-wrapper").css("height", maxHeight);
        $('.dropdown-trigger').dropdown();
    }
JS;

$this->registerJs($script);

?>

