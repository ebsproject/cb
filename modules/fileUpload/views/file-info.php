<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file for file upload info
 */
use app\modules\fileUpload\models\FileUploadHelper;
?>

<div class="row" style="margin-bottom:0px;">
<!-- File upload summary -->
<?php
    $fileStatus = strtoupper($fileUploadInfo['fileStatus']);
    $fileStatusDisplay = '';
    
    $statusTitle = 'Status unknown';
    $statusColor = 'grey';

    switch($fileStatus){
        case in_array($fileStatus,['IN QUEUE','CREATED']):
            $statusTitle = 'Queued for validation';
            $statusColor = 'grey';
            break;
        case 'VALIDATION IN PROGRESS':
            $statusTitle = 'Ongoing transaction validation.';
            $statusColor = 'orange';
            break;
        case in_array($fileStatus, ['VALIDATED', 'MERGE READY', 'CODING READY']):
            $statusTitle = 'Validation completed';
            $statusColor = 'blue';
            break;
        case in_array($fileStatus,['CREATION IN PROGRESS','UPDATE IN PROGRESS','MERGING IN PROGRESS']):
            $statusTitle = 'Ongoing transaction process.';
            $statusColor = 'yellow';
            break;
        case in_array($fileStatus,['VALIDATION FAILED','VALIDATION ERROR','CREATION FAILED','UPDATE FAILED']):
            $statusTitle = 'Transaction has failed.';
            $statusColor = 'red';
            break;
        case 'COMPLETED':
            $statusTitle = 'Transaction completed';
            $statusColor = 'green';
            break;
        case 'RESOLVING CONFLICT':
            $fileStatusStyle = FileUploadHelper::getStatusClass($fileStatus);

            $statusColor = $fileStatusStyle['class'];
            $statusTitle = $fileStatusStyle['title'];
            break;
        default:
            $statusTitle = 'Status unknown';
            $statusColor = 'grey';
    }
        
    $fileStatusDisplay = '<span title="'.$statusTitle.'" class="new badge '.$statusColor.' darken-2"><strong> '.\Yii::t('app', $fileStatus).' </strong></span>';

    echo '<div id="occurrence-detail-div" style="margin-left:20px;margin-bottom:0px" >
        <div class="col-md-4">
            <dl>
                <dt title="File Status">File Status</dt>
                <dd>' . $fileStatusDisplay . '</dd>
            </dl>
        </div>
        <div class="col-md-4">
            <dl>
                <dt title="Uploader">Uploader</dt>
                <dd>' . $fileUploadInfo['uploader'] . '</dd>
            </dl>
        </div>
        <div class="col-md-4">
            <dl>
                <dt title="Upload Timestamp">Upload Timestamp</dt>
                <dd>' . $fileUploadInfo['uploadTimestamp'] . '</dd>
            </dl>
        </div>
    </div>';
?>
</div>
