<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Display site breadcrumbs
 */

?>

<div class="col-md-12" align="right" style="margin: 0px; padding: 10px;">
    <!-- Header -->
    <div class="col-md-12 row pull-left" style="padding: 0px; text-align: left;">
        <div class="col-md-10">
            <h3 class="file-upload-error-browser-title">
                <a href="<?= $returnUrl ?>"><?= $sourceToolName ?></a>
                <small>» <?= $title ?> » <?= $fileUploadInfo['fileName'] ?></small>
            </h3>
        </div>
        <div class="col-md-2" style="padding: 0px;">
            <button class="btn btn-default pull-right lighten-4" id="file-upload-view-back-btn" type="button" style="margin-bottom:5px;">BACK</button>
        </div>
    </div>
</div>

<?php

$script = <<< JS

$("document").ready(function() {

    /**
     * On back button click event
     */
    $(document).on('click', '#file-upload-view-back-btn', function(e) {
        // Redirect
        window.location.assign('$returnUrl');
    });
});
JS;

$this->registerJs($script);