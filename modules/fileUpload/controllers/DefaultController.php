<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\fileUpload\controllers;

use yii\web\Controller;
use app\modules\fileUpload\models\ErrorLogModel;
use app\modules\fileUpload\models\FileUploadHelper;
use app\modules\fileUpload\models\SummaryModel;
use app\modules\inventoryManager\models\TemplatesModel;
use app\interfaces\models\IGermplasmFileUpload;

/**
 * Default controller for the `fileUpload` module
 */
class DefaultController extends Controller
{

    /**
     * Constructor for controller
     */
    public function __construct($id, $module,
        public ErrorLogModel $errorLogModel,
        public FileUploadHelper $fileUploadHelper,
        public SummaryModel $summaryModel,
        public TemplatesModel $templatesModel,
        public IGermplasmFileUpload $germplasmFileUpload,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the error logs browser
     * @param Integer fileDbId file upload identifier
     * @param String returnUrl URL for the back button
     * @param String sourceToolName name of the tool displaying the error log
     */
    public function actionErrorLogs($fileDbId, $returnUrl, $sourceToolName)
    {
        // Get file error log info
        $fileErrorLogInfo = $this->errorLogModel->search($fileDbId);

        // Render page
        return $this->render('error-logs', [
            "fileDbId" => $fileDbId,
            "returnUrl" => $returnUrl,
            "sourceToolName" => $sourceToolName,
            "fileErrorLogInfo" => $fileErrorLogInfo,
            'searchModel' => $this->errorLogModel
        ]);
    }

    /**
     * Renders the file upload summary browser
     * @param Integer fileDbId file upload identifier
     * @param String returnUrl URL for the back button
     * @param String sourceToolName name of the tool displaying the summary
     * @param String configAbbrevPrefix the prefix of the configurations to be used
     * @param String entity germplasm, seed, or pacakge
     */
    public function actionSummary($fileDbId, $returnUrl, $sourceToolName, $configAbbrevPrefix, $entity = null)
    {
        $fileUploadInfo = $this->fileUploadHelper->getFileUploadInfo($fileDbId);
        $supportedEntities = ['germplasm', 'germplasm_relation', 'seed', 'package', 'germplasm_attribute'];

        // Tab configurations
        $tabConfig = [
            "default" => [ "germplasm", "seed", "package" ],
            "seed-package" => [ "seed", "package" ],
            "seed" => [ "seed" ],
            "package" => [ "package" ],
            "germplasm_relation" => [ "germplasm_relation" ],
            "germplasm" => [ "germplasm" ],
            "germplasm_attribute" => [ "germplasm_attribute" ]
        ];

        $fileUploadType = $fileUploadInfo["remarks"];
        
        $supportedTabs = !empty($fileUploadType) && isset($tabConfig[$fileUploadType]) ?
            $tabConfig[$fileUploadType] : $tabConfig["default"];
        
        // if entity is defined for file upload record
        if(
            isset($fileUploadInfo["entity"]) 
            && !empty($fileUploadInfo["entity"]) 
            && in_array($fileUploadType, $supportedEntities)
        ){
            $supportedTabs = $tabConfig[strtolower($fileUploadInfo["entity"])] ?? $tabConfig["default"];
        }

        // provide only GERMPLASM tab for merge transactions, SEED and PACKAGE not applicable
        if(
            isset($fileUploadInfo["entity"]) && !empty($fileUploadInfo["entity"]) 
            && !in_array($fileUploadType, $supportedEntities) && $fileUploadInfo["action"] == 'merge'
        ){
            $supportedTabs = $tabConfig[strtolower($fileUploadInfo["entity"])] ?? $tabConfig["default"];
            $entity = strtolower($fileUploadInfo["entity"]);
        }
        
        // Check if entity is not supplied
        if(empty($entity)) {
            $entity = "germplasm";
            // If file upload remarks is 'seed-package' or 'seed',
            // set default entity = 'seed'
            if (in_array($fileUploadInfo["remarks"],["seed-package","seed"])) {
                $entity = "seed";
            }
            // If file upload remarks is 'package',
            // set default entity = 'package'
            else if ($fileUploadInfo["remarks"] == "package") {
                $entity = "package";
            }
            // If file upload remarks is 'germplasm_relation',
            // set default entity = 'germplasm_relation'
            else if ($fileUploadInfo["remarks"] == "germplasm_relation") {
                $entity = "germplasm_relation";
            }

            return $this->redirect([
                'default/summary',
                'fileDbId' => $fileDbId,
                'returnUrl' => $returnUrl,
                'sourceToolName' => $sourceToolName,
                'configAbbrevPrefix' => $configAbbrevPrefix,
                'entity' => $entity
            ]);
        }

        // Check if entity is applicable to the upload type.
        // If not applicable, redirect to the first item in the 
        // supported tabs config with a warning flash message.
        if(!in_array($entity, $supportedTabs)) {
            \Yii::$app->session->setFlash(
                'warning', 
                \Yii::t('app', 'Entity "' . $entity . '" is not supported for upload type: "' . $fileUploadType . '"')
            );
            return $this->redirect([
                'default/summary',
                'fileDbId' => $fileDbId,
                'returnUrl' => $returnUrl,
                'sourceToolName' => $sourceToolName,
                'configAbbrevPrefix' => $configAbbrevPrefix,
                'entity' => $supportedTabs[0] ?? null
            ]);
        }            

        // Retrieve records
        $dataProvider = $this->summaryModel->search($fileDbId, $entity);
        $searchModel = $this->summaryModel;
        $config = $this->fileUploadHelper->getConfigurations($configAbbrevPrefix, $fileUploadInfo['programCode'], $entity);
        $columns = $this->fileUploadHelper->buildColumns($config, $entity);

        // Render page
        return $this->render('summary', [
            "fileDbId" => $fileDbId,
            "returnUrl" => $returnUrl,
            'sourceToolName' => $sourceToolName,
            "fileUploadInfo" => $fileUploadInfo,
            "entity" => $entity,
            'tabConfig' => $tabConfig,
            'supportedTabs' => $supportedTabs,
            'configAbbrevPrefix' => $configAbbrevPrefix,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $columns
        ]);
    }

    /**
     * Consumes the `file_data` of the given `file_upload` record,
     * and rebuilds it as a CSV file based on the applicable column headers.
     * 
     * @param Integer $fileDbId file upload identifier
     * @param String $configAbbrevPrefix config abbrev prefix to use for retrieving configurations
     * @param String $sourceToolName source tool retrieval to use for specific functions
     */
    public function actionDownloadFileDataCsv($fileDbId, $configAbbrevPrefix, $sourceToolName = null) {
        // Retrieve file upload information
        $fileUploadInfo = $this->fileUploadHelper->getFileUploadInfo($fileDbId);
        $fileName = $fileUploadInfo['fileName'];
        $fileData = $fileUploadInfo['fileData'];
        $programCode = $fileUploadInfo['programCode'];

        // Get configs
        $useColumnHeader = $fileUploadInfo['action'] == 'merge' ? true : false;
        $config = $this->fileUploadHelper->getConfigurations($configAbbrevPrefix, $programCode);

        // Extract template headers based on config
        $headers = $this->templatesModel->extractTemplateHeaders(
            config: $config,
            entityOrderArr: [
                'germplasm',
                'germplasm_relation',
                'seed',
                'package',
                'germplasm_attribute'
            ],
            useColumnHeader: $useColumnHeader
        );

        $headers = array_unique($headers);

        // If tool source is germplasm and is not a merge transaction, run
        if($sourceToolName == "Germplasm" && $fileUploadInfo['action'] != 'merge'){
            // Retrieve column keys from fileData
            $rowKeys = [];
            if(isset($fileData[0]) && !empty($fileData[0])){
                foreach( $fileData[0] as $et ){
                    $rowKeys = array_merge($rowKeys,array_map('strtoupper',array_keys($et)));
                }
            }

            // Filters column headers that are not in the file upload originally
            $headers = array_intersect($headers, $rowKeys);
        }

        // Rebuild data from file_upload.file_data
        $data = $this->fileUploadHelper->rebuildFileData($headers, $fileData);

        // Build CSV
        if(!empty($headers)){
            $this->templatesModel->buildCSVFile(
                headers: $headers,
                programCode: "",
                type: "",
                data: $data,
                fileName: $fileName);
        }
        else{
            return false;
        }
    }
}
