<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\fileUpload\models;

// Import interfaces
use app\interfaces\models\IApi;
use app\interfaces\models\IGermplasmFileUpload;
use app\interfaces\models\IProgram;
use app\interfaces\models\IUserDashboardConfig;
// Import data providers
use app\dataproviders\ArrayDataProvider;

/**
 * Model for facilitating displaying the summary of the file upload
 */
class SummaryModel
{
    // Constants for the endpoints based on the given entity
    CONST GERMPLASM_ENDPOINT = "germplasm-search";
    CONST GERMPLASM_RELATION_ENDPOINT = "data-search?entity=germplasm_relation";
    CONST SEED_ENDPOINT = "seeds-search";
    CONST PACKAGE_ENDPOINT = "packages-search";
    CONST GERMPLASM_FILE_UPLOAD_ENDPOINT = "data-search";

    /**
     * Model class constructor
     */
    public function __construct(
        public IApi $api,
        public IGermplasmFileUpload $germplasmFileUpload,
        public IProgram $program,
        public IUserDashboardConfig $userDashboardConfig
    )
    { }

    /**
     * Retrieves data to be displayed in the browser
     * @param Integer fileDbId file upload identifier
     * @param String entity germplasm, seed, or package
     */
    public function search($fileDbId, $entity) {
        // Initialize variables
        $httpMethod = "POST";
        $basePath = "germplasm-file-uploads/$fileDbId/";
        $entityParts = explode('_',$entity);
        for($i = 1; $i < count($entityParts); $i++) {
            $entityParts[$i] = ucfirst($entityParts[$i]);
        }
        $entityApiField = implode("",$entityParts);
        $id = $entityApiField . "DbId";
        // Append correct endpoint based on entity
        if($entity == 'germplasm') {
            $basePath .= self::GERMPLASM_ENDPOINT;
        }
        else if($entity == 'germplasm_relation') {
            $basePath .= self::GERMPLASM_RELATION_ENDPOINT;
        }
        else if($entity == 'seed') {
            $basePath .= self::SEED_ENDPOINT;
        }
        else if($entity == 'package') {
            $basePath .= self::PACKAGE_ENDPOINT;
        }
        else if($entity == 'germplasm_attribute') {
            $basePath .= self::GERMPLASM_FILE_UPLOAD_ENDPOINT.'?entity='.$entity;
        }
        // Get filters
        $filter = $this->assembleUrlParameters(false, $id, $entity);

        // Retrieve data
        $result = $this->api->getApiResults(
            method: $httpMethod,
            url: $basePath,
            filter: $filter,
            rawData: json_encode([
                "$id" => "not null"
            ])
        );
        $data = $result['data'] ?? [];
        $totalCount = $result['totalCount'] ?? 0;

        // If data is empty, reset page
        if(empty($data)) {
            $filter = $this->assembleUrlParameters(true, $id, $entity);

            $result = $this->api->getApiResults(
                method: $httpMethod,
                url: $basePath,
                filter: $filter,
                rawData: json_encode([
                    "$id" => "not null"
                ])
            );
            $data = $result['data'] ?? [];
            $totalCount = $result['totalCount'] ?? 0;

        }

        // Return array data provider
        return new ArrayDataProvider([
            'allModels' => $data,
            'key' => $id,
            'restified' => true,
            'totalCount' => $totalCount
        ]);
    }
    
    /**
     * Assembles the URL parameters for retrieving browser data
     * @param Boolean resetPage whether or not to reset the page
     * @param String entityIdName name of the dbId based on entity
     * @param String entity germplasm, seed, or package
     * @return String url parameters in string format
     */
    public function assembleUrlParameters($resetPage, $entityIdName, $entity) {
        $gridId = 'dynagrid-file-upload-summary-' . $entity . '-grid';
        $defaultSortString = 'sort=' . $entityIdName . ':ASC';
        // Sorting
        $paramSort = $defaultSortString;
        $getParams = \Yii::$app->request->getQueryParams();

        // Pagination
        $paramPage = '';
        if($resetPage) {
            $paramPage = 'page=1';
            if (isset($getParams[$gridId . '-page'])) {
                $getParams[$gridId . '-page'] = 1;
            }
            else if (isset($getParams['dp-1-page'])){
                $getParams['dp-1-page'] = 1;
            }
            else if (isset($getParams['page'])){
                $getParams['page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);
        }
        else if (isset($getParams[$gridId . '-page'])){
            $paramPage = 'page=' . $getParams[$gridId . '-page'];
        }
        else if (isset($getParams['dp-1-page'])){
            $paramPage = 'page=' . $getParams['dp-1-page'];
        } else if (isset($getParams['page'])) {
            $paramPage = 'page=' . $getParams['page'];
        }
        
        // Page limit
        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $queryParams = [];
        if($paramLimit!='') $queryParams[] = $paramLimit;
        if($paramPage!='') $queryParams[] = $paramPage;
        if($paramSort!='') $queryParams[] = $paramSort;

        return implode("&", $queryParams);
    
    }

}