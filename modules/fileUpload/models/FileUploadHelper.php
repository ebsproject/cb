<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\fileUpload\models;

// Import interfaces
use app\interfaces\models\IConfig;
use app\interfaces\models\IGermplasmFileUpload;
use app\interfaces\models\IProgram;
// Import widgets 
use app\widgets\GermplasmInfo;
use app\widgets\SeedInfo;

/**
 * Class file for generic, reusable functions for the File Upload module
 */
class FileUploadHelper {

    /**
     * Model constructor
     */
    public function __construct (
        public IConfig $configurations,
        public IGermplasmFileUpload $germplasmFileUpload,
        public IProgram $program
    )
    { }
    
    /**
     * Retrieves the file upload information given the file id.
     * @param Integer $fileDbId file upload identifier
     * @return Array containing file upload information
     */
    public function getFileUploadInfo($fileDbId) {
        // Get record
        $result = $this->germplasmFileUpload->searchAll(
            [
                "germplasmFileUploadDbId" => "equals $fileDbId"
            ],
            "limit=1&sort=germplasmFileUploadDbId:ASC",
            false
        );
        $data = $result['data'] ?? [];
        $fileUploadRecord = $data[0] ?? [];

        // Extract details
        $fileName = $fileUploadRecord['fileName'] ?? 'Unknown file';
        $fileStatus = !empty($fileUploadRecord['fileStatus']) ? ucfirst($fileUploadRecord['fileStatus']) : 'Unknown status';
        $uploader = $fileUploadRecord['uploader'] ?? 'Unknown uploader';
        $uploadTimestamp = $fileUploadRecord['uploadTimestamp'] ?? 'Unknown timestamp';
        $fileData = $fileUploadRecord['fileData'] ?? [];
        $programCode = $fileUploadRecord['programCode'] ?? [];
        $errorLog = $fileUploadRecord['errorLog'] ?? [];
        $remarks = $fileUploadRecord['remarks'] ?? [];
        $entity = $fileUploadRecord['entity'] ?? '';
        $fileUploadAction = $fileUploadRecord['fileUploadAction'] ?? '';

        return [
            "fileName" => $fileName,
            "fileStatus" => $fileStatus,
            "uploader" => $uploader,
            "uploadTimestamp" => $uploadTimestamp,
            "programCode" => $programCode,
            "fileData" => $fileData,
            "errorLog" => $errorLog,
            "remarks" => $remarks,
            "entity" => $entity,
            "action" => $fileUploadAction
        ];
    }

    /**
     * Retrieves the configurations applicable to the current program based in the base abbrev
     * @param String baseAbbrev the base abbrev of the config
     * @param String programCode program code
     * @param String entity germplasm, seed or package
     * @return Array containing the configurations
     */
    public function getConfigurations($baseAbbrev, $programCode, $entity = null) {
        // Variable for default configuration values
        $configValues = [];
        
        // Get crop code
        $cropCode = $this->program->getCropCode($programCode);

        // Get system default config
        $systemDefaultConfigAbbrev = $baseAbbrev . "SYSTEM_DEFAULT";
        $systemDefaultConfig = $this->configurations->getConfigByAbbrev($systemDefaultConfigAbbrev) ?? [];

        // Get config based on passed baseAbbrev
        if(empty($systemDefaultConfig)){
            $systemDefaultConfig = $this->configurations->getConfigByAbbrev($baseAbbrev) ?? [];
        }

        $systemDefaultConfigValues = $systemDefaultConfig['values'] ?? [];

        // Get config for Program and crop
        // Condition if entity is equals to germplasm, set config abbrev to base abbrev
        if($entity == 'germplasm'){
            $configAbbrev = $baseAbbrev;
        }
        else{
            $configAbbrev = $baseAbbrev . $cropCode . "_" . $programCode;
        }

        $config = $this->configurations->getConfigByAbbrev($configAbbrev) ?? [];
    
        // If config does not exist, retrieve default config for the crop
        if(empty($config)) {
            $configAbbrev = $baseAbbrev . $cropCode . "_DEFAULT";
            $config = $this->configurations->getConfigByAbbrev($configAbbrev) ?? [];
            $configValues = $config['values'] ?? [];
        }
        
        return array_merge($systemDefaultConfigValues, $configValues);
    }

    /**
     * Builds the browser columns based on the given config
     * NOTE: This is a WIP function. There are parts of this function that are temporary.
     * Changes in the configs are required to properly display or hide columns.
     * @param Array config array containing the configurations
     * @param String entity germplas, seed, or package
     * @return Array array containing browser columns 
     */
    public function buildColumns($config, $entity) {
        $abbrevAttribCompat = [
            "hvdateCont" => "harvestDate",
            "hvMethDisc" => "harvestMethod",
            "volume" => "packageQuantity",
            "parentDesignation" => "parentGermplasmName",
            "childDesignation" => "childGermplasmName",
            "germplasmName" => "designation"
        ];

        // build columns
        $columns = [];
        $columnKeys = [];
        foreach($config AS $column){
            $columnAttribStr = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($column['abbrev'])))));
            $columnAttribStr = isset($abbrevAttribCompat[$columnAttribStr]) ? $abbrevAttribCompat[$columnAttribStr] : $columnAttribStr;

            $browserColumn = [];

            $viewConfig = $column['view'] ?? [];
            $visible = !empty($viewConfig['visible']) && $viewConfig['visible'] === 'true';
            $entities = $viewConfig['entities'] ?? [];

            if($visible && in_array($entity, $entities)) {
                $abbrev = $column['abbrev'] ?? '';
                $name = $column['name'] ?? $abbrev;

                // Build designation column contents with germplasm info modal
                if(str_contains($abbrev,'DESIGNATION') || $abbrev == 'GERMPLASM_NAME'){
                    $dbIdField = $abbrev == 'PARENT_DESIGNATION' ? 'parentGermplasmDbId' : 
                        (
                            $abbrev == 'CHILD_DESIGNATION' ? 'childGermplasmDbId' : 'germplasmDbId'
                        );
                    
                    $nameField = $abbrev == 'PARENT_DESIGNATION' ? 'parentGermplasmName' : 
                    (
                        $abbrev == 'CHILD_DESIGNATION' ? 'childGermplasmName' : 'designation'
                    );
                    $browserColumn = [
                        'attribute' => $columnAttribStr,
                        'label' => $name,
                        'contentOptions'=>[
                            'class' => 'germplasm-name-col'
                        ],    
                        'value' => function($data) use($dbIdField, $nameField) {
                            $germplasmDbId = $data[$dbIdField];
                            $designation = $data[$nameField];
                            
                            $linkAttrs = [
                                'id' => 'view-germplasm-widget-'.$germplasmDbId,
                                'class' => 'blue-text text-darken-4 header-highlight',
                                'title' => \Yii::t('app', 'Click to view more information'),
                                'data-label' => $designation,
                                'data-id' => $germplasmDbId,
                                'data-target' => '#view-germplasm-widget-modal',
                                'data-toggle' => 'modal'
                            ];
                
                            $widgetName = GermplasmInfo::widget([
                                'id' => $germplasmDbId,
                                'entityLabel' => $designation,
                                'linkAttrs' => $linkAttrs
                            ]);
                            return $widgetName;
                        },
                        'visible' => true,
                        'hidden' => false,
                        'format'=>'raw',
                        'mergeHeader' => true,
                        
                    ];
                }
                // Build seed name column contents with seed info modal
                else if($abbrev == 'SEED_NAME'){
                    $browserColumn = [
                        'attribute'=>$columnAttribStr,
                        'label' => $name,
                        'contentOptions'=>[
                            'class' => 'seed-name-col'
                        ],
                        'value'=>function($data) {
                            $seedDbId = $data['seedDbId'];
                            $seedName = $data['seedName'];
        
                            $linkAttrs = [
                                'id' => 'view-seed-widget-'.$seedDbId,
                                'class' => 'blue-text text-darken-4 header-highlight',
                                'title' => \Yii::t('app', 'Click to view more information'),
                                'data-label' => $seedName,
                                'data-id' => $seedDbId,
                                'data-target' => '#view-seed-widget-modal',
                                'data-toggle' => 'modal'
                            ];
        
                            $widgetName = SeedInfo::widget([
                                'id' => $seedDbId,
                                'entityLabel' => $seedName,
                                'label' => $seedName,
                                'linkAttrs' => $linkAttrs
                            ]);
                            return $widgetName;
                        },
                        'format'=>'raw',
                        'mergeHeader' => true
                    ];
                }
                // Generic column
                else{
                    $browserColumn = [
                        'attribute' => $columnAttribStr,
                        'label' => $name,
                        'visible' => true,
                        'hidden' => false,
                        'mergeHeader' => true,
                        'contentOptions' => [
                            "class" => "germplasm-name-col"
                        ]
                    ];
                }
            }

            if(!empty($browserColumn) && !in_array($columnAttribStr,$columnKeys)){
                array_push($columns,$browserColumn);
                array_push($columnKeys,$columnAttribStr);
            }
        };

        if(empty($columns)){
            $columns = [[]];
        }

        return $columns;
    }

    /**
     * Rebuilds the file data based on the given column headers.
     * 
     * @param Array $headers array containing column headers
     * @param Array $fileData `file_data` from the `file_upload` record
     * @return Array $data contains rebuilt `file_data` in the correct format for CSV building
     */
    public function rebuildFileData($headers, $fileData) {
        $data = [];

        // Loop through each row in the file data
        foreach($fileData as $rowNum => $row) {
            $unorderedRowData = [];
            $orderedRowData = [];

            // Retrieve data and store in the unordered array
            foreach($row as $entity => $values) {
                foreach($values as $key => $value) {
                    $unorderedRowData[$key] = $value;
                }
            }
            
            // Loop through the headers
            foreach($headers as $header) {
                $headerLower = strtolower($header);
                
                // remove KEEP and MERGE indicators
                if(str_contains($headerLower,'keep') || str_contains($headerLower,'merge')){
                    $entity = str_contains($headerLower,'keep') ? 'keep' : 'merge';

                    $headerLower = str_replace('_keep','',$headerLower);
                    $headerLower = str_replace('_merge','',$headerLower);
                }
                
                // if row has values for keep and merge entities
                if(in_array($entity,['keep','merge'])){
                    $orderedRowData[] = $row[$entity][$headerLower];
                }
                // If the row has a value for the current header,
                // add it to the ordered array.
                // Else, add an empty string to the ordered array.
                else if(isset($unorderedRowData[$headerLower])) {
                    $orderedRowData[] = $unorderedRowData[$headerLower];
                } else $orderedRowData[] = "";
            }

            // Add each ordered row data to the main data array
            $data[] = $orderedRowData;
        }

        return $data;
    }

    /**
     * Retrieve file upload status class and title value
     * 
     * @param String $fileUploadStatus file upload transaction status
     * 
     * @return mixed
     */
    public static function getStatusClass($fileUploadStatus){
        $statusClass = 'grey';
        $statusTitle = '';

        $fileUploadStatus = strtolower($fileUploadStatus);
        switch($fileUploadStatus){
            case (in_array($fileUploadStatus,['in queue','created'])):
                $statusClass = 'grey';
                $statusTitle = 'Queued for validation'; 
                break;
            case (in_array($fileUploadStatus,['validation in progress'])):
                $statusClass = 'orange'; 
                $statusTitle = 'Validation in progress';
                break;
            case (in_array($fileUploadStatus,['resolving conflict'])):
                $statusClass = 'purple'; 
                $statusTitle = 'Conflict resolution in progress';
                break;
            case (in_array($fileUploadStatus,['validated', 'coding ready'])):
                $statusClass = 'blue'; 
                $statusTitle = 'Transaction is ready to be completed';
                break;
            case (in_array($fileUploadStatus,['merge ready'])):
                $statusClass = 'blue'; 
                $statusTitle = 'Transaction is ready for merging';
                break;
            case (in_array($fileUploadStatus, ['creation in progress', 'merging in progress', 'coding in progress'])):
                $statusClass = 'yellow'; 
                $statusTitle = 'Completion of transaction in progress';
                break;
            case (in_array($fileUploadStatus,['created','completed'])):
                $statusClass = 'green'; 
                $statusTitle = 'Transaction has been completed';
                break;
            case (in_array($fileUploadStatus, ['validation error', 'validation failed', 'creation failed', 'merge failed', 'coding failed'])):
                $statusClass = 'red'; 
                $statusTitle = 'Transaction has failed';
                break;
            default:
                $statusClass = 'grey'; 
                $statusTitle = 'Unknown status';
                break;
        }

        return [
            'class' => $statusClass,
            'title' => $statusTitle
        ];
    }
}