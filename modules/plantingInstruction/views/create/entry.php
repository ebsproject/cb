<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders Entry management step for Create Packing job
 */

// review and submit URL
$reviewUrl = Url::to(['create/review', 'program' => $program, 'id' => $id]);

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id, 
    'program' => $program, 
    'active' => 'entry',
    'plantingJobCode'=> $plantingJobCode,
    'url' => $reviewUrl,
    'class' => 'disabled'
]);

$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';

$columns = [
    [
        //checkbox
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="entry-select-all" />
            <label for="entry-select-all"></label>
        ',
        'content'=> function ($data) {
            $plantingJobEntryDbId = $data['plantingJobEntryDbId'];
            $experimentDbId = $data['experimentDbId'];
            $entryDbId = $data['actualEntryDbId'];
            $germplasmDbId = $data['germplasmDbId'];
            $packageDbId = $data['packageDbId'];
            $actualGermplasmName = $data['actualGermplasmName'];

            return "
                <input
                    class='entry-grid-select filled-in'
                    type='checkbox'
                    id='$plantingJobEntryDbId'
                    data-experiment-id='$experimentDbId'
                    data-entry-id='$entryDbId'
                    data-germplasm-id='$germplasmDbId'
                    data-package-id='$packageDbId'
                    data-germplasm-name='$actualGermplasmName'
                '/>
                <label for='$plantingJobEntryDbId'></label>
            ";
        },        
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'experimentName', 
        'value' => function ($model, $key, $index, $widget) { 
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'format' => 'raw',
        'group' => true,  // enable grouping
        'groupHeader' => function ($model, $key, $index, $widget) {
            return [
                'content' => [
                    1 => $model['experimentName'],
                    2 => '.'
                ],
                'options' => ['class' => 'bold light-green lighten-4']
            ];
        },
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'entryCode',
        'label' => 'Entry code',
        'contentOptions' => ['class' => 'bold'],
        'group' => true,  // enable grouping
        'subGroupOf' => 1, // supplier column index is the parent group,
        'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
            return [
                'content' => [
                    5 => GridView::F_COUNT,
                    6 => GridView::F_COUNT,
                    9 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    5 => ['format' => 'number', 'decimals' => 0],
                    6 => ['format' => 'number', 'decimals' => 0],
                    9 => ['format' => 'number', 'decimals' => 0],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    5 => ['style' => 'text-align:left'],
                    6 => ['style' => 'text-align:left'],
                    7 => ['style' => 'text-align:left'],
                    8 => ['style' => 'text-align:left'],
                    9 => ['style' => 'text-align:left'],
                    12 => ['style' => 'text-align:left'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'info table-info bold']
            ];
        },
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'actualGermplasmName',
        'label' => 'Germplasm name',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'actualParentage',
        'label' => 'Parentage',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'value' => function ($model, $key, $index, $widget) { 
            return '<div
                title='.$model['actualParentage'].'
                class="truncated"
            >' . $model['actualParentage']. '</div>';
        },
    ],
    [
        'attribute' => 'siteCode',
        'label' => 'Site',
        'pageSummary' => 'Page Summary',
        'pageSummaryOptions' => ['class' => 'text-right'],
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_COUNT,
    ],
    [
        'attribute' => 'occurrenceName',
        'label' => 'Occurrence',
        'pageSummary' => 'Page Summary',
        'pageSummaryOptions' => ['class' => 'text-right'],
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_COUNT,
    ],
    [
        'attribute' => 'actualSeedName',
        'label' => 'Seed Name',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'actualPackageLabel',
        'label' => 'Package Label',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'envelopeCount',
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_SUM,
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'requiredPackageQuantity',
        'label' => 'Required',
        'value' => function ($model) {
            return empty($model['requiredPackageQuantity']) ? '<span class="not-set">(not set)</span>' : sprintf('%g', $model['requiredPackageQuantity']);
        },
        'pageSummary' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'actualPackageReserved',
        'label' => 'Reserved',
        'value' => function ($model) {
            return empty($model['actualPackageUnit']) ? '<span class="not-set">(not set)</span>' : sprintf('%g', $model['actualPackageReserved']);
        },
        'pageSummary' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'requiredPackageUnit',
        'label' => 'Unit',
        'format' => 'raw',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'contentOptions' => [
            'style'=>'width: 100px;'
        ],
    ],
    [
        'label' => 'Available',
        'attribute' => 'actualAvailableQuantity',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'value' => function($model){
            return !empty($model['actualPackageUnit']) ? sprintf('%g', $model['actualAvailableQuantity']) . ' ' . $model['actualPackageUnit'] : '<span class="not-set">(not set)</span>';
        },
    ],
        
];

$columns = array_merge($columns, $formColumns);
// replace column

$replaceCol = [
    [
        'attribute' => 'isReplaced',
        'label' => 'Replace',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
                'true' => 'Replaced',
                'false' => 'Not replaced'
        ],   
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
        'filterInputOptions' => [
            'placeholder' => '',
        ],
        'contentOptions' => [
            'style'=>'min-width: 120px;'
        ],
        'hAlign' => 'center',
        'order' => DynaGrid::ORDER_FIX_RIGHT,
        'value' => function ($model) {
            $label = $model['isReplaced'] ? 'Replaced' : 'Replace';
            $class = $model['isReplaced'] ? '' : ' grey black-text lighten-2';

            $button = Html::a(
                $label, '#!',
                [
                    'title' => Yii::t('app', 'Replace entry'),
                    'class' => 'btn btn-primary waves-effect waves-light replace-entry-btn replace-entry-btn-'. $model['plantingJobEntryDbId'] . $class,
                    'data-id' => $model['plantingJobEntryDbId'],
                    'data-planting_job_id' => $model['plantingJobDbId'],
                    'data-experiment_id' => $model['experimentDbId'],
                    'data-occurrence' => $model['occurrenceName'],
                    'data-experiment' => $model['experimentName'],
                    'data-entry_id' => $model['actualEntryDbId'],
                    'data-germplasm_id' => $model['germplasmDbId'],
                    'data-package_id' => $model['packageDbId'],
                    'data-target' => '#replace-entry-modal',
                    'data-toggle' => 'modal',
                    'data-summary' => [
                        'info' => [
                            'isReplaced' => $model['isReplaced'],
                            'replacementType' => $model['replacementType'],
                        ],
                        'entry' => [
                            'entryNumber' => $model['entryNumber'],
                            'entryCode' => $model['entryCode'],
                            'entryName' => $model['germplasmName'],
                            'parentage' => $model['parentage'],
                            'replacementGermplasmName' => $model['replacementGermplasmName'],
                            'replacementParentage' => $model['replacementParentage'],
                        ],
                        'quantity' => [
                            'requiredPackageQuantity' => number_format($model['requiredPackageQuantity']) . ' ' . $model['requiredPackageUnit'],
                            'reserved' => number_format($model['actualPackageReserved']) . ' ' . $model['packageUnit'],
                            'available' => $model['packageQuantity'] . ' ' . $model['packageUnit'],
                        ],
                        'package' => [
                            'seedName' => $model['seedName'],
                            'packageLabel' => $model['packageLabel'],
                            'packageQuantity' => number_format($model['packageQuantity']) . ' ' . $model['packageUnit'],
                            'replacementSeedName' => $model['replacementSeedName'],
                            'replacementPackageLabel' => $model['replacementPackageLabel'],
                            'replacementPackageQuantity' => number_format($model['replacementPackageQuantity']) . ' ' . $model['replacementPackageUnit'],
                        ],
                    ],
                    'style' => 'min-width: 108px'
                ]
            );

            return $button;
        },
    ]
];

$columns = array_merge($columns, $replaceCol);

$browserId = 'dynagrid-pim-planting-job-entries';

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', '<span class="insuff-summary">'. $insufficientSummary . "</span> You can replace entries with a check or with another package. Status should all be sufficient to proceed to next step.").
                '&emsp;'.'{summary}<br/><p id = "selected-items" class = "pull-right" style = "margin:-1px"><b id = "selected-count">'.$selectedItemsCount.'</b> selected <span id="selected-items-text">items</span>.</p>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                Html::beginTag(
                    'div',
                    [
                        'class' => 'text-left dropdown',
                        'style' => 'margin-left: 4px'
                    ]
                ) . "
                    <button
                        title='" . Yii::t('app', 'Bulk replace entries') . "'
                        id='dropdown-btn-bulk-replace-entries'
                        class='dropdown-button btn btn-default dropdown-button-pjax'
                        href=''
                        data-activates='dropdown-menu-bulk-replace-entry'
                        data-beloworigin='true'
                        data-constrainwidth='false'
                    >
                        <i class='material-icons' style='font-size: 200% !important; vertical-align: middle !important'>swap_horiz</i> <span class='caret'></span>
                    </button>

                    <ul
                        id='dropdown-menu-bulk-replace-entry'
                        class='dropdown-content'
                        data-beloworigin='false'
                    >
                        <li
                            class='replace-entry-option hide-loading'
                            title='Replace selected entries with a check'
                            data-replacement-type='REPLACEMENT_TYPE_REPLACE_WITH_CHECK'
                            data-replacement-type-text='Replace with check'
                        >" .
                            Html::a(Yii::t('app', 'Replace with check')) .
                        "</li>
                        <li
                            class='replace-entry-option hide-loading'
                            title='Replace selected entries with a filler'
                            data-replacement-type='REPLACEMENT_TYPE_REPLACE_WITH_FILLER'
                            data-replacement-type-text='Replace with filler'
                        >" .
                            Html::a(Yii::t('app', 'Replace with filler')) .
                        "</li>
                        <li
                            class='replace-entry-option hide-loading'
                            title='Replace selected entries with a package'
                            data-replacement-type='REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'
                            data-replacement-type-text='Replace with package'
                        >" .
                            Html::a(Yii::t('app', 'Replace with package')) .
                        "</li>
                        <li
                            class='replace-entry-option hide-loading'
                            title='Replace selected entries with a germplasm'
                            data-replacement-type='REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM'
                            data-replacement-type-text='Replace with germplasm'
                        >" .
                            Html::a(Yii::t('app', 'Replace with germplasm')) .
                        "</li>
                    </ul>" .
                    Html::button('<i class="glyphicon glyphicon-edit"></i> ', [
                        'type'=>'button', 
                        'title'=>Yii::t('app', 'Bulk update'), 
                        'class'=>'btn', 
                        'id'=>'bulk-update-btn',
                        'data-toggle' => 'modal',
                        'data-target' => '#bulk-update-modal'
                    ]) .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-entries-grid-id'
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// modal for bulk update
Modal::begin([
    'id'=> 'bulk-update-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-bulk-update-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
    '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light entry-confirm-update-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
    '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light entry-confirm-update-btn" mode ="all">'.\Yii::t('app','All').'</a>'.'&emsp;',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div style="margin: 10px 20px 5px 28px;"><div id="modal-progress" class="progress hidden"><div class="indeterminate"></div></div></div>';
    echo '<div id="modal-content"><p id="instructions" style="margin: 10px 0px 5px 28px">Select variable and specify value to bulk update.</p>';
    echo '<span id="note"></span>';
    echo '<div id="config-var" style="margin-left:30px;"></div></div>';
Modal::end();

// modal for replace entry form
Modal::begin([
    'id'=> 'replace-entry-modal',
    'header' => '<h4 class="replace-entry-header"><i class="material-icons inline-material-icon" style="margin-right: 4px;">swap_horiz</i>' . \Yii::t('app','Replace entry').'</h4>',
    'closeButton' => [
        'class'=>'close cancel-replace-entry-btn',
    ],
    'footer' => 
        Html::a(\Yii::t('app','Cancel'),['#'],[
            'class'=>'cancel-replace-entry-btn',
            'data-dismiss'=>'modal'
        ]).'&emsp;&nbsp;'.
        Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'replace-entry-next-btn btn-primary waves-effect waves-light',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to next step')
            ],
        ]).
        Button::widget([
            'label' => 'Next',
            'icon' => [
                'name' => 'arrow_forward',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'select-next-btn btn-primary waves-effect waves-light hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Go to preview')
            ],
        ]).
        Button::widget([
            'label' => 'Confirm',
            'icon' => [
                'name' => 'send',
                'position' => 'right'
            ],
            'options' => [
                'class' => 'select-confirm-btn btn-primary waves-effect waves-light hidden',
                'style' => 'margin-right: 34px;',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm replacement')
            ],
        ]),
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div style="margin: 10px 20px 5px 28px;"><div id="modal-progress" class="progress hidden"><div class="indeterminate"></div></div></div>';

    echo '<div class="replace-entry-modal-content parent-panel"></div>';
    echo '<div class="replace-entry-form-modal-content child-panel"></div>';
    echo '<div class="replace-entry-preview-modal-content"></div>';
Modal::end();

// button to trigger validate
echo Html::button('Validate', [
    'type'=>'button', 
    'class'=>'btn validate-statuses-btn hidden', 
]);

// reset grid url
$resetGridUrl = Url::toRoute(['/plantingInstruction/create/entry?program=' . $program .'&id='.$id.'&reset=hard_reset']);
// store selected items in session
$storeSessionItemsUrl = Url::to(['create/store-session-items']);
// render bulk update form url
$renderBulkUpdateUrl = Url::to(['create/render-bulk-update']);
// render field form in bulk update
$genericRenderConfigUrl = Url::to(['create/render-config-form']);
// get selected items url
$getSelectedItemsUrl = Url::to(['create/get-selected-items']);
// save row values url
$saveRowValueUrl = Url::to(['create/save-row-value']);
// bulk update fields url
$bulkUpdateEntryFieldsUrl = Url::to(['create/bulk-update-entry-fields']);
// check if all statuses are sifficient
$checkAllStatusesValidUrl = Url::to(['create/validate-entry-statuses', 'id' => $id]);
// render replace entry modal form url
$replaceFormUrl = Url::to(['create/replace-entry-form']);
// render replace entry selection modal form url
$replaceSelectionFormUrl = Url::to(['create/replace-entry-selection-form']);
// render replacement preview url
$replacePreviewUrl = Url::to(['create/render-replacement-preview']);
// confirm replacement url
$confirmReplacementUrl = Url::to(['create/confirm-replacement']);
// retieve entry IDs url
$retrieveEntryDbIdsUrl = Url::to(['create/retrieve-entry-db-ids']);
// get entries summary url
$getEntriesSummaryUrl = Url::to(['create/get-insufficient-entries-summary', 'id' => $id]);
// config values in the form
$configValues = json_encode($configData);
// page size urls
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
// Save browser settings
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

Yii::$app->view->registerJs("
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    plantingJobId = '".$id."',
    requiredFieldJson = '".$configValues."',
    bulkUpdateFieldsUrl = '".$bulkUpdateEntryFieldsUrl."',
    getSelectedItemsUrl = '".$getSelectedItemsUrl."',
    checkAllStatusesValidUrl = '".$checkAllStatusesValidUrl."',
    sessionName = 'entry_ids',
    modelClass = 'plantingJobEntry',
    entityIdName = 'plantingJobEntryDbId',
    setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

$this->registerJsFile("@web/js/planting-instructions-manager/modal_bulk_update.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS

var gridId = '$browserId';
var resetGridUrl = '$resetGridUrl';
var checkAll = false;
var experimentId = null;
var plantingJobEntryId = null;
var selectedEntityId = null;
var entryIdToReplace = null;
var germplasmIdToReplace = null;
var packageIdToReplace = null;
var selectedReplacementType = null;
var selectedReplacementTypeText = null;
var replacementId = null;
var program = '$program';
var occurrenceName = '';
var experimentName = '';
var experimentCheckboxDbIdSessionStorageName = 'PIM_experimentCheckboxIds' + '$id'
var checkboxSessionStorageName = 'PIM_plantingJobEntryCheckboxIds' + '$id'
var entryCheckboxDbIdSessionStorageName = 'PIM_entryCheckboxIds' + '$id'
var germplasmCheckboxDbIdSessionStorageName = 'PIM_germplasmCheckboxIds' + '$id'
var packageCheckboxDbIdSessionStorageName = 'PIM_packageCheckboxIds' + '$id'
var totalCheckboxesSessionStorageName = 'PIM_entryTotalCheckboxIds' + '$id'
let retrievedGermplasmNamesSessionStorageName = 'retrievedGermplasmNames' + '$id'
var retrieveEntryDbIdsAjax = null
let totalCheckboxes = 0
let selectedExperimentDbIds = []
let checkedBoxes = []
let selectedEntryDbIds = []
let selectedGermplasmDbIds = []
let selectedPackageDbIds = []
let retrievedGermplasmNames = []
var loading = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>';
let isBulkReplacement = false

sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify([]))
sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')
sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify([]))

sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify([]))

let isSelectAllChecked = null

compareCheckBoxCountWithTotalCount()

$(document).ready(function() {

    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();

    refresh();
});

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refresh();

    // update group header styling
    setTimeout(function() {
        updateSummaryRowStyle();
    }, 100);

    sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify([]))
    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify([]))
    sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify([]))
    sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify([]))
    sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify([]))
    sessionStorage.setItem(totalCheckboxesSessionStorageName, '$totalCount')

    sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify([]))

    selectedExperimentDbIds = []
    checkedBoxes = []
    selectedEntryDbIds = []
    selectedGermplasmDbIds = []
    selectedPackageDbIds = []

    retrievedGermplasmNames = []
});

function refresh(){
    var maxHeight = ($(window).height() - 315);
    $(".kv-grid-wrapper").css("height",maxHeight);
    $('.dropdown-button').dropdown()

    // reset occurrences browser
    $('#reset-entries-grid-id').on('click', function(e) {
        e.preventDefault();
        $.pjax.reload({
            container: '#' + gridId + '-pjax',
            replace: true,
            url: resetGridUrl
        });
    });

    // update group header styling
    updateSummaryRowStyle();

    // trigger validate statuses
    setTimeout(function() {
        $(".validate-statuses-btn").trigger('click');
    },10);

    // validate statuses
    $(document).on('click','.validate-statuses-btn',function(e){

        $('#next-btn').addClass('disabled');
        $('.review-step').addClass('disabled');
        
        setTimeout(function() {
            $.ajax({
                url: '$checkAllStatusesValidUrl',
                type: 'post',
                cache: false,
                data: {
                    id : '$id'
                },
                success: function(response){
                    // if there are insufficient entries, disable next button
                    if (response == 'false') {
                        $('#next-btn').addClass('disabled');
                        $('.review-step').addClass('disabled');
                    } else {
                        $('#next-btn').removeClass('disabled');
                        $('.review-step').removeClass('disabled');
                    }
                },
            });
        },10);

        e.stopImmediatePropagation();

    });

    // select grid
    $(document).on('click','.entry-grid-select',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset;
        var url = '$storeSessionItemsUrl';

        if($(this).prop('checked')){
            unset = false;
        }else{
            unset = true;
            $('#entry-select-all').prop('checked',false);
            $('#entry-select-all').removeAttr('checked');
            checkAll = false;
        }

        $.ajax({
            url: url,
            type: 'post',
            async:false,
            data: {
                entityDbId: $(this).prop('id'),
                unset: unset,
                storeAll: false,
                plantingJobDbId : '$id',
                sessionName: 'entry_ids'
            },
            success: function(response) {
                $('#selected-count').html(response);
                if(response == 0){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
            }
        });
    });

    // select all items
    $(document).on('click','#entry-select-all',function(e){
        if ($(this).prop("checked") === true)  {
            $('.entry-grid-select').prop('checked',true)
            $(".entry-grid-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4")

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Retrieving the selected entry records. Please wait for the system to finish.'

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            $('#system-loading-indicator').html(loading);
            $('#bulk-update-btn, #reset-entries-grid-id, .btn-default').addClass('disabled');

            retrieveEntryDbIdsAjax = $.ajax({ // retrieve all entry DB IDs
                type: 'POST',
                dataType: 'json',
                url: '$retrieveEntryDbIdsUrl',
                data: {
                    plantingJobDbId: '$id',
                    sessionName: 'entry_ids'
                },
                async: true,
                success: function (response) {
                    selectedExperimentDbIds = [ ...response.experimentDbIdArray ]
                    checkedBoxes = [ ...response.plantingJobEntryDbIdArray ]
                    selectedEntryDbIds = [ ...response.entryDbIdArray ]
                    selectedGermplasmDbIds = [ ...response.germplasmDbIdArray ]
                    selectedPackageDbIds = [ ...response.packageDbIdArray ]
                    retrievedGermplasmNames = [ ...response.retrievedGermplasmNames ]

                    sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify(selectedExperimentDbIds))
                    sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
                    sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify(selectedEntryDbIds))
                    sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify(selectedGermplasmDbIds))
                    sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify(selectedPackageDbIds))

                    sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify(retrievedGermplasmNames))
                    let retrievePlotDbIdsAjax = null

                    totalCheckboxes = checkedBoxes.length
                    sessionStorage.setItem(totalCheckboxesSessionStorageName, totalCheckboxes)

                    compareCheckBoxCountWithTotalCount()

                    retrieveEntryDbIdsAjax = null

                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i>' + 'Successfully retrieved the entry records.'

                    $('.toast').css('display','none')

                    Materialize.toast(message, 5000)

                    $('#system-loading-indicator').html('');
                    $('#bulk-update-btn, #reset-entries-grid-id, .btn-default').removeClass('disabled');

                },
                error: function (xhr, error, status) {
                    if (retrieveEntryDbIdsAjax) { // cancel ajax request if it is ongoing
                        retrieveEntryDbIdsAjax.abort()
                        retrieveEntryDbIdsAjax = null
                    }

                    message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'There was an error in retrieving the entry records.'

                    $('.toast').css('display','none')
                    Materialize.toast(message, 5000)

                    $('#system-loading-indicator').html('');
                    $('#bulk-update-btn, #reset-entries-grid-id, .btn-default').removeClass('disabled');

                }
            })
        } else {
            if (retrieveEntryDbIdsAjax) { // cancel ajax request if it is ongoing
                retrieveEntryDbIdsAjax.abort()
                retrieveEntryDbIdsAjax = null

                message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i>' + 'Cancelled retrieving the entry records.'

                $('.toast').css('display','none')
                
                Materialize.toast(message, 5000)
            }
            $('.entry-grid-select').prop('checked',false)
            $("input:checkbox.entry-grid-select").parent("td").parent("tr").removeClass("grey lighten-4")

            selectedExperimentDbIds = []
            checkedBoxes = []
            selectedEntryDbIds = []
            selectedGermplasmDbIds = []
            selectedPackageDbIds = []
            retrievedGermplasmNames = []

            sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify(selectedExperimentDbIds))
            sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
            sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify(selectedEntryDbIds))
            sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify(selectedGermplasmDbIds))
            sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify(selectedPackageDbIds))
            sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify(retrievedGermplasmNames))
            compareCheckBoxCountWithTotalCount()
        }

        e.stopImmediatePropagation();
    });

    // click row in entry browser
    $(document).on('click', '#'+ gridId +' tbody tr', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        if (retrieveEntryDbIdsAjax) {
            message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i>' + 'Please wait for the system to finish retrieving the entry records.'

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)
            return
        }

        var thisRow = $(this).find('input:checkbox')[0];

        if(thisRow !== undefined){
            $("#"+thisRow.id).trigger("click");
            let experimentDbId = $('#' + thisRow.id).data('experiment-id')
            let entryDbId = $('#' + thisRow.id).data('entry-id')
            let germplasmDbId = $('#' + thisRow.id).data('germplasm-id')
            let packageDbId = $('#' + thisRow.id).data('package-id')
            let germplasmName = $('#' + thisRow.id).data('germplasm-name')

            if(thisRow.checked){
                selectedExperimentDbIds.push(experimentDbId)
                checkedBoxes.push(thisRow.id)
                selectedEntryDbIds.push(entryDbId)
                selectedGermplasmDbIds.push(germplasmDbId)
                selectedPackageDbIds.push(packageDbId)

                retrievedGermplasmNames.push(germplasmName)

                sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify(selectedExperimentDbIds))
                sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
                sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify(selectedEntryDbIds))
                sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify(selectedGermplasmDbIds))
                sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify(selectedPackageDbIds))

                sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify(retrievedGermplasmNames))

                let checkedBoxesCount = checkedBoxes.length
                compareCheckBoxCountWithTotalCount()

                $(this).addClass('grey lighten-4')
            }else{
                selectedExperimentDbIds.splice(selectedExperimentDbIds.indexOf(experimentDbId), 1)
                checkedBoxes.splice(checkedBoxes.indexOf(thisRow.id), 1)
                selectedEntryDbIds.splice(selectedEntryDbIds.indexOf(entryDbId), 1)
                selectedGermplasmDbIds.splice(selectedGermplasmDbIds.indexOf(germplasmDbId), 1)
                selectedPackageDbIds.splice(selectedPackageDbIds.indexOf(packageDbId), 1)

                retrievedGermplasmNames.splice(retrievedGermplasmNames.indexOf(germplasmName), 1)

                sessionStorage.setItem(experimentCheckboxDbIdSessionStorageName, JSON.stringify(selectedExperimentDbIds))
                sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(checkedBoxes))
                sessionStorage.setItem(entryCheckboxDbIdSessionStorageName, JSON.stringify(selectedEntryDbIds))
                sessionStorage.setItem(germplasmCheckboxDbIdSessionStorageName, JSON.stringify(selectedGermplasmDbIds))
                sessionStorage.setItem(packageCheckboxDbIdSessionStorageName, JSON.stringify(selectedPackageDbIds))

                sessionStorage.setItem(retrievedGermplasmNamesSessionStorageName, JSON.stringify(retrievedGermplasmNames))

                let checkedBoxesCount = checkedBoxes.length
                compareCheckBoxCountWithTotalCount()

                $(this).removeClass('grey lighten-4')
            }
        }
    });

    // click row in entry replacement selection browser
    $(document).on('click', '#select-replacement-grid-container tbody tr', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var thisRow = $(this).find('input:radio')[0];

        replacementId = $(thisRow).attr('id');

        if(thisRow !== undefined){
            $("#"+replacementId).prop("checked", true);
        }
    });

    // confirm bulk update
    $(document).on('click','.entry-confirm-update-btn',function(e){

        e.preventDefault();
        e.stopImmediatePropagation();

        var selectedItems= [];
        var selectedItemsCount = 0;
        var mode = $(this).attr('mode');

        selectedItemsCount = $('#selected-count').html();

        var check = Number.isInteger(parseInt(selectedItemsCount))

        // get total count in the browser
        var totalCountStr = $('div.summary b').last().text().replace(",", "");
        var totalCount = parseInt(totalCountStr);

        selectedItemsCount = Number.isInteger(parseInt(selectedItemsCount)) ? parseInt(selectedItemsCount) : 'No';

        mode = (selectedItemsCount == totalCount) ? 'all' : mode;

        if((selectedItemsCount > 0 && selectedItemsCount !== 'No' && mode == "selected") || mode == "all"){
            // no variable selected 
            if(typeof variable === 'undefined' || variable == null || variable == ''){
                // hide loading indicator
                $('#modal-content').removeClass('hidden');
                $('#modal-progress').addClass('hidden');
                $('.confirm-update-btn').removeClass('disabled');
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>No variable selected.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            } else{
                var field = checkField();
                if(field['success'] == true){
                    // show loading indicator
                    $('#modal-content').removeClass('hidden');
                    $('#modal-progress').removeClass('hidden');
                    $('.entry-confirm-update-btn').addClass('disabled');

                    var dbIds = (checkedBoxes.length === '$totalCount') ? [] : JSON.stringify(checkedBoxes)

                    $.ajax({
                        url: '$bulkUpdateEntryFieldsUrl',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            mode: mode,
                            variable: variable,
                            dataValue: field['value'],
                            plantingJobDbId: plantingJobId,
                            dbIds: dbIds,
                            entityIdName: 'plantingJobEntryDbId',
                            totalCount: totalCount,
                            plantingJobCode: '$plantingJobCode',
                            program: program
                        },
                        success: function(response) {
                            // update entries summary
                            updateEntriesSummary();

                            variable = response['variable'];

                            var variableId = response['variableDbId'];
                            var updatedItemsCount = (response['count'] == null) ? $totalCount : response['count'];
                            $.each(response['selectedItems'], function(index, value){
                                var type = 'identification';
                                var fieldName = variable + '-' + type + '-' + variableId + '-' + value;

                                if(field['inputType'] == 'dropdown'){
                                    $('#' + fieldName).val(field['value']);
                                    $('#select2-' + fieldName + '-container').html(field['valueText'])
                                }
                                else{
                                    $('#' + fieldName).val(field['value']);
                                }
                            });
                       
                            if(!hasToast){
                                $('.toast').css('display','none');
                                var s = (updatedItemsCount) > 1 ? 's' : '';
                                var label = (mode == 'all') ? totalCount : updatedItemsCount;
                                var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated <b>"+ label +"</b> item"+s+".</span>";
                                Materialize.toast(notif, 5000);
                                $("[class*=input-validate]").val(null).trigger("change"); 
                                $("[class*=input-validate]").select2("val", "");
                            }

                            // hide loading indicator
                            $('#modal-content').removeClass('hidden');
                            $('#modal-progress').addClass('hidden');
                            $('.entry-confirm-update-btn').removeClass('disabled');

                            $(".validate-statuses-btn").trigger('click');
                        },
                        error: function(e) {
                        }
                    });
                }

                e.preventDefault();
                e.stopImmediatePropagation();
            }
        }else{ // no items selected
            // hide loading indicator
            $('#modal-content').removeClass('hidden');
            $('#modal-progress').addClass('hidden');
            $('.confirm-update-btn').removeClass('disabled');
            var hasToast = $('body').hasClass('toast');
            if(!hasToast){
                $('.toast').css('display','none');
                var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>No items selected.</span>";
                Materialize.toast(notif, 5000);
            }
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

    // render bulk update form
    $(document).on('click', '#bulk-update-btn', function() {

        $('#config-var').removeAttr('tabindex');
        
        $.ajax({
            url: '$renderBulkUpdateUrl', 
            type: 'post',
            dataType: 'json',
            data: {
                configVars: '$configValues'
            },
            success: function(response){
                
                $('#config-var').html(response);
                $('.loading-req-vars').remove();
                $('#req-vars').css('visibility','visible');
                $('#req-vars').css('width','');
                $('#req-vars').css('height','');
            }
        });
    });

    // save value upon change
    $(document).on('keypress keyup blur paste change', ".form-fields", function(event) {

        var id = this.id;
        var fields = (this.id).split('-');
        var column = fields[0];
        var targetTable = fields[1];
        var plantingJobEntryDbId = fields[3];
        var value = $('#' + id).val();

        // if value is empty, disable next button

        if(value == ''){
            $('#next-btn').addClass('disabled');
            $(this).addClass('invalid');
        }else{
            $(this).removeClass('invalid');

            // Save the changes in the database
            $.ajax({
                url: '$saveRowValueUrl',
                type: 'post',
                cache: false,
                data: {
                    entityDbId: plantingJobEntryDbId,
                    column: column,
                    targetTable: targetTable,
                    value: value,
                    entityIdName: 'plantingJobEntryDbId',
                    modelClass: 'plantingJobEntry'
                },
                success: function(response){
                    $(".validate-statuses-btn").trigger('click');

                    updateEntriesSummary();
                },
            });
        }
    });

    // replace entry
    $(document).on('click', '.replace-entry-btn', function() {

        var obj = $(this);
        var data = obj.attr('data-summary');

        plantingJobEntryId = obj.attr('data-id');
        plantingJobDbId = obj.attr('data-planting_job_id');
        experimentId = obj.attr('data-experiment_id');
        entryIdToReplace = obj.attr('data-entry_id');
        germplasmIdToReplace = obj.attr('data-germplasm_id');
        packageIdToReplace = obj.attr('data-package_id');

        occurrenceName = obj.attr('data-occurrence');
        experimentName = obj.attr('data-experiment');

        isBulkReplacement = false

        // reduce modal size to regular
        $('#replace-entry-modal').find('.modal-dialog').removeClass('modal-xl').addClass('modal-lg')

        // update header
        $('.replace-entry-header').html('<i class="material-icons inline-material-icon" style="margin-right: 4px;">swap_horiz</i> Replace entry')
        $('.progress').removeClass('hidden');
        $('.replace-entry-modal-content').html('');

        setTimeout(function() {
            $.ajax({
                url: '$replaceFormUrl',
                type: 'post',
                cache: false,
                data: {
                    data : data
                },
                success: function(response){

                    $('.progress').addClass('hidden');
                    $('.replace-entry-modal-content').html(response);

                    $('.replace-occurrence-name').html(occurrenceName);
                    $('.replace-experiment-name').html(experimentName);

                },
                error: function(response){

                    $('.progress').addClass('hidden');
                    $('.replace-entry-modal-content').html('<i>There was a problem while loading the content.</i>');

                },
            });
        },100);
    });

    // replace entry next button
    $(document).on('click', '.replace-entry-next-btn', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        // render form content
        showReplacementForm(experimentId);

        // show preview panel
        panelAction('child', 'show');
        panelAction('parent', 'hide');

        // update buttons
        $('.select-next-btn').removeClass('hidden');
        $('.replace-entry-next-btn').addClass('hidden');
    });

    // cancel replace entry
    $(document).on('click', '.cancel-replace-entry-btn', function(e){
        selectedReplacementType = null
        selectedReplacementTypeText = null
        resetReplaceModal();
    });

    // select entity as a replacement
    $(document).on('change', 'input[type=radio][name=selection-radio-button]', function() {
        replacementId = this.value;
    });

    // select record
    $(document).on('click', '.select-next-btn', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.progress').removeClass('hidden');
        $('.replace-entry-form-modal-content').addClass('hidden');

        // render form content
        showReplacementPreview(replacementId, occurrenceName, experimentName);

    });

    // confirm selection
    $(document).on('click', '.select-confirm-btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.progress').removeClass('hidden');
        $('.replace-entry-form-modal-content').html('');
            
        setTimeout(function () {
            $.ajax({
                url: '$confirmReplacementUrl',
                type: 'post',
                cache: false,
                data: {
                    plantingJobEntryId: (isBulkReplacement) ? checkedBoxes : plantingJobEntryId,
                    plantingJobId: '$id',
                    isBulkReplacement: isBulkReplacement,
                },
                success: function(response) {
                    const res = (typeof response == 'string') ? JSON.parse(response) : response

                    $('.progress').addClass('hidden');

                    if (res.true) { // display success if there is at least 1 successful replacement
                        $('#replace-entry-modal').modal('hide')

                        if (isBulkReplacement) {
                            // display notification
                            let entry = (res.true > 1) ? 'entries' : 'entry'
                            let failedReplacements = (res.false > 0) ? ` <span class='red-text text-lighten-1' style='font-weight: bold;'>\${res.false}</span> entries failed to be replaced.` : ``
                            let message = `Successfully replaced <span class='green-text text-lighten-1' style='font-weight: bold;'>\${res.true}</span> \${entry}.\${failedReplacements}`
                            let notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+ message +"</span>"
                            Materialize.toast(notif, 5000)

                            // update each button of selected entries
                            for (const id of checkedBoxes) {
                                $('.replace-entry-btn-' + id).text('Replaced');
                                $('.replace-entry-btn-' + id).removeClass('grey black-text lighten-2')
                            }
                        } else {
                            // display notification
                            let message = 'Successfully selected replacement for the entry.'
                            let notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>"+ message +"</span>"
                            Materialize.toast(notif, 5000)

                            // update button
                            $('.replace-entry-btn-' + plantingJobEntryId).text('Replaced');
                            $('.replace-entry-btn-' + plantingJobEntryId).removeClass('grey black-text lighten-2')
                        }

                        $('#reset-entries-grid-id').trigger('click')
                        resetReplaceModal()
                    } else {
                        $('.replace-entry-form-modal-content').html('<i>There was a problem while replacing the entry.</i>');
                    }
                },
                error: function (response) {
                    $('.replace-entry-form-modal-content').html('<i>There was a problem while replacing the entry.</i>');

                },
            });
        }, 100);

        selectedReplacementType = null
        selectedReplacementTypeText = null
    });

    // go back to parent panel
    $(document).on('click', '.go-back-to-replace-entry-parent-panel', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        // reduce modal size to regular
        $('#replace-entry-modal').find('.modal-dialog').removeClass('modal-xl').addClass('modal-lg')

        selectedReplacementType = null
        selectedReplacementTypeText = null
        $('.modal-body').css('max-height','550px');

        panelAction('parent', 'show');
        panelAction('child', 'hide');

        // show next button
        $('.replace-entry-next-btn').removeClass('hidden');
        $('.select-next-btn').addClass('hidden');

    });

    // go back to selection panel
    $(document).on('click', '.go-back-to-entry-selection-parent-panel', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        $('.modal-body').css('max-height','550px');

        // update buttons
        $('.select-confirm-btn').addClass('hidden');
        $('.select-next-btn').removeClass('hidden');   

        // update modal content
        $('.replace-entry-preview-modal-content').addClass('hidden');
        $('.replace-entry-form-modal-content').removeClass('hidden');
    });

    // choose a bulk entry replacement option
    $(document).on('click', '.replace-entry-option', function (e) {
        if (retrievedGermplasmNames.length == 0) {
            message = `
                <i
                    class="material-icons orange-text"
                    style="margin-right: 8px;"
                >warning</i> Please select at least one (1) entry.`

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            return
        } else if (retrievedGermplasmNames.length == 1) { // make it so that Single replace entry UI is provided instead
            selectedReplacementType = $(this).data('replacement-type')
            selectedReplacementTypeText = $(this).data('replacement-type-text')
            isBulkReplacement = false

            $('#replace-entry-modal').find('.modal-body').addClass('hidden')
            $('.replace-entry-btn-' + checkedBoxes[0]).trigger('click')

            $('.replace-entry-next-btn').trigger('click')
            $('#replace-entry-modal').find('.modal-body').removeClass('hidden')

            return
        }

        // make copies and remove duplicates
        tempSelectedExperimentDbIds = selectedExperimentDbIds.filter( (experimentDbId, index) =>
            selectedExperimentDbIds.indexOf(experimentDbId) === index
        )
        tempSelectedEntryDbIds = selectedEntryDbIds.filter( (entryDbId, index) =>
            selectedEntryDbIds.indexOf(entryDbId) === index
        )
        tempSelectedGermplasmDbIds = selectedGermplasmDbIds.filter( (germplasmDbId, index) =>
            selectedGermplasmDbIds.indexOf(germplasmDbId) === index
        )
        tempSelectedPackageDbIds = selectedPackageDbIds.filter( (packageDbId, index) =>
            selectedPackageDbIds.indexOf(packageDbId) === index
        )
        tempRetrievedGermplasmNames = retrievedGermplasmNames.filter( (germplasmName, index) =>
            retrievedGermplasmNames.indexOf(germplasmName) === index
        )

        if (tempRetrievedGermplasmNames.length > 1) {
            message = `
                <i
                    class="material-icons orange-text"
                    style="margin-right: 8px;"
                >warning</i> Please select entries that have the same germplasm.`

            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            return
        }

        isBulkReplacement = true
        selectedReplacementType = $(this).data('replacement-type')
        selectedReplacementTypeText = $(this).data('replacement-type-text')

        // change modal header
        $('.replace-entry-header').html('<i class="material-icons inline-material-icon" style="margin-right: 4px;">swap_horiz</i> Bulk replace entry')

        // maximize modal size
        $('#replace-entry-modal').find('.modal-dialog').removeClass('modal-lg').addClass('modal-xl')

        // display modal
        $('#replace-entry-modal').modal('show')
        $('.progress').removeClass('hidden')

        // clear modal contents
        $('.replace-entry-modal-content').html('')
        $('.replace-entry-form-modal-content').html('')
        $('.parent-panel').css('display','none')
        $('.child-panel').css('display','block')

        // display select-next button instead of replace-entry-next button
        $('.select-next-btn').removeClass('hidden')
        $('.select-next-btn').addClass('disabled')
        $('.replace-entry-next-btn').addClass('hidden')

        setTimeout(function () {
            $.ajax({
                url: '$replaceSelectionFormUrl',
                type: 'post',
                cache: false,
                dataType: 'json',
                data: {
                    plantingJobEntryDbIds: checkedBoxes,
                    replacementType: selectedReplacementType,
                    experimentId: tempSelectedExperimentDbIds,
                    entryIdToReplace: tempSelectedEntryDbIds,
                    germplasmIdToReplace: tempSelectedGermplasmDbIds,
                    packageIdToReplace: tempSelectedPackageDbIds,
                },
                success: function (response) {
                    $('.progress').addClass('hidden')
                    
                    $('.replace-entry-form-modal-content').removeClass('hidden')
                    $('.replace-entry-form-modal-content').html(response.data)

                    // if no available records for replacement, hide next button
                    if (!response.count) {
                        $('.select-next-btn').addClass('disabled')
                    } else {
                        $('.select-next-btn').removeClass('disabled')

                        $('#select-replacement-grid-container tbody tr:first-child').trigger('click')

                        // If there is only 1 available record, auto-redirect user to the next step
                        if (parseInt(response.count) == 1) {
                            $('.select-next-btn').trigger('click')
                        }
                    }
                },
            })
        }, 50)
    })
}

// update entry summary
function updateEntriesSummary(){
    setTimeout(function() {
        $.ajax({
            url: '$getEntriesSummaryUrl',
            type: 'post',
            cache: false,
            success: function(response){
                $('.insuff-summary').html(response);
            },
        });
    }, 50);
}

// update summary styling
function updateSummaryRowStyle(){
    $('[data-summary-col-seq=2]').css('line-height','2.6');
    $('[data-summary-col-seq=2]').css('color','transparent');
    $('[data-summary-col-seq=1]').css('vertical-align','middle');

    $('[data-summary-col-seq=3]').css('line-height','2.6');
    $('[data-summary-col-seq=4]').css('line-height','2.6');
    $('[data-summary-col-seq=5]').css('line-height','2.6');
    $('[data-summary-col-seq=6]').css('line-height','2.6');
    $('[data-summary-col-seq=7]').css('line-height','2.6');
    $('[data-summary-col-seq=8]').css('line-height','2.6');
    $('[data-summary-col-seq=9]').css('line-height','2.6');
    $('[data-summary-col-seq=11]').css('line-height','2.6');
}

// show replacement entry form
function showReplacementForm(experimentId){
    $('#replace-entry-modal').find('.modal-dialog').removeClass('modal-lg').addClass('modal-xl')
    
    var radios = $("input[name='replacement-type']");
    var selectedObj = radios.filter(":checked");
    selectedReplacementType = selectedReplacementType ?? $(selectedObj).val();
    selectedReplacementTypeText = selectedReplacementTypeText ?? $(selectedObj).attr('text');

    $('.progress').removeClass('hidden');
    $('.replace-entry-form-modal-content').html('');
    $('.select-next-btn').addClass('disabled');

    setTimeout(function() {
        $.ajax({
            url: '$replaceSelectionFormUrl',
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {
                experimentId: experimentId,
                replacementType: selectedReplacementType,
                entryIdToReplace: entryIdToReplace,
                germplasmIdToReplace: germplasmIdToReplace,
                packageIdToReplace: packageIdToReplace
            },
            success: function(response){
                $('.progress').addClass('hidden');

                $('.replace-entry-form-modal-content').removeClass('hidden');
                $('.replace-entry-form-modal-content').html(response.data);
                
                // if no available records for replacement, hide next button
                if(!response.count){
                    $('.select-next-btn').addClass('disabled');
                } else {
                    $('.select-next-btn').removeClass('disabled');

                    $('#select-replacement-grid-container tbody tr:first-child').trigger('click');

                    // If there is only 1 available record, auto-redirect user to the next step
                    if (parseInt(response.count) == 1) {
                        $('.select-next-btn').trigger('click')
                    }
                }
            },
        });
    }, 50);
}

// display replacement preview
function showReplacementPreview(replacementId, occurrenceName, experimentName){
    $('.select-next-btn').addClass('disabled');    

    setTimeout(function() {
        $.ajax({
            url: '$replacePreviewUrl',
            type: 'post',
            cache: false,
            dataType: 'json',
            data: {
                experimentId: (isBulkReplacement) ? selectedExperimentDbIds : experimentId,
                replacementType: selectedReplacementType,
                replacementTypeText: selectedReplacementTypeText,
                replacementId: replacementId,
                plantingJobEntryId: (isBulkReplacement) ? checkedBoxes : plantingJobEntryId,
                isBulkReplacement: isBulkReplacement,
            },
            success: function(response){
                $('.progress').addClass('hidden');

                $('.replace-entry-preview-modal-content').removeClass('hidden');
                $('.replace-entry-preview-modal-content').html(response);

                $('.replace-occurrence-name').html(occurrenceName);
                $('.replace-experiment-name').html(experimentName);

                // update buttons
                $('.select-confirm-btn').removeClass('hidden');
                $('.select-next-btn').addClass('hidden');
                $('.select-next-btn').removeClass('disabled');
            },
        });
    }, 100);
}

// replace modal content
function resetReplaceModal(){
    // update content panels
    $('.parent-panel').removeClass('slide-out-parent');
    $('.parent-panel').css('display','block');
    $('.child-panel').removeClass('slide-in-child');
    $('.child-panel').css('display','none');
    $('.replace-entry-preview-modal-content').addClass('hidden');

    // update buttons
    $('.select-next-btn').addClass('hidden');
    $('.select-next-btn').removeClass('disabled');
    $('.select-confirm-btn').addClass('hidden');
    $('.replace-entry-next-btn').removeClass('hidden');
}

// compare check box count with total count
function compareCheckBoxCountWithTotalCount () {
    if (totalCheckboxes > 0 && totalCheckboxes > checkedBoxes.length) {
        $('#entry-select-all').prop('checked',false);

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-count').html(checkedBoxes.length)
            $('#selected-items-text').html('item')
        } else if(checkedBoxes.length > 1) {
            $('#selected-count').html(checkedBoxes.length.toLocaleString())
            $('#selected-items-text').html('items')
        } else if (checkedBoxes.length === 0) {
            $('#selected-count').html('No')
            $('#selected-items-text').html('items')
        }
    } else if (totalCheckboxes > 0 && totalCheckboxes === checkedBoxes.length) {

        isSelectAllChecked = true
        $('#entry-select-all').prop('checked',true);

        // update checked box counter text
        if (checkedBoxes.length === 1) {
            $('#selected-count').html(checkedBoxes.length)
            $('#selected-items-text').html('item')
        } else if(checkedBoxes.length > 1) {
            $('#selected-count').html(checkedBoxes.length.toLocaleString())
            $('#selected-items-text').html('items')
        }
    } else if (checkedBoxes.length == 1){
        $('#selected-items-text').html('item')
    } else if (checkedBoxes.length == 0 || checkedBoxes.length > 1){
        $('#selected-items-text').html('items')
    }

}

JS);

?>

<style type="text/css">
    .hidden-content{
        color: transparent;
    }

    td[data-col-seq=3]{
        border-top: 1px solid transparent;
    }

    .modal-body {
        overflow-x: hidden;
    }
</style>