<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\components\GenericFormWidget;
use yii\bootstrap\ActiveForm;
use kartik\dynagrid\DynaGrid;
use macgyer\yii2materializecss\widgets\Button;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders Review and submit step for Create Packing job
 */

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id, 
    'btnId' => 'submit-btn',
    'program' => $program, 
    'active' => 'review',
    'plantingJobCode' => $plantingJobCode,
    'label' => 'Submit',
    'icon' => 'send',
    'title' => 'Submit Packing job creation',
    'options' => [
        'data-toggle' => 'modal',
        'data-target' => '#submit-modal',
        'data-name' => $plantingJobCode,
        'data-id' => $id
    ]
]);

$summaryString = '';
foreach ($summary as $key => $value) {
    $label = isset($value['label']) ? $value['label'] : '';
    $value = isset($value['value']) ? $value['value'] : '';

    $summaryString .= '<span>'.$label.': </span>' . '<b>'.number_format($value).'</b><br/>';
}

echo '
<div class="col s12 m7">
    <div class="card horizontal">
        <div class="card-stacked">
            <div class="card-content">
                <h5 style="margin-top:0px">Packing Job Summary</h5>'.
                $summaryString .
                // additional instructions
                '<ul class="collapsible" style="margin-top:15px">
                    <li title="Specify additional instructions for the Packing job">
                        <div class="collapsible-header">
                            <span class="secondary-content">Additional instructions</span>
                        </div>

                        <div class="collapsible-body" style="padding:0px">
                            <div style="margin: 25px 25px 0px 25px;">'.
                                GenericFormWidget::widget([
                                    'options' => $widgetOptions
                                ]).
                            '</div>
                            <div class="row"></div>
                        </div>
                    </li>    
                </ul>
            </div>
        </div>
    </div>
</div>
';

// loop through each experiments
$count = 1;
foreach ($experimentsSummary as $key => $value) {
    $summaryString = '';
    foreach ($value['summary'] as $k => $v) {
        $label = isset($v['label']) ? $v['label'] : '';
        $val = isset($v['value']) ? $v['value'] : '';

        if($label == 'experimentDbId'){
            $experimentId = $val;
        }else{
            $summaryString .= '<div class="col col-md-3"><span>'.$label.': </span>' . '<b>'.number_format($val).'</b></div>';
        }
    }

    echo '<ul class="collapsible" style="margin-top:15px">
        <li title="View entries of the Experiment">
            <div class="collapsible-header" data-experiment_id="'.$experimentId.'">
                <span class="secondary-content">'.$count.'.) '.$key.' Summary</span>
            </div>

            <div class="collapsible-body" style="padding:0px">
                <div style="margin: 25px 25px 0px 25px;">'.
                    $summaryString.
                    '<div class="row"></div>
                    <div class="entry-summary-'. $experimentId .'" data-experiment_id="'.$experimentId.'">
                    '.
                    // render entries summary per experiment
                    Yii::$app->controller->renderPartial('_entries_summary', [
                        'entriesDataProvider' => $entriesDataProvider[$experimentId],
                        'id' => 'entry-summaries-'.$experimentId
                    ]).
                    '
                    </div>
                </div>
                <div class="row"></div>
            </div>
        </li>    
    </ul>';

    $count++;
}

// submit Packing job modal
Modal::begin([
    'id' => 'submit-modal',
    'header' => '<h4>
            <i class="material-icons" style="vertical-align:bottom">check</i>
            Submit    
            <span class="submit-label-modal"></span>
        </h4>',
    'footer' => 
        Html::a('Cancel', ['#'], [
            'id' => 'cancel-save', 
            'title' => \Yii::t('app', 'Cancel'),
            'data-dismiss'=> 'modal'
        ]) . '&emsp;&nbsp;'.
        Button::widget([
            'label' => 'Confirm',
            'options' => [
                'class' => 'submit-confirm-btn',
                'href' => '#!',
                'title' => \Yii::t('app', 'Confirm submit Packing job creation')
            ],
        ])
        . '&emsp;&emsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'options' => [
        'data-backdrop' => 'static'
    ],
]);

?>
<div class="submit-modal-body">
    <div class="submit-confirmation-panel"><i class="fa fa-warning orange-text text-darken-3 fa-2x"></i>
        You are about to submit the Packing Job. Upon confirmation, the status of Packing job will be <i>Ready for packing</i>. Click confirm to proceed.
    </div>
    <div class="submit-confirmation-loading-panel hidden">
        <div class="margin-right-big loading">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>
</div>
<?php Modal::end();

// submit Packing job creation URL
$submitUrl = Url::to(['/plantingInstruction/create/submit', 'id' => $id, 'code' => $plantingJobCode, 'program' => $program]);
// save additional instructions
$saveValueUrl = Url::to(['/plantingInstruction/create/save-planting-job-info', 'id' => $id]);

$this->registerJs(<<<JS
    var plantingJobId = $id;

    // initialize collapsible element
    $('.collapsible').collapsible();

    // render job code in commit modal
    $(document).on('click','#submit-btn', function(){

        var obj = $(this);
        var name = obj.data('name');
        var id = obj.data('id');

        // add it to the modal header as text
        $('.submit-label-modal').html(name);

        $('.submit-confirm-btn').attr('data-id', id);
        $('.submit-confirm-btn').attr('data-name', name);

    });

    // submit Packing job creation
    $(document).on('click','.submit-confirm-btn', function(){

        // show loading indicator
        $('.submit-confirmation-loading-panel').removeClass('hidden');
        $('.submit-confirmation-panel').addClass('hidden');

        $.ajax({
            url: '$submitUrl',
            type: 'post',
            success: function(response){
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $('.submit-confirmation-panel').html(errorMessage);
                $('.submit-confirm-btn').addClass('disabled');
            }
        });
    });

    // save value upon change
    $(document).on('keypress keyup blur paste change', ".editable-field", function(event) {
        
        var id = this.id;
        var fields = (this.id).split('-');
        var column = fields[0];
        var value = $('#' + id).val();
        var isValid = true;

        // validate due date
        if(column == 'PLANTING_JOB_DUE_DATE' && value != ''){
            isValid = isValidDate(value);
        }

        var groupDiv = $(this).closest('div.control-group');
        if(isValid === true){
            // enable submit button
            $('#submit-btn').removeClass('disabled');

            // remove invalid class 
            $(this).removeClass('invalid');

            // remove error message
            $(groupDiv).find('p.help-block-error').text('');
            $(this).closest('div.control-group').removeClass('has-error');

            // Save the changes in the database
            $.ajax({
                url: '$saveValueUrl',
                type: 'post',
                cache: false,
                data: {
                    column: column,
                    value: value,
                },
                success: function(response){
                },
            });
        } else {
            // disable submit button
            $('#submit-btn').addClass('disabled');

            // add invalid class 
            $(this).addClass('invalid');

            if(isValid === false){
                isValid = 'Please enter a valid date';
            }

            // add error message
            $(groupDiv).find('p.help-block-error').html(isValid);
            $(this).closest('div.control-group').addClass('has-error');
        }

    });

    // check if date is valid
    function isValidDate(dateString) {
        var isValid = true;
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        
        isValid = dateString.match(regEx) != null;

        // if format is valid, check if year, month and day are valid
        if(isValid){
            var dateArr = dateString.split("-");

            var year = dateArr[0];
            var month = dateArr[1];
            var day = dateArr[2];

            if(year < 1970){
                return 'Please enter year later than 1969';
            }

            if(month > 12){
                return 'Please enter a valid month';
            }

            if(day > 31){
                return 'Please enter a valid day';
            }
        }

        return isValid;
    }

JS);

$this->registerCss('
    .input-group{
        display: block;
    }

    .has-error .help-block{
        color: #a94442 !important;
    }
');