<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\dynagrid\DynaGrid;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders Envelopes step for Create Packing job
 */

// entry management URL
$entryMgtUrl = Url::to(['create/entry', 'program' => $program, 'id' => $id]);

echo Yii::$app->controller->renderPartial('tabs', [
    'id' => $id, 
    'program' => $program, 
    'active' => 'envelopes',
    'plantingJobCode'=> $plantingJobCode,
    'url' => $entryMgtUrl
]);

$selectedItems = isset($selectedItems) ? $selectedItems : [];
$selectedItemsCount = count($selectedItems) > 0 ? count($selectedItems) : 'No';

$columns = [
    [
        //checkbox
        'label'=> "Checkbox",
        'header'=>'
            <input type="checkbox" class="filled-in" id="occurrence-select-all" />
            <label for="occurrence-select-all"></label>
        ',
        'content'=>function($data) use ($selectedItems){
            $plantingJobOccurrenceDbId = $data['plantingJobOccurrenceDbId'];

            if(in_array($plantingJobOccurrenceDbId, $selectedItems)){
                $checkbox = '<input class="occurrence-grid-select filled-in" type="checkbox" checked="checked" id="'.$data['plantingJobOccurrenceDbId'].'" />';
            }
            else{
                $checkbox = '<input class="occurrence-grid-select filled-in" type="checkbox" id="'.$data['plantingJobOccurrenceDbId'].'" />';
            }
            return $checkbox.'
                <label for="'.$data['plantingJobOccurrenceDbId'].'"></label>       
            ';
        },        
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'hiddenFromExport'=>true,
        'mergeHeader'=>true,
        'order'=> DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'attribute' => 'experimentCode',
        'format' => 'raw',
        'group' => true,
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'groupHeader' => function ($model) {

            return [
                // content to show in each summary cell
                'content' => [
                    1 => $model['experimentCode'],
                    2 => $model['experimentName']
                ],
                'options' => ['class' => 'bold light-green lighten-4 table-info' . $model['experimentName']]
            ];
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'experimentName',
        'group' => true,
        'format' => 'raw',
        'group' => true,
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentCode'] . '</span>';
        },
        'order' => DynaGrid::ORDER_FIX_LEFT
    ],
    [
        'attribute' => 'occurrenceName',
        'label' => 'Occurrence',
        'vAlign'=>'middle'
    ],
    [
        'attribute' => 'siteCode',
        'label' => 'Site',
        'vAlign'=>'middle'
    ],
    [
        'attribute' => 'occurrenceEntryCount',
        'label' => 'Entry Count',
        'vAlign'=>'middle',
        'value' => function ($model) {
            return number_format($model['occurrenceEntryCount']);
        },
    ],
    [
        'attribute' => 'occurrencePlotCount',
        'label' => 'Plot Count',
        'vAlign'=>'middle',
        'value' => function ($model) {
            return number_format($model['occurrencePlotCount']);
        },
    ],
    [
        'attribute' => 'plotDimensions',
        'vAlign'=>'middle'
    ]
];

$columns =array_merge($columns, $formColumns);

$browserId = 'planting-job-occurrences-browser';

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => 'cookie',
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "Set the quantities of seeds per envelope, unit and envelopes per plot for each Occurrence in your packing job. Values are saved upon change.").
            	'&emsp;'.'{summary}<br/><p id = "selected-items" class = "pull-right" style = "margin:-1px"><b id = "selected-count">'.$selectedItemsCount.'</b> selected items.</p>',
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                Html::beginTag('div', ['class' => 'text-left dropdown']) .
                    
                    Html::button('<i class="glyphicon glyphicon-edit"></i> ', [
                        'type'=>'button', 
                        'title'=>Yii::t('app', 'Bulk update'), 
                        'class'=>'btn', 
                        'id'=>'bulk-update-btn',
                        'data-toggle' => 'modal',
                        'data-target' => '#bulk-update-modal',
                        'style' => 'margin-left: 5px'
                    ]) .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-envelopes-grid-id'
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();

// modal for bulk update
Modal::begin([
    'id'=> 'bulk-update-modal',
    'header' => '<h4><i class="glyphicon glyphicon-edit"></i>  '.\Yii::t('app','Bulk Update').'</h4>',
    'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-bulk-update-btn','data-dismiss'=>'modal']).'&emsp;&nbsp;'.
    '<a id="bulk-update-selected" class="btn btn-primary green waves-effect waves-light confirm-update-btn" mode ="selected">'.\Yii::t('app','Selected').'</a>&nbsp;'.
    '<a id="bulk-update-all" class="btn btn-primary teal waves-effect waves-light confirm-update-btn" mode ="all">'.\Yii::t('app','All').'</a>'.'&emsp;',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static', 'tabindex' => false],
]);
    echo '<div style="margin: 10px 20px 5px 28px;"><div id="modal-progress" class="progress hidden"><div class="indeterminate"></div></div></div>';
    echo '<div id="modal-content"><p id="instructions" style="margin: 10px 0px 5px 28px">Select variable and specify value to bulk update.</p>';
    echo '<span id="note"></span>';
    echo '<div id="config-var" style="margin-left:30px;"></div></div>';
Modal::end();

// reset grid url
$resetGridUrl = Url::toRoute(['/plantingInstruction/create?program=' . $program .'&id='.$id.'&reset=hard_reset']);
// store selected items in session
$storeSessionItemsUrl = Url::to(['create/store-session-items']);
// render bulk update form url
$renderBulkUpdateUrl = Url::to(['create/render-bulk-update']);
// render field form in bulk update
$genericRenderConfigUrl = Url::to(['create/render-config-form']);
// get selected items url
$getSelectedItemsUrl = Url::to(['create/get-selected-items']);
// save row values url
$saveRowValueUrl = Url::to(['create/save-row-value']);
// bulk update fields url
$bulkUpdateFieldsUrl = Url::to(['create/bulk-update-fields']);

$configValues = json_encode($configData);

Yii::$app->view->registerJs("
    genericRenderConfigUrl = '".$genericRenderConfigUrl."',
    plantingJobId = '".$id."',
    requiredFieldJson = '".$configValues."',
    bulkUpdateFieldsUrl = '".$bulkUpdateFieldsUrl."',
    getSelectedItemsUrl = '".$getSelectedItemsUrl."',
    sessionName = 'occurrence_ids',
    modelClass = 'plantingJobOccurrence',
    entityIdName = 'plantingJobOccurrenceDbId'
    ;",
\yii\web\View::POS_HEAD);

$this->registerJsFile("@web/js/planting-instructions-manager/modal_bulk_update.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS

var gridId = '$browserId';
var resetGridUrl = '$resetGridUrl';
var checkAll = false;

$(document).ready(function() {
    refresh();
});

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refresh();

    // update group header styling
    setTimeout(function() {
        $('[data-summary-col-seq=1]').css('line-height','3.2');
        $('[data-summary-col-seq=2]').css('vertical-align','middle');
    }, 100);

    // remove select all
    $('#occurrence-select-all').prop('checked',false);
    $('#occurrence-select-all').removeAttr('checked');
});

function refresh(){
	var maxHeight = ($(window).height() - 315);
    $(".kv-grid-wrapper").css("height",maxHeight);

    // reset occurrences browser
    $('#reset-envelopes-grid-id').on('click', function(e) {
        e.preventDefault();
        $.pjax.reload({
            container: '#' + gridId + '-pjax',
            replace: true,
            url: resetGridUrl
        });
    });

    // update group header styling
    $('[data-summary-col-seq=1]').css('line-height','3.2');
    $('[data-summary-col-seq=2]').css('vertical-align','middle');

    checkSelectAll();

    // select grid
    $(document).on('click','.occurrence-grid-select',function(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset;
        var url = '$storeSessionItemsUrl';

        if($(this).prop('checked')){
            unset = false;
        }else{
            unset = true;
            $('#occurrence-select-all').prop('checked',false);
            $('#occurrence-select-all').removeAttr('checked');
            checkAll = false;
        }

        $.ajax({
            url: url,
            type: 'post',
            async:false,
            data: {
                entityDbId: $(this).prop('id'),
                unset: unset,
                storeAll: false,
                plantingJobDbId : '$id',
                sessionName: 'occurrence_ids'
            },
            success: function(response) {
                $('#selected-count').html(response);
                if(response == 0){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
            }
        });
    });

    checkSelectAll();

    // select all occurrences
    $(document).on('click','#occurrence-select-all',function(e){

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.occurrence-grid-select').attr('checked','checked');
            $('.occurrence-grid-select').prop('checked',true);
            $(".occurrence-grid-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            checkAll = true;
        }else{

            $(this).removeAttr('checked');
            $('.occurrence-grid-select').prop('checked',false);
            $('.occurrence-grid-select').removeAttr('checked');
            $("input:checkbox.occurrence-grid-select").parent("td").parent("tr").removeClass("grey lighten-4");
            checkAll = false;
        }

        var count = $('#selected-count').val() == '' ? 0 : $('#selected-count').val();
        var unset = $(this).prop('checked') ? false : true;
        var url = '$storeSessionItemsUrl';

        $.ajax({
            url: url,
            type: 'post',
            async: false,
            data: {
                entityDbId: 0,
                unset: unset,
                storeAll: true,
                plantingJobDbId : '$id',
                sessionName: 'occurrence_ids'
            },
            success: function(response) {
                $('#selected-count').html(response);
                
                if(unset){
                    $('#selected-count').html('No');
                }
            },
            error: function(e) {
            }
        });

    });

    // click row in occurrences browser
    $(document).on('click', '#'+ gridId +' tbody tr', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var thisRow = $(this).find('input:checkbox')[0];

        if(thisRow !== undefined){
            $("#"+thisRow.id).trigger("click");
            if(thisRow.checked){
                $(this).addClass("grey lighten-4");
            }else{
                $(this).removeClass("grey lighten-4");
            }
        }
    });

    // render bulk update form
    $(document).on('click', '#bulk-update-btn', function() {

        $('#config-var').removeAttr('tabindex');
        
        $.ajax({
            url: '$renderBulkUpdateUrl', 
            type: 'post',
            dataType: 'json',
            data: {
                configVars: '$configValues'
            },
            success: function(response){
                
                $('#config-var').html(response);
                $('.loading-req-vars').remove();
                $('#req-vars').css('visibility','visible');
                $('#req-vars').css('width','');
                $('#req-vars').css('height','');
            }
        });
    });

    // save value upon change
    $(document).on('keypress keyup blur paste change', ".form-fields", function(event) {

        var id = this.id;
        var fields = (this.id).split('-');
        var column = fields[0];
        var targetTable = fields[1];
        var plantingJobOccurrenceDbId = fields[3];
        var value = $('#' + id).val();

        // if value is empty, disable next button

        if(value == ''){
            $('#next-btn').addClass('disabled');
            $(this).addClass('invalid');
        }else{
            $('#next-btn').removeClass('disabled');
            $(this).removeClass('invalid');

            // Save the changes in the database
            $.ajax({
                url: '$saveRowValueUrl',
                type: 'post',
                cache: false,
                data: {
                    entityDbId: plantingJobOccurrenceDbId,
                    column: column,
                    targetTable: targetTable,
                    value: value,
                    entityIdName: 'plantingJobOccurrenceDbId',
                    modelClass: 'plantingJobOccurrence',
                    plantingJobDbId: plantingJobId
                },
                success: function(response){
                },
            });
        }
    });

    // restric integer inputs to integer only without decimals
    $(".numeric-req-input").on("keypress keyup blur paste",function (event) { 
        var max = $(this).attr('max');
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }      
    });
}

// Check whether all are selected or not
function checkSelectAll(){
    // get summary
    var summaryElement = document.getElementsByClassName("summary")[0];
    
    if(summaryElement != undefined){
        var summaryStr = summaryElement.innerText;
        if(summaryStr != undefined){
            var summaryArr = summaryStr.split(" of ");
            if(summaryArr[1] != undefined){
                summaryArr = summaryArr[1].split(" items");

                if($('#selected-count').html() == summaryArr[0]){
                    checkAll = true;
                } else {
                    checkAll = false;
                }
            }
        }
    }

    if(!checkAll){
        $('#occurrence-select-all').prop('checked',false);
        $('#occurrence-select-all').removeAttr('checked');
    }else{
        $('#occurrence-select-all').prop('checked',true);
        $('#occurrence-select-all').attr('checked', 'checked');
    }
}

JS);

?>
<style type="text/css">
    input[type=number]:not(.browser-default) {
        margin: 0 0 2px 0 !important;
    }
</style>