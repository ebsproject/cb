<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

/**
 * Entries summary per experiment
 */

$columns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'header' => false,
        'visible'=>true,
    ],
    [
        'attribute' => 'germplasmName',
        'label' => 'Germplasm name'
    ],
    [
        'attribute' => 'germplasmParentage',
        'label' => 'Parentage'
    ],
    [
        'attribute' => 'totalRequiredPackageQuantity',
        'label' => 'Seed quantity',
        'value' => function($data){
            return number_format($data['totalRequiredPackageQuantity']) . ' ' . $data['requiredPackageUnit'];
        }
    ],
    [
        'attribute' => 'totalEnvelopeCount',
        'label' => 'Envelopes',
        'value' => function($data){
            return number_format($data['totalEnvelopeCount']);
        }
    ],
    [
        'attribute' => 'isReplaced',
        'label' => 'Is replaced?',
        'value' => function($data){
            if($data['isReplaced']){
                $s = ($data['replacedOccurrenceCount'] > 1) ? 's' : '';
                return 'Yes, replaced in '.$data['replacedOccurrenceCount'] . ' occurrence'.$s;
            } else {
                return 'No';
            }
        }
    ]
];

$browserId = 'entries-summary';
DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => false,
    'showFilter' => false,
    'storage' => 'cookie',
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $id,
        'showPageSummary' => false,
        'dataProvider' => $entriesDataProvider,
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'id' => $id,
                'enablePushState' => false
            ],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'before' => false,
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => false
            ],
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $id
    ]
]);
DynaGrid::end();

?>

<style type="text/css">
    .pagination {
        margin: 0px !important;
    }
</style>