<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use kartik\grid\GridView;

/**
 * Replace entry form
 */

 if (!isset($isBulkReplacement) || !$isBulkReplacement) {
    $info = $data->info;
    $entry = $data->entry;
    $quantity = $data->quantity;
    $package = $data->package;
}

$notSet = '<span class="not-set">(not set)</span>';

// if in preview, show go back button
if(isset($isPreview) && $isPreview){
echo '<a href="#!" class="go-back-to-entry-selection-parent-panel" title="'. \Yii::t('app', 'Go back') .'">
        <i class="material-icons">arrow_back</i>
    </a>';
}

if (!isset($isBulkReplacement) || !$isBulkReplacement) {?>
    <p>You are replacing an entry in Occurrence <b class="replace-occurrence-name"></b> in Experiment  <b class="replace-experiment-name"></b>.</p>
    <?if ($info->isReplaced && (!isset($isPreview) || !$isPreview)) {?>
        <ul class='collapsible'>
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">remove_red_eye</i> <span><?echo \Yii::t('app', "You have already selected a replacement for this entry. Click this tab to view the replacement info.");?></span>
                </div>
                <div class="collapsible-body">
                    <div class="card-panel">
                        <div class="row">
                            <h5 class="card-header">Replacement</h5>
                            <div>
                                <div class="col col-md-4">Replacement type</div>
                                <div class="col col-md-8 bold"><?= $info->replacementType == 'replace' ? $notSet : $info->replacementType ?></div>
                            </div>
                            <div>
                                <div class="col col-md-4">Replacement germplasm name</div>
                                <div class="col col-md-8 bold"><?= ($info->replacementType  == 'Replace with package') ? $entry->entryName ?? $notSet : $entry->replacementGermplasmName ?? $notSet ?></div>
                            </div>
                            <?if ($info->replacementType != 'Replace with package') {?>
                                <div>
                                    <div class="col col-md-4">Replacement parentage</div>
                                    <div class="col col-md-8 bold"><?= trim($entry->replacementParentage) != '' ? $entry->replacementParentage : $notSet ?></div>
                                </div>                
                            <?}?>
                            <div class="clearfix"></div>

                            <div class="card-header border-top">PACKAGE</div>

                            <div>
                                <div class="col col-md-4">Replacement seed name</div>
                                <div class="col col-md-8 bold"><?= trim($package->replacementSeedName) != '' ? $package->replacementSeedName : $notSet ?></div>
                            </div>
                            <div>
                                <div class="col col-md-4">Replacement package label</div>
                                <div class="col col-md-8 bold"><?= trim($package->replacementPackageLabel) != '' ? $package->replacementPackageLabel : $notSet ?></div>
                            </div>
                            <div>
                                <div class="col col-md-4">Replacement package quantity</div>
                                <div class="col col-md-8 bold"><?= trim($package->replacementPackageQuantity) != '' ? $package->replacementPackageQuantity : $notSet ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    <?}?>

    <div class="card-panel">
        <div class="row">
            <h5 class="card-header">Original entry</h5>
            <div>
                <div class="col col-md-4">Entry no.</div>
                <div class="col col-md-8 bold"><?= $entry->entryNumber ?></div>
            </div>
            <div>
                <div class="col col-md-4">Entry code</div>
                <div class="col col-md-8 bold"><?= $entry->entryCode ?></div>
            </div>
            <div>
                <div class="col col-md-4">Germplasm name</div>
                <div class="col col-md-8 bold"><?= $entry->entryName ?></div>
            </div>
            <div>
                <div class="col col-md-4">Parentage</div>
                <div class="col col-md-8 bold"><?= $entry->parentage ?></div>
            </div>
            <div class="clearfix"></div>

            <div class="card-header border-top">QUANTITY</div>
            
            <div>
                <div class="col col-md-4">Required quantity</div>
                <div class="col col-md-8 bold"><?= $quantity->requiredPackageQuantity ?></div>
            </div>
            <div>
                <div class="col col-md-4">Reserved quantity</div>
                <div class="col col-md-8 bold"><?= (trim($quantity->reserved) != '') ? $quantity->reserved : $notSet ?></div>
            </div>
            <div>
                <div class="col col-md-4">Available quantity</div>
                <div class="col col-md-8 bold"><?= (trim($quantity->available) != '') ? $quantity->available : $notSet ?></div>
            </div>
            <div class="clearfix"></div>

            <div class="card-header border-top">PACKAGE</div>

            <div>
                <div class="col col-md-4">Seed name</div>
                <div class="col col-md-8 bold"><?= (trim($package->seedName) != '') ? $package->seedName : $notSet ?></div>
            </div>
            <div>
                <div class="col col-md-4">Package label</div>
                <div class="col col-md-8 bold"><?= (trim($package->packageLabel) != '') ? $package->packageLabel : $notSet ?></div>
            </div>
            <div>
                <div class="col col-md-4">Package quantity</div>
                <div class="col col-md-8 bold"><?= (trim($package->packageQuantity) != '') ? $package->packageQuantity : $notSet ?></div>
            </div>

        </div>
    </div><?
} else {?>
    <ul class='collapsible selected-entries-panel'>
        <li>
            <div class="collapsible-header">
                Original Entry Summary
            </div>
            <div class="collapsible-body xl">
                <div class="row no-margin-bottom">
                    <div class="col s3">
                        <p style="font-weight: bold;">Germplasm Name</p>
                        <span><?echo $germplasmName;?></span>
                    </div>
                    <div class="col s3">
                        <p style="font-weight: bold;">Parentage</p>
                        <span><?echo $parentage;?></span>
                    </div>
                </div>
                <div class="row no-margin-bottom">
                    <div class="col s12" style='max-height: 160px; overflow: auto;'>
                        <?// Render selected entries browser
                        echo GridView::widget([
                            'pjax' => true,
                            'dataProvider' => $plantingJobEntriesDataProvider,
                            'id' => 'selected-planting-job-entries-grid',
                            'columns' => [
                                [
                                    'attribute' => 'experimentName',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'entryCode',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'siteCode',
                                    'label' => 'Site',
                                    'format' => 'raw',
                                    'vAlign' => 'middle',
                                    'value' => function ($model) {
                                        return "<span title='{$model['siteName']}'>
                                            {$model['siteCode']}
                                        </span>
                                        ";
                                    }
                                ],
                                [
                                    'attribute' => 'occurrenceName',
                                    'label' => 'Occurrence',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'actualSeedName',
                                    'label' => 'Seed Name',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'actualPackageLabel',
                                    'label' => 'Pacakge Label',
                                    'format' => 'raw',
                                    'vAlign' => 'middle'
                                ],
                                [
                                    'attribute' => 'requiredPackageQuantity',
                                    'label' => 'Required',
                                    'format' => 'raw',
                                    'vAlign' => 'middle',
                                    'value' => function ($model) {
                                        return "<span>
                                            {$model['requiredPackageQuantity']} {$model['requiredPackageUnit']}
                                        </span>";
                                    }
                                ],
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered white z-depth-3'
                            ],
                            'striped' => false,
                        ]);?>
                    </div>
                </div>
            </div>
        </li>
    </ul>
<?}?>


<?php
if (!isset($isPreview) || !$isPreview) {
?>
    <div>
        <p class="bold">How would you like to replace this entry?</p>
        <?php
            // display replacement type options
            if (!empty($scaleValues)){
                foreach ($scaleValues as $key => $value) {
                    $checked = (!$key) ? 'checked' : '';
                    echo '
                        <input name="replacement-type" text="'.$value['displayName'].'" type="radio" class="with-gap" value="'. $value['abbrev'] .'" id="replacement-'.$value['abbrev'].'" '.$checked . ' />
                        <label for="replacement-'.$value['abbrev'].'">'.$value['displayName'].'</label>
                        <br/>
                    ';
                }
            }
        ?>
    </div>
<?php
}else{
    // show replacement summary?>
    <div class="card-panel">
        <div class="row">
            <h5 class="card-header">Replacement Summary</h5>
            <div>
                <div class="col col-md-4">Replacement type</div>
                <div class="col col-md-8 bold"><?= $replacementType ?></div>
            </div>
            <?if ($replacementType != 'Replace with package') {?>
                <div>
                    <div class="col col-md-4">Replacement germplasm name</div>
                    <div class="col col-md-8 bold"><?= trim($replacementData['designation']) != '' ? $replacementData['designation'] : $notSet ?></div>
                </div>
                <div>
                    <div class="col col-md-4">Replacement parentage</div>
                    <div class="col col-md-8 bold"><?= trim($replacementData['parentage']) != '' ? $replacementData['parentage'] : $notSet ?></div>
                </div>
            <?}?>
            <div class="clearfix"></div>

            <div class="card-header border-top">PACKAGE</div>

            <div>
                <div class="col col-md-4">Replacement seed name</div>
                <div class="col col-md-8 bold"><?= trim($replacementData['seedName']) != '' ? $replacementData['seedName'] : $notSet ?></div>
            </div>
            <div>
                <div class="col col-md-4">Replacement package label</div>
                <div class="col col-md-8 bold"><?= trim($replacementData['label']) != '' ? $replacementData['label'] : $notSet ?></div>
            </div>
            <div>
                <div class="col col-md-4">Replacement package quantity</div>
                <div class="col col-md-8 bold"><?= trim($replacementData['quantity']) != '' ? $replacementData['quantity'] : $notSet ?></div>
            </div>

        </div>
    </div><?
}?>

<?$this->registerJs(<<<JS
// Initialize MaterializeCSS' collapsible
$(document).ready(function () {
    $('.collapsible').collapsible()
});
JS);
?>

<style type="text/css">
    .card-header {
        margin-left: 11px;
        font-weight: bold;
    }

    .border-top {
        border-top: 1px solid #eee;
        padding-top: 14px;
        margin-top: 15px;
        margin-bottom: 10px;
    }

    [type="radio"]:disabled+label {
        color: rgba(0,0,0,0.2);
        cursor: not-allowed;
    }
</style>