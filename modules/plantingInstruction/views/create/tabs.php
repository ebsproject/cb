<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

/*
 * Renders the header tab for the create packing job
 */

$plantingInstructionsUrl = Url::to(['/plantingInstruction','program' => $program]);
$envelopesUrl = Url::to(['/plantingInstruction/create','program' => $program, 'id' => $id]);
$entryMgtUrl = Url::to(['/plantingInstruction/create/entry','program' => $program, 'id' => $id]);
$reviewUrl = Url::to(['/plantingInstruction/create/review','program' => $program, 'id' => $id]);

$icon = isset($icon) ? $icon : 'navigate_next';
$label = isset($label) ? $label : 'Next';
$url = isset($url) ? $url : '#';
$title = isset($title) ? $title : 'Go to next step';
$class = isset($class) ? $class : '';
$btnId = isset($btnId) ? $btnId : 'next-btn';
$reviewClass = 'review-step disabled';

$btnOptions = [
    'title' => Yii::t('app', $title),
    'class' => 'btn btn-primary waves-effect waves-light pull-right ' . $class,
    'id' => $btnId,
    'style' => 'margin-bottom: 10px;'
];

if(isset($options)){
    $btnOptions = array_merge($btnOptions, $options);
}
echo 
'<h3>' . Html::a( Yii::t('app', 'Planting Instructions Manager'), $plantingInstructionsUrl) .
    ' <small>» '. $plantingJobCode.'</small>'.
    Html::a(
        $label, $url,
        $btnOptions
    ).
'</h3>';

$tabs = [
    [
        'label' => 'Envelopes',
        'url' => $envelopesUrl,
        'linkClass' => ($active == 'envelopes') ? 'active' : ''
    ],
    [
        'label' => 'Entry management',
        'url' => $entryMgtUrl,
        'linkClass' => ($active == 'entry') ? 'active' : ''
    ],
    [
        'label' => 'Review and submit',
        'url' => $reviewUrl,
        'linkClass' => ($active == 'review') ? 'active ' . $reviewClass : $reviewClass
    ]
];

?>
<div class="col col-md-12" style="padding-right: 0px">
    <ul class="tabs">
        <?php
            // render tabs
            foreach ($tabs as $value) {

                $label = $value['label'];
                $url = $value['url'];
                $linkClass = $value['linkClass'];

                $item = '<li class="tab col">'.
                    '<a href="'.$url.'" class="'.$linkClass.' a-tab">' . $label . '</a>
                </li>';

                echo $item;
            }
        ?>
    </ul>
</div>
<div class="row"></div>

<style type="text/css">
    .disabled {
        pointer-events: none;
    }
</style>
