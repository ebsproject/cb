<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\grid\GridView;

if($totalCount) {
	echo '<p>Select the germplasm want to use then click the next button. <b>Hover</b> over parentages to see the full values.</p>';

	echo GridView::widget([
		'pjax' => true,
		'dataProvider' => $dataProvider,
		'id' => 'select-replacement-grid',
		'columns' => $columns,
		'tableOptions' => [
			'class' => 'table table-bordered white z-depth-3'
		],
		'striped' => false
	]);
} else {
	echo '<br/><p><i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> There are no other available germplasm and packages that can be used as a replacement for the entry.</p>';
}