<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

// Initializes nav bar dropdowns
Modal::begin([]);
Modal::end();

$packingJobsUrl = Url::to(['/plantingInstruction','program' => $program]);

echo
"<h3 style='margin-bottom: 20px;'>" . Html::a(Yii::t('app', 'Planting Instructions Manager'), $packingJobsUrl) .
    ' <small>» ' . $plantingJobCode . '</small>' .
    Html::a(
        'Back', Url::to(['/plantingInstruction', 'program' => $program]),
        [
            'title' => \Yii::t('app', 'Return to packing jobs browser'),
            'class' => 'btn btn-primary waves-effect waves-light pull-right',
            'id' => 'back-btn',
            'style' => 'margin-bottom: 10px',
        ]
    ).
'</h3>';

// build status values dropdown
$statusesString = '<ul id="update-status-dropdown" class="dropdown-content" data-beloworigin="false"">';

foreach ($statuses as $key => $value) {
    $label = isset($value['displayName']) ? $value['displayName'] : '';
    $hiddenStatuses = [
        'Draft',
        'Updating Entries in Progress',
        'Updating Packages in Progress',
        'Submission in Progress',
        'Cancellation in Progress',
        'Packing Job Failed',
    ];
    
    if (!in_array($label, $hiddenStatuses)) {
        $val = isset($value['value']) ? $value['value'] : '';
        $shouldHideStatus = (// check conditions first if a status should be hidden
            // Hide "packed" option if Planting Job Entries are not yet Reviewed and Submitted,
            // and the status is NOT "ready for packing" AND NOT "packing"
            $val == 'packed' &&
            ($plantingJobStatus != 'ready for packing' && $plantingJobStatus != 'packing' || !$isSubmitted) ||
            // Hide status if it's itself
            $val == $plantingJobStatus
        ) ? 'hidden' : '';
        $valClass = str_replace(' ', '-', $val);

        $statusesString .= "<li
            title='Update status to $label'
            class='status-item status-$valClass $shouldHideStatus'
        >".
            Html::a(
                $label,
                '#',
                [
                    'data-value' => $val,
                    'data-label' => $label,
                    'data-val-class' => $valClass,
                    'class' => 'update-status',
                    'data-id' => $id,
                    'data-planting-job-code' => $plantingJobCode,
                ]
            )
        .'</li>';
    }
}
$statusesString .= '</ul>';

// if the packing job is already submitted or the status is packed or packing cancelled, disable updating the status
$statusButton = '';

if (!$isSubmitted || in_array($plantingJobStatus, ['packed', 'packing cancelled'])) {
    $statusButton = '
        <div
            title="Job status"
            class="planting-job-status-btn btn-flat btn '.$statusClass.'"
            href="#!"
            style="
                padding: 0 1rem;
                height: 28px;
                line-height: 28px;
                color: #fff;
                cursor: auto;
            "
        >
            <span class="planting-job-status-value">'.$plantingJobStatus.' </span>
        </div>
    ';
} else {
    $statusButton = '
        <a
            title="Update job status"
            class="planting-job-status-btn dropdown-button btn dropdown-button-pjax '.$statusClass.'"
            href="#!"
            data-activates="update-status-dropdown"
            data-beloworigin="true"
            data-constrainwidth="false"
            style="
                padding: 0 1rem;
                height: 28px;
                line-height: 28px;
            "
        >
            <span class="planting-job-status-value">'.$plantingJobStatus.' </span>
            </span> <span class="update-status-caret caret"></span>
        </a>
    ';
}

// confirm update status job modal
Modal::begin([
    'id' => 'confirm-update-status-modal',
    'header' => '<h4><i class="material-icons">edit</i> Confirm update status</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#!',
            [
                'data-dismiss'=>'modal'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light confirm-update-status-btn',
                'id'=>'confirm-update-btn',
                'title'=>'Confirm update'
            ]
        ) .
        '&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to update Packing job status to <b><span id="planting-job-status-update"></span></b>. The status cannot be changed once updated. Click confirm to proceed.';
Modal::end();

$packingJobInfocolumns = [
    [
        'attribute' => 'plantingJobStatus',
        'label' => 'Status',
        'format' => 'raw',
        'value' => function () use ($statusButton, $statusesString) {
            return $statusButton . $statusesString;
        },
        'hAlign' => 'center',
    ],
    [
        'attribute' => 'programCode',
        'label' => 'Program',
        'format' => 'raw',
        'value' => function ($model) {
            if (!$model['programCode'] || !$model['programName'])
                return null;

            return "<span title='{$model['programName']}'> {$model['programCode']} </span>";
        },
    ],
    [
        'attribute' => 'creator',
        'label' => 'Creator',
    ],
    [
        'attribute' => 'plantingJobDueDate',
        'label' => 'Due Date',
    ],
    [
        'attribute' => 'facilityCode',
        'label' => 'Facility',
        'format' => 'raw',
        'value' => function ($model) {
            if (!$model['facilityCode'] || !$model['facilityName'])
                return null;

            return "<span title='{$model['facilityName']}'> {$model['facilityCode']} </span>";
        },
    ],
    [
        'attribute' => 'creationTimestamp',
        'label' => 'Created',
        'format' => 'datetime',
    ],
    [
        'attribute' => 'modificationTimestamp',
        'label' => 'Last Updated',
        'format' => 'datetime',
    ],
    [
        'attribute' => 'plantingJobInstructions',
        'label' => 'Instructions',
    ],
];

$packingJobBrowserId = 'planting-job-info-browser';
DynaGrid::begin([
    'columns' => $packingJobInfocolumns,
    'theme' => 'simple-default',
    'storage' => DynaGrid::TYPE_SESSION,
    'gridOptions' => [
        'id' => $packingJobBrowserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProviders['packingJobInfo'],
        'pjaxSettings' => [
            'options' => [
                'id' => $packingJobBrowserId . '-id',
                'enablePushState' => false
            ],
        ],
        'responsive' => true,
        'panel' => [
            'heading' => false,
            'before' => false,
            'after' => false,
            'footer' => false
        ],
    ],
    'options' => [
        'id' => $packingJobBrowserId,
    ],
]);

DynaGrid::end();

$occurrenceSummariesColumns = [
    [
        'attribute' => 'experimentName',
        'label' => 'Experiment',
        'format' => 'raw',
        'value' => function ($model) {
            return '<span class="hidden">' . $model['experimentName'] . '</span>';
        },
        'group' => true,  // enable grouping
        'groupHeader' => function ($model) {
            return [
                'content' => [
                    0 => $model['experimentName'],
                ],
                'options' => ['class' => 'bold light-green lighten-4']
            ];
        },
        'groupFooter' => function () {
            return [
                'content' => [
                    0 => 'Totals',
                    1 => GridView::F_COUNT,
                    2 => GridView::F_COUNT,
                    3 => GridView::F_SUM,
                    4 => GridView::F_SUM,
                    5 => GridView::F_SUM,
                ],
                'contentFormats' => [ // content reformatting for each summary cell
                    0 => [ 'format' => 'raw', 'style' => 'font-weight: 700'],
                    1 => ['format' => 'number', 'decimals' => 0],
                    2 => ['format' => 'number', 'decimals' => 0],
                    3 => ['format' => 'number', 'decimals' => 0],
                    4 => ['format' => 'number', 'decimals' => 0],
                    5 => ['format' => 'number', 'decimals' => 0],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'info table-info bold']
            ];
        },
    ],
    [
        'attribute' => 'occurrenceName',
        'label' => 'Occurrence',
        'format' => 'raw',
        'value' => function ($data) use ($program) {
            $occurrenceDbId = $data['occurrenceDbId'];
            $occurrenceName = $data['occurrenceName'];
            $href = Url::to(['/occurrence/view/entry', 'program' => $program, 'id' => $occurrenceDbId]);

            // Open view occurrence link to new tab
            return "<span>
                <a href='$href' onclick='window.open(this.href); return false;' title='View occurrence information in new tab'> $occurrenceName </a>
            </span>";
        }
    ],
    [
        'attribute' => 'entryCount',
        'label' => 'Number of Entries',
        'value' => function ($model) {
            return number_format($model['entryCount']);
        }
    ],
    [
        'attribute' => 'replacementCount',
        'label' => 'Number of Replacements',
        'value' => function ($model) {
            return number_format($model['replacementCount']);
        }
    ],
    [
        'attribute' => 'envelopeCount',
        'label' => 'Number of Envelopes',
        'value' => function ($model) {
            return number_format($model['envelopeCount']);
        }
    ],
];

$occurrenceSummariesBrowserId = 'planting-job-occurrence-summaries-browser';
$experimentCount = isset($processedSummary[0]) ? number_format($processedSummary[0]['value']) : 0;
$occurrenceCount = isset($processedSummary[1]) ? number_format($processedSummary[1]['value']) : 0;
$envelopeCount = isset($processedSummary[2]) ? number_format($processedSummary[2]['value']) : 0;
$replacementCount = isset($processedSummary[3]) ? number_format($processedSummary[3]['value']) : 0;
DynaGrid::begin([
    'columns' => $occurrenceSummariesColumns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $occurrenceSummariesBrowserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProviders['occurrenceSummaries'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $occurrenceSummariesBrowserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => false,
            'before' => \Yii::t('app', "This packing job consists of <b>$experimentCount</b> experiments, <b>$occurrenceCount</b> occurrences, <b>$envelopeCount</b> envelopes, and <b>$replacementCount</b> entry replacements."),
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' => Html::beginTag(
                    'div',
                    [
                        'class' => 'text-left',
                        'style' => 'margin-left: 4px'
                    ]
                ) .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-occurrence-summaries-grid-id'
                        ]
                    ).
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $occurrenceSummariesBrowserId
    ]
]);
DynaGrid::end();

// reset grid url
$resetGridUrl = Url::toRoute(["/plantingInstruction/view/packing-job?id=$id&program=$program"]);
// update status URL
$updateStatusUrl = Url::to(['/plantingInstruction/create/save-planting-job-info']);
// Cancel Packing job URL
$cancelPackingJobUrl = Url::to(['/plantingInstruction/create/cancel-packing-job', 'id' => $id, 'code' => $plantingJobCode, 'program' => $program]);

$this->registerJs(<<<JS
let occurrenceSummariesBrowserId = '$occurrenceSummariesBrowserId'
let resetGridUrl = '$resetGridUrl'
let statusClass = '$statusClass'
let oldStatusClass = ''
let program = '$program'
let id = '$id'
let isSubmitted = Boolean('$isSubmitted')

$(document).ready(function() {
    refresh()
});

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    refresh()
})

function refresh() {
    // initialize dropdown button
    $('.dropdown-button').dropdown()

    let dataBrowserHeight = ($(window).height() - 340)
    $('.kv-grid-wrapper').css('height', dataBrowserHeight)

    // reset occurrences browser
    $('#reset-occurrence-summaries-grid-id').on('click', function clickResetOccurrenceSummariesGridButton (e) {
        e.preventDefault()

        $.pjax.reload({
            container: '#' + occurrenceSummariesBrowserId + '-pjax',
            replace: true,
            url: resetGridUrl
        })
    })

    $('.planting-job-status-btn').on('click', function clickPlantingJobStatusButton (e) {
        // Set top position of dropdown menu
        $('#update-status-dropdown').css('top', '58px')

        // Disable scroll overflow to properly display dropdown menu
        $('#planting-job-info-browser-id-container').css('overflow-x', 'visible')
    })


    // update planting job status
    $(document).on('click', '.update-status', function clickUpdateStatusButton (e){
        e.stopPropagation()
        e.preventDefault()

        let label = $(this).attr('data-label')
        let id = $(this).attr('data-id')
        let plantingJobCode = $(this).attr('data-planting-job-code')
        let value = $(this).attr('data-value')
        let strictStatuses = ['packed', 'packing cancelled']

        // show confirmation
        if (strictStatuses.includes(value)) {
            $('#confirm-update-status-modal').modal('show')
            document.getElementById("confirm-update-btn").setAttribute("data-id", id)
            document.getElementById("confirm-update-btn").setAttribute("data-planting-job-code", plantingJobCode)
            document.getElementById("confirm-update-btn").setAttribute("data-value", value)
            document.getElementById("confirm-update-btn").setAttribute("data-label", label)
            $('#planting-job-status-update').html(label)
        } else {
            updateStatus('PLANTING_JOB_STATUS', id, value, label)
        }
    })

    // confirm update planting job status
    $(document).on('click', '.confirm-update-status-btn', function clickConfirmUpdateStatusButton (e) {
        e.stopPropagation()
        e.preventDefault()

        var label = $(this).attr('data-label')
        var id = $(this).attr('data-id')
        var plantingJobCode = $(this).attr('data-planting-job-code')
        var value = $(this).attr('data-value')
        var notif = ''

        $('.toast').css('display','none')

        // disable dropdown button
        toggleUpdateStatusDropdown(true)
        updateStatus('PLANTING_JOB_STATUS', id, value, label, plantingJobCode)
    })
}


// update status
function updateStatus(column, id, value, label, plantingJobCode = ''){
    let notif = ''

    // Change 'packed' value in relation with background workers handling the updating of Planting Job Status to 'packed'
    value = (value === 'packed') ? 'updating packages in progress' : value
    label = (label === 'Packed') ? 'Updating Packages in Progress' : label
    $('.toast').css('display','none')

    // Hide "packed" status option...
    // ...if new status is NOT "ready for packing" AND NOT "packing" OR NOT submitted
    if (value !== 'ready for packing' && value !== 'packing' || !isSubmitted)
        $('.status-packed').attr('hidden', true)
    else
        $('.status-packed').attr('hidden', false)

    if (value === 'packing cancelled') {
        $.ajax({
            url: '$cancelPackingJobUrl',
            type: 'post',
            success: function(response){
            },
            error: function(){
                var errorMessage = '<i>There was a problem loading the content.</i>';
                $('.submit-confirmation-panel').html(errorMessage);
                $('.submit-confirm-btn').addClass('disabled');
            }
        })
    } else {
        $.ajax({
            url: '$updateStatusUrl',
            type: 'post',
            async: false,
            data: {
                column: column,
                value: value,
                id: id,
                plantingJobCode: plantingJobCode
            },
            success: function(response) {
                $('.status-item').removeClass('hidden')
                $('#confirm-update-status-modal').modal('hide')

                notif = "<i class='material-icons green-text left'>check</i> Successfully updated status to "+ label +"."
                Materialize.toast(notif, 5000)

                $('.planting-job-status-value').html(value)
                $('.planting-job-status-btn').removeClass(statusClass).addClass(response)

                var valClass = value.replace(/ /g,"-")
                $('.status-' + valClass).addClass('hidden')

                statusClass = response

                // replace old class to new status class
                $('.grid-status-'+id).removeClass(oldStatusClass).addClass(statusClass)
                
                $('.view-planting-job-'+id).attr('data-status-class', statusClass)
                oldStatusClass = statusClass

                // update label of status in grid view
                $('.grid-status-'+id).html(label.toUpperCase())

                // set updated flag to true
                updated = true

                // Redirect to filtered browser
                if (value.includes('in progress')) window.location.assign(window.location.origin +
                    `/index.php/plantingInstruction?program=\${program}&PlantingJob[plantingJobDbId]=\${id}`
                )
            },
            error: function() {
                notif = "<i class='material-icons red-text left'>close</i> There was a problem updating the status."
                Materialize.toast(notif, 5000)
            }
        })
    }
}

// disable or enable update status dropdown
function toggleUpdateStatusDropdown(disable = true){
    // if to disable
    if(disable) {
        $('.update-status-caret').css('display', 'none')
        $('.planting-job-status-btn').css('pointer-events', 'none')
        $('#update-status-dropdown').addClass('hidden')
    } else {
        $('.update-status-caret').css('display', 'block')
        $('.planting-job-status-btn').css('pointer-events', '')
        $('#update-status-dropdown').removeClass('hidden')
    }
}

JS);

?>