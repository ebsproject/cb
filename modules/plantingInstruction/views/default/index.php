<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use app\components\FavoritesWidget;
use app\components\PrintoutsWidget;
use app\controllers\BrowserController;
use app\models\DataBrowserConfiguration;
use app\models\UserDashboardConfig;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders Planting instructions manager browser
 */
$columns = [
    [
        //checkbox
        'content' => function ($data) {
            $plantingJobDbId = $data['plantingJobDbId'];
            $occurrenceDbIds = implode('|', $data['occurrenceDbIds']);

            return "<input
                id='$plantingJobDbId'
                class='planting-job-grid-select filled-in'
                type='checkbox'
                data-occurrence-ids='$occurrenceDbIds'
            />
                <label for='$plantingJobDbId'></label>       
            ";
        },        
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'hiddenFromExport' => true,
        'mergeHeader' => true,
        'order' => DynaGrid::ORDER_FIX_LEFT,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'hAlign' => 'left',
        'vAlign' => 'top',
        'order' => DynaGrid::ORDER_FIX_LEFT,
        'template' => '{view} {update} {delete}',
        'buttons' => [
            'view' => function ($url, $data) {
                $render = Yii::$app->access->renderAccess("PIM_VIEW", "PLANTING_INSTRUCTIONS_MANAGER", $data['creatorDbId']);

                return !$render ? '' : Html::a(
                    '<i class="material-icons">remove_red_eye</i>','#',
                    [
                        'title' => Yii::t('app', 'View ' . $data['plantingJobType']),
                        'class' => 'view-planting-job view-planting-job-'.$data['plantingJobDbId'],
                        'data-id' => $data['plantingJobDbId'],
                        'data-label' => $data['plantingJobCode'],
                    ]
                );
            },
            'update' => function ($url, $data) use ($program) {
                $render = Yii::$app->access->renderAccess("PIM_UPDATE", "PLANTING_INSTRUCTIONS_MANAGER",$data['creatorDbId']);

                if ( // Allow entry substitutions/replacements for the following job statuses
                    strtolower($data['plantingJobStatus']) == 'packing on hold' ||
                    strtolower($data['plantingJobStatus']) == 'ready for packing' ||
                    strtolower($data['plantingJobStatus']) == 'packing'
                ) {
                    return !$render ? '' : Html::a(
                        '<i class="material-icons">edit</i>',
                        Url::to(['/plantingInstruction/create/entry', 
                            'program' => $program, 
                            'id' => $data['plantingJobDbId']]
                        ),
                        [
                            'title' => Yii::t('app', 'Make substitutions for ') . $data['plantingJobType']
                        ]
                    );
                } else if (strtolower($data['plantingJobStatus']) == 'draft') {
                    return !$render ? '' : Html::a(
                        '<i class="material-icons">edit</i>',
                        Url::to(['/plantingInstruction/create', 
                            'program' => $program, 
                            'id' => $data['plantingJobDbId']]
                        ),
                        [
                            'title' => Yii::t('app', 'Update ') . $data['plantingJobType']
                        ]
                    );
                }

                return null;
            },
            'delete' => function ($url, $data, $key) {
                $render = Yii::$app->access->renderAccess("PIM_DELETE", "PLANTING_INSTRUCTIONS_MANAGER", $data['creatorDbId']);
                if (strtolower($data['plantingJobStatus']) == 'packed' || strtolower($data['plantingJobStatus']) == 'updating entries in progress' || strtolower($data['plantingJobStatus']) == 'updating packages in progress')
                    return '';

                return !$render ? '' : Html::a('<i class="material-icons">delete</i>',
                    '#',
                    [
                        'class'=>'delete-planting-job',
                        'title' => Yii::t('app', 'Delete') . ' ' . $data['plantingJobType'],
                        'data-target' => '#delete-planting-job-modal',
                        'data-toggle' => 'modal',
                        'data-planting_job_id' => $data['plantingJobDbId'],
                        'data-label' => $data['plantingJobCode'],
                    ]
                );
            },

        ]
    ],
    [
        'attribute' => 'plantingJobStatus',
        'label' => 'Status',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $statusOptions,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
        'filterInputOptions' => [
            'placeholder' => '',
        ],
        'contentOptions' => [
            'style'=>'min-width: 150px;'
        ],
        'value' => function ($model) {
            $statusCategory = BrowserController::getStatusCategory($model['plantingJobStatus']);

            // Get CSS status class by category
            $class = BrowserController::getStatusClassByCategory($statusCategory);
            
            $value = strtoupper(trim($model['plantingJobStatus']));
            $jobId = $model['plantingJobDbId'];
            $statusVal = '';
            $statusVal .= '<span class="grid-status-'. $jobId .' bold new badge ' . $class . '">' . $value . '</span>';

            return $statusVal;
        },
    ],
    [
        'attribute' => 'plantingJobType',
        'label' => 'Job Type',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $typeOptions,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
        'filterInputOptions' => [
            'placeholder' => '',
        ],
        'contentOptions' => [
            'style'=>'min-width: 150px;'
        ],
    ],
    [
        'attribute' => 'plantingJobCode',
        'label' => 'Job Code',
        'format' => 'raw'
    ],
    [
        'attribute' => 'plantingJobDueDate',
        'label' => 'Due date',
        'format' => 'date',
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'clearBtn' => true,
                'todayHighlight' => true,
            ],
            // Plugin code for calendar clear date
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ],
        ]
    ],
    [
        'attribute' => 'experiments',
        'label' => 'Experiments',
        'noWrap' => true,
        'width' => '180px',
        'format' => 'html',
        'value' => function ($model) use ($program) {

            if($model['experiments'][0] == null){
                return null;
            }else {
                $count = count($model['experiments']);
                $finalValue = ' <ul class="collapsible" data-collapsible="accordion" style="min-width: 158px; margin: 2px;">';

                $experiments = $model['experiments'];
                
                $s = ($count > 1) ? 's' : '';
                $finalValue .= '<li>
                <div class="collapsible-header" style="padding: .5rem;"><span class="secondary-content">'
                    . $count. ' experiment'.$s.'</span>' .
                    '</div>
                <div class="collapsible-body" style="padding:0px">';

                $dataModel = [];
                if (!empty($experiments)) {
                    $variables = [];
                    foreach ($experiments as $experiment) {
                        $dataModel[] = ["experiment"=>$experiment];
                    }
                }
                $provider = new ArrayDataProvider([
                    'allModels' => $dataModel,
                    'pagination' => false
                ]);
                $finalValue .= GridView::widget([
                    'dataProvider' => $provider,
                    'layout' => '{items}',
                    'showHeader' => false,
                    'columns' => [
                        [
                            'attribute' => 'experiment',
                            'format' => 'raw',
                            'label' => false,
                        ]
                    ],
                    'tableOptions' => ["style" => "margin-bottom:0px"]
                ]);
                $finalValue .= '</div></li>';

                $finalValue = $finalValue .= '</ul>';

                return $finalValue;
            }
        }
    ],
    [
        'attribute' => 'occurrenceCount',
        'label' => 'Occurrence Count'
    ],
    [
        'attribute' => 'envelopeCount',
        'label' => 'Envelope Count',
        'value' => function ($model) {
            return number_format($model['envelopeCount']);
        },
    ],
    [
        'attribute' => 'facilityName',
        'label' => 'Facility'
    ],
    [

        'label' => 'Creator',
        'attribute' => 'creator',
        'visible' => false
    ],
    [    
        'attribute'=>'creationTimestamp',
        'format' => 'date',
        'visible' => false,
        'filterType' => GridView::FILTER_DATE,
        'filterInputOptions' => [
            'autocomplete' => 'off'
        ],
        'filterWidgetOptions' => [
            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'clearBtn' => true,
                'todayHighlight' => true,
            ],
            // Plugin code for calendar clear date
            'pluginEvents' => [
                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
            ],
        ]
    ]
];

$browserId = 'dynagrid-pim-planting-job-transactions';

$notificationsString = \Yii::t('app', 'NOTIFICATIONS');
$earlierString = \Yii::t('app', 'earlier');

$checkboxSessionStorageName = 'plantingJobOccurrenceCheckboxIds';
$render = Yii::$app->access->renderAccess("PIM_PRINTOUTS", "PLANTING_INSTRUCTIONS_MANAGER");

$printouts = !$render ? '' : PrintoutsWidget::widget([
    "product" => "Planting Instruction Manager",
    "program" => $program,
    "occurrenceIds" => [ ],
    "checkboxSessionStorage" => $checkboxSessionStorageName
]);


// Add restrictions on URL
if(isset($model['creatorDbId'])){
    $renderAccessUpdate = Yii::$app->access->renderAccess("PIM_UPDATE", "PLANTING_INSTRUCTIONS_MANAGER", $model['creatorDbId'], usage:'url');
} 

// check if background processing is triggered
$bgProcessingNotif = '';
$params = Yii::$app->request->queryParams;

if (
    Yii::$app->session->get('pim-bg-process') !== null &&
    (
        isset($params['PlantingJob']['plantingJobDbId'])
    )
) {
    if (Yii::$app->session->get('pim-bg-process-message')) {
        $notifMessage = Yii::$app->session->get('pim-bg-process-message');
        Yii::$app->session->remove('pim-bg-process-message');
    } else if (isset($params['PlantingJob']['plantingJobDbId'])) {
        // get status of transaction
        $bgEntityId = $params['PlantingJob']['plantingJobDbId'];
        $searchParams['plantingJobDbId'] = "$bgEntityId";

        $plantingJob = $searchModel->search($searchParams, 'limit=1', false);
        $plantingJobCode = isset($plantingJob['data'][0]['plantingJobCode']) ? $plantingJob['data'][0]['plantingJobCode'] : '';
        $plantingJobStatus = isset($plantingJob['data'][0]['plantingJobStatus']) ? $plantingJob['data'][0]['plantingJobStatus'] : '';

        $notifMessage = "You are now seeing the transaction for <b>$plantingJobCode</b> is now <b>$plantingJobStatus</b>. Click the Reset Grid button to display all records.";
    }

    if (str_contains($notifMessage, 'Please try again or file a report for assistance')) {
        $bgProcessingNotif = '
            <div class="alert warning">
                <div class="card-panel" style="">
                    <i class="fa fa-warning"></i> '.
                    $notifMessage
                    .'<button data-dismiss="alert" class="close" type="button">×</button>
                </div>
            </div>
        ';
    } else {
        $bgProcessingNotif = '
            <div class="alert info">
                <div class="card-panel" style="">
                    <i class="fa fa-info"></i> '.
                    $notifMessage
                    .'<button data-dismiss="alert" class="close" type="button">×</button>
                </div>
            </div>
        ';
    }
}
// unset background process session
Yii::$app->session->remove('pim-bg-process');

DynaGrid::begin([
    'columns' => $columns,
    'theme' => 'simple-default',
    'showPersonalize' => true,
    'showFilter' => false,
    'storage' => DynaGrid::TYPE_SESSION,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'gridOptions' => [
        'id' => $browserId . '-id',
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => $browserId . '-id'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],
        'responsive' => false,
        'panel' => [
            'heading' => ('<h3>' .
                Yii::t('app', 'Planting Instructions Manager') .
                FavoritesWidget::widget([
                    'module' => 'plantingInstruction'
                ]) .
                '</h3>') . $bgProcessingNotif,
            'before' => \Yii::t('app', "This is the browser for planting instructions manager. You may manage your packing or planting jobs here."),
            'after' => false,
        ],
        'toolbar' => [
            [
                'content' =>
                Html::beginTag('div', ['class' => 'tex-left dropdown']) .
                    Html::a('<i class="material-icons">notifications_none</i>
                        <span id="notif-badge-id"></span>',
                        '#',
                        [
                            'id' => 'notif-btn-id',
                            'class' => 'btn waves-effect waves-light pim-notification-button tooltipped',
                            'data-pjax' => 0,
                            'style' => 'margin-right:5px;overflow: visible !important;',
                            'data-position' => "top",
                            'data-tooltip' => \Yii::t('app', 'Notifications'),
                            'data-activates' => 'notifications-dropdown'
                        ]
                    ). ' '.
                    $printouts . ' ' .
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        '#',
                        [
                            'data-pjax' => true,
                            'class' => 'btn btn-default',
                            'title' => \Yii::t('app', 'Reset grid'),
                            'id' => 'reset-pim-grid-id'
                        ]
                    ) .
                    '{dynagridFilter}{dynagridSort}{dynagrid}' .
                    Html::endTag('div')
            ],
        ],
        'floatHeader' => true,
        'floatOverflowContainer' => true,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ],
    'submitButtonOptions' => [
        'icon' => 'glyphicon glyphicon-ok',
    ],
    'deleteButtonOptions' => [
        'icon' => 'glyphicon glyphicon-remove',
        'label' => Yii::t('app', 'Remove'),
    ],
    'options' => [
        'id' => $browserId
    ]
]);
DynaGrid::end();
?>

<!-- Notifications -->
<div id="notif-container">
    <ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;">
    </ul>
</div>

<?php

// modal for view Planting job
Modal::begin([
    'id' => 'view-planting-job-modal',
    'header' => '
        <h4>
            <i class="fa fa-info-circle"></i> Packing Job 
            <span id="view-planting-job-label"></span>
        </h4>',
    'footer' => 
        Html::a(\Yii::t('app','Close'),'#',[
            'data-dismiss'=>'modal',
            'class'=>'btn btn-primary waves-effect waves-light modal-close'
        ]).'&emsp;',
    'size' =>'modal-lg',
    'options' => [
        'data-backdrop'=>'static'
    ]
]);
echo '<div class="view-modal-body" style="min-height:230px"></div>';
Modal::end();

// delete planting job modal
Modal::begin([
    'id' => 'delete-planting-job-modal',
    'header' => '<h4 id="delete-planting-job-label"></h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#!',
            [
                'data-dismiss'=>'modal'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light delete-planting-job-btn',
                'id'=>'confirm-delete-btn',
                'title'=>'Confirm delete'
            ]
        ) .
        '&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to delete Packing job <b><span id="planting-job-delete"></span></b>. This action cannot be undone. Click confirm to proceed.';

Modal::end();

// confirm update status job modal
Modal::begin([
    'id' => 'confirm-update-status-modal',
    'header' => '<h4><i class="material-icons">edit</i> Confirm update status</h4>',
    'footer' =>
        Html::a(
            \Yii::t('app','Cancel'),
            '#!',
            [
                'data-dismiss'=>'modal'
            ]
        ) .
        '&nbsp;&nbsp' .
        Html::a(
            \Yii::t('app','Confirm'),
            null,
            [
                'class'=>'btn btn-primary waves-effect waves-light confirm-update-status-btn',
                'id'=>'confirm-update-btn',
                'title'=>'Confirm update'
            ]
        ) .
        '&nbsp;&nbsp',
    'options' => ['data-backdrop'=>'static']
]);
echo '<i class="fa fa-warning orange-text text-darken-3 fa-2x"></i> You are about to update Packing job status to <b><span id="planting-job-status-update"></span></b>. The status cannot be changed once updated. Click confirm to proceed.';

Modal::end();

$this->registerCssFile("@web/css/browser/notifications.css", [
    'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
    'position' => \yii\web\View::POS_HEAD
], 'css-browser-notifications');

// reset grid URL
$resetGridUrl = Url::toRoute(['/plantingInstruction?program=' . $program]);
// view URL
$viewUrl = Url::to(['/plantingInstruction/default/view']);
// update status URL
$updateStatusUrl = Url::to(['/plantingInstruction/create/save-planting-job-info']);
// delete planting job URL
$deleteUrl = Url::to(['/plantingInstruction/default/delete','program'=>$program]); // url for delete person ajax
// retrieve new notifications count
$newNotificationsUrl = Url::toRoute(['/plantingInstruction/default/new-notifications-count']);
// retrieve notifications url
$notificationsUrl = Url::toRoute(['/plantingInstruction/default/push-notifications']);
// page size urls
$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
// Save browser settings
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
var gridId = '$browserId';
var resetGridUrl = '$resetGridUrl';
var oldStatusClass = '';
var updated = false;
var plantingJobLabel = null;
var plantingJobId = null;
var column = 'PLANTING_JOB_STATUS';
var newNotificationsUrl = '$newNotificationsUrl';
var notificationsUrl = '$notificationsUrl';
const notificationsString = '$notificationsString'
const earlierString = '$earlierString'
const loadingIndicatorHtml = '<div class="progress" style="margin:0;z-index:999"><div class="indeterminate"></div></div>'
let retrieveNotificationsAjax = null
let noAction = true;
var program = '$program';
const checkboxSessionStorageName = '$checkboxSessionStorageName'

$(document).ready(function() {
    // Adjust browser page size, if the page size was changed in another browser
    adjustBrowserPageSize();

    refresh();

    notifDropdown();
    renderNotifications();
    
    setTimeout(function(){
        notificationInterval=setInterval(updateNotif, 5000);
    }, 5000);
});

// reload grid on pjax
$(document).on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refresh();

    notifDropdown();
    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; opacity: 1; left: 1641.61px; position: absolute; top: 64px; display: none;"></ul>');
    renderNotifications();
    updateNotif();
});

function refresh(e){
    updated = false;

    var maxHeight = ($(window).height() - 250);
    $(".kv-grid-wrapper").css("height",maxHeight);

    $('.collapsible').collapsible();

    // reset occurrences browser
    $('#reset-pim-grid-id').on('click', function(e) {
        e.preventDefault();
        $.pjax.reload({
            container: '#' + gridId + '-pjax',
            replace: true,
            url: resetGridUrl
        });
    });

    // view packing job
    $(document).on('click','.view-planting-job', function(e) {
        e.stopPropagation()
        e.preventDefault()

        let id = $(this).attr('data-id')

        // Redirect to filtered browser
        window.location.assign(window.location.origin +
            '/index.php/plantingInstruction/view/packing-job' +
            '?id=' + id +
            '&program=' + program
        )
    })

    // update planting job status
    $(document).on('click','.update-status', function(e){
        e.stopPropagation();
        e.preventDefault();

        var label = $(this).attr('data-label');
        var id = $(this).attr('data-id');
        var plantingJobCode = $(this).attr('data-planting-job-code');
        var value = $(this).attr('data-value');
        
        var toConfirmStatuses = ['packed', 'packing cancelled'];
        // show confirmation
        if(toConfirmStatuses.includes(value)){
            $('#confirm-update-status-modal').modal('show');
            document.getElementById("confirm-update-btn").setAttribute("data-id", id);
            document.getElementById("confirm-update-btn").setAttribute("data-planting-job-code", plantingJobCode);
            document.getElementById("confirm-update-btn").setAttribute("data-value", value);
            document.getElementById("confirm-update-btn").setAttribute("data-label", label);
            $('#planting-job-status-update').html(label);
        }else{
            updateStatus(column, id, value, label);
        }

    });

    // confirm update planting job status
    $(document).on('click','.confirm-update-status-btn', function(e){
        e.stopPropagation();
        e.preventDefault();

        var label = $(this).attr('data-label');
        var id = $(this).attr('data-id');
        var plantingJobCode = $(this).attr('data-planting-job-code');
        var value = $(this).attr('data-value');
        var notif = '';

        $('.toast').css('display','none');

        // disable dropdown button
        toggleUpdateStatusDropdown(true);
        updateStatus(column, id, value, label, plantingJobCode);

    });

    // upon close modal, reset grid
    $(document).on('click', '.modal-close, .close', function() {

        if(updated){
            $.pjax.reload({
                container: '#'+gridId+'-pjax'
            });
        }

        updated = false;
    });

    // delete planting job
    $(document).on('click', '.delete-planting-job', function(e) {
        obj = $(this)
        plantingJobLabel = obj.attr('data-label');
        plantingJobId = obj.attr('data-planting_job_id');

        $('#delete-planting-job-label').html('<i class="material-icons">delete</i> Delete <b>' + plantingJobLabel + '</b>');
        $('#planting-job-delete').html(plantingJobLabel);
        document.getElementById("confirm-delete-btn").setAttribute("data-packing_job_id", plantingJobId);
        $('#delete-planting-job-modal').modal('show');
    });

    // confirm delete planting job
    $(document).on('click', '#confirm-delete-btn', function(e) {
        e.preventDefault()
        e.stopImmediatePropagation()

        let url = '$deleteUrl'

        $('#confirm-delete-btn').addClass('disabled')

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                plantingJobId: obj.attr('data-planting_job_id'),
                plantingJobLabel: plantingJobLabel
            },
            async: true,
            success: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting the record.'

                if (res.success) {
                    $.pjax.reload({
                        container: '#'+gridId+'-pjax'
                    })
                    message = '<i class="material-icons green-text" style="margin-right: 8px;">check</i> Successfully deleted ' + res.label + '!'
                }

                $('#delete-planting-job-modal').modal('hide')
                $('#confirm-delete-btn').removeClass('disabled')

                Materialize.toast(message, 5000)
            },
            error: function (res) {
                let message = '<i class="material-icons red-text" style="margin-right: 8px;">close</i> There seems to be a problem with deleting ' + res.label + ' record.'

                $('#delete-planting-job-modal').modal('hide')
                $('#confirm-delete-btn').removeClass('disabled')
                Materialize.toast(message, 5000)
            }
        });
    });

    // Get occurrence IDs and ensure only 1 planting job is selected
    $('input[type="checkbox"]').on('change', function () {
        $('input[type="checkbox"]').not(this).prop('checked', false)

        // Store occurrence IDs into a JSON-stringified array
        let occurrenceDbIds = String($(this).data('occurrence-ids'))
        occurrenceDbIds = occurrenceDbIds.includes('|') ? occurrenceDbIds.split('|') : [ occurrenceDbIds ]

        sessionStorage.setItem(checkboxSessionStorageName, JSON.stringify(occurrenceDbIds))
    })
}

// update status
function updateStatus(column, id, value, label, plantingJobCode = ''){
    var notif = '';
    // Change 'packed' value in relation with background workers handling the updating of Planting Job Status to 'packed'
    value = (value == 'packed') ? 'updating packages in progress' : value
    label = (label == 'Packed') ? 'Updating Packages in Progress' : label
    $('.toast').css('display','none');

    $.ajax({
        url: '$updateStatusUrl',
        type: 'post',
        async: false,
        data: {
            column: column,
            value: value,
            id: id,
            plantingJobCode: plantingJobCode
        },
        success: function(response) {
            $('.status-item').removeClass('hidden');
            $('#confirm-update-status-modal').modal('hide');

            notif = "<i class='material-icons green-text left'>check</i> Successfully updated status to "+ label +".";
            Materialize.toast(notif, 5000);

            $('.planting-job-status-value').html(value);
            $('.planting-job-status-btn').removeClass(statusClass).addClass(response);

            var valClass = value.replace(/ /g,"-");
            $('.status-' + valClass).addClass('hidden');

            statusClass = response;

            // replace old class to new status class
            $('.grid-status-'+id).removeClass(oldStatusClass).addClass(statusClass);
            
            $('.view-planting-job-'+id).attr('data-status-class', statusClass);
            oldStatusClass = statusClass;

            // update label of status in grid view
            $('.grid-status-'+id).html(label.toUpperCase());

            // set updated flag to true
            updated = true;

            // Redirect to filtered browser
            window.location.assign(window.location.origin +
                `/index.php/plantingInstruction?program=\${program}&PlantingJob[plantingJobDbId]=\${id}`
            )
        },
        error: function() {
            notif = "<i class='material-icons red-text left'>close</i> There was a problem updating the status.";
            Materialize.toast(notif, 5000);
        }
    });
}

// display notifications in dropdown UI
function notifDropdown(){
    $('.pim-notification-button').on('click', function(e){
        // Display a loading indicator for the notif menu
        $('#notifications-dropdown').html(`<li>
                <h6 class="center ">\${notificationsString}&emsp;&emsp;
                    <span class="round red accent-2 white-text pull-right "><b class="notif-count">0</b></span>
                </h6>
            </li>
            <li class="divider"></li>
            <li class="header center">\${loadingIndicatorHtml}</li>
        `)

        retrieveNotificationsAjax = $.ajax({
            url: notificationsUrl,
            type: 'get',
            async: true,
            success: function(data){
                renderNotifications();
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    let obj = $(this)
                    let id = obj.data('transaction_id')
                    let abbrev = obj.data('abbrev')

                    $.pjax.reload({
                        container: '#' + gridId + '-pjax',
                        replace: true,
                        url: '/index.php/plantingInstruction?program='+ program +'&PlantingJob[plantingJobDbId]=' + id
                    })
                })
            },
            error: function(xhr, status, error) {
                // If response text is empty, don't display error notif
                if (!xhr.responseText) return

                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                    '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                    '<i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }  
        });
    });
}

// initialize dropdown materialize for notifications
function renderNotifications(){
    $('.pim-notification-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

// show new notifications
function updateNotif() {
    // if no action is triggered
    if(noAction){
        $.getJSON(newNotificationsUrl, 
        function(json){
            $('#notif-badge-id').html(json);
            if(parseInt(json)>0){
                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
        });
    }
}

// disable or enable update status dropdown
function toggleUpdateStatusDropdown(disable = true){

    // if to disable
    if(disable) {
        $('.update-status-caret').css('display', 'none');
        $('.planting-job-status-btn').css('pointer-events', 'none');
        $('#update-status-dropdown').addClass('hidden');
    } else {
        $('.update-status-caret').css('display', 'block');
        $('.planting-job-status-btn').css('pointer-events', '');
        $('#update-status-dropdown').removeClass('hidden');
    }
}

JS);