<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Renders view Packing job
 */

$summaryString = '';
foreach ($summary as $key => $value) {
    $label = isset($value['label']) ? $value['label'] : '';
    $value = isset($value['value']) ? $value['value'] : '';

    $summaryString .= '<span>'.$label.': </span>' . '<b>'.$value.'</b><br/>';
}

$additionalInfoString = '';
foreach ($addtlInstructions as $key => $value) {
    $label = isset($value['label']) ? $value['label'] : '';
    $value = isset($value['value']) ? $value['value'] : '';
    $value = empty($value) ? '<span class="not-set">(not set)</span>' : $value;

    $additionalInfoString .= '<span>'.$label.': </span>' . '<b>'.$value.'</b><br/>';
}

// build status values dropdown
$statusesStr = '<ul id="update-status-dropdown" class="dropdown-content" data-beloworigin="false">';
foreach ($statuses as $key => $value) {
    $label = isset($value['displayName']) ? $value['displayName'] : '';
    
    if($label !== 'Updating Entries in Progress'){
        $val = isset($value['value']) ? $value['value'] : '';
        $hiddenClass = ($val == $status) ? 'hidden' : '';
        $valClass = str_replace(' ', '-', $val);

        // Hide "Packed" option if Planting Job Entries are not yet Reviewed and Submitted, and the status is not yet Ready for Packing
        if ($val == 'packed' && ($plantingJobStatus != 'ready for packing' || !$isSubmitted))
            continue;

        $statusesStr .= "<li title='Update status to $label' class='status-item status-$valClass $hiddenClass'>".
            Html::a(
                $label,
                '#',
                [
                    'data-value' => $val,
                    'data-label' => $label,
                    'data-val-class' => $valClass,
                    'class' => 'update-status',
                    'data-id' => $id,
                    'data-planting-job-code' => $plantingJobCode,
                ]
            )
        .'</li>';
    }
}
$statusesStr .= '</ul>';

// if the packing job is already submitted or the status is packed or packing cancelled, disable updating the status
if(!$isSubmitted || in_array($status, ['packed', 'packing cancelled'])){
    $status = '
        <div
            title="Packing job status"
            class="planting-job-status-btn btn-flat btn '.$statusClass.'"
            href="#!"
            style="
                padding: 0 1rem;
                height: 28px;
                line-height: 28px;
                color: #fff;
                cursor: auto;
            "
        >
            <span class="planting-job-status-value">'.$status.' </span>
        </div>
    ';
}else{
    $status = '
        <a
            title="Update status"
            class="planting-job-status-btn dropdown-button btn dropdown-button-pjax '.$statusClass.'"
            href="#!"
            data-activates="update-status-dropdown"
            data-beloworigin="true" 
            data-constrainwidth="false"
            style="
                padding: 0 1rem;
                height: 28px;
                line-height: 28px;
            "
        >
            <span class="planting-job-status-value">'.$status.' </span>
            </span> <span class="update-status-caret caret"></span>
        </a>
    ';
}

echo '<b>Status: </b>' . $status . $statusesStr;

echo '
    <h5 style="margin-top:25px">Packing Job Summary</h5>'.
    $summaryString .
    // additional instructions
    '<ul class="collapsible" style="margin-top:15px">
        <li title="Specify additional instructions for the Packing job">
            <div class="collapsible-header">
                <span class="secondary-content">Additional instructions</span>
            </div>

            <div class="collapsible-body" style="padding:0px">
                <div style="margin: 19px 0px 0px 25px;">'.
                    $additionalInfoString.
                '</div>
                <div class="row"></div>
            </div>
        </li>    
    </ul>
';

$this->registerJs(<<<JS
    var statusClass = '$statusClass';

    // initialize collapsible element
    $('.collapsible').collapsible();

    // initialize dropdown button
    $('.dropdown-button').dropdown();

JS);
