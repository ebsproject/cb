<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\plantingInstruction\models;

use app\dataproviders\ArrayDataProvider;
use app\models\BackgroundJob;
use app\models\BaseModel;
use app\models\PlantingJob;
use app\models\UserDashboardConfig;

use Yii;

/**
 * This is the model class for Planting Instructions Manager application.
 */
class PlantingJobModel extends BaseModel {
	
    public function __construct(
        public BackgroundJob $backgroundJob,
        public PlantingJob $plantingJob,
    ) { }
    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundProcesses){
        $transactions = [];

        foreach ($backgroundProcesses as $key => $record) {
            $status = strtolower($record['jobStatus']);

            if($status == 'done'){
                $iconClass = 'green';
                $icon = 'check';
            }else if(in_array($status, ['in_progress', 'in_queue'])){
                $iconClass = 'grey';
                $icon = 'hourglass_empty';
            }else if($status == 'failed'){
                $iconClass = 'red';
                $icon = 'priority_high';
            }else{
                $iconClass = 'grey';
                $icon  = 'near_me';
            }

            $entityId = $record['entityDbId'];
            $endpointEntity = strtoupper($record['endpointEntity']);

            $message = $record['description'] . ' ' . str_replace('_', ' ', $status) . '!';
            $timeDiff = $this->backgroundJob->ago($record['modificationTimestamp']);

            $fieldParam = 'planting_job.id AS plantingJobDbId | planting_job.planting_job_code AS plantingJobCode';
            $filterFieldParam = 'plantingJobDbId';
            $displayField = 'plantingJobCode';
            
            $params = [
                'fields' => $fieldParam,
                $filterFieldParam => "equals $entityId",
            ];

            $result = $this->plantingJob->searchAll($params, 'limit=1', false);
            $entityName = isset($result['data'][0][$displayField]) ? $result['data'][0][$displayField] : '';
            $abbrev = isset($result['data'][0]['abbrev']) ? $result['data'][0]['abbrev'] : '';
            $item = '<li class="bg_notif-btn" data-transaction_id="'.$entityId.'" data-abbrev="'.$abbrev.'">
                <a class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle small '.$iconClass.'">'.$icon.'</span>
                    '.$message.'
                </a>';

            $timeElement = '<time class="media-meta">';
            $item .= $timeElement.' <b>'.$entityName.'</b></time>'.$timeElement.$timeDiff.'</time></li>';
            $transactions[] = $item;
        }

        return implode(' ', $transactions);

    }

    /**
     * Update Planting job status based from backgroud process status
     *
     * @param integer $userId user identifier
     */
    public function updatePlantingJobStatus($userId){
        $params = [
            'creatorDbId' => 'equals '.$userId,
            'application' => 'equals PLANTING_INSTRUCTIONS_MANAGER',
            'jobStatus' => 'equals DONE',
            'jobRemarks' => 'null'
        ];

        $result = $this->backgroundJob->searchAll($params);

        if($result['status'] == 200 && $result['totalCount'] > 0){
            foreach ($result['data'] as $key => $record) {
                $entityDbId = $record['entityDbId'];
                $description = strtolower($record['description']);
                // if background process is already done and the status is not yet updated
                if($record['jobStatus'] == 'DONE' && $record['jobRemarks'] != 'REFLECTED'){

                    // if updating entry records, change status to draft
                    if($description == 'updating entry records'){
                        
                        $this->plantingJob->updateOne($entityDbId, ['plantingJobStatus' => 'draft']);

                        // update background job transaction
                        $this->backgroundJob->updateOne($record['backgroundJobDbId'], ['jobRemarks' => 'REFLECTED']);
                    }
                }
            }
        }
    }

    /**
     * Push notifications from background process
     *
     * @param integer $userId user identifier
     * @return array list of notifications
     */
    public function pushNotifications($userId){

        // get new notifications
        $params = [
            'creatorDbId' => "equals $userId",
            'application' => 'equals PLANTING_INSTRUCTIONS_MANAGER',
            'jobStatus' => 'equals DONE|equals IN_QUEUE|equals IN_PROGRESS|equals FAILED',
            'isSeen' => 'equals false'
        ];

        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $filter, false);

        $newNotifications = isset($result['data']) ? $result['data'] : [];
        $unseenCount = count($newNotifications);
        $unseenNotifications = $this->getBackgroundJobNotifications($newNotifications);
        
        // get old notifications
        $oldNotifParams = [
            'creatorDbId' => "equals $userId",
            'application' => 'equals PLANTING_INSTRUCTIONS_MANAGER',
            'jobStatus' => 'equals DONE|equals IN_QUEUE|equals IN_PROGRESS|equals FAILED',
            'isSeen' => 'equals true'
        ];
        $oldNotifResult = $this->backgroundJob->searchAll($oldNotifParams, $filter, false);
        $oldNotifications = isset($oldNotifResult['data']) ? $oldNotifResult['data'] : [];
        $seenNotifications = $this->getBackgroundJobNotifications($oldNotifications);

        // update unseen notifications to seen
        $updateParams = [
            'jobIsSeen' => 'true'
        ];

        $this->backgroundJob->update($newNotifications, $updateParams);

        return [
            'unseenNotifications' => $unseenNotifications,
            'seenNotifications' => $seenNotifications,
            'unseenCount' => $unseenCount
        ];
        
    }

    /**
     * Search (POST) for planting job info
     * given a plantingJobDbId, params, and
     * return in a DataProvider type
     *
     * @param array $params params
     * 
     * @return ArrayDataProvider $dataProvider occurrence summary records
     */
    public function getPackingJobInfo ($params)
    {
        extract($params);
    
        Yii::debug("Searching for planting job info " . json_encode(['params' => $params]));

        $dataProviderId = 'planting-job-info-browser';

        $url = "planting-jobs-search?limit=1&sort=plantingJobDbId:desc";

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            $url,
            json_encode([
                "fields" => "planting_job.id AS plantingJobDbId |
                    planting_job.planting_job_code AS plantingJobCode |
                    planting_job.planting_job_status AS plantingJobStatus |
                    planting_job.is_submitted AS isSubmitted |
                    program.program_code AS programCode |
                    program.program_name AS programName |
                    creator.person_name AS creator |
                    planting_job.planting_job_due_date AS plantingJobDueDate |
                    facility.facility_code AS facilityCode |
                    facility.facility_name AS facilityName |
                    planting_job.creation_timestamp AS creationTimestamp |
                    planting_job.modification_timestamp AS modificationTimestamp |
                    planting_job.planting_job_instructions AS plantingJobInstructions",
                "plantingJobDbId" => "equals $id",
            ])
        );

        return new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'totalCount' => $output['totalCount'],
        ]);
    }
}