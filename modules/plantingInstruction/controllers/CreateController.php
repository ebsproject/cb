<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\plantingInstruction\controllers;

use app\components\B4RController;
use app\components\GenericFormWidget;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;
use app\models\Api;
use app\models\Browser;
use app\models\Config;
use app\models\Entry;
use app\models\Experiment;
use app\models\Occurrence;
use app\models\Package;
use app\models\PlantingInstruction;
use app\models\PlantingJob;
use app\models\PlantingJobEntry;
use app\models\PlantingJobOccurrence;
use app\models\Program;
use app\models\User;
use app\models\Variable;
use app\models\ScaleValue;
use app\models\Seed;
use app\models\Worker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

use Yii;

/**
 * Create controller for the `plantingInstruction` module
 */
class CreateController extends B4RController
{

    public function __construct ($id, $module,
        protected Api $api,
        protected Browser $browser,
        protected Config $configuration,
        protected Entry $entry,
        protected Experiment $experiment,
        protected GenericFormWidget $genericFormWidget,
        protected IVariableFilterComponent $variableFilterComponent,
        protected Occurrence $occurrence,
        protected Package $package,
        protected PlantingInstruction $plantingInstruction,
        protected PlantingJob $plantingJob,
        protected PlantingJobEntry $plantingJobEntry,
        protected PlantingJobOccurrence $plantingJobOccurrence,
        protected Program $program,
        protected Variable $variable,
        protected Seed $seed,
        protected ScaleValue $scaleValue,
        protected Worker $worker,
        $config=[]
    )
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Get planting job code by ID
     *
     * @param Integer $id Planting Job identifier
     * @return Text $plantingJobCode Planting Job code
     */
    public function getPlantingJobCode($id){
        $plantingJobParam['plantingJobDbId'] = "equals $id";
        $plantingJob = $this->plantingJob->searchAll($plantingJobParam, '', false);

        return isset($plantingJob['data'][0]['plantingJobCode']) ? $plantingJob['data'][0]['plantingJobCode'] : '';
    }

    /**
     * Renders the browser for the occurrences
     *
     * @param Text $program Current program of the user
     * @param Integer $id Planting job transaction
     * @return mixed browser for planting instructions manager
     */
    public function actionIndex($program, $id)
    {
        if (Yii::$app->access->renderAccess("PIM_UPDATE", "PLANTING_INSTRUCTIONS_MANAGER", usage: 'url')) {
            $userModel = new User();
            $userId = $userModel->getUserId();
            $filteredIdName = "pim_$userId"."_filtered_occurrence_ids_$id";
            $selectedIdName = "pim_$userId"."_selected_occurrence_ids_$id";
            
            //destroy selected items session
            if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entry-list-grid-page'])) ||
                (!isset($_GET['reset']) && !isset($_GET['dp-1-page']))) && isset($_SESSION[$selectedIdName])){
                unset($_SESSION[$selectedIdName]);
                if(isset($_SESSION[$filteredIdName])){
                    unset($_SESSION[$filteredIdName]);
                }
            }

            // get planting job code
            $plantingJobCode = $this->getPlantingJobCode($id);

            // render browser for planting job occurrences
            $searchModel = $this->plantingJobOccurrence;

            $params = Yii::$app->request->queryParams;

            $data = $searchModel->search($params, $id);

            // retrieve selected items
            $storedItems = Yii::$app->session->get($selectedIdName);

            // retrieve envelopes step configuration
            $configData = $this->configuration->getFormConfig('PACKING_JOB_FIELDS', 'envelopes');

            $formColumns = $this->buildFormColumns($configData, 'plantingJobOccurrence');

            return $this->render('index',[
                'program' => $program,
                'id' => $id,
                'plantingJobCode' => $plantingJobCode,
                'dataProvider' => $data,
                'searchModel' => $searchModel,
                'selectedItems' => $storedItems,
                'formColumns' => $formColumns,
                'configData' => $configData
            ]);
        }
    }

    /**
     * Create planting job
     *
     * @param Text $program Program abbrev
     * @param Integer $programId Program identifier
     */
    public function actionCreatePlantingJob($program, $programId){

        // get occurrence IDs in session
        $occurrenceIds = Yii::$app->session->get('em-pim-occurrence-ids');

        $plantingJobParams['records'][] = [
            'plantingJobStatus' => 'draft',
            'plantingJobType' => 'packing job',
            'programDbId' => $programId
        ];

        // insert planting job record
        $plantingJob = $this->plantingJob->create($plantingJobParams);

        if(isset($plantingJob['success']) && $plantingJob['success'] && isset($plantingJob['data'][0]['plantingJobDbId'])){
            $plantingJobDbId = $plantingJob['data'][0]['plantingJobDbId'];

            // insert planting job occurrence records
            foreach ($occurrenceIds as $value) {
                $occParams = [];
                $records[] = [
                    'plantingJobDbId' => (string) $plantingJobDbId,
                    'occurrenceDbId' => (string) $value
                ];

                // update Occurrence status
                $occParams['occurrenceDbId'] = "equals $value";

                // get current occurrence status
                $occData = $this->occurrence->searchAll($occParams, 'limit=1', false);
                $status = isset($occData['data'][0]['occurrenceStatus']) ? $occData['data'][0]['occurrenceStatus'] : '';

                $newStatus = "$status;draft packing";

                // update status of occurrences
                $updateOccParams['occurrenceStatus'] = $newStatus;
                $succ = $this->occurrence->updateOne($value, $updateOccParams);

            }
            $plantJobOccParams['records'] = $records;
            $plantingJobOccurrence = $this->plantingJobOccurrence->create($plantJobOccParams);
        }

        $this->redirect(['/plantingInstruction/create?program='.$program.'&id='.$plantingJobDbId]);
    }

    /**
     * Set selected items in the grid
     */
    public function actionStoreSessionItems(){

        $count = 0;
        $items = [];

        if(isset($_POST['unset'], $_POST['entityDbId'], $_POST['storeAll'], $_POST['plantingJobDbId'], $_POST['sessionName'])){
            extract($_POST);

            $userModel = new User();
            $userId = $userModel->getUserId();
            $filteredIdName = "pim_$userId"."_filtered_$sessionName"."_$plantingJobDbId";
            $selectedIdName = "pim_$userId"."_selected_$sessionName"."_$plantingJobDbId";   

            $items = (isset($_SESSION[$selectedIdName]) && $_SESSION[$selectedIdName] !== null) ? $_SESSION[$selectedIdName] : [];

            if($unset == "true"){
                if($storeAll == "true"){
                    $items = [];
                }
                else{

                    foreach($items as $key => $value){
                        $index = $value;

                        if($index == $entityDbId){
                            unset($items[$key]);
                        }
                    }
                }
            }
            else{
                if($storeAll == "true"){
                    $items = Yii::$app->session->get($filteredIdName);
                }
                else if(!in_array($entityDbId, $items)){     
                    $items[] = $entityDbId;
                }
            }

            $_SESSION[$selectedIdName] = $items;

            $count = (!empty($items)) ? count($items) : 0;
        }
        return json_encode($count);

    }

    /**
     * Build form columnss
     *
     * @param Array $fields Fields to be displayed in form
     * @param Text $entity Entity to be retrieved
     */
    public function buildFormColumns($fields, $entity){
        $columns = [];
        foreach ($fields as $key => $field) {

            if(!(isset($field['is_hidden']) && $field['is_hidden'])){
                $column = strtoupper($field['variable_abbrev']);

                $visible = (isset($field['is_shown']) && $field['is_shown'] == false) ? true : false;

                $defaultValue = (isset($field['default']) && !empty($field['default'])) ? $field['default'] : '';

                $varAbbrev = 'abbrev='.$column;
                $variableData = $this->variable->getAll($varAbbrev, false);

                $variable = [];
                if(isset($variableData['data'][0])){
                    $variable = $variableData['data'][0];
                }

                $header = isset($field['display_name']) && !empty($field['display_name']) ? $field['display_name'] : $variable['label'];
                $dataType = empty($variable) || !isset($variable) ? [] : $variable['dataType'];

                // check if field is required
                $isRequiredLabel = $isRequiredClass = '';
                $isRequired = false;
                if(isset($field['required']) && $field['required'] == 'required'){
                    $isRequiredLabel = '<span class="required" title="Required field">*</span>';
                    $isRequiredClass = ' required-field';
                    $isRequired = $field['required'];
                }

                // check if the field is disabled
                $isDisabled = isset($field['disabled']) ? $field['disabled'] : false;

                $attribute = isset($field['target_column']) && empty($field['secondary_target_column'] && $field['variable_type'] !== 'metadata') ? $field['target_column'] : $this->plantingJob->formatToCamelCase($column);

                // build field value
                $fieldValue = function($data) use ($defaultValue, $field, $entity, $attribute){
                    // get saved value if set else default value from config

                    $field['default'] = isset($data[$attribute]) ? $data[$attribute] : $defaultValue;
                
                    $rowId = $data[$entity.'DbId'];

                    $this->genericFormWidget->options = [
                        'config' => [$field],
                        'rowId' => $rowId,
                        'rowClass' => 'editable-field form-fields'
                    ];
                    $formattedValue = $this->genericFormWidget->generateFields();

                    return $formattedValue['requiredFields'][0]['rawValue'];
                };

                $order = !empty($isRequiredLabel) ? DynaGrid::ORDER_FIX_RIGHT : DynaGrid::ORDER_MIDDLE;
            }

            $formattedField =
                [
                    'attribute'=> $attribute,
                    'label' => '<span title="'.$header.'">'.Yii::t('app', $header).' '.$isRequiredLabel,
                    'format' => 'raw',
                    'encodeLabel'=> false,
                    'vAlign' => 'middle',
                    'visible' => true,
                    'value' => $fieldValue,
                    'order' => $order
                ];

            // if is sufficient field
            if($attribute == 'isSufficient'){

                $filterOptions = 
                    [
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => [
                                'true' => 'Sufficient',
                                'false' => 'Insufficient'
                        ],   
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'allowClear' => true,
                            ]
                        ],
                        'filterInputOptions' => [
                            'placeholder' => '',
                        ],
                        'contentOptions' => [
                            'style'=>'min-width: 120px;'
                        ],
                    ];

                $formattedField = array_merge($formattedField, $filterOptions);
            }

            $columns[] = $formattedField;
        }

        return $columns;
    }

    /**
     * Render bulk update form
     */
    public function actionRenderBulkUpdate(){
        if(isset($_POST['configVars'])){
            $configVars = json_decode($_POST['configVars'], true);

            foreach($configVars as $key => $value){
                if(!(isset($value['is_hidden']) && $value['is_hidden'])){ 
                    $varAbbrev = 'abbrev='.$value['variable_abbrev'];
                    $variableData = $this->variable->getAll($varAbbrev, false);
                    
                    $varInfo = [];
                    if(isset($variableData['data'][0])){
                        $varInfo = $variableData['data'][0];
                    }
                    
                    $header = isset($value['display_name']) && !empty($value['display_name']) ? $value['display_name'] : $varInfo['label'];
                    $fields[$varInfo['abbrev']] = ucwords(strtolower($header));
                }
            }

            $htmlData = $this->renderAjax('_bulk_update_form.php',[
               'data' => $fields
            ], true, false);
          
            return json_encode($htmlData);
        }
    }

    /**
     * Render the modal form field element
     *
     * @return $htmlData Field element
     */
    public function actionRenderConfigForm(){

        $config = $widgetOptions = [];
        $dataEndpoint = '';

        if(isset($_POST)){
            $data = $_POST;
            
            // get processed widget options
            $config = $this->plantingJob->getConfigForm($data);
            
            $htmlData = $this->renderAjax('_modal_generic_form.php',[
                'widgetOptions' => $config['widgetOptions'],
                'dataEndpoint' => $config['dataEndpoint'],
            ]);

            return json_encode($htmlData);
        }
    }

    /**
     * Retrieve selected items stored in session
     */
    public function actionGetSelectedItems(){

        extract($_POST);

        // if in entry management
        if($sessionName == 'entry_ids'){
            $selectedIdName = "planting-job-entry-param-url-".$plantingJobDbId;

            $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
            $response = [
                'selectedItems' => $selectedItems,
                'selectedItemsCount' => count($selectedItems)
            ];
            return json_encode(true);
        }

        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedIdName = "pim_$userId"."_selected_".$sessionName."_$plantingJobDbId";

        $selectedItems = (Yii::$app->session->get($selectedIdName) !== null) ? Yii::$app->session->get($selectedIdName) : [];
        $response = [
            'selectedItems' => $selectedItems,
            'selectedItemsCount' => count($selectedItems)
        ];

        return json_encode($response);
    }

    /**
     * Save row value
     */
    public function actionSaveRowValue(){
        
        if(isset($_POST['column']) && isset($_POST['value']) && isset($_POST['entityDbId'])){
            extract($_POST);

            $formattedColumn = $this->plantingJob->formatToCamelCase($column);

            // update planting job occurrence field value
            $params[$formattedColumn] = (string) $value;

            // if in entry management browser
            if(isset($plantingJobDbId) && !empty($plantingJobDbId)){
                $entryExists = $this->chekIfEntryExists($plantingJobDbId);

                if($entryExists){
                    $params['notes'] = 'UPDATED';
                }
            }

            $this->$modelClass->updateOne($entityDbId, $params);
        }
    }

    /**
     * Check if entries already exist in Planting job
     *
     * @param Integer $id Planting job identifier
     * @return Boolean Whether entries already exist or not
     */
    public function chekIfEntryExists($id){
        // check if entry already exists for the planting job
        $params['plantingJobDbId'] = "equals $id";
        $data = $this->plantingJobEntry->searchAll($params, 'limit=1', false);

        // if entries already exist
        if($data['totalCount']){
            return true;
        }

        return false;
    }

    /**
     * Bulk update fields in grid view
     */
    public function actionBulkUpdateFields(){
        $count = 0;
        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedItemsCount = 0;
        $variableDbId = 0;

        if(isset($_POST['mode'], $_POST['variable'], $_POST['dataValue'], $_POST['plantingJobDbId'], $_POST['sessionName'], $_POST['modelClass'], $_POST['entityIdName'])){
            extract($_POST);

            if($mode == 'selected'){
                $selectedIdName = "pim_$userId"."_selected_".$sessionName."_$plantingJobDbId";
                $selectedItems = Yii::$app->session->get($selectedIdName);
            }else{

                $selectedIdName = "pim_$userId"."_filtered_".$sessionName."_$plantingJobDbId";
                $selectedItems = Yii::$app->session->get($selectedIdName);

            }

            // get variable info
            $formattedColumn = $this->plantingJob->formatToCamelCase($variable);

            $varAbbrev = 'abbrev='.$variable;
            $variableData = $this->variable->getAll($varAbbrev, false);

            if(isset($variableData['data'][0])){
                $variableData = $variableData['data'][0];
                $variableDbId = $variableData['variableDbId'];
            }

            // get number of records to be updated
            $selectedItemsCount = count($selectedItems);

            // update planting job occurrence field value
            $requestParams[$formattedColumn] = (string) $dataValue;

            // if in entry management browser
            if(isset($plantingJobDbId) && !empty($plantingJobDbId)){
                $entryExists = $this->chekIfEntryExists($plantingJobDbId);

                if($entryExists){
                    $params['notes'] = 'UPDATED';
                }
            }

            // update records
            $this->$modelClass->updateMany($selectedItems, $requestParams);
        }

        $result = [
            'count' => $selectedItemsCount,
            'selectedItems' => $selectedItems,
            'variable' => $variable,
            'variableDbId' => $variableDbId,
            'variableType' => 'identification'
        ];

        return json_encode($result);

    }

    /**
     * Bulk update entry fields in grid view
     */
    public function actionBulkUpdateEntryFields(){
        $count = 0;
        $userModel = new User();
        $userId = $userModel->getUserId();
        $selectedItemsCount = 0;
        $variableDbId = 0;

        if(isset($_POST['mode'], $_POST['variable'], $_POST['dataValue'], $_POST['plantingJobDbId'])){
            extract($_POST);
            $totalCount = (int) $totalCount;

            // retrieve threshold value
            $limit = Yii::$app->config->getAppThreshold('PLANTING_INSTRUCTIONS_MANAGER', 'updateEntries');

            $filters = Yii::$app->session->get('planting-job-entry-param-filters-' . $plantingJobDbId);

            // get selected entries with pagination
            $urlWithPagination = Yii::$app->session->get('planting-job-entry-param-url-with-pagination-'.$plantingJobDbId);
            
            // get selected items with pagination
            $selectedWithPagination = Yii::$app->api->getParsedResponse('POST',
                $urlWithPagination, 
                json_encode($filters)
            );

            $selectedItemsWithPagination = isset($selectedWithPagination['data']) ? array_column($selectedWithPagination['data'], $entityIdName) : [];

            if($mode == 'selected'){

                $selectedItems = json_decode($_POST['dbIds']);

                $selectedItemsWithPagination = array_intersect($selectedItemsWithPagination, $selectedItems);
            } else if($mode == 'all' && $totalCount < $limit) {
                $url = Yii::$app->session->get('planting-job-entry-param-url-' . $plantingJobDbId);

                $entityDbIds = Yii::$app->api->getParsedResponse('POST',
                    $url, 
                    json_encode($filters),
                    '',
                    true
                );

                $selectedItems = isset($entityDbIds['data']) ? array_column($entityDbIds['data'], $entityIdName) : [];
            }

            // get variable info
            $formattedColumn = $this->plantingJob->formatToCamelCase($variable);

            $varAbbrev = 'abbrev='.$variable;
            $variableData = $this->variable->getAll($varAbbrev, false);

            if(isset($variableData['data'][0])){
                $variableData = $variableData['data'][0];
                $variableDbId = $variableData['variableDbId'];
            }
            
            // if records is greater that threshold, use background processing
            if($totalCount >= $limit && $mode == 'all'){

                $addtlParams = [
                    'description' => 'Updating entry records',
                    'entity' => 'ENTRY',
                    'entityDbId' => (string) $plantingJobDbId,
                    'endpointEntity' => 'PLANTING_JOB',
                    'application' => 'PLANTING_INSTRUCTIONS_MANAGER'
                ];

                $dataArray = [
                    'searchPath' => 'planting-job-entries-search',
                    'filters' => $filters,
                    'updatePath' => 'planting-job-entries',
                    'updateField' => $formattedColumn,
                    'updateValue' => (string) $dataValue,
                    'idName' => 'plantingJobEntryDbId',
                    'processName' => 'update-filter-records'

                ];

                $this->plantingJobEntry->updateMany(null, $dataArray, true, $addtlParams);

                // update of planting job status
                $updateParams['plantingJobStatus'] = 'updating entries in progress';
                $this->plantingJob->updateOne($plantingJobDbId, $updateParams);

                // notify and redirect user in PIM browser
                $message = 'The entries for <b>' . $plantingJobCode . '</b> are being updated in the background. You may proceed with other tasks. You will be notified in this page once done.';

                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> ' . $message);

                return $this->redirect(['/plantingInstruction?program=' . $program . '&PlantingJob[plantingJobDbId]=' . $plantingJobDbId]);

            }else{

                $requestParams = [
                    $formattedColumn => (string) $dataValue
                ];

                // update records
                foreach ($selectedItems as $key => $value) {
                    $this->plantingJobEntry->updateOne($value, $requestParams);
                }
            }

            // get number of records to be updated
            $selectedItemsCount = count($selectedItems);

        }

        $result = [
            'count' => $selectedItemsCount,
            'selectedItems' => $selectedItemsWithPagination,
            'variable' => $variable,
            'variableDbId' => $variableDbId,
            'variableType' => 'identification'
        ];

        return json_encode($result);

    }

    /**
     * Renders the page for Entry Management
     *
     * @param Text $program Current program of the user
     * @param Integer $id Planting job transaction
     * @return mixed page for entry management
     */
    public function actionEntry($program, $id)
    {
        if (Yii::$app->access->renderAccess("PIM_UPDATE", "PLANTING_INSTRUCTIONS_MANAGER", usage: 'url')) {

            // generate entries if not yet existing
            $this->generateEntries($id);

            $userModel = new User();
            $userId = $userModel->getUserId();

            $filteredIdName = "pim_$userId"."_filtered_entry_ids_$id";
            $selectedIdName = "pim_$userId"."_selected_entry_ids_$id";

            //destroy selected items session
            if(((isset($_GET['reset']) && $_GET['reset'] == 'hard_reset' && !isset($_GET['grid-entries-grid-page'])) ||
                (!isset($_GET['reset']) && !isset($_GET['dp-1-page']))) && isset($_SESSION[$selectedIdName])){
                unset($_SESSION[$selectedIdName]);
                if(isset($_SESSION[$filteredIdName])){
                    unset($_SESSION[$filteredIdName]);
                }
            }
            
            // get planting job code
            $plantingJobCode = $this->getPlantingJobCode($id);

            // render browser for planting job occurrences
            $searchModel = $this->plantingJobEntry;

            $params = Yii::$app->request->queryParams;

            $data = $searchModel->search($params, $id);

            // retrieve selected items
            $storedItems = Yii::$app->session->get($selectedIdName);

            // form config
            $configData = [
                [
                    'variable_abbrev' => 'IS_SUFFICIENT',
                    'display_name' => 'Status',
                    'fixed' => true,
                    'required' => 'required',
                    'variable_type' => 'identification',
                    'api_resource_endpoint' => 'planting-job-entries',
                ]
            ];

            $formColumns = $this->buildFormColumns($configData, 'plantingJobEntry');

            // get count of entries with insufficient seeds
            $insufficientSummary = $this->actionGetInsufficientEntriesSummary($id);

            return $this->render('entry',[
                'id' => $id,
                'program' => $program,
                'plantingJobCode' => $plantingJobCode,
                'dataProvider' => $data['data'],
                'totalCount' => $data['totalCount'],
                'searchModel' => $searchModel,
                'selectedItems' => $storedItems,
                'formColumns' => $formColumns,
                'configData' => $configData,
                'insufficientSummary' => $insufficientSummary
            ]);
        }
    }

    /**
     * Get summary of entries with insufficient seed
     *
     * @param Integer $id Planting job identifier
     * @return Text Insufficient seed summary
     */
    public function actionGetInsufficientEntriesSummary($id){
        $params['plantingJobDbId'] = "equals $id";
        $params['isSufficient'] = 'false';
        $data = $this->plantingJobEntry->searchAll($params, 'limit=1', false);
        $count = 0;

        if(isset($data['totalCount'])){
            $count = $data['totalCount'];
        }
        
        return $this->getSeedSummary($count);
    }

    /**
     * Get summary of entries with insufficient seed by count
     *
     * @param Integer $count Count of entries with insufficient seed
     * @return Text Insufficient seed summary
     */
    public function getSeedSummary($count){
        if($count > 0){
            if($count < 11){
                $words = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten'];
                $count = $totalInsufficientCount = $words[$count-1];
            } else {
                $totalInsufficientCount = number_format($count);
            }
            return '<i class="material-icons orange-text left">warning</i><b>' . $totalInsufficientCount . ' of your entries have insufficient seed.</b>';
        } else {
            return 'All entries have sufficient seed.';
        }
    }

    /**
     * Generate entries
     *
     * @param Integer $id Planting job identifier
     */
    public function generateEntries($id){
        // check if entry already exists for the planting job
        $params['plantingJobDbId'] = "equals $id";
        $data = $this->plantingJobEntry->searchAll($params, 'limit=1', false);

        // check if there are updated values
        $pjoParams['plantingJobDbId'] = "equals $id";
        $pjoParams['notes'] = 'UPDATED';
        $pjoData = $this->plantingJobOccurrence->searchAll($pjoParams, '', true);

        // if there are no entries, generate entries
        if(!$data['totalCount'] || $pjoData['totalCount'] > 0){
            $data = Yii::$app->api->getParsedResponse('POST',
                'planting-jobs/'.$id.'/entry-generations'
            );
        }

        // if there are updated planting job occurrence, remove notes
        if(isset($pjoData['data']) && $pjoData['totalCount'] > 0){
            $pjoUpdateParams['notes'] = '';
            foreach ($pjoData['data'] as $key => $value) {
                $this->plantingJobOccurrence->updateOne($value['plantingJobOccurrenceDbId'], $pjoUpdateParams);
            }
        }
    }

    /**
     * Retrieve all entry IDs
     *
     * @param Integer $plantingJobDbId Planting job identifier 
     * @return Array $entryDbIdArray Array of IDs
     */
    public function actionRetrieveEntryDbIds($plantingJobDbId = null)
    {
        $method = 'POST';
        $plantingJobDbId = (!$plantingJobDbId) ? $_POST['plantingJobDbId'] : $plantingJobDbId;

        // retrieve paramSort from either function parameter, session, or $_POST
        $paramSort = Yii::$app->session->get('planting-job-entry-param-sort-' . $plantingJobDbId);

        $url = 'planting-job-entries-search?' . $paramSort;

        // retrieve filters from either function parameter, session, or $_POST
        $filters = Yii::$app->session->get('planting-job-entry-param-filters-' . $plantingJobDbId);

        $checkedBoxesString = isset($_POST['checkedBoxesString']) ?  $_POST['checkedBoxesString'] : '';

        if ($checkedBoxesString !== '') $filters['plantingJobEntryDbId'] = $checkedBoxesString;

        // all attributes in the browser and corresponding fields parameter
        $attributes = [
            'plantingJobDbId' => 'pj.id AS plantingJobDbId',
            'experimentDbId' => 'e.id AS experimentDbId',
            'entryNumber' => 'et.entry_number AS entryNumber',
            'occurrenceDbId' => 'o.id AS occurrenceDbId',
            'experimentName' => 'e.experiment_name AS experimentName', 
            'entryName' => 'et.entry_name AS entryName',
            'germplasmDbId' => 'et.germplasm_id AS germplasmDbId',
            'siteCode' => 'gs.geospatial_object_code AS siteCode',
            'occurrenceName' => 'o.occurrence_name AS occurrenceName',
            'envelopeCount' => 'pje.envelope_count AS envelopeCount',
            'requiredPackageQuantity' => 'pje.required_package_quantity AS requiredPackageQuantity',
            'packageDbId' => 'p.id AS packageDbId',
            'packageQuantity' => 'p.package_quantity AS packageQuantity',
            'isSufficient' => 'pje.is_sufficient AS isSufficient',
            'isReplaced' => 'pje.is_replaced AS isReplaced',
        ];

        // default fields param
        $defaultFieldsParams = 'pje.id AS plantingJobEntryDbId';
        // default filter fields param
        $defaultFilterFieldsParam = 'pj.id AS plantingJobDbId';

        // build fields parameter based on sort params
        $fieldsParam = $this->browser->buildFieldsParam($attributes, $defaultFieldsParams, $paramSort, $filters, $defaultFilterFieldsParam);
       
        // if there's a fields parameter
        if (!empty($fieldsParam)) {
            $filters['fields'] = "$fieldsParam|et.germplasm_id AS germplasmDbId|p.id AS packageDbId|actualEntryDbId|actualGermplasmDbId|actualGermplasmName|actualPackageDbId";
        }

        array_walk($filters, function (&$value, $key) {
            if (
                $key != 'fields' &&
                $value != '' &&
                !str_contains(strtolower($value), 'equals') &&
                !str_contains(strtolower($value), '%')
            )
                $value = "equals $value";
        });

        $response = Yii::$app->api->getParsedResponse(
            $method,
            $url,
            json_encode($filters),
            '',
            true
        );

        $data = [];
        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        ) {
            // Consolidate all Entry records into a single array
            $data = $response['data'];

        }

        $experimentDbIdArray = [];
        $plantingJobEntryDbIdArray = [];
        $entryDbIdArray = [];
        $germplasmDbIdArray = [];
        $packageDbIdArray = [];

        $retrievedGermplasmNames = [];

        foreach ($data as $key => $value) {
            $experimentDbIdArray []= strval($value['experimentDbId']);
            $plantingJobEntryDbIdArray []= strval($value['plantingJobEntryDbId']);
            $entryDbIdArray []= strval($value['actualEntryDbId']);
            $germplasmDbIdArray []= strval($value['actualGermplasmDbId']);
            $packageDbIdArray []= strval($value['actualPackageDbId']);
            $retrievedGermplasmNames []= $value['actualGermplasmName'];
        }

        // Remove duplicate data values and return
        return json_encode([
            'experimentDbIdArray' => array_values(array_unique($experimentDbIdArray)),
            'plantingJobEntryDbIdArray' => $plantingJobEntryDbIdArray,
            'entryDbIdArray' => array_values(array_unique($entryDbIdArray)),
            'germplasmDbIdArray' => array_values(array_unique($germplasmDbIdArray)),
            'packageDbIdArray' => array_values(array_unique($packageDbIdArray)),
            'retrievedGermplasmNames' => array_values(array_unique($retrievedGermplasmNames)),
        ]);
    }

    /**
     * Validate if all statuses are Sufficient
     *
     * @param Integer $id Planting job identifier
     * @return Boolean whether all statuses are sufficient or not
     */
    public function actionValidateEntryStatuses($id){

        $params = [
            'plantingJobDbId' => "equals $id",
            'isSufficient' => 'false'
        ];

        $data = $this->plantingJobEntry->searchAll($params, 'limit=1', false);

        return (isset($data['totalCount']) && $data['totalCount'] > 0) ? 'false' : 'true';
    }

    /**
     * Renders the page for Review and Submit Packing Job
     *
     * @param Text $program Current program of the user
     * @param Integer $id Planting job transaction
     * @param Integer $experimentDbId Experiment identifier
     * @return mixed page for review and submit
     */
    public function actionReview($program, $id, $experimentDbId=null)
    {
        if (Yii::$app->access->renderAccess("PIM_UPDATE", "PLANTING_INSTRUCTIONS_MANAGER", usage: 'url')) {

            // get planting job code
            $plantingJobCode = $this->getPlantingJobCode($id);

            // get packing job summary
            $summary = $this->plantingJob->summary($id);

            // get processed summary
            $processedSummary = $this->plantingJob->getProcessedSummary($summary);

            // get additional instructions configuration
            $configData = $this->configuration->getFormConfig('PACKING_JOB_FIELDS', 'review');

            // get experiments summary
            $experimentsSummary = isset($summary['experiments']) ? $summary['experiments'] : [];
            $experimentsSummary = array_reverse($experimentsSummary);
            
            $processedExperimentsSummary = [];
            
            // sort parameters
            $sortAttrs = [
                'defaultOrder' => ['entryNumber' => SORT_ASC],
                'attributes' => ['entryNumber', 'entryDbId','germplasmName','isReplaced','totalRequiredPackageQuantity','totalEnvelopeCount', 'germplasmParentage']
            ];

            // loop through each experiments
            foreach ($experimentsSummary as $key => $value) {
                $processedExperimentsSummary[$value['experimentName']]['summary'] = $this->plantingJob->getProcessedSummary($value);

                // get experiment ID
                $experimentId = isset($value['experimentDbId']) ? $value['experimentDbId'] : 0;

                $paramLimit = 'limit=10';
                $gridName = "entry-summaries-$experimentId";

                $paramPage = isset($_GET["$gridName-page"]) ? "&page=" . $_GET["$gridName-page"] : "&page=";

                $paramSort = '';
                if (isset($_GET["$gridName-sort"])){
                    if(strpos($_GET["$gridName-sort"],'-') !== false){
                        $sort = str_replace('-', '', $_GET["$gridName-sort"]);
                        $paramSort = "&sort=$sort:DESC";
                    }
                    else{
                        $paramSort = '&sort='.$_GET["$gridName-sort"];
                    }
                }

                $data = Yii::$app->api->getParsedResponse('GET',
                    'planting-jobs/'.$id.'/entry-summaries?experimentDbId='.$experimentId.'&'.$paramLimit.$paramPage.$paramSort
                );

                $entries = isset($data['data'][0]['entry-summaries']) ? $data['data'][0]['entry-summaries'] : [];

                $entriesDataProvider[$experimentId] = new ArrayDataProvider([
                    'allModels' => $entries,
                    'id' => $gridName,
                    'key' => 'entryDbId',
                    'restified' => true,
                    'totalCount' => $data['totalCount'],
                    'sort' => $sortAttrs,
                ]);
            }

            $widgetOptions = [
                'mainModel' => 'PlantingJob',
                'config' => $configData,
                'mainApiResourceMethod' => 'POST',
                'mainApiResourceFilter' => ['plantingJobDbId' => (string) $id],
                'mainApiResource' => 'planting-jobs-search',
                'noInstructions' => 'true'
            ];
            $entriesDataProviderCheck = empty($entriesDataProvider) || !isset($entriesDataProvider) ? [] : $entriesDataProvider;

            return $this->render('review',[
                'id' => $id,
                'program' => $program,
                'plantingJobCode' => $plantingJobCode,
                'summary' => $processedSummary,
                'entriesDataProvider' => $entriesDataProviderCheck,
                'experimentsSummary' => $processedExperimentsSummary,
                'widgetOptions' => $widgetOptions
            ]);
        }
    }

    /**
     * Submit Packing Job creation
     *
     * @param Integer $id Packing job identifier
     * @param Text $code Packing job code
     * @param Text $program Current program of current user
     */
    public function actionSubmit ($id, $code, $program)
    {
        try { 
            // Update planting job status to "submission in progress"
            $this->plantingJob->updateOne($id, [ 'plantingJobStatus' => 'submission in progress']);

            // Trigger background worker
            $this->worker->invoke(
                'SubmitPackingJob',
                "Submitting $code",
                'PLANTING_INSTRUCTION',
                $id,
                'PLANTING_INSTRUCTION',
                'PLANTING_INSTRUCTIONS_MANAGER',
                'POST',
                [ 'plantingJobDbId' => $id, ]
            );

            // Notify and redirect
            Yii::$app->session->setFlash('info', "<i class='fa-fw fa fa-check'></i> The <b>$code</b> is being submitted in the background. You may proceed with other tasks. You will be notified in this page once done.");

            $this->redirect(["/plantingInstruction?program=$program&id=$id"]);
        } catch (\Exception $e) {
            // Update planting job status to "packing job failed"
            $this->plantingJob->updateOne($id, [ 'plantingJobStatus' => 'packing job failed']);

            // Notify and redirect
            Yii::$app->session->setFlash('warning', "<i class='fa-fw fa fa-warning'></i> There was a problem in submitting $code in the background. Please try again at a later time or <a href='https://ebsproject.atlassian.net/servicedesk/customer/portals'>report an issue</a>.");

            $this->redirect(["/plantingInstruction?program=$program&id=$id"]);
        }
    }

    /**
     * Cancel Packing Job
     *
     * @param Integer $id Packing job identifier
     * @param Text $code Packing job code
     * @param Text $program Current program of current user
     */
    public function actionCancelPackingJob ($id, $code, $program)
    {
        try { 
            // Update planting job status to "cancellation in progress"
            $this->plantingJob->updateOne($id, [ 'plantingJobStatus' => 'cancellation in progress']);

            // Trigger background worker
            $this->worker->invoke(
                'CancelPackingJob',
                "Cancelling $code",
                'PLANTING_INSTRUCTION',
                $id,
                'PLANTING_INSTRUCTION',
                'PLANTING_INSTRUCTIONS_MANAGER',
                'POST',
                [ 'plantingJobDbId' => $id, ]
            );

            // Notify and redirect
            Yii::$app->session->setFlash('info', "<i class='fa-fw fa fa-check'></i> The <b>$code</b> is being cancelled in the background. You may proceed with other tasks. You will be notified in this page once done.");

            $this->redirect(["/plantingInstruction?program=$program&id=$id"]);
        } catch (\Exception $e) {
            // Update planting job status to "packing job failed"
            $this->plantingJob->updateOne($id, [ 'plantingJobStatus' => 'packing job failed']);

            // Notify and redirect
            Yii::$app->session->setFlash('warning', "<i class='fa-fw fa fa-warning'></i> There was a problem in cancelling $code in the background. Please try again at a later time or <a href='https://ebsproject.atlassian.net/servicedesk/customer/portals'>report an issue</a>.");

            $this->redirect(["/plantingInstruction?program=$program&id=$id"]);
        }
    }

    /**
     * Update status of Occurrences in the Packing job
     *
     * @param Integer $id Packing job identifier
     * @param Text $packingStatus Packing job status
     */
    public function updateOccurrenceStatus($id, $packingStatus){

        // get occurrences in the Packing job
        $params['plantingJobDbId'] = "equals $id";
        $plantingJobOccs = $this->plantingJobOccurrence->searchAll($params);

        if(isset($plantingJobOccs['data']) && !empty($plantingJobOccs['data'])){
            $plantingJobOccs = $plantingJobOccs['data'];

            foreach ($plantingJobOccs as $key => $value) {
                $occurrenceId = $value['occurrenceDbId'];
                $occParams['occurrenceDbId'] = "equals $occurrenceId";

                // get current occurrence status
                $occData = $this->occurrence->searchAll($occParams, 'limit=1', false);
                $status = isset($occData['data'][0]['occurrenceStatus']) ? $occData['data'][0]['occurrenceStatus'] : '';

                // explode statuses
                $statusArr = explode(';', $status);

                $newStatusArr = [];
                foreach ($statusArr as $key => $value) {
                    // if status does not contain pack, include in new status array
                    if(strpos($value, 'pack') === false){
                        $newStatusArr[] = $value;
                    }
                }

                $status = implode(';', $newStatusArr);

                $packingStatus = ($packingStatus == 'draft') ? 'draft packing' : $packingStatus;
                $newStatus = $status . ';'.$packingStatus;

                // update status of occurrences
                $updateOccParams['occurrenceStatus'] = (string) $newStatus;

                $succ = $this->occurrence->updateOne($occurrenceId, $updateOccParams);
            }
        }
    }

    /**
     * Save Planting job info
     *
     * @param Integer $id Planting job identifier
     */
    public function actionSavePlantingJobInfo($id=null){
        $id = isset($_POST['id']) ? $_POST['id'] : $id;
        $plantingJobCode = isset($_POST['plantingJobCode']) ? $_POST['plantingJobCode'] : null;
        $column = isset($_POST['column']) ? $_POST['column'] : null;
        $value = isset($_POST['value']) ? $_POST['value'] : null;

        if(!empty($column)){
            $field = $this->plantingJob->formatToCamelCase($column);
            $field = ($field == 'plantingJobFacility') ? 'facilityDbId' : $field;
            $params[$field] = (string) $value;

            // update planting job
            $this->plantingJob->updateOne($id, $params);

            // if updating status, return status class
            if($column == 'PLANTING_JOB_STATUS'){

                // update status of Occurrences
                $this->updateOccurrenceStatus($id,$value);

                if ($value == 'updating packages in progress') {
                    $this->worker->invoke(
                        'CompletePackingJob',
                        "Completing $plantingJobCode",
                        'PLANTING_INSTRUCTION',
                        $id,
                        'PLANTING_INSTRUCTION',
                        'PLANTING_INSTRUCTIONS_MANAGER',
                        'POST',
                        [
                            'plantingJobDbId' => $id,
                        ]
                    );
    
                    $message = "The package quantities and seed withdraw logs for <b>$plantingJobCode</b> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.";
            
                    // set background processing info to session
                    Yii::$app->session->set('pim-bg-process-message', $message);
                    Yii::$app->session->set('pim-bg-process', true);    
                }

                // return status class
                return $this->plantingJob->getStatusClass($value);
            }
        }
    }

    /**
     * Render replace entry form
     *
     * @return Html Replace entry form
     */
    public function actionReplaceEntryForm(){
        $data = json_decode($_POST['data']);

        // set summary to session
        Yii::$app->session->set('original-entry-data', $data);

        // retrieve replacement type scale values
        $varRecord = $this->variable->getVariableByAbbrev('REPLACEMENT_TYPE');
        $scaleValues = $this->scaleValue->getVariableScaleValues($varRecord['variableDbId'], null);

        $viewFile = '@app/modules/plantingInstruction/views/create/_replace_entry_form.php';

        $htmlData = $this->renderAjax($viewFile,[
            'data' => $data,
            'scaleValues' => $scaleValues
        ]);

        return $htmlData;
    }

    /**
     * Render replace entry form
     *
     * @return Html Entry selection form
     */
    public function actionReplaceEntrySelectionForm(){
        $plantingJobEntryDbIds = $_POST['plantingJobEntryDbIds'] ?? null;
        $experimentId = $_POST['experimentId'];
        $replacementType = $_POST['replacementType'];
        $entryIdToReplace = $_POST['entryIdToReplace'];
        $germplasmIdToReplace = $_POST['germplasmIdToReplace'];
        $packageIdToReplace = isset($_POST['packageIdToReplace']) ? $_POST['packageIdToReplace'] : null;
        $plantingJobEntriesDataProvider = null;
        $germplasmName = '';
        $parentage = '';

        $viewFile = '@app/modules/plantingInstruction/views/create/_replace_entry_selection_form.php';

        // get selected planting job entries
        if ($plantingJobEntryDbIds) {
            $plantingJobEntriesData = $this->api->getApiResults(
                'POST',
                'planting-job-entries-search',
                json_encode([
                    'plantingJobEntryDbId' => 'equals ' . implode('|equals ', $plantingJobEntryDbIds)
                ]),
                '',
                true
            );

            $germplasmName = $plantingJobEntriesData['data'][0]['actualGermplasmName'];
            $parentage = $plantingJobEntriesData['data'][0]['actualParentage'];

            $plantingJobEntriesDataProvider = new ArrayDataProvider([
                'allModels' => $plantingJobEntriesData['data'],
                'key' => 'plantingJobEntryDbId',
                'pagination' => false,
            ]);
        }

        // Render view file with dropdown for Replace with Germplasm
        if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();
    
            $fieldTypeOptions = [];
            $fieldTypeOptionTooltip = [];
        
            if (isset($inputListFields) && !empty($inputListFields)) {
                foreach ($inputListFields as $key=>$option) {
                    $fieldTypeOptions[$key] = $option['text'];
                    $fieldTypeOptionTooltip[$key] = $option['description'];
                }
            }

            return json_encode([
                'data' => $this->renderAjax($viewFile, [
                    'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
                    'germplasmName' => $germplasmName,
                    'parentage' => $parentage,
                    'entity' => 'germplasm',
                    'fieldTypeOptions' => $fieldTypeOptions,
                    'fieldTypeOptionTooltip' => $fieldTypeOptionTooltip,
                    'packageIdToReplace' => $packageIdToReplace,
                ])
            ]);
        }

        // get selection form data provider and columns
        $data = $this->getReplacementSelectionData(
            $replacementType, 
            $experimentId, 
            $entryIdToReplace, 
            $germplasmIdToReplace,
            $packageIdToReplace
        );

        $columns = $this->getReplacementSelectionColumns($replacementType);

        // get replacement entity
        $entity = $this->getReplacementEntity($replacementType);

        $htmlData = $this->renderAjax($viewFile, [
            'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
            'germplasmName' => $germplasmName,
            'parentage' => $parentage,
            'dataProvider' => $data['dataProvider'],
            'totalCount' => $data['totalCount'],
            'columns' => $columns,
            'entity' => $entity,
            'packageIdToReplace' => null,
        ]);

        $data = [
            'data' => $htmlData,
            'count' => $data['totalCount']
        ];

        return json_encode($data);
    }

    /**
     * Get replacement selection columns
     *
     * @return Array List of data browser columns
     */
    public function getReplacementSelectionColumns($replacementType) {
        $columns = [];

        if($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'){
            $entityId = 'packageDbId';

            $columns = [
                [
                    'attribute' => 'label',
                    'label' => 'Package label',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'quantity',
                    'label' => 'Package quantity',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return (isset($data['quantity'])) ? number_format($data['quantity']) . ' ' . $data['unit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
                [
                    'attribute' => 'reserved',
                    'label' => 'Package reserved',
                ],
                [
                    'attribute' => 'seedName',
                    'enableSorting' => false,
                ],
            ];

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK'){
            $entityId = 'entryDbId';

            $columns = [
                [
                    'label' => Yii::t('app', 'Germplasm name'),
                    'value' => function ($data){
                        return $data['entryName'];
                    },
                    'format' => 'raw',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'entryCode',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'packageLabel',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'packageQuantity',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return isset($data['packageQuantity']) ? number_format($data['packageQuantity']) . ' ' . $data['packageUnit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
                [
                    'attribute' => 'reserved',
                    'label' => 'Package reserved'
                ],
            ];
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            $entityId = 'packageDbId';

            $columns = [
                [
                    'label' => Yii::t('app', 'Germplasm name'),
                    'value' => function ($data){
                        return $data['designation'];
                    },
                    'format' => 'raw',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'label',
                    'label' => 'Package label',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'quantity',
                    'label' => 'Package quantity',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data){
                        return (isset($data['quantity'])) ? number_format($data['quantity']) . ' ' . $data['unit'] : '<span class="not-set">(not set)</span>';
                    },
                ],
                [
                    'attribute' => 'reserved',
                    'label' => 'Package reserved',
                ],
                [
                    'attribute' => 'seedName',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'seedManager',
                    'enableSorting' => false,
                ],
            ];
        }

        $defaultCol = [
            [
                'header' => false,
                'hAlign' => 'center',
                'value' => function ($data) use ($entityId){
                    return '
                    <input id="'.$data[$entityId].'" class="with-gap" name="selection-radio-button" value="'.$data[$entityId].'" type="radio" />
                    <label for="'.$data[$entityId].'" style="padding-left: 18px !important; margin-left: -5px !important;"></label>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
        ];

        $columns = array_merge($defaultCol, $columns);

        return $columns;
    }

    /**
     * Get replacement selection data provider
     *
     * @param Text $replacementType Replacement type
     * @param Integer|Array $experimentId Experiment identifier
     * @param Integer|Array $entryIdToReplace Entry to be replaced identifier
     * @param Integer|Array $germplasmIdToReplace Germplasm to be replaced identifier
     * @param Integer|Array $packageIdToReplace Package to be replaced identifier
     * @return Array Entry replacement data provider
     */
    public function getReplacementSelectionData(
        $replacementType, 
        $experimentId,
        $entryIdToReplace,
        $germplasmIdToReplace,
        $packageIdToReplace
    ) {
        if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE') {
            $model = 'package';
            $sortParam = 'label';
            $entityId = 'packageDbId';

            $packageIdToReplace = !empty($packageIdToReplace) ? $packageIdToReplace : 0;
            // package parameters
            $params = [
                'germplasmDbId' => "equals " . ( gettype($germplasmIdToReplace) == 'array' ? implode('|equals ', $germplasmIdToReplace) : $germplasmIdToReplace ),
                'packageDbId' => 'not equals '. ( gettype($packageIdToReplace) == 'array' ? implode('|not equals ', $packageIdToReplace) : $packageIdToReplace ),
                'seedManager' => 'equals ' . Yii::$app->userprogram->get('abbrev'),
                'fields' => '
                    germplasm.id AS germplasmDbId |
                    seed.id AS seedDbId |
                    program.program_code AS seedManager |
                    package.id AS packageDbId |
                    package.package_label AS label |
                    package.package_quantity AS quantity |
                    package.package_unit AS unit |
                    package.package_reserved as reserved |
                    seed.seed_name AS seedName
                '
            ];

            $sort = [
                'defaultOrder' => ['label'=>SORT_ASC],
                'attributes' => ['packageDbId','label','quantity','unit']
            
            ];

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER') {
            // get program of experiment
            $experimentParams['experimentDbId'] = 'equals ' . ( gettype($experimentId) == 'array' ? implode('|equals ', $experimentId) : $experimentId );
            $experiment = $this->experiment->searchAll($experimentParams, 'limit=1', false);
            $programId = isset($experiment['data'][0]['programDbId']) ? $experiment['data'][0]['programDbId'] : 0;

            // get crop program
            $program = $this->program->getProgram($programId);
            $cropProgramId = $program["cropProgramDbId"] ?? null;

            // get filler germplasm ID in config
            $fillerConfig = $this->configuration->getConfigByAbbrev("DEFAULT_FILLER_GERMPLASM");
            $fillerGermplasmId = (!empty($cropProgramId) && isset($fillerConfig['' . $cropProgramId]["germplasm_id"])) 
                ? $fillerConfig['' . $cropProgramId]["germplasm_id"] : 0;

            $model = 'package';
            $sortParam = 'label';
            $entityId = 'packageDbId';

            $params = [
                'germplasmDbId' => "equals $fillerGermplasmId",
                'packageDbId' => 'not equals '. ( gettype($packageIdToReplace) == 'array' ? implode('|not equals ', $packageIdToReplace) : $packageIdToReplace ),
                'seedManager' => 'equals ' . Yii::$app->userprogram->get('abbrev'),
                'fields' => '
                    germplasm.id AS germplasmDbId |
                    seed.id AS seedDbId |
                    program.program_code AS seedManager |
                    package.id AS packageDbId |
                    package.package_label AS label |
                    package.package_quantity AS quantity |
                    package.package_unit AS unit |
                    package.package_reserved as reserved |
                    germplasm.designation AS designation |
                    seed.seed_name AS seedName 
                '
            ];

            $sort = [
                'defaultOrder' => ['packageDbId'=>SORT_ASC],
                'attributes' => ['packageDbId','label','packageQuantity','packageUnit','designation','seedName']
            
            ];

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK') {
            $model = 'entry';
            $sortParam = 'entryNumber';
            $entityId = 'entryDbId';
            $params = [
                'experimentDbId' => "equals " . ( gettype($experimentId) == 'array' ? implode('|equals ', $experimentId) : $experimentId ),
                'entryType' => 'equals check',
                'entryDbId' => 'not equals ' . ( gettype($entryIdToReplace) == 'array' ? implode('|not equals ', $entryIdToReplace) : $entryIdToReplace )
            ];
            $sort = [
                'defaultOrder' => ['entryNumber'=>SORT_ASC],
                'attributes' => ['entryDbId','entryNumber','packageLabel','packageQuantity','packageUnit']
            
            ];
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $model = 'germplasm';
            $sortParam = 'germplasmDbId';
            $entityId = 'germplasmDbId';

            $params = [
                'germplasmDbId' => "not equals " . ( gettype($germplasmIdToReplace) == 'array' ? implode('|not equals ', $germplasmIdToReplace) : $germplasmIdToReplace ),
            ];

            $sort = [
                'defaultOrder' => ['germplasmDbId' => SORT_ASC],
                'attributes' => ['germplasmDbId','name','germplasmCode','parentage','seedName','seedCode', 'label', 'packageCode', 'packageQuantity', 'packageUnit',],
            ];

        }

        if($model == 'package') {
            // use POST seed-packages-search
            $data = $this->seed->searchPackageRecords($params, 'all', $sortParam);
        } else {
            $data = $this->$model->searchAll($params, 'sort='.$sortParam, true)['data'];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => $entityId,
            'pagination'=>false,
            'sort' => $sort,
        ]);

        // get total count of records
        $totalCount = $dataProvider->getTotalCount();

        return [
            'dataProvider' => $dataProvider,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Get replacement entity by replacement type
     *
     * @param Text $type Replacement type
     * @return Text Replacement entity
     */
    public function getReplacementEntity($type){
        if($type == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'){
            return 'package';
        }

        if($type == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK'){
            return 'check';
        }

        if($type == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER'){
            return 'filler package';
        }
    }

    /**
     * Render preview of replacement
     */
    public function actionRenderReplacementPreview(){
        // replacement values
        $replacementType = $_POST['replacementType'];
        $replacementTypeText  = $_POST['replacementTypeText'];
        $replacementId  = $_POST['replacementId'];
        $plantingJobEntryId = $_POST['plantingJobEntryId'];

        $originalEntryData = null;
        $isBulkReplacement = $_POST['isBulkReplacement'] ?? false;
        $plantingJobEntriesDataProvider = null;
        $germplasmName = '';
        $parentage = '';

        if (gettype($isBulkReplacement) != 'boolean') {
            $isBulkReplacement = filter_var($isBulkReplacement, FILTER_VALIDATE_BOOLEAN);
        }

        if (!$isBulkReplacement) { // get summary of original entry from session
            $originalEntryData = Yii::$app->session->get('original-entry-data');
        } else { // get selected planting job entries
            $plantingJobEntriesData = $this->api->getApiResults(
                'POST',
                'planting-job-entries-search',
                json_encode([
                    'plantingJobEntryDbId' => 'equals ' . implode('|equals ', $plantingJobEntryId)
                ]),
                '',
                true
            );

            $germplasmName = $plantingJobEntriesData['data'][0]['actualGermplasmName'];
            $parentage = $plantingJobEntriesData['data'][0]['actualParentage'];

            $plantingJobEntriesDataProvider = new ArrayDataProvider([
                'allModels' => $plantingJobEntriesData['data'],
                'key' => 'plantingJobEntryDbId',
                'pagination' => false,
            ]);
        }

        $viewFile = '@app/modules/plantingInstruction/views/create/_replace_entry_form.php';

        // replacement data
        if($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'){
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'package.id AS packageDbId | package.package_quantity AS quantity | package.package_unit AS unit| package.package_label AS label | seed.seed_name AS seedName';

        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK'){
            $entityDbId = 'entryDbId';
            $modelClass = 'entry';

            $params['fields'] = 'entry.id AS entryDbId | package.package_quantity AS quantity | package.id AS packageDbId | package.package_unit AS unit | package.package_label AS label | germplasm.id AS germplasmDbId | germplasm.designation AS designation | germplasm.parentage AS parentage | seed.seed_name AS seedName';
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER'){
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'package.id AS packageDbId | package.package_quantity AS quantity | package.package_unit AS unit| package.package_label AS label | seed.seed_name AS seedName | germplasm.designation AS designation | 
                germplasm.parentage AS parentage | germplasm.id AS germplasmDbId | program.program_code AS seedManager';
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM'){
            $entityDbId = 'packageDbId';
            $modelClass = 'package';

            $params['fields'] = 'germplasm.id AS germplasmDbId |
                germplasm.designation AS designation |
                germplasm.parentage AS parentage |
                seed.seed_name AS seedName |
                package.id AS packageDbId |
                package.package_quantity AS quantity |
                package.package_unit AS unit |
                package.package_label AS label
            ';
        }

        $params[$entityDbId] = "equals $replacementId";

        $entity = $this->$modelClass->searchAll($params, 'limit=1', false);

        $data = isset($entity['data'][0]) ? $entity['data'][0] : '';

        $data['quantity'] = number_format($data['quantity']) . ' ' . $data['unit'];
   
        if($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE'){
            
            $replacementData = [
                'replacementPackageDbId' => (string) $data['packageDbId'],
                'replacementGermplasmDbId' => null,
            ];
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_CHECK'){
            
            $replacementData = [
                'replacementGermplasmDbId' => (string) $data['germplasmDbId'],
                'replacementEntryDbId' => (string) $data['entryDbId'],
            ];

            if(isset($data['packageDbId'])){
                $replacementData['replacementPackageDbId'] = (string) $data['packageDbId'];
            }
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_FILLER'){
            $replacementData = [
                'replacementGermplasmDbId' => (string) $data['germplasmDbId'],
            ];

            if(isset($data['packageDbId'])){
                $replacementData['replacementPackageDbId'] = (string) $data['packageDbId'];
            }
        } else if ($replacementType == 'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM') {
            $replacementData = [
                'replacementGermplasmDbId' => "{$data['germplasmDbId']}",
                'replacementPackageDbId' => "{$data['packageDbId']}",
            ];
        }

        $replacementData['replacementType'] = $replacementTypeText;
        $replacementData['isReplaced'] = 'true';

        Yii::$app->session->set('replacement-data-' .
            ((gettype($plantingJobEntryId) == 'array') ? implode('_', $plantingJobEntryId) : $plantingJobEntryId), $replacementData);

        $htmlData = $this->renderAjax($viewFile,[
            'data' => $originalEntryData,
            'replacementType' => $replacementTypeText,
            'replacementData' => $data,
            'isPreview' => true,
            'isBulkReplacement' => $isBulkReplacement,
            'plantingJobEntriesDataProvider' => $plantingJobEntriesDataProvider,
            'germplasmName' => $germplasmName,
            'parentage' => $parentage,
        ]);

        return json_encode($htmlData);
    }

    /**
     * Confirm entry replacement
     *
     * @return Boolean $success Whether successfully updated or not 
     */
    public function actionConfirmReplacement () {
        $id = $_POST['plantingJobEntryId'];
        $plantingJobId = $_POST['plantingJobId'];
        $isBulkReplacement = $_POST['isBulkReplacement'] ?? false;
        $results = [
            'true' => 0,
            'false' => 0,
            'total' => 0,
        ];

        // convert variable to bool type
        if (gettype($isBulkReplacement) != 'boolean') {
            $isBulkReplacement = filter_var($isBulkReplacement, FILTER_VALIDATE_BOOLEAN);
        }


        $replacementData = Yii::$app->session->get('replacement-data-' .
            (($isBulkReplacement) ? implode('_', $id) : $id));

        // update planting job entry/entries
        // count successful ("true") and failed ("false") replacements
        if ($isBulkReplacement) {
            foreach ($id as $i) {
                // Get this planting job entry's info
                $response = $this->plantingJobEntry->searchAll([ 'plantingJobEntryDbId' => "equals $i", ], '?limit=1', false);
                $packageLogDbId = $response['data'][0]['packageLogDbId'];
                $actualPackageDbId = $response['data'][0]['actualPackageDbId'];

                // Set planting job entry's package_log_id column to NULL
                $this->api->getApiResults(
                    'PUT',
                    "planting-job-entries/$i",
                    json_encode([
                       'packageLogDbId' => null,
                    ])
                );
                
                // Void current package log
                $this->api->getApiResults(
                    'DELETE',
                    "package-logs/$packageLogDbId"
                );

                // Recalculate package_reserved of actual package
                $this->api->getApiResults(
                    'POST',
                    "packages/$actualPackageDbId/package-reserved-generations"
                );

                $response = $this->plantingJobEntry->updateOne($i, $replacementData);
                $success = $response['success'] ? 'true' : 'false';
                $results[$success] += 1;
            }
        } else {
            // Get this planting job entry's info
            $response = $this->plantingJobEntry->searchAll([ 'plantingJobEntryDbId' => "equals $id", ], '?limit=1', false);
            $packageLogDbId = $response['data'][0]['packageLogDbId'];
            $actualPackageDbId = $response['data'][0]['actualPackageDbId'];

            // Set planting job entry's package_log_id column to NULL
            $this->api->getApiResults(
                'PUT',
                "planting-job-entries/$id",
                json_encode([
                   'packageLogDbId' => null,
                ])
            );
            
            // Void current package log
            $this->api->getApiResults(
                'DELETE',
                "package-logs/$packageLogDbId"
            );

            // Recalculate package_reserved of actual package
            $this->api->getApiResults(
                'POST',
                "packages/$actualPackageDbId/package-reserved-generations"
            );

            $response = $this->plantingJobEntry->updateOne($id, $replacementData);
            $success = $response['success'] ? 'true' : 'false';
            $results[$success] += 1;
        }


        // if there is at least 1 successful replacement...
        if ($results['true']) {
            // ...update planting job status and isSubmitted to draft and false, respectively
            $plantingJobParam = [
                'isSubmitted' => 'false',
                'plantingJobStatus' => 'draft'
            ];

            $this->plantingJob->updateOne($plantingJobId, $plantingJobParam);
        }

        $results['total'] = $results['true'] + $results['false'];

        return json_encode($results);
    }

    /**
     * Get germplasm replacement selection data provider
     *
     * @param string $attribute attribute identifier retrieved via $_POST[...]
     * @param string $searchValue specified data value retrieved via $_POST[...]
     * @param string $packageDbId unique package identifier retrieved via $_POST[...]
     * 
     * @return JSON contains HTML data and data count
     */
    public function actionGetGermplasmSelectionData ()
    {
        $attribute = $_POST['attribute'] ?? null;
        $searchValue = $_POST['searchValue'] ?? null;
        $packageDbId = $_POST['packageDbId'] ?? null;
        $packageDbId = json_decode($packageDbId);

        // If User searches for germplasm records using germplasm name/designation...
        // ...change "name" to "names" and use non-strict searching
        if ($attribute == 'name') {
            $attribute = 'names';
            $searchValue = "equals $searchValue";
        } else {
            $searchValue = "equals $searchValue";
        }

        $data = $this->seed->searchPackageRecords([
            'packageDbId' => "not equals " . ( gettype($packageDbId) == 'array' ? implode('|not equals ', $packageDbId) : $packageDbId ),
            "$attribute" => "$searchValue",
            'seedManager' => Yii::$app->userprogram->get('abbrev'), // filter by current user program
            'fields' => 'germplasm.id AS germplasmDbId |
                germplasm.designation AS name |
                germplasm.germplasm_code AS germplasmCode |
                germplasm.parentage AS parentage |
                seed.id AS seedDbId |
                seed.seed_name AS seedName |
                seed.seed_code AS seedCode |
                program.program_code AS seedManager |
                package.id AS packageDbId |
                package.package_label AS label |
                package.package_code AS packageCode |
                package.package_quantity AS packageQuantity |
                package.package_unit AS packageUnit |
                package.package_reserved AS packageReserved
            ',
        ], 'all');

        $sort = [
            'defaultOrder' => [ 'germplasmDbId' => SORT_ASC ],
            'attributes' => ['germplasmDbId','name','germplasmCode','parentage','seedName','seedCode', 'label', 'packageCode', 'packageQuantity', 'packageUnit', 'packageReserved'],
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'germplasmDbId',
            'pagination' => false,
            'sort' => $sort,
        ]);

        $totalCount = $dataProvider->getTotalCount();

        $columns = [
            [
                'header' => false,
                'hAlign' => 'center',
                'value' => function ($data) {
                    return '
                    <input id="'.$data['packageDbId'].'" class="with-gap" name="selection-radio-button" value="'.$data['packageDbId'].'" type="radio" />
                    <label for="'.$data['packageDbId'].'" style="padding-left: 18px !important; margin-left: -5px !important;"></label>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('app', 'Germplasm name'),
                'enableSorting' => false,
                'value' => function ($data) {
                    return $data['name'];
                },
            ],
            [
                'attribute' => 'parentage',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return "<div
                        title='{$data['parentage']}'
                        class='truncated'
                    > {$data['parentage']} </div>";
                },
            ],
            [
                'attribute' => 'seedName',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'label',
                'enableSorting' => false,
                'label' => 'Package label',
                'value' => function ($data) {
                    return $data['label'];
                },
            ],
            [
                'attribute' => 'packageQuantity',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return (isset($data['packageQuantity'])) ? number_format($data['packageQuantity']) . ' ' . $data['packageUnit'] : '<span class="not-set">(not set)</span>';
                },
            ],
            [
                'attribute' => 'packageReserved',
                'enableSorting' => false,
            ],
        ];

        return json_encode([
            'htmlData' => $this->renderAjax('_replace_entry_germplasm_selection_grid', compact(
                'dataProvider',
                'totalCount',
                'columns',
            )),
            'count' => $totalCount,
        ]);
    }
}
