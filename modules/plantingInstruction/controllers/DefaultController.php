<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\plantingInstruction\controllers;

use app\components\B4RController;
use app\models\BackgroundJob;
use app\models\Occurrence;
use app\models\PlantingJob;
use app\models\PlantingJobOccurrence;
use app\models\User;
use app\models\Variable;
use app\modules\plantingInstruction\models\PlantingJobModel;

use Yii;

/**
 * Default controller for the `plantingInstruction` module
 */
class DefaultController extends B4RController
{

    public function __construct ($id, $module,
        protected BackgroundJob $backgroundJob,
        protected Occurrence $occurrence,
        protected PlantingJob $plantingJob,
        protected PlantingJobModel $plantingJobModel,
        protected PlantingJobOccurrence $plantingJobOccurrence,
        protected User $user,
        protected Variable $variable,
        $config=[]
    )
    { parent::__construct($id, $module, $config); }

    /**
     * Renders the browser for the occurrences
     *
     * @param $program text current program of the user
     * @return mixed browser for planting instructions manager
     */
    public function actionIndex($program)
    {
        $searchModel = $this->plantingJob;

        //check queryParams value, if empty, set to empty
        $params = Yii::$app->request->queryParams ? Yii::$app->request->queryParams : [];
        $data = $searchModel->search($params);
        $statusOptions = $this->plantingJob->getPlantingJobTags('planting_job_status');
        $typeOptions = $this->plantingJob->getPlantingJobTags('planting_job_type');
        
        //check $data value, if empty, set to empty
        $dataValue = empty($data) || !isset($data) ? [] : $data[0];

        return $this->render('index',[
            'program' => $program,
            'dataProvider' => $dataValue,
            'searchModel' => $searchModel,
            'statusOptions' => $statusOptions,
            'typeOptions' => $typeOptions
        ]);
    }

    /**
     * Create Planting job and
     * Planting Job Occurrence records
     */
    public function actionStoreOccurrenceIds(){

        $occurrenceIds = isset($_POST['occurrenceIds']) ? $_POST['occurrenceIds'] : []; 
        $program = isset($_POST['program']) ? $_POST['program'] : ''; 
        $programId = isset($_POST['programId']) ? $_POST['programId'] : ''; 

        // store occurrence IDs to session
        Yii::$app->session->set('em-pim-occurrence-ids', $occurrenceIds);

        $this->redirect(['/plantingInstruction/create/create-planting-job?program='.$program.'&programId='.$programId]);

    }

    /**
     * View Planting Job
     *
     * @param Integer $id Planting Job identifier
     * @return Mixed View planting job page
     */
    public function actionView(){
        $id = isset($_POST['id']) ? $_POST['id'] : null;

        // get packing job summary
        $summary = $this->plantingJob->summary($id);

        $processedSummary = [];
        $isSubmitted = false;

        // get processed summary
        $processedSummary = $this->plantingJob->getProcessedSummary($summary);

        // get additional instructions
        $params['plantingJobDbId'] = "equals $id";
        $plantingJobData = $this->plantingJob->searchAll($params, 'limit=1', false);

        $addtlInstructions = [];
        $statusClass = 'grey';
        
        if(isset($plantingJobData['data'][0])){
            $plantingJobData = $plantingJobData['data'][0];

            // get additional instructions
            $addtlInstructions = $this->plantingJob->getProcessedAdditionalData($plantingJobData);

            $status = $plantingJobData['plantingJobStatus'];
            $isSubmitted = $plantingJobData['isSubmitted'];
            $statusClass = $this->plantingJob->getStatusClass($status);

        }

        // get planting job variable
        $statusVariableId = $this->variable->getAttributeByAbbrev('PLANTING_JOB_STATUS', 'variableDbId');

        // get planting job statuses
        $statusValues = $this->variable->searchAllVariableScales($statusVariableId, null, 'sort=orderNumber');
        $statuses = [];
        if(isset($statusValues['data'][0]['scaleValues']) && !empty($statusValues['data'][0]['scaleValues'])){
            $statuses = $statusValues['data'][0]['scaleValues'];
        }

        return Yii::$app->controller->renderAjax('view.php',[
            'id' => $id,
            'plantingJobCode' => $plantingJobData['plantingJobCode'],
            'plantingJobStatus' => $plantingJobData['plantingJobStatus'],
            'isSubmitted' => $plantingJobData['isSubmitted'],
            'summary' => $processedSummary,
            'addtlInstructions' => $addtlInstructions,
            'status' => $status,
            'statusClass' => $statusClass,
            'statuses' => $statuses,
            'isSubmitted' => $isSubmitted
        ]);
    }

    /**
     * Delete Planting job
     *
     * @return Array Success and label
     */
    public function actionDelete(){
        $id = $_POST['plantingJobId'];
        $label = $_POST['plantingJobLabel'];

        $params = [
            'plantingJobDbId' => "equals $id"
        ];

        $plantingJobOccs = $this->plantingJobOccurrence->searchAll($params, '', true);

        // loop through each planting job occurrences
        if(isset($plantingJobOccs['data']) && !empty($plantingJobOccs['data'])){
            foreach ($plantingJobOccs['data'] as $key => $value) {
                
                // remove packing job status in Occurrence
                $occurrenceId = $value['occurrenceDbId'];
                // get occurrence status
                $occParams['occurrenceDbId'] = "equals $occurrenceId";
                $occurrenceObj = $this->occurrence->searchAll($occParams, 'limit=1', false);
                $occurrenceStatus = isset($occurrenceObj['data'][0]['occurrenceStatus']) ? $occurrenceObj['data'][0]['occurrenceStatus'] : '';

                // remove packing job status in occurrence status
                $statusArr = explode(';', $occurrenceStatus);
                $occStatusArr = [];
                
                foreach ($statusArr as $k => $v) {
                    // exclude packing job status
                    if(strpos($v, 'pack') === false){
                        $occStatusArr[] = $v;
                    }
                }

                $occStatus = implode(';', $occStatusArr);
                $requestData['occurrenceStatus'] = $occStatus;

                // upate occurrence status
                $this->occurrence->updateOne($occurrenceId, $requestData);

                // delete planting job occurrence
                $this->plantingJobOccurrence->deleteOne($value['plantingJobOccurrenceDbId']);

            }
        }

        // delete planting job
        $transaction = $this->plantingJob->deleteOne($id);

        return json_encode([
            'success' => ($transaction['success']),
            'label' => $label,
        ]);

    }

    /**
     * Returns new notifications from background process
     */
    public function actionNewNotificationsCount(){
        $userId = $this->user->getUserId();

        $params = [
            'creatorDbId' => "equals $userId",
            'isSeen' => 'false',
            'jobStatus' => 'equals DONE|equals FAILED',
            'application' => 'equals PLANTING_INSTRUCTIONS_MANAGER'
        ];

        $sortParams = 'sort=modificationTimestamp:desc';
        $result = $this->backgroundJob->searchAll($params, $sortParams);

        // if there were problems retrieving 
        if($result['status'] != 200){
            return json_encode([
                'success' => false,
                'error' => 'Error while retrieving background processes. Kindly contact Administrator.'
            ]);
        } else {
            // update the planting job status
           $this->updatePlantingJobStatus($userId);
        }

        return json_encode($result['totalCount']);
    }

    /**
     * Push notifications from background process
     */
    public function actionPushNotifications(){
        $userId = $this->user->getUserId();
        $notifs = $this->plantingJobModel->pushNotifications($userId);

        return $this->renderAjax(
            'notifications',
            $notifs
        );
    }

    /**
     * Update Planting job status based from bacgroud process status
     */
    public function updatePlantingJobStatus($userId){

        $this->plantingJobModel->updatePlantingJobStatus($userId);
    }
}
