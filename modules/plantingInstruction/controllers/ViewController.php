<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\plantingInstruction\controllers;

use app\components\B4RController;
use app\models\PlantingJob;
use app\models\Variable;
use app\modules\plantingInstruction\models\PlantingJobModel;
use Yii;

/**
 * View controller for the `plantingInstruction` module
 */
class ViewController extends B4RController
{

    public function __construct ($id, $module,
        protected PlantingJob $plantingJob,
        protected PlantingJobModel $plantingJobModel,
        protected Variable $variable,
        $config=[]
    )
    { parent::__construct($id, $module, $config); }

    /**
     * Renders the packing job summary and
     * data browser for the occurrence summaries
     *
     * NOTE: As of v23.01, "packing job" and "planting job" are interchangeable
     *
     * @param int|string $id unique job identifier
     * @param string $program current program of the user
     *
     * @return string rendering string
     */
    public function actionPackingJob($id, $program)
    {
        
        if (Yii::$app->access->renderAccess("PIM_VIEW", "PLANTING_INSTRUCTIONS_MANAGER", usage: 'url')) {

            Yii::debug('Preparing View Packing Job page ' . json_encode([ 'id' => $id, 'program' => $program,]));

            //check queryParams value, if empty, set to empty
            $params = Yii::$app->request->queryParams ? Yii::$app->request->queryParams : [];

            // Get browser data values
            $packingJobInfo = $this->plantingJobModel->getPackingJobInfo($params);
            $occurrenceSummaries = $this->plantingJob->getOccurrenceSummaries($params);

            // get packing job summary
            $summary = $this->plantingJob->summary($id);

            // get processed summary
            $processedSummary = $this->plantingJob->getProcessedSummary($summary);

            // get planting job variable
            $statusVariableId = $this->variable->getAttributeByAbbrev('PLANTING_JOB_STATUS', 'variableDbId');

            // get planting job statuses
            $statusValues = $this->variable->searchAllVariableScales($statusVariableId, null, 'sort=orderNumber');
            $statuses = [];
            if(isset($statusValues['data'][0]['scaleValues']) && !empty($statusValues['data'][0]['scaleValues'])){
                $statuses = $statusValues['data'][0]['scaleValues'];
            }

            // check data value, if empty, set to empty
            $packingJobInfo = (empty($packingJobInfo) || !isset($packingJobInfo)) ? [] : $packingJobInfo;
            $occurrenceSummaries = (empty($occurrenceSummaries) || !isset($occurrenceSummaries)) ? [] : $occurrenceSummaries;

            $plantingJobStatus = count($packingJobInfo->allModels) > 0 ? $packingJobInfo->allModels[0]['plantingJobStatus'] : null;

            // render view file
            return $this->render('packing_job', [
                'id' => $id,
                'program' => $program,
                'dataProviders' => [
                    'packingJobInfo' => $packingJobInfo,
                    'occurrenceSummaries' => $occurrenceSummaries,
                ],
                'plantingJobCode' => count($packingJobInfo->allModels) > 0 ? $packingJobInfo->allModels[0]['plantingJobCode'] : null,
                'isSubmitted' => count($packingJobInfo->allModels) > 0 ? $packingJobInfo->allModels[0]['isSubmitted'] : null,
                'plantingJobStatus' => $plantingJobStatus,
                'statuses' => $statuses,
                'statusClass' => $this->plantingJob->getStatusClass($plantingJobStatus),
                'processedSummary' => $processedSummary,
            ]);
        }
    }
}
