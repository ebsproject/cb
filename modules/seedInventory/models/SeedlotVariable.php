<?php 

/* 
 * This file is part of EBS-Core Breeding. 
 * 
 * EBS-Core Breeding is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * EBS-Core Breeding is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\Variable;
use app\models\User;

/**
* Class for all seed inventory configuration management related methods
**/

class SeedlotVariable extends \app\models\Variable{
    public $creator_name;
    public $modifier_name;
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['id', 'property_id', 'method_id', 'scale_id', 'creator_id', 'modifier_id', 'target_variable_id', 'member_variable_id', 'class_variable_id'], 'integer'],
            [['abbrev', 'label', 'name', 'data_type', 'type', 'status', 'display_name', 'ontology_reference', 'bibliographical_reference', 'variable_set', 'synonym', 'remarks', 'creation_timestamp', 'modification_timestamp', 'notes', 'description', 'default_value', 'usage', 'data_level', 'column_table', 'member_data_type', 'field_prop', 'target_model', 'json_type', 'notif', 'modifier_name', 'creator_name'], 'safe'],
            [['not_null', 'is_void', 'is_column', 'is_computed'], 'boolean'],
        ];
    }

    /**
    * Return data provider 
    * @param String $selectSql columns defined
    * @param String $rawSql the raw sql to be converted into a data provider
    * @param string $condition Sql condition
    * @return SqlDataProvider $dataProvider data object for browser
    **/
    public static function getDataProvider($rawSql, $selectSql, $condition) {

        $sql = "
            select * from (
                select
                    {$selectSql}
                    {$rawSql}
            )t
            where is_void=false and data_level ilike '%seed%' 
            {$condition}
        ";

        $countSql = "
        select count(1) from (
                select
                    {$selectSql}
                    {$rawSql}
            )t
        where is_void=false and data_level ilike '%seed%' 
        {$condition}
        ";

        $totalCount = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'key'=>'id',
            'sort' => [
                'attributes' => [
                    'creation_timestamp',
                    'modification_timestamp',
                    'abbrev',
                    'label',
                    'name',
                    'data_type',
                    'type',
                    'display_name',
                    'usage',
                    'status',
                    'remarks',
                    'description',
                    'data_level',
                    'not_null',
                    'creator_name',
                    'modifier_name'
                ],
                'defaultOrder' => [
                    'creation_timestamp' => SORT_DESC,
                    'modification_timestamp' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Retrieve list of variables
     * @return SqlDataProvider Data Provider
     */
    public static function getSql($condition){
        $selectSql= "
            id,
            abbrev,
            label,
            name,
            data_type,
            type,
            display_name,
            usage,
            status,
            (select display_name from master.user where id=v.creator_id) as creator_name,
            creation_timestamp,
            (select display_name from master.user where id=v.modifier_id) as modifier_name,
            modification_timestamp,
            remarks,
            description,
            is_void,
            data_level,
            not_null
            
        ";
        $sql="
            FROM 
            master.variable v
        ";

        return SeedlotVariable::getDataProvider($sql, $selectSql, $condition);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains the different filter optiosn
     * @return ActiveDataProvider
     */
    public function search($params){
        
        $this->load($params);
        $query = [];
        $queryCondition= '';
        if(isset($this->status) && !empty($this->status)){
            
            $query[]= " status = '".$this->status."'";
        }
        if(isset($this->abbrev) && !empty($this->abbrev)){
            
            $query[]= " abbrev ilike '%".$this->abbrev."%'";
        }
        if(isset($this->label) && !empty($this->label)){
            
            $query[]= " label ilike '%".$this->label."%'";
        }
        if(isset($this->name) && !empty($this->name)){
            
            $query[]= " name ilike '%".$this->name."%'";
        }
        if(isset($this->data_type) && !empty($this->data_type)){
            
            $query[]= " data_type ilike '%".$this->data_type."%'";
        }
        if(isset($this->type) && !empty($this->type)){
            
            $query[]= " type ilike '%".$this->type."%'";
        }
        if(isset($this->display_name) && !empty($this->display_name)){
            
            $query[]= " display_name ilike '%".$this->display_name."%'";
        }
        if(isset($this->usage) && !empty($this->usage)){
            
            $query[]= " usage ilike '%".$this->usage."%'";
        }
        if(isset($this->status) && !empty($this->status)){
            
            $query[]= " status ilike '%".$this->status."%'";
        }

        if(isset($this->creator_name) && !empty($this->creator_name)){
            
            $query[]= " creator_name ilike '%".$this->creator_name."%'";
        }
        if(isset($this->modifier_name) && !empty($this->modifier_name)){
            
            $query[]= " modifier_name ilike '%".$this->modifier_name."%'";
        }
        if(isset($this->remarks) && !empty($this->remarks)){
            
            $query[]= " remarks ilike '%".$this->remarks."%'";
        }
        if(isset($this->description) && !empty($this->description)){
            
            $query[]= " description ilike '%".$this->description."%'";
        }
        if(isset($this->not_null) && !empty($this->not_null)){
            $query[]= " not_null = ".$this->not_null."";
        }
        if(isset($this->modification_timestamp) && $this->modification_timestamp!=''){
            $dayBegin = $this->modification_timestamp.' 00:00:00.000000';
            $dayEnd = $this->modification_timestamp.' 23:59:59.999999';
            $query[]=" modification_timestamp between '{$dayBegin}' and '{$dayEnd}'";
        }

        if(isset($this->creation_timestamp) && $this->creation_timestamp!=''){
            $dayBegin = $this->creation_timestamp.' 00:00:00.000000';
            $dayEnd = $this->creation_timestamp.' 23:59:59.999999';
            $query[]=" creation_timestamp between '{$dayBegin}' and '{$dayEnd}'";
        }

        $queryCondition = implode($query,' and ');
        if(!empty($queryCondition)){
            $queryCondition = ' and ('.$queryCondition.")";
        }

        return $this->getSql($queryCondition);
    }

}
?>