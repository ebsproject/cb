<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\Variable;
use app\models\User;
use app\models\Program;
use app\models\Facility;
use app\models\ScaleValue;
use ChromePhp;
use Monolog\Formatter\ChromePHPFormatter;

class ProgramFacilityModel extends  \yii\db\ActiveRecord
{


    public $program;
    public $facility;
    public $subfacility;
    public $subfacility_ids;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['program_id', 'facility_id', 'creator_id'], 'required'],
            [['program_id', 'facility_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['program_id', 'facility_id', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log', 'program', 'facility', 'subfacility', 'subfacility_id'], 'safe'],
            [['is_void'], 'boolean'],
            [['program_id', 'facility_id'], 'unique', 'targetAttribute' => ['program_id', 'facility_id']],
            [['facility_id'], 'exist', 'skipOnError' => true, 'targetClass' => Facility::className(), 'targetAttribute' => ['facility_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * Save record
     * @param  array $params program package type fields
     * @return array response
     */

    public static function saveRecord($params)
    {
        extract($params);
        $userModel = new User();
        $userId = $userModel->getUserId();

        foreach ($facility_list as $val) {

            $sqlCheck = "Select count(1) from master.program_facility where program_id={$program_id} and facility_id={$val}";
            $isExist = Yii::$app->db->createCommand($sqlCheck)->queryScalar();

            if (!$isExist) {

                $sql = "INSERT INTO master.program_facility(program_id, facility_id, creation_timestamp, creator_id) values (
                {$program_id},
                {$val}, 
                'NOW()', 
                {$userId})";

                Yii::$app->db->createCommand($sql)->execute();
            }
        }

        return [
            "success" => true,
            "message" => "Successfully created record."
        ];
    }


    /**
     * Update record
     * @param  array $params program package type fields
     * @return array response
     */

    public static function updateRecord($params)
    {
        extract($params);
        $userModel = new User();
        $userId = $userModel->getUserId();

        $SelectedIds = $old_facility_id;
        $SelectedIds = array_map(function ($elem) {
            return intval($elem);
        }, $SelectedIds);


        foreach ($SelectedIds as $val) {

            $sql = "Delete from master.program_facility where program_id={$program_id} and facility_id={$val}";
            Yii::$app->db->createCommand($sql)->execute();
        }


        foreach ($facility_id as $val) {

            $sqlCheck = "Select count(1) from master.program_facility where program_id={$program_id} and facility_id={$val}";
            $isExist = Yii::$app->db->createCommand($sqlCheck)->queryScalar();

            if (!$isExist) {

                $sql = "INSERT INTO master.program_facility(program_id, facility_id, creation_timestamp, creator_id) values (
                {$program_id},
                {$val}, 
                'NOW()', 
                {$userId})";

                Yii::$app->db->createCommand($sql)->execute();
            }
        }

        return [
            "success" => true,
            "message" => "Successfully created record."
        ];
    }




    /**
     * Delete unit conversion
     * @param  array $params program package type id
     * @return array program_facility record
     */

    public static function deleteRecord($params)
    {
        $ids = explode(":", $params['id']);

        foreach ($ids as $id) {
            $p = explode(";", $id);
            $sql = "Delete from master.program_facility where program_id={$p[0]} and facility_id in ({$p[1]})";
            Yii::$app->db->createCommand($sql)->execute();
        }
        return [
            "success" => true,
            "message" => "Successfully deleted record."
        ];
    }

    /**
     * Get unit conversion 
     * @param  array $params program package type id
     * @return array scale_conversion record
     */

    public static function getList($params)
    {

        extract($params);

        $sql = "
   select
        fc.program_id as id,
        (select abbrev from master.program where id = fc.program_id) as program,
        parent.name as facility,
        array_to_string(array_agg(child.name), ' , ') as subfacility,
        array_to_string(array_agg(child.id), ' , ') as subfacility_ids

    from
        master.program_facility fc,
        master.facility child,
        master.facility parent
    where
        fc.facility_id = child.id
        and fc.is_void = false
        and child.parent_id = parent.id and 
        fc.facility_id in ({$id})
    group by child.parent_id, fc.program_id, parent.name ";


        $programFacilityList = Yii::$app->db->createCommand($sql)->queryOne();
        $subfacility = explode(",", $programFacilityList['subfacility_ids']);


        $sqlParentFacility = "select parent_id from master.facility where id={$subfacility[0]}";
        $facilityId = Yii::$app->db->createCommand($sqlParentFacility)->queryScalar();

        $sqlProgramId = "select program_id from master.program_facility where facility_id={$subfacility[0]} limit 1";
        $program_id = Yii::$app->db->createCommand($sqlProgramId)->queryScalar();



        return [
            "program_id" => $program_id,
            "facility_id" => $facilityId,
            "subfacility_list" => $subfacility
        ];
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains the different filter optiosn
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $this->load($params);
        $query = [];
        $queryCondition = '';

        if (isset($this->program) && !empty($this->program)) {
            $query[] = " id =" . $this->program;
        }

        if (isset($this->facility) && !empty($this->facility)) {
            $query[] = " facility ilike '%" . $this->facility . "%'";
        }

        if (isset($this->subfacility) && !empty($this->subfacility)) {
            $query[] = " subfacility ilike '%" . $this->subfacility . "%'";
        }

        $queryCondition = implode($query, ' and ');
        if (!empty($queryCondition)) {
            $queryCondition = ' (' . $queryCondition . ")";
        }

        return ProgramFacilityModel::getProgramFacilityDataBrowser($queryCondition);
    }

    /**
     * Retrieve list of patterns
     * @return SqlDataProvider Data Provider
     */
    public static function getProgramFacilityDataBrowser($condition)
    {

        return ProgramFacilityModel::getDataProvider(null, null, $condition);
    }


    /**
     * Return data provider 
     * @param String $selectSql columns defined
     * @param String $rawSql the raw sql to be converted into a data provider
     * @param string $condition Sql condition
     * @return SqlDataProvider $dataProvider data object for browser
     **/
    public static function getDataProvider($rawSql, $selectSql, $condition)
    {

        if ($condition != "") {
            $condition = ' where ' . $condition;
        }

        $sql = "
        Select * from (select
	        fc.program_id as id,
	        (select abbrev from master.program where id = fc.program_id) as program,
	        parent.name as facility,
            array_to_string(array_agg(child.name), ' , ') as subfacility,
            array_to_string(array_agg(child.id), ' , ') as subfacility_ids

        from
	        master.program_facility fc,
	        master.facility child,
	        master.facility parent
        where
	        fc.facility_id = child.id
	        and fc.is_void = false
	        and child.parent_id = parent.id
        group by child.parent_id, fc.program_id, parent.name) as tbl {$condition}";


        $countSql = "select  count(distinct program_id) from master.program_facility";

        $totalCount = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'program',
                    'facility',
                    'subfacility'
                ],
                'defaultOrder' => [
                    'program' => 'SORT_ASC'
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Get the list of programs for select2 widget
     * @return array program list
     */
    public function getProgramList()
    {
        $varList = Yii::$app->db->createCommand("select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false")->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }

    public function getProgramListAll()
    {
        $varList = Yii::$app->db->createCommand("select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false")->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }

    /**
     * Get the list of programs for select2 widget
     * @return array program list
     */
    public function getProgramListWithRecord()
    {
        $sql = "select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false and id  IN (select program_id from master.program_facility group by program_id)";
        $varList = Yii::$app->db->createCommand($sql)->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }



    /**
     * Get the list of facility level 0
     * @return array program list
     */
    public function getFacilityListLevel0()
    {
        //$sql="select id, name,facility_type from master.facility where level =0 order by  name";
        $sql = "select distinct id, name,facility_type from master.facility as t1 where level =0 and (select id from master.facility where parent_id=t1.id limit 1) is not null order by  name";
        $facilityList = Yii::$app->db->createCommand($sql)->queryAll();
        $facilityArr = [];
        foreach ($facilityList as $p) {
            $facilityArr[$p['id']] = $p['name'];
        }
        return $facilityArr;
    }

    /**
     * Get the list of facility level 0
     * @return array program list
     */
    public function getSubFacility($params)
    {
        extract($params);

        $facilityList = Yii::$app->db->createCommand("select id, name,facility_type from master.facility where level =1 and  parent_id={$facility_id}  order by  name")->queryAll();
        $facilityArr = [];
        foreach ($facilityList as $p) {
            $facilityArr[$p['id']] = $p['name'];
        }
        return $facilityArr;
    }
}
