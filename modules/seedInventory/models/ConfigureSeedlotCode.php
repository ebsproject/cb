<?php 

/* 
 * This file is part of EBS-Core Breeding. 
 * 
 * EBS-Core Breeding is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * EBS-Core Breeding is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */ 

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\Variable;
use app\models\User;

/**
* Class for all seed inventory configuration management related methods
**/

class ConfigureSeedlotCode extends \yii\db\ActiveRecord{
    //columns
    public $pattern_name;
    public $program_abbrev;
    public $program_name;
    public $pattern_format;
    public $creator_name;
    public $modifier_name;
    public $remarks;
    public $is_active;
    public $creation_timestamp;
    public $modification_timestamp;
    public $description;

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            
            [['pattern_name','program_abbrev','program_name','pattern_format', 'creator_name', 'modifier_name', 'remarks', 'is_active', 'modification_timestamp', 'creation_timestamp', 'description'], 'safe'],
        ];
    }

    /**
    * Return data provider 
    * @param String $selectSql columns defined
    * @param String $rawSql the raw sql to be converted into a data provider
    * @param string $condition Sql condition
    * @return SqlDataProvider $dataProvider data object for browser
    **/
    public static function getDataProvider($rawSql, $selectSql, $condition) {

        $sql = "
            select * from (
                select
                    {$selectSql}
                    {$rawSql}
            )t 
            where is_void=false {$condition}
        ";

        $countSql = "
        select count(1) from (
            select 
                {$selectSql}
                {$rawSql}
        )t 
        where is_void=false {$condition}
        ";

        $totalCount = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'key'=>'row_id',
            'sort' => [
                'attributes' => [
                    'creation_timestamp',
                    'modification_timestamp',
                    'pattern_name',
                    'program_abbrev',
                    'program_name',
                    'pattern_format',
                    'creator_name',
                    'modifier_name',
                    'remarks',
                    'is_active',
                    'description'
                ],
                'defaultOrder' => [
                    'creation_timestamp' => SORT_DESC,
                    'modification_timestamp' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Retrieve list of patterns
     * @return SqlDataProvider Data Provider
     */
    public static function getPatternDataBrowser($condition){
        $selectSql= "
            (data::json->>'id')::text as id,
            (data::json->>'programAbbrev')::text as program_abbrev,
            (data::json->>'programName')::text as program_name,
            (data::json->>'patternName')::text as pattern_name,
            (data::json->>'patternFormat')::text as pattern_format,
            (select display_name from master.user where id=(data::json->>'creator_id')::int) as creator_name,
            (data::json->>'creation_timestamp') as creation_timestamp,
            (select display_name from master.user where id=(data::json->>'modifier_id')::int) as modifier_name,
            (data::json->>'modification_timestamp') as modification_timestamp,
            (data::json->>'remarks') as remarks,
            (data::json->>'is_active') as is_active,
            row_number() over() as row_id,
            (data::json->>'description')::text as description,
            (data::json->>'is_void')::boolean as is_void
            
        ";
        $sql="
            FROM 
            (select json_array_elements(value::json) as data from master.program_metadata where variable_id=(Select id from master.variable where abbrev='SEEDLOTCODE_PATTERN') 
            )t
        ";

        return ConfigureSeedlotCode::getDataProvider($sql, $selectSql, $condition);
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params contains the different filter optiosn
     * @return ActiveDataProvider
     */
    public function search($params){
        
        $this->load($params);
        $query = [];
        $queryCondition= '';
        if(isset($this->is_active) && !empty($this->is_active)){
            
            $query[]= " is_active = '".$this->is_active."'";
        }
        if(isset($this->pattern_name) && !empty($this->pattern_name)){
            
            $query[]= " pattern_name ilike '%".$this->pattern_name."%'";
        }
        if(isset($this->pattern_format) && !empty($this->pattern_format)){
            
            $query[]= " pattern_format ilike '%".$this->pattern_format."%'";
        }
        if(isset($this->program_name) && !empty($this->program_name)){
            
            $query[]= " program_name ilike '%".$this->program_name."%'";
        }
        if(isset($this->creator_name) && !empty($this->creator_name)){
            
            $query[]= " creator_name ilike '%".$this->creator_name."%'";
        }
        if(isset($this->modifier_name) && !empty($this->modifier_name)){
            
            $query[]= " modifier_name ilike '%".$this->modifier_name."%'";
        }
        if(isset($this->remarks) && !empty($this->remarks)){
            
            $query[]= " remarks ilike '%".$this->remarks."%'";
        }
        if(isset($this->description) && !empty($this->description)){
            
            $query[]= " description ilike '%".$this->description."%'";
        }
        if(isset($this->modification_timestamp) && $this->modification_timestamp!=''){
            $dayBegin = $this->modification_timestamp.' 00:00:00.000000';
            $dayEnd = $this->modification_timestamp.' 23:59:59.999999';
            $query[]=" modification_timestamp between '{$dayBegin}' and '{$dayEnd}'";
        }

        if(isset($this->creation_timestamp) && $this->creation_timestamp!=''){
            $dayBegin = $this->creation_timestamp.' 00:00:00.000000';
            $dayEnd = $this->creation_timestamp.' 23:59:59.999999';
            $query[]=" creation_timestamp between '{$dayBegin}' and '{$dayEnd}'";
        }

        $queryCondition = implode($query,' and ');
        if(!empty($queryCondition)){
            $queryCondition = ' and ('.$queryCondition.")";
        }

        return $this->getPatternDataBrowser($queryCondition);
    }

    /**
     * Get the list of identifier variables based on data level
     * @param  string $dataLevel plot,seed,study
     * @return array list of variables
     */
    public function getVarList($dataLevel){
        $varList = Yii::$app->db->createCommand("select abbrev,label as text from master.variable where is_void=false and type='identification' and data_level like '%{$dataLevel}%' order by label")->queryAll();
        $varArr=[];
        foreach($varList as $p){
            
            $varArr[$p['abbrev']]=$p['text'];
        }
        return $varArr;
    }

    /**
     * Get the list of programs for select2 widget
     * @return array program list
     */
    public function getProgramList(){
        $varList = Yii::$app->db->createCommand("select abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false")->queryAll();
        $programArr=[];
        foreach($varList as $p){
            $programArr[$p['abbrev']]=$p['text'];
        }
        return $programArr;

    }

    /**
     * Saves pattern record
     * @param  array $params information of the pattern
     * @return json response
     */
    public function savePattern($params){
        extract($params['value']);
        
        $userModel = new User();
        $userId = $userModel->getUserId();
        $program=$basic['program'];
        $pattern_format=$basic['pattern'];
        $sql = "
        select exists(
            Select 
                1 
            from 
                (
                    select 
                        json_array_elements(value::json) as data
                    from 
                        master.program_metadata 
                    where 
                        program_id=(select id from master.program where abbrev='{$program}') 
                        and variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')
                )t
            where
                (data::json->>'id')='{$program}-{$pattern_format}' and
                (data::json->>'is_void')::boolean=false
        )
        ";

        $exists = Yii::$app->db->createCommand($sql)->queryScalar();

        if($exists){
            return [
                "success"=>false,
                "message"=>"Pattern already exists in the program."
            ];
        }
        $sql = "Select value from master.program_metadata where program_id=(select id from master.program where abbrev='{$program}') and variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')";
        $data = Yii::$app->db->createCommand($sql)->queryScalar();

        if(!$data ){
            $data=[[
                "id"=>$program.'-'.$basic["pattern"],
                "programAbbrev" =>$program,
                "programName" =>$basic['programName'],
                "patternFormat"=>$pattern_format,
                "patternName"=>$basic["pattern_name"],
                "metadata"=>json_encode($metadata),
                "pattern"=>json_encode($pattern),
                "creator_id"=>$userId,
                "creation_timestamp"=> date('Y-m-d H:i:s', time()),
                "modifier_id"=>null,
                "modification_timestamp"=>null,
                "is_active"=>($is_active=='true')?true:false,
                "remarks"=>!empty($basic['remarks'])?$basic['remarks']:null,
                "description"=>!empty($basic['description'])?$basic['description']:null,
                "is_void"=>false
                ]
            ];
            $data=json_encode($data);
            
            Yii::$app->db->createCommand("INSERT INTO master.program_metadata(program_id, variable_id, value, creation_timestamp, creator_id)
                select 
                    (select id from master.program where abbrev='{$program}'),
                    (select id from master.variable where abbrev='SEEDLOTCODE_PATTERN'),
                    '{$data}' , 
                    'NOW()', 
                    {$userId}
                ;
                ")->execute();
        }else{ // variable for the program already exists
            
            $data=json_decode($data,true);
            $data=array_merge($data,[
                [
                    "id"=>$program.'-'.$basic["pattern"],
                    "programAbbrev" =>$program,
                    "programName" =>$basic['programName'],
                    "patternFormat"=>$pattern_format,
                    "patternName"=>$basic["pattern_name"],
                    "metadata"=>json_encode($metadata),
                    "pattern"=>json_encode($pattern),
                    "creator_id"=>$userId,
                    "creation_timestamp"=> date('Y-m-d H:i:s', time()),
                    "modifier_id"=>null,
                    "modification_timestamp"=>null,
                    "is_active"=>($is_active=='true')?true:false,
                    "remarks"=>!empty($basic['remarks'])?$basic['remarks']:null,
                    "description"=>!empty($basic['description'])?$basic['description']:null,
                    "is_void"=>false
                ]]);
            
            $data=json_encode($data);
            
            $data = Yii::$app->db->createCommand("UPDATE master.program_metadata set value='{$data}' where program_id=(select id from master.program where abbrev='{$program}') and is_void=false
                    ")->execute();
        }
        return [
            "success"=>true,
            "message"=>"Successfully created pattern."
        ];

    }

    /**
     * Retrieve list of pattern based on identifier
     * @param  array $params program abbrev and pattern format
     * @return array pattern record
     */
    public static function getPattern($params){
        extract($params);
        $sql= "
            select
                (data::json->>'id')::text as id,
                (data::json->>'programAbbrev')::text as \"programAbbrev\",
                (data::json->>'programName')::text as \"programName\",
                (data::json->>'patternName')::text as \"patternName\",
                (data::json->>'patternFormat')::text as \"patternFormat\",
                (data::json->>'remarks') as remarks,
                (data::json->>'is_active') as \"isActive\",
                ((data::json->>'metadata')::json->>'start_seq_no') as \"startNo\",
                ((data::json->>'metadata')::json->>'leading_zeros')::boolean as \"leadingZero\",
                ((data::json->>'metadata')::json->>'digits')::int as digits,
                (data::json->>'pattern')::json as \"pattern\",
                (data::json->>'description')::text as \"description\"
            FROM 
            (select json_array_elements(value::json) as data from master.program_metadata where variable_id=(Select id from master.variable where abbrev='SEEDLOTCODE_PATTERN') 
            )t
            where 
                (data::json->>'programAbbrev')::text='{$programAbbrev}'
                and (data::json->>'patternFormat')::text='{$patternFormat}'
        ";

        $res=Yii::$app->db->createCommand($sql)->queryOne();
        
        return $res;
    }

    /**
     * Updates pattern record
     * @param  array $params pattern record information
     * @return json response
     */
    public function updatePattern($params){
        extract($params['value']);
        
        $userModel = new User();
        $userId = $userModel->getUserId();
        $id=$params['id'];
        $program=$basic['program'];
        $pattern_format=$basic['pattern'];
        
        $sql = "Select value from master.program_metadata where program_id=(select id from master.program where abbrev='{$program}') and variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')";
        $data = Yii::$app->db->createCommand($sql)->queryScalar();

        $data=json_decode($data,true);
        $updatedData=[];
        foreach($data as $item){
            
            if($item['id']==$id){
                $sql = "
                    select exists(
                        Select 
                            1 
                        from 
                            (
                                select 
                                    json_array_elements(value::json) as data
                                from 
                                    master.program_metadata 
                                where 
                                    program_id=(select id from master.program where abbrev='{$program}') 
                                    and variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')
                            )t
                        where
                            (data::json->>'patternFormat')='{$pattern_format}' and
                            (data::json->>'is_void')::boolean=false

                    )
                ";

                $exists = Yii::$app->db->createCommand($sql)->queryScalar();
                
                $basic['description']=!empty($basic['description'])?$basic['description']:null;
                $basic['remarks']=!empty($basic['remarks'])?$basic['remarks']:null;
                $is_active=($is_active=='true')?true:false;
                
                if( $item['description']==$basic['description'] && $item['remarks']==$basic['remarks'] && $item['is_active']== $is_active && $exists){

                 return [
                        "success"=>false,
                        "message"=>"Pattern already exists in the program."
                    ];
                }
                
                $updatedData[]=[
                    "id"=>$program.'-'.$basic["pattern"],
                    "programAbbrev" =>$program,
                    "programName" =>$basic['programName'],
                    "patternFormat"=>$pattern_format,
                    "patternName"=>$basic["update-pattern_name"],
                    "metadata"=>json_encode($metadata),
                    "pattern"=>json_encode($pattern),
                    "creator_id"=>$item['creator_id'],
                    "creation_timestamp"=> $item['creation_timestamp'],
                    "modifier_id"=>$userId,
                    "modification_timestamp"=>date('Y-m-d H:i:s', time()),
                    "is_active"=>($is_active=='true')?true:false,
                    "remarks"=>!empty($basic['remarks'])?$basic['remarks']:null,
                    "description"=>!empty($basic['description'])?$basic['description']:null,
                    "is_void"=>false,
                ];
            }else{
                $updatedData[]=$item;
            }
        }
        
        $data=json_encode($updatedData);

        $data = Yii::$app->db->createCommand("UPDATE master.program_metadata set value='{$data}' where program_id=(select id from master.program where abbrev='{$program}') and is_void=false
            ")->execute();
        
        return [
            "success"=>true,
            "message"=>"Successfully created pattern."
        ];

    }
    
    /**
     * Set pattern record's is_void value 
     * @param  array $params pattern ID and value of true or false
     * @return json response
     */
    public function deletePattern($params){
        extract($params);
        
        $id=explode(',',$id);
        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $sql = "Select program_id from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')";
        $programArr = Yii::$app->db->createCommand($sql)->queryAll();

        foreach($programArr as $prog){
            $programId=$prog['program_id'];

            $sql = "Select value from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN') and program_id={$programId}";
            $data = Yii::$app->db->createCommand($sql)->queryScalar();

            $pattern=json_decode($data,true);
            $updatedData=[];

            foreach($pattern as $item){
                    if(in_array($item['id'],$id)){
                        $updatedData[]=[
                            "id"=>$item["id"],
                            "programAbbrev" =>$item["programAbbrev"],
                            "programName" =>$item['programName'],
                            "patternFormat"=>$item["patternFormat"],
                            "patternName"=>$item["patternName"],
                            "metadata"=>$item['metadata'],
                            "pattern"=>$item['pattern'],
                            "creator_id"=>$item['creator_id'],
                            "creation_timestamp"=> $item['creation_timestamp'],
                            "modifier_id"=>$userId,
                            "modification_timestamp"=>date('Y-m-d H:i:s', time()),
                            "is_active"=>$item['is_active'],
                            "remarks"=>$item['remarks'],
                            "description"=>isset($item['description'])?$item['description']:null,
                            "is_void"=>true,
                        ];
                    }else{
                        $updatedData[]=$item;
                    }
               
            }
            
            $data=json_encode($updatedData);

            $data = Yii::$app->db->createCommand("UPDATE master.program_metadata set value='{$data}' where program_id=($programId)")->execute();
        }
        
        return [
            "success"=>true,
            "message"=>"Successfully deleted pattern."
        ];

    }
    
    /**
     * Set  all pattern records' is_void value 
     * @return json response
     */
    public function deleteAllPattern(){
       
        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $sql = "Select program_id from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')";
        $programArr = Yii::$app->db->createCommand($sql)->queryAll();

        foreach($programArr as $prog){
            $programId=$prog['program_id'];
            $sql = "Select value from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN') and program_id={$programId}";
            $data = Yii::$app->db->createCommand($sql)->queryScalar();

            $pattern=json_decode($data,true);
            $updatedData=[];

            foreach($pattern as $item){
                
                    $updatedData[]=[
                        "id"=>$item["id"],
                        "programAbbrev" =>$item["programAbbrev"],
                        "programName" =>$item['programName'],
                        "patternFormat"=>$item["patternFormat"],
                        "patternName"=>$item["patternName"],
                        "metadata"=>$item['metadata'],
                        "pattern"=>$item['pattern'],
                        "creator_id"=>$item['creator_id'],
                        "creation_timestamp"=> $item['creation_timestamp'],
                        "modifier_id"=>$userId,
                        "modification_timestamp"=>date('Y-m-d H:i:s', time()),
                        "is_active"=>$item['is_active'],
                        "remarks"=>$item['remarks'],
                        "description"=>isset($item['description'])?$item['description']:null,
                        "is_void"=>true,
                    ];
               
            }
            
            $data=json_encode($updatedData);

            $data = Yii::$app->db->createCommand("UPDATE master.program_metadata set value='{$data}' where program_id={$programId}")->execute();
        }
        
        return [
            "success"=>true,
            "message"=>"Successfully deleted all patterns."
        ];

    }

    /**
     * Set pattern record's is_active value
     * @param  array $params ID and/or value
     * @return json response
     */
    public function updateIsActive($params){
        extract($params);
        
        if(isset($id)){
            $id=explode(',',$id);
        }
        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $sql = "Select program_id from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN')";
        $programArr = Yii::$app->db->createCommand($sql)->queryAll();

        foreach($programArr as $prog){
            $programId=$prog['program_id'];

            $sql = "Select value from master.program_metadata where variable_id=(select id from master.variable where abbrev='SEEDLOTCODE_PATTERN') and program_id={$programId}";
            $data = Yii::$app->db->createCommand($sql)->queryScalar();

            $pattern=json_decode($data,true);
            $updatedData=[];

            foreach($pattern as $item){
                if((isset($id) && in_array($item['id'],$id)) || !isset($id)){
                        $updatedData[]=[
                            "id"=>$item["id"],
                            "programAbbrev" =>$item["programAbbrev"],
                            "programName" =>$item['programName'],
                            "patternFormat"=>$item["patternFormat"],
                            "patternName"=>$item["patternName"],
                            "metadata"=>$item['metadata'],
                            "pattern"=>$item['pattern'],
                            "creator_id"=>$item['creator_id'],
                            "creation_timestamp"=> $item['creation_timestamp'],
                            "modifier_id"=>$userId,
                            "modification_timestamp"=>date('Y-m-d H:i:s', time()),
                            "is_active"=>($value=="true")?true:false,
                            "remarks"=>$item['remarks'],
                            "description"=>isset($item['description'])?$item['description']:null,
                            "is_void"=>$item['is_void'],
                        ];
                    }else{
                        $updatedData[]=$item;
                    }
               
            }
            
            $data=json_encode($updatedData);

            $data = Yii::$app->db->createCommand("UPDATE master.program_metadata set value='{$data}' where program_id=($programId)")->execute();
        }
        
        return [
            "success"=>true,
            "message"=>"Successfully updated pattern."
        ];

    }
}