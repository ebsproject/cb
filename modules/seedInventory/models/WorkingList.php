<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;
use app\models\Lists;

use app\interfaces\modules\seedInventory\models\IWorkingList;
use app\interfaces\models\IUserDashboardConfig;

class WorkingList implements IWorkingList
{
    public function __construct(
        public IUserDashboardConfig $userDashboardConfig
    )
    {

    }

    /**
     * Retrievs the working list for find seeds
     * @param $userId integer ID of the current user
     
     * @return array contains the attribute of the working list
     */
    public function getWorkingList($userId) {
        $params = [
            "abbrev" => 'equals WORKING_LIST_PACKAGE_'.$userId,
            "type" => 'equals package',
            "status" => 'equals draft', 
            "isActive" => 'false'
        ];

        $method = 'POST';
        $endpoint = 'lists-search';
        $searchParams = json_encode($params);

        $response = Yii::$app->api->getResponse($method, $endpoint.'?showWorkingList=true', $searchParams);

        $workingList = isset($response) && !empty($response) && isset($response['body']['result']) ? $response['body']['result'] : [];

        if($workingList['data'] != null && !empty($workingList['data'][0])){
            return $workingList['data'][0];
        }
        
        return [];
    }

    /**
     * Retrievs the current working list member
     * @param integer integer ID of the current working list
     * @return array contains list member data
     */
    public function getWorkingListMembers($listDbId) {
        $listMembersData=[];
        $path = 'lists/'.$listDbId.'/members-search?sort=orderNumber';
        
        $results = Yii::$app->api->getResponse('POST',$path,json_encode(''));

        if(isset($results) && !empty($results) && isset($results['body']['result']['data']) && $results['body']['result']['data']){
            $listMembersData = $results['body']['result']['data'];
            $totalPages = $results['body']['metadata']['pagination']['totalPages'];

            if($totalPages > 1){
                for($i=2;$i<=$totalPages;$i++){
                    $results = Yii::$app->api->getResponse('POST',$path.'&page='.$i,json_encode(''));

                    if(isset($results) && !empty($results) && isset($results['body']['result']['data'])){
                        $data = $results['body']['result']['data'];
                        $listMembersData = $data ? array_merge($listMembersData,$data) : $listMembersData;
                    }
                }
            }
        };
        
        return $listMembersData;
    }

    /**
     * Search method for find seeds working list
     * @param $listDbId integer ID of the working list
     */
    public function search($listDbId) {
        if (empty($listDbId)) {
            return new \yii\data\ArrayDataProvider([]);
        }

        $path = 'lists/'.$listDbId.'/members-search';

        $dataProviderId = 'workinglist-results-dp';
        $paramPage = '';

        if (isset($_GET[$dataProviderId . '-page'])){
            $paramPage = '&page=' . $_GET[$dataProviderId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $paramSort = '';
        if(isset($_GET['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $attribDataColCompat = [
                'seedCode' => 'GID',
                'entryCode' => 'sourceEntryCode',
                'entryNumber' => 'seedSourceEntryNumber',
                'plotNumber' => 'seedSourcePlotNumber',
                'plotCode' => 'seedSourcePlotCode',
                'experimentName' => 'experiment',
                'occurrenceName' => 'experimentOccurrence',
                'seasonCode' => 'sourceStudySeason',
                'stageName' => 'experimentStage'
            ];

            foreach($sortParams as $column) {
                $columnName = $column[0] == '-' ? substr($column, 1) : $column;
                $sortColumn = isset($attribDataColCompat[$columnName]) ? $attribDataColCompat[$columnName] : $columnName;

                if($column[0] == '-') {
                    $paramSort = $paramSort . $sortColumn . ':desc';
                } 
                else {
                    $paramSort = $paramSort . $sortColumn;
                }
                
                if($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $response = Yii::$app->api->getResponse('POST', $path.'?'.$paramLimit.$paramPage.$paramSort, json_encode(''));

        $output = $response;
        $data = $output['body']['result']['data'];
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            // 'key' => 'orderNumber',
            'restified' => true,
            'sort' => false,
            'id' => $dataProviderId,
            'totalCount' => $output['body']['metadata']['pagination']['totalCount']
        ]);

        return $dataProvider;
    }

}