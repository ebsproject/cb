<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for variable filter component
 */

namespace app\models;

namespace app\modules\seedInventory\models;

use app\models\Config;
use app\models\User;
use app\models\Variable;
use kartik\widgets\Select2;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

class VariableFilterComponentHarvest extends \yii\db\ActiveRecord
{

    /**
     * Get config values
     * @param none
     * @return array of config values
     */
    public static function getConfig()
    {
        $requiredConfig = Config::findOne(['abbrev' => 'FIND_SEED_FILTERS', 'is_void' => false]);

        if (!empty($requiredConfig)) {
            $configValues = $requiredConfig['config_value'];
            $records = $configValues['values'];

            return $records;
        } 
        else {
            return [];
        }
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public static function retrieveSelectionOptions()
    {

        $records = VariableFilterComponentHarvest::getConfig();
        if (!empty($records)) {
            foreach ($records as $record) {
                if ($record['basic_parameter'] == 'false') {
                    $options[] = $record;
                }
            }
            return $options;
        } else {
            return [];
        }

    }

    /**
     * Generate variable filter field html
     *
     * @param array user id variable filters
     * @return array html elements
     */
    public static function generateFilterFields($userId, $variableFilters, $isBasic)
    {
        $configVariables = VariableFilterComponentHarvest::getConfig();
        $variableFields = [];
        $size = ' col-md-12';

        if(!empty($configVariables) && $configVariables != null){
            
            foreach ($configVariables as $confVar){
                
                $varArray = [];

                $abbrev = $confVar['variable_abbrev'];

                $varArray['label'] = isset($confVar['field_label']) ? $confVar['field_label'] : '';
                $varArray['order_number'] = $confVar['order_number'];
                $varArray['required'] = isset($confVar['required']) ? true : false;
                $varArray['default'] = 0;
                
                $requiredStr = $confVar['required'] == 'true' || $abbrev == 'PROGRAM' ? ' required-field' : '';
                $reqStr = ($confVar['required'] == 'true' || $abbrev == 'PROGRAM' ? ' <span class="required">*</span>' : '');
                $label = Html::label(ucwords(strtolower($varArray['label'])).$reqStr, $abbrev,
                    [
                        'data-label' => ucwords(strtolower($varArray['label'])),
                        'style' => 'margin-top:10px;',
                    ]
                );
            
                $name = ($abbrev == 'VOLUME' ? 'filter[' . $abbrev . '_COND]':'filter[' . $abbrev . ']');

                $initVal = null;
                
                if($variableFilters != null && !empty($variableFilters)){

                    $initVal = null;
                    foreach($variableFilters as $varFilter){
                        if($varFilter['abbrev'] == $abbrev){
                            $initVal = VariableFilterComponentHarvest::getAbbrevValuesById($confVar,$varFilter,$abbrev,$userId);
                            break;
                        }
                    }

                }
                
                if ($confVar['basic_parameter'] == "true" && $isBasic == true) {

                    $varId = strtolower($abbrev) . '-filter';
                    $data = null;

                    if ($confVar['input_field']=='selection'){
                        $varArray['value'] = VariableFilterComponentHarvest::generateHTMLElements($name, $varId, $data, $varArray['default'], $reqStr, $label, $size, false,$initVal,$confVar['input_type']);
                    }
                    else{
                        $varArray['value'] = VariableFilterComponentHarvest::generateHTMLInputTextFieldElements($abbrev, $name, $varId, $varArray['default'], $reqStr, $label, $size, false);
                    }
                    
                    $variableFields[] = $varArray;
                
                }
                if ($confVar['basic_parameter'] == "false" && $variableFilters != null && $isBasic == false){

                    $varId = strtolower($abbrev).'-filter-selected';
                    $removeFxn = true;
                    $data = null;

                    foreach($variableFilters as $selectedFilters){
                        if($selectedFilters['abbrev'] == $abbrev){
                            if ($confVar['input_field']=='selection'){
                                $varArray['value'] = VariableFilterComponentHarvest::generateHTMLElements($name, $varId, $data, $varArray['default'], $reqStr, $label, $size, true,$initVal,$confVar['input_type']);
                            }
                            else{
                                $varArray['value'] = VariableFilterComponentHarvest::generateHTMLInputTextFieldElements($abbrev, $name, $varId, $varArray['default'], $reqStr, $label, $size, true);
                            }
                            
                            $variableFields[] = $varArray;
                            break;
                        }
                    }

                    
                }
                
            }
            return $variableFields;
        }
        else{
            return [];
        }
    }

    /**
     * Extract variable information from id
     *
     * @param array config values init variable info abbrev user id
     * @return array initial variable value info
     */
    public static function getAbbrevValuesById($configVariableInfo,$initVariableInfo,$abbrev,$userId){

        $initVal = [];

        if(isset($initVariableInfo['values']) && $initVariableInfo['values'] != '' && !empty($initVariableInfo['values']) && $initVariableInfo['values'] != [""]){
            $defaultValues = is_array($initVariableInfo['values']) ? $initVariableInfo['values'] : explode(",",$initVariableInfo['values']);

            if (count($defaultValues) > 1 && $defaultValues == '' && $defaultValues!= 0 && $defaultValues[0] == "") {
                array_shift($defaultValues);
            }
    
            $initVal = $defaultValues;
            $addCondition = '';
            
            if ($configVariableInfo['input_field']=='selection' && $abbrev != 'SOURCE_HARV_YEAR' && !empty($defaultValues) && $defaultValues != [''] && $defaultValues != 0){
                $values = [];
                $initVals = ['ids'=>"",'texts'=>""];

                foreach ($defaultValues as $defVal) {
                    $addCondition = " AND ".$configVariableInfo['target_table_alias'].".id= " . $defVal;
                    $dataSearch = VariableFilterComponentHarvest::getVariableFilterDataOld($userId, $abbrev, $configVariableInfo, null, $addCondition,1,0);
                    
                    if($dataSearch['count'] > 0){
                        if($configVariableInfo['input_type'] != "single"){
                            $initVals['ids'] = $initVals['ids'].','.$dataSearch['data'][0]['id'];
                            $initVals['texts'] = $initVals['texts'].'/'.$dataSearch['data'][0]['text'];
                        }
                        else{
                            $initVals = [
                                'ids'=>$dataSearch['data'][0]['id'],
                                'texts'=>$dataSearch['data'][0]['text']
                            ];
                        }
                    }
                    else{
                        $initVals = null;
                    }
                    
                }
                $initVal = $initVals;
            }
            else{
                $initVal = ['ids'=>','.implode(',',$defaultValues),'texts'=>'/'.implode('/',$defaultValues)];
            }
        }

        return $initVal;
    }

    /**
     * Generatesql and extract for basic search variable filters
     *
     * @param array of user id filter abbrev configuration variables q additional condition
     * @return array list
     */
    public static function getBasicVarData($userId, $filterAbbrev, $confVar=null, $condition,$q,$limit=null,$offset=null,$optionsCondition){

        $targetTable = $confVar['target_table'];
        $targetColumn = $confVar['reference_column'];
        $targetTableAlias = $confVar['target_table_alias'];
        $primAttrib = $confVar['primary_attribute'];
        $secAttrib = $confVar['secondary_attribute'];
        $secTargetTable = $confVar['secondary_target_table'];
        $secTargetColumn = $confVar['secondary_target_column'];
        $displayColumn = $confVar['target_column'];

        $displayClause = ($targetColumn == 'year' && $primAttrib == "" && $secAttrib == "") ? "DISTINCT {$targetTableAlias}.id, {$targetTableAlias}.{$targetColumn}" :  "{$targetTableAlias}.id, {$targetTableAlias}.{$displayColumn}, {$targetTableAlias}.program_id";
        
        if($targetColumn != 'year' && $primAttrib != "" && $secAttrib != ""){
            $displayClause = ($secTargetTable != "" ? "id, concat({$primAttrib},' (',{$secAttrib},')') as display_name" : "{$targetTableAlias}.id, concat({$targetTableAlias}.{$primAttrib},' (',{$targetTableAlias}.{$secAttrib},')') as display_name");
            $displayColumn = "display_name";
        }

        $secondarySourceClause = "";
        $connect = "{$targetTableAlias}";

        if($secTargetTable != "" && $secTargetColumn != ""){
            $secondarySourceClause = "
                sec as(
                    SELECT
                        {$displayClause}
                    FROM
                        {$secTargetTable}
                    WHERE
                        is_void = false
                ),
            ";

            $displayClause = "
                {$targetTableAlias}.id as id, 
                {$targetTableAlias}.{$confVar['target_column']},
                sec.display_name
            ";

            $connect = "{$targetTableAlias},sec";

            $condition = $targetTableAlias == 'e' ? " AND e.{$confVar['target_column']} = sec.id ".$condition : " AND {$targetTableAlias}.{$confVar['target_column']} = sec.id ".$condition;
        }

        $primarySourceClause = "
            p as(
                SELECT
                {$displayClause}
                FROM
                    {$targetTable} {$connect}
                WHERE
                    {$targetTableAlias}.is_void = false
                    {$condition}
            )
        ";
        $excepCond = ($displayColumn == 'year' || $confVar['target_column'] == 'phase_id' ? "p.{$confVar['target_column']} AS id, p.{$displayColumn} AS text" : "p.{$displayColumn} AS text, p.id AS id");
        
        $groupCond = ($confVar['target_column'] == 'phase_id' ? "p.{$confVar['target_column']}, p.{$displayColumn} " : "p.id, p.{$displayColumn} " );
        $groupCond = ($displayColumn == 'year' ? "p.{$displayColumn} " : $groupCond);
        
        $castCond = ($q==null ? '' : "CAST(p.{$displayColumn} AS TEXT) ilike '%{$q}%' AND" );
        $sql = "
                with
                {$secondarySourceClause}
                {$primarySourceClause}
                SELECT  
                    {$excepCond}
                FROM 
                    operational.seed_storage oss,
                    p
                    
                WHERE
                    {$castCond}
                    oss.{$targetColumn} = p.id
                    {$optionsCondition}
                GROUP BY {$groupCond}
                ORDER BY p.{$displayColumn} ASC
            ";
        // $sql = "
        //         with
        //         {$secondarySourceClause}
        //         {$primarySourceClause}
        //         SELECT  
        //             {$excepCond}
        //         FROM 
        //             p";
        // $whereClause = "";
        // if (isset($castCond) && isset($optionsCondition) && $optionsCondition != '') {
        //     $whereClause = "
        //             WHERE
        //                 {$castCond}
        //                 {$optionsCondition}";
        // }                    
        // $groupClause = "
        //             GROUP BY {$groupCond}
        //             ORDER BY p.{$displayColumn} ASC
        //         ";
        //ChromePhp::log("castCond isset ",isset($castCond)," value ",$castCond);
        //ChromePhp::log("optionsCondition isset ",isset($optionsCondition)," value ",$optionsCondition);

        $limitClause = "LIMIT {$limit} OFFSET {$offset}";
        // $query = $sql." ".$whereClause." ".$groupClause." ".$limitClause;
        $query = $sql." ".$limitClause;

        $sqlCount = "
            SELECT
                count (*)
            FROM
                (
                    {$sql}
                ) a
        ";
        
        //ChromePhp::log("-------->> ",$query);

        $data = [];//Yii::$app->db->createCommand($query)->queryAll();
        $count = 0;//Yii::$app->db->createCommand($sqlCount)->queryScalar();
        
        $list = array_values($data);
        //ChromePhp::log(">> ",$list);
        return ['data'=>$list,'count'=>$count];
    }

    /**
     * Generatesql and extract for additional search variable filters
     *
     * @param array of user id filter abbrev configuration variables q additional condition
     * @return array list
     */
    public static function getSelectedVarData($userId, $filterAbbrev, $confVar=null, $q = null, $addCondition,$limit=null,$offset=null){
        
        $targetTable = $confVar['target_table'];
        $targetColumn = $confVar['reference_column'];
        $primAttrib = $confVar['primary_attribute'];
        $secAttrib = $confVar['secondary_attribute'];
        $secTargetTable = $confVar['secondary_target_table'];
        $secTargetColumn = $confVar['secondary_target_column'];
        $displayColumn = $confVar['target_column'];

        if($confVar['allowed_values'] != ""){
            
            $splitValues = explode(",",$confVar['allowed_values']);

            $data = [];
            foreach($splitValues as $key => $value){
                $splitValue = explode(":",$value);
                array_push($data, ["text"=>$splitValue[1],"id"=>$splitValue[0]]);
            }

            $dataCount = count($data);
        }
        else {
            $displayClause = ($confVar['variable_abbrev'] == 'SOURCE_HARV_YEAR' ? " distinct year AS id, {$displayColumn} AS text" : "distinct id AS id, {$displayColumn} AS text");
            $addCondition = ($confVar['variable_abbrev'] == 'SOURCE_HARV_YEAR' ? $addCondition." AND year <=  ".date('Y')." AND year >= 1900" : $addCondition);
            $groupCondition = ($confVar['variable_abbrev'] == 'SOURCE_HARV_YEAR' ? "" : " GROUP BY id, {$displayColumn}");

            $addCondition = ($confVar['variable_abbrev'] == 'FACILITY' ? $addCondition." AND facility_type in ('Seed Warehouse','Building')" : $addCondition);
            $addCondition = ($confVar['variable_abbrev'] == 'SUB_FACILITY' ? $addCondition.' AND parent_id is not null' : $addCondition);
            

            $castCond = ($q==null ? '' : "CAST(p.{$displayColumn} AS TEXT) ilike '{$q}%' AND" );
            $sql = "
                SELECT  
                    {$displayClause}
                FROM 
                    {$targetTable} {$confVar['target_table_alias']}
                WHERE
                    CAST({$displayColumn} AS TEXT) ilike '{$q}%' 
                    {$addCondition}
                {$groupCondition}
                ORDER BY text ASC
            ";

            $limitClause = "LIMIT {$limit} OFFSET {$offset}";
            $query = $sql." ".$limitClause;

            $sqlCount = "
                SELECT
                    count (*)
                FROM
                    (
                        {$sql}
                    ) b
            ";

            $data = Yii::$app->db->createCommand($query)->queryAll();
            $dataCount = Yii::$app->db->createCommand($sqlCount)->queryScalar();
            
        }
        
        $list = array_values($data);
        return ['data'=>$list,'count'=>$dataCount];
    }

    /**
     * Retrieve data for variable filters
     *
     * @param array of user id filter abbrev q additional condition
     * @return array list
     */
    public static function getVariableFilterDataOld($userId, $filterAbbrev, $confVar = null,  $q = null, $addCondition = null,$limit=null,$offset=null,$optionsCondition=null)
    {
        $condition = $addCondition;
        if ($confVar['basic_parameter'] == "true"){
            if ($filterAbbrev == 'PROGRAM' && !empty($userId)) {
                
                // $userPrograms = VariableFilterComponentHarvest::getUserPrograms($userId);

                // if (isset($userPrograms) && $userPrograms != null && $userPrograms != 0){
                //     foreach ($userPrograms as $userProgram) {
                //         $programs[] = $userProgram['id'];
                //     };
                //     $condition = 'and '.$confVar['target_table_alias'].'.id in (' . implode(',', $programs) . ') '.$addCondition;
                // }
            }
            $list = VariableFilterComponentHarvest::getBasicVarData($userId, $filterAbbrev, $confVar ,$condition,$q,$limit,$offset,$optionsCondition);
            
        }
        if($confVar['basic_parameter'] == "false"){
            $list = VariableFilterComponentHarvest::getSelectedVarData($userId, $filterAbbrev, $confVar ,$q,$addCondition,$limit,$offset);

        }

        return $list;
    }

    /**
     * Returns programs that the user is a member of
     *
     * @param integer user id
     * @return array id text
     */
    // public static function getUserPrograms($userId){

    //     $sql = "
    //             select
    //                 p.id, p.name || ' (' || p.abbrev || ')' as text
    //             from 
    //                 master.team t,
    //                 master.team_member tm,
    //                 master.program_team pt,
    //                 master.program p
    //             where
    //                 tm.member_id = {$userId}
    //                 and tm.team_id = t.id
    //                 and t.abbrev != 'SG'
    //                 and tm.is_void = false
    //                 and t.is_void = false
    //                 and pt.team_id = t.id
    //                 and p.id = pt.program_id
    //                 and p.level_type = 20
    //             order by 
    //                 p.name asc
    //             ;";
        
    //             $result = Yii::$app->db->CreateCommand($sql)->queryAll();

    //     if(empty($result)){ //if no program
    //         $result = 0;
    //     }
    //     return $result;
    // }

    /**
     * Returns generated Select2 html fields
     *
     * @param name variable id data source elemenet value required string label size
     */
    public static function generateHTMLElements($name, $varId, $data, $val, $reqStr, $label, $size, $removeFxn, $initVal, $inputType)
    {   
        $dataUrl = Url::home() . '/seedInventory/' . Url::to('find-seeds/get-filter-data');
        $updateFilterOptionsUrl = Url::to(['find-seeds/update-filter-values']);  

        $pluginOptions = [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'placeholder' => 'Search value',
            'language' => [
                'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
            ],'ajax' => [
                'url' => $dataUrl,
                'dataType' => 'json',
                'delay'=> 250,
                'data'=> new JSExpression(
                    'function (params) {

                        var filters = $(".find-seeds-filter").serializeArray();
                        var id = $(this).attr("id");

                        return {
                            q: params.term, // search term
                            id: id,
                            page: params.page,
                            filters: JSON.stringify(filters)
                        }
                    }

                    '
                ),
                'processResults' => new JSExpression(
                    'function (data, params) {
                        
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                            more: (params.page * 5) < data.totalCount
                            },
                        };
                    }'
                ),
                'cache' => true
            ],
            'templateResult' => new JSExpression(
                'function formatRepo (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }

                    return repo.text;
                }'
            ),
            'templateSelection' => new JSExpression(
                'function formatRepoSelection (repo) {
                    return repo.text;
                }'
            ),
        ];

        if ($val == 0 || empty($val) || $val == null) {
            $val = null;
        };

        $initValuesText = null;
        $initValues = null;

        if(isset($initVal['ids']) && strpos($initVal['ids'],',')!== false){
            $data = null;
            $initValuesText = ($initVal == null ? null : array_slice(explode('/',$initVal['texts']),1));
            $initValues = ($initVal == null ? null : array_slice(array_map('intval', explode(',', $initVal['ids'])),1));
            $val = implode(',',$initValues);

        }
        else{
            $data = null;
            $initValuesText = ($initVal == null ? null : $initVal['texts']);
            $initValues = ($initVal == null ? null : $initVal['ids']);
            $val = $initValues;
        }

        $volumeValue = null;

        if ($varId == 'volume-filter-selected'){
            $volumeValue = '<span class="col-md-4">'.Html::input(
                'text',
                'filter[VOLUME_VALUE]',
                '',
                [
                    'id' => $varId . '-value',
                    'class' => 'variable-filter input-text-filter',
    
                ]
            ).'</span>';
        }

        $value = Select2::widget([
            'name' => $name,
            'id' => $varId,
            'data' => $data,
            'initValueText' => $initValuesText,
            'value' => $initValues,
            'options' => [
                'tags' => true,
                'class' => 'select2-input-validate' . $reqStr . ' variable-filter ',
                'multiple' => ($inputType === 'single' ? false : true),
            ],
            'pluginOptions' => $pluginOptions,
            'pluginEvents' => [
                "change" => "
                        function() { 
                            $('#'+this.id+'-hidden').val($(this).val()); 
                        }
                        ",
            ],
            'showToggleAll' => ($varId === 'study_name-filter' || $varId === 'experiment_name-filter' || empty($data) || $data == null ? false : true),
        ]);

        $hidden = Html::input(
            'text',
            $name,
            !empty($val) ? $val : null,
            [
                'id' => $varId . '-hidden',
                'class' => 'select2-input-validate-hidden hidden variable-filter',
            ]
        );

        $selectSize = 'col-md-8';
        $removeSize = null;
        $remove = '';
        if ($removeFxn == true) {
            $remove = \macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', ''),
                'icon' => [
                    'name' => 'cancel',
                    'position' => 'right',
                    'options' => [
                        'style' => 'margin-left:0px;',
                    ],
                ],
                'options' => [
                    'id' => $varId . '-remove-btn',
                    'class' => 'btn red '. $varId . '-remove-btn remove-btn',
                    'style' => 'padding-left:10px; padding-right:10px;',
                ],
            ]);
            $selectSize = 'col-md-7';
            $removeSize = 'col-md-1';
        };

        $label = Html::tag('div', $label, ['class' => 'col-md-4', 'style' => 'margin-bottom:5px;']);
        $select = Html::tag('div', $value . $volumeValue . $hidden, ['class' => '' . $selectSize, 'style' => 'margin-bottom:5px; padding-right:10px;']);
        $remove = Html::tag('div', $remove, ['class' => '' . $removeSize, 'style' => 'margin-bottom:5px;']);
        // $remove = null;

        $htmlElement = Html::tag('div', $label . $select . $remove, ['class' => 'control-label row ' . $size, 'style' => 'margin-bottom:10px; padding-right:0px;']);

        return $htmlElement;
    }

    /**
     * Returns generated chips variable filter fields
     *
     * @param name variable id data source elemenet value required string label size
     */
    public static function generateHTMLInputTextFieldElements($abbrev, $name, $varId, $val, $reqStr, $label, $size, $removeFxn)
    {

        $value = '<div class="chips-autocomplete variable-filter ' . $reqStr . '" id=' . $varId . ' style="margin: 0 0 10px 0;"></div>';

        // $caption = '<p class"caption">Input the ' . $abbrev . ' then hit Enter.</p>';
        $caption = null;

        $hidden = Html::input(
            'text',
            $name,
            '',
            [
                'id' => $varId . '-hidden',
                'class' => 'input-text-hidden hidden variable-filter input-text-filter',

            ]
        );

        $selectSize = 'col-md-8';
        $removeSize = null;
        $remove = '';
        if ($removeFxn == true) {
            $remove = \macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', ''),
                'icon' => [
                    'name' => 'cancel',
                    'position' => 'right',
                    'options' => [
                        'style' => 'margin-left:0px;',
                    ],
                ],
                'options' => [
                    'id' => $varId . '-remove-btn',
                    'class' => 'btn red '. $varId . '-remove-btn remove-btn',
                    'style' => 'padding-left:10px; padding-right:10px;',
                    'placeholder' => 'Input the ' . $abbrev . ' then hit Enter.'
                ],
            ]);
            $selectSize = 'col-md-7';
            $removeSize = 'col-md-1';
        };

        $label = Html::tag('div', $label, ['class' => 'col-md-4', 'style' => 'margin-bottom:5px;']);
        $select = Html::tag('div', $value . $hidden . $caption, ['class' => '' . $selectSize, 'style' => 'padding-right:10px;']);
        $remove = Html::tag('div', $remove, ['class' => '' . $removeSize, 'style' => 'margin-bottom:5px;']);

        $htmlElement = Html::tag('div', $label . $select . $remove, ['class' => 'row ' . $size, 'style' => 'margin-bottom:10px; padding-right:0px;']);

        return $htmlElement;
    }
}