<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * Breeding4result is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4result is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use yii\data\ArrayDataProvider;
use app\modules\seedInventory\models;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\models\ListMember;
use app\models\Package;
use app\models\Lists;
use app\models\BackgroundJob;
use app\models\User;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

use app\interfaces\models\IProgram;
use app\interfaces\models\ILists;
use app\interfaces\models\IListMember;
use app\interfaces\models\IPlatformListMember;
use app\interfaces\modules\seedInventory\models\IFindSeedListModel;
use app\interfaces\modules\seedInventory\models\IFindSeedDataBrowserModel;

/**
 * Model for the Find Seed search list
 */
class FindSeedListModel extends \app\models\BaseModel implements IFindSeedListModel
{
    public $configModel;
    public $programModel;
    public $germplasmNameModel;
    public $packageModel;
    public $listsModel;
    public $backgroundJobModel;
    public $listMemberModel;
    public $platformListMemberModel;

    protected $findSeedDataBrowserModel;
    public $workingListModel;
    
    public function __construct(
        IProgram $programModel,
        FindSeedDataBrowserModel $findSeedDataBrowserModel,
        WorkingList $workingListModel,
        ILists $listsModel,
        IListMember $listMemberModel,
        IPlatformListMember $platformListMemberModel
        )
    {
        $this->programModel = $programModel;
        $this->listsModel = $listsModel;
        $this->listMemberModel = $listMemberModel;
        $this->platformListMemberModel = $platformListMemberModel;
        
        $this->configModel = \Yii::$container->get('app\models\Config');
        $this->germplasmNameModel = \Yii::$container->get('app\models\GermplasmName');
        $this->packageModel = \Yii::$container->get('app\models\Package');
        $this->backgroundJobModel = \Yii::$container->get('app\models\BackgroundJob');

        $this->findSeedDataBrowserModel = $findSeedDataBrowserModel;
        $this->workingListModel = $workingListModel;
    }

    /**
     * Add items to platform.list_member
     * @param String $tableName workingList name
     * @param Integer $listId list identifier
     * @param $userId user identifier
     * 
     * @return Boolean status of addition of items to list
     */
    public function addListItems($tableName, $listId, $userId) {
        $workingList = $this->retrieveWorkingList($userId);

        $newItemRecords = array_map(function($workingListItem){
            $idStr = strval($workingListItem['packageDbId']);
            return [
                'id' => $idStr, 
                'displayValue' => $workingListItem['label']
            ];
        },$workingList['data']);

        return $this->insertProcessor($newItemRecords,$listId);
    }

    /**
     * Check if the temp table is existing
     *
     * @param String $tableName name of the temp table
     * @param Integer $userId user identifier
     * @return Boolean
     */
    public function checkIfTableExistWithoutCol($tableName,$userId)
    {
        $result = false;
        $params = [
            "abbrev" => 'equals WORKING_LIST_PACKAGE_'.$userId,
            "type" => 'equals package',
            "status" => 'equals draft', 
            "isActive" => 'false'
        ];
        
        $existingList = $this->listsModel->searchAll($params,'showWorkingList=true');
        if(isset($existingList['data']) && $existingList['data'] != null && !empty($existingList['data'])){
            $result = true;
        }
        
        return $result;
    }

    /**
     * Check seeds if in working list
     *
     * @param Array $selectedIds selected package ids
     * @param Integer $userId user identifier
     * @param Array $existingWorkingList working list data
     * 
     * @return mixed
     */
    public function validateSeedIds($selectedIds, $userId,$existingWorkingList)
    {
        $newListMemberIds = [];
        $duplicateIds = [];
        $results = [];

        if(!empty($existingWorkingList['data']) && !empty($existingWorkingList['totalCount'])){

            $idValues = array_column($existingWorkingList['data'],'packageDbId');
            $newListMemberIds = array_diff($selectedIds,$idValues);
            $duplicateIds = array_intersect($selectedIds,$idValues);

            $results = $newListMemberIds;
        }
        else{
            $results = $selectedIds;
        }

        return ['newListMembers'=>$results,'duplicateListMembers'=>$duplicateIds];
    }

    /**
     * Retrieve package label and id
     * @param Array $newListItems list items
     * @param Boolean $sortByInput to sort by the input values - optional
     * @param Object $sortByInputData - data for sorting by the input values - optional
     * 
     * @return Array package data
     */
    public function retrievePackageDataById($newListItems,$sortByInput = false, $sortByInputData = null){
        $packages=[];
        
        $params=[
            "packageDbId" => 'equals '.implode('|equals ',$newListItems)
        ];

        if($sortByInput){
            $params["sortByInput"] = $sortByInputData;
        }

        $method = 'POST';
        $endpoint = 'seed-packages-search?dataLevel=seed';
        
        $results = Yii::$app->api->getResponse($method, $endpoint, json_encode($params), '');
        $packagesData = $results['body']['result']['data'] ?? [];

        if(isset($results) && !empty($results) && isset($packagesData) && !empty($packagesData)){
            $totalPages = $results['body']['metadata']['pagination']['totalPages'];

            if($totalPages > 1){
                for($i=2;$i<=$totalPages;$i++){
                    $results = Yii::$app->api->getResponse($method, $endpoint.'&page='.$i, json_encode($params), '');
                    $data = $results['body']['result']['data'] ?? [];

                    if(isset($results) && isset($data) && !empty($data)){
                        $packagesData = $data ? array_merge($packagesData,$data) : $packagesData;
                    }
                }
            }
            
            $packages = array_map(function($record){
                return [
                    'displayValue'=>$record['label'],
                    'id'=>$record['packageDbId']
                ];
            },$packagesData);
        };

        return $packages;
    }

    /**
     * Insert values to the temporary table created
     * @param Array $newListItems list items
     * @param Integer $listId list id
     * @return Boolean insert status
     */
    public function insertSelectedItemsToWorkingList($newListItems,$listId){
        if(isset($newListItems) && !empty($newListItems) && $newListItems != null){
            $sortByInputData = [
                "column" => "packageDbId",
                "value" => implode('|',$newListItems)
            ];

            $packages = $this->retrievePackageDataById($newListItems, true, $sortByInputData);
            $workingListItems = count($packages) > 0 ? $packages : [];
            
            // insert items to list
            return $this->insertProcessor($workingListItems,$listId);
        }
        else{
            return false;
        }
    }

    /**
     * Insert selected items using a background job
     * @param Integer $listId list id
     * @param Array $searchParams search parameters 
     * 
     * @return mixed
     */
    public function insertItemsInBackground($listId,$searchParams){
        $userId = Yii::$app->session->get('user.id');

        // create background job record
        $backgroundJobParams = [
            "workerName" => "AddListItems",
            "description" => "Addition of items to current woking list.",
            "entity" => "LIST",
            "entityDbId" => "".$listId,
            "endpointEntity" => "LIST",
            "application" => "FIND_SEEDS",
            "method" => "POST",
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId,
            "remarks" => "Adding list items in progress...",
            "message" => "Adding list items in progress..."
        ];
        $transaction = $this->backgroundJobModel->create(["records" => [$backgroundJobParams]]);

        if(!isset($transaction) || empty($transaction) || !isset($transaction["data"]) || empty($transaction["data"])){
            return json_encode([
                "success" => false,
                "error" => "A connection error occurred while loading the page. Please try again or file a report for assistance."
            ]);
        }
        
        if (isset($transaction["status"]) && !empty($transaction["status"]) && $transaction["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while creating background process record. Kindly contact Administrator."
            ]);
        }
        else{
            $backgroundJobDbId = $transaction["data"][0]["backgroundJobDbId"];

            $host = getenv('CB_RABBITMQ_HOST');
            $port = getenv('CB_RABBITMQ_PORT');
            $user = getenv('CB_RABBITMQ_USER');
            $password = getenv('CB_RABBITMQ_PASSWORD');

            $connection = new AMQPStreamConnection($host, $port, $user, $password);

            $channel = $connection->channel();

            $channel->queue_declare(
                'AddListItems', //queue - Queue names may be up to 255 bytes of UTF-8 characters
                false,  //passive - can use this to check whether an exchange exists without modifying the server state
                true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                false   //auto delete - queue is deleted when last consumer unsubscribes
            );

            // Add token object to worker data
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            
            $msg = new AMQPMessage(
                json_encode([
                    "backgroundJobDbId" => $backgroundJobDbId,
                    "backgroundJobId" => $backgroundJobDbId,
                    "accessToken" => $token,
                    "tokenObject" => [
                        "token" => $token,
                        "refreshToken" => $refreshToken
                    ],
                    "data" => $searchParams,
                    "listDbId" => $listId
                ]),
                array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
            );

            $channel->basic_publish(
                $msg,   //message 
                '', //exchange
                'AddListItems'  //routing key (queue)
            );
            $channel->close();
            $connection->close();

            return json_encode(array("success" => true, "backgroundJobDbId" => $backgroundJobDbId));
        }
    }

    /**
     * Insert values to the temporary table created
     * @param Array $newListItems items to insert
     * @param Integer $listId list identifier
     * 
     * @return mixed
     */
    public function insertTempValuesFromSeedList($newListItems,$listId) {
        if(isset($newListItems) && !empty($newListItems) && $newListItems != null){
            $chunkLimit = 100; // limit to single package search
            $packages = [];

            // if more than 100 items
            if(count($newListItems) > $chunkLimit){
                // partition new items into chunks w/ 100 items
                $idChunks = array_chunk($newListItems,$chunkLimit);
                
                // retrieve package data for given list item
                $packageChunks = array_map(function($idChunk){
                    $params=[
                        "fields" => "package.package_label AS displayValue|package.id AS id",
                        "id" => implode('|',$idChunk)
                    ];

                    $packageChunk = $this->packageModel->searchAll($params,'');
                    if(isset($packageChunk) && !empty($packageChunk) && isset($packageChunk['data'])){ return $packageChunk['data']; }

                },$idChunks);
                
                // merge all package data
                foreach($packageChunks as $packageChunk){
                    $packages = ($packages == null || empty($packages) ? $packageChunk : array_merge($packages,$packageChunk));
                }
            }
            else{
                // retrieve package data for new list items
                $params=[
                    "fields" => "package.package_label AS displayValue|package.id AS id",
                    "id" => implode('|',$newListItems)
                ];
                $filters='';

                $packages = $this->packageModel->searchAll($params,$filters);

                if(isset($packages) && !empty($packages) && isset($packages['data'])){
                    $packages = $packages['data'];
                }
            }

            $workingListItems = count($packages) > 0 ? $packages : [];

            // insert items to list
            return $this->insertProcessor($workingListItems,$listId);;
        }
    }

    /**
     * Builds the prerequisite parameter for the processor
     * @param Array $data contains the request data for the endpoint
     * @param Integer $listId ID of the list
     * @param Integer $totalCount total count of the records to be inserted
     * 
     * @return mixed
     */
    public function insertProcessorWithPrereq($data, $listId, $totalCount) {
        $prerequisite = [
            "endpoint" => "packages-search",
            "data" => $data,
            "mapping" => [
                "packageDbId" => "id",
                "label" => "displayValue"
            ]
        ];

        return $this->insertProcessor([],$listId, $prerequisite, $totalCount);
    }

    /**
     * Processor implementation for inserting package records to list
     * @param Array $newItemRecords records to be inserted
     * @param Integer $listId list identifier
     * @param Array $prereq necessary pre-requisite
     * @param Integer $totalCount number of items  to be inserted
     * @return Boolean api call status
     */
    public function insertProcessor($newItemRecords=[],$listId=null,$prereq=[],$totalCount=0){
        $processorThreshold = !empty(Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList')) ? Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList') : 500;

        $itemRecordsCount = count($newItemRecords);
        if ($itemRecordsCount > 0) {
            $totalCount = $itemRecordsCount;
        }

        $backgroundProcess = true;       
        if(isset($listId) && !empty($listId)){
            $method = 'POST';
            $path = 'lists/'.$listId.'/members';
            $params = [
                'records' => $newItemRecords,
            ];
            
            if($totalCount > $processorThreshold){
                $backgroundProcess = true;
                $bgProcessParams = [
                    "httpMethod" => 'POST',
                    "endpoint" => 'v3/'.$path,
                    "description" => "Addition of items to working list",
                    "entity" => "LIST",
                    "entityDbId" => strval($listId),
                    "endpointEntity" => "LIST_MEMBER",
                    "application" => "FIND_SEEDS",
                ];

                if (!empty($prereq)) {
                    $bgProcessParams['requestData'] = [
                        "prerequisite" => $prereq
                    ];
                } else {
                    $bgProcessParams['requestData'] = $params;
                }
                $params = $bgProcessParams;
                $path = 'processor';
            }
            $results = Yii::$app->api->getResponse($method, $path, json_encode($params,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
            
            if(isset($results['status']) && $results['status'] == 200){
                return true;
            }
            else{
                return false;
            }
        }
    }

    /**
     * Delete values to the temporary table created
     * @param Array $selectedRemoveIds ids of records to be removed
     * @param Integer $userId user id
     * @param Integer $itemCount number of items
     * @return Boolean status
     */
    public function deleteTempValuesFromSeedList($selectedRemoveIds, $userId,$itemCount)
    {   
        $limit = !empty(Yii::$app->config->getAppThreshold('FIND_SEEDS', 'deleteWorkingList')) ? Yii::$app->config->getAppThreshold('FIND_SEEDS', 'deleteWorkingList') : 500;

        $backgroundProcess = false;
        $additionalParams = [];
        if ($itemCount > $limit) {

            // call processor
            $backgroundProcess = true;
            $additionalParams = [
                "description" => "Deletion of working list items",
                "entity" => "LIST",
                "endpointEntity" => "LIST_MEMBER",
                "application" => "FIND_SEEDS"
            ];
        }
        
        $response = $this->listMemberModel->deleteMany($selectedRemoveIds, $backgroundProcess, $additionalParams); 

        if (isset($response['status']) && !empty($response['status']) && $response['status'] == 200) {
            $affectedRecords = $response['totalCount'];
            return true;

        } else {
            return false;
        }
    }

    /**
     * Get working list browser data provider
     * @param Integer $userId user id 
     * @return mixed
     */
    public function getListDataProvider($userId) {

        $columnConfig = $this->findSeedDataBrowserModel->getColumns();
        $totalCount = 0;
        
        $workingList = $this->retrieveWorkingList($userId);

        $totalCount = $workingList['totalCount'];
        $results = $workingList['data'];

        if ($totalCount > 0) {
            $listDataProvider = new ArrayDataProvider([
                'allModels' => $results,
                'totalCount' => $totalCount,
                'sort' => false,
            ]);
        } else {
            $empty = [];
            $listDataProvider = new ArrayDataProvider([
                'allModels' => $empty,
                'totalCount' => $totalCount,
                'sort' => false,
            ]);
        }
 
    
        return ['data' => $listDataProvider, 'count' => $totalCount];
    }

    /**
    * Retrieve saved lists of a certain type for working list
    * @param Integer $userId user id 
    * @param String $listType list type
    * @param array data provider items count
    **/
    public function getSavedSeedListDataProvider($userId,$listType){

        $method = 'POST';
        $path = 'lists-search';
        $strId = (string)$userId;
        $listData = [];

        if($listType == 'package'){
            $rawData = [
                'type' => 'equals '.$listType,
                'creatorDbId' => 'equals '.$strId,
                'isActive' => 'true'
            ];
        }

        if($listType == 'germplasm'){
            $rawData = [
                'type' => 'equals germplasm',
                'creatorDbId' => 'equals '.$strId
            ];
        }
        $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), 'sort=creationTimestamp:desc', true);
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
            $results = $results['body']['result']['data'];

            foreach ($results as $listItem) {
                $listData[] = [
                    'id' => $listItem['listDbId'],
                    'display_name' => $listItem['displayName'],
                    'abbrev' => $listItem['abbrev'],
                    'item_count' => $listItem['memberCount']
                ];
            }
        }

        $listDataProvider = new ArrayDataProvider([
            'key' => 'id',
            'allModels' => $listData,
            'pagination' => false
        ]);

        return ['data' => $listDataProvider];
    }

    /**
    * Retrieve saved list items by id
    * @param Integer $id list id
    * @return Array list items
    **/
    public function getSavedListById($id){

        $apiCallResult = Yii::$app->api->getResponse('POST','lists/'.$id.'/members-search',json_encode(["isActive" => "TRUE"]),'',true);

        $success = true;
        $error = '';
        $result = [];
        if(isset($apiCallResult['status']) && !empty($apiCallResult['status']) && $apiCallResult['status'] == 200){
            $data = $apiCallResult['body']['result']['data'];

            if(isset($data) && !empty($data)){
                $result = array_column($data,'designation');
            }
        }
        else{
            $success = false;
            $error = isset($apiCallResult['body']['metadata']['status'][0]['message']) && !empty($apiCallResult['body']['metadata']['status'][0]['message']) ? $apiCallResult['body']['metadata']['status'][0]['message'] : 'Error encountered in retrieving saved list.';
        }

        return ['success'=>$success,'error'=>$error,'result'=>$result];
    }

    /**
    * Retrieve saved list information
    * @param Integer $id list id
    * @return Array list data
    **/
    public function getSavedListData($id){
        $params = [
            "listDbId"=>"equals ".$id
        ];
        $apiCallResult = Yii::$app->api->getResponse('POST','lists-search',json_encode($params));
        
        $data = [];
        if(isset($apiCallResult['status']) && !empty($apiCallResult['status']) && $apiCallResult['status'] == 200){
            $data = isset($apiCallResult['body']['result']['data'][0]) && !empty($apiCallResult['body']['result']['data'][0]) ?  $apiCallResult['body']['result']['data'][0] : [];
        }

        return $data;
    }

    /**
    * Retrieve saved list items by id
    * @param Integer $id list id
    * @return Array list items
    **/
    public function getSavedSeedListById($id){
        $totalCount = 0;
        $listId = $id;
        $listAbbrev = '';
        $result = [];

        $params = [
            "listDbId" => "equals ".$id
        ];

        $workingList = $this->listsModel->searchAll($params,'showWorkingList=true');
        if($workingList['data'] != null && !empty($workingList['data'][0])){
            $listId = $workingList['data'][0]['listDbId'];
            $listAbbrev = $workingList['data'][0]['abbrev'];
            $memberFilters = '';
            
            $workingListMembers = $this->platformListMemberModel->searchAllMembers($workingList['data'][0]['listDbId'],$memberFilters,null);
            if($workingListMembers['totalPages'] > 0 && !empty($workingListMembers['data'])){

                $result = $workingListMembers['data'];
                $totalCount = count($workingListMembers['data']);
            }
        }
        
        return ['data'=>$result,'totalCount'=>$totalCount,'listId'=>$listId,'listAbbrev'=>$listAbbrev];
    }
    
    /**
     * Save list basic information
     * @param Array list info
     * @return mixed api call return
     */
    public function saveListBasicInformation($listInfo){
        
        $record = [
            'records' => [
                    $listInfo
                ]
        ];
        
        $apiCallResult = Yii::$app->api->getResponse('POST','lists',json_encode($record));
        
        return isset($apiCallResult) && !empty($apiCallResult) ? $apiCallResult : [];
    }

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function removeDuplicates($listDbId, $idArray) {
        // check existing Ids
        $idStr = implode('|equals ', $idArray);
        $results = $this->platformListMemberModel->searchAllMembers($listDbId, '', ["packageDbId" => 'equals '. $idStr]);
        
        if(isset($results) &&!empty($results) && isset($results['totalCount'])){
            if ($results['totalCount'] == 0) {
                return $idArray;
            } else {
                $data = $results['data'];
                $data = array_column($data, 'packageDbId');
                $data = array_diff($idArray, $data);
                return $data;
            }
        }
        else{
            return $idArray;
        }
    }

    /**
     * Retrieve working list items
     * @param Integer $userId user id
     * @return array data total count
     */
    public function retrieveWorkingList($userId){

        $totalCount = 0;
        $result = [];
        $listId = 0;
        $listAbbrev = '';
        $totalPageCount = 0;
        
        $params = [
            "abbrev" => 'equals WORKING_LIST_PACKAGE_'.$userId,
            "type" => 'equals package',
            "status" => 'equals draft', 
            "isActive" => 'false'
        ];

        $workingList = $this->listsModel->searchAll($params,'showWorkingList=true');

        if(isset($workingList) && isset($workingList['data']) && !empty($workingList['data'][0])){
            $listId = $workingList['data'][0]['listDbId'];
            $listAbbrev = $workingList['data'][0]['abbrev'];
            $memberFilters = '';
            
            $workingListMembers = $this->platformListMemberModel->searchAllMembers($workingList['data'][0]['listDbId'],$memberFilters,null);

            if(isset($workingListMembers) && isset($workingListMembers['totalPages']) && $workingListMembers['totalPages'] > 0 && !empty($workingListMembers['data'])){
                
                $result = $workingListMembers['data'];
                $totalCount = $workingListMembers['totalCount'];
                $totalPageCount = $workingListMembers['totalPages'];
            }
        }
        
        return ['data'=>$result,'totalCount'=>$totalCount,'listId'=>$listId,'listAbbrev'=>$listAbbrev, 'totalPageCount' => $totalPageCount];
    }

    /**
     * Clear working list items
     * @param Integer $userId user id
     * @return boolean status
     */
    public function clearWorkingList($userId){
        $success = false;
        $bgprocess = false;

        $workingListRes = $this->workingListModel->getWorkingList($userId);

        $listDbId = isset($workingListRes['listDbId']) ? $workingListRes['listDbId'] : null;    

        if(isset($listDbId) && $listDbId){
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('FIND_SEEDS', 'deleteWorkingList')) ? Yii::$app->config->getAppThreshold('FIND_SEEDS', 'deleteWorkingList') : 500;
            $totalCount = $workingListRes['memberCount'];

            // pass to bg process if exceed threshold
            if ($totalCount > $bgThreshold) {
                // void existing working list
                $newAbbrev = 'WORKING_LIST_PACKAGE_'.$userId.'-DELETED';

                $params = [
                "abbrev" => 'WORKING_LIST_PACKAGE-DELETED_'.$listDbId,
                "name" => 'Working List Package DELETED_'.$listDbId,
                "displayName"=>'Working List Package DELETE',
                ];
                $callResult = $this->listsModel->updateOne($listDbId, $params);

                // create new working list
                $record = [
                    'abbrev'=> 'WORKING_LIST_PACKAGE_'.$userId,
                    'name'=> 'Working List Package '.$userId,
                    'description'=>'user session package working list',
                    'displayName'=>'Find Seeds Package Working List',
                    'remarks'=>'user session package working list',
                    'type'=> 'package',
                    'status' => 'draft',
                    'isActive' => 'false',
                    'listUsage' => 'working list'
                ];
                $result = $this->saveListBasicInformation($record);

                if(isset($result['status']) && !empty($result['status']) && $result['status'] == 200){
                    $workingListDbId = $result['body']['result']['data'][0]['listDbId'];
                    Yii::$app->session->set('workingListDbId',$workingListDbId);
                }

                // void list members in the background
                $backgroundProcess = true;
                $additionalParams = [
                    "description" => "Clearing of working list items",
                    "entity" => "LIST",
                    'entityDbId' => (string) $listDbId, 
                    "endpointEntity" => "LIST_MEMBER",
                    "application" => "FIND_SEEDS",
                    'requestData' => [
                        "prerequisite" => [
                            "endpoint" => "lists/".$listDbId."/members-search"
                        ]
                    ]
                ];
                $response = $this->listMemberModel->deleteMany("listMemberDbId", $backgroundProcess, $additionalParams);

                $success = true;
                $bgprocess = true;
            } else {
                $data = $this->workingListModel->getWorkingListMembers($listDbId);

                if($data){
                    $memberIds = array_column($data, 'listMemberDbId');
                    $response = $this->listMemberModel->deleteMany($memberIds);

                    $success = false;
                    $bgprocess = false;
                    if ($response['status'] == 200) {
                        $affectedRecords = $response['totalCount'];
                        $success = true;
                    }
                }

            }
        }

        return ['success'=>$success,'bgprocess'=>$bgprocess];
    }

    /**
     * update list record information
     * @param Integer $listDbId list identifier
     * @param Array $params record parameters
     * @return array mixed
     */
    public function updateListRecord($listDbId,$params){
        $newName = '';
        $newId = '';
        $error = '';
        $success = true;

        $data = $this->listsModel->updateOne($listDbId,$params);
        if (isset($data['status']) && !empty($data['status']) && $data['status'] == 200) {
            $success = true;
            $newName = $params['name'];
            $newId = $data['data'][0]['listDbId'];
        } else {
            $error = $data['message'];
            $success = false;
        }

        return ['success'=>$success,'error'=>$error,'newName'=>$newName,'newListDbId'=>$newId];
    }

    /**
     * Create working list
     * @param Integer user id
     * @return Array working list
     */
    public function createWorkingList($userId){
        $tableName = '';
        $checkIfTableExist = $this->checkIfTableExistWithoutCol($tableName,$userId);

        if($checkIfTableExist == false){
            
            $record = [
                "records" => [
                    0 => [
                        "abbrev"=>'WORKING_LIST_PACKAGE_'.$userId,
                        "name"=>'Working List Package '.$userId,
                        "description"=>'user session package working list',
                        "displayName"=>'Working List Package',
                        "remarks"=>'user session package working list',
                        "type"=>'package',
                        "status" =>'draft',
                        "isActive" =>'false',
                        'listUsage' => 'working list'
                    ]
                ]
            ];
            
            $response = $this->listsModel->create($record);
            if (isset($response['status']) && !empty($response['status']) && $response['status'] == 200) {
                $existingWorkingList = $this->retrieveWorkingList($userId);
            } else {
                $existingWorkingList = null;
            }
        }
        else{
            $existingWorkingList = $this->retrieveWorkingList($userId);
        }

        return $existingWorkingList;
    }

    /**
     * Saves the items from the working list to a final list
     * @param Array $data contains the request data with the list ID and list name
     * @return Array contains whether the operation is succesfull with a notification message
     */
    public function saveListItems($data, $userId) {

        $listId = $data['listId'];
        $listName = $data['listName'];
        $tableName = 'temp_findseed_'.$userId;

        $success = true;
        $message = '';
        
        $recordCount = $this->getItemsCount();
        if(isset($recordCount) && $recordCount > 0){
            $success = $this->addListItems($tableName,$listId,$userId);
            if($success){    
                $success = true;
                $message = "Successfully created ".$listName."!";
            }
        }
        else{
            $message = $listName." has no items.";
        }

        return [
            'success' => $success,
            'message' => $message
        ];
    }

    /**
     * Retrieves the total count of the items in a working list
     * @return Integer total record count
     */
    public function getItemsCount() {

        $userModel = new User();
        $userId = $userModel->getUserId();
        $tableName = 'temp_findseed_'.$userId;
        $recordCount = 0;

        $checkTempList = $this->checkIfTableExistWithoutCol($tableName,$userId);
        if($checkTempList == true){
            $workingList = $this->retrieveWorkingList($userId);
            $recordCount = $workingList['totalCount'];
        }
        return $recordCount;
    }
}
