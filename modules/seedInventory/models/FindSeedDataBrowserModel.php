<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * Breeding4result is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4result is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

namespace app\modules\seedInventory\models;
use yii\data\SqlDataProvider;

use app\models\GermplasmName;
use app\models\Config;
use app\models\Package;
use app\models\User;
use app\models\Program;
use app\models\Variable;

use app\modules\seedInventory\models\VariableFilterComponent;

use app\interfaces\models\IProgram;
use app\interfaces\models\IConfig;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IIndicesSearch;
use app\interfaces\modules\seedInventory\models\IFindSeedDataBrowserModel;
use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

use kartik\widgets\Select2;
use app\dataproviders\ArrayDataProvider;

/**
 * Model for the Find Seed data browser
 */
class FindSeedDataBrowserModel extends \app\models\BaseModel implements IFindSeedDataBrowserModel
{
    private $dynamicFields = [];
    private $dynamicRules = [];
    private $columns=[];
    private $sqlQuery='';
  
    public $configModel;
    public $programModel;
    public $germplasmNameModel;
    public $packageModel;
    public $variableFilterComponent;
    public $indicesSearchModel;
    
    public function __construct(
        IProgram $programModel,
        IConfig $configModel,
        IVariableFilterComponent $variableFilterComponent,
        IIndicesSearch $indicesSearchModel,
        public IUserDashboardConfig $userDashboardConfig
        )
    {
        $this->programModel = $programModel;
        $this->configModel = $configModel;
        $this->variableFilterComponent = $variableFilterComponent;
        $this->germplasmNameModel = \Yii::$container->get('app\models\GermplasmName');
        $this->packageModel = \Yii::$container->get('app\models\Package');
        $this->indicesSearchModel = $indicesSearchModel;
    }

    /**
     * PHP getter magic method.
     * This method is overridden so that attributes are the dynamic fields
     *
     * @param string $name property name
     * @return mixed property value
     * @see getAttribute()
     */
    public function __get($name) {

        if (array_key_exists($name, $this->dynamicFields))
            return $this->dynamicFields[$name];
        else
            return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes are the dynamic fields
     *
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value) {

        if (array_key_exists($name, $this->dynamicFields))
            $this->dynamicFields[$name] = $value;
        else
            parent::__set($name, $value);
    }

    /**
     * Returns the validation rules for attributes.
     * This method is overridden
     * 
     * @return array validation rules
     */
    public function rules() {
        
        return [
            [['id'],'safe'],
            [$this->dynamicRules,'safe']
        ];
    }

    /**
     *   Set dynamic columns
     * @param $config array 
     */
    public function setColumnsForFiltering($config = null)
    {

        foreach ($this->getColumns() as $col) {

            $this->dynamicFields[strtolower($col['variable_abbrev'])] = null;
            $this->columns[] = strtolower($col['variable_abbrev']);

            $this->dynamicRules[] = strtolower($col['variable_abbrev']);
        }

        return true;
    }

    /**
     * Retrieve columns from configuration dataset
     * @return array Array of configuration
     */
    public function getColumns(){

        $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_QUERY_RESULTS');

        $columns = [];
        if(isset($config) && !empty($config)){
            $mainTableInfo = [
                'main_table'=> isset($config['main_table']) ? $config['main_table'] : '',
                'main_table_alias'=>isset($config['main_table_alias']) ? $config['main_table_alias'] : ''
            ];

            if(isset($config['values']) && !empty($config['values'])){
                $records = $config['values'];
                $records = $this->validateColumnConfigVariables($records);

                if(isset($records) && !empty($records) && $records != null && $records != []){
                    $columns = $records;
                }
            }
        }

        return $columns;
    }

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function validateColumnConfigVariables($records) {
        $validatedRecords = [];

        if(isset($records) && !empty($records) && $records != null) {
            foreach($records as $variableKey => $variableInfo) {
                $params = [
                    'abbrev'=>'equals ' . $variableInfo['variable_abbrev']
                ];
    
                $results = Yii::$app->api->getResponse('POST', 'variables-search', json_encode($params));

                if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])) {
                    $results = $results['body']['result']['data'];

                    foreach($results as $index=>$variableFilter){
                        
                        if($variableFilter['abbrev']==$variableInfo['variable_abbrev']){
                            $records[$variableKey]['field_description']=$results[$index]['description'];
                            $validatedRecords[] = $records[$variableKey];
                        }
                    }
                }
            }
        }
        return $validatedRecords;
    }

    /**
     * Get search DataProvider
     * @return mixed
     */
    public function getDataProvider($condition, $condStr=null, $columns=null, $inputListOrder=false, $browserFilters=[], $getAllResponse=false, $getParamsOnly=false) {
        $dataProviderId = 'dynagrid-seed-search-search-grid';

        if (!empty($condition)) {
            /** PROCESS FILTERS IN QUERY PARAMS */
            // Normalize designation
            foreach ($condition as $key=>$normalizeCond) {
                if($normalizeCond['abbrev']=='DESIGNATION' || $normalizeCond['abbrev']=='NAME'){
                    $normalize = $normalizeCond['values'];
                    $records = $this->germplasmNameModel->getStandardizedGermplasmName($normalize);
                    $arrayColumnNormalize = array_column($records,'norm_input_product_name');
                    $condition[$key]['values'] = $normalizeCond['values'];
                    $condition[$key]['api_body_param'] = 'designation';
                    break;
                }
            }

            // Get API resource method and endpoint
            $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_PARAMS');
            
            $configValues = [];
            if(isset($config) && !empty($config)){
                $configValues = $config['values'];
            }

            // Retrieve and process input list type value from config
            $inputListTypeValues = $this->variableFilterComponent->retrieveInputListConfig();
            $inputListTypes = [];

            if(isset($inputListTypeValues) && !empty($inputListTypeValues)){
                foreach($inputListTypeValues as $key=>$option){
                    $inputListTypeValues[strtoupper($key)] = $option;
                    array_push($inputListTypes,strtoupper($key));
                };
            }

            // Process parameters and variables from seed params config
            $finalParams = [];
            if(!empty($configValues)){
                foreach($configValues as $keyConfig=>$configVal){
                    foreach($condition as $key=>$conditionAbbrev){
                        if ($conditionAbbrev['abbrev'] == $configVal['variable_abbrev']){
                            $api_resource_method = $configVal['api_resource_method'];
                            $api_resource_endpoint = $configVal['api_resource_endpoint'];
                            $api_body_param = $configVal['api_body_param'] ?? "";

                            $condition[$key]['api_resource_method'] = $api_resource_method;
                            $condition[$key]['api_resource_endpoint'] = $api_resource_endpoint;
                            $condition[$key]['api_body_param'] = $api_body_param;

                            $conditionValues = $condition[$key]['values'];
                            $apiBodyParam = $condition[$key]['api_body_param'];

                            if(in_array($conditionAbbrev['abbrev'],$inputListTypes)){
                                $finalParams[$apiBodyParam] = implode('|', $conditionValues);
                            }
                            else{
                                if($conditionAbbrev['abbrev'] == 'VOLUME'){
                                    $finalParams[$apiBodyParam] = implode('|', $conditionValues);
                                }
                                else if(strpos(implode('',$conditionValues),'range') !== false){
                                    $values = '';
                                    $delimiter = '';
                                    foreach($conditionValues as $key=>$val){
                                        $delimiter = $values === '' ? '' : '|';
                                        $values .= $delimiter . (strpos($val,'range') !== false ? $val : 'equals '. $val);
                                    }
                                    $finalParams[$apiBodyParam] = $values;
                                }
                                else{
                                    $finalParams[$apiBodyParam] = 'equals '.implode('|equals ', $conditionValues);
                                }
                            }
                        }

                        $isListType = in_array($conditionAbbrev['abbrev'],$inputListTypes);
                        $isSearchParam = isset($inputListTypeValues[$conditionAbbrev['abbrev']]) && !isset($finalParams[$inputListTypeValues[$conditionAbbrev['abbrev']]['apiBodyParam']]);
                        
                        // Get api_body_param for input values
                        if($isListType && $isSearchParam){
                            // set search values
                            $conditionValues = implode('|', $condition[$key]['values']);

                            // add as final search param condition
                            if($condition[$key]['abbrev'] != "NAME"){
                                $finalParams[$inputListTypeValues[$conditionAbbrev['abbrev']]['apiBodyParam']] = $conditionValues;
                            }
                            
                            // add sort condition for input list search
                            $finalParams['sortByInput'] = [
                                "column"=>"".$inputListTypeValues[$conditionAbbrev['abbrev']]['apiBodyParam'],
                                "value"=>$conditionValues
                            ];
                        }

                        if($conditionAbbrev['abbrev'] == 'packageDbId' && (!isset($finalParams['packageDbId']))){
                            $finalParams['packageDbId'] = 'not equals '.implode('| not equals ', $condition[$key]['values']);
                        }
    
                        if($conditionAbbrev['abbrev'] == 'workingListDbId' && (!isset($finalParams['workingListDbId']))){
                            $finalParams['workingListDbId'] = strval($condition[$key]['values']);
                        }
    
                        if($conditionAbbrev['abbrev'] == 'multipleSelect' && (!isset($finalParams['multipleSelect']))){
                            $finalParams['multipleSelect'] = $condition[$key]['values'];
                        }
    
                        if($conditionAbbrev['abbrev'] == 'germplasmDbId' && (!isset($finalParams['germplasmDbId'])) && !empty($condition[$key]['values']) && $condition[$key]['values'] != null){
                            $finalParams['germplasmDbId'] = 'not equals '.implode('| not equals ', $condition[$key]['values']);
                        }
                    }
                }
            }
            
            /** PROCESS FILTERS FROM DATA BROWSER */
            // Get API resource method and endpoint
            $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_QUERY_RESULTS');

            if(isset($config) && !empty($config)){
                $configValues = $config['values'];
            }

            $tempFilters = [];
            foreach ($browserFilters as $key=>$filter) {
                foreach ($configValues as $keyConfig => $configVal) {
                    // check if wildcard search
                    $tempStr = implode('',$filter['values']);
                    $tempArr = strpos($tempStr,',') === FALSE ? $filter['values'] : explode(',',$tempStr);
                        
                    $prepend = strpos($tempStr,'%') === FALSE ? 'equals ' : '';

                    if ($filter['abbrev'] != 'SOURCE_HARV_YEAR'){
                        // Get api_body_param for input values
                        if($conditionAbbrev['abbrev'] == 'DESIGNATION'){
                            $records = $this->germplasmNameModel->getStandardizedGermplasmName($filter['values']);
                            $arrayColumnNormalize = array_column($records,'norm_input_product_name');
                            $finalParams['germplasmNormalizedName'] = $prepend.implode('|'.$prepend, $arrayColumnNormalize);
                        }
                        else if($conditionAbbrev['abbrev'] == 'SEED'){
                            $tempStr = implode('',$filter[$key]['values']);
                            $prepend = strpos($tempStr,'%') === FALSE ? 'equals ' : '';

                            $tempArr = strpos($tempStr,',') === FALSE ? $filter[$key]['values'] : explode(',',$tempStr);
                            $finalParams['seedName'] = $prepend.implode('|'.$prepend, $tempArr);
                        }
                        else if($conditionAbbrev['abbrev'] == 'LABEL'){
                            $tempStr = implode('',$filter[$key]['values']);
                            $prepend = strpos($tempStr,'%') === FALSE ? 'equals ' : '';

                            $tempArr = strpos($tempStr,',') === FALSE ? $filter[$key]['values'] : explode(',',$tempStr);
                            $finalParams['label'] = $prepend.implode('|'.$prepend, $tempArr);
                        }
                        else if ($filter['abbrev'] == $configVal['variable_abbrev']) {
                            $apiBodyParam = $configVal['api_body_param'] ?? "";
                            $filter['api_body_param'] = $apiBodyParam;

                            $conditionValues = $prepend.implode('|'.$prepend, $tempArr);
                            $tempFilters[$apiBodyParam] = trim($conditionValues);
                        }
                    }
                    else{
                        $tempFilters['sourceHarvestYear'] = $prepend.implode('|'.$prepend, $tempArr);
                    }
                }
            }

            if (!empty($tempFilters)) {
                $finalParams['__browser_filters__'] = $tempFilters;
            }

            $params = isset($finalParams) && !empty($finalParams) ? $finalParams : [];

            // Get API resource method and endpoint
            $queryConfig = $config;
            $queryConfigValues = [];
            if(isset($queryConfig) && !empty($queryConfig)){
                $queryConfigValues = $queryConfig['values'];
            }
            $paramSort = '';

            // set default results browser configuration
            $isGrouped = isset(Yii::$app->session['qrIsGrouped']) ? Yii::$app->session['qrIsGrouped'] : 'true';

            $sortParams = '';
            if(isset($_GET['sort'])) {
                $sortParams = $_GET['sort'];
            } else if (isset($_GET['dp-1-sort'])) {
                $sortParams = $_GET['dp-1-sort'];
            } else if (isset($_GET[$dataProviderId.'-sort'])) {
                $sortParams = $_GET[$dataProviderId.'-sort'];
            }

            if (empty($sortParams) && !isset($finalParams['sortByInput'])) {
                $paramSort = '&sort=designation:asc|sourceHarvestYear:desc|experimentYear:desc';
            } else {
                $paramSort = isset($finalParams['sortByInput']) ? '':'&sort=designation:asc';

                if ($isGrouped == 'false') {
                    $paramSort = '';
                }
            }

            // Set sorting parameters
            foreach($queryConfigValues as $keyConfig=>$configVal){ 
                // for sorting
                if (!empty($sortParams)) {
                    if($sortParams[0] == '-') {
                        $desc = ':desc';
                        $column = substr($sortParams, 1);
                    } else {
                        $desc = '';
                        $column = $sortParams;
                    }
                    
                    if ($column == strtolower($configVal['variable_abbrev'])) {
                        if (empty($paramSort)) {
                            $paramSort = '&sort=';
                        } else {
                            $paramSort .= '|';
                        }
                        $paramSort = $paramSort . ($configVal['api_body_param'] ?? "") . $desc;
                        $setSort = true;
                        break;
                    } else if ($column == 'label') {
                        if (empty($paramSort)) {
                            $paramSort = '&sort=';
                        } else {
                            $paramSort .= '|';
                        }
                        $paramSort = $paramSort . $column . $desc;
                        $setSort = true;
                        break;
                    } else if ($column == 'designation') {
                        $paramSort = '&sort='. $column . $desc;
                        $setSort = true;
                        break;
                    } else if ($column == 'gid') {
                        if (empty($paramSort)) {
                            $paramSort = '&sort=';
                        } else {
                            $paramSort .= '|';
                        }
                        $paramSort = $paramSort . 'seedCode' . $desc;
                        $setSort = true;
                        break;
                    }
                }
            }

            /** get current page number and page size */
            $paramPage = '';
            $pageCount = 1;
            if (isset($_GET[$dataProviderId . '-page'])){
                $paramPage = '&page=' . $_GET[$dataProviderId . '-page'];
                $pageCount = $_GET[$dataProviderId.'-page'];
            } else if (isset($_GET['page'])) {
                $paramPage = '&page=' . $_GET['page'];
                $pageCount = $_GET['page'];
            }

            $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

            if ($getAllResponse) {
                if ($getParamsOnly) {
                    return $params;
                } else {
                    $results = $this->packageModel->searchAll($params,'',true,true);
                    return $results;
                }
            }

            if (isset(Yii::$app->session['packageLimitQr'])) {
                $packageLimit = Yii::$app->session['packageLimitQr'];
                if ($packageLimit > 0 && $packageLimit != '') {
                    $params['groupLimit'] = $packageLimit;
                }
            }

            if (isset(Yii::$app->session['qrIsRetained'])) {
                $qrIsRetained = Yii::$app->session['qrIsRetained'];
                if ($qrIsRetained == 'true') {
                    $params['retainSelection'] = $qrIsRetained;
                }
            }

            $specialCharList = '/[\'\/\\`\\\"]/';
            if( (!empty($params['seedName']) && isset($params['seedName']) && $params['seedName']) || 
                (!empty($params['germplasmNormalizedName']) && isset($params['germplasmNormalizedName']) && $params['germplasmNormalizedName']) || 
                (!empty($params['label']) && isset($params['label']) && $params['label'])
                ){
                if(!empty($params['seedName']) && isset($params['seedName']) && $params['seedName']) $testStr = $params['seedName'];
                if(!empty($params['germplasmNormalizedName']) && isset($params['germplasmNormalizedName']) && $params['germplasmNormalizedName']) $testStr = $params['germplasmNormalizedName'];
                if(!empty($params['label']) && isset($params['label']) && $params['label']) $testStr = $params['label'];

                $params['sortByInput']['value'] = $testStr;
            }

            // apply special character parsing to browser filters
            if(isset($params['__browser_filters__'])){
                foreach($params['__browser_filters__'] as $key=>$filterValue){
                    if( in_array($key,['label','seedName','designation']) && $filterValue != ''){
                        $testStr = $filterValue;
                        if(preg_match($specialCharList, $testStr)){
                            $escapeBackslash = preg_replace("/\\\/",'\\\\\\\\',$testStr);
                            $testStr = $escapeBackslash;
                        }
                        $params['__browser_filters__'][$key] = $testStr;
                    }
                }
            }
            $results = [];

            //set parameters to search ACTIVE packages only
            $params['packageStatus'] = "active";

            $advancedSearchTotalCount = 0;
            $advancedSearchCurrentPage = 1;

            $requestBody = json_encode($params);
            $dataLevel = 'dataLevel=all&';

            $results = Yii::$app->api->getResponse('POST', 'seed-packages-search?'.$dataLevel.$paramLimit.$paramPage.$paramSort, $requestBody, '');
            $totalCount  = isset($results['body']['metadata']['pagination']['totalCount']) && !empty($results['body']['metadata']['pagination']['totalCount']) ? $results['body']['metadata']['pagination']['totalCount'] : 0;
            
            if ($pageCount != 1 && $totalCount == 0) {
                $paramPage = "&page=1";
                $results = Yii::$app->api->getResponse('POST', 'seed-packages-search?'.$dataLevel.$paramLimit.$paramPage.$paramSort, $requestBody, '');
                $totalCount  = isset($results['body']['metadata']['pagination']['totalCount']) && !empty($results['body']['metadata']['pagination']['totalCount']) ? $results['body']['metadata']['pagination']['totalCount'] : 0;
            
                // set page to 1
                $_GET['page'] = 1;
                $_GET[$dataProviderId . '-page'] = 1;
            }

            if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                $results = $results['body']['result']['data'];
            }

            if ($totalCount > 0) {
                $dataProvider = new ArrayDataProvider([
                    'allModels' => $results,
                    'totalCount' => $totalCount,
                    'restified' => true,
                    'disableDefaultSort' => true,
                    'id' => $dataProviderId
                ]);
            } else {
                $empty = [];
                $dataProvider = new ArrayDataProvider([
                    'allModels' => $empty,
                    'totalCount' => $totalCount,
                    'sort' => false,
                ]);
            }
            Yii::$app->session->set('fsEncodedParams', json_encode($params));

            return ['data' => $dataProvider, 'count' => $totalCount];
        } else {
            $empty = [];
            $dataProvider = new ArrayDataProvider([
                'allModels' => $empty,
                'totalCount' => 0,
                'sort' => false,
                'id' => $dataProviderId,
                'pagination' => false
            ]);
            return ['data' => $dataProvider, 'count' => 0];
        }
    }

    /**
     * Retrieve program code
     * @param array program value
     * @return string program code
     */
    public function getProgramCode($programIds){
        $params = ['programDbId'=>'equals '.implode('|equals ',$programIds)];
        $programs = $this->programModel->searchAll($params);
        $programCode = isset($programs) && !empty($programs) && isset($programs['data']) && !empty($programs['data']) ? implode('|',array_column($programs['data'],'programCode')) : '';

        return $programCode;
    }

    /**
     * Process variable filter values
     * @param array passed filters
     * @return array variable filter
     */
    public function processFilters($passedFilters)
    {
        $variableFilters = [];

        $inputListTypeValues = $this->variableFilterComponent->retrieveInputListConfig();
        $inputListTypes = [];
        if(isset($inputListTypeValues) && !empty($inputListTypeValues)){
            foreach($inputListTypeValues as $key=>$option){
                $inputListTypeValues[strtoupper($key)] = $option;
                array_push($inputListTypes,strtoupper($key));
            };
        }
        
        foreach ($passedFilters as $filter) {
            if (!empty($filter['value']) && !empty($filter['name'])) {
                if(in_array($filter['name'],$inputListTypes) || $filter['name'] == 'PLOT_CODE' ||
                     $filter['name'] == 'VOLUME' || $filter['name'] == 'PLOTNO' || $filter['name'] == 'ENTNO' || $filter['name'] == 'UNIT' || 
                     $filter['name'] == 'EXPERIMENT_TYPE' || $filter['name'] == 'HVDATE_CONT' || $filter['name'] == 'ENTCODE' || $filter['name'] == 'REP') {
                
                    $valueId = preg_split('/,/', $filter['value'], -1, PREG_SPLIT_NO_EMPTY);
                } else if($filter['name']=='includeSeedlist' || $filter['name']=='userId'){
                    $valueId=$filter['value'];
                } else {
                    $valueId = array_map('intval',preg_split('/,/', $filter['value'], -1, PREG_SPLIT_NO_EMPTY));
                }

                array_push(
                    $variableFilters, 
                    [
                        'abbrev' => $filter['name'], 
                        'values' => $valueId, 
                        "operator"=>"in",
                        "filter_type"=>'search'
                    ]
                );
            }
        }

        return $variableFilters;
    }
    /**
     * Retrieve entry data
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function search($params, $dataFilters, $inputListOrder){
        $this->load($params);
        $condStr = '';
        $condTable = '';
        $browserFilters = [];
        
        if($params){
            foreach ($params['FindSeedDataBrowserModel'] as $key => $value) {
                if($value!==null && $value!=''){
                    $condStr = $condStr . " and {$key} ilike '%". $value ."%'";
                    $browserFilters[]=[
                        "abbrev"=>strtoupper($key),
                        "values"=>[$value],
                        "operator"=>'ilike any',
                        "filter_type"=>'column'
                    ];
                    
                }
            }
        }
        $data = $this->getDataProvider($dataFilters, $condStr, $this->columns, $inputListOrder, $browserFilters);
        return $data['data'];
    }

    /**
     * Retrieve search dquery parameters
     * needed in query builder 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function getSearchData($params, $dataFilters, $inputListOrder){
        $condStr = '';   
        if($params){
            foreach ($params['FindSeedDataBrowserModel'] as $key => $value) {
                if($value!==null && $value!=''){
                    $dataFilters[]=[
                        "abbrev"=>strtoupper($key),
                        "values"=>[$value],
                        "operator"=>'ilike any',
                        "filter_type"=>'column'
                    ];
                    
                }
            }
        }

        return $dataFilters;
    }

    /**
     * Retrieve all package ids
     * 
     * @param String $encodedParams search parameters
     * 
     * @return Array ids of single-package records
     */
    public function retrieveAllSinglePackageRecordIds($encodedParams){
        $method = 'POST';
        $endpoint = 'seed-packages-search';

        $decodedParams = json_decode($encodedParams);
        $results = Yii::$app->api->getResponse($method, $endpoint, $encodedParams, 'dataLevel=all', true);
        
        // extract single package records packageDbIds
        $singlePkgRecords = [];
        if(isset($results['body']['result']['data']) && !empty($results['body']['result']['data'])){
            $results = $results['body']['result']['data'];

            foreach($results as $record){
                if( intval($record['packageCount']) == 1 || 
                    (isset($decodedParams->groupLimit) && intval($decodedParams->groupLimit) == 1) ){
                    array_push($singlePkgRecords,$record['packageDbId']);
                }
            }
        }

        return $singlePkgRecords;
    }

    /**
     * Build fields parameter in search params
     * 
     * @param Array $searchParams search parameters
     * 
     * @return $searchParameters
     */
    public function buildSearchParamFields($searchParams){
        $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_PARAMS');

        $placeholder = [];
        $placeholder['fields'] = 'package.package_label AS label|' .
            'package.package_code AS packageCode|' .
            'package.package_status AS packageStatus|' .
            'germplasm.id AS germplasmDbId|' .
            'germplasm.designation AS names|' .
            'germplasm.designation|' .
            'germplasm.germplasm_code AS germplasmCode|' .
            'germplasm.germplasm_normalized_name AS germplasmNormalizedName|' .
            'package.id AS packageDbId|' .
            'seed.id AS seedDbId|' .
            'seed.seed_code AS seedCode|' .
            'seed.seed_name AS seedName|' .
            'seed.harvest_date AS harvestDate';

        $tempStr = implode('|',array_filter(
                array_map(function ($variable){
                    if(!empty($variable['output_id_value'] && !empty($variable['target_table']))){
                        return ($variable['target_table'] == 'location_occurrence_group' ? 'plot' : $variable['target_table']).
                                '.'.$variable['output_id_value'].' AS '.($variable['api_body_param'] ?? "");
                    }
                },$config['values']),
                function ($value){
                    return ($value !== NULL && $value !== FALSE && $value !== "");
                }
            ));

        $searchParams['fields'] = $placeholder['fields'].'|'.$tempStr;

        return $searchParams;
    }

    /**
     * Retrieve filtered data using parameters via Advanced Search api call
     * 
     * @param Array $param parameters
     * @param Array $limit limit
     * @param Array $page  page 
     * @return mixed
     */
    public function getFilteredData($params,$limit,$page) {

        $excludedParams = [
            "sortByInput","fields","groupOrderLimit","retainSelection","packageStatus",
            "_browser_filters_","workingListDbId","mtaStatus", "rshtReferenceNo","moistureContent",
            "multipleSelect","distinctOn","groupLimit"];
        $normalizeParams = ["designaton", "germplasmName","names"];
        
        // Build search params
        $searchParams = [];
        $searchParams['searchIndex'] = 'cb-seed-packages-search';

        foreach($params as $key => $paramValue){
            if(!in_array($key,$excludedParams) && $key != '__browser_filters__'){
                $searchParams[$key] = $paramValue;
            }
            
            // Process designation and germplasm name; normalize values
            if(in_array($key,$normalizeParams) && $key != '__browser_filters__'){

                $paramValue = explode('|',$paramValue);
                $output = $this->germplasmNameModel->getStandardizedGermplasmName($paramValue);

                $normalizedValues = implode('|',array_column($output,'norm_input_product_name'));

                if($key == "names"){
                    unset($searchParams['names']);
                    $searchParams['germplasmNormalizedName'] = $normalizedValues;
                }
                else{
                    $searchParams[$key] = $normalizedValues;
                }
            }
        }

        // Process browser filters
        if(isset($params['__browser_filters__'])){
            // process filters
            $browserFilters = $params['__browser_filters__'];

            foreach($browserFilters as $key => $paramValue){
                if(!in_array($key,$excludedParams)){
                    $searchParams[$key] = $paramValue;
                }

                if(in_array($key,$normalizeParams)){

                    $paramValue = explode('|',$paramValue);
                    $output = $this->germplasmNameModel->getStandardizedGermplasmName($paramValue);

                    $normalizedValues = implode('|',array_column($output,'norm_input_product_name'));

                    if($key == "names"){
                        unset($searchParams['names']);
                        $searchParams['germplasmNormalizedName'] = $normalizedValues;
                    }
                    else{
                        $searchParams[$key] = $normalizedValues;
                    }
                }
            }
        }

        if(($limit.$page) != ""){
            $limit = "?" . $limit;
        }
        
        // Retrieve packageIds from advanced search
        $encodedParams= json_encode($searchParams);
        $results = $this->indicesSearchModel->search($encodedParams,$limit.$page);

        if($results['status'] != 200){
            return [];
        }

        return [
            'data' => $results['body']['result']['data'] ?? [],
            'totalCount' => $results['body']['metadata']['pagination']['totalCount'] ?? 0,
            'currentPage' => $results['body']['metadata']['pagination']['currentPage'] ?? 1
        ]; 
    }}
