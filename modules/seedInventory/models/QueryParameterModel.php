<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for query parameters
 */

namespace app\models;

namespace app\modules\seedInventory\models;
use yii\data\SqlDataProvider;

use app\models\Config;
use app\models\User;
use app\models\Variable;
use kartik\widgets\Select2;
use Yii;
use yii\helpers\ArrayHelper;
use app\dataproviders\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

use app\interfaces\modules\seedInventory\models\IQueryParameterModel;

class QueryParameterModel extends \yii\db\ActiveRecord implements IQueryParameterModel
{
    public function __construct()
    {
        
    }

    /**
     * Validate query parameter
     * @param String $name name of query param record
     * @param String $type type of param record
     * @param Array $data search data
     * @return array api call return
     */
    public function getQueryParam($name,$type,$data){

        $duplicate = false;
        $params = [
            'dataBrowserDbId'=>'equals find-seeds',
            'type'=>'equals '.$type,
            'name'=>'equals '.$name
        ];

        $apiCallResult = Yii::$app->api->getResponse('POST','data-browser-configurations-search',json_encode($params));
        if(isset($apiCallResult) && !empty($apiCallResult) && !empty($apiCallResult['status']) && $apiCallResult['status'] == 200 && isset($apiCallResult['body']['result']['data']) && isset($apiCallResult['body']['result']['data'][0]) && $apiCallResult['body']['result']['data'][0] != []){
            $callResult = $apiCallResult['body']['result']['data'];    
            $data = json_decode($data,true);
            
            $duplicate = !empty(array_intersect($data,$callResult[0]['data'])) ? true : true;
        }
    
        return $duplicate;
    }

    /**
     * Get the query parameters
     * @param Integer $userId user identification
     * @param String $toolId tool identifier
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsDataProvider($userId, $toolId) {

        $params = [
            'dataBrowserDbId'=>'equals '.$toolId,
            'type'=>'equals filter',
        ];

        $sort = '?sort=configDbId:desc';
        $apiCallResult = Yii::$app->api->getResponse('POST','data-browser-configurations-search'.$sort,json_encode($params));

        if(isset($apiCallResult['status']) && !empty($apiCallResult['status']) && $apiCallResult['status'] == 200 && isset($apiCallResult['body']['result']['data']) && isset($apiCallResult['body']['result']['data'][0]) && $apiCallResult['body']['result']['data'][0] != []){
            $callResult = $apiCallResult['body']['result']['data'];

            $data = [];
            foreach($callResult as $key=>$value){
                $data[] = [
                    'configDbId'=>$value['configDbId'],
                    'name'=>$value['name'],
                    'data'=>$value['data'],
                ];
            }
            $dataCount = intval($apiCallResult['body']['metadata']['pagination']['totalCount']);

            $sort = [
                'attributes' => ['configDbId'],
                'defaultOrder' => [
                    'configDbId'=>SORT_DESC
                ]
            ];

            $data = new ArrayDataProvider([
                'key'=>'configDbId',
                'allModels' => $data,
                'sort' => $sort,
                'pagination' => false
            ]);
        }
        else{
            $data = new ArrayDataProvider([
                'key'=>'configDbId',
                'allModels' => [],
                'sort' => [],
                'pagination' => false
            ]);
            $dataCount = 0;
        }

        return ['data' => $data, 'count' => $dataCount];
    }

    /**
     * Save query parameters
     * @param Arrat $queryParamRecord record data to be saved
     * @return Array $result Array of query parameters
     */
    public function saveQueryParam($queryParamRecord){

        $record = ['records' => [$queryParamRecord]];
        $apiCallResult = Yii::$app->api->getResponse(
            'POST',
            'data-browser-configurations/',
            json_encode($record,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );
        return isset($apiCallResult['status']) && !empty($apiCallResult['status']) ? $apiCallResult['status'] : 500;
    }

    /**
     * Get the query parameters by id
     * @param Integer $paramId id of param record
     * @param Integer $userId user identifier
     * @param String $appTool tool name
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsById($paramId,$userId,$appTool) {

        $params = [
            'dataBrowserDbId' =>'equals '.$appTool,
            'configDbId' =>'equals '.$paramId,
            'type'=>'equals filter'
        ];
        
        $apiCallResult = Yii::$app->api->getResponse('POST','data-browser-configurations-search',json_encode($params));
        if( isset($apiCallResult) && !empty($apiCallResult['status']) && isset($apiCallResult['status']) && 
            $apiCallResult['status'] == 200 && isset($apiCallResult['body']['result']['data']) && 
            isset($apiCallResult['body']['result']['data'][0]) && $apiCallResult['body']['result']['data'][0] != []){
            $callResult = $apiCallResult['body']['result']['data'][0];
            if(isset($callResult['data']) && !empty($callResult['data']) && isset($callResult['data']['ids']) && isset($callResult['data']['texts'])){
                return json_encode($callResult['data']['ids'],JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            }
            else{
                return json_encode($callResult['data'],JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            }
        }
        else{
            return '';
        }
    }
}