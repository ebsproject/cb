<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use ChromePhp;


class UnitOfVolumeModel extends \app\models\ScaleValue
{



    /**
     * Return data provider 
     * @param String $selectSql columns defined
     * @param String $rawSql the raw sql to be converted into a data provider
     * @param string $condition Sql condition
     * @return SqlDataProvider $dataProvider data object for browser
     **/
    public static function getDataProvider($rawSql, $selectSql, $condition)
    {

        $conversionColumnFieldResult = UnitOfVolumeModel::getUnitScaleDefined();

        $columnTemp = "id integer,Unit character varying,Description text";

        foreach ($conversionColumnFieldResult as $h) {

            $columnTemp = $columnTemp . "," . $h['value'] . " float";
        }

        $sql = "select * from crosstab('
        select 
        source_unit.id,
        source_unit.value,
        source_unit.description,
        target_unit.value,
        cv.conversion_value as value
        
        from master.scale_conversion as cv 
        left join master.scale_value as source_unit on source_unit.id=cv.source_unit_id
        left join master.scale_value as target_unit on target_unit.id=cv.target_unit_id
        
        order by id
        '
        ,'select distinct target_unit.value from master.scale_conversion cv left join master.scale_value as target_unit on target_unit.id=cv.target_unit_id order by target_unit.value')
        as final_result({$columnTemp})";


        $countSql = "select  count(distinct source_unit_id) from master.scale_conversion ";

        $totalCount = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'key' => 'unit',
            'sort' => [
                'attributes' => [
                    'unit'
                ],
                'defaultOrder' => [
                    'unit' => 'SORT_ASC'
                ]
            ]
        ]);

        return $dataProvider;
    }

    public static function getUnitScaleDefined()
    {
        $sqlConversionColumnField = "select distinct  (select id from master.scale_value where id=target_unit_id), (select value from master.scale_value where id=target_unit_id)from master.scale_conversion order by (select value from master.scale_value where id=target_unit_id)";
        return Yii::$app->db->createCommand($sqlConversionColumnField)->queryAll();
    }


    public function getUnitScaleValue()
    {

        $unitScale = Yii::$app->db->createCommand("select id,description||' ('||value||')' as text from master.scale_value where scale_id=983 and is_void=false")->queryAll();
        $unitScaleArr = [];
        foreach ($unitScale as $u) {
            $unitScaleArr[$u['id']] = $u['text'];
        }
        return $unitScaleArr;
    }

    public function getUnitScaleList()
    {

        $unitScale = Yii::$app->db->createCommand("select id, value from master.scale_value where scale_id=983 and is_void=false")->queryAll();

        $unitScaleArrList = array();
        foreach ($unitScale as $u) {
            $row = array(
                'id' => $u['id'],
                'value' => $u['value']
            );
            array_push($unitScaleArrList, $row);
        }


        return $unitScaleArrList;
    }

    public static function getUnitScaleValueId()
    {

        $unitScale = Yii::$app->db->createCommand("select id, value from master.scale_value where scale_id=983 and is_void=false")->queryAll();

        // $unitScaleArrList=array();
        $key = array();
        foreach ($unitScale as $u) {
            $key["{$u['value']}"] = $u['id'];
        }


        return $key;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains the different filter optiosn
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $this->load($params);
        $query = [];
        $queryCondition = '';

        return UnitOfVolumeModel::getDataProvider(null, null, null);
    }
}
