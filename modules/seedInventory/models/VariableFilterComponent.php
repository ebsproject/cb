<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for variable filter component
 */

namespace app\models;

namespace app\modules\seedInventory\models;
use Yii;

use yii\data\SqlDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use app\models\Config;
use app\models\User;
use app\models\Program;
use app\models\Variable;
use app\interfaces\models\IProgram;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;
use app\interfaces\models\IConfig;
use app\interfaces\models\IVariable;
use app\interfaces\models\IApi;
use app\interfaces\models\IUser;


use kartik\widgets\Select2;

class VariableFilterComponent extends \yii\db\ActiveRecord implements IVariableFilterComponent
{

    public $configModel;
    public $variableModel;
    public $programModel;
    public $api;
    public $userModel;

    public function __construct(
            IConfig $configModel, 
            IVariable $variableModel,
            IProgram $programModel,
            IApi $api,
            IUser $userModel
        )
    {
        $this->configModel = $configModel;
        $this->variableModel = $variableModel;
        $this->programModel = $programModel;
        $this->api = $api;

        $this->userModel = $userModel;
    }

    /**
     * Get config values
     * @param none
     * @return array of config values
     */
    public function getConfig()
    {
        $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_PARAMS');
        
        $result = [];
        if(isset($config) && !empty($config)){
            $mainTableInfo = [
                'main_table'=> isset($config['main_table']) && $config['main_table'] != '' ? $config['main_table'] : '',
                'main_table_alias'=> isset($config['main_table_alias']) && $config['main_table_alias'] != '' ? $config['main_table_alias'] : ''
            ];

            $records = [];
            if(isset($config['values']) && !empty($config['values'])){
                $records = $config['values'];
                $records = $this->validateConfigVariables($records);
    
                if(isset($records) && !empty($records) && $records != null && $records != []){
                    $result = ['records'=>$records,'mainTableInfo' => $mainTableInfo];
                }
            }
        }

        return $result;
    }

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function validateConfigVariables($records)
    {
        $validatedRecords = [];
        if(isset($records) && !empty($records) && $records != null){
            foreach($records as $variableKey => $variableInfo){
                $variableInfo = $this->variableModel->getVariableByAbbrev($variableInfo['variable_abbrev']);

                if(isset($variableInfo) && !empty($variableInfo)){
                    $records[$variableKey]['field_description'] = isset($variableInfo['description']) && $variableInfo['description'] != '' ? $variableInfo['description'] : '';
                    $records[$variableKey]['field_label'] = isset($variableInfo['label']) && $variableInfo['label'] != '' ? $variableInfo['label'] : '';

                    $validatedRecords[] = $records[$variableKey];
                }
            }
        }
        return $validatedRecords;
    }

    /**
     * Retrieve input list field type values and configuration
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveInputListConfig(){
        $config = $this->configModel->getConfigByAbbrev('FIND_SEEDS_PARAMS');

        $inputListConfig = [];
        
        if(isset($config) && !empty($config)){
            if(isset($config['input_field_type']) && !empty($config['input_field_type'])){
                
                $records = $config['input_field_type'];
                foreach($records as $index=>$value){
                    
                    $inputListConfig[$value['id']] = [
                        'text' => $value['label'].' - '.$value['description'],
                        'description' => [
                            'data-toggle'=>"tooltip",
                            'title'=>$value['description']
                        ],
                        'validation' => $value['validation'],
                        'apiBodyParam' => $value['api_body_param'] ?? ""
                    ];
                };
            }
        }

        return $inputListConfig;
    }

    /**
     * Retrieve config file from db for options in variable filter selection
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveRequiredFilters(){
        $requiredFilters = [];
        $configVariables = $this->getConfig();

        if(isset($configVariables) && !empty($configVariables) && isset($configVariables['records']) && !empty($configVariables['records'])){
            $records = $configVariables['records'];

            if (!empty($records)) {
                foreach ($records as $record) {
                    if ($record['required'] == 'true') { $requiredFilters[] = $record; }
                }

                if(isset($requiredFilters) && $requiredFilters != null){
                    $requiredFilters = array_column($requiredFilters,'variable_abbrev');
                }
            }
        }

        return $requiredFilters;
    } 

    /**
     * Retrieve config file from db for options in variable filter selection
     * @param none
     * @return array of abbrev and corresponding field labels
     */
    public function retrieveSelectionOptions()
    {
        $options = [];
        $configVariables = $this->getConfig();

        if(isset($configVariables) && !empty($configVariables) && isset($configVariables['records']) && !empty($configVariables['records'])){
            $records = $configVariables['records'];

            if (!empty($records)) {
                foreach ($records as $record) {
                    if ($record['basic_parameter'] == 'false') { $options[] = $record; }
                }
            }
        }

        return $options;
    }

    /**
     * Generate filter field html for given variables
     * @param Array configVariables
     * @param Array variableFilters
     * @param Integer userId
     * @param Boolean isBasic
     * @return Array variableFields
     */
    public function generateInputFields($configVariables, $variableFilters, $userId, $isBasic=true) // TODO: refactor
    {
        $variableFields = [];
        foreach ($configVariables as $confVar){
            $varArray = [];
            $abbrev = $confVar['variable_abbrev'];

            $varArray['label'] = isset($confVar['field_label']) ? $confVar['field_label'] : '';
            $varArray['order_number'] = isset($confVar['order_number']) ? $confVar['order_number'] : '';
            $varArray['required'] = isset($confVar['required']) ? true : false;
            $varArray['default'] = 0;
            $varArray['type'] = isset($confVar['input_field']) ? $confVar['input_field'] : '';
            $varArray['abbrev'] = isset($confVar['variable_abbrev']) ? $confVar['variable_abbrev'] : '';
            
            $requiredStr = $isBasic ? '' : ($confVar['required'] == 'true' || $abbrev == 'PROGRAM' ? ' <span class="required">*</span>' : '');
            $label = Html::label(ucwords(strtolower($varArray['label'])).$requiredStr, $abbrev,
                [
                    'data-label' => ucwords(strtolower($varArray['label'])),
                    'style' => 'margin-top:10px;',
                ]
            );
            
            $name = 'filter[' . $abbrev . ']';
            
            $initVal = $this->getInitFilterValues($userId,$variableFilters,$confVar);

            $size = $confVar['basic_parameter'] == "true" ? ' col-md-4' : ' col-md-12';

            if($confVar['variable_abbrev'] == 'SEASON' || $confVar['variable_abbrev'] == 'EXPERIMENT_YEAR'){
                $size = ' col-md-2';
            }
            
            $varId = strtolower($abbrev) . '-filter'. ( !$isBasic ? '-selected' : '');
            $varArray['component_id'] = $varId;

            $data = null;
            $removeFxn = !$isBasic;
            
            if ($confVar['input_field']=='selection'){
                $varArray['value'] = $this->generateHTMLElements($name, $varId, $data, $varArray['default'], $requiredStr, $label, $size, $removeFxn, $initVal,$confVar['input_type'],$confVar);
            }
            else{
                $varArray['value'] = $this->generateHTMLInputTextFieldElements($abbrev, $name, $varId, $varArray['default'], $requiredStr, $label, $size, $removeFxn, $confVar,$initVal);
            }
            $variableFields[] = $varArray;
        }

        return $variableFields;
    }

    /**
     * Generate variable filter field html
     *
     * @param Array user id
     * @param Array variableFilters
     * @param Boolean isBasic
     * @param Array additionalFilter
     * @return Array html elements
     */
    public function generateFilterFields($userId, $variableFilters, $isBasic, $additionalFilter = [])
    {
        $userId = isset($userId) && !empty($userId) ? $userId : $this->user->getUserId();

        $configInfo = $this->getConfig();
        $configVariables = isset($configInfo) && !empty($configInfo) && isset($configInfo['records']) ? $configInfo['records'] : [];
        $variableFields = [];
        $fields = [];

        if(isset($configInfo) && !empty($configInfo) && isset($configInfo['records']) && !empty($configVariables) && $configVariables != null){
            
            if($isBasic && $additionalFilter == []){
                foreach ($configVariables as $confVar){
                    if($confVar['basic_parameter'] == "true"){ $variableFields[] = $confVar; }
                }    
            }
            
            if(!$isBasic && !empty($additionalFilter)){
                foreach($additionalFilter as $index=>$filterAbbrev){
                    foreach ($configVariables as $confVar){
                        if($confVar['variable_abbrev'] == $filterAbbrev){
                            $variableFields[] = $confVar;
                            break;
                        }
                    }
                }
            }
            
            $fields = $this->generateInputFields($variableFields, $variableFilters, $userId, $isBasic);
        }

        return $fields;
    }

    /**
     * Retrieve filter initial values; refactoring of retrieveInitialValues()
     *
     * @param Integer user id
     * @param Array variable filters
     * @param Array config variable
     * @return array initial value
     */
    public function getInitFilterValues($userId,$variableFilters,$confVar){
        $initVal = [];
        
        if($variableFilters != null && !empty($variableFilters)){
            $existingFilterAbbrevArr = array_column($variableFilters,'abbrev');
            // should only use searches for filters with existing filters

            // check if target filter has existing filters
            // check if confVar variable_abbrev is in existingFilterAbbrevArr
            if(in_array($confVar['variable_abbrev'],$existingFilterAbbrevArr)){
                // retrieve existing filter values for confVar
                $existingFilterValues = null;
                foreach($variableFilters as $existingFilter){
                    if($existingFilter['abbrev'] == $confVar['variable_abbrev']){
                        $existingFilterValues = $existingFilter['values'];
                        break;
                    }
                }
                // retrieve initial values for target filter

                // if input field
                if($confVar['input_field'] == 'input field'){
                    $initVal =  is_array($existingFilterValues) ? $existingFilterValues.join(',') : $existingFilterValues;
                }

                // if unit
                if($confVar['input_field'] == 'selection' && $confVar['variable_abbrev'] == 'UNIT'){
                    $initVal = ['ids'=>$existingFilterValues,'texts'=>$existingFilterValues];
                }

                // if volume
                if($confVar['input_field'] == 'selection' && $confVar['variable_abbrev'] == 'VOLUME'){
                    // retrieve default condition values from config
                    $configDefaultConditions = explode(',',$confVar['default_value']);
                    $conditionGuide = [];
                    foreach($configDefaultConditions as $defCond){
                        $tmp = explode(':',$defCond);
                        $temp = explode('(',$tmp[1]);
                        $conditionGuide[trim($tmp[0])] = [$temp[0], explode(')',$temp[1])[0]]; 
                    }

                    // need to process condition format of value
                    $volumeCondition = null;
                    $volumeValue = null;
                    if(strpos($existingFilterValues,'/')){ // gte/100
                        $volumeSplitValues = explode('/',$existingFilterValues);

                        $volumeCondition = $volumeSplitValues[0];
                        $volumeValue = $volumeSplitValues[1];
                    }

                    if( strpos($existingFilterValues,'than') || 
                        strpos($existingFilterValues,'or') || 
                        strpos($existingFilterValues,'to')){ // greater than or equal to 100
                        $volumeSplitValues = explode(' ',$existingFilterValues);
                        
                        $volumeCondition = implode(' ',array_slice($volumeSplitValues,0,count($volumeSplitValues)-1));
                        $volumeValue = $volumeSplitValues[count($volumeSplitValues)-1];
                    }

                    if($volumeCondition && isset($volumeValue)){
                        if(!empty($conditionGuide[$volumeCondition]) && $conditionGuide[$volumeCondition]){
                            $initVal = ['ids'=>$volumeCondition,'texts'=>(implode('(',$conditionGuide[$volumeCondition])).')'];
                        }
                        else{
                            foreach($conditionGuide as $key=>$value){
                                if(trim($value[0]) == trim($volumeCondition)){
                                    $initVal = ['ids'=>  $key,'texts'=>(implode('(',$value)).')'];
                                    break;
                                }
                            }
                        }
                    }
                }

                if($confVar['input_field'] == 'selection' && !in_array($confVar['variable_abbrev'],['UNIT','VOLUME'])){
                    // retrieve data corresponding to existing filter values
                    $filter = [
                        ['abbrev' => $confVar['variable_abbrev'],
                        'values' => $existingFilterValues,
                        'config_info' => $confVar]
                        ];

                    $limit = NULL;
                    $offset = 0;
                    $q = ''; 
                    
                    $initialVal = $this->apiCallHandler($userId,$filter,$confVar,$limit,$offset,$q);
                    
                    if(isset($initialVal) && isset($initialVal['data']) ){
                        $initialVal = $initialVal['data'];
                    
                        if($initialVal != null && !empty($initialVal)){
                            $idString = implode(',',array_column($initialVal,'id'));
                            $textString = implode('/',array_column($initialVal,'text'));
                            
                            $initVal = ['ids'=>$idString,'texts'=>$textString];
                        }
                    }
                }
            }
        }

        return $initVal;
    }

    /**
     * Returns generated Select2 html fields
     *
     * @param String name
     * @param Integer variable id
     * @param Array data 
     * @param Array val
     * @param String required string 
     * @param String size
     * @param Boolean removeFunction
     * @param Array initVal
     * @param String input type 
     * @param Array variable config info
     * @return HTML htmlElement
     */
    public static function generateHTMLElements($name, $varId, $data, $val, $reqStr, $label, $size, $removeFxn, $initVal, $inputType,$varConfigInfo)
    {   
        $dataUrl = Url::home() . '/seedInventory/' . Url::to('find-seeds/get-filter-data');

        $pluginOptions = [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'placeholder' => 'Search query parameter value',
            'minimumResultsForSearch' => 1,
            'language' => [
                'errorLoading' => new JSExpression("function(){ return 'Waiting for results...'; }"),
            ],'ajax' => [
                'url' => $dataUrl,
                'dataType' => 'json',
                'delay'=> 250,
                'type' => 'POST',
                'data'=> new JSExpression(
                    'function (params) {

                        var filters = $(".find-seeds-filter").serializeArray();
                        var id = $(this).attr("id");
                        var page = params.page != null ? params.page : 1;
                        var q = params.term != null ? params.term : "";
                        
                        return {
                            q: q, 
                            id: id,
                            page: page,
                            filters: JSON.stringify(filters)
                        }
                    }
                    '
                ),
                'processResults' => new JSExpression(
                    'function (data, params) {
                        
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                            more: (params.page * 5) < data.totalCount
                            },
                        };
                    }'
                ),
                'cache' => true
            ],
            'templateResult' => new JSExpression(
                'function formatRepo (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }

                    return repo.text;
                }'
            ),
            'templateSelection' => new JSExpression(
                'function formatRepoSelection (repo) {
                    return repo.text;
                }'
            ),
        ];
        
        $selectSize = 'col-md-12';
        $volumeValue = null;
        
        if ($varId == 'volume-filter-selected'){
            $selectSize = 'col-md-8';
            $volumeValue = '<span class="col-md-4">'.Html::input(
                'text',
                'filter[VOLUME_VALUE]',
                $val,
                [
                    'id' => $varId . '-value',
                    'class' => 'variable-filter input-text-filter'
    
                ]
            ).'</span>';
        }

        if ($val == 0 || empty($val) || $val == null) {
            $val = null;
        };

        $initValuesText = '';
        $initValues = null;
        $data = null;
        $val = null;
        // format initial values and display texts
        if(isset($initVal['ids']) && !empty($initVal['ids']) && $initVal != null){
            $data = null;
            $outputFormat = $varId == 'experiment_type-filter' || $varId == 'unit-filter-selected' ? 'strval' : 'intval';

            $initValText = $inputType == 'single' ? $initVal['texts'] : [$initVal['texts']];
            $initValId = $inputType == 'single' ? $initVal['ids'] : [$initVal['ids']];

            $initValuesText = strpos($initVal['texts'],'/')!== false ? explode('/',$initVal['texts']) : $initValText;
            $initValues = strpos($initVal['ids'],',')!== false ? array_map($outputFormat, explode(',', $initVal['ids'])) : $initValId;

            $val = is_array($initValues) == true ? implode(',',$initValues) : $initValues;
        }
       
        $value = Select2::widget([
            'name' => $name,
            'id' => $varId,
            'data' => $data,
            'initValueText' => $initValuesText,
            'value' => ($varId == 'volume-filter-selected') ? $val : $initValues,
            'options' => [
                'tags' => true,
                'class' => 'select2-input-validate' . $reqStr . ' variable-filter ',
                'multiple' => ($inputType === 'single' ? false : true),
                'minimumResultsForSearch' => 1,
            ],
            'pluginOptions' => $pluginOptions,
            'pluginEvents' => [
                "change" => "
                        function() { 

                            if($(this).val()!=''){
                                var value = $(this).val()+ '';
                                var values = value.split(',');
                                var val = '', counter = 1;

                                $.each(values,function(i,v){ 
                                    val = val+v;
                                    
                                    if(counter < values.length){
                                        val = val+',';
                                    }
                                    counter += 1;
                                })
                                $('#'+this.id+'-hidden').val(val);
                            }
                            else{
                                $('#'+this.id+'-hidden').val($(this).val());
                            }
                            
                        }",
            ],
            'showToggleAll' => ($varId === 'study_name-filter' || $varId === 'experiment_name-filter' || empty($data) || $data == null ? false : true),
        ]);

        $hidden = Html::input(
            'text',
            $name,
            !empty($val) ? $val : null,
            [
                'id' => $varId . '-hidden',
                'class' => 'select2-input-validate-hidden hidden variable-filter',
            ]
        );

        $removeSize = null;
        $remove = '';
        
        if ($removeFxn == true) {
            $remove = \macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', ''),
                'icon' => [
                    'name' => 'remove',
                    'position' => 'right',
                    'options' => [
                        'style' => 'margin-left:0px;',
                    ],
                ],
                'options' => [
                    'id' => $varId . '-remove-btn',
                    'class' => 'btn red '. $varId . '-remove-btn remove-btn',
                    'style' => 'padding-left:10px; padding-right:10px;',
                    'title' => 'Remove filter',
                ],
            ]);
            $selectSize = 'col-md-11';
            $removeSize = 'col-md-1';
        };
        $label = Html::tag('div', $label, ['class' => 'row', 'style' => 'margin-bottom:0px; margin-top:0px;','title'=>$varConfigInfo['field_description']]);
        $select = Html::tag('div', $hidden.$value.$volumeValue, ['class' => 'row ' . $selectSize, 'style' => 'padding-right:0px; margin-bottom:5px;']);
        $remove = Html::tag('div', $remove, ['class' => '' . $removeSize, 'style' => 'margin-bottom:5px; padding-left:15px; padding-right:0px;']);

        $style = ($varId == 'program-filter' ? 'margin-bottom:0px; padding-left:5px;' : 'margin-top:0px; margin-bottom:5px; padding-left:5px;');
        $style .= ($size == ' col-md-' ? 'margin-left:20px;' : '');
        
        $htmlElement = Html::tag('div', $label . $select . $remove, ['class' => 'control-label row' . $size, 'style' => $style]);

        return $htmlElement;
    }

    /**
     * Returns generated input textfield variable filter fields
     *
     * @param String abbrev
     * @param String name
     * @param Integer variable ID
     * @param Array value
     * @param String reqStr
     * @param String label
     * @param String size
     * @param Boolean removeFxn
     * @param Array varConfigInfo
     * @param Array initVal
     * @return HTML htmlElement
     */
    public static function generateHTMLInputTextFieldElements($abbrev, $name, $varId, $val, $reqStr, $label, $size, $removeFxn,$varConfigInfo,$initVal)
    {
        $initialValue = is_array($initVal) ? implode(',',$initVal) : $initVal; 

        $caption = ($varId == 'plotno-filter-selected' || $varId == 'entno-filter-selected' || $varId == 'rep-filter-selected'? 'Integer or range e.g. 1 or 2-10, comma-separated.' : '');
        $value =  ($varId ==  'hvdate_cont-filter-selected' ? '<div class ="variable-filter" ><input type="text" class="datepicker-filter" id="' . $varId . '"></input></div>' :  '<div class="'. $reqStr . '" style="margin: 0 0 2px 0; "><input type="text" class="variable-filter input-filter input-'.$varId.'" id="' . $varId . '" placeholder="'.$caption.'" value="'.$initialValue.'" ></input></div>');

        $hidden = Html::input(
            'text',
            $name,
            '',
            [
                'id' => $varId . '-hidden',
                'class' => 'input-text-hidden hidden variable-filter input-text-filter',

            ]
        );

        $selectSize = 'col-md-12';
        $removeSize = null;
        $remove = '';

        if ($removeFxn == true) {
            $remove = \macgyer\yii2materializecss\widgets\Button::widget([
                'label' => Yii::t('app', ''),
                'icon' => [
                    'name' => 'remove',
                    'position' => 'right',
                    'options' => [
                        'style' => 'margin-left:0px;',
                    ],
                ],
                'options' => [
                    'id' => $varId . '-remove-btn',
                    'class' => 'btn red '. $varId . '-remove-btn remove-btn',
                    'style' => 'padding-left:10px; padding-right:10px;',
                ],
            ]);
            $selectSize = 'col-md-11';
            $removeSize = 'col-md-1';
        };

        $style = 'margin-bottom:5px; padding-left:5px; margin-left:0px;';
        $style .= ($size == ' col-md-12' ? 'margin-left:0px;' : '');

        $label = Html::tag('div', $label, ['class' => 'row', 'style' => 'margin-bottom:5px;', 'title'=>$varConfigInfo['field_description']]);
        $select = Html::tag('div', $value . $hidden, ['class' => 'row ' . $selectSize, 'style' => 'padding-right:0px; margin-bottom:5px;']);
        $remove = Html::tag('div', $remove, ['class' => '' . $removeSize, 'style' => 'margin-bottom:5px; padding-left:15px; padding-right:0px;']);
        $htmlElement = Html::tag('div', $label . $select . $remove, ['class' => 'control-label  row ' . $size, 'style' => $style]);

        return $htmlElement;
    }

    /**
     * Generates fields params from existing variable filter values for search filter
     * @param Integer userId 
     * @param Array existingFilters 
     * @param Array targetFilter
     * @param Boolean dependencyApplicable
     * @param String q
     * @return Array $params with fields and filters for API search
     */
    public function generateSearchFieldsParams($userId,$existingFilters,$targetFilter,$dependencyApplicable,$q)
    {
        $applicableFilters = ['PROGRAM','EXPERIMENT_YEAR','EVALUATION_STAGE','EXPERIMENT_NAME','LOCATION','LOCATION_CODE'];
        $searchFields='';
        $searchParams=[];

        $paramsInclusion = [
            "experiment" => ['PROGRAM','EXPERIMENT_YEAR','EXPERIMENT_TYPE','OCCURRENCE_NAME','EVALUATION_STAGE','EXPERIMENT_NAME','LOCATION','LOCATION_CODE','SEASON'],
            "package" => ['PROGRAM','FACILITY','SUB_FACILITY','VOLUME','UNIT']
        ];

        $userId = !empty($this->userModel->getUserId()) ? $this->userModel->getUserId() : $userId;
        $programs = $this->programModel->searchAll();

        $userPrograms = isset($programs['data']) && !empty($programs['data']) ? array_values(array_map(function($program){
            return [
                'id'=>$program['programDbId'],
                'code'=>$program['programCode'],
                'name'=>$program['programName'],
                'description'=>$program['description']
            ];
        },$programs['data'])) : [];
        
        $existingFilterAbbrevs = !empty($existingFilters) && $existingFilters != null ? array_column($existingFilters,'abbrev') : []; 

        // add column filters
        if($dependencyApplicable == true){
            foreach($existingFilters as $existingFilter){
                $applicableFilter = $existingFilter['config_info']['basic_parameter'] == true || ($existingFilter['config_info']['basic_parameter'] == false && in_array($existingFilter['abbrev'],$applicableFilters));
                
                if(($existingFilter['values'] != "" || !empty($existingFilter['values'])) && $existingFilter['abbrev'] != 'VOLUME' && $applicableFilter == true){ // not null or empty
                    // transform values of existing filters to string separated with vertical bar
                    $columnFilterValues = is_array($existingFilter['values'])==true ? implode('|equals ',$existingFilter['values']) : str_replace(',','|equals ',$existingFilter['values']) ;
                    
                    // set column filter header
                    $columnFilter = ($existingFilter['config_info']['output_id_value'] == 'id' ? $existingFilter['config_info']['target_table'].'Db'.ucwords($existingFilter['config_info']['output_id_value']) : lcfirst(str_replace(' ','',ucwords(str_replace('_',' ',$existingFilter['config_info']['output_id_value'])))));
                    $outputColumnFilter = $existingFilter['config_info']['target_table'].'.'.$existingFilter['config_info']['output_id_value'];
                    
                    $outputColumnFilter = $existingFilter['abbrev'] == 'SUB_FACILITY' ? 'container.parent_facility_id' : $outputColumnFilter;
                    $outputColumnFilter = $existingFilter['abbrev'] == 'FACILITY' ? 'container.root_facility_id' : $outputColumnFilter;

                    // determine params inclusion of target filter
                    $targetFiltersParamsInc = in_array($targetFilter['variable_abbrev'],$paramsInclusion['package']) ? 'package' : 'experiment';
                    $toAddParam = in_array($existingFilter['abbrev'],$paramsInclusion[$targetFiltersParamsInc]);

                    // if included in params inclusion of target filter call, add new filter, else skip
                    if($toAddParam){
                        // add existing filter to fields parameter
                        $searchFields .= $searchFields == "" ? "" : "|";
                        $searchFields .= $outputColumnFilter.' AS '.$columnFilter;
                        
                        // add existing filter to search param as column filter
                        $searchParams[$columnFilter] = "equals ".$columnFilterValues;

                        // validate existing filter value for PROGRAM - check against list of valid filters
                        if($existingFilter['abbrev'] == 'PROGRAM'){
                            $searchVal = is_array($existingFilter['values'])==true ? $existingFilter['values'] : explode(',',$existingFilter['values']);
                            $ids = array_map('strval',array_column($userPrograms,'id'));
                            $flag = false;
                            foreach($searchVal as $programVal){
                                if(!in_array($programVal,$ids)){
                                    $flag = true;
                                }
                            }
                            $searchParams['programDbId'] = $flag == true ? 'equals '.implode('|equals ',array_column($userPrograms,'id')) : $searchParams['programDbId'];
                        }
                    }
                }
            }
        }

        // if target filter has defined allowed values
        if($targetFilter['allowed_values'] != "" && isset($targetFilter['allowed_values']) && $targetFilter['variable_abbrev'] != 'SUB_FACILITY'){ // for existing filters of variables with pre-existing conditions
            $existingFilterConditions = explode('|',$targetFilter['allowed_values']);
            foreach($existingFilterConditions as $filterCondition){
                $targetTable = $targetFilter['target_table'];

                // extract conditions
                $condition = explode(':',$filterCondition);
                $condition[0] = trim($condition[0]);
                $condition[1] = str_replace(',','|',trim($condition[1]));

                $conditionAlias = lcfirst(str_replace(' ','',ucwords(str_replace('_',' ',$condition[0]))));
                // add to fields and column filters
                $searchFields .= $searchFields == "" ? "" : "|";
                $searchFields .= $targetTable.'.'.trim($condition[0]).' AS '.$conditionAlias;

                $searchParams[$conditionAlias] = $condition[1];
            }
        }

        if(!empty($q) && $q != "" && $q != null){
            $searchParams['text'] = $q.'%';
        }

        // if no existing program filter, but target filter is program
        if(!in_array('PROGRAM',$existingFilterAbbrevs) && $targetFilter['variable_abbrev'] == 'PROGRAM'){
            $searchParams['id'] = implode('|',array_column($userPrograms,'id'));
        }

        // add target filter to fields parameter as id and text
        if(!empty($targetFilter)){
            $outputTargetTable = $targetFilter['target_table'];

            $searchFields .= $searchFields == "" ? "" : "|";
            $searchFields .= '"' . $outputTargetTable.'".'.$targetFilter['output_id_value'].' AS id'.'|"'.$outputTargetTable.'".'.$targetFilter['output_display_value'].' AS text';
        }
        $searchParams['fields']=$searchFields;

        // add distinctOn to remove redundant api call results - issues if target abbrev is FACILITY, EXPERIMENT TYPE, SEASON
        $searchParams['distinctOn']='id';

        return $searchParams;
    }

    /**
     * Retrieves matching and filtered package-level values using generated fields params
     * @param Array searchParams
     * @param Array targetFilter
     * @param String resourceEndpoint
     * @param String resourceMethod
     * @param Integer limit
     * @param Integer offset
     * @return Array $filterValues with corresponding id and text values 
     */
    public function extractSearchFilterValues($searchParams,$targetFilter,$resourceEndpoint, $resourceMethod,$limit,$offset)
    {
        $searchData = [];
        $searchResult = null;
        $count = 0;

        $addedParamFields = ['PROGRAM','FACILITY','SUB_FACILITY','VOLUME','UNIT'];
        
        $method = isset($targetFilter['api_resource_method']) && $targetFilter['api_resource_method'] != "" ? $targetFilter['api_resource_method'] : 'POST';
        $endpoint = isset($targetFilter['api_resource_endpoint']) && $targetFilter['api_resource_endpoint'] != "" ? $targetFilter['api_resource_endpoint'] : 'packages-search';

        $searchParams['id'] = "not equals null";

        $sort = '?sort='.($targetFilter['search_sort_column'] == 'display_value' ? 'text' : 'id').':'.$targetFilter['search_sort_order'];

        if(in_array($targetFilter['variable_abbrev'],$addedParamFields)){
            $sort .= '&dataLevel=seed';

            if(isset($searchParams['fields']) && !empty($searchParams['fields'])){
                $searchParams['fields'] = $searchParams['fields'].'|package.id AS packageDbId|seed.id AS seedDbId|seed.germplasm_id AS germplasmDbId';
                $searchParams['distinctOn'] = 'id';
            }
        }
        
        $endPointParams = json_encode($searchParams);
        
        if($targetFilter['variable_abbrev'] == 'PROGRAM'){
            $programFilters = $this->getProgramFilterOptions($searchParams,$limit,$offset);
            $searchResult = !empty($programFilters) && isset($programFilters['results']) ? $programFilters['results'] : [];
            $count = !empty($programFilters) && isset($programFilters['totalCount']) ? $programFilters['totalCount'] : 0;
        }
        else if(in_array($targetFilter['variable_abbrev'],['PACKAGE_UNIT','UNIT'])){
            $unitFilters = $this->getUnitFilterOptions($searchParams,$limit,$offset);
            $searchResult = !empty($unitFilters) && isset($unitFilters['results']) ? $unitFilters['results'] : [];
            $count = !empty($unitFilters) && isset($unitFilters['totalCount']) ? $unitFilters['totalCount'] : 0;
        }
        else{
            $resultLimits = "";
            if($resourceEndpoint == ""){
                $resultLimits = ($limit == NULL || $limit == 0 ? '&page='.($offset+1) : '&limit='.$limit.'&page='.intval(($offset/$limit)+1));
            }
            
            $resourceCall = Yii::$app->api->getResponse($method,$endpoint.$sort.$resultLimits,$endPointParams);
            if(isset($resourceCall) && $resourceCall['status'] == 200 && isset($resourceCall['body']['result']['data'])){
                $data = $resourceCall['body']['result']['data'];
                $totalPageCount = $resourceCall['body']['metadata']['pagination']['totalPages'];
                $count =  $resourceCall['body']['metadata']['pagination']['totalCount'];

                $searchResult = array_values(array_map("unserialize", array_unique(array_map("serialize", $data))));

                // remove results data with null or "" values for id and text
                $searchResult = array_values(array_filter($searchResult,function($dataPoint){
                    if($dataPoint['id'] != "" && $dataPoint['text'] != "" && $dataPoint['id'] != null && $dataPoint['text'] != null){
                        return $dataPoint;
                    }
                }));
            }
        }

        $searchData = [
            'data'=>$searchResult,
            'count'=>$count
        ];
        return $searchData;
    }

    /**
     * Retrieve program filter options
     * @param Array searchParams current search parameters
     * @return Array results filter options
     */
    public function getProgramFilterOptions($searchParams,$limit,$offset)
    {
        $results = [];
        $totalCount = 0;

        $userId = $this->userModel->getUserId();
        $userPrograms = $this->userModel->getUserPrograms($userId);

        if(isset($userPrograms) && !empty($userPrograms)){
            // format results
            if(!empty($searchParams) && isset($searchParams['programDbId'])){
                $currentProgramFilters = str_replace('equals ','',$searchParams['programDbId']);
                $currentProgramFilters = explode('|',$currentProgramFilters);

                foreach($currentProgramFilters as $currentProgramFilter){
                    foreach($userPrograms as $program){
                        if($program['id'] == intval($currentProgramFilter)){
                            array_push($results,$program);
                            break;
                        }
                    }
                }
                $totalCount = count($results);
            }
            else if(!empty($searchParams) && isset($searchParams['text'])){
                $currentProgramFilter = str_replace('%','',$searchParams['text']);

                foreach($userPrograms as $program){
                    if(strpos(strtolower($program['text']),strtolower($currentProgramFilter)) !== false && !in_array($program,$results)){
                        array_push($results,$program);
                    }
                }

                $totalCount = count($results);
            }
            else{
                $page = intval($offset/$limit);
                $userProgramsChunks = array_chunk($userPrograms,$limit);

                $results = $userProgramsChunks[$page];
                $totalCount = count($userPrograms);
            }
        }

        return [
            'results' => $results,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Retrieve unit scale values
     * 
     * @param Array $searchParams search parameters
     * @param String $limit search limit
     * @param String $offset search offset
     * 
     * @return mixed
     */
    public function getUnitFilterOptions($searchParams,$limit,$offset)
    {
        $variableDbId;
        $selectedUnit = "";
        $extractedUnits = [];
        
        if(!empty($searchParams) && isset($searchParams['unit'])){
            $selectedUnit = str_replace('%','',$searchParams['unit']);
        }

        $result = $this->variableModel->getVariableByAbbrev('PACKAGE_UNIT');
        if(isset($result) && !empty($result) && isset($result["variableDbId"]) && !empty($result["variableDbId"])){
            $variableDbId = $result["variableDbId"];

            $scales = $this->variableModel->getVariableScales($variableDbId);

            foreach($scales['scaleValues'] as $scaleValue){
                if( strpos(strtolower($scaleValue['value']),strtolower($selectedUnit)) !== false && 
                    !in_array($scaleValue['value'],$extractedUnits)){
                    array_push(
                        $extractedUnits,
                        ['id' => strtolower($scaleValue['value']),'text' => $scaleValue['displayName']]
                    );
                }
            }
        }

        return [
            'results' => $extractedUnits,
            'totalCount' => count($extractedUnits)
        ];
    }


    /**
     * Calls necessary api call to handle search
     *
     * @param Integer userId
     * @param Array variableFilters
     * @param Array variableInfo
     * @param Integer limit
     * @param Integer offset
     * @param String q
     * @return Array search data results
     */
    public function apiCallHandler($userId,$variableFilters,$variableInfo,$limit,$offset,$q)
    {
        $applicableFilters = ['EXPERIMENT_YEAR','EVALUATION_STAGE','EXPERIMENT_NAME','LOCATION','LOCATION_CODE'];

        if(!isset($variableInfo) || empty($variableInfo)){
            return ['data'=>[], 'count'=>0];
        }

        $dependencyApplicable = in_array($variableInfo['variable_abbrev'],$applicableFilters) || $variableInfo['basic_parameter'] == 'true' ? true : false;
        $searchParams = $this->generateSearchFieldsParams($userId,$variableFilters,$variableInfo,$dependencyApplicable,$q);
        $searchData = [];

        if($variableInfo['default_value'] != ""){
            $defaultValues = explode(',',$variableInfo['default_value']);

            foreach($defaultValues as $key=>$value){
                $option = explode(':',$value);
                $defaultOptions[] = [
                    'id'=> $option[0],
                    'text'=> $option[1]
                ];
            }
            $searchData = ['count'=>count($defaultOptions),'data'=>$defaultOptions];
        }
        else{
            $searchData = $this->extractSearchFilterValues($searchParams,$variableInfo,'','',$limit,$offset);
        }

        return $searchData;
    }

    /**
     * Extract variable values and configuration information for existing filters
     * @param Array existingFilters 
     * @param Array configVar 
     * @param Integer id
     * @return Array variableFilters
     */
    public function extractVariableFilters($existingFilters,$configVar,$id)
    {
        $variableFilters = [];

        foreach($existingFilters as $value){
            $configInfo = [];

            $valArr = json_decode(json_encode($value), true);
            
            $valueName = $valArr['name'];
            $valueValue = $valArr['value'];

            $name = count(explode('[',$valueName))>1 ? explode(']',explode('[',$valueName)[1])[0] : $valueName;
            foreach($configVar as $val){
                if($val['variable_abbrev'] == $name){
                    $configInfo = $val;
                    break;
                }
            }
            if( isset($configInfo) && !empty($configInfo) ){
                if($valueValue != "" && $valueName != "_csrf" && $valueName != "filter[INPUT_VALUE]" && 
                    $valueName != "filter[INPUT_FIELD]" && $name != strtoupper(explode('-',$id)[0])){
                    $addedFilters = array_column($variableFilters,'abbrev');
                    if(strpos($valueName,'[]') == false && $valueValue){
                        if(in_array($name,$addedFilters)){ // overwrite duplicate filter
                            $variableFilters[array_search($name,$addedFilters)] = ['abbrev'=>$name,'values'=>$valueValue,'config_info' => $configInfo];
                        }
                        else{
                            $variableFilters[] = ['abbrev'=>$name,'values'=>$valueValue,'config_info' => $configInfo];
                        }
                    }
                }
            }       
        }

        return $variableFilters;
    }

    /**
     * Extract and format query params for storage
     * @param Integer userId
     * @param Array params
     * @param Array configVars
     * @param String inputListType
     * @return array display values query param values and id
     */
    public function extractQueryParamsValuesForStorage($userId,$params,$configVars,$inputListType)
    {

        $displayValues = '';
        $queryParamValues = null;
        $queryParamIds = null;

        foreach($params as $key=>$param){
            $paramsConfig[$key] = [
                'abbrev'=>$param['name'],
                'values'=>$param['value'],
                'config_info'=>[],
                'input_list'=>false
            ];
            $queValue = [];

            if(in_array(strtolower($param['name']),$inputListType) || $param['name'] == 'NAME'){
                $paramsConfig[$key]['input_list'] = true;
                $queValue['ids'] = $param['value'];
                $queValue['texts'] = $param['value'];
            }
            else{
                foreach($configVars as $index=>$content){
                    if($content['variable_abbrev']==$param['name'] && $content != ""){
                        $paramsConfig[$key]['config_info'] = $content;
                        $queryParamValue = null;

                        if( $paramsConfig[$key]['input_list'] == true || $paramsConfig[$key]['abbrev'] == 'NAME' || 
                            $paramsConfig[$key]['abbrev'] == 'VOLUME' || $paramsConfig[$key]['config_info']['input_field'] == 'input field'){
                            $queryParamValue = ['texts' => $paramsConfig[$key]['values']];

                            $queValue['texts'] = $queryParamValue['texts'];
                            $queValue['ids'] = $queValue['texts'];
                        }
                        else{

                            $filter = [$paramsConfig[$key]];
                            $q = '';

                            if($paramsConfig[$key]['abbrev'] == 'UNIT'){
                                $q = isset($paramsConfig[$key]['values']) && !empty($paramsConfig[$key]['values']) ? $paramsConfig[$key]['values'] : '';
                            }

                            $queryParamValue = $this->apiCallHandler($userId,$filter,$content,NULL,0,$q);

                            if(isset($queryParamValue['data']) && $queryParamValue['count']>0){
                                $queValue['ids'] = implode(',',array_column($queryParamValue['data'],'id'));
                                $queValue['texts'] = implode(',',array_column($queryParamValue['data'],'text'));
                            }

                            if((!isset($queValue['ids']) && $paramsConfig[$key]['abbrev'] == 'VOLUME') || 
                                (!isset($queValue['ids']) && !isset($paramsConfig[$key]['config_info']['allowed_values']))){
                                $queValue['ids'] = $queValue['texts'];
                            }
                        }
                        break;
                    } 
                }
            }

            if(!empty($queValue) && isset($queValue['texts']) && !empty($queValue['texts']) && isset($queValue['ids']) && !empty($queValue['ids']) ){
                $displayValues .= $paramsConfig[$key]['abbrev'].':'.$queValue['texts'].'::';
                $queryParamValues[] = [$paramsConfig[$key]['abbrev']=>$queValue['texts']];
                $queryParamIds[] = [$paramsConfig[$key]['abbrev']=>$queValue['ids']];
            }
            
        }
        return [
            'displayValues'=>$displayValues,
            'queryParamValues'=>$queryParamValues
            ,'queryParamIds'=>$queryParamIds
        ];
    }
}