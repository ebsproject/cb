<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\Variable;
use app\models\User;
use app\models\Program;
use app\models\ScaleValue;
use ChromePhp;
use Monolog\Formatter\ChromePHPFormatter;

class ProgramPackageTypeModel extends  \yii\db\ActiveRecord
{

    public $program;
    public $package_type;
    // /**
    //  * {@inheritdoc}
    //  */
    public function rules()
    {
        return [
            [['program_id', 'scale_value_id', 'creator_id'], 'required'],
            [['program_id', 'scale_value_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['program_id', 'scale_value_id', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log', 'package_type', 'program'], 'safe'],
            [['is_void'], 'boolean'],
            [['program_id', 'scale_value_id'], 'unique', 'targetAttribute' => ['program_id', 'scale_value_id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['scale_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => ScaleValue::className(), 'targetAttribute' => ['scale_value_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * Save record
     * @param  array $params program package type fields
     * @return array response
     */

    public static function saveRecord($params)
    {
        extract($params);
        $userModel = new User();
        $userId = $userModel->getUserId();

        foreach ($package_type_list as $val) {

            $sqlCheck = "Select count(1) from master.program_scale_value where program_id={$program_id} and scale_value_id={$val}";
            $isExist = Yii::$app->db->createCommand($sqlCheck)->queryScalar();

            if (!$isExist) {

                $sql = "INSERT INTO master.program_scale_value(program_id, scale_value_id, creation_timestamp, creator_id) values (
                {$program_id},
                {$val}, 
                'NOW()', 
                {$userId})";

                Yii::$app->db->createCommand($sql)->execute();
            }
        }

        return [
            "success" => true,
            "message" => "Successfully created record."
        ];
    }


    /**
     * Update record
     * @param  array $params program package type fields
     * @return array response
     */

    public static function updateRecord($params)
    {
        extract($params);
        $userModel = new User();
        $userId = $userModel->getUserId();

        $sql = "Delete from master.program_scale_value where program_id={$program_id}";
        Yii::$app->db->createCommand($sql)->execute();

        foreach ($package_type_list as $val) {

            $sql = "INSERT INTO master.program_scale_value(program_id, scale_value_id, creation_timestamp, creator_id) values (
                {$program_id},
                {$val}, 
                'NOW()', 
                {$userId})";

            Yii::$app->db->createCommand($sql)->execute();
        }

        return [
            "success" => true,
            "message" => "Successfully created record."
        ];
    }




    /**
     * Delete unit conversion
     * @param  array $params program package type id
     * @return array program_scale_value record
     */

    public static function deleteRecord($params)
    {
        $ids = explode(",", $params['id']);

        foreach ($ids as $id) {

            $sql = "Delete from master.program_scale_value where program_id={$id}";
            Yii::$app->db->createCommand($sql)->execute();
        }
        return [
            "success" => true,
            "message" => "Successfully deleted record."
        ];
    }

    /**
     * Get unit conversion 
     * @param  array $params program package type id
     * @return array scale_conversion record
     */

    public static function getList($params)
    {

        extract($params);

        $sql = "SELECT program_id,t2.name,
        (select array_to_string(array_agg(t4.id), ',')  as package_type 
         from master.program_scale_value as t3 left join master.scale_value as t4 on t3.scale_value_id=t4.id 
     where t3.program_id=t1.program_id group by t3.program_id limit 1) as package_type
     FROM master.program_scale_value as t1 LEFT JOIN master.program as t2 on program_id=t2.id where program_id={$id} group by program_id,t2.name order by t2.name";


        $programPackageTypeList = Yii::$app->db->createCommand($sql)->queryOne();

        return [
            "program_id" => $programPackageTypeList['program_id'],
            "package_type_list" => $programPackageTypeList['package_type']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains the different filter optiosn
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $this->load($params);
        $query = [];
        $queryCondition = '';

        if (isset($this->program) && !empty($this->program)) {
            $query[] = " id =" . $this->program;
        }

        if (isset($this->package_type) && !empty($this->package_type)) {
            $query[] = " package_type ilike '%" . $this->package_type . "%'";
        }

        $queryCondition = implode($query, ' and ');
        if (!empty($queryCondition)) {
            $queryCondition = ' (' . $queryCondition . ")";
        }

        return ProgramPackageTypeModel::getProgramPackageDataBrowser($queryCondition);
    }

    /**
     * Retrieve list of patterns
     * @return SqlDataProvider Data Provider
     */
    public static function getProgramPackageDataBrowser($condition)
    {

        return ProgramPackageTypeModel::getDataProvider(null, null, $condition);
    }



    /**
     * Return data provider 
     * @param String $selectSql columns defined
     * @param String $rawSql the raw sql to be converted into a data provider
     * @param string $condition Sql condition
     * @return SqlDataProvider $dataProvider data object for browser
     **/
    public static function getDataProvider($rawSql, $selectSql, $condition)
    {

        if ($condition != "") {
            $condition = ' where ' . $condition;
        }

        $sql = "select * from (select program_id as id,t2.name as program,
            (select array_to_string(array_agg(t4.display_name), ' , ')  as package_type 
            from master.program_scale_value as t3 left join master.scale_value as t4 on t3.scale_value_id=t4.id 
            where t3.program_id=t1.program_id group by t3.program_id limit 1) as package_type
            from master.program_scale_value as t1 left  join master.program as t2 on program_id=t2.id where t1.is_void=false group by program_id,t2.name) as tbl {$condition} ";


        $countSql = "select  count(distinct program_id) from master.program_scale_value";

        $totalCount = Yii::$app->db->createCommand($countSql)->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'program',
                    'package_type'
                ],
                'defaultOrder' => [
                    'program' => 'SORT_ASC'
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Get the list of programs for select2 widget
     * @return array program list
     */
    public function getProgramList()
    {
        $varList = Yii::$app->db->createCommand("select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false and id not IN (select program_id from master.program_scale_value group by program_id)")->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }

    /**
     * Get the list of programs for select2 widget
     * @return array program list
     */
    public function getProgramListWithRecord()
    {
        $varList = Yii::$app->db->createCommand("select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false and id  IN (select program_id from master.program_scale_value group by program_id)")->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }

    public function getProgramListAll()
    {
        $varList = Yii::$app->db->createCommand("select id,abbrev,name||' ('||abbrev||')' as text from master.program where is_void=false")->queryAll();
        $programArr = [];
        foreach ($varList as $p) {
            $programArr[$p['id']] = $p['text'];
        }
        return $programArr;
    }
    /*
    * Get the list of package type for select2 widget
    * @return array program list
    */
    public function getPackageType()
    {

        $variableSql = Yii::$app->db->createCommand("select scale_id from master.variable where abbrev='PACKAGE_TYPE'")->queryOne();
        $scale_id = $variableSql['scale_id'];

        // select * from master.scale_value where scale_id=1874;"

        $scaleValueList = Yii::$app->db->createCommand("select * from master.scale_value where scale_id={$scale_id}")->queryAll();
        $packageTypeArr = [];
        foreach ($scaleValueList as $p) {
            $packageTypeArr[$p['id']] = $p['value'];
        }


        return $packageTypeArr;
    }
}
