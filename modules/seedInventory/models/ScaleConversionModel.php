<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\models;

use Yii;
use yii\data\SqlDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use app\models\ScaleConversion;
use app\models\User;
use \app\modules\seedInventory\models\UnitOfVolumeModel;
use ChromePhp;


class ScaleConversionModel extends ScaleConversion
{


    /**
     * Save unit conversion
     * @param  array $params scale_conversion fields
     * @return array response
     */

    public static function saveConversion($params)
    {
        extract($params['value']);
        $userModel = new User();
        $userId = $userModel->getUserId();



        foreach ($data as $val) {

            $sql = "Select * from master.scale_conversion where source_unit_id={$val['source_unit_id']} and target_unit_id={$val['source_target_unit_id']}";
            $data = Yii::$app->db->createCommand($sql)->queryScalar();

            if (!$data) {


                $sql = "INSERT INTO master.scale_conversion(source_unit_id,  target_unit_id, conversion_value, creation_timestamp, creator_id) values (
                {$val['source_unit_id']},
                {$val['source_target_unit_id']},
                {$val['conversion_value']}, 
                'NOW()', 
                {$userId})";

                Yii::$app->db->createCommand($sql)->execute();
            } else {
                $sql = "UPDATE master.scale_conversion SET  conversion_value={$val['conversion_value']} where source_unit_id={$val['source_unit_id']} and target_unit_id={$val['source_target_unit_id']}";
                Yii::$app->db->createCommand($sql)->execute();
            }
        }
        return [
            "success" => true,
            "message" => "Successfully created pattern."
        ];
    }


    /**
     * Update unit conversion
     * @param  array $params scale_conversion fields
     * @return array response
     */

    public static function updateConversion($params)
    {
        extract($params['value']);
        $userModel = new User();
        $userId = $userModel->getUserId();



        foreach ($data as $val) {

            $sql = "Select * from master.scale_conversion where source_unit_id={$val['source_unit_id']} and target_unit_id={$val['source_target_unit_id']}";
            $data = Yii::$app->db->createCommand($sql)->queryScalar();

            $sql = "UPDATE master.scale_conversion SET  conversion_value={$val['conversion_value']} where source_unit_id={$val['source_unit_id']} and target_unit_id={$val['source_target_unit_id']}";
            Yii::$app->db->createCommand($sql)->execute();
        }
        return [
            "success" => true,
            "message" => "Successfully created pattern."
        ];
    }


    /**
     * Delete unit conversion
     * @param  array $params scale_id
     * @return array scale_conversion record
     */

    public static function deleteConversion($params)
    {
        // extract($params['value']);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $ids = explode(",", $params['id']);
        
        foreach ($ids as $id) {
            
             $sql = "Delete from master.scale_conversion where source_unit_id={$id}";
            // ChromePhp::log($sql);
            Yii::$app->db->createCommand($sql)->execute();
        }
        return [
            "success" => true,
            "message" => "Successfully deleted record."
        ];
    }

    /**
     * Get unit conversion 
     * @param  array $params scale_id
     * @return array scale_conversion record
     */

    public static function getConversion($params)
    {
        extract($params);
        $conversionValue = 0;

        // $sqlGetScaleValueId = Yii::$app->db->createCommand("select id from master.scale_value where scale_id=983 and value='{$unit}' and is_void=false")->queryScalar();


        $unitScale = Yii::$app->db->createCommand("select id, value,description from master.scale_value where scale_id=983 and is_void=false")->queryAll();
        $unitScaleArrList = array();
        $unitScaleParam = array();
        foreach ($unitScale as $u) {

            ChromePhp::log($u['id']);

            if ($u['id'] == $id) {

                $unitScaleParam = array(
                    'id' => $u['id'],
                    'value' => $u['value'],
                    'description' => $u['description']
                );
            }

            $conversionValueResult = Yii::$app->db->createCommand("select conversion_value from master.scale_conversion where source_unit_id={$id} and target_unit_id={$u['id']} and is_void=false")->queryOne();

            if ($conversionValueResult) {
                $conversionValue = $conversionValueResult['conversion_value'];
            } else {
                $conversionValue = 0;
            }


            $row = array(
                'id' => $u['id'],
                'value' => $u['value'],
                'conversion_value' => $conversionValue
            );
            array_push($unitScaleArrList, $row);
        }

        return [
            "unitScaleList" => $unitScaleArrList,
            "scaleValueArr" => $unitScaleParam
        ];
    }
}
