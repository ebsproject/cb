<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * List management controller for the `SeedInventory` module
 */

namespace app\modules\seedInventory\controllers;

use app\controllers\Dashboard;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use app\components\B4RController;
use app\models\Config;
use app\models\SeedStorageSearch;
use app\models\Package;
use app\models\User;
use app\models\Lists;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\modules\account\models\ListModel;
use app\modules\seedInventory\models\FindSeedListModel;
use app\modules\seedInventory\models\FindSeedDataBrowserModel;
use app\modules\seedInventory\models\WorkingList;

use Yii;

class ListManagementController extends B4RController
{
    protected $seedSearchBrowserModel;
    protected $seedSearchListModel;
    protected $workingListModel;

    public $listsModel;
    public $platformListModel;
    public $platformListMemberModel;
    public $userModel;

    public function __construct(
        $id,
        $module,
        $config,
        FindSeedDataBrowserModel $seedSearchBrowserModel,
        FindSeedListModel $seedSearchListModel,
        WorkingList $workingListModel
    ){
        $this->seedSearchBrowserModel = $seedSearchBrowserModel;
        $this->seedSearchListModel = $seedSearchListModel;
        $this->workingListModel = $workingListModel;

        $this->listsModel = \Yii::$container->get('app\models\Lists');
        $this->platformListModel = \Yii::$container->get('app\models\PlatformList');
        $this->platformListMemberModel = \Yii::$container->get('app\models\PlatformListMember');
        $this->userModel = \Yii::$container->get('app\models\User');

        parent::__construct($id, $module, $config);
    }

    /**
     * Method for updating working list into a final list; Creates a new working list
     * @return mixed will indicate if the request is successful
     */
    public function actionUpdateWorkingList() {
        $success = false;
        $message = '';
        $error = '';

        if (isset($_POST)) {
            $userModel = new User();
            $userId = $userModel->getUserId();
            $data = Yii::$app->request->post();

            $workingListRes = $this->workingListModel->getWorkingList($userId);
    
            if (!empty($workingListRes['listDbId']) && $workingListRes['memberCount'] > 0) {
                
                $listInfo = $data['listInfo'];
                $workingListDbId = $workingListRes['listDbId'];
                $parameters = [
                    'abbrev'=> trim($listInfo[2]['abbrev']),
                    'name'=> trim($listInfo[1]['name']),
                    'description'=>$listInfo[5]['description'],
                    'displayName'=>$listInfo[3]['display_name'],
                    'remarks'=>$listInfo[6]['remarks'],
                    'listUsage' => 'final list',
                    'status' => 'created',
                    'isActive' => 'true'
                ];
                
                $listName = trim($listInfo[1]['name']);
                $callResult = $this->listsModel->updateOne($workingListDbId, $parameters);

                //create new working list
                $record = [
                    'abbrev'=> 'WORKING_LIST_PACKAGE_'.$userId,
                    'name'=> 'Working List Package '.$userId,
                    'description'=>'user session package working list',
                    'displayName'=>'Working List Package',
                    'remarks'=>'user session package working list',
                    'type'=> 'package',
                    'status' => 'draft',
                    'isActive' => 'false',
                    'listUsage' => 'working list'
                ];
                $result = $this->seedSearchListModel->saveListBasicInformation($record);
                
                if(isset($callResult['status']) && $callResult['status'] == 200){
                    $success = true;
                }
                else{
                    $error = isset($callResult['status']) && $callResult['status'] == 400 ? ['Encountered error when saving information.'," Name or Abbrev already exists. Please provide a new one. "] : ['Encountered error when saving information.',"".$callResult['message']];
                }
    
                if ($success){
                    $message = "List: &nbsp; <b>".$listName."</b> &nbsp; successfully created.";
                } 
                else {
                    if (!empty($error)) {
    
                        $errorStr = '';
    
                        foreach ($error as $e) {
                            if (is_array($e)) {
                                $e = $e[0];
                            }
                            $errorStr = empty($errorStr) ? $errorStr : $errorStr . "<br/>";

                            if(isset($callResult['status']) && $callResult['status'] == 400){
                                $errorStr = $errorStr . " Name or Abbrev already exists. Please provide a new one. ";
                            }
                            else{
                                $errorStr = $errorStr . $e;
                            }
                        }
                        $message = $errorStr;
                    }
                    Yii::$app->session->setFlash('error', $errorStr);
                }

                return json_encode(["success" => $success, 'message' => $message, 'error' => $error,'listId'=> $workingListDbId,'listName' => $listName]);
    
            } else {
                return json_encode(["success" => false,'error' => [], 'message' => "List is empty."]);
            }

        }
    }

    /**
     * Save working list items to new list
     */
    public function actionSaveWorkingList(){
        //  retrieve current working list and list items
        $userId = $this->userModel->getUserId();
        $data = Yii::$app->request->post();

        $threshold = 2000;
        $workingListRes = $this->workingListModel->getWorkingList($userId);

        if (!empty($workingListRes['listDbId']) && intval($workingListRes['memberCount']) > 0) {
            $listInfo = $data['listInfo'];
            $listName = trim($listInfo[1]['name']);
            // create new list
            $record = [
                'abbrev'=> trim($listInfo[2]['abbrev']),
                'name'=> trim($listInfo[1]['name']),
                'description'=>$listInfo[5]['description'],
                'displayName'=>$listInfo[3]['display_name'],
                'remarks'=>$listInfo[6]['remarks'],
                'listUsage' => 'final list',
                'status' => 'created',
                'isActive' => 'true',
                'type' => 'package'
            ];

            $result = $this->seedSearchListModel->saveListBasicInformation($record);

            if(isset($result['status']) && !empty($result['status']) && $result['status'] == 200){
                $workingListDbId = $workingListRes['listDbId'];
                $totalMemberCount = intval($workingListRes['memberCount']);
                $newListDbId = $result['body']['result']['data'][0]['listDbId'];
                
                // update list items with new list_id of newly created list
                if($totalMemberCount > 0){
                    $workingListMembersRes = $this->workingListModel->getWorkingListMembers($workingListDbId);
                    $listMemberDbIdArr = !empty($workingListMembersRes) ? array_column($workingListMembersRes,'listMemberDbId') : [];
                    $listMemberDbIds = implode('|',$listMemberDbIdArr);
                    
                    $requestData = [
                        "listDbId" => $newListDbId
                    ];
            
                    $updateRes = $this->platformListMemberModel->bulkUpdate($listMemberDbIds, $newListDbId, $requestData);

                    return json_encode(["success" => true, 'message' => 'Successfully created new list '.$listName, 'error' => [],'listId'=> $newListDbId,'listName' => $listName,'bgprocess'=>false]);
                }
                else{
                    return json_encode(["success" => false,'error' => [], 'message' => "Working list is empty."]);
                }
            }
            else{
                return json_encode(["success" => false,'error' => ['Error encountered in saving new list.'], 'message' => "Error encountered in saving new list."]);
            }
        }
        else{
            return json_encode(["success" => false,'error' => [], 'message' => "Working list is empty."]);
        }
    }


    /**
     * Retrieves and saves list basic information
     * @param id
     * @return success error messages
     */
    public function actionCreateList($id = null){

        $model = $this->platformListModel;

		$success = false;
        $error = [];
        
        if(isset($_POST)){
            
            $data = Yii::$app->request->post();
            $listItemCount = $this->seedSearchListModel->getItemsCount();

            if($listItemCount > 0){
                if(isset($data['listInfo'])){
                
                    $listInfo = $data['listInfo'];

                    $parameters = [
                        'abbrev'=> trim($listInfo[2]['abbrev']),
                        'name'=> trim($listInfo[1]['name']),
                        'description'=>$listInfo[5]['description'],
                        'displayName'=>$listInfo[3]['display_name'],
                        'remarks'=>$listInfo[6]['remarks'],
                        'type'=> $listInfo[4]['type']
                    ];

                    $callResult = $this->seedSearchListModel->saveListBasicInformation($parameters);
                    
                    $listName = '';
                    $listId = ''; 
                    
                    if(isset($callResult['status']) && $callResult['status'] == 200){
                        $success = true;
                        $listName = $parameters['name'];
                        $listId = $callResult['body']['result']['data'][0]['listDbId'];
                    }
                    else{
                        $error = ['Encountered error when saving information.'];
                    }
        
                    if ($success){
                        $action = !empty($id) ?  'updated' : 'created';
                        Yii::$app->session->setFlash('success', "List: <b>".$model->name."</b> successfully ".$action.".");
                    } else {
                        if (!empty($error)) {
        
                            $errorStr = '';
        
                            foreach ($error as $e) {
                                if (is_array($e)) {
                                    $e = $e[0];
                                }
                                $errorStr = empty($errorStr) ? $errorStr : $errorStr . "<br/>";
                                $errorStr = $errorStr . "<br/>" . $e;
                            }
                        }
                        Yii::$app->session->setFlash('error', $errorStr);
                    }
                }

                return json_encode(["success" => $success,'error' => $error,'listId'=> $listId,'listName' => $listName]);
            }
            else{
                $success = false;
                $error = [];
                $message = "List is empty.";
                return json_encode(["success" => $success,'error' => $error, 'message' => $message]);
            }
        }
    }

    /**
     * Checks if list abbrev is valid
     * @param id
     * @return success error messages
     */
    public function actionValidateListAbbrev($id = null){

        $isValidAbbrev = true;
        $attr = 'abbrev';
        $message = '';

        if(isset($_POST)){
            
            $data = Yii::$app->request->post();
            $listAbbrev = $data['abbrev'];

            if(!empty($id)){
                $model = $this->listsModel->getOne($id);

                if(isset($model) && !empty($model)){
                    if(isset($model['abbrev']) && !empty($model['abbrev']) && $listAbbrev == $model['abbrev']){
                        $isValidAbbrev = $this->platformListModel->validateAbbrev($listAbbrev);
                    }
                }
                else{
                    Yii::$app->session->setFlash('error', 'A connection error occurred while loading the page. Please try again or file a report for assistance.');
                    $message = 'Error encountered in retrieving list data!';
                }
            }
            else{
                $isValidAbbrev = $this->platformListModel->validateAbbrev($listAbbrev);
            }

            if(!$isValidAbbrev){
                $message = "Name or Abbrev already exists. Please provide a new one.";
            }
        }
        else{
            $message = 'Notice! No valid values provided!';
        }

        return json_encode([
            'isValid' => $isValidAbbrev,
            'attr' => $attr,
            'message' => $message
        ]);
    }

    // validate seed ids
    public function actionValidateSeedIds(){

        $labels = '';
        $userModel = new User();
        $userId = $userModel->getUserId();
        
        $existingWorkingList = $this->seedSearchListModel->createWorkingList($userId);
        $newWorkingListSeedIds = [];
        $decodedString = json_decode($_POST['ids']);
        if(!empty($decodedString) && $decodedString!=null && $decodedString[0]!=""){
            $newWorkingListSeedIds = $this->seedSearchListModel->validateSeedIds($decodedString, $userId, $existingWorkingList);

            $labels = $newWorkingListSeedIds['duplicateListMembers'] == null ? '' : implode(',',$newWorkingListSeedIds['duplicateListMembers']);
        }
        
        return json_encode(['labels'=>$labels,'newList'=>$newWorkingListSeedIds,'success'=>true]);
    }

    public function actionGetSearchData(){
        $inputListOrder = false;
        if(Yii::$app->session['filterAbbrevs']!=null){
            $filterAbbrevs = Yii::$app->session['filterAbbrevs'];
            $searchData = $this->seedSearchBrowserModel->getSearchData(isset($_GET['FindSeedDataBrowserModel']) ? ['FindSeedDataBrowserModel' => $_GET['FindSeedDataBrowserModel']] : NULL, $filterAbbrevs, $inputListOrder);
        }
    }

    /**
     * Adds items to list
     * @return list html data
     */
    public function actionAddListItems(){
        $userId = $this->userModel->getUserId();
        $insertItemsStatus = false;

        // retrieve post values
        $postIdValues = $_POST['ids'] ?? '';
        $decodedString = json_decode($postIdValues);

        // retrieve addWorkingList threshold value
        $threshValue = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList');
        $bgProcessingThreshold = isset($threshValue) && !empty($threshValue) ? $threshValue : 500;

        if (isset($postIdValues) && !empty($postIdValues) && !empty($decodedString) && $decodedString != null && $decodedString[0] != "") {
            $listId = Yii::$app->session['workingListDbId'];

            // if ids count exceed bg processing threshold
            if(count($decodedString) > $bgProcessingThreshold){
                $idsArr = $decodedString;
                $searchParams = [
                    "fields" => "package.id AS packageDbId",
                    "packageDbId" => implode(',',$idsArr)
                ];

                $sentForBgProcessing = $this->seedSearchListModel->insertItemsInBackground($listId,$searchParams);

                $insertItemsStatus = true;
                $bgprocess = true;
            }
            else{
                // insert items to working list
                $insertItemsStatus = $this->seedSearchListModel->insertSelectedItemsToWorkingList($decodedString,$listId);

                if($insertItemsStatus == true){
                    Yii::$app->session->set('selectedItems', null);
                }

                $bgprocess = false;
            }
        }

        return json_encode(['success'=>$insertItemsStatus,'bgprocess'=>$bgprocess]);
    }    

    /**
     * Method for adding all the query results in the working list
     * @return list html data
     */
    public function actionAddAllToList(){

        $userId = $this->userModel->getUserId();
        $totalCount = $_POST['totalCount'];

        // retrieve threshold values
        $threshValues = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList');
        $limit = !empty($threshValues) ? $threshValues : 500;
        
        // retrieve session values
        $listId = Yii::$app->session['workingListDbId'];

        // session values
        $sessionFilters = Yii::$app->session['dataFilters'];
        $filterAbbrevs = Yii::$app->session['filterAbbrevs'];        

        // check if query parameters in json format in session are saved
        if ((isset(Yii::$app->session['fsEncodedParams'])) && !empty(Yii::$app->session['fsEncodedParams'])) {
            $encodedParams = Yii::$app->session['fsEncodedParams'];
            $encodedParams = json_decode($encodedParams, true);

            if (isset($encodedParams['retainSelection'])) {
                unset($encodedParams['retainSelection']);
            }
            
            $updatedSearchParams = $this->seedSearchBrowserModel->buildSearchParamFields($encodedParams);

            $status = $this->seedSearchListModel->insertItemsInBackground($listId, $updatedSearchParams);

            return json_encode(['existingIdStr' => '', 'success' => true]);
        } // check if session filters exists; build query parameters
        else if (isset($sessionFilters) && !empty($sessionFilters) && isset($filterAbbrevs) && !empty($filterAbbrevs)) {
            $inputListFilter = ['DESIGNATION', 'GID', 'LABEL'];
            $inputListOrder = false;

            $searchModel = $this->seedSearchBrowserModel;

            $isMultipleSelect = 'false';
            if (isset(Yii::$app->session['isMultipleSelect'])) {
                $isMultipleSelect = Yii::$app->session['isMultipleSelect'];
            }

            foreach ($filterAbbrevs as $key=>$value) {
                if (in_array($value['abbrev'],$inputListFilter)) $inputListOrder = true;
            }

            $workingListRes = $this->workingListModel->getWorkingList($userId);
            $workingListCount = count($workingListRes);
            
            if (!empty($workingListRes) && $workingListCount > 0) {
                
                $workingListDbId = $workingListRes['listDbId'];
                $workingListCount = $workingListRes['memberCount'];

                $workingListAttr = ($isMultipleSelect == 'true') ? 'package' : 'germplasm';
                $newFilter = [
                    'abbrev' => 'workingListDbId',
                    'filter_type' => 'search',
                    'operator' => 'not in',
                    'values' => $workingListDbId
                ];
                array_push($filterAbbrevs, $newFilter);

                $newFilter = [
                    'abbrev' => 'multipleSelect',
                    'filter_type' => 'search',
                    'operator' => 'not in',
                    'values' => $isMultipleSelect
                ];
                array_push($filterAbbrevs, $newFilter);
            }
            $resFiltersSess = Yii::$app->session['resultsFilters'];
            $params = (isset($resFiltersSess) && $resFiltersSess != null ? ['FindSeedDataBrowserModel' => $resFiltersSess] : NULL);

            // get packages
            $condStr = '';
            $condTable = '';
            $browserFilters = [];

            if($params){
                foreach ($params['FindSeedDataBrowserModel'] as $key => $value) {
                    if($value!==null && $value!=''){
                        $condStr = $condStr . " and {$key} ilike '%". $value ."%'";
                        $browserFilters[]=[
                            "abbrev"=>strtoupper($key),
                            "values"=>[$value],
                            "operator"=>'ilike any',
                            "filter_type"=>'column'
                        ];
                    }
                }
            }

            if ($totalCount > $limit) {

                $status = $this->seedSearchListModel->insertItemsInBackground($listId,$filterAbbrevs);

                return json_encode(['existingIdStr' => '','success'=>true]);
            } 
            else {
                $data = $this->seedSearchBrowserModel->getDataProvider($filterAbbrevs, $condStr, null, $inputListOrder, $browserFilters, true, false);

                if ($data['totalCount'] > 0) {
                    $labels = '';
                    
                    $newData = array_map(
                            function($item){ 
                                return [
                                    "id"=>$item['packageDbId'], 
                                    "displayValue"=>$item['label']
                                ];
                            }, 
                            $data['data']
                        );
                        
                    $status = $this->seedSearchListModel->insertProcessor($newData,$listId);

                    return json_encode(['existingIdStr' => '','success'=>true]);
                }
            }
        }        
    }

    /**
     * Removes items from list
     * @return success status
     */
    public function actionRemoveListItems(){

        $userModel = new User();
        $userId = $userModel->getUserId();

        if (isset($_POST['idRemove'])) {
            
            $decodedRemoveString = json_decode($_POST['idRemove']);
            $removeStatus = $this->seedSearchListModel->deleteTempValuesFromSeedList($decodedRemoveString, $userId, count($decodedRemoveString));

            return json_encode(['success' => $removeStatus]);
        }
    }

    /**
     * Save list items
     * @return success status
     */
    public function actionSaveListItems(){
        
        $data = Yii::$app->request->post();
        
        $userModel = new User();
        $userId = $userModel->getUserId();
        $message = '';
        $success = false;
        
        if(isset($data['listId'])){

            $returnArr = $this->seedSearchListModel->saveListItems($data, $userId);

            $success = $returnArr['success'];
            $message = $returnArr['message'];
        }
        return json_encode(['success' => $success, 'message' => $message]);
    }

    /**
     * Retrieve list item count
     * @return record count
     */
    public function actionGetItemsCount(){
        $userModel = new User();
        $userId = $userModel->getUserId();
        $tableName = 'temp_findseed_'.$userId;
        $recordCount = 0;
        $data = [];

        $checkTempList = $this->seedSearchListModel->checkIfTableExistWithoutCol($tableName,$userId);

        if($checkTempList == true){
            $workingList = $this->workingListModel->getWorkingList($userId);
            $workingListDbId = $workingList['listDbId'];
            
            $workingListData = $this->workingListModel->search($workingListDbId);

            if(!empty($workingListData)){
                $listDataProvider = $workingListData;
                
                $data['count'] = $listDataProvider->getTotalCount();
                $data['data'] = $listDataProvider;

                $recordCount = $data['count'];
            }
        }
        return $recordCount;
    }

    /**
    * Function for reordering list members
    * @return if action is successful
    **/

    public function actionReorderList(){

        $listMemberDbId = isset($_POST['dataId']) ? $_POST['dataId'] : 0;
        $newRowNumber = isset($_POST['newRowNumber']) ? $_POST['newRowNumber'] : 1;
        $requestData = [
            "orderNumber" => $newRowNumber
        ];
        $this->platformListMemberModel->updateOne($listMemberDbId, $requestData);

        return json_encode(array( "success" => true));
    }

    /**
    * Function for retrieving saved list
    * @return if action is successful
    **/
    public function actionGetSavedListDataProvider(){
        $listType = isset($_POST['listType']) ? $_POST['listType'] : 'name';

        $userModel = new User();
        $userId = $userModel->getUserId();

        $listDataProvider = $this->seedSearchListModel->getSavedSeedListDataProvider($userId,$listType);

        return json_encode($listDataProvider['data']);
    }

    /**
    * Function for retrieving saved list data provider
    * @return if action is successful
    **/
    public function actionGetSavedSeedListDataProvider(){

        $userModel = new User();
        $userId = $userModel->getUserId();
        
        if(isset($_POST['listType'])){
            $listType = $_POST['listType'];
        }

        $listDataProvider = $this->seedSearchListModel->getSavedSeedListDataProvider($userId,$listType);

        return Yii::$app->controller->renderPartial('@app/modules/seedInventory/views/seed-list/open_list', [
            'listDataProvider'=> isset($listDataProvider['data']) && !empty($listDataProvider['data']) ? $listDataProvider['data'] : [],
            'filters'=>'',
            'dataType'=>$_POST['listType']
            ]);
    }

    /**
    * Function for retrieving saved items of list
    * @return if action is successful
    **/
    public function actionGetSelectedListItems(){

        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $inputListLimit = !empty(Yii::$app->config->getAppThreshold('FIND_SEEDS', 'inputListLimit')) ? Yii::$app->config->getAppThreshold('FIND_SEEDS', 'inputListLimit') : 500;
        
        if(!empty($id)){
            $results = $this->seedSearchListModel->getSavedListById(intval($id));

            $listItems = $results['result'];
            $designation = "";
            $listItemsCount = 0;

            if(isset($listItems) && $listItems != [] && $listItems != null){
                $listItemsCount = count($listItems);

                if($listItemsCount <= $inputListLimit){
                    foreach($listItems as $key=>$value){
                        if($designation != ""){
                            $designation .= ',';
                        }
                        $designation .= $value;
                    }
                }
                else{
                    return json_encode(['success'=>false,'error'=>'Notice! List exceeds '.$inputListLimit.' items. Please select a smaller list.','value'=>""]);    
                }
            }

            if(!empty($designation)){
                return json_encode(['success'=>$results['success'],'error'=>$results['error'],'value'=>json_encode($designation,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)]);
            }
        }
    }

    /**
    * Function for retrieving working list with additional items from selected saved list
    * @return if action is successful
    **/
    public function actionGetSelectedSeedListItems(){
        $userModel = new User();
        $userId = $userModel->getUserId();

        $bgProcessingThresh = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList');
        $bgProcess = false;

        $labels = '';
        $listId = null;
        $insertItemsStatus = false;
        $totalCount = 0;

        if(isset(Yii::$app->session['workingListDbId']) && Yii::$app->session['workingListDbId'] != ''){
            $listId = Yii::$app->session['workingListDbId'];

            $workingListRes = $this->workingListModel->getWorkingList($userId);
            $workingListMemberCount = $workingListRes['memberCount'];

            $listData = $this->workingListModel->getWorkingListMembers($listId);
            $workingList = $listData;

            $existingWorkingList['listId'] = $listId;
            $existingWorkingList['data'] = $workingList;
            $existingWorkingList['totalCount'] = count($workingList);
        }
        else{
            $existingWorkingList = $this->seedSearchListModel->createWorkingList($userId);
            $workingListRes = $this->workingListModel->getWorkingList($userId);
            $listId = $workingListRes['listDbId'];
        }
        
        if (isset($_POST['id'])) {    
            $sourceListDbId = (string)$_POST['id'];

            // retrieve list data
            $listData = $this->seedSearchListModel->getSavedListData($sourceListDbId);
            // check member count
            $listMemberCount = $listData['memberCount'];

            if(intval($listMemberCount) > $bgProcessingThresh){
                $searchParams = [
                    "sourceListDbId" => $sourceListDbId
                ];

                $sentForBgProcessing = $this->seedSearchListModel->insertItemsInBackground($listId,$searchParams);
                $bgProcess = true;
                $insertItemsStatus = true;
            }
            else{
                $listData = $this->workingListModel->getWorkingListMembers($sourceListDbId);
                $savedListMembers['data'] = $listData;

                if(!empty($savedListMembers['data']) && $savedListMembers['data']!=null && $savedListMembers['data'][0]!=""){
                    $savedListMembersIds = array_column($savedListMembers['data'],'packageDbId');
                    $newWorkingListSeedIds = $this->seedSearchListModel->validateSeedIds($savedListMembersIds, $userId, $existingWorkingList);
                    
                    $labels = $newWorkingListSeedIds['duplicateListMembers'] == null || empty($newWorkingListSeedIds['duplicateListMembers']) ? '' : implode(',',$newWorkingListSeedIds['duplicateListMembers']);
                    $newWorkingListSeedIds = $newWorkingListSeedIds['newListMembers'];
                    $totalCount = count($newWorkingListSeedIds);
                    
                    if(isset($listId) && !empty($newWorkingListSeedIds)){
                        $insertItemsStatus = $this->seedSearchListModel->insertSelectedItemsToWorkingList($newWorkingListSeedIds,$listId);
                    }
                }
            }

        }

        $existingIdStr = '';
        $filterAbbrevs = Yii::$app->session['filterAbbrevs'];
        $inputListOrder = false;
        
        if(Yii::$app->session['filterAbbrevs']!=null){
            $searchData=$this->seedSearchBrowserModel->getSearchData(isset($_GET['FindSeedDataBrowserModel']) ? ['FindSeedDataBrowserModel' => $_GET['FindSeedDataBrowserModel']] : NULL, $filterAbbrevs, $inputListOrder);
        }
        return json_encode(['duplicateItems'=>$labels,'success'=> $insertItemsStatus,'count'=>$totalCount,'bgprocess'=>$bgProcess]);
    }

    /**
     * set the currently selected package data in session to null.
     * @return array
     */
    public function actionEmptyUserSelection(){
        Yii::$app->session->set('selectedItems', null);
        return json_encode(['success'=> true]);
    }

    /**
     * clear working list
     * @return array status
     */
    public function actionEmptyWorkingList(){
        
        $userModel = new User();
        $userId = $userModel->getUserId();
        $clearListStatus = $this->seedSearchListModel->clearWorkingList($userId);
        return json_encode(['success'=> $clearListStatus['success'],'bgprocess'=>$clearListStatus['bgprocess']]);
    }

    /**
     * Retrieve Working List Count
     * 
     * @return array totalCount
     */
    public function actionGetWorkingListCount(){
        $userId = $this->userModel->getUserId();
        
        $workingListRes = $this->workingListModel->getWorkingList($userId);
        $listCount = $workingListRes['memberCount'] ?? 0;
        
        return json_encode(['totalCount'=>$listCount]);
    }

}

?>