<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\seedInventory\controllers;

use app\components\B4RController;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * FSearch Results Browser Configuration Controller for the `SeedInventory` module
 */
class BrowserConfigurationController extends B4RController
{
    public function __construct($id,$module,$config)
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * Set useAdvancedSearch session variable value
     */
    public function actionSetUseAdvancedSearch()
    {
        // retrieve most recent selected items
        $postParams = Yii::$app->request->post();

        $useAdvancedSearch = isset($postParams['useAdvancedSearch']) ? $postParams['useAdvancedSearch'] : 'false';
        Yii::$app->session->set('useAdvancedSearch', $useAdvancedSearch);

        return json_encode(['success'=>true]);
    }

    /**
     * Retrieve session value
     */
    public function actionGetUseAdvancedSearch()
    {
        return Yii::$app->session['useAdvancedSearch'];
    }
}