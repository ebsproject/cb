<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\controllers;

use app\components\B4RController;
use app\controllers\Dashboard;
use app\models\BackgroundJob;
use app\models\Config;
use app\models\SeedStorageSearch;
use app\models\User;
use app\models\DataBrowserFilterConfig;
use app\models\Program;
use app\models\Package;
use app\models\Lists;
use app\models\Seed;
use app\models\Worker;
use app\models\GermplasmName;

use app\interfaces\modules\seedInventory\models\IVariableFilterComponent;
use app\interfaces\modules\seedInventory\models\IFindSeedListModel;
use app\interfaces\modules\seedInventory\models\IWorkingList;

use app\modules\seedInventory\models\FindSeedDataBrowserModel;
use app\modules\seedInventory\models\FindSeedListModel;
use app\modules\seedInventory\models\WorkingList;
use app\modules\seedInventory\models\VariableFilterComponent;
use app\modules\seedInventory\models\QueryParameterModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Find Seeds Controller for the `SeedInventory` module
 */
class FindSeedsController extends B4RController
{
    protected $variableFilterComponent;
    protected $queryParameterModel;
    protected $user;
    protected $programModel;
    protected $listsModel;
    protected $findSeedDataBrowserModel;
    protected $seedSearchListModel;
    protected $workingListModel;
    protected $dataBrowserConfigModel;

    protected $dashboardModel;
    protected $backgroundJobModel;
    protected $worker;

    public function __construct(
        $id,
        $module,
        $config,
        IVariableFilterComponent $variableFilterComponent,
        QueryParameterModel $queryParameterModel,
        FindSeedDataBrowserModel $findSeedDataBrowserModel,
        IFindSeedListModel $seedSearchListModel,
        Program $programModel,
        User $user,
        IWorkingList $workingListModel,
        Worker $worker
    ) {
        $this->variableFilterComponent = $variableFilterComponent;
        $this->queryParameterModel = $queryParameterModel;
        $this->user = $user;
        $this->programModel = $programModel;
        $this->findSeedDataBrowserModel = $findSeedDataBrowserModel;
        $this->seedSearchListModel = $seedSearchListModel;
        $this->workingListModel = $workingListModel;
        $this->worker = $worker;

        $this->dashboardModel = \Yii::$container->get('app\controllers\Dashboard');
        $this->backgroundJobModel = \Yii::$container->get('app\models\BackgroundJob');
        $this->listsModel = \Yii::$container->get('app\models\Lists');
        $this->dataBrowserConfigModel = \Yii::$container->get('app\models\DataBrowserConfiguration');

        parent::__construct($id, $module, $config);
    }

    /**
     *  Renders find seeds page
     */
    public function actionIndex()
    {

        // set session variables
        $this->setSessionQueryParams(null, null);
        Yii::$app->session->set('resultsFilters', null);
        Yii::$app->session->set('resetBrowserFilters', false);
        Yii::$app->session->set('minimizeQueryParams', false);
        Yii::$app->session->set('selectedFilters', null);
        Yii::$app->session->set('selectedItems', null);
        Yii::$app->session->set('dataFilters', null);

        $this->actionSetWorkingListVisibility(true);
        return $this->actionFind('true');
    }

    /**
     * Retrieve dashboard filter values
     * @param Array $defaultFilters dashboard filters
     * @return Array transformed filters
     */
    public function retrieveDefaultFilters($defaultFilters)
    {

        $userId = $this->user->getUserId();
        $preloadedFilters = [
            ['abbrev' => 'includeSeedlist', 'values' => false],
            ['abbrev' => 'userId', 'values' => $userId],
        ];

        if (!empty($defaultFilters)) {
            foreach ($defaultFilters as $key => $value) {
                if ($value != null && $value != "") {
                    $index = strtoupper($key);

                    if (strpos($key, '_') != -1) {
                        $index = ($key == 'stage_id' ? 'EVALUATION_STAGE' : strtoupper(explode('_', $key)[0]));
                    }
                    if ($key == 'year') {
                        $index = 'EXPERIMENT_YEAR';
                    }

                    if ($key == 'program_id') {
                        array_push($preloadedFilters, ['abbrev' => $index, 'values' => $value]);
                    }
                }
            }
        }

        return $preloadedFilters;
    }

    /**
     * Extract find values according to variable filters provided
     * @param String resetSelection indicator if reset is needed
     * @return HTML index page
     */
    public function actionFind($resetSelection = null)
    {
        // Retrieve filters, dashboard parameters, and page values
        $postValues = \Yii::$app->request->post();
        $defaultFilters = $this->dashboardModel->getFilters();
        $dashProgram = isset($defaultFilters) && !empty($defaultFilters) && isset($defaultFilters->program_id) ? $defaultFilters->program_id : '';

        $userId = $this->user->getUserId();

        $selectionOptions = $this->variableFilterComponent->retrieveSelectionOptions();
        $list = ($selectionOptions == null || empty($selectionOptions) ? [] : ArrayHelper::map($selectionOptions, 'variable_abbrev', 'field_label'));
        $filterSelection = $list;

        $searchModel = $this->findSeedDataBrowserModel;
        $this->findSeedDataBrowserModel->setColumnsForFiltering();

        $dataFilters = [];
        $filterValues = [];
        $filters = [];

        // set session column filter values
        if (isset($_GET['FindSeedDataBrowserModel']) && $_GET['FindSeedDataBrowserModel'] != null) {
            $columnFilterValues = $_GET['FindSeedDataBrowserModel'];
            Yii::$app->session->set('resultsFilters', $columnFilterValues);
        }

        if (isset($_GET[1]) || isset($_GET['1'])) {
            Yii::$app->session->set('blankPageRedirect', true);
        }

        // set session variables
        $isMultipleSelect = 'false';
        $isAutoSelect = 'false';
        $isGrouped = 'true';
        $isRetained = 'true';
        $packageLimitQr = 0;

        if (isset(Yii::$app->session['isMultipleSelect'])) {
            $isMultipleSelect = Yii::$app->session['isMultipleSelect'];
        }

        if (isset(Yii::$app->session['isAutoSelect'])) {
            $isAutoSelect = Yii::$app->session['isAutoSelect'];
        }

        if (isset(Yii::$app->session['qrIsGrouped'])) {
            $isGrouped = Yii::$app->session['qrIsGrouped'];
        }

        if (isset(Yii::$app->session['qrIsRetained'])) {
            $isRetained = Yii::$app->session['qrIsRetained'];
        } else {
            Yii::$app->session->set('qrIsRetained', $isRetained);
        }

        if (isset(Yii::$app->session['packageLimitQr'])) {
            $packageLimitQr  = Yii::$app->session['packageLimitQr'];
        }

        if (isset($postValues['forceReset']) && !empty($postValues['forceReset']) && boolval($postValues['forceReset'])) {
            Yii::$app->session->set('resetBrowserFilters', true);
            Yii::$app->session->set('resultsFilters', null);
        } else {
            Yii::$app->session->set('resetBrowserFilters', false);
        }

        if (isset($_POST['filter']) && empty($_POST['filter']) && isset(Yii::$app->session['dataFilters']) && empty(Yii::$app->session['dataFilters'])) {
            $filterValues = [['abbrev' => 'PROGRAM', 'values' => $dashProgram]];
            Yii::$app->session->set('filterAbbrevs', $filterValues);
            Yii::$app->session->set('dataFilters', $dataFilters);
        }

        // Update columns for Advanced search; disable package data columns
        $this->findSeedDataBrowserModel->setColumnsForFiltering();

        // If has search filters, process, set, and prep filter values
        if (isset($_POST['filter']) && !empty($_POST['filter'])) {
            $data = Yii::$app->request->post();

            if (isset($data['filter'])) {
                $filters = $data['filter'];
                Yii::$app->session->set('dataFilters', $filters);
            } else {
                $filters = Yii::$app->session['dataFilters'];
            }

            // empty selected ids session
            $resetSelection = 'true';

            $varFilters = [];
            $dataFilters = $this->findSeedDataBrowserModel->processFilters($filters);

            Yii::$app->session->set('dataFilters', $dataFilters);
            Yii::$app->session->set('filterAbbrevs', $dataFilters);
            Yii::$app->session->set('visibilityCheck', false);

            Yii::$app->session->set('minimizeQueryParams', true);

            if (isset($_POST['limitGroup'])) {
                Yii::$app->session->set('packageLimitQr', $_POST['limitGroup']);
            }
        }
        // If has no search filters, initialize page data
        else {
            // set working list dataprovider
            $listDataProvider = new \yii\data\ArrayDataProvider([]);

            // check germplasm list for saved germplasm lists in additional filters
            $germplasmListDataProvider = [];
            $data = ['data' => '', 'count' => 0];

            // check if has existing working list
            $listData = $this->workingListModel->getWorkingList($userId);
            $checkIfTableExist = $listData && count($listData) > 0 ? true : false;

            // create working list if not existing
            if (!$checkIfTableExist) {
                $record = [
                    'abbrev' => 'WORKING_LIST_PACKAGE_' . $userId,
                    'name' => 'Working List Package ' . $userId,
                    'description' => 'user session package working list',
                    'displayName' => 'Working List Package',
                    'remarks' => 'user session package working list',
                    'type' => 'package',
                    'status' => 'draft',
                    'isActive' => 'false',
                    'listUsage' => 'working list'
                ];
                $result = $this->seedSearchListModel->saveListBasicInformation($record);

                $checkIfTableExist = (isset($result['status']) && $result['status'] == 200) ? true : false;

                // set newly created list as working list
                $listData = $this->workingListModel->getWorkingList($userId);
            }

            // retrieve working list items
            $listItemCount = 0;
            if ($checkIfTableExist || $checkIfTableExist == 1) {
                $workingListDbId = $listData['listDbId'];
                $listItemCount = $listData['memberCount'] ?? 0;
                Yii::$app->session->set('workingListDbId', $workingListDbId);

                $data['count'] = $listItemCount;
                $data['data'] = new \yii\data\ArrayDataProvider([]);
            }

            $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();
            if (!isset($inputListFields) || empty($inputListFields)) {
                $inputListFields = [
                    'fieldTypes' => [],
                    'fieldValidation' => [],
                    'fieldDescription' => [],
                    'config' => []
                ];
            }

            // search packages
            if ((isset(Yii::$app->session['dataFilters'])) && !empty(Yii::$app->session['dataFilters'])) {
                $sessionFilters = Yii::$app->session['dataFilters'];
                $filterAbbrevs = Yii::$app->session['filterAbbrevs'];
                $activeTab = Yii::$app->session['activeTab'];
                $variableFilters = [];

                $inputListFilter = isset($inputListFields) && !empty($inputListFields) ? array_map('strtoupper', array_keys($inputListFields)) : [];
                $inputListOrder = false;

                foreach ($filterAbbrevs as $key => $value) {
                    if (in_array($value['abbrev'], $inputListFilter)) $inputListOrder = true;
                }

                if ($checkIfTableExist || $checkIfTableExist == 1) {
                    // include working list items as filters for search (exclude items already in working list)
                    if ($data['count'] > 0) {
                        $workingListAttr = ($isMultipleSelect == 'true') ? 'package' : 'germplasm';
                        $newFilter = [
                            'abbrev' => 'workingListDbId',
                            'filter_type' => 'search',
                            'operator' => 'not in',
                            'values' => $workingListDbId
                        ];
                        array_push($filterAbbrevs, $newFilter);

                        $newFilter = [
                            'abbrev' => 'multipleSelect',
                            'filter_type' => 'search',
                            'operator' => 'not in',
                            'values' => $isMultipleSelect
                        ];
                        array_push($filterAbbrevs, $newFilter);
                    }
                }

                // check if need to reset browser
                if ((isset($postValues['forceReset']) && !empty($postValues['forceReset']) && boolval($postValues['forceReset'])) ||
                    Yii::$app->session['resetBrowserFilters']
                ) {
                    $params = NULL;
                } else {
                    // retrieve data provider for search results browser
                    $params = (isset(Yii::$app->session['resultsFilters']) && Yii::$app->session['resultsFilters'] != null ? ['FindSeedDataBrowserModel' => Yii::$app->session['resultsFilters']] : NULL);
                }

                // retrieve search results
                $dataProvider = $searchModel->search($params, $filterAbbrevs, $inputListOrder);
                // retrieve formatted browser parameters
                $searchData = $this->findSeedDataBrowserModel->getSearchData($params, $filterAbbrevs, $inputListOrder);
                // retrieve formatted search parameters
                $dataFilter = $this->findSeedDataBrowserModel->processFilters($sessionFilters);
                $tableName = 'temp_findseed_' . $userId;
                $itemCount = $data['count'];

                // generate html filters
                $filterValues = $filterAbbrevs;
                $filters = $this->variableFilterComponent->generateFilterFields($userId, $filterValues, true);
                $requiredFilters = $this->variableFilterComponent->retrieveRequiredFilters();
                Yii::$app->session->set('requiredFilters', $requiredFilters);

                $searchData = isset($_GET['FindSeedDataBrowserModel']) ? ['FindSeedDataBrowserModel' => $_GET['FindSeedDataBrowserModel']] : NULL;
                $resetSelection = 'false';
            } else {

                // for actionIndex
                $dataProvider = new \yii\data\ArrayDataProvider([]);

                // retrieve config and required fields
                $requiredFilters = $this->variableFilterComponent->retrieveRequiredFilters();

                Yii::$app->session->set('requiredFilters', $requiredFilters);

                $itemCount = $data['count'];
                $searchData = null;
            }

            $thresholdLimit = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'inputListLimit');
            $inputListLimit = isset($thresholdLimit) && !empty($thresholdLimit) ? $thresholdLimit : 500;

            $filterHtmlData = $this->renderPartial(
                '_filter_seeds',
                [
                    'filters' => $filters,
                    'filterValues' => $filterValues,
                    'filterSelection' => $filterSelection,
                    'program' => $dashProgram,
                    'programAbbrev' => Yii::$app->userprogram->get('abbrev'),
                    'activeTab' => 'variable',
                    'id' => $userId,
                    'inputListFields' => $inputListFields,
                    'requiredFilters' => $requiredFilters,
                    'tempTable' => $data,
                    'inputListLimit' => $inputListLimit,
                    'germplasmListDataProvider' => $germplasmListDataProvider['data'] ?? [],
                    'searchBrowserId' => 'dynagrid-seed-search-search-grid'
                ]
            );
            $isRetained = $isRetained == 'true' ? 'checked' : '';

            // get default value of the selection checkbox
            if ($isGrouped == 'true') {
                $isGrouped = 'checked';
                $isMultipleSelect = ($isMultipleSelect == 'true') ? 'checked' : '';
            } else {
                $isGrouped = '';
                $isMultipleSelect = 'checked';
            }
            $isAutoSelect = ($isAutoSelect == 'true' ? 'checked' : '');

            // adjust default isMultipleSelect session value
            if ($isMultipleSelect != 'false' && $isMultipleSelect != '') {
                Yii::$app->session->set('isMultipleSelect', 'true');
            }

            // retrieve threshold values
            $deleteLimit = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'deleteWorkingList');
            $insertLimit = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'addWorkingList');
            $thresholdValue = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'exportWorkingList');

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'listDataProvider' => $listDataProvider,
                'workingListModel' => $this->workingListModel,
                'workingListItemsCount' => $listItemCount,
                'searchModel' => $searchModel,
                'filters' => $filters,
                'filterHtmlData' => $filterHtmlData,
                'filterSelection' => $filterSelection,
                'filterValues' => $filterValues,
                'itemCount' => $itemCount,
                'searchData' => $searchData,
                'program' => Yii::$app->userprogram->get('abbrev'),
                'isMultipleSelect' => $isMultipleSelect,
                'isAutoSelect' => $isAutoSelect,
                'deleteLimit' => $deleteLimit,
                'insertLimit' => $insertLimit,
                'resetSelection' => $resetSelection,
                'isGrouped' => $isGrouped,
                'isRetained' => $isRetained,
                'packageLimitQr' => $packageLimitQr,
                'thresholdValue' => $thresholdValue,
                'browserId' => ['dynagrid-seed-search-search-grid'],
            ]);
        }
    }

    /**
     * Generate basic search filters
     * @return mixed basic filters html, filters, and filter selection options
     */
    public function actionGenerateBasicFilters()
    {
        $userId = $this->user->getUserId();
        $defaultFilters = [];
        if (isset(Yii::$app->session['dataFilters']) && !empty(Yii::$app->session['dataFilters'])) {
            $defaultFilters = Yii::$app->session['dataFilters'];
        } else {
            $defaultFilters = $this->dashboardModel->getFilters();

            if (!isset($defaultFilters)) {
                Yii::$app->session->setFlash('error', 'A connection error occurred while loading the page. Please try again or file a report for assistance.');
            }
        }

        $selectionOptions = $this->variableFilterComponent->retrieveSelectionOptions();
        $filterSelection = ($selectionOptions == null || empty($selectionOptions) ? [] : ArrayHelper::map($selectionOptions, 'variable_abbrev', 'field_label'));

        $filterValues = $this->retrieveDefaultFilters($defaultFilters);

        // retrieve variable filters
        $filters = $this->variableFilterComponent->generateFilterFields($userId, $filterValues, true);

        $basicFilters = $this->renderAjax(
            '/find-seeds-params/_basic_filters',
            [
                'filters' => $filters
            ]
        );

        return json_encode([
            'filtersHTML' => $basicFilters,
            'filters' => $filterValues,
            'filterSelection' => $filterSelection
        ]);
    }

    /**
     * Set session marker for list visibility
     * @param boolean check
     * @return json success
     */
    public function actionSetWorkingListVisibility($check)
    {
        if ($check == true) {
            Yii::$app->session->set('visibilityCheck', true);
        }

        return json_encode(['success' => true]);
    }

    /**
     * Extract variable filter option values
     * @param array q id page filter
     * @return json totalCount items
     */
    public function actionGetFilterData($q = null, $id = null, $page = null, $filters = null) // for refactoring
    {

        $configData = $this->variableFilterComponent->getConfig();

        $configVar = isset($configData) && !empty($configData) && isset($configData['records']) && !empty($configData['records']) ? $configData['records'] : [];
        $mainTableInfo = isset($configData) && !empty($configData) && isset($configData['mainTableInfo']) && !empty($configData['mainTableInfo']) ? $configData['mainTableInfo'] : [];
        $userId = $this->user->getUserId();

        $existingFilters = json_decode($filters, true);
        $variableInfo = [];
        $limit = 5;

        if (isset($_POST['filters']) && !empty($_POST['filters']) && isset($_POST['id']) && !empty($_POST['id'])) {
            $existingFilters = json_decode($_POST['filters']);
            $id = $_POST['id'];
            $q = isset($_POST['q']) ? $_POST['q'] : '';
            $page = intval($_POST['page']);
            $page = ($page == null ? 1 : $page);
            $offset = ($page == 1 ? 0 : (($page - 1) * 5) + 1);

            $variableFilters = $this->variableFilterComponent->extractVariableFilters($existingFilters, $configVar, $id);

            $variableInfo = [];
            foreach ($configVar as $key => $value) {
                if ($value['variable_abbrev'] == strtoupper(explode('-', $id)[0])) {
                    $variableInfo = $value;
                    break;
                }
            }
            $data = $this->variableFilterComponent->apiCallHandler($userId, $variableFilters, $variableInfo, $limit, $offset, $q);

            $totalCount = $data['count'];
            $callData = $data['data'];
        } else {
            $totalCount = 0;
            $callData = [];
        }

        return json_encode(['totalCount' => $totalCount, 'items' => $callData]);
    }

    /**
     * Load experiment occurrence data from experiment id 
     * @param array q id page filter
     * @return json totalCount items
     */
    public function actionGetExperimentOccurrenceData()
    {
        if (isset($_POST['experimentId']) && !empty($_POST['experimentId']) && $_POST['experimentId'] != 'NaN') {
            $experimentId = $_POST['experimentId'];
            $userId = !empty($this->user->getUserId()) ? $this->user->getUserId() : '';

            $configData = $this->variableFilterComponent->getConfig();
            $configVars = $configData['records'];

            $variableInfo['OCCURRENCE_NAME'] = '';
            $variableInfo['EXPERIMENT_NAME'] = '';

            if (isset($configData['records']) && !empty($configData['records'])) {
                $variableInfo['OCCURRENCE_NAME'] = array_values(array_filter(array_map(function ($configVar) {
                    if ($configVar['variable_abbrev'] == 'OCCURRENCE_NAME') {
                        return $configVar;
                    }
                }, $configVars)))[0];

                $variableInfo['EXPERIMENT_NAME'] = array_values(array_filter(array_map(function ($configVar) {
                    if ($configVar['variable_abbrev'] == 'EXPERIMENT_NAME') {
                        return $configVar;
                    }
                }, $configVars)))[0];
            }

            $variableFilters = [
                [
                    'abbrev' => 'EXPERIMENT_NAME',
                    'values' => $experimentId,
                    'config_info' => $variableInfo['EXPERIMENT_NAME']
                ]
            ];

            $data = $this->variableFilterComponent->apiCallHandler($userId, $variableFilters, $variableInfo['OCCURRENCE_NAME'], NULL, 0, "");

            $totalCount = $data['count'];
            $callData = $data['data'];

            return json_encode(['count' => $totalCount, 'data' => $callData]);
        }
    }

    /**
     * Render selected additional variable filters
     * @param none
     * @return html selected seed inventory variables
     */
    public function actionGenerate()
    {
        $userId = !empty($this->user->getUserId()) ? $this->user->getUserId() : '';

        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data = Yii::$app->request->post();

            $additionalFilters = strpos($data['selectedFilters'], ',') !== false ? explode(',', $data['selectedFilters']) : [$data['selectedFilters']];
            $filters = isset($data['queryValues']) ? $data['queryValues'] : [];
            $filterValues = [];

            if (!empty($data['filterSelection']) && isset($data['queryValues'])) {

                Yii::$app->session->set('selectedFilters', $data['queryValues']);

                foreach ($filters as $key => $filterPair) {
                    array_push($filterValues, $filterPair);
                }
            }
            $additionalFilters = [$additionalFilters[count($additionalFilters) - 1]];
            $filterHtml = $this->variableFilterComponent->generateFilterFields($userId, $filterValues, false, $additionalFilters);

            $currField = isset($filterHtml) && !empty($filterHtml) && isset($filterHtml[0]['value']) ? $filterHtml[0]['value'] : '';
            $HTMLField = $this->renderAjax(
                '/find-seeds-params/_additional_field',
                [
                    'filterField' => $currField
                ]
            );

            return json_encode([
                'elementId' => $additionalFilters[0],
                'elementField' => $HTMLField
            ]);
        }
    }

    /**
     * Set csrf to false
     * @param action
     * @return action
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Resets result summary information modal
     */
    public function actionResetSummary()
    {
        Yii::$app->session->set('fsEncodedParams', '');
    }

    /**
     * Reset query parameters
     * Defaults to current program 
     */
    public function actionResetQueryParam()
    {

        $defaultFilters = $this->dashboardModel->getFilters();
        $dashProgram = isset($defaultFilters->program_id) && !empty($defaultFilters->program_id) ? $defaultFilters->program_id : '';
        $userId = $this->user->getUserId();

        $filterValues = $this->retrieveDefaultFilters($defaultFilters);

        $dataFilters = $this->findSeedDataBrowserModel->processFilters($filterValues);
        $this->setSessionQueryParams(null, null);

        Yii::$app->session->set('filterAbbrevs', $dataFilters);
        Yii::$app->session->set('dataFilters', $filterValues);
    }

    /**
     * Get query parameter values by id
     * Defaults to current program 
     */
    public function actionGetParamValuesById()
    {

        $data = Yii::$app->request->post();
        $params = $data['queryParams'];
        $type = $data['type'];

        $inputListTypeData = $this->variableFilterComponent->retrieveInputListConfig();
        $inputListType = isset($inputListTypeData) && $inputListTypeData != [] ? array_map('strtolower', array_keys($inputListTypeData)) : [];

        $configData = $this->variableFilterComponent->getConfig();

        $configVars = [];
        $mainTableInfo = [];
        if (isset($configData) && !empty($configData) && isset($configData['records']) && !empty($configData['records'])) {
            $configVars = $configData['records'];
            $mainTableInfo = $configData['mainTableInfo'];
        }

        $userId = !empty($this->user->getUserId()) ? $this->user->getUserId() : '';
        $inputListFlag = false;
        $queryParams = null;

        $paramsForStorage = $this->variableFilterComponent->extractQueryParamsValuesForStorage($userId, $params, $configVars, $inputListType);
        $queryParam = ['ids' => $paramsForStorage['queryParamIds'], 'texts' => $paramsForStorage['queryParamValues']];

        return json_encode([
            'success' => true,
            'queryParams' => [
                'values' => $paramsForStorage['displayValues'],
                'ids' => $queryParam
            ],
            'type' => $type
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * Save query parameters
     */
    public function actionSaveQueryParams()
    {

        $data = Yii::$app->request->post();
        $userId = $this->user->getUserId();

        $appTool = 'find-seeds';

        $test = json_decode($data['data'], JSON_UNESCAPED_SLASHES);
        $queryParamRecord = [
            'userDbId' => $userId,
            'name' => $data['name'],
            'data' => json_decode($data['data'], JSON_UNESCAPED_SLASHES),
            'dataBrowserDbId' => $appTool,
            'type' => 'filter'
        ];
        $status = $this->queryParameterModel->saveQueryParam($queryParamRecord);

        $success = false;
        if ($status == 200) {
            $success = true;
        }

        return json_encode(["success" => $success, 'error' => '', 'queryParamName' => $data['name']]);
    }

    /**
     * Get query parameters list data provider
     */
    public function actionGetQueryParamsDataProvider()
    {

        $userId = !empty($this->user->getUserId()) ? $this->user->getUserId() : '';
        $appTool = 'find-seeds';

        $queryParamsDataProvider = $this->queryParameterModel->getQueryParamsDataProvider($userId, $appTool);

        return Yii::$app->controller->renderPartial('@app/modules/seedInventory/views/find-seeds-params/_view_query_params', [
            'queryParamsDataProvider' => $queryParamsDataProvider['data'],
        ]);
    }

    /**
     * Get query parameters by id 
     */
    public function actionGetQueryParamsById()
    {

        $data = Yii::$app->request->post();
        $paramId = $data['id'];
        $userId = $this->user->getUserId();

        $appTool = 'find-seeds';
        $queryParam = null;

        $this->setSessionQueryParams($paramId, $userId);
        $queryParam = $this->queryParameterModel->getQueryParamsById($paramId, $userId, $appTool);

        return json_encode(["success" => true, 'error' => '', 'query' => $queryParam], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * Render new query parameter fields with selected query parameters 
     */
    public function actionLoadSelectedParamsToFilters()
    {

        $data = Yii::$app->request->post();
        $filterValues = $data['selectedParams'];

        $userId = $this->user->getUserId();

        $defaultFilters = $this->dashboardModel->getFilters();
        $dashProgram = $defaultFilters->program_id;

        $filters = $this->variableFilterComponent->generateFilterFields($userId, $filterValues, true);

        $selectionOptions = $this->variableFilterComponent->retrieveSelectionOptions();
        $list = ($selectionOptions == null || empty($selectionOptions) ? [] : ArrayHelper::map($selectionOptions, 'variable_abbrev', 'field_label'));
        $filterSelection = $list;

        $requiredFilters = $this->variableFilterComponent->retrieveRequiredFilters();
        Yii::$app->session->set('requiredFilters', $requiredFilters);

        $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();

        $germplasmListDataProvider = [];

        return Yii::$app->controller->renderAjax(
            '/find-seeds-params/_variable_filters',
            [
                'id' => $userId,
                'program' => $dashProgram,
                'filterSelection' => $filterSelection,
                'filterValues' => $filterValues,
                'filters' => $filters,
                'programAbbrev' => Yii::$app->userprogram->get('abbrev'),
                'inputListFields' => $inputListFields,
                'requiredFilters' => $requiredFilters,
                'germplasmListDataProvider' => $germplasmListDataProvider
            ]
        );
    }

    /**
     * Function for retrieving germplasm list data
     * @return array germplasm list data
     */
    public function actionGetGermplasmList()
    {

        $userId = !empty($this->user->getUserId()) ? $this->user->getUserId() : '';

        $defaultFilters = $this->dashboardModel->getFilters();
        if (!isset($defaultFilters)) {
            Yii::$app->session->setFlash('error', 'A connection error occurred while loading the page. Please try again or file a report for assistance.');
        }

        $dashProgram = isset($defaultFilters) && !empty($defaultFilters) && isset($defaultFilters->program_id) && !empty($defaultFilters->program_id) ? $defaultFilters->program_id : '';

        $filterValues = Yii::$app->session['filterAbbrevs'];
        $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();

        $germplasmListDataProvider = $this->seedSearchListModel->getSavedSeedListDataProvider($userId, 'germplasm');
        $dataProvider = isset($germplasmListDataProvider['data']) && !empty($germplasmListDataProvider['data']) ? $germplasmListDataProvider['data'] : [];

        return Yii::$app->controller->renderAjax('/find-seeds-params/_input_list_filter', [
            'id' => $userId,
            'program' => $dashProgram,
            'filterValues' => $filterValues,
            'inputListFields' => $inputListFields,
            'inputListParamStatus' => true,
            'addFilterParamStatus' => false,
            'germplasmListDataProvider' => $dataProvider
        ]);
    }

    /**
     * Get the session query parameters id
     * @param none
     * @return Array $user id session params id
     */
    public function actionGetSessionParam()
    {

        return Yii::$app->session['paramId'];
    }

    /**
     * Set the session query parameters id
     * @param none
     * @return null
     */
    public function setSessionQueryParams($paramId, $userId)
    {
        Yii::$app->session->set('userId', $userId);
        Yii::$app->session->set('paramId', $paramId);
    }

    /**
     * validate query parameters
     * @param none
     * @return null
     */
    public function actionValidateQueryParams()
    {

        $duplicate = false;
        $data = Yii::$app->request->post();

        if (isset($data) && $data != null && !empty($data)) {
            $duplicate = $this->queryParameterModel->getQueryParam($data['name'], $data['type'], $data['data']);
        }

        return json_encode($duplicate);
    }

    /**
     * retrieve query parameters state
     * @return boolean params state
     */
    public function actionGetQueryParamState()
    {

        return Yii::$app->session['minimizeQueryParams'];
    }

    /**
     * retrieve page redirect state
     * @return boolean redirect state
     */
    public function actionGetPageRedirect()
    {

        return Yii::$app->session['blankPageRedirect'];
    }

    /**
     * retrieve selected filters
     * @return array additional filters
     */
    public function actionGetSelectedFilters()
    {

        return json_encode(Yii::$app->session['selectedFilters']);
    }

    /**
     * set the currently selected mode in session. mode can be single or multiple selection
     * @return array
     */
    public function actionSetSessionSelection()
    {

        $isMultipleSelect = $_POST['isMultipleSelect'];
        Yii::$app->session->set('isMultipleSelect', $isMultipleSelect);
        return json_encode(['success' => true]);
    }

    /**
     * set the automatic selection status in session
     * @return array
     */
    public function actionSetSessionAutoSelection()
    {

        $isAutoSelect = $_POST['isAutoSelect'];
        Yii::$app->session->set('isAutoSelect', $isAutoSelect);

        return json_encode(['success' => true]);
    }

    /**
     * store grid group settings in session
     * @return array
     */
    public function actionSetSessionIsGrouped()
    {
        $isGrouped = $_POST['isGrouped'];
        Yii::$app->session->set('qrIsGrouped', $isGrouped);
        return json_encode(['success' => true]);
    }

    /**
     * store chosen user selection retention in session
     * @return array
     */
    public function actionSetSessionIsRetained()
    {
        $isRetained = $_POST['isRetained'];
        Yii::$app->session->set('qrIsRetained', $isRetained);
        return json_encode(['success' => true]);
    }

    /**
     * set the currently selected package data in session.
     * @return array
     */
    public function actionSaveUserSelection()
    {
        $selectedItems = $_POST['selectedItems'];
        $sessionName = isset($_POST['sessionName']) ? $_POST['sessionName'] : 'selectedItems';
        Yii::$app->session->set($sessionName, $selectedItems);
        return json_encode(['success' => true]);
    }

    /**
     * set the currently selected package data in session to null.
     * @return array
     */
    public function actionEmptyUserSelection()
    {
        Yii::$app->session->set('selectedItems', null);
        return json_encode(['success' => true]);
    }

    /**
     * get the currently selected package data in session.
     * @return array
     */
    public function actionGetUserSelection()
    {
        $sessionName = isset($_POST['sessionName']) ? $_POST['sessionName'] : 'selectedItems';
        return json_encode(Yii::$app->session[$sessionName]);
    }

    /**
     * Function for checking existing active jobs for finds seeds
     * @return json summarizes the result with status and message
     */
    public function actionCheckBackgroundJob()
    {
        $method = $_POST['method'];
        $userId = $this->user->getUserId();

        $params = [
            "creatorDbId" => (string) $userId,
            "isSeen" => "false",
            "application" => "FIND_SEEDS",
            "method" => $method
        ];

        $label = ($method == "DELETE") ? 'Deletion' : 'Insertion';
        $result = $this->backgroundJobModel->searchAll($params, "sort=modificationTimestamp:desc");

        $totalCount = 0;

        if (isset($result) && !empty($result) && isset($result['status']) && $result['status'] == 200 && $result['totalCount'] > 0) {
            // there should only be 1 active job per user
            $data = $result['data'][0];
            $status = $data['jobStatus'];
            $totalCount = $result['totalCount'];

            $startTime = $result['data'][0]['startTime'];
            $endTime =  $result['data'][0]['endTime'];

            $timeConsumed = strtotime($endTime) - strtotime($startTime);

            if ($data['description'] == 'Clearing of working list items') {

                if ($status == 'DONE' || $status == 'FAILED') {
                    $params = [
                        "jobIsSeen" => "true"
                    ];
                    $requestBody[] = $data;
                    $result = $this->backgroundJobModel->update($requestBody, $params);

                    $entityDbId = $data['entityDbId'];
                    $deleteResult = $this->listsModel->deleteOne($entityDbId);

                    $message = "Deletion of working list items " . $status;
                    if ($status == 'DONE') {
                        return json_encode([
                            "count" => 0,
                            "status" => "DONE",
                            "message" => $message
                        ]);
                    } else {
                        return json_encode([
                            "count" => 0,
                            "status" => "waiting",
                            "message" => $message
                        ]);
                    }
                } else {
                    if ($status == 'IN_PROGRESS') {
                        $message = "Deletion of working list items in progress";
                    } else {
                        $message = "Deletion of working list items in queue";
                    }

                    return json_encode([
                        "count" => 1,
                        "status" => $status,
                        "message" => $message
                    ]);
                }
            } else if ($data['description'] == 'Preparing CSV Working List for downloading') {
                if ($status == 'DONE' || $status == 'FAILED') {
                    $params = [
                        "jobIsSeen" => "true"
                    ];
                    $requestBody[] = $data;
                    $this->backgroundJobModel->update($requestBody, $params);

                    $message = "Preparing CSV Working List for downloading $status";

                    if ($status == 'DONE') {
                        $filename = $data['jobRemarks'];
                        $downloadLink = "
                            <a
                                id = 'export-working-list-link-btn'
                                href='/index.php/occurrence/default/download-data?filename=$filename&dataFormat=csv'
                            >
                                here
                            </a>
                        ";

                        return json_encode([
                            "count" => 0,
                            "status" => "DONE",
                            "message" => "$message. Click $downloadLink to download your working list."
                        ]);
                    } else {
                        return json_encode([
                            "count" => 0,
                            "status" => "waiting",
                            "message" => $message
                        ]);
                    }
                } else {
                    if ($status == 'IN_PROGRESS') {
                        $message = "Preparing CSV Working List for downloading in progress";
                    } else {
                        $message = "Preparing CSV Working List for downloading in queue";
                    }

                    return json_encode([
                        "count" => 1,
                        "status" => $status,
                        "message" => $message
                    ]);
                }
            } else if ($data['description'] == 'Retrieving single-package record result ids') {

                $baseMessage = 'Retrieving single-package record result ids';
                $message = $baseMessage . " is " . strtolower($status) . ".";

                if ($status == 'DONE' || $status == 'FAILED') {
                    if ($status == 'DONE') {
                        $params = [
                            "jobIsSeen" => "true"
                        ];
                        $requestBody[] = $data;
                        $this->backgroundJobModel->update($requestBody, $params);
                    }

                    $message = $baseMessage . " is " . strtolower($status) . ".";
                } else {
                    $message .= ' The selected items count will be updated once the process has been completed.';
                    $status = 'waiting';
                }

                return json_encode([
                    "count" => 1,
                    "status" => $status,
                    "message" => $message
                ]);
            } else if ($data['message'] = "Insertion of working list items in progress" && $status == 'IN_PROGRESS') {

                $message = "Insertion of working list via background process is currently in progress. Check the notifications for progress updates.";
                $status = 'waiting';


                return json_encode([
                    "count" => 1,
                    "status" => $status,
                    "message" => $message
                ]);
            } else {

                if ($status == 'DONE' || $status == 'FAILED') {

                    // update is_seen
                    // update unseen notifications to seen
                    $params = [
                        "jobIsSeen" => "true"
                    ];
                    $requestBody[] = $data;
                    $result = $this->backgroundJobModel->update($requestBody, $params);
                    if ($status == 'DONE') {
                        $message = $label . " of working list items done. Manually refresh the results browser when needed.";
                    } else {
                        $message = $label . " of working list items failed";
                    }
                } else {

                    if ($status == 'IN_PROGRESS') {
                        $message = $label . " of working list items in progress";
                    } else {
                        $message = $label . " of working list items in queue";
                    }
                }

                return json_encode([
                    "count" => $totalCount,
                    "status" => $status,
                    "message" => $message,
                    "start" => $startTime,
                    "end" => $endTime,
                    "timeConsumed" => $timeConsumed
                ]);
            }
        }

        return json_encode([
            "count" => $totalCount,
            "status" => "waiting"
        ]);
    }

    /**
     * Renders view more information of a seed in widget
     * @return mixed seed information
     */
    public function actionViewInfo()
    {
        $id = isset($_POST['entity_id']) ? $_POST['entity_id'] : null;
        $program = isset($_POST['program']) ? $_POST['program'] : Yii::$app->userprogram->get('abbrev'); //current program

        $data = Seed::getSeedInfo($id);
        return Yii::$app->controller->renderAjax('@app/widgets/views/seed/tabs.php', [
            'data' => $data,
            'program' => $program
        ]);
    }

    /**
     * Renders input list search summary results in modal
     * 
     * @return success, result and message
     */
    public function actionShowResultsSummary()
    {
        //set filter value of params using session
        $params  = Yii::$app->session['fsEncodedParams'] ?? "";
        $param = json_decode($params);

        // retrieve input list type values
        $inputListFields = $this->variableFilterComponent->retrieveInputListConfig();

        //check if param is empty, in case of user clicking the button upon loading the tool
        //check if the search is using input list
        if (empty($param) || (empty($param->sortByInput) && !isset($param->names))) {
            return json_encode([
                "success" => false,
                "result" => "",
                "message" => "Results summary currently only supports information from searches using Input List Values"
            ]);
        }

        $columnAttrib = "";
        $columnValues = "";

        // build search params
        if (isset($param->names)) { // retrieve names parameters
            $columnAttrib = "designation";
            $columnValues = $param->names;
            $columnName = trim(explode('-', $inputListFields['name']['text'])[0]);
        } else if (isset($param->sortByInput) && !empty($param->sortByInput)) { // retrieve parameters from sortByInput
            $columnAttrib = $param->sortByInput->column;
            $columnValues = $param->sortByInput->value;
            $columnName = trim(explode('-', $inputListFields[$columnAttrib]['text'])[0]);
        }

        if (empty($columnAttrib) || empty($columnValues)) {
            return json_encode([
                "success" => false,
                "result" => "",
                "message" => "Error encountered retrieving input list values."
            ]);
        }

        //use param as parameters in this API call in json format
        $result = Yii::$app->api->getParsedResponse('POST', 'seed-packages-search?dataLevel=all', json_encode($param), '', true);

        //set value of getNames that has a result in the API call
        $getNames = array_column($result['data'], $columnAttrib);

        //set value of finalOtherNames that has a result in the API call; As the API call also include germplasmOtherNames
        $getOtherNames = array_column($result['data'], "germplasmOtherNames");
        $implodeOtherNames = implode(';', $getOtherNames);
        $finalOtherNames = explode(';', $implodeOtherNames);


        //separate array removing '|', set value of filterNames inputted in the Input List
        $columnValues = str_replace('equals ', '', $columnValues);
        $filterNames = explode('|', $columnValues);

        //merge both germplasmName and germplasmOtherNames for the final result
        $finalGetNames = array_merge($getNames, $finalOtherNames);

        //set normalize values for both searched and result germplasm names
        $normalizeGetNames = GermplasmName::getStandardizedGermplasmName($finalGetNames);
        $normalizeFilterNames = GermplasmName::getStandardizedGermplasmName($filterNames);

        //get normalized values
        $finalNormGetNames = array_column($normalizeGetNames, 'norm_input_product_name');
        $finalNormSearchNames = array_column($normalizeFilterNames, 'norm_input_product_name');

        //filter values of filterNames using getNames to obj
        $noResult = array_unique(array_diff($finalNormSearchNames, $finalNormGetNames)); // Remove duplicate

        //get names that has values
        $withResult = array_unique(array_intersect($finalNormSearchNames, $finalNormGetNames)); // Remove duplicate

        //filtered values convert obj to array, then array to string
        $textResult = implode(",\n", array_values($noResult));

        //Unfiltered input list item count entered by user
        $countItemAll = count($filterNames);

        //Filtered input list item count 
        $countItemWithResult = count($withResult) ?? '';

        return json_encode([
            "success" => true,
            "result" => $textResult,
            "message" => "Out of <strong>" . $countItemAll . "</strong> input list item(s),
                        <strong>" . $countItemWithResult . "</strong> item(s) has results. <br>
                         <br><strong>" . ucwords(strtolower($columnName)) . "</strong> values with no results: "
        ]);
    }

    /**
     * Exports all data across pages in the working list
     * 
     * @param int|string $itemCount total item count of the working list     * 
     */
    public function actionExportWorkingList($itemCount)
    {
        $workingListDbId = Yii::$app->session->get('workingListDbId');
        $columnConfig = $this->findSeedDataBrowserModel->getColumns();
        $csvAttributes = [];
        $csvHeaders = [];
        $thresholdValue = Yii::$app->config->getAppThreshold('FIND_SEEDS', 'exportWorkingList');

        $attribDataColCompat = [
            'gid' => 'GID',
            'src_entry' => 'sourceEntryCode',
            'source_entry' => 'seedSourceEntryNumber',
            'src_plot' => 'seedSourcePlotNumber',
            'plot_code' => 'seedSourcePlotCode',
            'experiment_name' => 'experiment',
            'occurrence_name' => 'experimentOccurrence',
            'season' => 'sourceStudySeason',
            'evaluation_stage' => 'experimentStage'
        ];

        foreach ($columnConfig as $col) {
            // Set CSV Headers
            $csvHeaders[] = $col['field_label'];

            // Set CSV Attributes
            $variableAbbrev = strtolower($col['variable_abbrev']);
            $csvAttributes[] = [
                'apiBodyParam' => $col['api_body_param'] ?? null,
                'colAbbrev' => isset($attribDataColCompat[$variableAbbrev]) && !empty($attribDataColCompat[$variableAbbrev]) ? $attribDataColCompat[$variableAbbrev] : $variableAbbrev
            ];
        }

        if (intval($itemCount) >= $thresholdValue) { // Background job export
            $description = 'Preparing CSV Working List for downloading';
            $timestamp = date("Ymd_His", time());
            $filename = 'Seed_Search_Working_List_Items_' . $timestamp;

            $csvAttributes = [
                'parentage',
                'designation',
                'seedManager',
                'label',
                'seedName',
                'GID',
                'quantity',
                'unit',
                'germplasmOtherNames',
                'experimentYear',
                'experimentType',
                'experiment',
                'experimentOccurrence',
                'harvestDate',
                'sourceStudySeason',
                'sourceHarvestYear',
                'experimentStage',
                'sourceEntryCode',
                'seedSourceEntryNumber',
                'seedSourcePlotNumber',
                'seedSourcePlotCode',
                'replication',
                'facility',
                'subFacility',
                'container',
                'moistureContent'
            ];

            $this->worker->invoke(
                'BuildCsvData',
                $description,
                'LIST',
                "$workingListDbId",
                'LIST',
                'FIND_SEEDS',
                'POST',
                [
                    'url' => "lists/$workingListDbId/members-search?sort=orderNumber",
                    'requestBody' => null,
                    'description' => $description,
                    'csvHeaders' => $csvHeaders,
                    'csvAttributes' => $csvAttributes,
                    'fileName' => $filename,
                    'httpMethod' => 'POST',
                ],
                [
                    'remarks' => $filename,
                ]
            );

            // Reload page
            $program = Yii::$app->session->get('program');
            Yii::$app->response->redirect(["/seedInventory/find-seeds/find?program=$program"]);
        } else { // Regular export
            // retrieve all Working List members
            $workingListData = $this->workingListModel->getWorkingListMembers($workingListDbId);

            $csvContent = [[]];
            $i = 1;

            if (!empty($workingListData)) {
                foreach ($csvHeaders as $value)
                    array_push($csvContent[0], $value);

                foreach ($workingListData as $value) {
                    $csvContent[$i] = [];
                    foreach ($csvAttributes as $v) {
                        if (isset($value[$v['apiBodyParam']])) {
                            array_push($csvContent[$i], $value[$v['apiBodyParam']]);
                        } else if (isset($value[$v['colAbbrev']])) {
                            array_push($csvContent[$i], $value[$v['colAbbrev']]);
                        } else {
                            array_push($csvContent[$i], null);
                        }
                    }

                    $i += 1;
                }
            } else {
                $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
                exit;
            }

            $timestamp = date("Ymd_His", time());
            $filename = 'Seed_Search_Working_List_Items_' . $timestamp . '.csv';

            header("Content-Disposition: attachment; filename={$filename}; Content-Type: application/csv;");
            $fp = fopen('php://output', 'wb');

            if ($fp === false) die('Failed to create CSV file.');

            foreach ($csvContent as $line) fputcsv($fp, $line);

            fclose($fp);

            exit;
        }
    }

    /**
     * Check user session selected items and single-package records
     */
    public function actionCheckCurrentSelectedIds()
    {

        // retrieve single-package records from most recet search
        $currentSelectedIds = json_decode(Yii::$app->session['fsSinglePackageRecordIds']);

        // retrieve most recent selected items
        $postParams = Yii::$app->request->post();
        $sessionStorageIds = json_decode($postParams['currSelection']);

        $sameSessionSelectedIds = ($sessionStorageIds === $currentSelectedIds ? true : false);

        return json_encode(['sameSessionSelectedIds' => $sameSessionSelectedIds]);
    }

    /**
     * Retrieve ids of all single-package records
     * 
     * @return Array single-package record ids
     */
    public function actionGetSinglePackageRecordIds()
    {
        $postParams = Yii::$app->request->post();

        // build search parameters
        $encodedParams = Yii::$app->session['fsEncodedParams'];

        $totalCount = intval($postParams['totalCount']);
        $threshold = 3000;

        $singlePkgRecordIds = [];
        $packageRecordIds = [];
        $bgProcess = false;

        if (isset($encodedParams) && !empty($encodedParams) && $totalCount != 0) {
            if ($totalCount > $threshold) {
                $bgProcess = true;
            } else {
                $singlePkgRecordIds = $this->findSeedDataBrowserModel->retrieveAllSinglePackageRecordIds($encodedParams);

                if (isset($singlePkgRecordIds) && !empty($singlePkgRecordIds)) {
                    $packageRecordIds = array_map('strval', $singlePkgRecordIds);
                }

                Yii::$app->session->set('fsSinglePackageRecordIds', json_encode($packageRecordIds));
            }
        }

        return json_encode(['bgProcess' => $bgProcess, 'ids' => $packageRecordIds]);
    }

    /**
     * Retrieve ids of all single-package records using background process worker
     */
    public function actionGetSinglePackageRecordIdsBgProcess()
    {
        $encodedParams = Yii::$app->session['fsEncodedParams'];
        $userId = $this->user->getUserId();

        $this->worker->invoke(
            'SeedSearchProcessor',
            'Retrieving single-package record result ids',
            'PACKAGE',
            $userId,
            'PACKAGE',
            'FIND_SEEDS',
            'POST',
            [
                'processName' => 'retrieve-single-package-records',
                'filters' => $encodedParams

            ],
            [
                'remarks' => '',
            ]
        );
    }

    /**
     * Get single-package record ids count from data browser configuration record
     * 
     * @return mixed stored package ids
     */
    public function actionGetSinglePackageRecordIdsCount()
    {
        $userId = $this->user->getUserId();

        $params = [
            'dataBrowserDbId' => 'seeds-search-grid',
            'name' => 'Single-package record IDs',
            'userDbId' => "" . $userId
        ];

        $results = $this->dataBrowserConfigModel->searchAll($params);

        if ($results['status'] != 200) {
            return 0;
        }

        $configData = $results['data'];
        if (isset($configData) && !empty($configData)) {

            $decodedData = json_decode($configData[0]['data']);
            $ids = array_map('strval', $decodedData->ids);

            Yii::$app->session->set('fsSinglePackageRecordIds', json_encode($ids));

            return json_encode(['ids' => $ids]);
        }
    }
}
