<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\controllers;

use yii\web\Controller;
use ChromePhp;

/**
 * Controller for  `seedInventory` module
 */
class UnitOfVolumeController extends Controller
{


    /**
     * Create Unit Conversion
     * @return array response
     */
    public function actionCreate(){

    	echo json_encode(\app\modules\seedInventory\models\ScaleConversionModel::saveConversion($_POST));
    }

    /**
     * Update Unit Conversion
     * @return array response
     */
    public function actionUpdate(){

    	 echo json_encode(\app\modules\seedInventory\models\ScaleConversionModel::saveConversion($_POST));
    }

    /**
     * Delete Unit Conversion
     * @return array response
     */
    public function actionDelete(){
   
        echo json_encode(\app\modules\seedInventory\models\ScaleConversionModel::deleteConversion($_POST));
    }


        /**
     * Get pattern
     * @return array response
     */
    public function actionGet(){

    	return $this->renderAjax('@app/modules/seedInventory/views/unit/forms/update',[ "data" => \app\modules\seedInventory\models\ScaleConversionModel::getConversion($_POST)]);

    }


}
