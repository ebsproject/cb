<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Experiment creation controller for the `SeedInventory` module
 */

namespace app\modules\seedInventory\controllers;

use yii\data\ArrayDataProvider;
use app\controllers\Dashboard;
use app\components\B4RController;
use app\models\Experiment;
use app\models\ExperimentSearch;
use app\models\Program;

use app\interfaces\modules\seedInventory\models\IWorkingList;
use app\modules\experimentCreation\models\ExperimentModel;

use app\models\EntryList;
use app\models\PlatformList;
use app\models\PlatformListMember;
use app\models\Entry;
use app\modules\experimentCreation\models\EntryOrderModel;

use Yii;

class ExperimentCreationController extends B4RController
{

    public function __construct(
        $id,
        $module,
        $config,
        public Dashboard $dashboard,
        public ExperimentSearch $experimentSearch,
        public Experiment $experiment,
        public Program $program,
        public IWorkingList $workingListModel,
        public ExperimentModel $experimentModel,
        public EntryList $mainEntryList,
        public PlatformList $platformlist,
        public PlatformListMember $platformListMember,
        public Entry $mainEntry,
        public EntryOrderModel $mainEntryOrderModel,
    ){
        parent::__construct($id, $module, $config);
    }


    /**
     * Get experiments for the Select Experiment modal
     *
     * @param string $program name of the current program
     */
    public function actionGetExperimentDataProvider($program) {
        // add condition for filtering owned experiments
        $isOwned = 'isOwned';
        $pageParams = '&sort=experimentYear:desc&experimentDbId:asc';

        $path = 'experiments-search?'.$isOwned.$pageParams;
        $params = [
            "experimentStatus" => "in: ('draft', 'entry list created')",
            "programCode"=>"equals ".$program
        ];
        $experimentData = Yii::$app->api->getParsedResponse('POST', $path, json_encode($params), null, true);

        $experimentDataProvider = new ArrayDataProvider([
            'key' => 'experimentDbId',
            'allModels' => $experimentData['data'],
            'pagination' => false
        ]);

        return Yii::$app->controller->renderPartial('@app/modules/seedInventory/views/seed-list/add_to_experiment', [
            'experimentDataProvider'=> $experimentDataProvider,
            'dataType'=>'exp-datatype'
        ]);
    }

    /**
     * Add to experiment's entry list
     *
     * @param string $program name of the current program
     */
    public function actionAddToExperiment($program) {
        $success = false;

        if (isset($_POST['experimentDbId']) && !empty($_POST['experimentDbId'])) {
            $experimentDbId = $_POST['experimentDbId'];
            $listDbId = Yii::$app->session['workingListDbId'];
            $experiment = $this->experimentModel->getOne($experimentDbId);

            $configData = $this->experimentModel->retrieveConfiguration('specify-entry-list', $program, $experimentDbId, $experiment['data']['dataProcessDbId']);
            // retrieve default values
            foreach($configData as $config){
                if(isset($config['default']) && !empty($config['default'])){
                    switch($config['variable_abbrev']){
                        case 'ENTRY_TYPE':
                            $entryType = $config['default'];
                            break;
                        case 'ENTRY_CLASS':
                            $entryClass = $config['default'];
                            break;
                        case 'ENTRY_ROLE':
                            $entryRole = $config['default'];
                            break;
                        default:
                            break;
                    }
                }
            }

            //Get the working experiment info
            $experiment = $this->experimentModel->getExperiment($experimentDbId);

            //Get entry list
            $entryListId = $this->mainEntryList->getEntryListId($experimentDbId, true);

            $param = [
                "listDbId"=>"equals ".$listDbId
            ];
            $list = Yii::$app->api->getResponse('POST','lists-search?showWorkingList=true',json_encode($param), '', true);
            $listRecord = $list['body']['result']['data'];
            $listMemberCount = $listRecord[0]['memberCount'];

            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createEntries') : 500;

            if($listMemberCount > $bgThreshold) {
                //call background process for creating entries
                $addtlParams = [
                    "description"=>"Creating of entry records",
                    "entity"=>"ENTRY",
                    "entityDbId"=>$experimentDbId,
                    "endpointEntity"=>"ENTRY", 
                    "application"=>"EXPERIMENT_CREATION"
                ];
                $dataArray = [
                    'entity'=>'list',
                    'entityId' => $listDbId."",
                    'defaultValues' => [
                        'entryListDbId' => $entryListId,
                        'entryType' => isset($entryType) ? $entryType : null,
                        'allowedEntryType' => null,
                        'entryClass' => isset($entryClass) ? $entryClass : null,
                        'entryRole' => isset($entryRole) ? $entryRole : null
                    ],
                    'processName' => 'create-entry-records',
                    'experimentDbId' => $experimentDbId.""
                ];
                $create = $this->experimentModel->create($dataArray, true, $addtlParams);

                $experiment = $this->experimentModel->getExperiment($experimentDbId);
                $experimentStatus = $experiment['experimentStatus'];

                if(!empty($experimentStatus)){
                    $status = explode(';',$experimentStatus);
                    $status = array_filter($status);
                }else{
                    $status = [];
                }

                if(!empty($create['data'])){
                    $bgStatus = $this->experimentModel->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                    if($bgStatus !== 'done' && !empty($bgStatus)){

                        $updatedStatus = implode(';', $status);
                        $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                        $this->experimentModel->updateOne($experimentDbId, ["experimentStatus"=>"$newStatus"]);

                        //update experiment
                        Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> are being created in the background. You may proceed with other tasks. You will be notified in this page once done.');
                        Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentDbId]);
                    }
                } else {
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentDbId]);
                }

            } else {
                //get platformlist members
                $list = $this->platformListMember->getListMembers($listDbId, true);

                $listMembers = $list[0]['members'];
                $programId = isset($experiment['programDbId']) ? $experiment['programDbId'] : "";
                $seedFilterIds = array();

                $createRecords = [];
                foreach($listMembers as $members){
                    if($members['isActive']){
                        $germplasmId = $members['germplasmDbId'];
                        $temp = [
                            'entryName' => $members['designation'],
                            'entryType' => !empty($entryType) ? $entryType : 'test',
                            'entryStatus' => 'active',
                            'entryListDbId' => "$entryListId",
                            'germplasmDbId' => "$germplasmId"
                        ];

                        if(!empty($entryClass)){
                            $temp['entryClass'] = $entryClass;
                        }

                        if(!empty($experiment['experimentType']) && ($experiment['experimentType'] != 'Breeding Trial' && $experiment['experimentType'] != 'Observation')){
                            if(!empty($entryRole)){
                                $temp['entryRole'] = $entryRole;
                            }
                        }

                        if(isset($members['seedDbId'])){
                            $seedDbId = $members['seedDbId'];
                            $temp['seedDbId'] = "$seedDbId";
                        }
                        if(isset($members['packageDbId']) && $members['programDbId'] == $programId){
                            $packageDbId = $members['packageDbId'];
                            $temp['packageDbId'] = "$packageDbId";
                        }

                        $createRecords[] = $temp;
                    }
                }

                $createdEntryIds = $this->mainEntry->createRecords($createRecords);

                //update experiment status
                $this->mainEntryOrderModel->entryListAfterUpdate($experimentDbId);

                //Update seedDbId for entry if list is product
                if(in_array($list[0]['type'], ['designation', 'germplasm'])){
                    $this->experimentModel->autoSelectSinglePackage($entryListId, $experimentDbId);
                }

                Yii::$app->session->setFlash('info', '<i class="fa fa-info-circle"></i> The entry records for <strong>'.$experiment['experimentName'].'</strong> have been successfully created.');
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentDbId]);
            }
            $success = true;
        }

        $result = [
            'success' => $success
        ];

        return json_encode($result);
    }
}
?>