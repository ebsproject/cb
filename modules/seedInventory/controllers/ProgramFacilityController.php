<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\modules\seedInventory\controllers;

use yii\web\Controller;
use ChromePhp;

/**
 * Controller for  `seedInventory` module
 */
class ProgramFacilityController extends Controller
{


    /**
     * Create Unit Conversion
     * @return array response
     */
    public function actionCreate(){

    	echo json_encode(\app\modules\seedInventory\models\ProgramFacilityModel::saveRecord($_POST));
    }

    /**
     * Update Unit Conversion
     * @return array response
     */
    public function actionUpdate(){

 
    	echo json_encode(\app\modules\seedInventory\models\ProgramFacilityModel::updateRecord($_POST));
    }

    /**
     * Delete Unit Conversion
     * @return array response
     */
    public function actionDeleteRecord(){
   
       echo json_encode(\app\modules\seedInventory\models\ProgramFacilityModel::deleteRecord($_POST));
    }


        /**
     * Get pattern
     * @return array response
     */
    public function actionGet(){

        return $this->renderAjax('forms/update',[ "data" => \app\modules\seedInventory\models\ProgramFacilityModel::getList($_POST)]);

    }

    public function actionGetSubfacility(){

        echo json_encode(\app\modules\seedInventory\models\ProgramFacilityModel::getSubfacility($_POST));

    }


}
