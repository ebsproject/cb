<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\modules\seedInventory\controllers;

use yii\web\Controller;

/**
 * Controller for Configuration of Seedlot Code Pattern in the `seedInventory` module
 */
class SeedlotCodePatternController extends Controller
{

 public function actionSample()
    {
        return $this->render('forms/sample');
    }
    /**
     * Get pattern
     * @return array response
     */
    public function actionGet(){

    	return $this->renderAjax('forms/update',[ 
    			"data" => \app\modules\seedInventory\models\ConfigureSeedlotCode::getPattern($_POST)
    		]);
    }

    /**
     * Create pattern
     * @return array response
     */
    public function actionCreate(){

    	echo json_encode(\app\modules\seedInventory\models\ConfigureSeedlotCode::savePattern($_POST));
    }

    /**
     * Update pattern
     * @return array response
     */
    public function actionUpdate(){

    	echo json_encode(\app\modules\seedInventory\models\ConfigureSeedlotCode::updatePattern($_POST));
    }

    /**
     * Delete pattern
     * @return array response
     */
    public function actionDelete(){

        echo json_encode(\app\modules\seedInventory\models\ConfigureSeedlotCode::deletePattern($_POST));
    }

    /**
     * Delete all pattern
     * @return array response
     */
    public function actionDeleteAll(){

        echo json_encode(\app\modules\seedInventory\models\ConfigureSeedlotCode::deleteAllPattern());
    }

    /**
     * Set pattern to active or inactive
     * @return array response
     */
    public function actionUpdateIsActive(){

        echo json_encode(\app\modules\seedInventory\models\ConfigureSeedlotCode::updateIsActive($_POST));
    }
}
