<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use kartik\sortable\Sortable;
use app\modules\seedInventory\models\UnitOfVolumeModel;

\yii\jui\JuiAsset::register($this);

$scaleValueArr = UnitOfVolumeModel::getUnitScaleValue();
$unitScaleList = UnitOfVolumeModel::getUnitScaleList();
?>


<div id="add-conversion-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">

    <div class="modal-dialog">

        <form class="col s12" id="create-volume-form" action="post">

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4><i class="material-icons">add</i> Create Unit of Volume/Qty Conversion</h4>
                </div>
                <div class="modal-body" style="max-height: 730px;">
                    <div class="row">

                        <div class="col s12">

                            <label class="control-label" style="font-size:1.5rem">Unit of Volume/Qty<span style="color:red">*</span></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'unit_of_volume',
                                'data' => $scaleValueArr,
                                'options' => ['placeholder' => 'Select', 'id' => 'select-scale_value-id', 'style' => 'width:50%'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'id' => 'select-scale_value-id'
                                ],
                            ]);
                            ?>
                        </div>

                    </div>
                    <div class="row" style="font-size:1.5rem">

                        1 <span id="selectedVolumeId"> </span> is equivalent to <br>

                    </div>

                    <?php foreach ($unitScaleList as $s) { ?>

                        <div class="row row-unit-scale" id="row-<?php echo $s['id'] ?>">
                            <div class="input-field col s12">
                                <input class='conversion_value' name=<?php echo $s['id'] ?> id=<?php echo $s['id'] ?> type="number"  min="0" step="any" placeholder="" class="validate">
                                <label for="<?php echo $s['id'] ?>" style="font-size:1.5rem"><?php echo $s['value'] ?> </label>
                                <span class="helper-text" data-error="Please fill conversion name"></span>
                            </div>
                        </div>

                    <?php } ?>
                </div>
                <div class="modal-footer">

                    <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

                    <?php
                    echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>' . Yii::t('app', 'Submit'), [
                        'class' => 'btn create-volume-btn waves-effect',
                        'disabled' => true,
                        'form' => 'create-volume-form'
                    ]);
                    ?>
                </div>
            </div>
        </form>

    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {

        var selectedScale;


        $('#add-conversion-modal').on('show.bs.modal', function(e) {

            $("#select-scale_value-id").val(null).trigger('change');
            $(".conversion_value").each(function() {
                $(this).val(null);
            })
        });


        $("#select-scale_value-id").on('change', function(e) {

            selectedScale = $("#select-scale_value-id").val();
            $(".create-volume-btn").attr('disabled', false);

            $("#selectedVolumeId").html($(this).select2('data')[0]['text']);
            $(".row-unit-scale").removeClass('hidden');
            $(".conversion_value").each(function() {

                if ($(this).attr('id') == $("#select-scale_value-id").val()) {
                    $("#row-" + $(this).attr('id')).addClass('hidden');
                }
            })
        });

        $("#create-volume-form").on('submit', function(e) {
            e.preventDefault();
            var conversion = [];

            $(".conversion_value").each(function() {
                //alert($(this).val() + $(this).attr('id'));
                var conversionVal = 0;
                if ($(this).val()) {
                    conversionVal = $(this).val();
                }

                if (selectedScale == $(this).attr('id')) {
                    conversionVal = 1;
                }

                conversion.push({
                    "source_unit_id": $("#select-scale_value-id").val(),
                    "source_target_unit_id": $(this).attr('id'),
                    "conversion_value": conversionVal
                })
            })

            createJson = {
                'data': conversion
            }

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['unit-of-volume/create']); ?>',
                type: 'post',
                data: {
                    value: createJson,
                },
                dataType: 'json',
                success: function(response) {

                    if (response.success) {

                        $('#add-conversion-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully created record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-unit-volume-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });


        });

    });
</script>