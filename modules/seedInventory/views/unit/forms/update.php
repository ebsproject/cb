<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use kartik\sortable\Sortable;
use app\modules\seedInventory\models\UnitOfVolumeModel;

extract($data);

ChromePhp::log($scaleValueArr);
ChromePhp::log($unitScaleList);

?>

<form class="col s12" id="update-volume-form" action="post">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="material-icons">add</i> Update Unit of Volume/Qty Conversion</h4>
            </div>
            <div class="modal-body" style="max-height: 730px;">
                <div class="row">

                    <div class="col s12">

                        <label class="control-label-unit" style="font-size:1.5rem" id="<?php echo $scaleValueArr['id']; ?>">Unit of Volume/Qty: <span style="color:black;font-size:1.5rem"><?php echo $scaleValueArr['description'].'('.$scaleValueArr['value'].')';?></span></label>
                        <!-- <?php
                        //echo Select2::widget([
                            // 'name' => 'unit_of_volume',
                            // 'data' => $scaleValueArr,
                            // 'options' => ['placeholder' => 'Select', 'id' => 'select-scale_value-id', 'style' => 'width:50%'],
                            // 'pluginOptions' => [
                            //     'allowClear' => true,
                            //     'id' => 'select-scale_value-id'
                            // ],
                        // ]);

                        ?> -->
                    </div>

                </div>
                <div class="row" style="font-size:1.5rem">

                    1 <span id="selectedVolumeId"> </span> is equivalent to <br>

                </div>

                <?php foreach ($unitScaleList as $s) { 
                    
                    if(!($s['value']==$scaleValueArr['value'])){
                    ?>

                    <div class="row row-unit-scale" id="row-<?php echo $s['id'] ?>">
                        <div class="input-field col s12">
                        <label class="active" style="font-size:1.5rem"><?php echo $s['value'] ?></label>
                            <input class='conversion_update_value' name=<?php echo $s['id'] ?> id=<?php echo $s['id'] ?> type="number" min="0" step="any" placeholder=" "  value="<?php echo $s['conversion_value'];?>" > 
                        </div>
                    </div>

                <?php }} ?>
            </div>
            <div class="modal-footer">

                <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

                <?php
                echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>' . Yii::t('app', 'Submit'), [
                    'class' => 'btn update-volume-btn waves-effect',
                    'disabled' => false,
                    'form' => 'update-volume-form'
                ]);
                ?>
            </div>
        </div>

    </div>

</form>

<script type="text/javascript">

    $(document).ready(function () {

        var selectedScale;


        $("#update-volume-form").on('submit', function (e) {
            e.preventDefault();
            var conversion = [];

            $(".conversion_update_value").each(function () {
                //alert($(this).val() + $(this).attr('id'));
                var conversionVal=0;
                if($(this).val()){
                    conversionVal=$(this).val();
                }

                if(selectedScale==$(this).attr('id')){
                    conversionVal=1;
                }

                conversion.push({
                    "source_unit_id": $(".control-label-unit").attr('id'),
                    "source_target_unit_id": $(this).attr('id'),
                    "conversion_value": conversionVal
                })
            })

            createJson={
                'data':conversion
            }

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['unit-of-volume/update']); ?>',
                type: 'post',
                data: {
                    value: createJson,
                },
                dataType: 'json',
                success: function (response) {

                    if (response.success) {
                       
                        $('#conversion-update-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-unit-volume-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });


        });

    });





</script>