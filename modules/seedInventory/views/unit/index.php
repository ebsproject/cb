<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\web\JqueryAsset;
use kartik\select2\Select2;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use \app\modules\seedInventory\models\UnitOfVolumeModel;


$actionColumns = [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '1%'
    ],
    [
        'label' => "Checkbox",
        'header' => '<input type="checkbox" class="filled-in" data-id-string="" id="seed-inventory-unit-conversion_check-all" /><label style="padding-left: 20px;" for="seed-inventory-unit-conversion_check-all"></label>',
        'content' => function ($model) {
            return '
            <input class="seed-inventory-unit-conversion_checkbox filled-in" row-id="' . $model['id'] . '"  type="checkbox" id="' . $model['id'] . '" />
            <label style="padding-left: 20px;" for="' . $model['id'] . '"></label>';
        },

        'width' => '1%'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Actions',
        'noWrap' => true,
        'template' => '{edit}',
        'buttons' => [
            'edit' => function ($url, $model, $key) {

                return yii\helpers\Html::a(
                    '<i class="material-icons">create</i>',
                    '#',
                    [
                        'style' => 'cursor: pointer;',
                        'class' => 'update-conversion-btn',
                        'title' => 'Edit Unit of Volume',
                        'data-id' => $model['id'],
                        //                    'data-pattern_format' =>$model['pattern_format'],
                        'data-toggle' => "modal",
                        "data-target" => "#conversion-update-modal",

                    ]

                );
            },
        ]
    ]
];

$columns = array();
$column = [
[
    'attribute' => 'unit',
    'label' => 'Unit',
    'hAlign' => 'center',
    'vAlign' => 'middle',
],
[
    'attribute' => 'description',
    'label' => 'Description',
    'hAlign' => 'center',
    'vAlign' => 'middle',
]];


$columns=array_merge($columns, $column);

$otherColumns = UnitOfVolumeModel::getUnitScaleDefined();

foreach ($otherColumns as $h) {
    $col = [
        'attribute' => $h['value'],
        'label' => $h['value'],
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ];
    array_push($columns, $col);
}

$gridColumns = array_merge($actionColumns, $columns);
$searchModel = new \app\modules\seedInventory\models\UnitOfVolumeModel();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

$dynagrid = kartik\dynagrid\DynaGrid::begin([
    'columns' => $gridColumns,
    'theme' => 'simple-bordered',
    'showPersonalize' => true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'options' => ['id' => 'seed-inventory-unit-volume-grid', 'class' => ""],
    'gridOptions' => [
        'moduleId' => 'gridview',
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => ['id' => 'seed-inventory-unit-volume-grid'],
            'beforeGrid' => '',
            'afterGrid' => ''
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => false,
        'responsiveWrap' => false,

        'panel' => [
            'before' => \Yii::t('app', 'This is the browser for all the unit of volume conversion. '),
        ],
        'toolbar' =>  [
            [
                'content' => yii\helpers\Html::a('<i class="material-icons right">add</i> ' .
                    \Yii::t('app', 'Create'), '#', [
                    'id' => 'add-pattern-btn-id',
                    'class' => 'btn waves-effect waves-light white-text light-green darken-3',
                    'style' => 'margin-right:5px;',
                    'title' => \Yii::t('app', 'Create Seedlot code pattern'),
                    'data-toggle' => "modal",
                    "data-target" => "#add-conversion-modal",
                ]) . ' ' .
                    yii\helpers\Html::a('<i class="material-icons right">delete_forever</i>' . \Yii::t('app', 'Delete'), '#', [
                        'id' => 'conversion-delete-btn-id',
                        'class' => 'btn red darken-3 waves-effect waves-light',
                        'style' => 'margin-right:5px;',
                        'title' => \Yii::t('app', 'Delete unit conversion'),
                        'data-toggle' => "modal",
                        "data-target" => "#conversion-delete-modal",
                    ]) . ' ' .
                    yii\helpers\Html::a('<i class="glyphicon glyphicon-repeat"></i>', '#', [
                        'class' => 'btn btn-default',
                        'title' => \Yii::t('app', 'Reset grid'),
                        'id' => 'unit-reset-grid-id'
                    ]) . '{dynagridFilter}{dynagridSort}{dynagrid}',
            ],

            'exportConfig' => false,

        ]
    ]

]);
kartik\dynagrid\DynaGrid::end();
?>

<!-- Create Modal -->
<?php echo $this->render('forms/create.php'); ?>

<!-- Update Modal -->
<div id="conversion-update-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">

</div>


<!-- Delete Modal -->
<div id="conversion-delete-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title "><i class="material-icons amber-text text-darken-3">warning</i><?php echo \Yii::t('app', 'Delete Conversion'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <span class="hidden" id="delete-conversion-count-0"><?php echo \Yii::t('app', 'Select at least one record.'); ?></span>

                    <span style="" id="delete-conversion-count" class="hidden"><?php echo \Yii::t('app', 'You are about to delete '); ?> 0 <?php echo \Yii::t('app', 'record(s)'); ?>.</span>

                </div>
            </div>

            <div class="modal-footer">
                <a class="btn-flat " data-dismiss="modal" style="margin-right:5px;background-color:transparent !important; text-transform:none !important;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

                <button type="button" class="btn hidden conversion-delete-selected-btn" id="conversion-delete-selected-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app', 'Delete'); ?></button>

                <button type="button" class="btn hidden conversion-delete-selected-btn" id="conversion-delete-all-btn"" data-dismiss=" modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app', 'Delete'); ?></button>

                <!-- <button type="button" class="btn hidden" data-dismiss="modal" id="conversion-delete-all-btn" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app', 'Apply to all'); ?></button> -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {


        $('#unit-reset-grid-id').on('click', function(e) {

            e.preventDefault();
            $.pjax.reload({
                container: '#seed-inventory-unit-volume-grid-pjax',
                replace: true,
                url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
            });
        });

        // Select row(s)
        $(document).on('click', "#seed-inventory-unit-volume-grid tbody tr", function(e) {
            this_row = $(this).find('input:checkbox')[0];

            if (this_row.checked) {

                this_row.checked = false;
                $(this).removeClass("warning");

                if ($("#seed-inventory-unit-conversion_check-all").prop("checked") === true) {
                    $("#seed-inventory-unit-conversion_check-all").prop("checked", false);
                }

            } else {

                $(this).addClass("warning");
                this_row.checked = true;
                // $("#" + this_row.id).prop("checked");
            }
        });

        // select/unselect all
        $(document).on('click', "#seed-inventory-unit-conversion_check-all", function(e) {

            if ($(this).prop("checked") === true) {
                $(this).attr('checked', 'checked');
                $('.seed-inventory-unit-conversion_checkbox').attr('checked', 'checked');
                $('.seed-inventory-unit-conversion_checkbox').prop('checked', true);
                $("input:checkbox.seed-inventory-unit-conversion_checkbox").parent("td").parent("tr").addClass("warning");

                $("input:checkbox.seed-inventory-unit-conversion_checkbox").each(function() {
                    if ($(this).prop("checked") == true) {
                        // alert($(this).attr("row-id")));
                        selectedRowConversion.push($(this).attr("row-id"));
                    }
                });


            } else {

                $(this).removeAttr('checked');
                $('.seed-inventory-unit-conversion_checkbox').prop('checked', false);
                $('.seed-inventory-unit-conversion_checkbox').removeAttr('checked');
                $("input:checkbox.seed-inventory-unit-conversion_checkbox").parent("td").parent("tr").removeClass("warning");
                selectedRowConversion = [];
            }
        });


        var selectedRowConversion = [];
        // renders delete modal dialog
        $('#conversion-delete-modal').on('show.bs.modal', function(e) {

            if ($('#seed-inventory-unit-conversion_check-all').prop("checked") === true) {
                $("#conversion-delete-all-btn").removeClass("hidden");
                $(".conversion-delete-selected-btn").addClass("hidden");
                $("#conversion-delete-selected-btn").removeClass("hidden");
                $("#delete-conversion-count-0").addClass("hidden");
                $("#delete-conversion-count").removeClass("hidden");

                $("#delete-conversion-count").html("<?php echo \Yii::t('app', 'You have selected all records. Are you sure you want to delete all records?               '); ?> ");

            } else {
                $("input:checkbox.seed-inventory-unit-conversion_checkbox").each(function() {
                    if ($(this).prop("checked") == true) {
                        // alert($(this).attr("row-id")));
                        selectedRowConversion.push($(this).attr("row-id"));
                    }
                });

                if (selectedRowConversion.length == 0) {

                    var notif = "<i class='material-icons amber-text'>warning</i> Please select at least 1 record.";
                    Materialize.toast(notif, 5000);
                    return e.preventDefault();
                } else {
                    if (selectedRowConversion.length == 1) {
                        $("#delete-conversion-count").html("<?php echo \Yii::t('app', 'You are about to delete '); ?><b> " + selectedRowConversion.length + "</b> <?php echo \Yii::t('app', 'record.'); ?>");
                    } else {
                        $("#delete-conversion-count").html("<?php echo \Yii::t('app', 'You are about to delete '); ?><b> " + selectedRowConversion.length + "</b> <?php echo \Yii::t('app', 'records.'); ?>");
                    }
                    $("#delete-conversion-count").removeClass("hidden");
                    $("#delete-conversion-count-0").addClass("hidden");

                    $(".conversion-delete-selected-btn").addClass("hidden");
                    $("#conversion-delete-all-btn").removeClass("hidden");
                }
            }
        });

        // delete selected conversion
        $('.conversion-delete-selected-btn').on('click', function(e) {

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['unit-of-volume/delete']); ?>',
                type: 'post',
                data: {
                    id: selectedRowConversion.join(","),
                },
                dataType: 'json',
                success: function(response) {

                    if (response.success) {
                        $('#conversion-delete-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully deleted record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-unit-volume-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });
        });

        $('#conversion-delete-modal').on('hide.bs.modal', function(e) {
            selectedRowConversion = [];
            $("#delete-conversion-count-0").removeClass("hidden");
            $("#delete-conversion-count").addClass("hidden");
            $("#conversion-delete-all-btn").addClass("hidden");
            $(".conversion-delete-all-btn").addClass("hidden");
        });


        $(document).on('click', '.update-conversion-btn', function(e) {


            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['unit-of-volume/get']); ?>',
                type: 'post',
                data: {
                    id: $(this).data('id')

                },
                success: function(response) {
                  
                    if (response) {
                        $("#conversion-update-modal").html(response);
                        $('.loading-div').html('');
                    }
                },
                error: function() {

                }
            });

        });


    });
</script>