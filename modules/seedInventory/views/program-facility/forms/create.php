<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use kartik\sortable\Sortable;
use app\modules\seedInventory\models\ProgramFacilityModel;
use yii\helpers\Url;
use yii\bootstrap\Button;

\yii\jui\JuiAsset::register($this);

$programArr = ProgramFacilityModel::getProgramList();
$facilityLevel0 = ProgramFacilityModel::getFacilityListLevel0();
// $facilityLevel1 = ProgramFacilityModel::getSubFacilityListLevel1(1199);
// ChromePhp::log($programArr);

?>


<div id="add-program-facility-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">

    <div class="modal-dialog modal-lg">


        <form class="col s12" id="create-program-facility-form" action="post">

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4><i class="material-icons">add</i> Create Program Facility </h4>
                </div>
                <div class="modal-body" style="max-height: 730px; height:280px;">
                    <div class="row">

                        <div class="col s12">

                            <label for="program_name"><?php echo Yii::t('app', 'Program'); ?><span style="color:red">*</span></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'program',
                                'data' => $programArr,
                                'options' => [
                                    'placeholder' => 'Select a program ...',
                                    'id' => 'select-program-id2',
                                    'style' => 'width:80%',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'id' => 'select-program-id2'
                                ],
                            ]);
                            ?>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col s12">

                            <label for="facility_name"><?php echo Yii::t('app', 'Facility'); ?><span style="color:red">*</span></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'facility',
                                'data' => $facilityLevel0,
                                'options' => [
                                    'placeholder' => 'Select a facility ...',
                                    'id' => 'select-facility-id',
                                    'style' => 'width:80%',
                                    'class' => 'select2-facility-filter-selection'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'id' => 'select-facility-id',
                                    'multiple' => false
                                ],
                            ]);
                            ?>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col s12">

                            <label for="package_type_name"><?php echo Yii::t('app', 'Subfacility'); ?><span style="color:red">*</span></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'subfacility',
                                'data' => [],
                                'options' => [
                                    'placeholder' => 'Select a facility ...',
                                    'id' => 'select-facility-id2',
                                    'style' => 'width:80%',
                                    'class' => 'select2-facility-filter-selection2'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'id' => 'select-facility-id2',
                                    'multiple' => true
                                ],
                            ]);
                            ?>
                        </div>

                    </div>


                </div>

            </div>
            <div class="modal-footer">

                <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

                <?php
                echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>' . Yii::t('app', 'Submit'), [
                    'class' => 'btn create-facility-btn waves-effect',
                    'disabled' => true,
                    'form' => 'create-program-facility-form'
                ]);
                ?>
            </div>

        </form>

    </div>

</div>



<script type="text/javascript">
    $(document).ready(function() {



        $('#add-program-facility-modal').on('show.bs.modal', function(e) {

            $("#select-program-id2").val(null).trigger('change');
            $("#select-facility-id").val(null).trigger('change');

        });

        $("#select-facility-id").on('change', function(e) {

            if ($("#select-facility-id2").val() == '' || $("#select-facility-id").val() == '' || $("#select-program-id2").val() == '') {
                $(".create-facility-btn").attr('disabled', true);
            } else {
                $(".create-facility-btn").attr('disabled', false);
            }
        });

        $("#select-program-id2").on('change', function(e) {

            if ($("#select-facility-id2").val() == '' || $("#select-facility-id").val() == '' || $("#select-program-id2").val() == '') {
                $(".create-facility-btn").attr('disabled', true);
            } else {
                $(".create-facility-btn").attr('disabled', false);
            }
        });

        //disable create button when program name and program is not filled
        $("#select-facility-id").on('change', function(e) {

            if ($("#select-facility-id2").val() == '' || $("#select-facility-id").val() == '' || $("#select-program-id2").val() == '') {
                $(".create-facility-btn").attr('disabled', true);
            } else {
                $(".create-facility-btn").attr('disabled', false);
            }
        });

        $("#select-facility-id2").on('change', function(e) {

            if ($("#select-facility-id2").val() == '' || $("#select-facility-id").val() == '' || $("#select-program-id2").val() == '') {
                $(".create-facility-btn").attr('disabled', true);
            } else {
                $(".create-facility-btn").attr('disabled', false);
            }
        });


        $("#select-facility-id").on('change', function(e) {

            // var data = $('.select2-facility-filter-selection').val();
            var data = $('#select-facility-id').val();

            $('#select-facility-id2').val(null).trigger('change');

            if (data != "") {

                $.ajax({
                    url: '<?php echo yii\helpers\Url::toRoute(['program-facility/get-subfacility']); ?>',
                    type: 'post',
                    data: {
                        facility_id: data,

                    },
                    dataType: 'json',
                    success: function(response) {
                        $('#select-facility-id2').get(0).options.length = 0;
                        if (response) {
                            var s1 = document.getElementById('select-facility-id2');


                            $.each(response, function(index, value) {
                                var s1 = document.getElementById('select-facility-id2');
                                var newOption = document.createElement("option");
                                newOption.value = index;
                                newOption.innerHTML = value;
                                s1.options.add(newOption);
                            });

                        } else {
                            var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                            Materialize.toast(notif, 5000);
                        }
                    }
                });
            }



        }).trigger('change');


        $("#create-program-facility-form").on('submit', function(e) {
            e.preventDefault();
            var packageTypeListofProgram = [];

            var program_id = $("#select-program-id2").val();
            var data = $('.select2-facility-filter-selection2').val();




            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['program-facility/create']); ?>',
                type: 'post',
                data: {
                    "program_id": program_id,
                    "facility_list": data
                },
                dataType: 'json',
                success: function(response) {

                    if (response.success) {

                        $('#add-program-facility-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully created record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-facility-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });


        });

    });
</script>