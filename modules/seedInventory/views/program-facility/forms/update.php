<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use kartik\sortable\Sortable;
use app\modules\seedInventory\models\ProgramFacilityModel;
use ChromePhp;

\yii\jui\JuiAsset::register($this);
extract($data);
$programArr = ProgramFacilityModel::getProgramList();
$facilityLevel0 = ProgramFacilityModel::getFacilityListLevel0();
$program_id_selected = (int) $program_id;




//  ChromePhp::log(explode(",",$subfacility_list));

$SelectedIds = $subfacility_list;
$SelectedIds = array_map(function ($elem) {
    return intval($elem);
}, $SelectedIds);

$subfacilityJson = json_encode($SelectedIds);
// ChromePhp::log($subfacilityJson);

?>

<form class="col s12" id="update-program-facility-form" action="post">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="material-icons">add</i> Update Program Facility </h4>
            </div>
            <div class="modal-body" style="max-height: 730px; height:280px;">
                <div class="row">

                    <div class="col s12">

                        <label for="program_name"><?php echo Yii::t('app', 'Program'); ?><span style="color:red">*</span></label>
                        <?php
                        echo Select2::widget([
                            'name' => 'program',
                            'data' => $programArr,
                            'options' => [
                                'placeholder' => 'Select a program ...',
                                'id' => 'update-select-program-id3',
                                'style' => 'width:80%',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id' => 'update-select-program-id3',
                                'disabled' => true

                            ],
                            'value' => $program_id
                        ]);
                        ?>

                    </div>

                </div>

                <div class="row">

                    <div class="col s12">

                        <label for="facility_name"><?php echo Yii::t('app', 'Facility'); ?><span style="color:red">*</span></label>
                        <?php
                        echo Select2::widget([
                            'name' => 'facility',
                            'data' => $facilityLevel0,
                            'options' => [
                                'placeholder' => 'Select a facility ...',
                                'id' => 'update-select-facility-id',
                                'style' => 'width:80%',
                                'class' => 'select2-facility-filter-selection'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id' => 'update-select-facility-id',
                                'multiple' => false
                            ],
                            'value' => $facility_id
                        ]);
                        ?>
                    </div>

                </div>


                <div class="row">

                    <div class="col s12">

                        <label for="package_type_name"><?php echo Yii::t('app', 'Subfacility'); ?><span style="color:red">*</span></label>
                        <?php
                        echo Select2::widget([
                            'name' => 'subfacility',
                            'data' => [],
                            'options' => [
                                'placeholder' => 'Select a facility ...',
                                'id' => 'update-select-facility-id2',
                                'style' => 'width:80%',
                                'class' => 'select2-facility-filter-selection2'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id' => 'update-select-facility-id2',
                                'multiple' => true
                            ],
                            'value' => $subfacility_list

                            //$subfacility_list                       
                        ]);
                        ?>
                    </div>

                </div>
            </div>

        </div>
        <div class="modal-footer">

            <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

            <?php
            echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>' . Yii::t('app', 'Submit'), [
                'class' => 'btn update-program-facility-btn waves-effect',
                'disabled' => false,
                'form' => 'update-program-facility-form'
            ]);
            ?>
        </div>
    </div>
</form>



<script type="text/javascript">
    $(document).ready(function() {

        $("#update-select-program-id3").on('change', function(e) {

            if ($("#update-select-program-id3").val() == '' || $("#update-select-facility-id2").val() == '' || $("#update-select-facility-id" ).val()== '') {
                $(".update-program-facility-btn").attr('disabled', true);
            } else {
                $(".update-program-facility-btn").attr('disabled', false);
            }
        });

        $("#update-select-facility-id2").on('change', function(e) {

            if ($("#update-select-program-id3").val() == '' || $("#update-select-facility-id2").val() == '' || $("#update-select-facility-id" ).val()== '') {
                $(".update-program-facility-btn").attr('disabled', true);
            } else {
                $(".update-program-facility-btn").attr('disabled', false);
            }
        });
        // //disable create button when program name and program is not filled
        $("#update-select-facility-id").on('change', function(e) {

            if ($("#update-select-program-id3").val() == '' || $("#update-select-facility-id2").val() == '' || $("#update-select-facility-id" ).val()== '') {
                $(".update-program-facility-btn").attr('disabled', true);
            } else {
                $(".update-program-facility-btn").attr('disabled', false);
            }
        });



        $("#update-select-facility-id").on('change', function(e) {

            var data = $('#update-select-facility-id').val();

            if (data != "") {

                $.ajax({
                    url: '<?php echo yii\helpers\Url::toRoute(['program-facility/get-subfacility']); ?>',
                    type: 'post',
                    data: {
                        facility_id: data,

                    },
                    dataType: 'json',
                    success: function(response) {
                        $('#update-select-facility-id2').get(0).options.length = 0;
                        if (response) {
                            var s1 = document.getElementById('update-select-facility-id2');


                            $.each(response, function(index, value) {
                                var s1 = document.getElementById('update-select-facility-id2');
                                var newOption = document.createElement("option");
                                newOption.value = index;
                                newOption.innerHTML = value;
                                s1.options.add(newOption);
                            });

                            $('#update-select-facility-id2').val(<?php echo $subfacilityJson ?>);
                            $(".update-program-facility-btn").attr('disabled', false);


                        } else {
                            var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                            Materialize.toast(notif, 5000);
                        }
                    }
                });
            }

        }).trigger('change');


        $("#update-program-facility-form").on('submit', function(e) {
            e.preventDefault();
            var packageTypeListofProgram = [];

            var program_id = $("#update-select-program-id3").val();
            var facility_id = $("#update-select-facility-id2").val();
            var old_facility_id = JSON.parse('<?php echo json_encode($subfacility_list); ?>');



            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['program-facility/update']); ?>',
                type: 'post',
                data: {
                    "program_id": program_id,
                    "facility_id": facility_id,
                    "old_facility_id": old_facility_id
                },
                dataType: 'json',
                success: function(response) {

                    if (response.success) {

                        $('#program-facility-update-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully update record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-facility-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });


        });

    });
</script>