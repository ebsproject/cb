<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View of Config Seedlot Code
**/

use yii\web\JqueryAsset;
use kartik\select2\Select2;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use \app\modules\seedInventory\models\ConfigureSeedlotCode;

\yii\jui\JuiAsset::register($this);

?>

<?php

$this->registerCss('
    .sortable > div{ 
        float: left;
    } 
    #pattern-form.sortable li{
        margin: 4px;
        padding: 0px 5px;
        border: 1px dashed #ddd;
        min-height:  3rem;
        width: fit-content;
    }
    .grid-item>.card{
        margin:.5rem 0 .5rem 0;
        font-weight: 500;
        width: fit-content;
    }
    .grid-item>.card:hover{
        background-color: #dff0d8;
    }
    .grid-item>.card>.card-content{
        min-width: 40px;
        width: fit-content;
    }
    label{
        font-size:.9rem;
    }
');

$programArr= ConfigureSeedlotCode::getProgramList();

$studyVarArr= ConfigureSeedlotCode::getVarList('study');

$plotVarArr= ConfigureSeedlotCode::getVarList('plot');

$seedVarArr= ConfigureSeedlotCode::getVarList('seed');

$actionColumns = [
	[
        'class' => 'kartik\grid\SerialColumn',
        'width'=>'1%'
    ], 
    [
        'label'=> "Checkbox",
        'mergeHeader'=>true,
        'header'=>'<input type="checkbox" class="filled-in" data-id-string="" id="seed-inventory-seedlotCode_check-all" /><label style="padding-left: 20px;" for="seed-inventory-seedlotCode_check-all"></label>',
        'content'=>function($model) {
            return '
            <input class="seed-inventory-seedlotCode_checkbox filled-in" data-id="'.$model['id'].'"  type="checkbox" id="'.$model['row_id'].'" />
            <label style="padding-left: 20px;" for="'.$model['row_id'].'"></label>';
        },     

        'width'=>'1%'
    ],
    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => 'Actions',
        'noWrap' => true,
        'template' => '{enable}{edit}',
        'buttons' => [
           
            'edit' => function ($url, $model, $key) {

                return yii\helpers\Html::a('<i class="material-icons">create</i>',
                    '#',
                    ['style'=>'cursor: pointer;',
                    'class' => 'update-pattern-btn', 
                    'title' => 'Edit pattern',
                    'data-program_abbrev' =>$model['program_abbrev'],
                    'data-pattern_format' =>$model['pattern_format'],
                    'data-toggle'=>"modal",
                    "data-target"=>"#update-pattern-modal",

                ]

            );

            },
        ]
    ]
];

$columns = [
	[
        'attribute'=>'pattern_name', 	
        'label'=>'Pattern Name', 	
        'hAlign'=>'center',
        'vAlign'=>'middle',
    ],
    [
        'label'=>'Used by', 
        'attribute'=>'program_name',
        'hAlign'=>'center',
        'vAlign'=>'middle',	
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>$programArr,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true, ],
        ],
        'filterInputOptions'=>['placeholder'=>'Program',],
        
    ],
    [
        'attribute'=>'pattern_format',
        'label'=>'Pattern',
        'hAlign'=>'center',
        'vAlign'=>'middle', 
        'format'=>'raw',
        'noWrap'=>true,
        'width'=>'20%',
        'value'=>function($data){

            $string=$data['pattern_format'];
            $pattern = '/{/';
            $replacement = ',';
            $string=preg_replace($pattern, $replacement, $string);

            $pattern = '/}/';
            $replacement = ',';
            $string=preg_replace($pattern, $replacement, $string);

            $tags= explode(',', $string);
            
            $result='';
            foreach($tags as $tag){
                if(!empty(trim($tag))){
                    $result.='<span class="badge white card grey-text" style="float: none;">'.$tag.'</span>';
                }

            }
            return $result;

        }	
    ],
    [
        'attribute'=>'description',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'attribute'=>'creator_name',
        'label'=>'Creator',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'attribute'=>'creation_timestamp',
        'label'=>'Creation Timestamp',
        'format' => 'dateTime',
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true, 
                'clearBtn' => true,
                'options'=>[
                    'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ], 
    ],

    [
        'attribute'=>'modifier_name',
        'label'=>'Modifier',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'attribute'=>'modification_timestamp',
        'label'=>'Modification Timestamp',
        'format' => 'dateTime',
        'hAlign'=>'center',
        'vAlign'=>'middle', 
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true, 
                'clearBtn' => true,
                'options'=>[
                    'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],    
    ],
    [
        'attribute'=>'remarks',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'label'=>'Status',
        'attribute'=>'is_active',
        'hAlign'=>'center',
        'vAlign'=>'middle',  
        'format'=>'raw',  
        'noWrap'=>true,
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>['true'=>'Active','false'=>'Inactive'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true, ],
        ],
        'filterInputOptions'=>['placeholder'=>'Status',],
        'value'=>function($data){
            
            if( $data['is_active']=='true'){
                return '<div class="switch" style="display:inline">
                <label>
                <span >Inactive</span>
                <input type="checkbox" class="pattern-status-switch" checked data-id="'.$data['id'].'">
                <span class="lever"></span>
                <span >Active</span>
                </label>
                </div>';
           }else{
                return '<div class="switch" style="display:inline">
                <label>
                <span >Inactive</span>
                <input type="checkbox" class="pattern-status-switch" data-id="'.$data['id'].'">
                <span class="lever"></span>
                <span >Active</span>
                </label>
                </div>';
            }

        },
        
    ],

];
$gridColumns = array_merge($actionColumns,$columns);
$searchModel= new \app\modules\seedInventory\models\ConfigureSeedlotCode();
$dataProvider= $searchModel->search(Yii::$app->request->queryParams);

?>

<?php
$dynagrid = kartik\dynagrid\DynaGrid::begin([
    'columns' => $gridColumns,
    'theme'=>'simple-bordered',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'options'=>['id'=>'seed-inventory-seedlotCode-grid','class'=>""], 
    'gridOptions'=>[
        'moduleId' => 'gridview',
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'seed-inventory-seedlotCode-grid'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],

        'dataProvider'=> $dataProvider,
        'filterModel'=> $searchModel,
        'showPageSummary'=>false,
        'responsiveWrap'=>false,

        'panel'=>[
            'before' => \Yii::t('app', 'This is the browser for all the patterns of seedlot codes. '),
        ],
        'toolbar' =>  [
            [
                'content'=> yii\helpers\Html::a('<i class="material-icons right">add</i> '.
                    \Yii::t('app', 'Create'), '#', [
                        'id'=>'add-pattern-btn-id',
                        'class'=>'btn waves-effect waves-light white-text light-green darken-3', 
                        'style'=>'margin-right:5px;',
                        'title'=>\Yii::t('app','Create Seedlot code pattern'),
                        'data-toggle'=>"modal",
                        "data-target"=>"#add-pattern-modal",
                    ]).' '.
                
                yii\helpers\Html::a('<i class="glyphicon glyphicon-repeat"></i>','#', [
                    'class' => 'btn btn-default', 
                    'title'=>\Yii::t('app','Reset grid'),
                    'id'=>'reset-grid-id'
                ]).'{dynagridFilter}{dynagridSort}{dynagrid}'."<a class='dropdown-button pattern-dropdown-button btn' data-beloworigin='true' href='#' 'style'='margin-right:5px;' data-activates='pattern-dropdown'><i class='material-icons'>settings</i> </a>".
                    '<ul id="pattern-dropdown" class="dropdown-content">'.
                        '<li><a href="#!" id="delete-pattern-btn" data-toggle="modal" data-target="#pattern-delete-modal">Delete</a></li>'.
                        '<li><a href="#!" id="active-pattern-btn" data-toggle="modal" data-target="#pattern-set-active-modal">Set to Active</a></li>'.
                        '<li><a href="#!" id="inactive-pattern-btn" data-toggle="modal" data-target="#pattern-set-inactive-modal">Set to Inactive</a></li>'.
                    '</ul>',
            ],

            'exportConfig' =>false,

        ]
    ]

]);
kartik\dynagrid\DynaGrid::end();
?>    
<!-- Create Modal -->
<?php echo $this->render('forms/create.php');?>

<!-- Update Modal -->
<div id="update-pattern-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    
</div>

<!-- Delete Modal -->
<div id="pattern-delete-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title "><i class="material-icons amber-text text-darken-3">warning</i><?php echo \Yii::t('app','Delete Pattern');?></h4>
            </div>
            <div class="modal-body" >
                <div class="row">

                    <span class="hidden" id="delete-pattern-count-0" ><?php echo \Yii::t('app','Select at least one record.');?></span>

                    <span style="" id="delete-pattern-count" class="hidden" ><?php echo \Yii::t('app','You are about to delete ');?> 0 <?php echo \Yii::t('app','record(s)');?>.</span>

                </div>
            </div>

            <div class="modal-footer">
                <a class="btn-flat " data-dismiss="modal" style="margin-right:5px;background-color:transparent !important; text-transform:none !important;"><?php echo \Yii::t('app','Cancel');?></a>
            
                <button type="button" class="btn hidden pattern-delete-selected-btn" id="pattern-delete-selected-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply to selected');?></button>

                <button type="button" class="btn hidden pattern-delete-selected-btn" id="pattern-delete-apply-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply');?></button>

                <button type="button" class="btn hidden" data-dismiss="modal"  id="pattern-delete-all-btn" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply to all');?></button>               
            </div>
        </div>
    </div>
</div>

<!-- Set Active Modal -->
<div id="pattern-set-active-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title "><i class="material-icons">check_circle</i><?php echo \Yii::t('app','Set Active Pattern');?></h4>
            </div>
            <div class="modal-body" >
                <div class="row">

                    <span class="hidden" id="set-active-pattern-count-0" ><?php echo \Yii::t('app','Select at least one record.');?></span>

                    <span style="" id="set-active-pattern-count" class="hidden" ><?php echo \Yii::t('app','You have selected');?> 0 <?php echo \Yii::t('app','record(s)');?>.</span>

                </div>
            </div>

            <div class="modal-footer">
                <a class="btn-flat " data-dismiss="modal" style="margin-right:5px;background-color:transparent !important; text-transform:none !important;"><?php echo \Yii::t('app','Cancel');?></a>
            
                <button type="button" class="btn hidden pattern-set-active-selected-btn" id="pattern-set-active-selected-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply to selected');?></button>

                <button type="button" class="btn hidden pattern-set-active-selected-btn" id="pattern-set-active-apply-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply');?></button>

                <button type="button" class="btn hidden" data-dismiss="modal"  id="pattern-set-active-all-btn" style="margin-right:5px;"><?php echo \Yii::t('app','Apply to all');?></button>               
            </div>
        </div>
    </div>
</div>

<!-- Set Inactive Modal -->
<div id="pattern-set-inactive-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title "><i class="material-icons">block</i><?php echo \Yii::t('app','Set Inactive Pattern');?></h4>
            </div>
            <div class="modal-body" >
                <div class="row">

                    <span class="hidden" id="set-inactive-pattern-count-0" ><?php echo \Yii::t('app','Select at least one record.');?></span>

                    <span style="" id="set-inactive-pattern-count" class="hidden" ><?php echo \Yii::t('app','You have selected');?> 0 <?php echo \Yii::t('app','record(s)');?>.</span>

                </div>
            </div>

            <div class="modal-footer">
                <a class="btn-flat " data-dismiss="modal" style="margin-right:5px;background-color:transparent !important; text-transform:none !important;"><?php echo \Yii::t('app','Cancel');?></a>
            
                <button type="button" class="btn hidden pattern-set-inactive-selected-btn" id="pattern-set-inactive-selected-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply to selected');?></button>

                <button type="button" class="btn hidden pattern-set-inactive-selected-btn" id="pattern-set-inactive-apply-btn" data-dismiss="modal" style="margin-right:5px;"><i class="material-icons right">check</i><?php echo \Yii::t('app','Apply');?></button>

                <button type="button" class="btn hidden" data-dismiss="modal"  id="pattern-set-inactive-all-btn" style="margin-right:5px;"><?php echo \Yii::t('app','Apply to all');?></button>               
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var selectedRows=[];
    $(document).on('click', '.update-pattern-btn', function(e) {
        $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/get']); ?>',
            type: 'post',
            data:{
                programAbbrev:$(this).data('program_abbrev'),
                patternFormat:$(this).data('pattern_format'),
            },
            success: function(response) {
                if (response) {
                    $("#update-pattern-modal").html(response);
                    $('.loading-div').html('');
                }
            },
            error: function() {
                
            }
        });

    });

    $(document).on('change', '.pattern-status-switch', function(e) {
        
        if($(this).prop("checked")==true){
            isActiveValue=true;
        }else{
            isActiveValue=false;
        }
        $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update-is-active']); ?>',
                type: 'post',
                data:{
                    id:$(this).attr("data-id"),
                    value:isActiveValue
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
    });
    function renderBrowser(){

        $('.specify-format-collapsible').collapsible('open', 0);
        $('#pattern-order-switch').prop("checked",true); 

        // reset data browser, remove filters
        $('#reset-grid-id').on('click', function(e) {

            e.preventDefault();
            $.pjax.reload({
                container: '#seed-inventory-seedlotCode-grid-pjax',
                replace: true,
            });
        });
        // trigger saving of pattern
        $("#create-pattern-form").on('submit',function(e){
            e.preventDefault();
            var data= $(this).serializeArray();
            var sortableArr=$( "#pattern-form" ).sortable('toArray');
            var pattern=[];
            var basic={};

            patternFormat='';
            for(var i=0; i<sortableArr.length;i++){
                var matches=sortableArr[i].match(/^\d*-(.+)-pattern-item-(.+$)/);
                if(matches!==null){
                    patternFormat+="{"+matches[2]+"}";
                    if(matches[1]!="text"){
                        pattern.push({
                            "is_variable":true,
                            "data_level":matches[1],
                            "value": matches[2],
                            "order":i,
                            "id":sortableArr[i]
                        });
                    }else {
                        pattern.push({
                            "is_variable":false,
                            "data_level":null,
                            "value": matches[2],
                            "order":i,
                            "id":sortableArr[i]
                        });
                    }
                }else{
                    patternFormat+="{"+$("#sequence-pattern-item>.grid-item>.card>.card-content").html()+"}";
                    pattern.push({
                        "is_variable":false,
                        "data_level":null,
                        "value":$("#sequence-pattern-item>.grid-item>.card>.card-content").html(),
                        "order":i,
                        "id":sortableArr[i]
                    });
                }
            }
            digits=null;
            
            if($("#leading-zero-label").val()!=undefined){
                digits=parseInt($("#leading-zero-label").val());
            }
            
            metadata={
                "start_seq_no":$("#sequence-number-label").val(),
                "leading_zeros": $("#leading-pattern-id").prop("checked"),
                "digits":digits,
            };

            for(var i=0; i<data.length;i++){
                basic[data[i]['name']]=data[i]['value'];
            }
            basic["programName"]=$("#select-program-id").select2('data')[0]['text'];
            basic["pattern"]=patternFormat;
            basic["description"]=$("#description-id").val();
            createJson={
                "basic":basic,
                "metadata":metadata,
                "pattern":pattern,
                "is_active": $("#set-active-id").prop("checked")
            };       

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/create']); ?>',
                type: 'post',
                data:{
                    value:createJson,
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#add-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully created pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        //change the pattern label in the preview when select2 has changed
        $("#select-experiment-label-id").on('change',function(e){
            $("#experiment-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });
        
        $("#select-plot-label-id").on('change',function(e){
            $("#plot-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });

        $("#select-seedlot-label-id").on('change',function(e){
            $("#seedlot-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });

        //disable create button when program name and program is not filled
        $("#select-program-id").on('change',function(e){

            if($("#select-program-id").val()=='' || $("#pattern_name").val()=='' || $("#sequence-number-label").val()=='' || ($("#leading-pattern-id").prop("checked") && $("#leading-zero-label").val()=='')){
                $(".create-btn").attr('disabled',true);        
            }else{
                $(".create-btn").attr('disabled',false);        
            }
        });

        $("#pattern_name").on('keyup paste',function(e){
            
            if($(this).val()=='' || $("#select-program-id").val()=='' || $("#sequence-number-label").val()=='' || ($("#leading-pattern-id").prop("checked") && $("#leading-zero-label").val()=='')){
                $(this).addClass('invalid');        
                $(".create-btn").attr('disabled',true);        
            }else{
                $(this).removeClass('invalid');  
                $(".create-btn").attr('disabled',false);        
            }

        });

        //Add leading zero on sequence number input field and change the pattern value in the preview when enabled, when disabled, revert changes
        $("#leading-pattern-id").on('click',function(e){

            var leadingZero=parseInt($("#leading-zero-label").val());            
            var labelString=$("#sequence-number-label").val();
            var labelLength=leadingZero+labelString.length;
            var paddedSequenceNo=labelString.padStart(labelLength,'0');
            
            if($(this).prop("checked") === true)  {
                $('#leading-zero-label').attr('disabled',false);
                // $("#sequence-number-label").val(labelString.padStart(labelLength,'0'));
                
                $("#sequence-pattern-item .card-content").html(paddedSequenceNo);

            }else{
                $('#leading-zero-label').attr('disabled',true);
                $("#sequence-pattern-item .card-content").html(labelString);
                
            }
            $(".create-btn").attr('disabled',false);
            if($("#select-program-id").val()=='' || $("#pattern_name").val()=='' || $("#sequence-number-label").val()==""){
                $(".create-btn").attr('disabled',true);
            }

        });

        $("#sequence-number-label").on('change paste',function(e){
            var paddedSequenceNo=$("#sequence-number-label").val(); // no leading zeros
            if($(this).val()!==""){
                if($("#leading-pattern-id").prop("checked") === true)  {
                    leadingZero=parseInt($("#leading-zero-label").val());
                    
                    var labelString=$("#sequence-number-label").val();
                    var labelLength=leadingZero+labelString.length;
                    paddedSequenceNo=labelString.padStart(labelLength,'0');
                }
                
                $("#sequence-pattern-item .card-content").html(paddedSequenceNo);      
                $(".create-btn").attr('disabled',false);
                $("#leading-pattern-id").attr("disabled",false);
                // $("#leading-zero-label").attr("disabled",false);
            }else{
                $("#leading-pattern-id").attr("disabled",true);
                $("#leading-zero-label").attr("disabled",true);
                $(".create-btn").attr('disabled',true);
            }
            if($("#select-program-id").val()=='' || $("#pattern_name").val()=='' || ($("#leading-pattern-id").prop("checked") && $("#leading-zero-label").val()=='')){
                $(".create-btn").attr('disabled',true);
            }
            
        });

        $("#leading-zero-label").on('change',function(e){
            if($("#leading-pattern-id").prop("checked") === true)  {
                labelString=$("#sequence-number-label").val();
                
                thisVal=parseInt($(this).val());
                labelLength=thisVal+labelString.toString().length;
                var paddedSequenceNo=labelString.padStart(labelLength,'0');

                
                $("#sequence-pattern-item .card-content").html(paddedSequenceNo);  

                $(".create-btn").attr('disabled',false);

                if($("#select-program-id").val()=='' || $("#pattern_name").val()=='' || $("#leading-zero-label").val()==''){
                    $(".create-btn").attr('disabled',true);
                }
            }
        });

        // disabled add button when there is no input
        $("#text-label").on('keyup paste',function(e){
            if($(this).val()==''){
                $("#add-text-pattern-btn").attr('disabled',true);        
            }else{
                $("#add-text-pattern-btn").attr('disabled',false);        
            }
        });
        //add text pattern on the preview
        $("#add-text-pattern-btn").on('click',function(e){
            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-text-pattern-item-'+$("#text-label").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#text-label").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';

            $( "#pattern-form" ).sortable('destroy');
            if($('#pattern-order-switch').prop("checked")==false){
                $( "#pattern-form" ).append(thisItem);
            }else{
                $( "#pattern-form" ).prepend(thisItem);
            }
            
            $( "#pattern-form" ).sortable();
            $(".item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#pattern-form" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#pattern-form" ).sortable();
                }                
            });
        });
        //add experiment pattern in the preview
        $('#add-experiment-pattern-btn').on('click',function(e){

            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-experiment-pattern-item-'+$("#select-experiment-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+

            $("#select-experiment-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';

            $( "#pattern-form" ).sortable('destroy');
            if($('#pattern-order-switch').prop("checked")==false){
                $( "#pattern-form" ).append(thisItem);
            }else{
                $( "#pattern-form" ).prepend(thisItem);
            }
            $( "#pattern-form" ).sortable();
            
            $(".item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#pattern-form" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#pattern-form" ).sortable();
                }                
            });
        });
        //add plot pattern in the preview
        $('#add-plot-pattern-btn').on('click',function(e){
            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-plot-pattern-item-'+$("#select-plot-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#select-plot-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';
            $( "#pattern-form" ).sortable('destroy');
            if($('#pattern-order-switch').prop("checked")==false){
                $( "#pattern-form" ).append(thisItem);
            }else{
                $( "#pattern-form" ).prepend(thisItem);
            }            
            $( "#pattern-form" ).sortable();
            
            $(".item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#pattern-form" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#pattern-form" ).sortable();
                }                
            });
        });
        //add seedlot pattern in the preview
        $('#add-seedlot-pattern-btn').on('click',function(e){

            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-seedlot-pattern-item-'+$("#select-seedlot-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#select-seedlot-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';
            $( "#pattern-form" ).sortable('destroy');
            if($('#pattern-order-switch').prop("checked")==false){
                $( "#pattern-form" ).append(thisItem);
            }else{
                $( "#pattern-form" ).prepend(thisItem);
            }
            $( "#pattern-form" ).sortable();
            
            $(".item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#pattern-form" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#pattern-form" ).sortable();
                }                
            });
        });

        $( "#pattern-form" ).sortable();

        // Select row(s)
        $(document).on('click',"#seed-inventory-seedlotCode-grid tbody tr",function(e){
            this_row=$(this).find('input:checkbox')[0];
            
            if(this_row.checked){

                this_row.checked=false;  
                $(this).removeClass("warning");

                if($("#seed-inventory-seedlotCode_check-all").prop("checked") === true)  {
                    $("#seed-inventory-seedlotCode_check-all").prop("checked",false);
                }

            }else{

                $(this).addClass("warning");
                this_row.checked=true;  
                $("#"+this_row.id).prop("checked");  
            }

        });
        // select/unselect all
        $(document).on('click',"#seed-inventory-seedlotCode_check-all",function(e){

            if($(this).prop("checked") === true)  {
                $(this).attr('checked','checked');
                $('.seed-inventory-seedlotCode_checkbox').attr('checked','checked');
                $('.seed-inventory-seedlotCode_checkbox').prop('checked',true);
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").parent("td").parent("tr").addClass("warning");

            }else{

                $(this).removeAttr('checked');
                $('.seed-inventory-seedlotCode_checkbox').prop('checked',false);
                $('.seed-inventory-seedlotCode_checkbox').removeAttr('checked');
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").parent("td").parent("tr").removeClass("warning");      
            }
        });
        // renders delete modal dialog
        $('#pattern-delete-modal').on('show.bs.modal',function(e){

            if($('#seed-inventory-seedlotCode_check-all').prop("checked") === true)  {
                $("#pattern-delete-all-btn").removeClass("hidden");
                $(".pattern-delete-selected-btn").addClass("hidden");
                $("#pattern-delete-selected-btn").removeClass("hidden");
                $("#delete-pattern-count-0").addClass("hidden");
                $("#delete-pattern-count").removeClass("hidden");

                $("#delete-pattern-count").html("<?php echo \Yii::t('app','You have selected all records. Click <b>Apply to Selected</b> to delete records in this page or <b>Apply to All</b> to delete all records.');?> ");

            }else{
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
                
                if(selectedRows.length==0){

                    var notif = "<i class='material-icons amber-text'>warning</i> Please select at least 1 record.";
                    Materialize.toast(notif, 5000); 
                    return e.preventDefault();
                }else{
                    if(selectedRows.length==1){
                        $("#delete-pattern-count").html("<?php echo \Yii::t('app','You are about to delete ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','record.');?>");
                    }else{
                        $("#delete-pattern-count").html("<?php echo \Yii::t('app','You are about to delete ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','records.');?>");
                    }
                    $("#delete-pattern-count").removeClass("hidden");
                    $("#delete-pattern-count-0").addClass("hidden");
                    
                    $(".pattern-delete-selected-btn").addClass("hidden");
                    $("#pattern-delete-apply-btn").removeClass("hidden");                       
                }
            }
        });
        // renders set active pattern modal dialog
        $('#pattern-set-active-modal').on('show.bs.modal',function(e){

            if($('#seed-inventory-seedlotCode_check-all').prop("checked") === true)  {
                $("#pattern-set-active-all-btn").removeClass("hidden");
                $(".pattern-set-active-selected-btn").addClass("hidden");
                $("#pattern-set-active-selected-btn").removeClass("hidden");
                $("#set-active-pattern-count-0").addClass("hidden");
                $("#set-active-pattern-count").removeClass("hidden");

                $("#set-active-pattern-count").html("<?php echo \Yii::t('app','You have selected all records. Click <b>Apply to Selected</b> to update records in this page or <b>Apply to All</b> to update all records.');?> ");
            }else{

                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
                
                if(selectedRows.length==0){

                    var notif = "<i class='material-icons amber-text'>warning</i> Please select at least 1 record.";
                    Materialize.toast(notif, 5000); 
                    return e.preventDefault();
                }else{
                    if(selectedRows.length==1){
                        $("#set-active-pattern-count").html("<?php echo \Yii::t('app','You are about to set ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','record to active.');?>");
                    }else{
                        $("#set-active-pattern-count").html("<?php echo \Yii::t('app','You are about to set ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','records to active.');?>");
                    }
                    $("#set-active-pattern-count").removeClass("hidden");
                    $("#set-active-pattern-count-0").addClass("hidden");
                    
                    $(".pattern-set-active-selected-btn").addClass("hidden");
                    $("#pattern-set-active-apply-btn").removeClass("hidden");                       
                }
            }
        });
        // renders set inactive pattern modal dialog
        $('#pattern-set-inactive-modal').on('show.bs.modal',function(e){

            if($('#seed-inventory-seedlotCode_check-all').prop("checked") === true)  {
                $("#pattern-set-inactive-all-btn").removeClass("hidden");
                $(".pattern-set-inactive-selected-btn").addClass("hidden");
                $("#pattern-set-inactive-selected-btn").removeClass("hidden");
                $("#set-inactive-pattern-count-0").addClass("hidden");
                $("#set-inactive-pattern-count").removeClass("hidden");

                $("#set-inactive-pattern-count").html("<?php echo \Yii::t('app','You have selected all records. Click <b>Apply to Selected</b> to update records in this page or <b>Apply to All</b> to update all records.');?> ");

            }else{
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
                
                if(selectedRows.length==0){

                    var notif = "<i class='material-icons amber-text'>warning</i> Please select at least 1 record.";
                    Materialize.toast(notif, 5000); 
                    return e.preventDefault();
                }else{
                    if(selectedRows.length==1){
                        $("#set-inactive-pattern-count").html("<?php echo \Yii::t('app','You are about to set ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','record to inactive.');?>");
                    }else{
                        $("#set-inactive-pattern-count").html("<?php echo \Yii::t('app','You are about to set ');?><b> "+selectedRows.length+"</b> <?php echo \Yii::t('app','records to inactive.');?>");
                    }
                    $("#set-inactive-pattern-count").removeClass("hidden");
                    $("#set-inactive-pattern-count-0").addClass("hidden");
                    
                    $(".pattern-set-inactive-selected-btn").addClass("hidden");
                    $("#pattern-set-inactive-apply-btn").removeClass("hidden");                       
                }
            }
        });
        // Clear selections on modal close
        $('#pattern-set-active-modal').on('hide.bs.modal',function(e){
            selectedRows=[];
            $("#set-active-pattern-count-0").removeClass("hidden");
            $("#set-active-pattern-count").addClass("hidden");
            $("#pattern-set-active-all-btn").addClass("hidden");
            $(".pattern-set-active-selected-btn").addClass("hidden");
        });

        $('#pattern-set-inactive-modal').on('hide.bs.modal',function(e){
            selectedRows=[];
            $("#set-inactive-pattern-count-0").removeClass("hidden");
            $("#set-inactive-pattern-count").addClass("hidden");
            $("#pattern-set-inactive-all-btn").addClass("hidden");
            $(".pattern-set-inactive-apply-btn").addClass("hidden");
        });

        $('#pattern-delete-modal').on('hide.bs.modal',function(e){
            selectedRows=[];
            $("#delete-pattern-count-0").removeClass("hidden");
            $("#delete-pattern-count").addClass("hidden");
            $("#pattern-delete-all-btn").addClass("hidden");
            $(".pattern-delete-apply-btn").addClass("hidden");
        });

        // delete selected pattern
        $('.pattern-delete-selected-btn').on('click',function(e){
            if(selectedRows.length==0){
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
            }
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/delete']); ?>',
                type: 'post',
                data:{
                    id:selectedRows.join(","),
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully deleted pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        // delete all pattern
        $('#pattern-delete-all-btn').on('click',function(e){
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/delete-all']); ?>',
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully deleted all patterns.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        // set is active selected pattern
        $('.pattern-set-active-selected-btn').on('click',function(e){
            
            if(selectedRows.length==0){
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
            }
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update-is-active']); ?>',
                type: 'post',
                data:{
                    id:selectedRows.join(","),
                    value:true
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        // set is active all pattern
        $('#pattern-set-active-all-btn').on('click',function(e){

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update-is-active']); ?>',
                dataType: 'json',
                type: 'post',
                data:{
                    value:true
                },
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated all patterns.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        // set is active selected pattern
        $('.pattern-set-inactive-selected-btn').on('click',function(e){
            if(selectedRows.length==0){
                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    if($(this).prop("checked")==true){
                        selectedRows.push($(this).attr("data-id"));     
                    }
                });
            }
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update-is-active']); ?>',
                type: 'post',
                data:{
                    id:selectedRows.join(","),
                    value:false
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        // set is inactive all pattern
        $('#pattern-set-inactive-all-btn').on('click',function(e){
            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update-is-active']); ?>',
                dataType: 'json',
                type: 'post',
                data:{
                    value:false
                },
                success: function(response) {
                    
                    if(response.success){
                        $('#delete-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated all patterns.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true,
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });
        // reset form input fields on modal show
        $('#add-pattern-modal').on('show.bs.modal',function(e){
            $("#pattern_name").val(null);            
            $("#sequence-number-label").val(1);
            $("#leading-zero-label").val(1);
            $("#leading-zero-label").attr('disabled',true);
            $("#text-label").val(null);
            
            if($("#leading-pattern-id").prop("checked")==true){
                $("#leading-pattern-id").prop("checked",false);
            }

            $("#select-program-id").val(null).trigger('change');
            $("#description-id").val(null);
            $("#remarks-id").val(null);
            $("#set-active-id").prop("checked",false);

            $( "#pattern-form" ).sortable('destroy');
            $( "#pattern-form" ).html('<li id="sequence-pattern-item" class="ui-sortable-handle" style="" draggable="true" role="option" aria-grabbed="false"><div class="grid-item "><div class="card"><div class="card-content truncate" style="padding:12px">1</div></div></div></li>');
            $( "#pattern-form" ).sortable();
        });
    }
    
    $(document).ready(function(){

        renderBrowser();

        $(document).on('ready pjax:success', function(e) {
        
            $('.pattern-dropdown-button').dropdown({
                inDuration: 300,
                outDuration: 225,
                constrainWidth: false, // Does not change width of dropdown to that of the activator
                hover: false, // Activate on hover
                gutter: 0, // Spacing from edge
                belowOrigin: true, // Displays dropdown below the button
                alignment: 'left', // Displays dropdown with edge aligned to the left of button
                stopPropagation: false // Stops event propagation
            });  

            $('#reset-grid-id').on('click', function(e) {

                e.preventDefault();
                $.pjax.reload({
                    container: '#seed-inventory-seedlotCode-grid-pjax',
                    replace: true,
                });
            });
            
        });
        
    });

</script>
