<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View of Create Seedlot Code Pattern
**/
use kartik\select2\Select2;
use kartik\sortable\Sortable;
use \app\modules\seedInventory\models\ConfigureSeedlotCode;

$programArr= ConfigureSeedlotCode::getProgramList();

$studyVarArr= ConfigureSeedlotCode::getVarList('study');

$plotVarArr= ConfigureSeedlotCode::getVarList('plot');

$seedVarArr= ConfigureSeedlotCode::getVarList('seed');

?>

<div id="add-pattern-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    
    <div class="modal-dialog modal-lg">

        <form class="col s12" id="create-pattern-form" action="post">

            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4><i class="material-icons">add</i><?php echo Yii::t('app','Create Seedlot Code Pattern'); ?></h4>
                </div>

                <div class="modal-body" style="max-height: 730px;">

                    <div class="row">

                        <div class="input-field col s6">
                            <input name="pattern_name" id="pattern_name" type="text" placeholder="" class="validate">
                            <label for="pattern_name"><?php echo Yii::t('app','Pattern Name'); ?><span style="color:red">*</span></label>
                            <span class="helper-text" data-error="Please fill pattern name" ></span>
                        </div>

                        <div class=" col s6" style="padding: 0 .75rem;">

                            <label class="control-label" ><?php echo Yii::t('app','Used by'); ?><span style="color:red">*</span></label>
                            <?php
                            echo Select2::widget([
                                'name' => 'program',
                                'data' => $programArr,
                                'options' => ['placeholder' => 'Select a program ...', 'id'=>'select-program-id', 'style'=>'width:50%'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'id'=>'select-program-id'
                                ],
                            ]);
                            ?>
                        </div>
                    </div>

                    <div class="row">

                        <div class=" col s6">
                            <label for="description-id" ><?php echo Yii::t('app','Description'); ?></label>    
                            <?php echo yii\helpers\Html::textArea('description',"",['id'=>'description-id',"class"=>"form-control filter-form-description"]); 
                            ?>
                        </div>
                        <div class=" col s6">
                            <label for="remarks-id" ><?php echo Yii::t('app','Remarks'); ?></label>    
                            <?php echo yii\helpers\Html::textArea('remarks',"",['id'=>'remarks-id',"class"=>"form-control filter-form-remarks"]); 
                            ?>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="col s6 offset-s6" style="">

                            <input name="active" class="filled-in " type="checkbox" id="set-active-id" />
                            <label class="" style="" for="set-active-id"><?php echo Yii::t('app','Active'); ?></label>

                        </div>
                    </div>

                    <ul class="collapsible specify-format-collapsible">
                        <li>
                            <div class="collapsible-header">
                                <?php echo Yii::t('app','Specify Format'); ?>

                            </div>

                            <div class="collapsible-body" style="margin-bottom: 0px">

                                <div class="switch" style="display:inline">
                                    <label>
                                        <span ><?php echo Yii::t('app','Add after sequence number'); ?></span>
                                        <input type="checkbox" id="pattern-order-switch">
                                        <span class="lever"></span>
                                        <span ><?php echo Yii::t('app','Add before sequence number'); ?></span>
                                    </label>
                                </div>
                                <hr>

                                <div class="row" style="">

                                    <div class="col s4">
                                        
                                        <label class="" style="" ><?php echo Yii::t('app','Select and Add label'); ?></label><br><br>
                                        
                                        <div class=" row" style="">

                                            <div class=" col s9" style="">
                                                <?php
                                                if(!empty($studyVarArr)){
                                                    echo Select2::widget([
                                                        'name' => 'experiment-label-select',
                                                        'data' => $studyVarArr,
                                                        'options' => [
                                                            'id'=>'select-experiment-label-id'
                                                        ],
                                                        'pluginOptions' => [

                                                            'id'=>'select-experiment-label-id'
                                                        ],
                                                    ]);
                                                    echo '<label class="" style="" for="experiment-pattern-id">'.Yii::t('app','Experiment label').'</label>';
                                                }
                                                ?>
                                            </div>

                                            <div class="col s2 ">
                                                <?php
                                                if(!empty($studyVarArr)){
                                                    ?>
                                                    <a class="btn waves-effect waves-light" id="add-experiment-pattern-btn"><i class="material-icons">add</i></a>
                                                    <?php
                                                }?>
                                            </div>
                                        </div>

                                        <div class=" row" style="">

                                            <div class=" col s9" style="">
                                                <?php
                                                if(!empty($plotVarArr)){
                                                    echo Select2::widget([
                                                        'name' => 'plot-label-select',
                                                        'data' => $plotVarArr,
                                                        'options' => [
                                                            'id'=>'select-plot-label-id'
                                                        ],
                                                        'pluginOptions' => [
                                                            'id'=>'select-plot-label-id'
                                                        ],
                                                    ]);
                                                    echo '<label class="" style="" for="experiment-pattern-id">'.Yii::t('app','Plot label').'</label>';
                                                }?>
                                            </div>

                                            <div class=" col s2" >
                                                <?php
                                                if(!empty($plotVarArr)){
                                                    ?>
                                                    <a class="btn waves-effect waves-light" id="add-plot-pattern-btn"><i class="material-icons">add</i></a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class=" row" style="">

                                            <div class=" col s9" style="">
                                                <?php
                                                if(!empty($seedVarArr)){
                                                    echo Select2::widget([
                                                        'name' => 'seedlot-label-select',
                                                        'data' => $seedVarArr,
                                                        'options' => [
                                                            'id'=>'select-seedlot-label-id'
                                                        ],
                                                        'pluginOptions' => [
                                                            'allowClear' => true,
                                                            'id'=>'select-seedlot-label-id'
                                                        ],
                                                    ]);
                                                    echo '<label class="" style="" for="experiment-pattern-id">'.Yii::t('app','Seed label').'</label>';
                                                }
                                                ?>
                                            </div>

                                            <div class=" col s2" >
                                                <?php
                                                if(!empty($seedVarArr)){?>

                                                    <a class="btn waves-effect waves-light" id="add-seedlot-pattern-btn"><i class="material-icons">add</i></a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col text-center">
                                        <div style="border-left: 1px solid #ccc;height: 230px;"></div>
                                    </div>

                                    <div class="col s4">

                                        <label class="" style="" ><?php echo Yii::t('app','Specify sequence format'); ?></label><br><br>

                                        <div class=" row" style="margin-bottom:0px; ">

                                            <div class="col ">
                                                <input class="filled-in" checked disabled type="checkbox" id="sequence-pattern-id" />
                                                <label class="" style="" for="sequence-pattern-id"><?php echo Yii::t('app','Sequence number'); ?></label>
                                            </div>

                                            <div  class="col"  style="margin-left: 3.1rem; margin-bottom:0px; ">

                                                <div class="col ">
                                                    <input class="filled-in" type="checkbox" id="leading-pattern-id" disabled/>
                                                    <label class="" style="" for="leading-pattern-id"><?php echo Yii::t('app','Leading zero'); ?></label>
                                                </div>

                                                <div class="col"  style="margin-left: 3.1rem; margin-bottom:0px; ">
                                                    <input id="leading-zero-label" value="1" min=1 type="number" disabled placeholder="" class="validate" required>
                                                    <label for="leading-zero-label"></label>
                                                </div>
                                                <br>

                                                <label ><?php echo Yii::t('app','Start at: '); ?></label>
                                                <input id="sequence-number-label" value="1" min=0 type="number" placeholder="" class="validate">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col text-center">
                                        <div style="border-left: 1px solid #ccc;height: 230px;"></div>
                                    </div>

                                    <div class="col s3">
                                        <label class="" style="" ><?php echo Yii::t('app','Add custom text'); ?></label><br><br>
                                        <div class=" row" style="margin-bottom:0px; ">
                                            <div class="col s9 " style="">
                                                <input id="text-label" type="text" placeholder="Input text" class="validate">
                                            </div>
                                            <div class="col s2">
                                                <a class="btn waves-effect waves-light" id="add-text-pattern-btn" disabled><i class="material-icons ">add</i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label><?php echo Yii::t('app','Here is the preview of the pattern. Drag to reorder or click the remove icon to delete'); ?></label>
                                <?php

                                echo Sortable::widget([
                                    'type'=>'grid',
                                    'itemOptions'=>['class'=>'', 'style'=>""],
                                    'options'=>['id'=>'pattern-form'],
                                    'items'=>[
                                        ['content'=>'<div class="grid-item "><div class="card">'.'<div class="card-content truncate" style="padding:12px">1'.'</div></div></div>', "options"=>['id'=>"sequence-pattern-item"]],
                                    ]
                                ]);
                                ?>
                            </div>

                        </li>
                    </ul>

                </div>
                <div class="modal-footer">
                    
                    <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app','Cancel');?></a>

                <?php echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>'.Yii::t('app','Submit'), [
                    'class' => 'btn create-btn waves-effect ',
                    'disabled'=>true,
                    'form'=>'create-pattern-form'
                ]); ?>
                </div>
            </div>
        </form>
    </div>
</div>