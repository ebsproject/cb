<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View of Update Seedlot Code Pattern
**/
use kartik\select2\Select2;
use kartik\sortable\Sortable;
use \app\modules\seedInventory\models\ConfigureSeedlotCode;

$programArr= ConfigureSeedlotCode::getProgramList();

$studyVarArr= ConfigureSeedlotCode::getVarList('study');

$plotVarArr= ConfigureSeedlotCode::getVarList('plot');

$seedVarArr= ConfigureSeedlotCode::getVarList('seed');
extract($data);
?>
<!-- Update Modal -->

<form class="col s12" id="update-pattern-form" action="post">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="material-icons">edit</i><?php echo Yii::t('app','Update Seedlot Code Pattern'); ?></h4>
            </div>
            <div class="modal-body" style="max-height: 730px;">

                <div class="row">

                    <div class="input-field col s6">
                        <input name="update-pattern_name" id="update-pattern_name" type="text" placeholder="" class="validate" value="<?php echo $patternName; ?>">
                        <label  for="update-pattern_name"><?php echo Yii::t('app','Pattern Name'); ?><span style="color:red">*</span></label>

                    </div>

                    <div class=" col s6" style="padding: 0 .75rem;">
                        <label class="control-label" ><?php echo Yii::t('app','Used by'); ?> <span style="color:red">*</span></label>

                        <?php

                        echo Select2::widget([
                            'name' => 'program',
                            'data' => $programArr,
                            'options' => ['placeholder' => 'Select a program ...', 'id'=>'update-select-program-id', 'style'=>'width:50%'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id'=>'update-select-program-id',
                                'disabled'=>true
                            ],
                            "value"=>$programAbbrev
                        ]);

                        ?>

                    </div>

                </div>

                <div class="row">
                    <div class=" col s6">
                        <label for="update-description-id" ><?php echo Yii::t('app','Description'); ?></label>    
                        <?php echo yii\helpers\Html::textArea('description',$description,['id'=>'update-description-id',"class"=>"form-control filter-form-description"]); 
                        ?>
                    </div>
                    <div class=" col s6">
                        <label for="update-remarks-id" ><?php echo Yii::t('app','Remarks'); ?></label>    
                        <?php echo yii\helpers\Html::textArea('remarks',$remarks,['id'=>'update-remarks-id',"class"=>"form-control filter-form-remarks"]); 
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col s6 offset-s6" style="">

                        <input name="active" class="filled-in " type="checkbox" id="update-set-active-id" <?php echo ($isActive=='true')?'checked':''; ?> />
                        <label class="" style="" for="update-set-active-id"><?php echo Yii::t('app','Active'); ?></label>                        
                    </div>
                </div>

                <ul class="collapsible update-specify-format-collapsible">
                    <li>
                        <div class="collapsible-header">
                            <?php echo Yii::t('app','Specify Format'); ?>
                        </div>

                        <div class="collapsible-body" style="margin-bottom: 0px">

                            <div class="switch" style="display:inline">

                                <label>
                                    <span ><?php echo Yii::t('app','Add after sequence number'); ?></span>
                                    <input type="checkbox" id="update-pattern-order-switch">
                                    <span class="lever"></span>
                                    <span ><?php echo Yii::t('app','Add before sequence number'); ?></span>
                                </label>
                            </div>

                            <hr>

                            <div class="row" style="">

                                <div class="col s4">
                                    <label class="" style="" ><?php echo Yii::t('app','Select and Add label'); ?></label><br><br>
                                    <div class=" row" style="">

                                        <div class=" col s9" style="">
                                            <?php
                                            if(!empty($studyVarArr)){
                                                echo Select2::widget([
                                                    'name' => 'experiment-label-select',
                                                    'data' => $studyVarArr,
                                                    'options' => [
                                                        'id'=>'update-select-experiment-label-id'
                                                    ],
                                                    'pluginOptions' => [

                                                        'id'=>'update-select-experiment-label-id'
                                                    ],
                                                ]);
                                                echo '<label class="" style="" for="update-experiment-pattern-id">'.Yii::t('app','Experiment label').'</label>';
                                            }
                                            ?>
                                        </div>
                                        <div class="col s2 ">

                                            <?php
                                            if(!empty($studyVarArr)){
                                                ?>
                                                <a class="btn waves-effect waves-light" id="update-add-experiment-pattern-btn"><i class="material-icons">add</i></a>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class=" row" style="">

                                        <div class=" col s9" style="">
                                            <?php
                                            if(!empty($plotVarArr)){
                                                echo Select2::widget([
                                                    'name' => 'plot-label-select',
                                                    'data' => $plotVarArr,
                                                    'options' => [
                                                        'id'=>'update-select-plot-label-id'
                                                    ],
                                                    'pluginOptions' => [
                                                        'id'=>'update-select-plot-label-id'
                                                    ],
                                                ]);
                                                echo '<label class="" style="" for="update-experiment-pattern-id">'.Yii::t('app','Plot label').'</label>';
                                            }
                                            
                                            ?>
                                        </div>
                                        <div class=" col s2" >
                                            <?php
                                            if(!empty($plotVarArr)){
                                                ?>
                                                <a class="btn waves-effect waves-light" id="update-add-plot-pattern-btn"><i class="material-icons">add</i></a>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class=" row" style="">

                                        <div class=" col s9" style="">
                                            <?php
                                            if(!empty($seedVarArr)){
                                                echo Select2::widget([
                                                    'name' => 'seedlot-label-select',
                                                    'data' => $seedVarArr,
                                                    'options' => [
                                                        'id'=>'update-select-seedlot-label-id'
                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'id'=>'update-select-seedlot-label-id'
                                                    ],
                                                ]);
                                                echo '<label class="" style="" for="update-experiment-pattern-id">'.Yii::t('app','Seed label').'</label>';
                                            }
                                            ?>
                                        </div>
                                        <div class=" col s2" >
                                            <?php
                                            if(!empty($seedVarArr)){
                                                ?>
                                                <a class="btn waves-effect waves-light" id="update-add-seedlot-pattern-btn"><i class="material-icons">add</i></a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="col text-center">
                                    <div style="border-left: 1px solid #ccc;height: 230px;"></div>
                                </div>

                                <div class="col s4">
                                    <label class="" style="" ><?php echo Yii::t('app','Specify sequence format'); ?></label><br><br>
                                    <div class=" row" style="margin-bottom:0px; ">

                                        <div class="col ">
                                            <input class="filled-in" checked disabled type="checkbox" id="update-sequence-pattern-id"  />
                                            <label class="" style="" for="update-sequence-pattern-id"><?php echo Yii::t('app','Sequence number'); ?></label>

                                        </div>
                                        <div  class="col"  style="margin-left: 3.1rem; margin-bottom:0px; ">
                                            <div class="col ">

                                                <input class="filled-in" type="checkbox" id="update-leading-pattern-id" <?php echo ($leadingZero)?'checked':''; ?>/>
                                                <label class="" style="" for="update-leading-pattern-id"><?php echo Yii::t('app','Leading zero'); ?></label>

                                            </div>
                                            
                                            <div class="col"  style="margin-left: 3.1rem; margin-bottom:0px; ">
                                                <input id="update-leading-zero-label" min=1 type="number" disabled placeholder="" class="validate" value="<?php echo ($leadingZero=='true')? ($digits):'1';?>" required>
                                                <label for="update-leading-zero-label"></label>

                                            </div>
                                            <br>
                                            <label ><?php echo Yii::t('app','Start at:'); ?> </label>
                                            <input id="update-sequence-number-label" min=0 type="number" placeholder="" class="validate" value="<?php echo $startNo; ?>" required >

                                        </div>
                                    </div>

                                </div>
                                <div class="col text-center">
                                    <div style="border-left: 1px solid #ccc;height: 230px;"></div>
                                </div>

                                <div class="col s3">
                                    <label class="" style="" ><?php echo Yii::t('app','Add custom text'); ?></label><br><br>
                                    <div class=" row" style="margin-bottom:0px; ">
                                        <div class="col s9 " style="">

                                            <input id="update-text-label" type="text" placeholder="Input text" class="validate">

                                        </div>
                                        <div class="col s2">
                                            <a class="btn waves-effect waves-light" id="update-add-text-pattern-btn" disabled><i class="material-icons ">add</i></a>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <label><?php echo Yii::t('app','Here is the preview of the pattern. Drag to reorder or click the remove icon to delete'); ?></label>
                            <?php
                            $items=json_decode($pattern);
                            $itemsList=[];
                            foreach($items as $item){
                                $itemId=$item->id;
                                
                                $JsFunction = new \yii\web\JsExpression("
                                    function closeItem() {
                                        if($( '#update-pattern-sortable' ).sortable( 'instance' )!==undefined){
                                            $( '#update-pattern-sortable' ).sortable('destroy');
                                        }
                                       
                                        $('#".$itemId."').remove();
                                        $( '#update-pattern-sortable' ).sortable();
                                   }; 
                                   closeItem();"
                               );

                                if($itemId=='sequence-pattern-item' || $itemId=='update-sequence-pattern-item'){
                                    $itemsList[]=[
                                        'content'=>'<div class="grid-item "><div class="card">'.'<div class="card-content truncate" style="padding:12px">'.$item->value.'</div></div></div>', 
                                        "options"=>['id'=>'update-sequence-pattern-item']
                                    ];
                                }else{

                                    $itemsList[]=[
                                        'content'=>'<div class="grid-item "><div class="card"><a href="#!" class="close fixed-color item-close" onclick="'.$JsFunction.'" id="" type="button" style="font-weight:normal;margin-top: 7px;" title="Remove pattern"><i class="material-icons">close</i></a>'.'<div class="card-content truncate" style="padding:12px">'.$item->value.'</div></div></div>', 
                                        "options"=>['id'=>$itemId]
                                    ];
                                }

                            }

                            echo Sortable::widget([
                                'type'=>'grid',
                                'itemOptions'=>['class'=>'', 'style'=>""],
                                'options'=>['id'=>'update-pattern-sortable'],
                                'items'=>$itemsList,
                                'pluginEvents' => [
                                    'sortupdate' => 'function(ui) { 

                                        $(".update-btn").attr("disabled",false);
                                        
                                    }',
                                    'sortcreate' => 'function(ui) { 

                                        $(".update-btn").attr("disabled",false);        
                                        
                                    }',
                                ]
                            ]);
                            ?>
                        </div>

                    </li>
                </ul>

            </div>
            <div class="modal-footer">

                <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app','Cancel');?></a>

                <?php echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>'.Yii::t('app','Submit'), [
                    'class' => 'btn update-btn waves-effect ',
                    'disabled'=>true,
                    'form'=>'update-pattern-form'
                ]); ?>
            </div>
        </div>    
    </div>
</form>
<script type="text/javascript">
    var defaultPatternName='<?php echo $patternName; ?>';
    var defaultRemarks='<?php echo $remarks; ?>';
    var defaultDescription='<?php echo $description; ?>';
    var defaultIsActive=<?php echo $isActive; ?>;
    var defaultLeadingZero=<?php echo ($leadingZero)?'true':'false'; ?>;
    var defaultDigits=parseInt(<?php echo $digits; ?>);
    var defaultStartNo='<?php echo $startNo; ?>';

    $(document).ready(function(){
        $('.pattern-dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: false, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
            }
        );
        
        $('.update-specify-format-collapsible').collapsible('open', 0);
        Materialize.updateTextFields();
        
        $('#update-pattern-order-switch').prop("checked",true); 

        if($("#update-leading-pattern-id").prop("checked") === true)  {
            $('#update-leading-zero-label').attr('disabled',false);
        }else{
            $('#update-leading-zero-label').attr('disabled',true);
        }
        paddedSequenceNo=defaultStartNo;
        
        if(defaultDigits!=null && defaultLeadingZero==true){
            labelString=$("#update-sequence-number-label").val();
            labelLength=defaultDigits+labelString.toString().length;
            paddedSequenceNo=labelString.padStart(labelLength,'0');
        }
        
        $("#update-sequence-pattern-item .card-content").html(paddedSequenceNo);

        //change the pattern label in the preview when select2 has changed

        $("#update-select-experiment-label-id").on('change',function(e){
            $("#update-experiment-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });

        $("#update-select-plot-label-id").on('change',function(e){
            $("#update-plot-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });

        $("#update-select-seedlot-label-id").on('change',function(e){
            $("#update-seedlot-pattern-item .card-content").html($(this).select2('data')[0]['text']);
        });

        $("#update-pattern-form").on('submit',function(e){
            e.preventDefault();
            var data= $(this).serializeArray();
            if($( "#update-pattern-sortable" ).sortable( "instance" )==undefined){
                $( "#update-pattern-sortable" ).sortable();
            }
            
            var sortableArr=$( "#update-pattern-sortable" ).sortable('toArray');
            var pattern=[];
            var basic={};

            patternFormat='';
            for(var i=0; i<sortableArr.length;i++){
                var matches=sortableArr[i].match(/^\d*-(.+)-pattern-item-(.+$)/);
                if(matches!==null){
                    patternFormat+="{"+matches[2]+"}";
                    if(matches[1]!="text"){
                        pattern.push({
                            "is_variable":true,
                            "data_level":matches[1],
                            "value": matches[2],
                            "order":i,
                            "id":sortableArr[i]
                        });
                    }else {
                        pattern.push({
                            "is_variable":false,
                            "data_level":null,
                            "value": matches[2],
                            "order":i,
                            "id":sortableArr[i]
                        });
                    }
                }else{
                    patternFormat+="{"+$("#update-sequence-pattern-item>.grid-item>.card>.card-content").html()+"}";
                    pattern.push({
                        "is_variable":false,
                        "data_level":null,
                        "value": $("#update-sequence-pattern-item>.grid-item>.card>.card-content").html(),
                        "order":i,
                        "id":sortableArr[i]
                    });
                }
            }
            digits=null;
            if($("#update-leading-zero-label").val()!==''){
                digits=parseInt($("#update-leading-zero-label").val());
            }
            
            metadata={
                "start_seq_no":$("#update-sequence-number-label").val(),
                "leading_zeros": $("#update-leading-pattern-id").prop("checked"),
                "digits":digits,
            };

            for(var i=0; i<data.length;i++){
                basic[data[i]['name']]=data[i]['value'];
            }
            basic["programName"]=$("#update-select-program-id").select2('data')[0]['text'];
            basic["program"]=$("#update-select-program-id").val();
            basic["description"]=$("#update-description-id").val();
            basic["pattern"]=patternFormat;
            
            createJson={
                "basic":basic,
                "metadata":metadata,
                "pattern":pattern,
                "is_active": $("#update-set-active-id").prop("checked")
            };       

            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['seedlot-code-pattern/update']); ?>',
                type: 'post',
                data:{
                    value:createJson,
                    id:'<?php echo $id;?>'
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response.success){
                        $('#update-pattern-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully updated pattern.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-seedlotCode-grid-pjax',
                            replace: true
                        });
                    }else{
                        var notif = "<i class='material-icons orange-text'>warning</i>"+response.message;
                        Materialize.toast(notif,5000);
                    }
                }
            });
        });

        function checkUpdates(){

            isChanged=$("#update-remarks-id").val()===defaultRemarks && ($("#update-pattern_name").val()===defaultPatternName ) && $("#update-description-id").val()===defaultDescription && ($("#update-set-active-id").prop('checked')).toString()===defaultIsActive.toString() && ($("#update-leading-pattern-id").prop("checked")).toString()===defaultLeadingZero.toString() && ($("#update-sequence-number-label").val()==defaultStartNo);

            if( ($("#update-leading-zero-label").val()==defaultDigits || $("#update-leading-zero-label").val().trim()=='')  && ($("#update-leading-pattern-id").prop("checked")).toString()== defaultLeadingZero.toString() && isChanged){

                $(".update-btn").attr('disabled',true);        
            }else if($("#update-pattern_name").val().trim()=='' || $("#update-sequence-number-label").val().trim()=='' || $("#update-leading-zero-label").val().trim()==''){

                $(".update-btn").attr('disabled',true);        
            }else{

                $(".update-btn").attr('disabled',false);        
            }
        
        }

        $("#update-pattern_name").on('keyup paste',function(e){
            checkUpdates();
        });

        $("#update-remarks-id").on('keyup paste',function(e){
           checkUpdates();
        });

        $("#update-description-id").on('keyup paste',function(e){
            checkUpdates();
        });

        $("#update-set-active-id").on('click',function(e){
           checkUpdates();

        });

        //Add leading zero on sequence number input field and change the pattern value in the preview when enabled, when disabled, revert changes
        
        $("#update-leading-pattern-id").on('click',function(e){
            var leadingZero=parseInt($("#update-leading-zero-label").val());
            
            var labelString=$("#update-sequence-number-label").val();
            var labelLength=leadingZero+labelString.length;
            var paddedSequenceNo=labelString.padStart(labelLength,'0');
            
            if($(this).prop("checked") === true)  {
                $('#update-leading-zero-label').attr('disabled',false);
                $("#update-sequence-pattern-item .card-content").html(paddedSequenceNo);
            }else{
                $('#update-leading-zero-label').attr('disabled',true);
                $("#update-sequence-pattern-item .card-content").html(labelString);
            }
           checkUpdates();
        });

        $("#update-sequence-number-label").on('change paste',function(e){
            if($(this).val()!==""){
                var paddedSequenceNo=$("#update-sequence-number-label").val(); // no leading zeros
                if($("#update-leading-pattern-id").prop("checked") === true)  {
                    leadingZero=parseInt($("#update-leading-zero-label").val());
                    var labelString=$("#update-sequence-number-label").val();
                    var labelLength=leadingZero+labelString.length;
                    paddedSequenceNo=labelString.padStart(labelLength,'0');
                }
                checkUpdates();
                $("#update-sequence-pattern-item .card-content").html(paddedSequenceNo);      
            }else{
                $("#update-leading-pattern-id").attr("disabled",true);
                $("#update-leading-zero-label").attr("disabled",true);
                $(".update-btn").attr('disabled',true);  
            }
            
        });

        $("#update-leading-zero-label").on('change',function(e){
            if($("#update-leading-pattern-id").prop("checked") === true)  {
                labelString=$("#update-sequence-number-label").val();
                
                thisVal=parseInt($(this).val());
                labelLength=thisVal+labelString.toString().length;
                var paddedSequenceNo=labelString.padStart(labelLength,'0');
                
                $("#update-sequence-pattern-item .card-content").html(paddedSequenceNo);  
            }
            checkUpdates();
        });

        // disabled add button when there is no input
        $("#update-text-label").on('keyup paste',function(e){
            if($(this).val()==''){
                $("#update-add-text-pattern-btn").attr('disabled',true);        
            }else{
                $("#update-add-text-pattern-btn").attr('disabled',false);        
            }
        });
        //add text pattern on the preview
        $("#update-add-text-pattern-btn").on('click',function(e){
            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-text-pattern-item-'+$("#update-text-label").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#update-text-label").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';
           
            if($( "#update-pattern-sortable" ).sortable( "instance" )!==undefined){
                $( "#update-pattern-sortable" ).sortable('destroy');
            }
            if($('#update-pattern-order-switch').prop("checked")==false){
                $( "#update-pattern-sortable" ).append(thisItem);
            }else{
                $( "#update-pattern-sortable" ).prepend(thisItem);
            }
            
            $( "#update-pattern-sortable" ).sortable();
            $(".update-item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#update-pattern-sortable" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#update-pattern-sortable" ).sortable();
                }                
            });
        });
        //add experiment pattern in the preview
        $('#update-add-experiment-pattern-btn').on('click',function(e){

            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-experiment-pattern-item-'+$("#update-select-experiment-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color update-item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+

            $("#update-select-experiment-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';

            if($( "#update-pattern-sortable" ).sortable( "instance" )!==undefined){
                $( "#update-pattern-sortable" ).sortable('destroy');
            }
            
            if($('#update-pattern-order-switch').prop("checked")==false){
                $( "#update-pattern-sortable" ).append(thisItem);
            }else{
                $( "#update-pattern-sortable" ).prepend(thisItem);
            }            
            $( "#update-pattern-sortable" ).sortable();
            
            $(".update-item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#update-pattern-sortable" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#update-pattern-sortable" ).sortable();
                }                
            });
        });
        //add plot pattern in the preview
        $('#update-add-plot-pattern-btn').on('click',function(e){
            var thisTime=new Date().getTime();
            
            var thisItemId=thisTime+'-plot-pattern-item-'+$("#update-select-plot-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color update-item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#update-select-plot-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';
            if($( "#update-pattern-sortable" ).sortable( "instance" )!==undefined){
                $( "#update-pattern-sortable" ).sortable('destroy');
            }
            if($('#update-pattern-order-switch').prop("checked")==false){
                $( "#update-pattern-sortable" ).append(thisItem);
            }else{
                $( "#update-pattern-sortable" ).prepend(thisItem);
            }            
            $( "#update-pattern-sortable" ).sortable();
            
            $(".update-item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#update-pattern-sortable" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#update-pattern-sortable" ).sortable();
                }                
            });
        });
        //add seedlot pattern in the preview
        $('#update-add-seedlot-pattern-btn').on('click',function(e){

            var thisTime=new Date().getTime();
            var thisItemId=thisTime+'-seedlot-pattern-item-'+$("#update-select-seedlot-label-id").val();
            var thisItem=' <li id="'+thisItemId+'" class="ui-sortable-handle" style="" draggable="true"'+'role="option" aria-grabbed="false">'+
            '<div class="grid-item ">'+
            '<div class="card">'+
            '<a href="#!" class="close fixed-color update-item-close" id="" type="button" '+'style="font-weight:normal;margin-top: 7px;" title="Remove pattern">'+
            '<i class="material-icons">close</i>'+
            '</a>'+
            '<div class="card-content truncate" style="padding:12px">'+
            $("#update-select-seedlot-label-id").val()+
            '</div>'+
            '</div>'+
            '</div>'+
            '</li>';
            if($( "#update-pattern-sortable" ).sortable( "instance" )!==undefined){
                $( "#update-pattern-sortable" ).sortable('destroy');
            }
            if($('#update-pattern-order-switch').prop("checked")==false){
                $( "#update-pattern-sortable" ).append(thisItem);
            }else{
                $( "#update-pattern-sortable" ).prepend(thisItem);
            }
            $( "#update-pattern-sortable" ).sortable();
            
            $(".update-item-close").on('click',function(e){
                if($(this).parent().parent().parent().attr('id')==thisItemId){
                    $( "#update-pattern-sortable" ).sortable('destroy');
                    $(this).parent().parent().parent().remove();
                    $( "#update-pattern-sortable" ).sortable();
                }                
            });
        });

    });
</script>