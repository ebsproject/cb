<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

 /**
 * Index file for Find Seeds
 **/

use app\components\FavoritesWidget;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\export\ExportMenu;

use app\models\Config;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
use app\components\WorkingListWidget;

?>

<div class="row" style="margin-bottom:0px;">
    <div class="col-md-10">
        <h3>
            <?=Yii::t('app', 'Inventory Search') .
                FavoritesWidget::widget([
                    'module' => 'seedInventory',
                    'controller' => 'find-seeds',
                ])?>
        </h3>
    </div>
</div>
<div id="bg-progress-notif" style="display:none" class="info hidden">
    <div class="card-panel"><span id="bg-progress-notif-msg">test</span></div>
</div>

<div class = "row" style="";>    
    <ul class="collapsible collapsible-filter">
        <li class="" id="filters">
            <div class="progress find-loading-progress hidden" style="margin:0px;padding:0px;">
                <div class="indeterminate"></div>
            </div>
            <div class="collapsible-header active"><i class="material-icons">search</i>Search Parameters</div>
            <div class="collapsible-body" id="query-params">
                <?php echo $filterHtmlData; ?>
            </div>
                
        </li>
    </ul>    
</div>

<div class = "row" style="";>
    <?php
        $browserId = 'dynagrid-seed-search-search-grid';

        (new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

        $defaultValues = [];
        $actionColumns = [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '1%',
                'header' => false,
                'visible'=>true,
                'order' => DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'label' => "Checkbox",
                'mergeHeader' => true,
                'header' => '
                    <input type="checkbox" class="filled-in" data-id-string="" id="seed-inventory-seedlotCode_check-all" />
                    <label style="padding-left: 20px;" for="seed-inventory-seedlotCode_check-all"></label>',
                    'headerOptions'=>['class'=>'checkbox-row hidden'],
                'content' => function ($model) {
                    $isAutoSelect = Yii::$app->session['isAutoSelect'];
                    $checked='';
                    $disabled='';
                    $addClass = '';
                    if(isset($model["packageCount"]) && $model["packageCount"]==1){
                        if($isAutoSelect=='true'){ 
                            $checked='checked="true"';
                            $addClass = 'qr-default-value';
                        }
                        else{
                            $checked='';
                        }
                    }

                    if (isset($model['isDisabled']) && $model['isDisabled']) {
                        return '<span class="disabled-row"></span>';
                    } else {
                        return '
                            <input '.$checked.' class="seed-inventory-seedlotCode_checkbox '.$addClass.' filled-in '.$disabled.'" type="checkbox" id="seed-' . $model["packageDbId"] . '" value="'.$model["packageDbId"].'" />
                            <label for="seed-' . $model["packageDbId"] . '"  style="padding-left: 20px;" ></label>';
                    }
                },
                'hiddenFromExport' => true,
                'width' => '1%',
                'contentOptions'=>['class'=>'checkbox-row hidden'],
                'order' => DynaGrid::ORDER_FIX_LEFT,
            ],
            [
                'label' => "RadioButton",
                'mergeHeader' => true,
                'header' => '<button type="button" class="close kv-clear-radio transparent" title="Clear selection" style="margin-left:10px;" id="deselect-radio-btn">×</button>',
                'headerOptions'=>['class'=>'radio-btn-row'],
                'content' => function ($model) {
                    $isAutoSelect = Yii::$app->session['isAutoSelect'];
                    $limitQr = Yii::$app->session['packageLimitQr'];
                    $productId = isset($model["germplasmDbId"]) ? intval($model["germplasmDbId"]) : null; 
                    $recordId = $model["packageDbId"];
                    $checked='';
                    $addClass = '';

                    if (isset($model['isDisabled']) && $model['isDisabled']) {
                        return '<span class="disabled-row"></span>';
                    } else {

                        if((isset($model["packageCount"]) && $model["packageCount"]==1) || (isset(Yii::$app->session['packageLimitQr']) && $limitQr == 1)){
                            if($isAutoSelect=='true'){ 
                                $checked='checked';
                                $addClass = 'qr-default-value';
                            }
                            else{
                                $checked='';
                            }
                        }

                        return '
                            <input class="seed-inventory-radio-btn with-gap '.$addClass.'" type="radio" name="seed-radio-' . $productId . '" '.$checked.' id="seed-radio-id-' . $recordId . '" value="' . $recordId . '"/> <label for="seed-radio-id-' . $recordId . '""></label>
                        ';
                    }
                },
                'hiddenFromExport' => true,
                'width' => '1%',
                'contentOptions'=>['class'=>'radio-btn-row'],
                'visible'=>true,  
                'order' => DynaGrid::ORDER_FIX_LEFT
            ],
        ];

        $columns=[];

        $dataBrowserModel = \Yii::$container->get('\app\modules\seedInventory\models\FindSeedDataBrowserModel');
        $columnConfig= $dataBrowserModel->getColumns();

        $groupingOptions = [
            'attribute' => 'GERMPLASM CODE',
            'label' => 'GERMPLASM CODE',
            'encodeLabel' => '',
            'visible'=> true,
            'format' => 'raw',
            'content' => function($model){
                return '<span class="">'.$model['germplasmCode'].'</span>';
            },
            'order' => DynaGrid::ORDER_FIX_LEFT
        ];

        if ($isGrouped == 'checked') {
            $groupingOptions['group'] = true;
            $groupingOptions['groupHeader'] = function ($model, $key, $index, $widget) use ($actionColumns, $columns){
                    return [
                        'mergeColumns' => [
                            [1,6],
                            [7,9],
                            [10,25]
                        ], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            1 => $model['designation'],
                            7 => 'Package Count: '. $model['packageCount'],
                            10 => 'Seed Count: ' . $model['seedCount'],
                            //5 => 'Seedlot Available: ' . $model['product_count'],
                        ],
                        'contentFormats' => [      // content reformatting for each summary cell
                            8 => ['format' => 'number', 'decimals' => 0],
                        ],
                        'contentOptions' => [      // content html attributes for each summary cell
                            1 => ['style' => 'font-variant:small-caps'],
                            8 => ['style' => 'text-align:left'],
                        ],
                        // html attributes for group summary row
                        'options' => ['class' => 'info table-info' .$model['designation'],'style' => 'font-weight:bold;']
                    ];
                };
        }

        $columns[] = $groupingOptions;
        
        $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
        $sortAttributes = [];
        foreach ($columnConfig as $col) {
            $tooltip = (isset($col['field_description'])) ? $col['field_description'] : $col['field_label'];
            $header_name = $col['field_label'];
            
            if($col['variable_abbrev'] == 'PARENTAGE'){
                $attribute = strtolower($col['variable_abbrev']);
                $label = '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>';
                $encodeLabel = false;
                $visible = ($col['hidden']=='false')?true:false;
                
                $columns[] = [
                    'attribute' => $attribute,
                    'label' => $label,
                    'encodeLabel' => $encodeLabel,
                    'visible'=> $visible,
                    'format' => 'raw',
                    'value' => function($model) use ($col){
                        return '<div style="overflow-wrap: break-word;max-width:200px">' . $model['parentage'] . '</div>';
                    },
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ];
            }
            else if($col['variable_abbrev']=='DESIGNATION'){
                $designationColumn = [
                    'attribute' => strtolower($col['variable_abbrev']),
                    'label' =>  '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>',
                    'encodeLabel' => false,
                    'hidden' =>($col['hidden']=='true')?true:false,
                    'visible'=>($col['hidden']=='false')?true:false,
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $widget) { 
                        return $model['designation'];
                    },
                    'order' => DynaGrid::ORDER_FIX_LEFT
                ];
                $columns[] = $designationColumn;

            }
            else if($col['variable_abbrev']=='SOURCE_STUDY_NAME' || $col['variable_abbrev']=='SOURCE_STUDY'){
                $variable=$col['variable_abbrev'];
                $columns[]=[
                    'attribute' => strtolower($col['variable_abbrev']),
                    'label' =>  '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>',
                    'encodeLabel' => false,
                    'hidden' =>($col['hidden']=='true')?true:false,
                    'visible'=>($col['hidden']=='false')?true:false,
                    'format' => 'raw',
                    'value'=>function($model) use($variable, $notSet){
                        $entityName = (isset($model[strtolower($variable)])) ? $model[strtolower($variable)] : $notSet;

                        $source_study_id = isset($model["source_study_id"]) ? $model["source_study_id"] : null; 
                        $linkAttrs = [
                            'title' => \Yii::t('app', 'Click to view more information'),
                            'data-entity_id' => $source_study_id,
                            'data-entity_label' => $entityName,
                            'data-entity_type' => 'study',
                            'data-target' => '#view-entity-modal',
                            'data-toggle' => 'modal'
                        ];

                        $entityClass = ['class' => 'blue-text text-darken-4 view-more-info header-highlight'];
                        $viewMoreInfoClass = ['class' => 'view-more-info fixed-color'];

                        return Html::a($entityName, '#', array_merge($entityClass,$linkAttrs));
                    }
                ];
            } else if ($col['variable_abbrev'] == 'SOURCE_HARV_YEAR'){
                $variable=$col['variable_abbrev'];
                $columns[]=[
                    'attribute' => strtolower($col['variable_abbrev']),
                    'label' =>  '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>',
                    'encodeLabel' => false,
                    'hidden' =>($col['hidden']=='true')?true:false,
                    'visible'=>($col['hidden']=='false')?true:false,
                    'format' => 'raw',
                    'value'=>function($model){
                        if (isset($model['harvestDate']) && !empty($model['harvestDate'])) {
                            $harvestDate = explode('-',$model['harvestDate']);

                            return (isset($harvestDate[0])) ? $harvestDate[0] : null;
                        }
                        return null;
                    }
                ];
            }else{

                $attribute = ($col['variable_abbrev'] == 'PROGRAM') ? strtolower('SEED_MANAGER') : strtolower($col['variable_abbrev']);
                $label = '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>';
                $encodeLabel = false;
                $visible = ($col['hidden']=='false')?true:false;
                
                $columns[] = [
                    'attribute' => $attribute,
                    'label' => $label,
                    'encodeLabel' => $encodeLabel,
                    'visible'=> $visible,
                    'format' => 'raw',
                    'value' => function($model) use ($col){
                        $colAbbrev = strtolower($col['variable_abbrev']);
                        if (isset($model[$col['api_body_param']]) && $col['api_body_param'] != null && !empty($col['api_body_param'])) {
                            return ($col['api_body_param'] == 'germplasmOtherNames' ? nl2br(str_replace('|',"\r\n",$model[$col['api_body_param']])) : $model[$col['api_body_param']]);
                        } else if (isset($model[$colAbbrev])) {
                            return $model[$colAbbrev];
                        } else { return null; }
                    }
                ];
            }
            if (!in_array($col['variable_abbrev'],['PROGRAM','SOURCE_HARV_YEAR'])) {
                $sortAttributes[] = strtolower($col['variable_abbrev']);
            }
        }
                
        $gridColumns = array_merge($actionColumns, $columns);

        if (!empty($dataProvider) && ($dataProvider instanceof app\dataproviders\ArrayDataProvider)) {
            $dataProvider->setSort(
                ["attributes" => $sortAttributes]
            );
        }

        $resultsConfigOptions = "
            <ul id='dropdown-config-find-seeds' class='dropdown-content' style='width: 180px !important; margin-top:5px; margin-left:5px;' data-beloworigin='false'>
                <li>
                    <div class='switch left' style='display:inline-block; margin-top: 10px; margin-left: 5px;'>
                        <label style='margin-bottom:0px;' title='Enable selection of multiple records for each search result group.'>
                            <input type='checkbox' ".$isMultipleSelect." id='selectOption-switch-id' data-pjax=0>
                            <span class='lever'></span>Multiple Selection
                        </label>
                    </div>
                </li>
                <li>
                    <div class='switch left' style='display:inline-block; margin-top: 10px; margin-left: 5px;'>
                        <label style='margin-bottom:0px;' title='Automatically select the search result group with a singular record.'>
                            <input type='checkbox' ".$isAutoSelect." id='autoSelectOption-switch-id' data-pjax=0>
                            <span class='lever'></span>Automatic Selection
                        </label>
                    </div>
                </li>
                <li>
                    <div class='switch left' style='display:inline-block; margin-top: 10px; margin-left: 5px;'>
                        <label style='margin-bottom:0px;' title='Group search result records according to Germplasm Name.'>
                            <input type='checkbox' ".$isGrouped." id='grouped-switch-id' data-pjax=0>
                            <span class='lever'></span>Group Results
                        </label>
                    </div>
                </li>
                <li>
                    <div class='switch left' style='display:inline-block; margin-top: 10px; margin-left: 5px;'>
                        <label style='margin-bottom:0px;' title='Retain selected search result records after addition to Working List.'>
                            <input type='checkbox' ".$isRetained." id='retained-switch-id' data-pjax=0>
                            <span class='lever'></span>Retain Selection
                        </label>
                    </div>
                </li>
            </ul>";

        // Build notification button and area
        echo '<div id="notif-container">
                <ul id="notifications-dropdown" class="dropdown-content notifications-dropdown"></ul>
            </div>';

        $notifBtn = Html::a(
            '<i class="material-icons large">notifications_none</i>
            <span id="ss-notif-badge-id" class=""></span>',
            '#',
            [
                'id' => 'seeds-search-notifs-btn',
                'class' => 'btn waves-effect waves-light seeds-search-notification-button wl-tooltipped',
                'data-pjax' => 0,
                'style' => "overflow: visible !important; margin-left:5px;",
                "data-position" => "top",
                "data-tooltip" => \Yii::t('app', 'Notifications'),
                "data-activates" => "notifications-dropdown",
                'disabled' => false,
            ]
        );

        // Temporary! For initial implementation of Advanced Search
        $pagerInfo = [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ];

        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme' => 'panel-default',
            'showPersonalize' => true,
            'storage' => DynaGrid::TYPE_SESSION,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'tableOptions'=>[
                    'class' => $browserId.'-table'
                ],
                'options'=>[
                    'id' => $browserId.'-table-con'
                ],
                'id' => $browserId.'-table-con',
                'striped'=>false,
                'hover' => true,
                'toggleDataContainer' => [
                    'class' => 'btn-group mr-2'
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'responsive' => true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options'=>[
                        'id'=>$browserId
                    ],
                    'beforeGrid'=>'',
                    'afterGrid'=>''
                ],
                'responsiveWrap' => false,
                'panel' => [
                    'heading' => false,
                    'before' => \Yii::t('app', 'Manage your search results here.') . ' {summary}' .
                    '<br/>
                    <p id="total-selected-text" class="pull-right" style="margin:-1px;"></p>
                    <p id="total-selected-text2" class = "pull-right" style = "margin: -1px;">
                        <b><span id="selected-items-count"> </span></b> <span id="selected-items-text"> </span>
                    </p>',
                    'after' => false,
                ],
                'rowOptions'=>function($model){
                        if( isset($model['includeseedlistoption']) && !empty($model['includeseedlistoption']) && $model['includeseedlistoption']=='includeRow'){
                            return ['class' => 'includedSeedlot'];   
                        }

                        if( isset($model['product_group']) && !empty($model['product_group']) && $model['product_group'] == 1){
                            return ['class' => 'product_group1'];
                        }else{
                            return ['class' => 'product_group2'];
                        }
                },
                'toolbar' => [
                    [
                        'content' =>
                            '&nbsp;' .
                            Html::Button(
                                '<i class="glyphicon glyphicon-plus"></i>', 
                                [
                                    'class' => 'waves-effect waves-light btn btn-primary light-green darken-3',
                                    'style' => 'margin-left:5px;margin-right:0px;',
                                    'id' => 'add-to-list-btn',
                                    'disabled' => true,
                                    'data-position' => 'top',
                                    'data-delay'=>'50',
                                    'title'=> \Yii::t('app', "Add to Seedlist"),
                            ]).
                            Html::Button(
                                '<i class="material-icons right">add_to_photos</i>', 
                                [
                                    'class' => 'waves-effect waves-light btn btn-primary light-green darken-3',
                                    'style' => 'margin-right:0px; margin-left: 0px;',
                                    'id' => 'add-all-to-list-btn',
                                    'disabled' => false,
                                    'data-position' => 'top',
                                    'data-delay'=>'50',
                                    'title'=> \Yii::t('app', "Add ALL records to Seedlist"),
                            ]).
                            WorkingListWidget::widget(
                                [
                                'returnUrl' => 'seedInventory/find-seeds/find',
                                'listType' => 'package',
                                'sourceTool' => 'Inventory Search',
                                'itemCount' => $workingListItemsCount ?? 0
                            ]).'&emsp;'.
                            Html::a(
                            '<i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>', '#!',
                            [
                                'class' => 'waves-effect waves-light btn btn-primary dropdown-trigger dropdown-button-pjax',
                                'style' => 'margin-right:0px; margin-left: 5px;',
                                'data-beloworigin' => 'true',
                                'data-constrainwidth' => 'false',
                                'data-activates'=>'dropdown-config-find-seeds',
                                'title'=> \Yii::t('app', "Search results settings")
                            ]) . '&emsp;'.
                            $resultsConfigOptions.
                            $notifBtn.'&emsp;'.
                            Html::a(
                                '<i class="glyphicon glyphicon-repeat"></i>',
                                ['',"program"=>$program],
                                [
                                    'class' => 'btn btn-default', 
                                    'id'=>'reset-search-grid',
                                    'style' => 'margin-right:0px;',
                                    'title'=>\Yii::t('app','Reset grid'),
                                    'data-pjax' => true
                                ]).
                            '{dynagridFilter}{dynagridSort}{dynagrid}',
                    ],
                ],
                'pager' => $pagerInfo,
            ],
            'options' => [
                'id' => $browserId
            ],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => 'Remove',
            ]
        ]);
        
        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }
        
        DynaGrid::end();
    ?>
</div>

<?php
echo \Yii::$app->controller->renderPartial('_modals.php');

$workerName = 'AddListItems';

$findUrl = Url::to(['find',"program"=> isset($programAbbrev) && !empty($programAbbrev) ? $programAbbrev : $program]);
$createListUrl = Url::to(['list-management/create-list']);
$updateWorkingListUrl = Url::to(['list-management/update-working-list']);
$addListUrl = Url::to(['list-management/add-list-items']);
$addAllListUrl = Url::to(['list-management/add-all-to-list']);
$removeListUrl = Url::to(['list-management/remove-list-items']);
$validateAbbrevUrl = Url::to(['list-management/validate-list-abbrev']);
$saveListItemsUrl = Url::to(['list-management/save-list-items']);
$getItemCountUrl = Url::to(['list-management/get-items-count']);
$viewEntityUrl = Url::to(['/search/default/viewentity']);
$exportEntityUrl = Url::to(['/search/default/export']);
$getSeedListProviderUrl = Url::to(['list-management/get-saved-seed-list-data-provider']);
$getListByIdUrl = Url::to(['list-management/get-selected-list-items']);
$getSeedListByIdUrl = Url::to(['list-management/get-selected-seed-list-items']);
$getQueryParamBrowserState = Url::to(['get-query-param-state']);
$setIsMultipleSelectUrl = Url::to(['set-session-selection']);
$setIsAutoSelectUrl = Url::to(['set-session-auto-selection']);
$setIsGroupedUrl = Url::to(['set-session-is-grouped']);
$setIsRetainedUrl = Url::to(['set-session-is-retained']);
$saveSelectedItemsUrl = Url::to(['save-user-selection']);
$emptySelectedItemsUrl = Url::to(['list-management/empty-user-selection']);
$getSelectedItemsUrl = Url::to(['get-user-selection']);
$clearAllUrl = \yii\helpers\Url::to(['list-management/empty-working-list']);
$validateIdsUrl = Url::to(['list-management/validate-seed-ids']);
$getSearchDataUrl = Url::to(['list-management/get-search-data']);
$checkBgJobUrl = Url::to(['check-background-job']);
$getBasicFiltersUrl = Url::to(['generate-basic-filters']);
$saveWorkingListUrl = Url::to(['list-management/save-working-list']);
$getExperimentProviderUrl = Url::to(['experiment-creation/get-experiment-data-provider', 'program' => $program]);
$addToExperimentUrl = Url::to(['experiment-creation/add-to-experiment', 'program' => $program]);

// for notifications
$newNotificationsUrl = Url::to(['/workingList/default/new-notifications-count','workerName' => $workerName, 'remarks' => 'seed search']);
$notificationsUrl = Url::to(['/workingList/default/push-notifications', 'workerNames' => 'AddListItems']);
$browserUrl = Url::to(['index']);

// For working list actions
$urlWorkingList = Url::to(['/workingList/default/index']);

$appName = app\controllers\Constants::APP_NAME;

$filtervalues = json_encode($filterValues);
$filterfields = json_encode($filters);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

$this->registerJs(<<<JS
    var browserGridId = '$browserId'
    var qResultsSessionStorage = browserGridId+"-table-con_checkbox_storage";
    var qResultsGridId = browserGridId+"-table-con";
    var qResultsSelectClass = "seed-inventory-seedlotCode_checkbox";
    var qResultsPjaxGridId = browserGridId+"-pjax";
    var qResultsDisplaySummaryId = "total-selected-text";
    var qResultsSelectAllId = "seed-inventory-seedlotCode_check-all";
    var ssWorkerNames = '$workerName';
    var ssNewNotificationsUrl = '$newNotificationsUrl';
    var ssNotificationsUrl = '$notificationsUrl';
    var ssbrowserUrl = '$browserUrl';
    var ssRefreshBrowserUrl = '$findUrl';
    var ssgridId = '$browserId';
JS, 
yii\web\View::POS_BEGIN);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

// add global js for notifications
$this->registerJsFile("@web/js/find-seeds/notifications.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<<JS
    var browserGrdId = '$browserId';
    var seedListBrowserGrdId = 'dynagrid-find-seed-list';

    var hasFilter = false, switchToMultiple = false;
    var selectSwitchVal='', autoSelectSwitchVal='';
    var includeSeedlist=false;
    var showWorkingList= $(".find-seeds-list").hasClass('hidden') ? false : true;
    var selectedItems=[];
    var userSelection = [];
    var currentlySelectedItems=[];
    var bottomBorder = 180;
    var savedWorkingList = false, addedItems = false, removeItems = false;
    var getIdsInBgProcess = true;
    var filterValues = '$filtervalues';
    var filterfields = '$filterfields';

    // Working list threshold
    var workingListItemLimit = 40000;
    var workingListCount = '$workingListItemsCount';

    checkBackgroundProcessStatus();
    setInterval(checkBackgroundProcessDelete, 20000);
    setInterval(checkBackgroundProcessStatus, 20000);
    resetSessionSelection();
    
    // prep notifications
    renderNotifications();
    notifDropdown();
    updateNotif();

    if ('$resetSelection' == 'true') {
        sessionStorage.removeItem(browserGrdId+'-table-con'+'_checkbox_storage');
        displayTotalCountFS(null);
    }

    if(window.innerHeight < 800){
        $("#"+browserGrdId+"-table-con-container").css('height','750px');
    }
    else{
        $("#"+browserGrdId+"-table-con-container").css('height',window.innerHeight-bottomBorder);
    }

    // getQueryParamBrowserState();
    $('.dropdown-trigger').dropdown();
    $('.dropdown-button').dropdown();

        // Redirect to working list
    $(document).on('click', '#working-list-btn', function(){
        window.location = '$urlWorkingList';
    });
    
    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();

        // check for bg process
        checkBackgroundProcessStatus();

        // prep motifications
        manageNotifs();

        if(filterfields.length == 2){
            generateBasicFilters();
        }

        var itemCount = '$itemCount';

        changeSelection($('#selectOption-switch-id').is(':checked'));
        autoSelectItems($('#autoSelectOption-switch-id').is(':checked'));

        var retain = $('#retained-switch-id').is(':checked');

        if(retain){
            disableSelectionRows();
        }

        if (currentlySelectedItems == null) {
            currentlySelectedItems = [];
        }
        if (currentlySelectedItems.length != 0) {
            disableButtons = false;
            $("#add-to-list-btn").attr("disabled",disableButtons);
        } else {
            $('#selected-items-count').html('');
            $('#selected-items-text').html('');
        }

        var currentSelection = sessionStorage.getItem(qResultsSessionStorage);
        if($('#autoSelectOption-switch-id').is(':checked') && currentSelection != ''){
            getAllSinglePackageIds()
        }

        // resets search results browser
        $(document).on("click","#reset-search-grid", function(e) {
            e.preventDefault();

            $.pjax.reload({
                container: '#'+browserGrdId+'-pjax', 
                url:'$findUrl',
                replace:false,
                type: 'POST',
                data: {
                    forceReset:true
                }
            });
        });
    });

    // refreshes page on pjax success
    $(document).on('ready pjax:success','#'+browserGrdId+'-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();

        $('.dropdown-trigger').dropdown();
        $('.seed-inventory-radio-btn').each(function(){ count++; });

        if(count > 0){
            $("#add-all-to-list-btn").attr("disabled",false);
        }
        if(includeSeedlist){
            $("#removeRowOption-checkbox-id").attr("checked",true);
        }else{
            $("#removeRowOption-checkbox-id").attr("checked",false);
        }

        // TEMPRORARY
        $('td[name="FindSeedDataBrowserModel[mta_status]"]').attr("hidden",false);
        $('td[name="FindSeedDataBrowserModel[mc_cont]"]').attr("hidden",false);
        $('td[name="FindSeedDataBrowserModel[rsht_no]"]').attr("hidden",false);

        changeSelection($('#selectOption-switch-id').is(':checked'));
        autoSelectItems($('#autoSelectOption-switch-id').is(':checked'));

        var retain = $('#retained-switch-id').is(':checked');
        var val = $('#selectOption-switch-id').is(':checked');

        if(retain){
            disableSelectionRows();
        }

        setTimeout(function(){
            var disableButtons = true;
            if (currentlySelectedItems == null) {
                currentlySelectedItems = [];
            }
            if (currentlySelectedItems.length != 0) {
                disableButtons = false;
            } else {
                $('#selected-items-count').html('');
                $('#selected-items-text').html('');
            }

            $("#add-to-list-btn").attr("disabled",disableButtons);
        }, 1000);
        resetSessionSelection();

        var currentSelection = sessionStorage.getItem(qResultsSessionStorage);

        if($('#autoSelectOption-switch-id').is(':checked') && currentSelection != ''){
            getAllSinglePackageIds()
        }

        manageNotifs();
    });

    //adjust height of seed list and results data browser after pjax
    $(document).on('ready pjax:complete','#'+browserGrdId+'-pjax', function(e) {
        e.stopPropagation();
        e.preventDefault();

        updateWorkingListItemCount();

        // check for bg process
        checkBackgroundProcessStatus();

        $(".find-loading-progress").addClass("hidden");
        $('.dropdown-trigger').dropdown();
        
        var bottomPadding = 50;
        if(window.innerHeight < 800){
            $("#"+browserGrdId+"-table-con-container").css('height','750px');
        }
        else{
            $("#"+browserGrdId+"-table-con-container").css('height',window.innerHeight-bottomBorder);
        }

        manageNotifs();
    });

    // collapse query parameter browser
    function getQueryParamBrowserState(){
        $.ajax({
            url: '$getQueryParamBrowserState',
            type: 'post',
            success: function(response) {
                if(response == 1 || response == true){
                    $('.collapsible-filter').collapsible('close', 0);
                }
            }
        });
    }

    // resets the session selection on reset of grid
    function resetSessionSelection() {
        $('#reset-search-grid').on('click', function() {
            sessionStorage.removeItem(browserGrdId+'-table-con'+'_checkbox_storage');
            displayTotalCountFS(null);
        });
    }

    // generates basic filters and adds the returned filters to the query search parameter section
    function generateBasicFilters(){
        $('#add-basic-filters-loading-progress').removeClass('hidden').trigger('change');

        $.ajax({
            url: '$getBasicFiltersUrl',
            type: 'POST',
            success: function(response) {
                data = JSON.parse(response);
                if(data){
                    $('#basic_filters_div').html(data.filtersHTML).trigger('change');
                    $('#add-basic-filters-loading-progress').addClass('hidden').trigger('change');
                    
                    refreshQueryParameterValues(data.filters,data.filterSelection,[]);
                }
            },
            error: function(error){

            }
        })
    }

    // option to display radio button or checkbox for single or multiple selection of product
    $(document).on('click','#selectOption-switch-id', function(e){
        val=$(this).is(':checked');
        changeSelection(val);

        var retain = $('#retained-switch-id').is(':checked');

        autoSelectItems($('#autoSelectOption-switch-id').is(':checked'));
        // save selected option in session
        $.ajax({
            url: '$setIsMultipleSelectUrl',
            type: 'POST',
            dataType: 'json',
            data: {isMultipleSelect: selectSwitchVal},
            success: function(response) {
                // clear selection if switching from multiple to single
                if(val){
                    $.pjax.reload({
                        container: '#'+browserGrdId+'-pjax', 
                        replace:false
                    });
                }
                else{
                    $('#clear-selection-confirm-modal').modal('show');
                }
            }
        })
    });

    // option to set automatic selection of single-package result groups
    $(document).on('click','#autoSelectOption-switch-id', function(e){
        var value = $('#'+browserGrdId+'-pjax .kv-panel-before .summary').html();
        var totalCount = 0;

        if(value != null){
            totalCount = getBrowserItemCount(browserGrdId+'-pjax');
        }
        
        clearSelectionFS();
        
        if(totalCount > 0){
            val=$(this).is(':checked');
            autoSelectItems(val);
        
            $.ajax({
                url: '$setIsAutoSelectUrl',
                type: 'POST',
                dataType: 'json',
                data: {isAutoSelect: val},
                success: function(response) {
                    $.pjax.reload({
                        // url: $('.find-seeds-filter').attr('action'),
                        container: '#'+browserGrdId+'-pjax', 
                        replace:false
                    });
                }
            });
        }
        else{
            $('#autoSelectOption-switch-id').prop('checked',false);
            $('#autoSelectOption-switch-id').removeAttr('checked');
        }
    });

    $(document).on('click','#grouped-switch-id', function(e){
        
        clearSelectionFS();
        
        val=$(this).is(':checked');
        $.ajax({
            url: '$setIsGroupedUrl',
            type: 'POST',
            dataType: 'json',
            data: {isGrouped: val},
            success: function(response) {

                if (val) {
                    $.pjax.reload({
                        url: $('.find-seeds-filter').attr('action'),
                        container: '#'+browserGrdId+'-pjax', 
                        replace: true
                    });
                } else {
                    $.pjax.reload({
                        // url: $('.find-seeds-filter').attr('action'),
                        container: '#'+browserGrdId+'-pjax', 
                        replace:false
                    });
                }
            }
        });
    });

    $(document).on('click','#retained-switch-id', function(e){
        val=$(this).is(':checked');

        $.ajax({
            url: '$setIsRetainedUrl',
            type: 'POST',
            dataType: 'json',
            data: {isRetained:val},
            success: function(response) {
                $.pjax.reload({
                    container: '#'+browserGrdId+'-pjax', 
                    replace:false
                });
            }
        });
    });

    // deselect all radio buttons in the results data browser
    $(document).on('click','#deselect-radio-btn', function(e){
        // $('#add-all-to-list-btn').attr('disabled',true);

        val=$('#selectOption-switch-id').is(':checked');
        var ids=[];        
        if(val==true){ // multiple select
            ids = retrieveCheckedItems();
        }else{  // single select
            ids = retrieveRadioItems();
        }
        $(".seed-inventory-radio-btn").prop("checked",false);
        
        if(ids==[]){
            $('#add-to-list-btn').attr('disabled',true);
        }

        clearSelectionFS();
    });

    // click the header checkbox in seed list
    $(document).on('click','#select-seed-list', function(e){
        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.seed-inventory-seed-list-select').attr('checked','checked');
            $('.seed-inventory-seed-list-select').prop('checked',true);
            $(".seed-inventory-seed-list-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
        }else{
            $(this).removeAttr('checked');
            $('.seed-inventory-seed-list-select').prop('checked',false);
            $('.seed-inventory-seed-list-select').removeAttr('checked');
            $("input:checkbox.seed-inventory-seed-list-select").parent("td").parent("tr").removeClass("grey lighten-4");
        }
    });

    // cancel switch from multiple selection to single selection
    $(document).on('click','#cancel-clear-selection-btn', function(e){
        $('#clear-selection-confirm-modal').modal('hide');
        
        changeSelection(true);
        $('#selectOption-switch-id').attr('checked','checked');
        $('#selectOption-switch-id').prop('checked',true);
    });

    // proceed with switch from multiple selection to single selection
    $(document).on('click','#clear-user-selection-btn', function(e){        
        changeSelection(false);
        clearSelectionFS();
        emptySessionUserSelection();

        $('#clear-selection-confirm-modal').modal('hide');
    });

    // get seedlot IDs of checked checkboxes
    function retrieveCheckedItems(){
        var ids = getSelectionFS();
        return ids;
    }

    // get seedlot IDs of checked radio buttons
    function retrieveRadioItems(){
        var ids = getSelectionFS();
        return ids;
    }

    // Add to seed list 
    $(document).on('click','#add-all-to-list-btn', function(e) {
        var value = $('#'+browserGrdId+'-table-con .kv-panel-before .summary').html();
        var totalCount = 0;

        if(value != null){
            // Get browser total browser result
            totalCount = getBrowserItemCount(browserGrdId+'-table-con');

            // Disable add all button temporarily to avoid double click
            $("#add-all-to-list-btn").attr('disabled',true);

            var limit = '$insertLimit';
            addedItems = false;

            // Determine if the total of items in the working list and the added items in the browser are within the working list threshold
            totalCount = parseInt(workingListCount) + parseInt(totalCount);

            if (totalCount > 0) {
                if(totalCount <= workingListItemLimit){
                    // Notify that currently processing
                    showMessage('Addition of items to working list in progress..','message');
                    val=$('#selectOption-switch-id').is(':checked');
                    var retainSelection = $('#retained-switch-id').is(':checked');
                    var ids=[];       

                    if($('.find-seeds-list').hasClass('hidden')){
                        $('.find-seeds-list').removeClass('hidden');
                        $('.find-seeds-list').show();
                    }
                    
                    $(".find-loading-progress").removeClass("hidden");
                    
                    if(totalCount > limit){
                        var message = 'Addition of working list items will be sent for background process.<br>'+
                                'Check the notification button for progress. Working list item count will be updated once the process has complete';
                        showMessage(message,'info');
                    }

                    setTimeout(function(){
                        $.ajax({
                            url: '$addAllListUrl',
                            type: 'post',
                            dataType: 'json',
                            cache: false,
                            data: {
                                totalCount: totalCount,
                                limit: limit
                            },
                            success: function(response) {
                                addedItems = response.success;
                                if(response.success != false){

                                    if(totalCount < limit){
                                        $('.working-list-container').removeClass('hidden');
                                        $('.find-seeds-list').removeClass('hidden');
                                        $('.find-seeds-list').show();
                                        
                                        $('#list-view-body').css('display','block');
                                        showWorkingList = true;
                                    }
                                    else{
                                        manageNotifs()
                                        
                                        checkBackgroundProcessStatus();

                                        $("#add-to-list-btn").attr('disabled',true);
                                        $("#add-all-to-list-btn").attr('disabled',true);
                                    }
                                }
                                else{
                                    showMessage('Error encountered in adding items to working list.','error');
                                }

                                $(document).on('click','#select-seed-list', function(e){
                                    if($(this).prop("checked") === true)  {
                                        $(this).attr('checked','checked');
                                        $('.seed-inventory-seed-list-select').attr('checked','checked');
                                        $('.seed-inventory-seed-list-select').prop('checked',true);
                                        $(".seed-inventory-seed-list-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
                                    }else{
                                        $(this).removeAttr('checked');
                                        $('.seed-inventory-seed-list-select').prop('checked',false);
                                        $('.seed-inventory-seed-list-select').removeAttr('checked');
                                        $("input:checkbox.seed-inventory-seed-list-select").parent("td").parent("tr").removeClass("grey lighten-4");
                                    }
                                });
                            },
                            complete: function(){
                                updateSearchResults();
                                updateWorkingListItemCount();

                                if(totalCount < limit){
                                    if(!retainSelection){
                                        $.pjax.xhr = null;
                                        $.pjax.reload({
                                            url:'$findUrl',
                                            container: '#'+browserGrdId+'-pjax', 
                                            replace:false
                                        });
                                    }
                                    //flash notification if success
                                    showFlashMessage(totalCount);

                                    var hasToast = $('body').hasClass('toast');
                                    if(!hasToast){
                                        $('.toast').css('display','none');
                                        showMessage('Successfully added items to working list!','success');
                                    }
                                }
                                $(".find-loading-progress").addClass("hidden");
                            }
                        });
                    }, 500);

                    if(totalCount > limit){
                    setInterval(checkBackgroundProcessStatus, 20000);
                    }
                }
                else{
                    // If added items exceed the working list limit 
                    var message = 'Exceed maximum Working List limit of '+ workingListItemLimit +' items.'
                    showMessage(message,'warning');
                    $(".find-loading-progress").addClass("hidden");
                }
            }
        }    
        else {
            showMessage('Empty search results!','error');
        }    
    });
   
    // Add selected items from query results to seed list 
    $(document).on('click','#add-to-list-btn', function(e) {
        var val = $('#selectOption-switch-id').is(':checked');
        var selectionRetained = $('#retained-switch-id').is(':checked');
        var ids=[];    
        
        if(val == true){ // multiple select
            ids = retrieveCheckedItems();
        }else{  // single select
            ids = retrieveRadioItems();
        }  
        
        // clear session storage
        clearSelectionFS();
        
        $(".find-loading-progress").removeClass("hidden");
        $("#add-all-to-list-btn").attr('disabled',true);
        $("#add-to-list-btn").attr('disabled',true);
        
        var totalCount = ids.length;
        var limit = '$insertLimit'; 
        addedItems = false;
        showMessage('Adding items to list in progress...','message');
        
        if(totalCount < 1){
            showMessage('No items selected.','warning');
            $(".find-loading-progress").removeClass("hidden");
        }
        else{
            setTimeout(function(){
                checkBackgroundProcessStatus();
                addSelectedItemsToWorkingList(totalCount,limit,JSON.stringify(ids));
            }, 500);
        }
    });

    // facilitate adding of selected items to the working list
    function addSelectedItemsToWorkingList(totalCount,limit,ids){
        $.ajax({
            url: '$addListUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: {
                ids: ids
            },
            success: function(response) {
                addedItems = response.success;

                if(response.success && !response.bgprocess){
                    $('.collapsible-seed-list').removeClass('hidden');

                    $('.collapsible-seed-list').removeClass('hidden');
                    $('.working-list-container').removeClass('hidden');
                    $('.find-seeds-list').removeClass('hidden');
                    $('.collapsible-seed-list').show();
                    $('.find-seeds-list').show();
                    
                    $('#list-view-body').css('display','block');
                    showWorkingList = true;
                }
                else if(response.success && response.bgprocess){
                    showMessage('Addition of working list items is successfully sent for background process!','success');
                    checkBackgroundProcessStatus();
                }
                else{
                    showMessage('Error in adding items to working list.','error');
                    $(".find-loading-progress").removeClass("hidden");
                    $("#add-to-list-btn").attr("disabled",true);
                    $('#add-all-to-list-btn').attr('disabled',true);
                }
                
                $(document).on('click','#select-seed-list', function(e){
                    if($(this).prop("checked") === true)  {
                        $(this).attr('checked','checked');
                        $('.seed-inventory-seed-list-select').attr('checked','checked');
                        $('.seed-inventory-seed-list-select').prop('checked',true);
                        $(".seed-inventory-seed-list-select:checkbox").parent("td").parent("tr").addClass("grey lighten-4");
                    }else{
                        $(this).removeAttr('checked');
                        $('.seed-inventory-seed-list-select').prop('checked',false);
                        $('.seed-inventory-seed-list-select').removeAttr('checked');
                        $("input:checkbox.seed-inventory-seed-list-select").parent("td").parent("tr").removeClass("grey lighten-4");
                    }
                });
            },
            complete: function(){
                updateSearchResults();

                if(totalCount < limit){
                    $.pjax.xhr = null;
                    $.pjax.reload({
                        url:'$findUrl',
                        container: '#'+browserGrdId+'-pjax',
                        replace:false
                    });
                }
                $(".find-loading-progress").addClass("hidden");

                // Flash notification if success
                showFlashMessage(totalCount);

                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    showMessage('Successfully added items to the Working List.','success');
                }
            }
        });
    }

    // call to validate seed ids to be added ti working list
    function validateSeedIds(sessionUserSelection){
        ids = [];
        $.ajax({
            url: '$validateIdsUrl',
            type: 'post',
            dataType: 'json',
            cache: false,
            async:false,
            data: {
                ids: JSON.stringify(sessionUserSelection)
            },
            success: function(response) {
                ids = response.newList['newListMembers'];

                if (response.labels !== '' && response.labels !== null) {
                    var exist = response.labels.split(',');
                    Materialize.toast("<i class='material-icons orange-text'>warning</i>&nbsp; There are " + exist.length + " duplicate values selected", 5000, );
                }
            },
            complete: function(){
            }
        });
        
        return ids;
    }

    // call to update search results after processing of items for working lists
    function updateSearchResults(){
        $.ajax({
            url: '$getSearchDataUrl',
            type: 'post',
            cache: false,
            async:false,
            data: {},
            success: function(response) {
            },
            complete: function(){
                $(".find-loading-progress").addClass("hidden");
            }
        });
    }

    // Close seed search list
    $(document).on('click','#cancel-list-btn', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        if(showWorkingList){
            showWorkingList=false;

            $(this).html('<i class="material-icons">visibility</i>');
            $(this).attr('title','Show List Viewer');

        }else{
            showWorkingList=true;
            $(this).attr('title','Hide List Viewer');
            $(this).html('<i class="material-icons">visibility_off</i>');
        }

        $.pjax.xhr = null;
        $.pjax.reload({
            // url:'$findUrl',
            container: '#'+browserGrdId+'-pjax',
            replace:false
        });
    });

    function getBrowserItemCount(browserId){
        var value = $('#'+browserId+' .kv-panel-before .summary').html();
        var totalCount = 0;

        if(value != null){
            var split = value.split('of');

            if(split != null && split.length == 2){
                split = split[1].replace('items', '');
                split = split.replace('item', '');
                split = split.replace('<b>', '');
                split = split.replace('</b>', '');
                split = split.replace(' ', '');
                split = split.replace(',', '');

                totalCount = parseInt(split);
            }
        }   
        return totalCount;
    }

    function getWorkingListItemCount(){
        var value = $('#'+seedListBrowserGrdId+' .kv-panel-before .summary').html();
        var totalCount = 0;

        if(value != null){
            var split = value.split('of');

            if(split != null && split.length == 2){
                split = split[1].replace('items', '');
                split = split.replace('item', '');
                split = split.replace('<b>', '');
                split = split.replace('</b>', '');
                split = split.replace(' ', '');
                split = split.replace(',', '');

                totalCount = parseInt(split);
            }
        }   
        return totalCount;
    }

    // Checks if list has items
    $(document).on('click','#save-list-btn', function(e) {

        event.preventDefault();
        
        var totalCount = getWorkingListItemCount();
        if(totalCount > 0){
            $('#confirm-save-modal').modal('show');
        }
        else{
            showMessage("Notice! Working list is empty!",'warning');
        }

    });
    
    // Checks if list has items
    $(document).on('click','#open-add-to-experiment-modal-btn', function(e) {
        event.preventDefault();

        var totalCount = getWorkingListItemCount();
        if(totalCount > 0){
            // retrieve experiments here
            $('#add-to-experiment-modal').modal('hide');

            $.ajax({
                url: '$getExperimentProviderUrl',
                type: 'POST',
                data:{
                },
                success: function(response) {
                    $('.add-to-experiment-modal-body').html(response);
                    $('#add-to-experiment-modal').modal('show');
                    $('#add-to-experiment-btn').attr('disabled',false);
                },
                error: function() {
                }
            })
        }
        else{
            showMessage("Notice! Working list is empty!",'warning');
        }
    });

    // close add to experiment modal
    $(document).on('click','#cancel-open-add-to-experiment-modal-btn', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#add-to-experiment-modal').modal('hide');
    });

    // retrieve search parameters and pass on to find()
    $(document).on('click','#add-to-experiment-btn', function(e) {
        $('#add-to-experiment-btn').attr('disabled',true);
        e.stopPropagation();
        e.preventDefault();

        var selectedExperiment = null;
        $(".selected-experiment-select-radio").each(function(){
            if($(this).prop("checked") === true)  {
                selectedExperiment = $(this).attr("id");
            }
        });

        if(selectedExperiment != null) {
            selectedExperiment = parseInt(selectedExperiment.split('-')[3]);
            $(".find-loading-progress").removeClass("hidden");
            $('#add-to-experiment-modal').modal('hide');
            showMessage('Entries are being added to the selected experiment. Redirecting...','warning');
            $.ajax({
                url: '$addToExperimentUrl',
                type: 'POST',
                dataType: 'json',
                data:{
                    experimentDbId: selectedExperiment
                },
                success: function(response) {
                    if(response.success != true){
                        showMessage('There was a problem while adding entries. Please try again later or file a report for assistance.','error');
                    }
                },
                error: function(err) {
                }
            })
        } else {
            $('#invalid-experiment-select').show();
        }
    });

    // Render basic info modal
    $(document).on('click','#save-btn', function(e) {

        event.preventDefault();
        $('#confirm-save-modal').modal('hide');

        var totalCount = getWorkingListItemCount();
        if(totalCount > 0){
            $('#item-count-hidden').html('Total number of items in list: '+totalCount);
            $('#create-list-modal').modal('show');
            $('#create-list-modal').on('shown.bs.modal', function() {
                $('#list-name').trigger('focus');
            });
        }
        else{
            showMessage("Notice! Working list is empty!",'warning');
        }
    });

    // // Render basic info modal
    $(document).on('click','#save-working-list-items-btn', function(e) {
        event.preventDefault();
        $('#clear-list-confirm-modal').modal('hide');

        var totalCount = getWorkingListItemCount();
        if(totalCount > 0){
            $('#item-count-hidden').html('Total number of items in list: '+totalCount);
            $('#create-list-modal').modal('show');
            $('#create-list-modal').on('shown.bs.modal', function() {
                $('#list-name').trigger('focus');
            });
        }
        else{
            showMessage("Notice! Working list is empty!",'warning');
        }
    });

    // Returns to current page
    $(document).on('click','#return-btn', function(e) {

        event.preventDefault();
        $('#confirm-save-modal').modal('hide');

    });

    // retrieve search parameters and pass on to working list
    $(document).on('click','#open-list-btn', function(e) {

        event.preventDefault();

        $('#open-list-modal').modal('hide');

        $.ajax({
            url: '$getSeedListProviderUrl',
            type: 'POST',
            data:{
                listType:'package'
            },
            success: function(response) {
                $('#dataType-hidden').val('package');
                $('.open-list-modal-body').html(response);
                $('#open-list-modal').modal('show');
            },
            error: function() {
                
            }
        })
        
        $('#invalid-select').hide();

    });

    // Removes item/s from list
    $(document).on('click','#remove-list-btn', function(e) {
        
        e.stopPropagation();
        e.preventDefault();

        var idRemove = retrieveCheckedItemsWL();
        var limit = '$deleteLimit';
        var retainSelection = $('#retained-switch-id').is(':checked');

        $(".find-loading-progress").removeClass("hidden");

        if(idRemove != null && idRemove.length > 0){
            $.ajax({
                url: '$removeListUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    idRemove: JSON.stringify(idRemove)
                },
                success: function(response) {
                    $('#'+seedListBrowserGrdId).b4rCheckboxColumn("clearSelection");
                    
                    if (idRemove.length > limit){
                        Materialize.toast("<i class='material-icons green-text'>check</i>&nbsp; Deletion of working list items is successfully sent for background process ", 3000);
                        checkBackgroundProcessDelete();
                    }
                },
                complete: function(){
                    $.pjax.reload({
                        container: '#'+seedListBrowserGrdId+'-pjax',
                        replace: false
                    });
                    $.pjax.xhr = null;

                    if(!retainSelection){
                        $.pjax.reload({
                            // url:'$findUrl',
                            container: '#'+browserGrdId+'-pjax', 
                            replace:false
                        });
                    } 
                    
                    $(".find-loading-progress").removeClass("hidden").addClass("hidden");

                    if (idRemove.length < limit) {
                        showMessage('Successfully deleted the selected items!','success');
                        showMessage('Manually refresh the Search Results browser to reflect the changes.','info');
                    }
                }
            });
        }
        else{
            $(".find-loading-progress").removeClass("hidden").addClass("hidden");
            showMessage('No list items selected.','warning');
        }
    });

    // close confirmation modal
    $(document).on('click','#cancel-clear-list-btn',function(e){
        e.preventDefault();
        $('#clear-list-confirm-modal').modal('hide');
    });

    // render confirmation modal
    $(document).on('click','#confirm-clear-list',function(e){
        e.preventDefault();
        $('#clear-list-confirm-modal').modal('show');
    });

    // clear entire working list
    $(document).on('click','#clear-working-list',function(e){
        e.preventDefault();
        checkBackgroundProcessDelete();

        $('#clear-list-confirm-modal').modal('hide');
        $(".find-loading-progress").removeClass("hidden");
        var totalCount = getWorkingListItemCount();
        var limit = '$deleteLimit';
        
        removeItems = false;
        if(totalCount > 0){
            showMessage('Clearing working list in progress...','warning');
            $(".find-loading-progress").removeClass("hidden");
        
            $.ajax({
                type: 'POST',
                url: '$clearAllUrl',
                dataType: 'json',
                success: function(response) {
                    if(response.success){
                        removeItems = response.bgprocess

                        if(!response.bgprocess){
                            $.pjax.reload({
                                container: '#'+seedListBrowserGrdId+'-pjax',
                                replace: false
                            });
                            $.pjax.xhr = null;
                        }
                        else{
                            showMessage('Clearing working list passed to background process!','success');
                            checkBackgroundProcessDelete();
                        }
                    }
                    else{
                        showMessage('Error in clearing working list.','error');
                    }
                },
                complete: function(){
                    $(".find-loading-progress").addClass("hidden");

                    if(!removeItems){
                        showMessage('Clearing working list complete!','success');
                        showMessage('Manually refresh the Search Results browser to reflect the changes.','info');
                    }
                }
            });
        }
        else{
            $(".find-loading-progress").addClass("hidden");
            showMessage('Working list is empty.','warning');
        }
    });

    // Renders Toast message
    function showMessage(message,type){
        var output = '';
        var icon = '';

        if(type == 'success'){
            icon = "<i class='material-icons green-text'>check</i>&nbsp; ";
        }
        else if(type == 'error'){
            icon = "<i class='material-icons red-text'>clear</i>&nbsp; ";
        }
        else if(type == 'warning'){
            icon = "<i class='material-icons yellow-text'>warning</i>&nbsp; ";
        }
        else if(type == 'message'){
            icon = "<i class='material-icons white-text'>info</i>&nbsp; ";
        }
        output = '<span class="left-align" style="text-align:left;">'+icon.concat(message)+'</span>';
        $('.toast').css('display','none');
        Materialize.toast(output, 5000);
    }
    
    /**
        Changes the UI to checkbox or radio button
        @params boolean isMultipleSelected if multiple is selected value is true, otherwise, false
     */
    function changeSelection(isMultipleSelected) {
        if(isMultipleSelected){
            selectSwitchVal=true;
            switchToMultiple=true;
            $(".checkbox-row").removeClass("hidden");
            $(".radio-btn-row").addClass("hidden");
        
            // uncheck the header checkbox
            $('#seed-inventory-seedlotCode_check-all').prop('checked',false);
            $('#seed-inventory-seedlotCode_check-all').removeAttr('checked');
        }else{  // single select
            selectSwitchVal=false;
            switchToMultiple=false;
            // Make it false if the add to list button is already working
            $(".checkbox-row").addClass("hidden");
            $(".radio-btn-row").removeClass("hidden");
        }
        
        renderSelectedItems();
    }

    /**
        Changes sets autoSelect status
        @params boolean isAutoSelected if auto selection value is true or false
    */
    function autoSelectItems(isAutoSelected) {
        
        if(isAutoSelected){
            autoSelectSwitchVal=true;
        }else{ 
            autoSelectSwitchVal=false;
        }
    }

    /**
     * check progress and status of background delete job
    */
    function checkBackgroundProcessDelete() {

        var url = '$checkBgJobUrl';
        $.ajax({
            type: 'POST',
            url: url,
            data: {method: "DELETE"},
            dataType: "json",
            success: function(response){
                var count = response.count;
                var status = response.status;
                var message = response.message;
    
                if (status != 'waiting') {
                    if (status == 'FAILED') {
                        showMessage(message,'error');
                        $('#bg-delete-notif').hide();
                        $('#'+seedListBrowserGrdId).removeClass("blur-div");
                    } else if (status == 'DONE') {
                        showMessage(message,'success');
                        $('#bg-delete-notif').hide();
                        $('#'+seedListBrowserGrdId).removeClass("blur-div");

                        $.pjax.reload({
                            container: '#'+seedListBrowserGrdId+'-pjax', 
                            replace:false,
                        });
                        
                        $.pjax.xhr = null;
                        removeItems = false;

                        showMessage('Manually refresh the Search Results browser to reflect the changes.','info');
                    } else if(status == 'IN_PROGRESS'){
                        $('#bg-delete-notif-msg').html("<i class='fa fa-info'></i> " + message);
                        $('#bg-delete-notif').show();

                        $('#'+seedListBrowserGrdId).addClass("blur-div"); 
                    }
                    else {
                        $('#bg-delete-notif-msg').html("");
                        $('#bg-delete-notif').hide();

                        $('#'+seedListBrowserGrdId).removeClass("blur-div"); 
                    }
                }
            }
        });
    }

    /**
     * check background process of add item backgroupd job
     */
    function checkBackgroundProcessStatus(){

        var url = '$checkBgJobUrl';
        var method = "POST";
        
        $.ajax({
            type: 'POST',
            url: url,
            data: {method: method},
            dataType: "json",
            success: function(response){
                var count = response.count;
                var status = response.status;
                var message = response.message;
                updateWorkingListItemCount();

                updateWorkingListItemCount();

                if (status != 'waiting') {
                    if (status == 'FAILED') {
                        $('#bg-progress-notif-msg').html("<i class='fa fa-exclamation-circle'></i> " + message);
                        $('#bg-progress-notif').removeClass("hidden").addClass('active');
                        $('#bg-progress-notif').removeClass("info").addClass('error');
                        $('#bg-progress-notif').show();

                        updateWorkingListItemCount();
                    } 
                    else if (status == 'DONE') {
                        $('#bg-progress-notif-msg').html("<i class='fa fa-check-circle'></i> " + message);
                        $('#bg-progress-notif').removeClass("hidden").addClass('active');
                        $('#bg-progress-notif').removeClass("info").addClass('success');
                        $('#bg-progress-notif').show();
                        
                        updateWorkingListItemCount();

                        // $.pjax.reload({
                        //     container: '#'+browserGrdId+'-pjax',
                        //     replace: false
                        // });
                        // $.pjax.xhr = null;
                        
                        if(savedWorkingList==true){
                            $('#clear-working-list').trigger('click');
                        }

                        if(addedItems==true){
                            $("#add-to-list-btn").attr('disabled',false);
                            $("#add-all-to-list-btn").attr('disabled',false);

                            $.pjax.reload({
                                container: '#'+browserGrdId+'-pjax', 
                                replace:false,
                            });
                            
                            $.pjax.xhr = null;

                            addedItems = false;
                        }

                        if(message != undefined && message.includes('Retrieving single-package record result ids')){
                            getIdsInBgProcess = false;
                            getSelectedItemsCount();
                        }
                        
                    } 
                    else if(status == 'waiting' && count == 0){ // default waiting state

                        $('#bg-progress-notif-msg').html("");
                        $('#bg-progress-notif').removeClass("success");
                        $('#bg-progress-notif').removeClass("error");
                        $('#bg-progress-notif').removeClass('active').addClass('hidden');
                        $('#bg-progress-notif').hide();

                        updateWorkingListItemCount();
                    }
                    else {

                        $('#bg-progress-notif-msg').html("<i class='fa fa-info'></i> " + message);
                        $('#bg-progress-notif').removeClass("success");
                        $('#bg-progress-notif').removeClass("error");
                        $('#bg-progress-notif').removeClass("hidden").addClass('active');
                        $('#bg-progress-notif').show();
                        
                        if(savedWorkingList==true){
                            $('#'+seedListBrowserGrdId).addClass("blur-div"); 
                        }

                        if(addedItems==true){
                            $("#add-to-list-btn").attr('disabled',true);
                            $("#add-all-to-list-btn").attr('disabled',true);
                        }

                        updateWorkingListItemCount();
                    }
                }
                else{
                    if(message != undefined && message.includes('Retrieving single-package record result ids')){
                        getIdsInBgProcess = true;
                    }
                    
                    updateWorkingListItemCount();
                    
                    $('#bg-progress-notif').removeClass('active').addClass('hidden');
                    $('#bg-progress-notif').hide();
                }

                checkBackgroundProcessDelete()
            }
        });

    }

    // save selected items to session
    function storeSelectionToSession(itemId,source, sessionName='selectedItems'){
        var index =-1;
        var selectedItems = getSessionUserSelection(sessionName);
        
        if(selectedItems == null){
            selectedItems = [];
        }
        if(itemId != null && itemId != ""){

            if(Array.isArray(itemId) == true){
                if(source == 'select'){
                    $.each(itemId,function(i,e){
                        if(selectedItems.indexOf(e) === -1){ selectedItems.push(e); }
                    });
                }
                if(source == 'deselect' && selectedItems != []){
                    $.each(itemId,function(i,e){
                        if(selectedItems.indexOf(e) !== -1){ selectedItems.splice(index, 1); }
                    });
                }
            }else{
                parts = itemId.split('-');
                id = parts[parts.length-1];
                index = selectedItems.indexOf(id);
                if(index == -1 && source == 'select'){ selectedItems.push(id); }
                if(index !== -1 && source == 'deselect'){ selectedItems.splice(index, 1); }
            }            
        } else {
            selectedItems = [];
        }
        
        if(selectedItems != null && selectedItems != []){
            $.ajax({
                type: 'POST',
                url: '$saveSelectedItemsUrl',
                async:false,
                dataType: 'JSON',
                data: {
                    selectedItems:selectedItems, sessionName: sessionName
                },
                success: function(response) {
                }
            });
        }

        if (sessionName == 'selectedItems') {
            setTimeout(function(){
                if (selectedItems == null) {
                    selectedItems = [];
                }
                if (selectedItems.length != 0) {
                    
                    $('#selected-items-count').html(selectedItems.length);
                    $('#selected-items-text').html('selected items.');
                } else {
                    $('#selected-items-count').html('');
                    $('#selected-items-text').html('');
                }
            }, 100);
        }
    }

    function emptySessionUserSelection(){
        var empty = null;

        var checkedItems = retrieveCheckedItems();
        var selectedRadio = retrieveRadioItems();

        if(checkedItems != []){
            $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                if($(this).prop("checked") === true)  {
                    $(this).prop("checked",false);
                    $(this).checked = false;
                    $(this).removeClass('isClicked');
                }
            });
        }

        if(selectedRadio != []){
            $(".seed-inventory-radio-btn").each(function(){
                if($(this).prop("checked") === true)  {
                    $(this).checked = false;
                    $(this).attr("checked",false);
                    $(this).prop("checked",false);
                    $(this).removeClass('isClicked');
                }
            });
        }

        $.ajax({
            type: 'POST',
            url: '$emptySelectedItemsUrl',
            dataType: 'json',
            async:false,
            success: function(response) {
            }
        });
    }
        

    // retrieve session user selection
    function getSessionUserSelection(sessionName = 'selectedItems'){

        if (sessionName == 'selectedItems') {
            var userSelection = getSelectionFS();
            return userSelection;
        } else {
            var userSelection;
            $.ajax({
                type: 'POST',
                url: '$getSelectedItemsUrl',
                dataType:'JSON',
                data: {
                    sessionName: sessionName
                },
                async:false,
                success: function(response) {
                    userSelection = response;
                }
            });
            return userSelection;
        }
    }

    // update checked status of selected items in browser
    function renderSelectedItems(){
        var selectedItems = getSessionUserSelection();
        currentlySelectedItems = selectedItems;

        if(selectedItems != null && selectedItems != []){
            var resultsArray = [];

            if($('#selectOption-switch-id').is(':checked')){

                $("input:checkbox.seed-inventory-seedlotCode_checkbox").each(function(){
                    this_row = $(this);
                    idNumber = this_row.val();
                    id = this_row.id;
                    
                    if(selectedItems.indexOf(idNumber) !== -1){
                        this_row.checked = true;
                        this_row.attr('checked',true);
                        this_row.prop('checked',true);
                        this_row.parent("td").parent("tr").addClass("grey lighten-4");
                        this_row.parent("td").parent("tr").addClass("success1");
                    }
                });
            }
            else{
                $(".seed-inventory-radio-btn").each(function(){
                    id = "seed-radio-id-"+$(this).val();
                    if(selectedItems.indexOf($(this).val()) !== -1){
                        $("#"+id).prop("checked",true).trigger('change');
                        $("#"+id).addClass('isClicked');
                        $("#"+id).parent("td").parent("tr").addClass("grey lighten-4");
                        $("#"+id).parent("td").parent("tr").addClass("success1");
                    }
                });
            }
        }
    }

    function disableSelectionRows(){
        $.each($(".disabled-row"), function() {
            $(this).parent("td").parent("tr").addClass("grey lighten-1");
        });
    }

    // grey out selected radio button row
    $(document).on('click','.seed-inventory-radio-btn',function(e){
        var row = $(this).parent("td").parent("tr");
        if(row.hasClass("grey")){
            $(this).parent("td").parent("tr").removeClass("grey lighten-4");
            $(this).parent("td").parent("tr").removeClass("success1");
        }
        else{
            $(this).parent("td").parent("tr").addClass("grey lighten-4");
            $(this).parent("td").parent("tr").addClass("success1");
        }
    });

    // Retrieves basic info and list items
    $(document).on('click','#create-list-btn',function(e){

        e.stopPropagation();
        e.preventDefault();

        var abbrev = {'abbrev':$('#list-abbrev').val()};

        var listInfo = $('#create-list-form').serializeArray().map(function(elem){
            return {[elem.name.split("[")[1]]:elem.value};
        });

        var hasValidAbbrev = true;
        var transactionId = '';
        $('.save-list-loading-progress').removeClass('hidden');

        if(listInfo[1]['name]'] != '' && listInfo[3]['display_name]'] != ''){
            $.ajax({
                type: 'POST',
                url: '$validateAbbrevUrl',
                data: abbrev,
                dataType: 'json',
                success: function(response) {
                    if (response.isValid == false) {

                        if (response.attr == 'name') {
                            $('#list-name').focus();
                        } else  if (response.attr == 'abbrev'){
                            $('#list-abbrev').focus();
                        } else {
                            $('#list-aname').focus();
                            $('#list-abbrev').focus();
                        }

                        hasValidAbbrev = false;

                        showMessage(response.message,'error');
                        $('.save-list-loading-progress').addClass('hidden');

                    }
                    else{

                        transactionId = 1;
                        createNewListFromWorkingList(listInfo);
                    }
                }
            });
        }
        else{
            hasValidAbbrev = false;
            showMessage("Name and Display Name of list is required!",'error');
        }
    });

    // create new list
    function createNewListFromWorkingList(listInfo) {
        $.ajax({
            type: 'POST',
            url: '$saveWorkingListUrl',
            data: {
                listInfo
            },
            dataType: 'json',
            success: function(response) {
                if(response.success == true){
                    $('#create-list-modal').modal('hide');
                    $('.save-list-loading-progress').addClass('hidden');
                    $(".find-loading-progress").addClass("hidden");
                    showMessage(response.message,'success');

                    if(response.bgprocess){
                        Materialize.toast("<i class='material-icons green-text'>check</i>&nbsp; Updating of working list items is in progress.", 3000);
                        $("#add-to-list-btn").attr("disabled",true);
                        $("#add-all-to-list-btn").attr("disabled",true);
                        $('#'+seedListBrowserGrdId).addClass("blur-div");
                    }
                    else{
                        $('#save-confirm-modal').modal('show');
                        $('#list-name, #list-abbrev, #list-display-name, #entry-list-description, #entry-list-remarks').val('');

                        $.pjax.reload({
                            container: '#'+seedListBrowserGrdId+'-pjax',
                            replace: false
                        });
                        $.pjax.xhr = null;
                    }
                }
                else{
                    showMessage(response.message,'error');
                    $('.save-list-loading-progress').addClass('hidden');
                }
            }
        });

        return true;
    }

    // updates the working list to final list
    function updateWorkingList(listInfo) {
        $.ajax({
            type: 'POST',
            url: '$updateWorkingListUrl',
            data: {
                listInfo
            },
            dataType: 'json',
            success: function(response) {
                if(response.success == true){
                    $('#create-list-modal').modal('hide');
                    $('.save-list-loading-progress').addClass('hidden');
                    $(".find-loading-progress").addClass("hidden");
                    showMessage(response.message,'success');
                    
                    $('#save-confirm-modal').modal('show');
                    $('#list-name, #list-abbrev, #list-display-name, #entry-list-description, #entry-list-remarks').val('');

                    $.pjax.reload({
                        container: '#'+seedListBrowserGrdId+'-pjax',
                        replace: false
                    });
                    $.pjax.xhr = null;
                }
                else{
                    showMessage(response.message,'error');
                    $('.save-list-loading-progress').addClass('hidden');
                }
            }
        });

        return false;
    }

    // Passes basic list info to be saved in database
    function createNewList(listInfo){
        $('.save-list-loading-progress').removeClass('hidden');
        $.ajax({
            type: 'POST',
            url: '$createListUrl',
            data: {
                listInfo
            },
            dataType: 'json',
            success: function(response) {
                if(response.success == true){
                    $('#create-list-modal').modal('hide');
                    $('.save-list-loading-progress').addClass('hidden');
                    // saveItemsToList(response.listId,response.listName);
                }
                else{
                    showMessage(response.message,'error');
                }
            }
        });

        return false;
    }

    // Passes list items to be saved in database
    function saveItemsToList(listId,listName){
        var success = false;
        $(".find-loading-progress").removeClass("hidden");
        showMessage('Saving working list items in progress...','warning');

        var totalCount = getWorkingListItemCount();
        var limit = '$insertLimit';
        
        if (totalCount < limit) {
            showMessage('Clearing working list in progress...','warning');
        }

        if(totalCount > 0){
            $.ajax({
                type: 'POST',
                url: '$saveListItemsUrl',
                data: {
                    listId,
                    listName
                },
                dataType: 'json',
                success: function(response) {
                    if(response.success != false){
                        success = true;
                        savedWorkingList = response.success;
                        if(totalCount < limit){
                            $(".find-loading-progress").addClass("hidden");
                            showMessage(response.message,'success');
                            
                            $('#save-confirm-modal').modal('show');
                            $('#list-name, #list-abbrev, #list-display-name, #entry-list-description, #entry-list-remarks').val('');
                        
                            if(success == true){
                                $('#clear-working-list').trigger('click');
                            }
                        }
                        else{
                            Materialize.toast("<i class='material-icons green-text'>check</i>&nbsp; Saving of working list items is successfully sent for background process ", 3000);
                            checkBackgroundProcessStatus();
                        }
                    }
                    else{
                        if(response.message != ""){ showMessage(response.message,'warning'); }
                    }
                },
                complete: function(){
                    $('.save-list-loading-progress').addClass('hidden');
                }
            });
        }
        else{
            showMessage('Working list is empty.','warning');
        }
        return false;
    }

    // Reload page after saving
    $('#save-confirm-modal').on('hidden.bs.modal', function () {
        // $.pjax.reload({
        //     container: '#'+seedListBrowserGrdId+'-pjax'
        // });
    });

    //when view more information is clicked
    $(document).on('click', '.view-more-info', function(e) {
        var obj = $(this);
        var entity_label = obj.data('entity_label');
        var entity_id = obj.data('entity_id');
        var entity_type = obj.data('entity_type');

        $('#entity-label').html('<i class="fa fa-info-circle"></i> ' + entity_label + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $('.view-entity-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        
        //load content of view more information
        setTimeout(function(){ 
            $.ajax({
            url: '$viewEntityUrl',
            data: {
                entity_id: entity_id,
                entity_type: entity_type,
                entity_label: entity_label
            },
            type: 'POST',
            async: true,
            success: function(data) {
                $('.view-entity-modal-body').html(data);          
            },
            error: function(){
                $('.view-entity-modal-body').html('<i>There was a problem while loading record information.</i>'); 
            }
        });
        }, 300);
    });

    $(document).on('click', '#li-3', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-entity-modal').modal('show').find('#view-entity-3').load($(this).attr('value')); //load view studies

        $('#li-2').removeClass('active');
        $('#li-1').removeClass('active');
        $('#li-3').addClass('active');
        $('#li-4').removeClass('active');
        $('#li-5').removeClass('active');
        $('#li-6').removeClass('active');
        $('#li-7').removeClass('active');
        $('#li-8').removeClass('active');

        $('#view-entity-3').removeClass('hidden').addClass('active');
        $('#view-entity-1').removeClass('active').addClass('hidden');
        $('#view-entity-2').removeClass('active').addClass('hidden');
        $('#view-entity-4').removeClass('active').addClass('hidden');
        $('#view-entity-5').removeClass('active').addClass('hidden');
        $('#view-entity-6').removeClass('active').addClass('hidden');
        $('#view-entity-7').removeClass('active').addClass('hidden');
        $('#view-entity-8').removeClass('active').addClass('hidden');
    });
    $(document).on('click', '#li-4', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-entity-modal').modal('show').find('#view-entity-4').load($(this).attr('value')); //load view seed lots

        $('#li-2').removeClass('active');
        $('#li-1').removeClass('active');
        $('#li-4').addClass('active');
        $('#li-3').removeClass('active');
        $('#li-5').removeClass('active');
        $('#li-6').removeClass('active');
        $('#li-7').removeClass('active');
        $('#li-8').removeClass('active');

        $('#view-entity-4').removeClass('hidden').addClass('active');
        $('#view-entity-1').removeClass('active').addClass('hidden');
        $('#view-entity-2').removeClass('active').addClass('hidden');
        $('#view-entity-3').removeClass('active').addClass('hidden');
        $('#view-entity-5').removeClass('active').addClass('hidden');
        $('#view-entity-6').removeClass('active').addClass('hidden');
        $('#view-entity-7').removeClass('active').addClass('hidden');
        $('#view-entity-8').removeClass('active').addClass('hidden');
    });
    $(document).on('click', '#li-8', function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#view-entity-modal').modal('show').find('#view-entity-8').load($(this).attr('value')); //load view passport data

        $('#li-2').removeClass('active');
        $('#li-1').removeClass('active');
        $('#li-6').removeClass('active');
        $('#li-3').removeClass('active');
        $('#li-4').removeClass('active');
        $('#li-5').removeClass('active');
        $('#li-7').removeClass('active');
        $('#li-8').addClass('active');

        $('#view-entity-8').removeClass('hidden').addClass('active');
        $('#view-entity-7').removeClass('active').addClass('hidden');
        $('#view-entity-6').removeClass('active').addClass('hidden');
        $('#view-entity-1').removeClass('active').addClass('hidden');
        $('#view-entity-2').removeClass('active').addClass('hidden');
        $('#view-entity-3').removeClass('active').addClass('hidden');
        $('#view-entity-4').removeClass('active').addClass('hidden');
        $('#view-entity-5').removeClass('active').addClass('hidden');
    });
    //export data
    $(document).on('click', '.export-data', function(e) {
        var id = this.id;
        var obj = $(this);
        var attr = obj.data('attr');
        var label = obj.data('label');
        window.location = '$exportEntityUrl?entityId='+id+'&attr='+attr+'&label='+label;
        $(window).on('beforeunload', function(){
            $('#loading').html('');
            $('#system-loading-indicator').css('display','none');
        });
    });
     //print pedigree tree
        $(document).on('click', '#print-tree', function(e) {
            var popUpAndPrint = function()
            {
                var pedigreeTree = $('#graph');
                var height = $(window).height() - 100;
                var width = $(window).width() - 100;
                var app_name = '$appName';
                
                var printWindow = window.open('', 'PrintMap',
                'width=' + width + ',height=' + height);
                printWindow.document.writeln('<style type="text/css" media="print"> @media print{ @page {size: portrait;} .card-panel.pedigree-tree{font-family: monospace;} .pull-right{float:right;}}</style>');
                printWindow.document.writeln('<p style="font-size:20px;">Pedigree tree<br/><span style="font-size:12px;font-weight:300">Generated from ' + app_name + '</span></p>');
                printWindow.document.writeln($(pedigreeTree).html());

                printWindow.document.close();
                printWindow.print();
                printWindow.close();
            };
            setTimeout(popUpAndPrint, 100);
        });

        //print passport data
        $(document).on('click', '#print-passport', function(e) {
            var popUpAndPrint = function()
            {
                var passport = $('#passport');
                var height = $(window).height() - 100;
                var width = $(window).width() - 100;

                var printWindow = window.open('', 'PrintMap',
                'width=' + width + ',height=' + height);
                printWindow.document.writeln('<style type="text/css" media="print"> @media print{ @page {size: portrait;} #passport-header{display:block;font-size:23px;text-align:center;} .table{width: 100%;} th{text-align:left;}}</style>');
                printWindow.document.writeln('');
                printWindow.document.writeln($(passport).html());

                printWindow.document.close();
                printWindow.print();
                printWindow.close();
            };
            setTimeout(popUpAndPrint, 100);
        });

        // retrieve search parameters and pass on to find()
    $(document).on('click','#cancel-load-btn', function(e) {

        event.preventDefault();
        $('#open-list-modal').modal('hide');

    });

    // retrieve search parameters and pass on to find()
    $(document).on('click','#load-list-btn', function(e) {

        e.stopPropagation();
        e.preventDefault();

        var limit = '$insertLimit';
        var selectedList = null;
        $(".selected-list-select-radio").each(function(){
            if($(this).prop("checked") === true)  {
                selectedList = $(this).attr("id");
            }
        });
        if(selectedList != null){
            
            var dataType = $('#dataType-hidden').val();
            if(dataType == 'germplasm'){
                if(selectedList != null){
                    selectedList = parseInt(selectedList.split('-')[3]);
                    $(".find-loading-progress").removeClass("hidden");
                    showMessage('Selected saved list is currently being retrieved...','warning');
                    $.ajax({
                        url: '$getListByIdUrl',
                        type: 'POST',
                        dataType: 'json',
                        data:{
                            id: selectedList
                        },
                        success: function(response) {
                            
                            if(response.success == true){
                                $('#field-type-selection').val('name').trigger('change');
                                $('#field-type-selection-2').val('name').trigger('change');
                                $('#input-field-hidden').val('name').trigger('change');

                                var inputValues = response.value;
                                var newValues = (inputValues.replace(/"/g, '')).replace(/,/g, '\\n');
                                $('#input-area').val(newValues).trigger('change');
                            }
                            else{
                                showMessage(response.error,'error');
                            }
                        },
                        error: function() {
                            
                        },
                        complete: function(){
                            $(".find-loading-progress").addClass("hidden");
                            showMessage('Completed retrieving saved list items','success');
                        }
                    })
                }
            }

            if (dataType == 'package'){
                if(selectedList != null){
                    selectedList = parseInt(selectedList.split('-')[3]);
                    $(".find-loading-progress").removeClass("hidden");
                    showMessage('Selected saved list is currently being retrieved...','message');
                    addedItems = true
                    $.ajax({
                        url: '$getSeedListByIdUrl',
                        type: 'POST',
                        dataType: 'json',
                        data:{
                            id: selectedList
                        },
                        success: function(response) {
                            if(response.success != false){
                                if(!response.bgprocess){
                                    $.pjax.reload({
                                        container: '#'+seedListBrowserGrdId+'-pjax',
                                        replace:false
                                    });

                                    if (response.duplicateItems !== '' && response.duplicateItems !== null && (typeof response.duplicateItems !== 'undefined')) {
                                            var exist = response.duplicateItems;
                                            Materialize.toast("<i class='material-icons orange-text'>warning</i>&nbsp; The following seedlot code is/are already in the working list:  " + exist, 5000, );
                                    }
                                    showMessage('Completed retrieving saved list items.','success');
                                }
                                else{
                                    Materialize.toast("<i class='material-icons green-text'>check</i>&nbsp; Addition of working list items is successfully sent for background process ", 3000);
                                    checkBackgroundProcessStatus();
                                }
                            }
                            else{
                                showMessage('Error in retrieving saved seed list','error');
                            }
                        },
                        error: function() {
                            showMessage('Error in retrieving saved seed list','error');
                        },
                        complete: function(){
                            $(".find-loading-progress").addClass("hidden");
                        }
                    })
                }
            }

            $('#open-list-modal').modal('hide');
        }
        else{
            $('#invalid-select').show();
        }

    });

    $(document).on('click', '#export-working-list-link-btn', function exportWorkingListLink (e) {
        message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Exporting all data in the working list. Please wait.'
        $('.toast').css('display','none')
        Materialize.toast(message, 5000)

        $('#system-loading-indicator').css('display','none')
    })

    /**
     * Show flash notification upon adding items to Working List
     * 
     */
    function showFlashMessage(totalCount){
        message = "  <strong>" + totalCount + "</strong> of selected items have been added to the Working List. You can view the items in the <a id='working-list-btn'><u style ='cursor:pointer'> Working List</u></a> page.";
        $('#bg-progress-notif-msg').html("<i class='fa fa-check-circle'></i> " + message);
        $('#bg-progress-notif').removeClass("hidden").addClass('active');
        $('#bg-progress-notif').removeClass("info").addClass('success');
        $('#bg-progress-notif').show();
        showMessage('Successfully added items!','success');
    } 
JS;

$this->registerJs($script);
$this->registerJsFile(
    '@web/js/find-seeds/selection.js',[
        'depends' => ['app\assets\AppAsset'],
        'position'=>\yii\web\View::POS_END
    ]
);

$this->registerCssFile(
    "@web/css/browser/notifications.css", 
    [
        'depends' => [\macgyer\yii2materializecss\assets\MaterializeAsset::className()],
        'position' => \yii\web\View::POS_HEAD
    ], 
    'css-browser-notifications'
);

// Sets placement of Toast on page
$this->registerCss('
        #toast-container {
            top: auto !important;
            right: auto !important;
            bottom: 85%;
            left:55%;
            z-index: 10000;
        }
        .product_group1{
            background-color:#e0e0e0
        }
        .product_group2{
            background-color:#e1f5fe
        }
        .includedSeedlot{
            background-color:#a5d6a7;
            // color: #616161;
            // font-style: italic;   
        }
        [type="radio"]:not(:checked), [type="radio"]:checked {
            pointer-events: auto;
        }
        .switch label .lever {
           margin: 0 5px;
        }
        .dropdown-content {
           overflow-y: visible;
        }
        .dropdown-content .dropdown-content {
           margin-left: -100%;
        }

        .blur-div{
            -webkit-filter: blur(5px);
            -moz-filter: blur(5px);
            -o-filter: blur(5px);
            -ms-filter: blur(5px);
            filter: blur(2px);
            background-color: #ccc;
            pointer-events: none;
        }

        .info > .card-panel {
            background: #d9edf7;
            color: #31708f;
        }
        
        .success > .card-panel {
            background: #dcedc8;
            color: #689f38;
        }

        .error > .card-panel {
            background: #ffcdd2;
            color: #F44336;
        }

        .warning > .card-panel {
            background: #fff9c4;
            color: #827717;
        }
    ');

?>
