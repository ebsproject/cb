<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;

//modal for viewing designation information
Modal::begin([
    'id' => 'view-entity-modal',
    'header' => '<h4 id="entity-label"></h4>',
    'footer' =>
        Html::a(\Yii::t('app','Ok'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;',
        'closeButton' => [
            'class' => 'hidden'
        ],
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static'],
    ]);
    echo '<div class="view-entity-modal-body"></div>';
Modal::end();

// create list modal
Modal::begin([
    'id' => 'create-list-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">add</i>&nbsp;'.Yii::t('app','Create List').'</h4></div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Create'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close',
                'id' => 'create-list-btn',
                'title' => 'Create new list'
            ]) . '&emsp;',
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999;'],
]);

echo '<div class="create-list-modal-body">' .
    Yii::$app->controller->renderPartial('/seed-list/create_list', [
        'name' => '',
        'abbrev' => '',
        'display_name' => '',
        'type' => '',
        'description' => '',
        'remarks' => '',
        'id' => null,
        'header' => '',
        'showTabs' => false,
        'create' => true,
    ]);
Modal::end();

// add to experiment modal
Modal::begin([
    'id' => 'add-to-experiment-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">add</i>&nbsp;'.Yii::t('app','Select Experiment').'</h4></div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Cancel'),'#!',
        [
            'id' => 'cancel-open-add-to-experiment-modal-btn',
        ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Add to Selected Experiment'),'#!',
            [
                'class' => 'btn btn-secondary waves-effect waves-light modal-close',
                'id' => 'add-to-experiment-btn',
                'title' => 'Add items to selected experiment'
            ]) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="add-to-experiment-modal-body" id="add-to-experiment-modal-body"></div>';
Modal::end();

// save confirmation modal
Modal::begin([
    'id' => 'confirm-save-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">check_circle</i>&nbsp;Seed Search List Confirmation</h4></div>
            <div class="col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        ',
    'footer' =>
        Html::a(\Yii::t('app', 'Cancel'),['#'],
            [
                'class' => 'modal-close text-align:center',
                'id' => 'return-btn',
                'title' => 'Go back to search',
                'data-dismiss'=>'modal'
            ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Confirm'),'#!',
            [
                'class' => 'btn btn-primary waves-effect waves-light modal-close text-align:center',
                'id' => 'save-btn',
                'title' => 'Continue to save list',
                'style' => 'margin-right:5px;'
            ]),
    'closeButton' => [
        'class' => 'hidden',
    ],
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="create-list-modal-body">
            <span>
                Do you want to save your seed search list? Saving your list will clear the current list on the browser.
            </span>
        </div>';
Modal::end();

// save notification modal
Modal::begin([
    'id' => 'save-confirm-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">check_circle</i>&nbsp;Seed Search List Confirmation</h4></div>
        </div>
        ',
    'footer' => Html::a(\Yii::t('app', 'Close'),['#'],
                [
                    'class' => 'btn btn-primary waves-effect waves-light modal-close text-align:center',
                    'id' => 'close-modal-btn',
                    'title' => 'Close',
                    'data-dismiss'=>'modal'
                ]),
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="save-confirm-modal-body">
            <span><h6>
                Successfully saved your list. You can update your list in the List Management Tool.
            </h6></span>
        </div>';
Modal::end();

// open list modal
Modal::begin([
    'id' => 'open-list-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">playlist_add</i>&nbsp;Open Saved List</h4></div>
        </div>
        ',
    'footer' => 
        Html::a(\Yii::t('app', 'Cancel'),'#!',
        [
            'id' => 'cancel-load-btn',
        ]) . '&emsp;'.
        Html::a(\Yii::t('app', 'Select'),'#!',
            [
                'class' => 'btn btn-secondary waves-effect waves-light modal-close',
                'id' => 'load-list-btn',
                'title' => 'Load items of selected list'
            ]) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="open-list-modal-body" id="open-list-modal-body"></div>';
Modal::end();

// clear list confirmation
Modal::begin([
    'id' => 'clear-list-confirm-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">clear</i>&nbsp;Clear Working List Confirmation</h4></div>
        </div>
        ',
    'footer' => 
        Html::a(\Yii::t('app', 'Cancel'),'#',
        [
            'id' => 'cancel-clear-list-btn',
        ]) . '&emsp;'.
        Html::a('Save',
        '#',
        [
            'id' => 'save-working-list-items-btn',
            'title' => 'Save current working list',
            'class' => 'btn btn-primary waves-effect waves-light',
            'style' => 'margin-right:5px;'
        ]) .
        Html::a('Clear',
        '#',
        [
            'id' => 'clear-working-list',
            'title' => 'Clear items from working list',
            'class' => 'btn btn-primary waves-effect waves-light',
            'style' => 'margin-right:5px;'
        ]),
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="clear-list-confirm-modal-body" id="confirm-clear-list-body">Do you want to clear the Working List? All items on the Working List will be removed if you proceed.</div>';
Modal::end();

// clear user selection confimation modal
Modal::begin([
    'id' => 'clear-selection-confirm-modal',
    'header' => '
        <div>
            <div class="col-md-6"><h4><i class="material-icons widget-icons">clear</i>&nbsp;Clear User Selection</h4></div>
        </div>
        ',
    'footer' => 
        Html::a(\Yii::t('app', 'Cancel'),'#',
        [
            'id' => 'cancel-clear-selection-btn',
        ]) . '&emsp;'.
        Html::a('Clear' . '<i class="material-icons right">clear</i>',
        '#',
        [
            'id' => 'clear-user-selection-btn',
            'title' => 'Clear selected items',
            'class' => 'btn btn-primary waves-effect waves-light'
        ]) . '&emsp;',
    'size' => 'modal-lg',
    'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
]);

echo '<div class="clear-selection-confirm-modal-body" id="confirm-clear-selection-body">Switching from multiple selection to single selection will clear your selected packages. Do you want to proceed?</div>';
Modal::end();

