<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin([
    'action' => ['find','program'=>$programAbbrev],
    'enableClientValidation' => false,
    'id' => 'find-seeds-filter',
    'options' => [
        'class' => 'find-seeds-filter',
    ],
]);

$packageLimitQr = 1;
$checked = '';
$disabled = 'disabled';
if (isset(Yii::$app->session['packageLimitQr'])) {
    $packageLimitQr = Yii::$app->session['packageLimitQr'];

    if ($packageLimitQr != 0) {
        $checked = 'checked=true';
        $disabled = '';
    }
}

?>
<div class="row variable-filters" id="variable-filters" style = "margin-bottom: 0px;">
    <?php
    
        echo Yii::$app->controller->renderPartial('/find-seeds-params/_variable_filters', [
            'id' => $id,
            'program' => $program,
            'filterSelection' => $filterSelection,
            'filterValues' => $filterValues,
            'filters' => $filters,
            'programAbbrev' => $programAbbrev,
            'inputListFields' => $inputListFields,
            'requiredFilters' => $requiredFilters,
            'germplasmListDataProvider' => $germplasmListDataProvider
            ]);
        ?>
</div>
<div class="row action-btns" id="action-btns" style="margin-bottom:5px;">
    <?=Html::submitButton(\Yii::t('app','Find'),[
        'class' => 'waves-effect waves-light btn light-green darken-3 pull-right',
        'style' => 'margin-right:0px;',
        'id' => 'find-btn',
        'disabled' => true,
        'data-pjax'=>true,
        'title' => Yii::t('app', 'Find Seeds'),
        ])?>
    <?= Html::a('<span>Reset</span>', '#!', 
        [
        'id' => 'reset-btn',
        'class' => 'btn light-green darken-3 pull-right',
        'style' => 'margin-right:5px;',
        'title' => 'Clear all search parameter',
        'data-pjax' => true
        ]) 
    ?>
    <?= Html::a('<i class="material-icons">playlist_add</i>', '', 
        [
        'id' => 'load-query-btn',
        'class' => 'btn light-green darken-3 pull-right',
        'style' => 'margin-right:5px;',
        'title' => 'Open saved search parameter'
        ]) 
    ?>
    <?= Html::a('<i class="material-icons">save</i>', '#!', 
        [
        'id' => 'save-query-btn',
        'class' => 'btn light-green darken-3 pull-right',
        'style' => 'margin-right:0px;',
        'title' => 'Save search parameter'
        ]) 
    ?>
    <?= Html::a(\Yii::t('app','Results Summary'), '#!', 
        [
        'id' => 'show-summary-btn',
        'class' => 'btn light-green darken-3 pull-left',
        'style' => 'margin-right:5px;',
        'title' => \Yii::t('app','Show search results summary information')
        ]) 
    ?>
    <div id="notif_field" class="red-text text-darken-2" style = "display:none; margin-top:10px; margin-left:5px;"></div>
    <div class="pull-right" style="margin-right:20px;">
        <input type="checkbox" <?php echo $checked;?> id="set-limit-group" class="filled-in" name="Form[set_limit_group]" style="opacity:0"><label for="set-limit-group">Set limit of packages per germplasm: &nbsp;</label>
        <input id="limit-packages" min="1" <?php echo $disabled;?> value=<?php echo $packageLimitQr;?> type="number" style="width: 50px;">
    </div>
</div>
<?php 

    ActiveForm::end();

    // save query parameters modal
    Modal::begin([
        'id' => 'query-info-modal',
        'header' => '
            <div>
                <div class="col-md-6"><h4><i class="material-icons widget-icons">playlist_add</i>&nbsp;Search Parameter Basic Info</h4></div>
            </div>
            ',
        'footer' => 
            Html::a(\Yii::t('app', 'Cancel'),'#',
            [
                'id' => 'cancel-save-params-btn',
            ]) . '&emsp;'.
            Html::a('Save' . '<i class="material-icons right">send</i>',
                '#',
                [
                    'id' => 'save-query-params-btn',
                    'title' => 'Save current search parameters',
                    'class' => 'btn btn-primary waves-effect waves-light'
                ]) . '&emsp;',
        'size' => 'modal-lg',
        'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
    ]);

    echo '<div class="view-query-modal-body" id="view-query-modal-body">' .
        Yii::$app->controller->renderPartial('/find-seeds-params/_save_query_params', [
            'name' => '',
            'abbrev' => '',
            'display_name' => '',
            'type' => '',
            'description' => '',
            'remarks' => '',
            'id' => null,
            'header' => '',
            'showTabs' => false,
            'create' => true,
        ]) . '</div>';
    Modal::end();

    // open query parameter list modal
    Modal::begin([
        'id' => 'open-query-params-modal',
        'header' => '
            <div>
                <div class="col-md-6"><h4><i class="material-icons widget-icons">view_list</i>&nbsp;View Saved Search Parameters</h4></div>
            </div>
            ',
        'footer' => 
            Html::a(\Yii::t('app', 'Cancel'),'#',
            [
                'id' => 'cancel-load-params-btn',
            ]) . '&emsp;'.
            Html::a(\Yii::t('app', 'Select'),'#!',
                [
                    'class' => 'btn btn-secondary waves-effect waves-light modal-close',
                    'id' => 'load-query-params-btn',
                    'title'=>'Load selected search parameters'
                ]) . '&emsp;',
        'size' => 'modal-lg',
        'options' => ['data-backdrop' => 'static', 'style' => 'z-index: 9999999;'],
    ]);

    echo '<div class="view-query-params-modal-body" id="view-query-modal-body"></div>';
    Modal::end();

    // for search input result
    Modal::begin([
        'id' => 'result-modal',
        'header' => '<h4><i class="glyphicon glyphicon-list-alt"></i>  Input List Search Results Summary</h4>',
        'footer' => Html::a(\Yii::t('app','Close'),['#'],['id'=>'close-btn','data-dismiss'=>'modal', 'class' => 'btn btn-primary teal waves-effect waves-light']).'&nbsp;&nbsp',
        'size' => 'modal-md',
        'options' => ['data-backdrop' => 'static'],
        'closeButton' => ['class' => 'hidden'],
    ]);
    echo '<div id="result-body"><span id="result-message"></span><br>'.
            '<textarea id="result-name" style="background-color: white margin:2px; height: 100px;" readonly></textarea>'.
         '</div>';
    Modal::end();
?>
<?php

    $updateFilterOptionsUrl = Url::to(['find-seeds/update-filter-values']);    
    $listFilterUrl = Url::to(['list', 'id' => $id, 'program' => $program]);
    $variableFilterUrl = Url::to(['variable', 'id' => $id, 'program' => $program]); 
    $saveQueryParamsUrl = Url::to(['save-query-params']);
    $getQueryParamsDataProviderUrl = Url::to(['get-query-params-data-provider']);
    $setQueryParamsUrl = Url::to(['get-param-values-by-id']);
    $getParamsByIdUrl = Url::to(['get-query-params-by-id']);
    $loadParamsToFormUrl = Url::to(['load-selected-params-to-filters']);   
    $resetQueryUrl = Url::to(['reset-query-param',"program"=>$programAbbrev]);
    $clearAllUrl = Url::to(['index']);
    $findUrl = Url::to(['find',"program"=>$programAbbrev]);
    $getSessionParam = Url::to(['get-session-param']);
    $validateQueryParamsUrl = Url::to(['validate-query-params']);
    $getRequiredFiltersUrl = Url::to(['get-required-filters']);
    $getSinglePackageRecordIdsUrl = Url::to(['get-single-package-record-ids']);
    $getSinglePackageRecordIdsBgProcessUrl = Url::to(['get-single-package-record-ids-bg-process']);
    $getSinglePackageRecordIdsCountUrl = Url::to(['get-single-package-record-ids-count']);
    $checkCurrSelectedIdsUrl = Url::to(['check-current-selected-ids']);
    $getResultsSummaryUrl = Url::to(['find-seeds/show-results-summary']);
    $resetSummaryUrl = Url::to(['reset-summary']);
    
    $fieldTypeValues = [];
    if(isset($inputListFields) && !empty($inputListFields)){
        foreach($inputListFields as $key=>$option){
            array_push($fieldTypeValues,strtolower($key));
        };
    }
    $inputListTypeData = isset($fieldTypeValues) && $fieldTypeValues != [] ? json_encode($fieldTypeValues) : "{}";

$script = <<<JS
    var searchBrowserId= '$searchBrowserId';
    var getIdsInBackground = false;

    // notification for missing working list
    $(document).ready(function(){
        if(tempTable == null && tempTable['data']==null && tempTable['data']==''){
            showMessage('Error in loading working list. List does not exist.','error');
        }
    });

    // update variable filter values on page reload
    $(document).on('change','.variable-filter',function(e){

        var data = $('.find-seeds-filter').serializeArray();
        varFilters = [];

        $.each(data,function(index,val){
                if(val['value'] !== "" && val['name'] != "_csrf" && val['name'] != "filter-selection-hidden" && val['name'] != "field-type-selection"){
                    varFilters.push(val);
                    hasFilter = true;
                } 
            })

        if(hasFilter == false){
            $('#find-btn').attr('disabled',true);
        }
        else{
            $('#find-btn').attr('disabled',false);
            hasFilter = false;
        }
    });

    // update filter option values
    function updateFilterValues(id, values){

        $.ajax({
            url: '$updateFilterOptionsUrl',
            type: 'post',
            data: {
                sourceFilter: sourceFilter,                
            },
            success: function(response){
            },
            error: function(){
            }
        })
    }

    // fetch filter option values
    function getFilters(){
        var filters = $(".find-seeds-filter").serializeArray();
        var id = $(this).attr("id");

        $.ajax({
            url: "'.$updateFilterOptionsUrl.'",
            type: "post",
            data: {
                sourceFilter: filters,
                id: id,          
            },
            success: function(response){
                return JSON.parse(response);
            },
            error: function(){
            }
        })
    }

    $(document).on('click', '#set-limit-group', function(e) {
        if ($(this).prop("checked") === true) {
            $("#limit-packages").removeAttr("disabled");
            $("#limit-packages").focus();
        } else {
            $("#limit-packages").attr("disabled", true);
        }
    });

    // hide load query params modal
    $(document).on('click','#cancel-load-params-btn', function(e) {
        event.preventDefault();
        $('#open-query-params-modal').modal('hide');
    });

    // hide save query params modal
    $(document).on('click','#cancel-save-params-btn', function(e) {
        event.preventDefault();
        $('#query-params-name').removeClass('has-error');
        $('.field-query-params-name').find(".help-block").html('');
        $('#query-info-modal').modal('hide');
    });

    // retrieve search parameters and pass on to find()
    $(document).on('click','#reset-btn', function(e) {
        event.preventDefault();
        resetSummary();
        resetFilters();
    });

    // Resets summary
    function resetSummary(){
        $.ajax({
            url: '$resetSummaryUrl',
            async: false,
            success: function(response){
                $('#result-message').html('');
                $('#result-name').addClass('hidden');
            },
            error: function(){
                notif = '<i class="material-icons red-text left">close</i> An error occurred while resetting results summary. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        })
    }

    function resetFilters(){
        $('.variable-filter').val("").change();
        
        $.ajax({
            url: '$resetQueryUrl',
            async: false,
            success: function(response){
                $('#filter-selection').val("").trigger('change');
                $('#filter-selection-hidden').val("").trigger('change');
                $('.select2-additional-filters-hidden').val("").trigger('change');
                $(".add-filters-loading-progress").addClass("hidden");

                var additionalFilterValues = retrieveAdditionalFilterValues();
                generateAdditionalFilters(additionalFilterValues,'all');
            },
            error: function(){
                showMessage('Error in clearing parameter and search values.','error');
            }
        })             
        $('#program-filter').val($program).trigger('change');
        
        $.pjax.reload({
            url: '$clearAllUrl',
            container: '#'+searchBrowserId+'-pjax', 
            replace:true
        });

        sessionStorage.removeItem('search-grid-table-con'+'_checkbox_storage');
        displayTotalCountFS(null);
        
        $('#input-list-filters').hide();
        $('#input-list-filters').addClass('hidden').trigger('change');

        $('#additional-params-panel').removeClass('col-md-6').addClass('col-md-12').trigger('change');
        $('#additional-params-panel').removeClass('hidden').trigger('change');
        $('#additional-params-panel').show();
    }

    // save current query parameters function
    $(document).on('click','#save-query-btn', function(e) {

        e.preventDefault();

        var data = $('.find-seeds-filter').serializeArray();
        var extractedFilters = getFilterValues(data);
        var filterType = 'filter';
        var queryParams = [];
        if(extractedFilters['cleanValues'] === true && extractedFilters['filter'] != null && extractedFilters['filter'].length > 0){
            
            $.each(extractedFilters['filter'],function(index,val){
                if(val.value != ""){
                    queryParams.push({'name':val.name,'value':val.value});
                } 
            })

            if(queryParams != null && queryParams.length > 0 && queryParams != []){
                showMessage('Extracting selected values...','message');

                $.ajax({
                    url: ' $setQueryParamsUrl',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        queryParams:queryParams,
                        type:filterType,
                    },
                    success: function(response) {
                        var displayValues = JSON.stringify(response.queryParams['values']).replace(/::/g, '; ');
                        var values = JSON.stringify(response.queryParams['ids']);
                        
                        $('#query-params-type').val(''+response.type).trigger('change');
                        $('#query-params-area').val(displayValues.replace(/"/g, '')).trigger('change');
                        $('#query-params-area').trigger('autoresize'); 
                        
                        $('#params-hidden').val(values).trigger('change');
                        $('#query-params-name').val('').trigger('change');
                        
                        $('#query-params-name').removeClass('has-error');
                        $('.field-query-params-name').find(".help-block").html('');

                        $('#query-info-modal').modal('show');
                    },
                    error: function(){
                        showMessage('<span>Error encountered in saving search parameters','error');
                    }
                })
            }
            else{
                showMessage('<span>No filters selected.','warning');
            }
            
        }
        else{
            showMessage('<span>No filters selected.','warning');
        }

    });

    //show summary result modal
    function showResultModal(){
        $('#result-modal').modal('show');
    }

    // load selected query parameters function
    $(document).on('click','#show-summary-btn', function(e) {
        $('#result-message').html('').trigger('change');
        $('#result-name').addClass('hidden');
        $.ajax({
            url: '$getResultsSummaryUrl',
            type: 'POST',
            dataType: 'json',
            success: function(response){
                var message = response['message'];
                $('#result-message').html('<span>'+message+'</span>').trigger('change');
                if(response['success']){
                    var result = response['result'];
                    $('#result-name').val(result);
                    $('#result-name').removeClass('hidden');
                    showResultModal();
                }
                showResultModal();
            },
            error: function(error){
                notif = '<i class="material-icons red-text left">close</i> An error occurred while showing summary. Please try again or file a report for assistance.';
                Materialize.toast(notif, 5000);
            }
        });
    });    

    // load selected query parameters function
    $(document).on('click','#load-query-btn', function(e) {
        
        e.preventDefault();
        $('#invalid-notif').hide();

        $.ajax({
            url: ' $getQueryParamsDataProviderUrl',
            type: 'POST',
            async:false,
            success: function(response) {
                $('.view-query-params-modal-body').html(response);
                setSessionParamIndicator();
            }
        })
        
        $('#open-query-params-modal').modal('show');

    });

    // retrive session param and select param in table
    function setSessionParamIndicator(){

        $.ajax({
            url: ' $getSessionParam',
            type: 'POST',
            success: function(response) {
                $('#params-item-'+response).prop("checked",true);
            }
        })
    }

    // save current query parameters information function
    $(document).on('click','#save-query-params-btn', function(e) {

        e.preventDefault();
        
        var queryInfo = $('#save-params-form').serializeArray();
        var hasEmptyVal = false;
        var name, type, data;

        $('#query-params-name').removeClass('has-error');
        $('.field-query-params-name').find(".help-block").html('');

        if (queryInfo[1]['value'] != "" && queryInfo[2]['value'] != "" && queryInfo[3]['value'] != ""){
            name = queryInfo[1]['value'];
            type = queryInfo[2]['value'];
            data = $('#params-hidden').val();
            
            validation = validateQueryParamsValues(name,type,data);
            
            if(validation == true){
                $('#query-params-name').addClass('has-error');
                $('.field-query-params-name').find(".help-block").html('Parameter information already exists.');
            }
            else{   
                $.ajax({
                    url: '$saveQueryParamsUrl',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        name:name,
                        type:type,
                        data:data,
                    },
                    success: function(response){
                        $('#query-params-name').removeClass('has-error');
                        $('.field-query-params-name').find(".help-block").html('');

                        showMessage('<span>'+response.queryParamName+' has been successfully saved.</span>','success');
                        $('#query-info-modal').modal('hide');
                    },
                })   
            }
        }
        else{
            if(queryInfo[1]['value'] == ""){
                $('#query-params-name').focus();
                $('#query-params-name').addClass('has-error');
                $('.field-query-params-name').find(".help-block").html('Name cannot be blank');
            }
            if(queryInfo[2]['value'] == ""){
                $('#query-params-type').focus();
                $('#query-params-type').addClass('has-error');
                $('.field-query-params-type').find(".help-block").html('Type cannot be blank');
            }
            if(queryInfo[3]['value'] == ""){
                $('#query-params-area').focus();
                $('#query-params-area').addClass('has-error');
                $('.field-query-params-area').find(".help-block").html('Data cannot be blank');
            }
        }
    });

    // validate query params input values
    function validateQueryParamsValues(name,type,values){

        invalid = false;

        $.ajax({
            url: '$validateQueryParamsUrl',
            type: 'POST',
            dataType: 'JSON',
            data:{
                name:name,
                type:type,
                data:values,
            },
            async:false,
            success: function(response){
                invalid = response;
            },
        });

        return invalid;
    }

    // load selected query parameters function
    $(document).on('click','#load-query-params-btn', function(e) {
        
        e.preventDefault();

        var selectedParams;
        
        $(".query-params-select-radio").each(function(){
            if($(this).prop("checked") === true)  {
                selectedParams = $(this).attr("id");
            }
        });

        if(selectedParams != null){
            $(".find-loading-progress").removeClass("hidden");

            selectedParams = parseInt(selectedParams.split('-')[2]);

            $.ajax({
                async: true,
                url: '$getParamsByIdUrl',
                type: 'POST',
                dataType: 'json',
                data:{
                    id: selectedParams
                },
                success: function(response) {
                    $('#open-query-params-modal').modal('hide');
                    
                    var selectedFilters = [];
                    var selectedQueryParams = [];
                    var key,value,val;
                    
                    var queryParams = JSON.parse(response.query); 
                    var nonIntegerValueType = ['VOLUME','UNIT','EXPERIMENT_TYPE','HARVEST_DATE','PLOT_CODE','PLOTNO','ENTNO','ENTCODE'];

                    $.each(queryParams,function(i,v){
                        if(v != null){
                            key = Object.keys(v)[0];
                            val = Object.values(v)[0];

                            if(val.includes(',') && nonIntegerValueType.indexOf(key) == -1 && inputListTypes.indexOf(key.toLowerCase(),0) == -1){
                                arrayValue = val.split(',');
                                value = arrayValue.map(Number);
                            }
                            if(!val.includes(',') && nonIntegerValueType.indexOf(key) == -1 && inputListTypes.indexOf(key.toLowerCase(),0) == -1) {
                                value = [parseInt(val)];
                            }
                            if(nonIntegerValueType.indexOf(key) !== -1 && inputListTypes.indexOf(key.toLowerCase(),0) == -1){
                                if(!val.includes(',')){
                                    value = val;
                                }else{ 
                                    value = val.split(',');
                                }
                            }
                            if(inputListTypes.indexOf(key.toLowerCase(),0) != -1){
                                value = val.includes(',') ? (''+val).split(',') : [val];
                            }
                            if(key == 'VOLUME'){
                                if(!val.includes(',')){
                                    splitValue = val.split(' ');

                                    if(splitValue[0].trim() == "="){ value = " et/"+splitValue[1]; }
                                    if(splitValue[0].trim() == "<"){ value = " lt/"+splitValue[1]; }
                                    if(splitValue[0].trim() == ">"){ value = " gt/"+splitValue[1]; }
                                    if(splitValue[0].trim() == ">="){ value = " gte/"+splitValue[1]; }
                                    if(splitValue[0].trim() == "<="){ value = " lte/"+splitValue[1]; }
                                }
                                else{
                                    splitValue = val.split(',');
                                    conditionValue = splitValue.slice(0,splitValue.length-1).join(' '); 

                                    if(Number.isNaN(Number(splitValue[splitValue.length-1].trim())) == true){
                                        showMessage('Invalid quantity search values!',error);
                                        volumeValue = "";
                                    }
                                    else{
                                        volumeValue = splitValue[splitValue.length-1];
                                    }

                                    if(conditionValue.trim() == "equals to"){ value = " et/"+volumeValue; }
                                    else if(conditionValue.trim() == "less than to"){ value = " lt/"+volumeValue; }
                                    else if(conditionValue.trim() == "greater than"){ value = " gt/"+volumeValue; }
                                    else if(conditionValue.trim() == "greater than or equal to"){ value = " gte/"+volumeValue; }
                                    else if(conditionValue.trim() == "less than or equal to"){ value = " lte/"+volumeValue; }
                                    else{ value = "" };
                                }
                            }
                            selectedQueryParams.push({abbrev:key,values:value});
                        }
                    })

                    loadParamsToForm(selectedQueryParams);
                    
                },
            });
        }
        else{
            $('#invalid-notif').show();
        }
    });

    // loads selected and processed filters to query parameter filter fields
    function loadParamsToForm(savedQueryParams){
        event.preventDefault();
        
        $.ajax({
            async: true,
            url:'$loadParamsToFormUrl',
            type: 'post',
            dataType: 'html',
            data: {
                selectedParams:savedQueryParams,
            },
            success: function(response){
                $('.variable-filters').html(response);
            },
            complete: function(){

                $('.collapsible-add-filter').collapsible();

                setTimeout(function() { 
                    $("#find-btn").click();
                },700);

            }
        });
        return false;
    }

    // retrieve search parameters and pass on to find()
    $(document).on('click','#find-btn', function(e) {

        e.preventDefault();
        var data = $('.find-seeds-filter').serializeArray();
        var filters = getFilterValues(data);
        var isRequiredFilter = false, isInputList = false;
        var requiredParams = '';
        
        // check filters if required and/or input list type
        $.each(filters['filter'],function(index,value){
            if(value['value'] != "" && requiredFilters != null && requiredFilters != [] && requiredFilters.indexOf(value['name']) != -1){
                isRequiredFilter = true;
            }
        
            if(value['value'] != "" && (inputListFields.indexOf(value['name'].toLowerCase()) != -1)){
                isInputList = true;
            }
        });
        
        // check if has missing required filters
        if(requiredFilters != null && requiredFilters != []){
            requiredParams = requiredFilters.join();
            requiredParams = requiredParams.replace(/_/g, ' ').toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            }).replace(/,/g, ', ');
            missingFilter = (isRequiredFilter == false && isInputList == false ? true : false); 
        }
        else{
            missingFilter = (isInputList == false ? true : false); 
        }
        
        var limitGroup = 0;
        var limitGroupError = false;
        if ($('#set-limit-group').prop("checked") === true) {
            var value = $("#limit-packages").val();
            if ($.isNumeric(value) && value > 0) {
                limitGroup = value;
            } else {
                limitGroupError = true;
                showMessage('<span>Invalid integer for package limit.</span>','error');
            }
        }

        if(filters['cleanValues']== true && missingFilter == false && limitGroupError == false){
            if(getWorkingListItemCount() > 0){
                showMessage('<span class="left-align">All items in the Working List are excluded from the seed search results.</span>','message');
            }

            sessionStorage.removeItem('search-grid-table-con'+'_checkbox_storage');
            displayTotalCountFS(null);
            findSeeds(filters, limitGroup);
        }

        if((filters['cleanValues'] != true && missingFilter == false) || (filters['cleanValues'] != true && missingFilter)){
            showMessage('<span>Invalid input value. '+filters['error']+'</span>','error');
        }

        // Validation for inputList values
        filters['inputList'] = filters['inputList'] ?? false;
        // Condition if input list has no value or text, show this error
        if(filters['inputList'] == false && missingFilter == true){
            showMessage('<span>Missing required filters. Use at least 1 of the basic search parameters ('+requiredParams+') or the input list.</span>','error');
        }

        $('.collapsible-add-filter').collapsible();
    });

    // call to seed search function
    function findSeeds(filters, limitGroup){
        var url = $('.find-seeds-filter').attr('action');
        var includeSeedlist=$("#removeRowOption-checkbox-id").prop("checked");
        
        filter = filters['filter'];
        
        filter.push({"name": "userId","value":userId});
        filter.push({"name": "includeSeedlist","value":includeSeedlist});

        $.ajax({
            url: url,
            type: 'post',
            data: {
                filter:filter,
                activeTab:'variable',
                limitGroup: limitGroup
            },
            success: function(response){
                $(".find-loading-progress").removeClass("hidden");
                $.pjax.reload({
                    url: url,
                    container: '#'+searchBrowserId+'-pjax', 
                    replace:true
                });
                $('.collapsible-filter').collapsible('close', 0);
                $('.collapsible-add-filter').collapsible('close', 0);
            },
            error: function(){
                showMessage('Error encountered in seeds search.','error');
            }
        })
    }

    // Get all single package record ids
    function getAllSinglePackageIds(){
        var currentSelection = sessionStorage.getItem(qResultsSessionStorage);

        // check if need to get package ids
        $.ajax({
            url: '$checkCurrSelectedIdsUrl',
            type: 'post',
            data: {
                currSelection:currentSelection
            },
            success: function(response){
                var parsedResponse = JSON.parse(response)
                var sameSessionSelectedIds = parsedResponse.sameSessionSelectedIds

                // proceed to retrieving ids if different session and selected ids
                if(!sameSessionSelectedIds){
                    $('.toast').css('display','none')

                    var value = $('#'+searchBrowserId+'-pjax .kv-panel-before .summary').html();
                    var totalCount = 0;

                    if(value != null){
                        var split = value.split('of');
                        split = split[1].replace('items', '');
                        split = split.replace('item', '');
                        split = split.replace('<b>', '');
                        split = split.replace('</b>', '');
                        split = split.replace(' ', '');
                        split = split.replace(',', '');
                        totalCount = parseInt(split);
                    }
                    
                    if(totalCount != 0){
                        message = 'Retrieving single package record ids in progress. <br/>Selected items count will be updated once completed.'
                        showMessage(message,'info');
                    
                        $.ajax({
                            url: '$getSinglePackageRecordIdsUrl',
                            type: 'post',
                            data: {
                                activeTab:'variable',
                                totalCount:totalCount
                            },
                            success: function(response){
                                var parsedResponse = JSON.parse(response)

                                if(parsedResponse.bgProcess){
                                    if(!getIdsInBackground){
                                        clearSelectionFS();
                                        getAllSinglePackageIdsViaBgProcess(totalCount);
                                    }
                                }
                                else{
                                    var selectedItems = parsedResponse.ids;

                                    if(selectedItems != null){
                                        if(currentSelection != null && currentSelection != JSON.stringify(selectedItems)){
                                            displayTotalCountFS(selectedItems);
                                            sessionStorage.setItem(qResultsSessionStorage, '');
                                            sessionStorage.setItem(qResultsSessionStorage, JSON.stringify(selectedItems));
                                        }
                                    }
                                    else{
                                        displayTotalCountFS(null);
                                    }

                                    message = 'Successfully retrieved single package record ids!';
                                    showMessage(message,'success');
                                }
                            },
                            error: function(){
                                showMessage('Error encountered in retrieving single package records ids.','error');
                            }
                        })
                    }
                }
            },
            error: function(){
            }
        })
    }

    // Retrieve all single-package records ids via background process worker
    function getAllSinglePackageIdsViaBgProcess(totalCount){
        var message = 'Background process for retrieving single package records ids in progress.'+
                      ' The selected items count will be updated once process is completed.'

        $('#bg-progress-notif-msg').html("<i class='fa fa-info'></i> " + message);
        $('#bg-progress-notif').removeClass("success");
        $('#bg-progress-notif').removeClass("error");
        $('#bg-progress-notif').removeClass("hidden").addClass('active');
        $('#bg-progress-notif').addClass('info');
        $('#bg-progress-notif').show();

        $.ajax({
            url: '$getSinglePackageRecordIdsBgProcessUrl',
            type: 'post',
            data: {
                activeTab:'variable',
                totalCount:totalCount
            },
            success: function(){
                getIdsInBackground = true;

            },
            error: function(){
                showMessage('Error encountered in retrieving single package records ids via background process.','error');
            }
        })
    }

    // get selected items count from data browser configuration record
    function getSelectedItemsCount(){
        sessionStorage.setItem(qResultsSessionStorage, '');

        $.ajax({
            url: '$getSinglePackageRecordIdsCountUrl',
            type: 'post',
            success: function(response){
                var parsedResponse = JSON.parse(response);
                var selectedItems = parsedResponse.ids;

                if(selectedItems != undefined && selectedItems != null){
                    var currentSelection = sessionStorage.getItem(qResultsSessionStorage);

                    if(currentSelection != null && currentSelection != JSON.stringify(selectedItems)){
                        displayTotalCountFS(selectedItems);
                        sessionStorage.setItem(qResultsSessionStorage, '');
                        sessionStorage.setItem(qResultsSessionStorage, JSON.stringify(selectedItems));
                    }

                    getIdsInBackground = false;
                }
                else{
                    displayTotalCountFS(null);
                }
            },
            error: function(){
                showMessage('Error encountered in retrieving selected items count.','error');
            }
        })
    }

    // extract variable filters and values
    function getFilterValues(data){

        var flagInput = false, flag = 0, volumeFlag = false;
        var placeholder = [];
        var str = '', error = '';
        var output = {'cleanValues':true,'filter':filter,'error':''};
        
        var isEmpty = true;
        $.each(data,function(index,value){
            if(value.value !== "" && value.name !== "_csrf") { 
                isEmpty = (isEmpty && false); 
            };
        });

        if(isEmpty == false){
            $.each(data,function(i,v){
                flag = 0;
                str = v.name;

                if(placeholder.length > 0){
                    $.each(placeholder,function(j,k){
                        
                        if(k.name === str){
                            v.value = k.value;
                            flag = 1;
                        }
                    });
                    v['value'] = v['value'].replaceAll(', ',',');
                }
                if(flag == 0 && str.includes("][")==false && v.value != ""){
                    placeholder.push(v);
                }
            });
            $.each(placeholder,function(i,v){
                str = v.name;
                if(str.includes('[')){
                        v.name = str.split("[")[1].split("]")[0];
                }
            });

            var inputFieldType = "";
            var inputValues = "";
            var volumeField = "";
            var volumeValue = "";

            var excludedNameValues = ['-selection','_csrf','INPUT_','VOLUME'];
            var asIsValues = ['PLOTNO','ENTNO','ENTCODE','REP'];

            var filter = $.map(placeholder, function(v,i) {

                if(v.name == 'INPUT_FIELD'){ inputFieldType = v.value.toUpperCase(); }
                if(v.name == 'INPUT_VALUE'){ inputValues = v.value;}
                if(v.name == 'VOLUME'){
                        var splitValues = v.value.includes('/') ? v.value.split('/') : [v.value];
                        volumeField = splitValues[0]; 
                    }
                if(v.name == 'VOLUME_VALUE'){ volumeValue = v.value; }

                if( v.name != "" && (excludedNameValues.some(val => v.name.includes(val))) == false){
                    if(asIsValues.includes(v.name)) v.value = v.value.replace(/\s/g, '');
                    return v;
                }
            });
            output = validateFilterValues(
                    filter,
                    {'inputFieldType':inputFieldType,'inputValues':inputValues},
                    {'volumeField':volumeField,'volumeValue':volumeValue}
                ); 
            return output;
        }
        else{
            return {'cleanValues':false,'filter':filter,'error':'No filters selected.'};
        }

    }

    // validates input filter values
    function validateFilterValues(filter, inputListValues, volumeValues){

        var volumeFlag = false, flagInput = true, flag = 0;
        var numericInputFields = ['PLOTNO','ENTNO','PLOT_NO','ENTRY_NO','REP'];
        var dateCheck = {'cleanValues':true, 'filter':filter, 'error':''};
        var output = {'cleanValues':true,'filter':filter,'error':''};

        output = validateInputListValues(inputListValues,output,flagInput);
        output = validateVolumeCondition(volumeValues,output,volumeFlag);

        flag = true;
        $.each(filter,function(index,val){
            if(numericInputFields.includes(val.name) && val.value != ""){
                numericCheck = validateNumericInputs(val,output);
                if(numericCheck['cleanValues'] == false){
                    output = {'cleanValues':numericCheck['cleanValues'],'filter':filter,'error':numericCheck['error']};
                    return false;
                }
            }

            if(val.name == 'HVDATE_CONT' && val.value != ""){
                dateCheck = validateDateValues(val.value);
                if(dateCheck['cleanValues'] == false){
                    output = {'cleanValues':dateCheck['cleanValues'],'filter':filter,'error':dateCheck['error']};
                    return false;
                }
            }
        });
        return output;
    }

    // validate input list values
    function validateInputListValues(inputListValues,output,flagInput){
        let inputListLimit = '$inputListLimit';
        inputListLimit = parseInt(inputListLimit);

        if((inputListValues['inputFieldType']=="" && inputListValues['inputValues']!="") || (inputListValues['inputFieldType']!="" && inputListValues['inputValues']=="")){
            output = {'cleanValues':false,'filter':output['filter'],'error':'Requires both field type and input field values.'};
            return output;
        } 

        if(inputListValues['inputFieldType']!="" && inputListValues['inputValues']!=""){
            
            value = inputListValues['inputValues'];
            listValidation = listInputValidation(inputListValues['inputFieldType'],value.split(","),flagInput);

            valuesCount = value.split(",").length;
            validValueCount = true;
            errorMessage = '';

            if(listValidation['flag'] == true && validValueCount == true && valuesCount <= inputListLimit){
                output['filter'].push({'name':inputListValues['inputFieldType'].toUpperCase(),'value':value});
                output = {'cleanValues':true,'filter':output['filter'],'error':''};
            }
            else{
                if(valuesCount > inputListLimit){
                    errorMessage = 'Notice! List exceeds '+inputListLimit+' items. Please use a smaller list.'
                }
                error = listValidation['error'] + errorMessage;
                output = {'cleanValues':false,'filter':output['filter'],'error':error,'inputList':true};
            }
        }
        return output; 
    }

    // validate values for volume condition
    function validateVolumeCondition(volumeValues,output,volumeFlag){
        
        if(volumeValues['volumeField']!="" && volumeValues['volumeValue']!=""){
            value = "";
            
            if(Number.isNaN(Number(volumeValues['volumeValue'])) == true || Number(volumeValues['volumeValue']) < 0){
                volumeFlag = true;
            }
            if(volumeFlag == false){
                if(volumeValues['volumeField'].trim() == "gt"){
                    value = value.concat("greater than ");
                }
                else if(volumeValues['volumeField'].trim() == "lt"){
                    value = value.concat("less than ");
                }
                else if(volumeValues['volumeField'].trim() == "et"){
                    value = value.concat("equals ");
                }
                else if(volumeValues['volumeField'].trim() == "gte"){
                    value = value.concat("greater than or equal to ");
                }
                else if(volumeValues['volumeField'].trim() == "lte"){
                    value = value.concat("less than or equal to ");
                }
                value = value.concat(volumeValues['volumeValue']);

                output['filter'].push({'name':'VOLUME','value':value});
                output = {'cleanValues':true,'filter':output['filter'],'error':''};
            }
            else{
                output = {'cleanValues':false,'filter':output['filter'],'error':'Input volume should be numeric.'};
            }

        }
        if((volumeValues['volumeField'] != "" && volumeValues['volumeValue'] == "") || (volumeValues['volumeField'] == "" && volumeValues['volumeValue'] != "" && volumeValues['volumeValue'] != 0))
        {
            output = {'cleanValues':false,'filter':output['filter'],'error':'Requires both volume condition and value.'};
        }

        return output;
    }

    // validate values of numeric input fields
    function validateNumericInputs(value,output){
        var values, sections,flag = true;
        var rangeCheck = {'cleanValues':true, 'filter':output['filter'], 'error':''};
        var formatCheck = {'cleanValues':true, 'filter':output['filter'], 'error':''};
        var numericCheck = {'cleanValues':true, 'filter':output['filter'], 'error':''};
        var rangeFilters = "", numericFilter = "", hasRange = false;
        
        values = value['value'].includes(',') ? value['value'].split(",") : [value['value']];
        $.each(values,function(ind,val){  
            if(val.includes('-')){ // has range values
                sections = val.split('-');
                
                rangeCheck = validateRangeValues(val);
                if(rangeCheck['cleanValues'] == false){
                    output = {'cleanValues':rangeCheck['cleanValues'],'filter':output['filter'],'error':rangeCheck['error']};
                }
                else{
                    rangeFilters = rangeFilters + "range: " + values[ind] + ',';
                    hasRange = true;
                }
            }
            else{ // if numeric and valid format
                numericCheck = checkForNumericValue(val.trim());
                if(numericCheck['cleanValues'] == false){
                    showMessage('<span>Invalid input value. '+numericCheck['error']+'</span>','error');
                }

                formatCheck = checkForSpecialCharacters(val);
                if(formatCheck['cleanValues'] == false){
                    showMessage('<span>Invalid input value. '+formatCheck['error']+'</span>','error');
                }

                if(formatCheck['cleanValues'] == true && numericCheck['cleanValues'] == true){
                    numericFilter = numericFilter + values[ind] + ',';
                }
            }
        });
        if(hasRange == true){
            $.each(output['filter'],function(i,v){
                if(v['name'] == value['name']){ output['filter'][i]['value'] = numericFilter+rangeFilters; }
            });
        }
        return output;
    }

    // validate if input is valid range values
    function validateRangeValues(values){

        var rangeValues;
        var output = {'cleanValues':true,'error':''};        

        if(values.includes('-')){
            rangeValues = values.split("-");

            if(rangeValues.length > 2){
                output = {'cleanValues':false, 'error':'Requires 2 numeric range values.'};
            }

            if(Number.isNaN(Number(rangeValues[0])) == true || Number.isNaN(Number(rangeValues[1])) == true){
                output = {'cleanValues':false,'error':'Requires numeric range values.'};
            }
        }
        return output;
    }

    // validate if input has special characters
    function checkForSpecialCharacters(value){

        var format = /[!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?]+/;
        var output = {'cleanValues':true,'error':''}; 

        if(format.test(value) == true){
            output = {'cleanValues':false,'error':"Requires '-' as value separator."};
        }

        return output;
    }

    // validate if input is valid nuemric value
    function checkForNumericValue(value){

        var output = {'cleanValues':true,'error':''};
        var postgresMaxInt = 2147483647; 

        if((Number(value) < 0) || (Number.isNaN(Number(value)) == true)){
            output = {'cleanValues':false, 'error':'Requires valid numeric value.'};
        }
        if(Number(value) > postgresMaxInt){
            output = {'cleanValues':false, 'error':'Exceeded acceptable integer value.'};
        }
        return output;
    }

    // validate date vaue input
    function validateDateValues(value){
        var dateValues = value.split('-');
        var output = {'cleanValues':true,'error':''};

        if(dateValues.length < 3 || dateValues.length > 3){
            output = {'cleanValues':false, 'error':'Invalid date format.'};
        }
        else if(parseInt(dateValues[0]) > new Date().getFullYear() || parseInt(dateValues[0]) < 1902){
            output = {'cleanValues':false, 'error':'Invalid year value.'};
        }
        else if(parseInt(dateValues[1]) < 1 || parseInt(dateValues[1]) > 12){
            output = {'cleanValues':false, 'error':'Invalid month value.'};
        }
        else if(parseInt(dateValues[2]) < 1 || parseInt(dateValues[2]) > 31){
            output = {'cleanValues':false, 'error':'Invalid day value.'};
        }
        return output;
    }

JS;
$this->registerJS('var inputListFields='.$inputListTypeData.', requiredFilters='.json_encode($requiredFilters).', inputListTypes='.$inputListTypeData.', tempTable='.json_encode($tempTable).'; '.$script);
?>