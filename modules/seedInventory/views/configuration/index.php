<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Index file for Seed Inventory Configuration
**/

?>

<div class="seedInventory-config-index ">
    <h3> <?= Yii::t('app', 'Seed Inventory Configuration') ?></h3>
    <ul class="collapsible seedInventory-config-index-collapsible"  data-collapsible="expandable">
        <li>
            <div class="collapsible-header"><a><i class="material-icons">straighten</i><?php echo \Yii::t('app','Configure Quantities');?></a></div>
            <div class="collapsible-body">
                <span>
                    <?php echo $this->renderFile('@app/modules/seedInventory/views/unit/index.php');?>
                </span>
            </div>
        </li>
        <li>
            <div class="collapsible-header"><a><i class="material-icons">code</i><?php echo \Yii::t('app','Configure Seedlot Codes');?></a></div>
            <div class="collapsible-body">
                <span>
                    <?php echo $this->renderFile('@app/modules/seedInventory/views/seedlot-code-pattern/index.php');?>
                </span>
            </div>
        </li>
        <li>
            <div class="collapsible-header"><a><i class="material-icons">category</i><?php echo \Yii::t('app','Seed Inventory Variables');?></a></div>
            <div class="collapsible-body">
                <span>
                    <?php echo $this->renderFile('@app/modules/seedInventory/views/seedlot-variable/index.php');?>
                </span>
            </div>
        </li>
        <li>
            <div class="collapsible-header"><a><i class="material-icons">category</i><?php echo \Yii::t('app','Seed Storage Facility');?></a></div>
            <div class="collapsible-body">
                <span>
                    <?php  echo $this->renderFile('@app/modules/seedInventory/views/program-facility/index.php');?>
                </span>
            </div>
        </li>
        <li>
            <div class="collapsible-header"><a><i class="material-icons">category</i><?php echo \Yii::t('app','Seed Package Type');?></a></div>
            <div class="collapsible-body">
                <span>
                    <?php  echo $this->renderFile('@app/modules/seedInventory/views/program-package-type/index.php');?>
                </span>
            </div>
        </li>



    </ul>
</div>
