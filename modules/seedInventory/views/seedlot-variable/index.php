<?php
/*
* This file is part of EBS-Core Breeding.
*
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* View of Seedlot Variables
**/

use kartik\grid\GridView;
use kartik\date\DatePicker;
use \app\modules\seedInventory\models\SeedlotVariable;

?>
<!--  View Variable Information Modal-->
<div  class="fade modal in" id="summary-variable-modal" tabindex="-1" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span class="loading-div"></span>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 id="summary-variable-header-id"></h4>
            </div>
            <div class="modal-body summary-variable-modal-content">

            </div>
            <div class="modal-footer">
                <a class="btn-flat " data-dismiss="modal" style="margin-right:5px;background-color:transparent !important; text-transform:none !important;"><?php echo \Yii::t('app','Close');?></a>

            </div>
        </div>
    </div>
</div>
<?php
$actionColumns = [
	[
        'class' => 'kartik\grid\SerialColumn',
        'width'=>'1%'
    ], 

    [
        'class'=>'kartik\grid\ActionColumn',
        'header' => 'Actions',
        'noWrap' => true,
        'template' => '{view}',
        'width'=>'1%',
        'buttons' => [
           
            'view' => function ($url, $model, $key) {

                return yii\helpers\Html::a('<i class="material-icons">visibility</i>',
                    '#',
                    ['style'=>'cursor: pointer;',
                    'class' => 'view-variable-btn', 
                    'title' => 'View variable information',
                    'data-id' =>$model['id'],

                    'data-pjax' => 0,

                    'data-variable_id' => $model["id"],
                    'data-variable_name' => $model["name"],
                    'data-toggle'=>"modal",
                    "data-target"=>"#summary-variable-modal",

                ]

            );

            },
        ]
    ]
];

$columns = [
	
    'abbrev',
    'label',
    'name',
    'data_type',
    'type',
    'display_name',
    [
        'attribute'=>'description',
        'value' => function($data){
            if(trim($data['description'])==''){
                return null;
            }else{
                return $data['description'];
            }
        }
    ],
    
    'usage',
    'status',
    // 'not_null:boolean',
    [
        'attribute' => 'not_null', 
        'vAlign' => 'middle',
        'hAlign'=>'center',
        'filterType' => GridView::FILTER_SELECT2,
        
        'filter'=>['true'=>'Yes','false'=>'No'], 
        'filterWidgetOptions' => [
            'id'=>'not_null-column-id',
            'pluginOptions' => [
                'allowClear' => true,
                'id'=>'not_null-column-id',
            ],                    
        ],     
        'filterInputOptions'=>['placeholder'=>'Not Null',],
        'value'=>function($data){
            if($data['not_null']){
                return 'Yes';
            }else{
                return 'No';
            }

        } 
    ],
    [
        'attribute'=>'creator_name',
        'label'=>'Creator',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'attribute'=>'creation_timestamp',
        'label'=>'Creation Timestamp',
        'format' => 'dateTime',
        'hAlign'=>'center',
        'vAlign'=>'middle',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true, 
                'clearBtn' => true,
                'options'=>[
                    'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ], 
    ],

    [
        'attribute'=>'modifier_name',
        'label'=>'Modifier',
        'hAlign'=>'center',
        'vAlign'=>'middle',     
    ],
    [
        'attribute'=>'modification_timestamp',
        'label'=>'Modification Timestamp',
        'format' => 'dateTime',
        'hAlign'=>'center',
        'vAlign'=>'middle', 
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [

            'pluginOptions'=> [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton'=>true,
                'todayHighlight' => true,
                'showButtonPanel'=>true, 
                'clearBtn' => true,
                'options'=>[
                    'type'=>DatePicker::TYPE_COMPONENT_APPEND,
                    'removeButton'=>true,
                ]
            ],
            'pluginEvents' => [
                "clearDate" => 'function (e) {$(e.target).find("input").change();}',
            ],
        ],    
    ],

];
$gridColumns = array_merge($actionColumns,$columns);
$searchModel = new SeedlotVariable();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

?>

<?php
$dynagrid = kartik\dynagrid\DynaGrid::begin([
    'columns' => $gridColumns,
    'theme'=>'simple-bordered',
    'showPersonalize'=>true,
    'storage' => 'cookie',
    'showFilter' => false,
    'showSort' => false,
    'allowFilterSetting' => false,
    'allowSortSetting' => false,
    'options'=>['id'=>'seed-inventory-seedlotVariable-grid','class'=>""], 
    'gridOptions'=>[
        'moduleId' => 'gridview',
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>['id'=>'seed-inventory-seedlotVariable-grid'],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],

        'dataProvider'=> $dataProvider,
        'filterModel'=> $searchModel,
        'showPageSummary'=>false,
        'responsiveWrap'=>false,

        'panel'=>[
            'before' => \Yii::t('app', 'This is the browser for all the variables for seed inventory. '),
        ],
        'toolbar' =>  [
            [
                'content'=> yii\helpers\Html::a('<i class="glyphicon glyphicon-repeat"></i>','#', 
                    [
                        'class' => 'btn btn-default', 
                        'title'=>\Yii::t('app','Reset grid'),
                        'id'=>'seedlotVariable-reset-grid-id'
                    ]).'{dynagridFilter}{dynagridSort}{dynagrid}',
            ],

            'exportConfig' =>false,

        ]
    ]

]);
kartik\dynagrid\DynaGrid::end();
?>   
<script type="text/javascript"> 
    $(document).on('click','#seedlotVariable-reset-grid-id', function(e) {

        e.preventDefault();
        $.pjax.reload({
            container: '#seed-inventory-seedlotVariable-grid-pjax',
            replace: true,
            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
        });
    });
    $(document).ready(function(){
        $('#summary-variable-modal').on('show.bs.modal',function(e){
            
            var obj =$(e.relatedTarget)
            var variableName = obj.data('variable_name');
            var variableId = obj.data('variable_id');      
            viewVariableUrl='<?php echo  yii\helpers\Url::toRoute(["/variable/view"]);?>';
            variableUrl= '<?php echo yii\helpers\Url::toRoute(["/variable/default/view"])?>';

            $(".view-complete-details-btn-id").prop("href",viewVariableUrl+"/"+variableId);
            $('#summary-variable-header-id').html('<i class="material-icons"></i> ' + variableName);

            $(".summary-variable-modal-content").html('');
            $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
            
            $.ajax({
                url: variableUrl+"?id="+variableId,
                type: 'get',

                success: function(response) {
                    
                    $(".summary-variable-modal-content").html(response);
                    $('.tabs').tabs();
                    $('.loading-div').html('');

                },
                error: function() {
                    $("summary-.variable-modal-content").html('<div class="card-panel red"><span class="white-text">There seems to be a problem while retrieving variable information.</span></div>');
                }
            });
        });
    });
</script>