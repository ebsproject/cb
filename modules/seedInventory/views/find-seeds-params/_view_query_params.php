<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Add from Seed List modal content
 **/

use app\models\PlatformList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use yii\web\JqueryAsset;

?>

<div>
    <?php
        $notif = '<div id="invalid-notif" class="red-text text-darken-2" style = "display:none; margin-bottom:10px;" ><span style="">No search parameter selected!</span></div>';

        $listDataProvider = $queryParamsDataProvider;

        $actionColumns = [
            [
                'label' => "Radio Button",
                'mergeHeader' => true,
                'header' => '<button type="button" class="close kv-clear-radio transparent" title="Clear selection" style="margin-left:10px;" id="select-query-params">×</button>',
                'headerOptions'=>['class'=>'radio-btn-row'],
                'content' => function ($model) {
                    return '
                        <input class="query-params-select-radio filled-in with-gap" name="params-radio-tn-grp" type="radio" id="params-item-' . $model["configDbId"] . '" />
                        <label for="params-item-' . $model["configDbId"] . '" id="label-for-radio" style="padding-left: 20px;" ></label>';
                },
                'width' => '1%',
            ],
            
        ];

        $columns = [
            [
                'attribute' => 'name',
                'header' => 'Name',
                'visible' => true,
                'format' => 'raw',
            ],
            [
                'attribute' => 'data',
                'header' => 'Search Parameters',
                'visible' => true,
                'format' => 'raw',
                'content' => function($model){
                    $val = $model['data'];
                    if(isset($val['ids']) && isset($val['texts'])){
                        return json_encode($val['texts']);
                    }
                    else{
                        return json_encode($val);
                    }
                }
            ],

        ];
        $gridColumns = array_merge($actionColumns, $columns);

        echo $notif;

        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme' => 'simple-default',
            'showPersonalize' => true,
            'storage' => 'cookie',
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'dynagrid-view-params',
                'dataProvider' => $listDataProvider,
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'dynagrid-view-params',
                    ],
                ],
                'panel' => [
                    'before' => \Yii::t('app', 'Select a saved search parameter here.'),
                    'after' => false,
                ],
                'toolbar' => '',
            ],
            'options' => ['id'=>'dynagrid-view-params'],
            
        ]);
        DynaGrid::end();
    ?>
</div>