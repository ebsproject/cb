<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file creating the basic info of saved query parameters
 **/

use app\models\DataBrowserFilterConfig;
use app\models\PlatformList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$notif = '<p id="invalid-note" class="red-text text-darken-2 hidden">'.\Yii::t('app', 'Name cannot be blank.').'</p>';
$hidden = '</div><span>' . Html::tag('h5', '', ['class' => 'params-hidden', 'id' => 'params-hidden', 'style'=>'display:none;']) . '</span>';

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'save-params-form',
    'fieldConfig' => function ($model, $attribute) {
        if (in_array($attribute, ['name', 'type'])) {
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        } else {
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    },
]);

$model = new DataBrowserFilterConfig();

echo '<div class="row">' . 
$form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id' => 'query-params-name',
    'class' => 'form-control',
    'autofocus' => 'autofocus',
]) .
$form->field($model, 'type')->textInput([
    'title' => 'Type of configuration',
    'id' => 'query-params-type',
    'readonly' => true
]) .
$form->field($model, 'data')->textarea([
    'class' => 'materialize-textarea',
    'title' => 'Search Parameters',
    'readonly' => true,
    'id' => 'query-params-area'
]) . $hidden;

ActiveForm::end();

$script = <<<JS

JS;

$this->registerJs($script);
