<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

/**
* View file creating the input list tab
**/

use kartik\select2\Select2;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div style = "margin-bottom:10px;">
    <caption>Choose the type of values you want to search, then input the list of search values below. Input values can be separated by commas (,) or new lines (Enter key).<b> Field type is required to be selected before input values can be added.</b></caption>
</div>

<div class = "row field-type-selection-2" style = "margin-bottom:5px;">
    <div class = "input-text-area-2 col-md-12 hidden" id="input-text-area-2" style="margin-bottom:0px; padding-right:0px;">
        <div class="row" id="no-list-layout" style = "margin-bottom:5px;">
            <div class= "col-md-11" id="field-type-selector">
                <?php
                    
                    $fieldTypeOptions = ["empty" => "Select a field type"];
                    $fieldTypeOptionTooltip['empty'] = ['data-toggle'=>"tooltip",'title'=>!empty($value) ? $value : ''];
                
                    if(isset($inputListFields) && !empty($inputListFields)){
                        foreach($inputListFields as $key=>$option){
                            $fieldTypeOptions[$key] = $option['text'];
                            $fieldTypeOptionTooltip[$key] = $option['description'];
                        }
                    }
                    
                    echo Select2::widget([
                        'name' => 'field-type-selection-2',
                        'id' => 'field-type-selection-2',
                        'data' => $fieldTypeOptions,
                        'value' => 'empty',
                        'options' => [
                            'tags' => true,
                            'class' => 'field-type-selection variable-filter',
                            'multiple' => false,
                            'options' => $fieldTypeOptionTooltip,
                        ],
                    ]);
                ?>
            </div>
            <div class= "col-md-1" id="show-list-btn">
                <?php
                    echo \macgyer\yii2materializecss\widgets\Button::widget([
                        'label' => Yii::t('app', ''),
                        'icon' => [
                            'name' => 'playlist_add',
                            'position' => 'right',
                            'options' => [
                                'style' => 'margin-left:0px;',
                            ],
                        ],
                        'options' => [
                            'class'=>'light-green darken-3 pull-right',
                            'id' => 'add-list-btn',
                            'style' => 'margin-left:0px;margin-right: 0px;',
                            'title'=>'Add items from designation list',
                        ],
                    ]);
                ?>
            </div>
        </div>
        <div class="row" style = "margin-bottom:0px;">
            <div class="germplasm-list" id="germplasm-list" style="margin-bottom:0px;">
                <?php
                    $listDataProvider = isset($germplasmListDataProvider) && !empty($germplasmListDataProvider) ? $germplasmListDataProvider : new \yii\data\ArrayDataProvider([]);
                    $actionColumns = [
                        ['class' => 'yii\grid\SerialColumn'], 
                        [
                            'class'=>'kartik\grid\ActionColumn',
                            'header' => Yii::t('app', 'Actions'),
                            'template' => '{select}',
                            'buttons' => [
                                'select' => function ($url, $model, $key) {
                                    return Html::a('<i class="material-icons">exit_to_app</i>',
                                        '#',
                                        [
                                            'class'=>'add-items-to-list',
                                            'title' => Yii::t('app', 'Select saved list'),
                                            'data-saved_list_id' => $model["id"]
                                        ]
                                    );
                                },
                            ]
                        ],
                        
                    ];
            
                    $columns = [
                        [
                            'attribute' => 'id',
                            'header' => 'Id',
                            'visible' => true,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'display_name',
                            'header' => 'Name',
                            'visible' => true,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'abbrev',
                            'header' => 'Abbrev',
                            'visible' => true,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'item_count',
                            'header' => 'Number of Items',
                            'visible' => true,
                            'format' => 'raw',
                        ],
            
                    ];
                    $gridColumns = array_merge($actionColumns, $columns);
            
                    $dynagrid = DynaGrid::begin([
                        'columns' => $gridColumns,
                        'theme' => 'panel-default',
                        'showPersonalize' => true,
                        'storage' => 'cookie',
                        'showFilter' => false,
                        'showSort' => false,
                        'allowFilterSetting' => false,
                        'allowSortSetting' => false,
                        'gridOptions' => [
                            'id' => 'dynagrid-saved-designation-list',
                            'dataProvider' => $listDataProvider,
                            'floatHeader' =>false,
                            'floatOverflowContainer'=> true,
                            'showPageSummary' => false,
                            'pjax' => true,
                            'pjaxSettings' => [
                                'options' => [
                                    'id' => 'dynagrid-saved-designation-list',
                                ],
                            ],
                            'panel' => [
                                'before' => false,
                                'after' => false,
                            ],
                            'toolbar' => '',
                        ],
                        'options' => [
                            'id'=>'dynagrid-saved-designation-list',
                            'style' => 'margin-bottom:0px;'
                        ],
                        'submitButtonOptions' => [
                            'icon' => 'glyphicon glyphicon-ok',
                        ],
                        'deleteButtonOptions' => [
                            'icon' => 'glyphicon glyphicon-remove',
                            'label' => 'Remove',
                        ],
                    ]);
                    if (substr($dynagrid->theme, 0, 6) == 'simple') {
                        $dynagrid->gridOptions['panel'] = false;
                    }
                    DynaGrid::end();
                ?>
            </div>
        </div>
    </div>
    <div class="custom-control custom-radio custom-control-inline" id="has-list-layout" style="margin-left: 0px;margin-right:0px;padding-right: 5px;padding-left: 10px;">
    <div class="row" style="margin-bottom:0px;">
            <textarea class="form-control input-filter variable-filter" id="input-area" rows="25" spellcheck="false" style="position: relative; background-color: white; line-height: 25px;"></textarea>
            <?php
                echo Html::input(
                    'text',
                    'filter[INPUT_VALUE]',
                    '',
                    [
                        'id' => 'input-value-hidden',
                        'class' => 'hidden input-value-hidden variable-filter',
                    ]
                );
            ?>
        </div>
        <?php
            echo Html::input(
                'text',
                'filter[INPUT_FIELD]',
                '',
                [
                    'id' => 'input-field-hidden',
                    'class' => 'hidden select2-field-type-hidden variable-filter',
                ]
            );
        ?>
    </div>
</div>

<?php
    $count = $filterValues && count($filterValues) > 0 ? count($filterValues) : 0;
    $data = isset($filterValues) && !empty($filterValues) ? json_encode($filterValues) : '{}';
    $inputListTypes = isset($inputListFields) && !empty($inputListFields) ? json_encode($inputListFields) : '{}';

    $inputListParamStatus = $inputListParamStatus != true && $inputListParamStatus != 1 ? 0 : true;
    $addFilterParamStatus = $addFilterParamStatus != true && $addFilterParamStatus != 1 ? 0 : true;

    $getListProviderUrl = Url::to(['list-management/get-saved-seed-list-data-provider']);
    $getGermplasmListProviderUrl = Url::to(['list-management/get-saved-list-data-provider']);
    $getListByIdUrl = Url::to(['list-management/get-selected-list-items']);

$script = <<<JS

    var values = '';
    var selectedList = null;
    var indexMatch = -1;

    var inputListType = inputListTypes !== '' ? Object.keys(inputListTypes).map(type => {
        return type.toLowerCase();
    }) : [];
    
    // updates values of field type and input are on page reload 
    if(count>0){
        $('#field-type-selection-2').val('empty').trigger('change');
        $('#field-type-selection').val('empty').trigger('change');
        $.each(data,function(i,v){
            listType = v.abbrev;
            listValues = v.values;

            indexMatch = inputListType.indexOf(listType.toLowerCase());
            if(indexMatch != -1){
                listType = inputListType[indexMatch];

                $('#field-type-selection-2').val(listType).trigger('change');
                $('#field-type-selection').val(listType).trigger('change');
                $('#input-field-hidden').val(listType).trigger('change');
                $('#field-type-selection-hidden').val(listType).trigger('change');

                values = listValues.join().replace(/,/g, '\\n');
                $('#input-area').val(values).trigger('change');
                $('#input-value-hidden').val(listValues.join()).trigger('change');
                document.getElementById("find-btn").disabled = false;
            }
        })
    }

    $(document).ready(function() {
        updateInputListFilterValues();
        
        $("#dynagrid-saved-designation-list").find('.kv-grid-wrapper').css('height',285);
        $("#dynagrid-saved-designation-list-container").css('height',285);
        $("#dynagrid-saved-designation-list").find('.kv-panel-pager').addClass('hidden').trigger('change');
        $("#dynagrid-saved-designation-list").find('.panel-heading').addClass('hidden').trigger('change');
        $("#dynagrid-saved-designation-list").find('.panel-footer').addClass('hidden').trigger('change');
        
        $('#dataType-hidden').val('germplasm');
    });

    function updateInputListFilterValues(){

        var allFilters = $('.find-seeds-filter').serializeArray();

        var inputListValue = "", inputListType = "";
        $.each(allFilters, function(index,value){
            if(value['name'] == "filter[INPUT_VALUE]" && value['value'] != ""){
                inputListValue = value['value'];
            }

            if(value['name'] == "filter[INPUT_FIELD]" && value['value'] != ""){
                inputListType = value['value'];
            }
        });
        
        if(inputListValue != "" && inputListType!=""){
            $('#field-type-selection-2').val(inputListType).trigger('change');
            $('#field-type-selection').val(inputListType).trigger('change');
            $('#field-type-selection-hidden').val(inputListType).trigger('change');

            values = inputListValue.replace(/,/g, '\\n');
            $('#input-area').val(values).trigger('change');
            $('#input-value-hidden').val(inputListValue).trigger('change');
            document.getElementById("find-btn").disabled = false;
        }
        else{
            $('#field-type-selection-2').val('empty').trigger('change');
            $('#field-type-selection').val('empty').trigger('change');
        }
    }

    // sets selected field type
    $(document).on('change','#field-type-selection', function(e) {

        if($(this).val() != 'empty'){
            $('#input-field-hidden').val($(this).val());
            document.getElementById("find-btn").disabled = false;
        }
        else{
            $('#input-field-hidden').val('');
        }
    });
    
    // sets selected field type
    $(document).on('change','#field-type-selection-2', function(e) {

        if($(this).val() != 'empty'){
            $('#input-field-hidden').val($(this).val());
            document.getElementById("find-btn").disabled = false;
            $('#input-area').attr('disabled', false);
        }
        else{
            $('#input-field-hidden').val('');
            $('#input-area').attr('disabled', true);
        }
    });

    // sets input value
    $(document).on('change input paste keyup','#input-area', function(e) {
        inputValues = $(this).val();
        fieldType = $('#input-field-hidden').val();

        if(inputValues != ""){
            $('#input-value-hidden').val($('#input-area').val().replace(/\\n/g, ","));
            document.getElementById("find-btn").disabled = false;
        }
        else{
            $('#input-value-hidden').val("");
        }
    });

    // checks if input values are valid
    function listInputValidation(fieldType,values,flag){
        var postgresMaxInt = 2147483647;
        var output = {'flag':true,'error':''};

        if(fieldType=='GID'){ 
            $(values).each(function(i,v){
        
                if(Number.isNaN(Number(v)) == true){
                    flag = flag && false;
                    output = {'flag':flag,'error':'Requires valid numeric input value.'};
                }
                if(Number(v) > postgresMaxInt){
                    flag = flag && false;
                    output = {'flag':flag,'error':'Exceeded valid numeric input.'};
                }
            })
        }
        return output;

    }

    // retrieve search parameters and pass on to find()
    $(document).on('click','#add-list-btn', function(e) {
        e.preventDefault();
        
        var data = $('.find-seeds-filter').serializeArray();
        hasAdditionalFilters = checkForAdditionalParams(data,'filters');
        visibleAdditionalFilters = ($('#additional-params-panel').hasClass('col-md-6') == true && $('#additional-params-panel').hasClass('hidden') != true);
        
        $.ajax({
            url: '$getListProviderUrl',
            type: 'POST',
            data:{
                listType:'germplasm'
            },
            async:false,
            success: function(response) {
                
                if(visibleAdditionalFilters == true){
                    $('.open-list-modal-body').html(response);
                    $('#open-list-modal').modal('show');
                }
                $('#dataType-hidden').val('germplasm');
            },
            error: function() {
                
            }
        })
    });

    $(document).on('click','.add-items-to-list', function(e) {

        e.preventDefault();
        e.stopPropagation();
        
        var listObj = $(this)
        var selectedList = listObj.data('saved_list_id')
        
        if(selectedList != null){
            $.ajax({
                url: '$getListByIdUrl',
                type: 'POST',
                dataType: 'json',
                data:{
                    id: selectedList
                },
                success: function(response) {
                    if(response.success == true){
                        $('#field-type-selection').val('name').trigger('change');
                        $('#field-type-selection-2').val('name').trigger('change');
                        $('#input-field-hidden').val('name').trigger('change');

                        var inputValues = response.value;
                        var newValues = (inputValues.replace(/"/g, '')).replace(/,/g, '\\n');
                        
                        $('#input-area').val(newValues).trigger('change');
                    }
                    else{
                        if(response.error){
                            showMessage(''+response.error,'error');
                        }
                    }
                },
                error: function(error) {
                    if(error.status != 200){
                        showMessage('Error encountered in retrieving saved germplasm list.','error');
                    }
                }
            })
        }
        else{
            showMessage('No germplasm list selected. Select an item from the germplasm list.','error');
        }

    });

    // Renders Toast message
    function showMessage(message,type){
        var output = '';
        var icon = '';

        if(type == 'success'){
            icon = "<i class='material-icons green-text'>check</i>&nbsp; ";
        }
        else if(type == 'error'){
            icon = "<i class='material-icons red-text'>clear</i>&nbsp; ";
        }
        else if(type == 'warning'){
            icon = "<i class='material-icons yellow-text'>warning</i>&nbsp; ";
        }
        else if(type == 'message'){
            icon = "<i class='material-icons white-text'>info</i>&nbsp; ";
        }
        output = '<span class="left-align" style="text-align:left;">'+icon.concat(message)+'</span>';

        var hasToast = $('body').hasClass('toast');
        if(!hasToast){
            $('.toast').css('display','none');
            Materialize.toast(output, 5000);
        }
    }
JS;

$this->registerJS("var count=" . $count . ", data=" . $data . ", inputListTypes=".$inputListTypes.", inputListParamStatus=".$inputListParamStatus.", addFilterParamStatus=".$addFilterParamStatus." ;" . $script);

// Sets placement of Toast on page
$this->registerCss('
        textarea{
            resize:none;
        }
    ');
?>