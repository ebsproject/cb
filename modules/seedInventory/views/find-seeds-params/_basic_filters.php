<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

/**
* View file creating the basic filters section
**/

use kartik\select2\Select2;

?>

<div>
    <?php
    if(isset($filters) && !empty($filters)){
        foreach ($filters as $filter) {
            if (isset($filter['value'])) { echo $filter['value']; }
        }
    }
    ?>
</div>