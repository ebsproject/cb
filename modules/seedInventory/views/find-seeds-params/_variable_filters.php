<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

/**
* View file creating the variable filters tab
**/

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
?>


<div class="row" style="margin-bottom:0px;">
    <div class="col-md-12" style="padding-right:0px;">
        <div class="basic-search-params" >
            <div id="notif_field" class="row text-darken-2" style = "margin-top:0px; margin-bottom:0px; margin-left:5px;"> 
                <span> Fill in at least 1 of the basic search parameters (<?php  echo ucwords(strtolower(str_replace('_',' ',implode(', ',$requiredFilters)))); ?>) OR use the Input List functionality below.</span>
            </div>
            <div class="progress add-basic-filters-loading-progress hidden" id="add-basic-filters-loading-progress" style="margin:0px;padding:0px;"><div class="indeterminate"></div></div>
            <div id="basic_filters_div" class="row" style="margin-top:0px;padding-top:0px; margin-bottom:0px; ">
                <?php
                    foreach ($filters as $filter) {
                        if (isset($filter['value'])) { echo $filter['value']; }
                    }
                ?>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom:0px; padding-right:0px;">
            <ul class="collapsible collapsible-add-filter" style="padding-right:0px;">
                <li class="additional-params" id="additional-params">
                    <div class="collapsible-header" id="additional-params-header"><i class="material-icons">list</i>Additional Search Parameters</div>
                    <div class="collapsible-body" id="additional-params">
                        <div class="row" style="margin-bottom:0px;">
                            <div class="panel panel-default additional-params-panel" id = "additional-params-panel" style="margin-bottom:5px; height:355px; padding-right:0px;">
                                <div class="progress add-filters-loading-progress hidden" id="add-filters-loading-progress" style="margin:0px;padding:0px;"><div class="indeterminate"></div></div>
                                <div class="row selected-variable-filters" id="selected-variable-filters" style="padding-left: 0px;margin-left: 10px;margin-bottom: 0px;">
                                    <ul id="additional-param-list">
                                    </ul>
                                </div>
                            </div>
                            <div class="input-list-filters hidden" id="input-list-filters" style="margin-left: 0px;padding-left: 10px;padding-right: 0px;">
                                <?php
                                    echo Yii::$app->controller->renderPartial('/find-seeds-params/_input_list_filter', [
                                            'id' => $id,
                                            'program' => $program,
                                            'filterValues' => $filterValues,
                                            'inputListFields' => $inputListFields,
                                            'inputListParamStatus' => true,
                                            'addFilterParamStatus' => false,
                                            'germplasmListDataProvider' => $germplasmListDataProvider
                                        ]);
                                ?>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:0px; margin-top:5px;">
                            <div class="params-option-btn">
                                <?= Html::a(\Yii::t('app','INPUT LIST'), '#!', 
                                    [
                                    'id' => 'show-list-params-btn',
                                    'class' => 'btn light-green darken-3 pull-left',
                                    'style' => 'margin-right:5px;',
                                    'title' => 'Input List parameters',
                                    'data-pjax' => true
                                    ]) 
                                ?>
                                <?= Html::a(\Yii::t('app','FILTERS'), '#', 
                                    [
                                    'id' => 'show-add-params-btn',
                                    'class' => 'btn light-green darken-3 pull-left dropdown-trigger dropdown-button-pjax',
                                    'style' => 'margin-right:5px;',
                                    'title' => 'Additional Parameter filters',
                                    'data-beloworigin' => 'false',
                                    'data-constrainwidth' => 'false',
                                    'data-alignment' => 'left',
                                    'data-activates'=>'dropdown-add-params',
                                    ]). '&emsp;'.
                                    "<ul id='dropdown-add-params' class='dropdown-content' style='width: 150px !important; margin-top:0px; margin-left:95px;' data-beloworigin='false' alignment: 'right'>
                                        ".
                                        implode('',array_map(function($key,$value){
                                            return '<li id="'.$key.'"><a href="#!" class="add-filter-option" id="'.$key.'">'.ucwords(strtolower($value)).'</a></li>';
                                        },array_keys($filterSelection),$filterSelection))
                                    ."</ul>"
                                ?>
                                <?php
                                    echo Html::input(
                                        'text',
                                        'filter-selection-hidden',
                                        '',
                                        [
                                            'id' => 'filter-selection-hidden',
                                            'class' => 'select2-filter-selection-hidden hidden',
                                        ]
                                    );

                                    echo Html::input(
                                        'text',
                                        'additional-filters-hidden',
                                        '',
                                        [
                                            'id' => 'additional-filters-hidden',
                                            'class' => 'select2-additional-filters-hidden hidden',
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                        
                    </div>
                </li>
            </ul>
        </div>   
    </div> 
</div>
<?php
    $findUrl = Url::to(['find',"program"=>$programAbbrev]);
    $resetQueryUrl = Url::to(['reset-query-param',"program"=>$programAbbrev]);
    $generateSelectedFiltersUrl = Url::home() . '/seedInventory/' . Url::to('find-seeds/generate');
    $getIdValuesUrl = Url::to(['find-seeds/get-id-values']);
    $saveQueryParamsUrl = Url::to(['save-query-params']);
    $getQueryParamsDataProviderUrl = Url::to(['get-query-params-data-provider']);
    $setQueryParamsUrl = Url::to(['get-param-values-by-id']);
    $getParamsByIdUrl = Url::to(['get-query-params-by-id']);
    $loadParamsToFormUrl = Url::to(['load-selected-params-to-filters']);
    $getExperimentOccurrenceUrl = Url::to(['get-experiment-occurrence-data']);
    $getGermplasmListDataUrl = Url::to(['get-germplasm-list']);
    
    $filterCount = count($filterValues);
    $dataValues = json_encode($filterValues);
    $selectionValues = json_encode($filterSelection);
    $inputListTypes = json_encode($inputListFields);

$script = <<<JS
    var userId=$id;

    $(document).ready(function() {

        var count = filterCount;
        var data =  dataValues;
        var additionalOptions = selectionValues;
        var program = program;

        var defaultFilter = '';
        var selectedFilters = [];
        var chips = [];
        var volumeValues = [];
        var volumeVal;

        var flag = 0;

        if(count > 0){
            refreshQueryParameterValues(data,additionalOptions, selectedFilters);

            var dataFilters = $('.find-seeds-filter').serializeArray();
            transitionAdditionalParameterSections(dataFilters,'both');
        }

        $(".find-loading-progress").addClass("hidden");
    });

    // loads pre-existing query parameter values
    function refreshQueryParameterValues(data,additionalOptions, selectedFilters){

        listTypes = (inputListTypes == null ? [] : Object.values(inputListTypes).map((x)=>{ return x.toUpperCase(); }));
        additionalFilters = [];
        
        $.each(data,function(key,value){
            flag=0;
            $.each(additionalOptions,function(key,val){
                if(key == value['abbrev']){
                    if(value['values'] != null) {
                        if(Array.isArray(value['values']) == true){
                            variableValues = value['values'].join(",");
                        }
                        else{
                            variableValues = value['values'];
                        }
                        selectedFilters.push(value['abbrev']);
                    }
                    else{
                        variableValues = "";
                    }

                    additionalFilters.push({"abbrev":value['abbrev'],"values":variableValues});
                    flag=1;
                    return false;
                }
            });

            if( flag==0 && listTypes.indexOf(value['abbrev']) == -1){
                filterValues = value['values'];
                filterAbbrev = value['abbrev'];

                $('#'+filterAbbrev.toLowerCase()+'-filter').val(filterValues).trigger('change');
                $('#'+filterAbbrev.toLowerCase()+'-filter-hidden').val(filterValues).trigger('change');
            }
            if(flag==0 && listTypes.indexOf(value['abbrev']) != -1){
                $('#show-list-params-btn').trigger('click');
            }

            $('#find-btn').attr('disabled',false);
        });
        
        if(selectedFilters != null && selectedFilters.length > 0){
            
            $('#filter-selection').val(selectedFilters[selectedFilters.length-1]).trigger('change');
            $('#filter-selection-hidden').val(selectedFilters[selectedFilters.length-1]).trigger('change');
            $('#additional-filters-hidden').val(','+selectedFilters.join(','));

            generateAdditionalFilters(additionalFilters,'all');
        }
    }

    // hide load query params modal
    $(document).on('click','#cancel-load-params-btn', function(e) {

        e.preventDefault();
        $('#open-query-params-modal').modal('hide');

    });

    // hide save query params modal
    $(document).on('click','#cancel-save-params-btn', function(e) {

        e.preventDefault();
        $('#query-info-modal').modal('hide');

    });
    
    // show additional params section
    $(document).on('click','#show-add-params-btn', function(e) {
        e.preventDefault();
        
        var data = $('.find-seeds-filter').serializeArray();
        transitionAdditionalParameterSections(data,'filters');
    });

    // show input list params section
    $(document).on('click','#show-list-params-btn', function(e) {
        e.preventDefault();

        var data = $('.find-seeds-filter').serializeArray();
        
        // retrieve germplasm list browser data provider
        url = '$getGermplasmListDataUrl';
        $.ajax({
            url:'$getGermplasmListDataUrl',
            type:'POST',
            data:{
                filterValues:data
            },
            success: function(response){
                $('#input-list-filters').html(response);
                transitionAdditionalParameterSections(data,'list');
            }
        });
    });

    // manage loading of relevant additional parameter sections
    function transitionAdditionalParameterSections(data,source){
        
        var hasInputList = checkForAdditionalParams(data,'list');
        var hasAdditionalFilters = checkForAdditionalParams(data,'filters');

        // has both input list and additional filters
        if( (source=='both' && hasInputList == true && hasAdditionalFilters == true) || 
            (source=='list' && hasAdditionalFilters == true) || (source=='filters' && hasInputList == true)){
            $('#no-list-layout').show();
            $('#no-list-layout').removeClass('hidden').trigger('change');
            
            $('#field-type-selector').removeClass('col-md-12').addClass('col-md-6').trigger('change');
            $('#has-list-layout').removeClass('col-md-6').addClass('col-md-6').trigger('change');
            $("#input-area").css('height',300);

            $('#input-text-area-2').removeClass('col-md-6').addClass('col-md-6').trigger('change');
            $('#input-text-area-2').removeClass('hidden').trigger('change');

            $('#input-list-filters').removeClass('hidden').trigger('change');
            $('#input-list-filters').show();

            updateInputListFilterValues();

            if(!hasAdditionalFilters && hasInputList && source == 'list'){
                $('#additional-params-panel').removeClass('hidden').trigger('change');
                $('#additional-params-panel').show();
            }

        }
        else if(hasInputList == false && (source == 'filters' || source == 'both')){ // only has additional filters, show only additional filters
            $('#input-list-filters').hide();
            $('#input-list-filters').addClass('hidden').trigger('change');

            $('#additional-params-panel').removeClass('col-md-6').addClass('col-md-12').trigger('change');

            updateInputListFilterValues();

            $('#additional-params-panel').removeClass('hidden').trigger('change');
            $('#additional-params-panel').show();

        }
        else if(hasAdditionalFilters == false && (source == 'list' || source == 'both')){ // only has input list, show only input list
            $('#input-text-area-2').removeClass('hidden').trigger('change');

            $('#show-list-btn').hide();
            $('#show-list-btn').addClass('hidden').trigger('change');
            $('#show-list-btn').removeClass('col-md-1').trigger('change');

            $('#has-list-layout').show();
            $('#has-list-layout').removeClass('hidden').trigger('change');
            
            $('#no-list-layout').show();
            $('#no-list-layout').removeClass('hidden').trigger('change');
            $('#field-type-selector').removeClass('col-md-11').trigger('change');
            $('#field-type-selector').addClass('col-md-12').trigger('change');
            
            $('#input-text-area-2').removeClass('col-md-12').addClass('col-md-6').trigger('change');
            $('#has-list-layout').removeClass('col-md-12').addClass('col-md-6').trigger('change');

            $("#input-area").css('height',300);

            $("#dynagrid-open-list").find('.kv-grid-wrapper').css('height',205);
            $("#dynagrid-open-list-container").css('height',205);
            $("#dynagrid-open-list").find('.kv-panel-pager').addClass('hidden').trigger('change');
            $("#dynagrid-open-list").find('.kv-panel-before').addClass('hidden').trigger('change');

            $('#additional-params-panel').hide();
            $('#additional-params-panel').addClass('hidden').trigger('change');

            $('#input-list-filters').removeClass('col-md-6').addClass('col-md-12').trigger('change');
            $('#input-list-filters').removeClass('hidden').trigger('change');
            $('#input-list-filters').show();    
        }
    }
    
    // check if there are additional parameters selected
    function checkForAdditionalParams(data,source){

        var additionalFilters = retrieveAdditionalFilterValues();
        var inputListValue = "", inputListType = "", hasAdditionalFilter = false;
        var listTypes = ( inputListTypes['fieldTypes'] == null ? [] : Object.values(inputListTypes['fieldTypes']));
        var flag = false
        additionalFilter = '';
        $.each(data,function(i,v){
            if(v['name'] == "filter[INPUT_VALUE]"){ inputListValue = v['value']; }
            if(v['name'] == "filter[INPUT_FIELD]"){ inputListType = v['value']; }
            
            if(v['name'] == "filter-selection-hidden" && v['value'] != ""){ 
                additionalFilter = v['value'];
                return true;
            }
        });

        $.each(data,function(i,v){
            if(additionalFilter != '' && v['name'] == "filter["+additionalFilter+"][]" && v['value'] != ""){ 
                hasAdditionalFilter = true;
            }
        });
        
        var existingParams = false;
        if(hasAdditionalFilter == true && additionalFilters.length > 0){
            existingParams = true;
        }

        if(source == 'list' && inputListValue == "" && inputListType != ""){
            $('#field-type-selection-2').val('empty').trigger('change');
            $('#field-type-selection').val('empty').trigger('change');
            $('#field-type-selection-hidden').val('').trigger('change');
        }

        if(source == 'list' && inputListValue != "" && inputListType == ""){
            $('#input-area').val('').trigger('change');
            $('#input-value-hidden').val('').trigger('change');
            document.getElementById("find-btn").disabled = true;
        }
        
        return existingParams;
    }

    $(document).on('click','.add-filter-option', function(e) {

        e.preventDefault();

        $(".add-filters-loading-progress").removeClass("hidden");
        
        var newFilter = $(this).attr('id');
        var additionalFilters = $('#additional-filters-hidden').val();
        var currentAdditionalParams = additionalFilters.split(',');
        
        if(currentAdditionalParams.indexOf(newFilter) === -1){
            var additionalFilterValues = retrieveAdditionalFilterValues();
            
            $('#filter-selection-hidden').val(newFilter).trigger('change');
            $('#additional-filters-hidden').val(additionalFilters+','+newFilter).trigger('change');
            
            if( $("#filter-selection-hidden").val() != "" ){
                generateAdditionalFilters(additionalFilterValues,'last');
                $('#show-add-params-btn').trigger('click');
            }
        }
        else{
            $(".add-filters-loading-progress").addClass("hidden");
        }
    });

    // trigger for input field value validation
    $(document).on('change','.input-filter',function(e) {
        var element = $(this).attr('id');
        var currValue = $('#'+element).val();
        var filterId = (''+$(this).attr('id')).split('-')[0];
        var valueCheck = {'cleanValues':true, 'error':''};

        valueCheck = inputValueValidation(currValue,filterId);
        if(valueCheck['cleanValues'] == true){
            $('#'+element+'-hidden').val($(this).val());
        }
    });

    // check and add selected harvest date
    $(document).on('change','#hvdate_cont-filter-selected', function(e) {

        var value = $(this).val();
        if(value != ""){
            var validation = checkDateValues($(this).val());

            if(validation['cleanValues'] == false){
                $('#hvdate_cont-filter-selected-hidden').val('');
                $('#hvdate_cont-filter-selected').val('');
            }
            else{
                $('#hvdate_cont-filter-selected-hidden').val($(this).val());
                document.getElementById("find-btn").disabled = false;
            }
        }
    });

    // date validation
    function checkDateValues(dateValue){
        date = dateValue.split('-');
        error = '';
        validYear = true, validMonth = true, validDay = true;

        startYear = 1902;
        endYear = new Date().getFullYear();
        if(date.length == 3){
            
            validYear = (Number.isNaN(Number(date[0])) == false && date[0] <= endYear &&  date[0] >= startYear) ? true : false;
            validMonth = (Number.isNaN(Number(date[1])) == false && date[1] <= 12 &&  date[1] >= 0) ? true : false;
            validDay = (Number.isNaN(Number(date[2])) == false && date[2] <= 31 &&  date[2] >= 0) ? true : false;

            if(validYear == true && validMonth == true && validDay == true){
                return {'cleanValues':true,'error':''};
            }
            else{
                return {'cleanValues':false,'error':'Error in date format.'};
            }
        }
        else{
            return {'cleanValues':false,'error':'Error in date format.'};
        }
    }

    // update volume filter value
    $(document).on('change','#volume-filter-selected-value', function(e) {
        var value = $('#volume-filter-selected-value').val();
        var validation = inputValueValidation(value,'volume');
        
        if(validation['cleanValues'] == true){
            if($('#volume-filter-selected').val() == "" || $('#volume-filter-selected').val() == ''){
                $('#volume-filter-selected-hidden').val("");
            }
            else{
                $('#volume-filter-selected-hidden').val($('#volume-filter-selected').val()+'/'+value);
            }  
        }
        else{
            $('#volume-filter-selected-hidden').val("");
        }
    });

    // update volume filter condition
    $(document).on('change','#volume-filter-selected', function(e) {

    var value = $('#volume-filter-selected-value').val();
    var validation;

    if($('#volume-filter-selected').val() == "" || $('#volume-filter-selected').val() == null || $('#volume-filter-selected-value').val() == "" || $('#volume-filter-selected-value').val() == null){
        $('#volume-filter-selected-hidden').val("");
    }

    if(!($('#volume-filter-selected-value').val() < 0)){
        validation = inputValueValidation(value,'volume');

        if(validation['cleanValues'] == true){
            $('#volume-filter-selected-hidden').val($('#volume-filter-selected').val()+'/'+value); 
        }
        else{
            $('#volume-filter-selected-hidden').val("");
        }
    }        
    });

    //valdation for input field values 
    function inputValueValidation(data,filterId){
        var inputValue = data;
        var multiValues = data.split(',');
        var rangeValues, multiValCheckFlag = true; 
        
        var delimiter = (inputValue == "" ? '':',');

        var rangeCheck = {'cleanValues':true, 'error':''};
        var formatCheck = {'cleanValues':true, 'error':''};
        var numericCheck = {'cleanValues':true, 'error':''};
        var multiValCheck = {'cleanValues':true, 'error':''};
        var clearCheck = {'cleanValues':true, 'error':''};

        var errorMessage = "";
        if((filterId == 'entno' || filterId == 'entno' || filterId == 'rep' || filterId == 'plotno' || filterId == 'volume') && data != ""){
            
            if(multiValues.length > 0){
                $.each(multiValues,function(index,value){
                    
                    if(value != ""){
                        rangeValues = value.split('-');
                        if(rangeValues.length >1 && value.includes('-') && filterId != 'volume'){
                            rangeCheck = validateRangeValues(value);
                            if(rangeCheck['cleanValues'] == false){
                                errorMessage = (errorMessage.includes(rangeCheck['error'])==true) ? errorMessage : errorMessage+" "+rangeCheck['error'];
                            }
                        }
                        else{
                            formatCheck = checkForSpecialCharacters(value);
                            if(formatCheck['cleanValues'] == false){
                                errorMessage = (errorMessage.includes(formatCheck['error'])==true) ? errorMessage : errorMessage+" "+formatCheck['error'];
                            }

                            numericCheck = checkForNumericValue(value.trim());
                            if(numericCheck['cleanValues'] == false){
                                errorMessage = (errorMessage.includes(numericCheck['error'])==true) ? errorMessage : errorMessage+" "+numericCheck['error'];
                            }
                        }
                        
                        if(rangeCheck['cleanValues'] == false || formatCheck['cleanValues'] == false || numericCheck['cleanValues'] == false){
                            multiValCheckFlag = (multiValCheckFlag && false);
                        }
                    }         
                });
            }
            if(errorMessage != ""){
                showMessage('<span>Invalid input value. '+errorMessage+'</span>','error');
            }
            return (multiValCheckFlag === false ? {'cleanValues':false, 'error':'Invalid input values.'} : clearCheck);
        }
        else{
            return clearCheck;
        }
    }

    // retrieves additional filter values
    function retrieveAdditionalFilterValues(){

        var currAdditionalFilterValues = $('.find-seeds-filter').serializeArray();
        var selectionOptions = selectionValues;
        var filterName, duplicate,additionalFilterValues=[];

        $.each(currAdditionalFilterValues,function(key,value){
            if(value.name != "_csrf" && value.name != "filter-selection[]" && value.name != "filter-selection-hidden" && value.name != "field-type-selection"){
                
                if( value.name != null && value.name.includes('[') && value.name.includes(']') ){
                    filterName = (''+value.name.split('[')[1]).split(']')[0];
                    filterValue = value.value;
                    duplicate = false;
                    
                    $.each(selectionOptions,function(optionKey,optionValue){

                        if(optionKey == filterName){
                            
                            $.each(additionalFilterValues,function(valuesKey,valuesValue){
                                if(valuesValue.abbrev == optionKey){
                                    duplicate = true;
                                    additionalFilterValues[valuesKey].values = filterValue;
                                    return 0;
                                }
                            })
                            if(duplicate != true){
                                additionalFilterValues.push({'abbrev':optionKey,'values':filterValue});
                            }
                            return 0;
                        }
                    })
                }
            }
        });
        
        return additionalFilterValues;
    }

    // renders selected additional filters
    function generateAdditionalFilters(filterValues,mode){
        $(".add-filters-loading-progress").removeClass("hidden");

        var data = $('#filter-selection-hidden').val();
        var additionalFilters = $('#additional-filters-hidden').val();
        var values = filterValues;
        
        var splitAdditionalFilters = additionalFilters.split(',');
        var filtersToAdd = null;
        if(mode == 'last'){
            filtersToAdd = [splitAdditionalFilters[splitAdditionalFilters.length-1]];
        }

        if(mode == 'all'){
            filtersToAdd = splitAdditionalFilters.filter(item => item);
        }
        
        if(filtersToAdd == null || filtersToAdd.length == 0){
            $('#additional-param-list').empty()
        }
        
        if(filtersToAdd.length > 0){
            for(addFilter of filtersToAdd){
                $.ajax({
                    url:'$generateSelectedFiltersUrl',
                    type: 'post',
                    data: {
                        filterSelection:data,
                        queryValues:values,
                        selectedFilters:addFilter
                    },
                    success: function(response){
                        if(response != null && response != [] && response.length > 0){
                            var data = JSON.parse(response);

                            var splitValues = data.elementId.split(',');
                            var elementId = splitValues[splitValues.length-1].toLowerCase()+'-filter-li';
                            $('#additional-param-list').append($('<li id="'+elementId+'">').html(data.elementField));
                        }
                        else{
                            showMessage('Error rendering additional filter.','error');
                        }
                        
                    }
                });
            }
        }

        $(".add-filters-loading-progress").addClass("hidden");
    }

    //removes additional filter
    $(document).on('click','.remove-btn', function(e) {

        event.preventDefault();
    //    TODO: REFACTOR
        var filterId = (''+$(this).attr('id')).split("-")[0];
        var hiddenValue = $('#filter-selection-hidden').val();
        var item = document.getElementById(filterId);
        var additionalFilters = $('#additional-filters-hidden').val();
        
        if(additionalFilters.includes(filterId.toUpperCase()) == true){
            additionalFilters = additionalFilters.replace(','+filterId.toUpperCase(),'');
            $('#additional-filters-hidden').val(additionalFilters).trigger('change');
        }

        if(hiddenValue == filterId.toUpperCase()){
            newSelection = additionalFilters.split(',');
            $('#filter-selection').val(newSelection[newSelection.length-1]).trigger('change');
            $('#filter-selection-hidden').val(newSelection[newSelection.length-1]).trigger('change');
        }

        var elementId = $(this).closest('button').attr('id');
        var splitValue = elementId.split('-');
        var listItemId = splitValue[0]+'-filter-li';

        var elementToRemove = document.getElementById(listItemId);
        elementToRemove.parentNode.removeChild(elementToRemove);

        var lis = document.getElementById("additional-param-list").getElementsByTagName("li");
        var itemIdsArr = [];
        var idValue = '';


        for (li of lis) {
            idValue = li.getAttribute("id")
            if(idValue){
                itemIdsArr.push(idValue.split('-')[0].toUpperCase())
            }
        }

        idValue = itemIdsArr.join(',')
        $('#additional-filters-hidden').val(idValue).trigger('change');
    });

    // validate if input is valid range values
    function validateRangeValues(values){

    var rangeValues;
    var output = {'cleanValues':true,'error':''};        

    if(values.includes('-')){
        rangeValues = values.split("-");

        if(rangeValues.length > 2){
            output = {'cleanValues':false, 'error':'Requires 2 numeric range values.'};
        }

        if(Number.isNaN(Number(rangeValues[0])) == true || Number.isNaN(Number(rangeValues[1])) == true){
            output = {'cleanValues':false,'error':'Requires numeric range values.'};
        }
    }
    return output;
    }

    // validate if input has special characters
    function checkForSpecialCharacters(value){

    var format = /[!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?]+/;
    var output = {'cleanValues':true,'error':''}; 

    if(format.test(value) == true){
        output = {'cleanValues':false,'error':"Requires '-' as value separator."};
    }

    return output;
    }

    // validate if input is valid nuemric value
    function checkForNumericValue(value){

    var output = {'cleanValues':true,'error':''};
    var postgresMaxInt = 2147483647; 

    if((Number(value) < 0) || (Number.isNaN(Number(value)) == true)){
        output = {'cleanValues':false, 'error':'Requires valid numeric value.'};
    }
    if(Number(value) > postgresMaxInt){
        output = {'cleanValues':false, 'error':'Exceeded acceptable integer value.'};
    }
    return output;
    }

    $(document).on('change','#experiment_name-filter-selected', function(e) {
        var value = $(this).val();
        if( value != null &&  value != ""){
            var experimentId = value.join(',');
        
            $.ajax({
                async: true,
                url: '$getExperimentOccurrenceUrl',
                type: 'post',
                dataType: 'json',
                data: {
                    experimentId:experimentId,
                },
                success: function(response){
                    var count = parseInt(response['count']);
                    var data = response['data'];

                    if(count > 0){
                        var ids = data.map(function(e) {
                            return JSON.stringify(e.id);
                        });
                        
                        $('#occurrence_name-filter').select2({
                            data: data,
                            width: '100%'
                            });
                        $('#occurrence_name-filter').val(ids).trigger('change');
                        $('#occurrence_name-filter-hidden').val(ids.join(',')).trigger('change');

                        $('#occurrence_name-filter').parent().addClass('select2-container--krajee');
                        $('#occurrence_name-filter').parent().width('100%');
                    }
                }
            });
        }
        else{
            var occurrenceValue = $('#occurrence_name-filter').val();
            if(occurrenceValue != null && occurrenceValue != ""){
                $('#occurrence_name-filter').val("").trigger('change');
            }
        }
        
    });

JS;

$this->registerJS('var filterCount = '.$filterCount.', dataValues = '.$dataValues.', selectionValues = '.$selectionValues.', program = '.$program.', inputListTypes='.$inputListTypes.';'.$script);

// Sets placement of Toast on page
$this->registerCss('
    .basic-search-params {
        overflow: auto;
    }

    .additional-params-panel {
        overflow: auto;
    }

    .add-filter-remove-btn {
        margin-bottom:5px; 
        padding-left:15px; 
        padding-right:0px;
    }
');
?>