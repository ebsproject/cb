<?php
/*
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

/**
* View file creating the selected variable filters section
**/

use yii\helpers\Url;

?>

<div class="row"> 
    <ul id="additional-param2-list">
    <?php
        foreach($filters as $index=>$filter){
            if(isset($filter['value'])){ echo '<li>'.$filter['value'].'</li>'; };
        }
    ?>
    </ul>
</div>

<?php

    $values = json_encode($filterValues);
    $filterHtml = json_encode($filters);

    $getSelectedFiltersUrl = Url::to(['get-selected-filters']);

$script = <<<JS

    var hasFilter = false;
    var chips = [];

    // initializes datepicker
    $('.datepicker-filter').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    // set previously selected filter values
    $(document).ready(function() {
        updateAdditionalFilters();
    })

    // Renders Toast message
    function showMessage(message,type){

        var output = '';
        var icon = '';

        if(type == 'success'){
            
            icon = "<i class='material-icons green-text'>check</i>&nbsp; ";
        }
        else if(type == 'error'){
            icon = "<i class='material-icons red-text'>clear</i>&nbsp; ";
        }
        else if(type == 'warning'){
            icon = "<i class='material-icons yellow-text'>warning</i>&nbsp; ";
        }
        else if(type == 'message'){
            icon = "<i class='material-icons white-text'>info</i>&nbsp; ";
        }
        output = icon.concat(message);
        $('.toast').css('display','none');
        Materialize.toast(output, 5000);
    }
    
    // update filter values
    function updateAdditionalFilters(){

        inputFieldFilters = ['PLOTNO','ENTNO','ENTCODE','PLOT_CODE','REP'];

        $.each(filterValues,function(index,value){
            if(value.abbrev == 'VOLUME'){
                inputValue = Array.isArray(value['values']) == true ? value['values'][0] : value['values'];
                if(inputValue.indexOf('/') != -1){
                    volumeValue = inputValue.split("/")[1];
                }
                else{
                    volumeValue = inputValue.split(" ");
                    volumeValue = (volumeValue.length > 2 ? volumeValue[volumeValue.length-1] : volumeValue[1]);
                }
                $('#volume-filter-selected-value').val(volumeValue).trigger('change');
            }

            if(inputFieldFilters.indexOf(value.abbrev) != -1){
                $('#'+value['abbrev'].toLowerCase()+'-filter-selected').val(value['values']).trigger('change');
            }
        });
    }

JS;

$this->registerJS('var filterValues = '.$values.', filterHtml = '.$filterHtml.'; '.$script);
?>