<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Add to Experiment modal content
 **/

use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use yii\web\JqueryAsset;

?>

<div>
    <?php
        $notif = '<div id="invalid-experiment-select" class="red-text text-darken-2" style = "display:none">No experiment selected!</div>';

        $experimentDataProvider = $experimentDataProvider;
        $dataCount = ($experimentDataProvider == null || $experimentDataProvider->allModels == null ? 0 : count($experimentDataProvider->allModels));

        echo $notif . Html::input(
            'text',
            'dataType',
            $dataType,
            [
                'id' => 'dataType-hidden',
                'class' => 'input-text-hidden hidden variable-filter input-text-filter'

            ]
        ) . Html::input(
            'text',
            'dataCount',
            $dataCount,
            [
                'id' => 'data-count-hidden',
                'class' => 'input-text-hidden hidden data-count'

            ]
        );

        $actionColumns = [
            [
                'label' => "Radio Button",
                'mergeHeader' => true,
                'header' => '<button type="button" class="close kv-clear-radio transparent hidden" title="Clear selection" style="margin-left:10px;" id="select-experiment">×</button>',
                'headerOptions'=>['class'=>'radio-btn-row'],
                'content' => function ($model) {
                        return '
                        <input class="selected-experiment-select-radio saved-experiment-select-radio filled-in with-gap" name="experiment-radio-tn-grp" type="radio" id="saved-experiment-item-' . $model["experimentDbId"] . '" />
                        <label for="saved-experiment-item-' . $model["experimentDbId"] . '" id="label-for-radio" style="padding-left: 20px;" ></label>';

                },
                'width' => '1%',
            ],
        ];

        $columns = [
            [
                'attribute'=>'experimentName',
                'label' => Yii::t('app', 'Experiment'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentType',
                'label' => Yii::t('app', 'Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'occurrenceCount',
                'label' => Yii::t('app', 'Occurrence Count'),
                'value' => function ($data) {
                  return '<span class="new badge">'.$data['occurrenceCount'].'</span>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'entryCount',
                'label' => Yii::t('app', 'Entry Count'),
                'value' => function ($data) {
                  return '<span class="new badge">'.$data['entryCount'].'</span>';
                },
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'projectName',
                'label' => Yii::t('app', 'Project'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'seasonName',
                'label' => Yii::t('app', 'Season'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'stageCode',
                'label' => Yii::t('app', 'Stage'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentYear',
                'label' => Yii::t('app', 'Year'),
                'format' => 'raw',
                'enableSorting' => false,
            ],
            [
                'attribute'=>'experimentDesignType',
                'label' => Yii::t('app', 'Design Type'),
                'format' => 'raw',
                'enableSorting' => false,
            ],

        ];
        $gridColumns = array_merge($actionColumns, $columns);

        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme' => 'panel-default',
            'showPersonalize' => true,
            'storage' => 'cookie',
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'dynagrid-open-experiment',
                'dataProvider' => $experimentDataProvider,
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'dynagrid-open-experiment',
                    ],
                ],
                'panel' => [
                    'before' => \Yii::t('app', 'Select an experiment here.'),
                    'after' => false,
                ],
                'toolbar' => '',
            ],
            'options' => ['id'=>'dynagrid-open-experiment'],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => 'Remove',
            ],
        ]);
        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }
        DynaGrid::end();
    ?>
</div>