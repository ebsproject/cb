<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file creating the basic info of a list
 **/

use app\models\PlatformList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$required = '<span style="color: red">*</span>';

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'create-list-form',
    'fieldConfig' => function ($model, $attribute) {
        if (in_array($attribute, ['abbrev', 'display_name', 'name', 'type'])) {
            return ['options' => ['class' => 'input-field form-group col-md-6']];
        } else {
            return ['options' => ['class' => 'input-field form-group col-md-12']];
        }
    },
]);

$model = \Yii::$container->get('app\models\PlatformList');
$entity = 'package';

$hidden = '</div><span>' . Html::tag('h5', '', ['class' => 'item-count-hidden', 'id' => 'item-count-hidden']) . '</span>';
$progress = '<div class="progress save-list-loading-progress hidden" id="save-list-loading-progress" style="margin:0px;padding:0px;"><div class="indeterminate"></div></div>';
echo '' . $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'id' => 'list-name',
    'class' => 'form-control',
    'onkeyup' => 'js:
        var nameValue = this.value;
        $("#list-display-name").val(nameValue).trigger("change");
        $("#list-abbrev").val(nameValue.replace(/[^A-Z0-9]/ig, "_").toUpperCase()).trigger("change");
    '
]) .
$form->field($model, 'abbrev')->textInput([
    'maxlength' => true,
    'id' => 'list-abbrev',
    'class' => 'form-control',
    'readonly' => true,
    'onkeyup' => 'js: $("#list-abbrev").val(this.value.charAt(0).toUpperCase() + this.value.slice(1));',
]) .
$form->field($model, 'display_name')->textInput([
    'maxlength' => true,
    'class' => 'form-control',
    'id' => 'list-display-name',
]) .
$form->field($model, 'type')->textInput([
    'maxlength' => true,
    'id' => 'list-type',
    'class' => 'form-control',
    'readonly' => true,
    'value' => $entity,
]) .
$form->field($model, 'description')->textarea([
    'class' => 'materialize-textarea',
    'title' => 'Description about the list',
    'id' => 'entry-list-description',
]) .

$form->field($model, 'remarks')->textarea([
    'class' => 'materialize-textarea',
    'title' => 'Additional details about the list',
    'row' => 2,
    'id' => 'entry-list-remarks',
]) . $hidden . $progress;

ActiveForm::end();

$script = <<<JS

JS;

$this->registerJs($script);
