<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View file creating the seed search list view
 **/

use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\JqueryAsset;

use yii\helpers\Url;
use app\components\ManageSortsWidget;

use app\models\UserDashboardConfig;

\yii\jui\JuiAsset::register($this);

?>
<?php
    $actionColumns = [
        [
            'class' => 'app\components\B4RCheckboxColumn',
            'checkboxOptions' => function ($model) {
                $listItemId = $model["listMemberDbId"];
                return [
                    'value' => $listItemId,
                    'id' => $listItemId,
                    'class'=> 'seed-inventory-seed-list-select',
                ];
            },
            'order' => DynaGrid::ORDER_FIX_LEFT,
            'hiddenFromExport' => true,
            'width' => '1%',
            'hidden' => false,
            'visible' => true,
            'selectAllId' => 'select-seed-list'
        ],
        [
            'attribute' => 'order_number',
            'label'=>'',
            'filter' => false,
            'format' => 'raw',
            'contentOptions' => function($data) {
                return ['class' => 'sort_order_arr', "data-id"=>$data['listMemberDbId']];
            },
            'value' => function($data){
                return $data['orderNumber'];
            },
            'width' => '1%',
            'visible'=>true,
            'hidden'=>false
        ],
        
    ];
    $columns=[];
    $listHeaders = ['DESIGNATION','GID','LABEL'];

    $resetGridUrl = Url::to(['index', 'program' => $programAbbrev]);
    $dataBrowserModel = \Yii::$container->get('\app\modules\seedInventory\models\FindSeedDataBrowserModel');
    $columnConfig= $dataBrowserModel->getColumns();

    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';

    $attribDataColCompat = [
        'gid' => 'GID',
        'src_entry' => 'sourceEntryCode',
        'source_entry' => 'seedSourceEntryNumber',
        'src_plot' => 'seedSourcePlotNumber',
        'plot_code' => 'seedSourcePlotCode',
        'experiment_name' => 'experiment',
        'occurrence_name' => 'experimentOccurrence',
        'season' => 'sourceStudySeason',
        'evaluation_stage' => 'experimentStage'
    ];
    
    foreach ($columnConfig as $col) {
        $header_name = $col['field_label'];     
        $variableAbbrev = strtolower($col['variable_abbrev']);
        $colAbbrev = isset($attribDataColCompat[$variableAbbrev]) && !empty($attribDataColCompat[$variableAbbrev]) ? $attribDataColCompat[$variableAbbrev] : $variableAbbrev;
        
        if($header_name == 'SEED'){
            $header_name = 'SEED CODE';
        }

        if($header_name == 'GERMPLASM'){
            $header_name = 'DESIGNATION';
        }
        
        $visible = in_array($col['variable_abbrev'],$listHeaders) == true ? true : false;
        $tooltip = (isset($col['field_description'])) ? $col['field_description'] : $col['field_label'];
        $label = '<span class="tooltipped" data-position="top" title="'.$tooltip.'">'.$header_name.'</span>';
        
        $columns[]=[
            'attribute' => $colAbbrev,
            'label' =>  $label,
            'encodeLabel' => false,
            'visible'=> $visible,
            'format' => 'raw',
            'value' => function($model) use ($col, $colAbbrev){
                if (isset($model[$col['api_body_param']]) && 
                    $col['api_body_param'] != null &&
                    !empty($col['api_body_param'])) {
                    return $model[$col['api_body_param']];
                } else if (isset($model[$colAbbrev])) {
                    return $model[$colAbbrev];
                } else { return null; }
            },
        ];
    }

    $gridColumns = array_merge($actionColumns, $columns);

    $dynagrid = DynaGrid::begin([
        'columns' => $gridColumns,
        'theme' => 'panel-default',
        'showPersonalize' => true,
        'storage' => DynaGrid::TYPE_SESSION,
        'showFilter' => false,
        'showSort' => false,
        'allowFilterSetting' => false,
        'allowSortSetting' => false,
        'gridOptions' => [
            'tableOptions'=>[
                'class'=>'dynagrid-find-seed-list-table'
            ],
            'options'=>[
                'id' => 'dynagrid-find-seed-list',
            ],
            'id' => 'dynagrid-find-seed-list',
            'striped'=>false,
            'hover' => true,
            'dataProvider' => $listDataProvider,
            'floatHeader' =>true,
            'floatOverflowContainer'=> true,
            'showPageSummary' => false,
            'responsive' => true,
            'responsiveWrap' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'id' => 'dynagrid-find-seed-list',
                ],
            ],
            'panel' => [
                'heading' => false,
                'before' => '<p class = "pull-left" style = "margin: -1px;">'.\Yii::t('app', 'Manage your working list.') .'</p>'. ' {summary}' ,
                'after' => false,
            ],
            'toolbar' => [
                [
                    'content'=> 
                        Html::a(
                            '<i class="material-icons right" style="margin-left:0px;">add</i>', 
                            '#',
                            [
                                'data-pjax' => true,
                                'class' => 'btn btn-default light-green darken-3',
                                'style' => 'margin-right: 0px;',
                                'id' => 'open-add-to-experiment-modal-btn',
                                'title'=>\Yii::t('app','Add to Experiment')
                            ])
                        .
                        Html::a(
                            '<i class="material-icons right" style="margin-left:0px;">save</i>', 
                            '#', 
                            [
                                'data-pjax' => true,
                                'class' => 'btn btn-default light-green darken-3',
                                'style' => 'margin-right:5px;',
                                'id' => 'save-list-btn', 
                                'title'=>\Yii::t('app','Save list')
                            ])
                        .
                        Html::a(
                            '<i class="material-icons right" style="margin-left:0px;">delete</i>', 
                            '#!', 
                            [
                                'class' => 'btn btn-default dropdown-trigger dropdown-button-pjax', 
                                'id' => 'remove-items-options-btn',
                                'title'=>\Yii::t('app','Remove item options'),
                                'data-beloworigin' => 'true',
                                'data-constrainwidth' => 'false',
                                'data-activates'=>'dropdown-remove-items-options'
                            ]) 
                        .'&emsp;'
                        .
                        "<ul id='dropdown-remove-items-options' class='dropdown-content' style='width: 180px !important; margin-top:5px; margin-left:5px;' data-beloworigin='false'>
                            <li><a href='#!' id='remove-list-btn' class='remove_items'>Remove Selected</a></li>
                            <li><a href='#!' id='confirm-clear-list' class='remove_items'>Clear Working List</a></li>
                        </ul>"
                        .
                        Html::a(
                            '<i class="material-icons right" style="margin-left:0px;">playlist_add</i>', 
                            '#', 
                            [
                                'data-pjax' => true,
                                'class' => 'btn btn-default', 
                                'style' => 'margin-left:5px;',
                                'id' => 'open-list-btn',
                                'title'=>\Yii::t('app','Import items from saved list'),
                            ]) 
                        .
                        Html::a(
                            '<i class="material-icons right" style="margin-left:0px;">download</i>', 
                            null, 
                            [
                                'data-pjax' => true,
                                'class' => 'btn btn-default',
                                'style' => 'margin-right:5px;',
                                'id' => 'export-working-list-btn', 
                                'title' => \Yii::t('app','Export all data in list'),
                            ])
                        . 
                        ManageSortsWidget::widget([
                            'dataBrowserId' => 'dynagrid-find-seed-list',
                            'gridId' => 'dynagrid-find-seed-list',
                            'searchModel' => 'WorkingList',
                            'btnClass' => 'sort-action-button',
                            'resetGridUrl' => $resetGridUrl.'&reset=hard_reset[WorkingList]'
                        ])
                        .'{dynagrid}'
                ],
                
            ],
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ],
        ],
        'options' => [
            'id'=>'dynagrid-find-seed-list'
        ],
        'submitButtonOptions' => [
            'icon' => 'glyphicon glyphicon-ok',
        ],
        'deleteButtonOptions' => [
            'icon' => 'glyphicon glyphicon-remove',
            'label' => 'Remove',
        ],
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = false;
    }
    DynaGrid::end();
?>

<?php
$reorderUrl = \yii\helpers\Url::to(['list-management/reorder-list']);
$findUrl = \yii\helpers\Url::to(['find',"program"=>$programAbbrev]);
$indexUrl = \yii\helpers\Url::to(['',"program"=>$programAbbrev]);
$exportWorkingListUrl = \yii\helpers\Url::to(['export-working-list']);

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');

// Check if browser Id is empty (occurrence tab)
$browserId = empty($browserId) ? 'none' : $browserId;

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = ". json_encode($browserId) .",
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$script = <<<JS

    var count = 0;
    let thresholdValue = '$thresholdValue'
    $(document).on('submit', '#dynagrid-find-seed-list-grid-modal', function(event) {        
        $(this).find("form").attr('action',document.URL);
    })
    $(document).ready(function() {
        $('.dropdown-trigger').dropdown();

        $(document).on('ready pjax:success', function(e) {
            count = 0;
            count = $("input:checkbox.seed-inventory-seed-list-select").length;
            
            if(count==0){
                $('#remove-list-btn').prop('disabled',true);
                $('#remove-list-btn').attr('disabled',true);

                $('#clear-working-list').prop('disabled',true);
                $('#clear-working-list').attr('disabled',true);
            }
            else{
                $('#remove-list-btn').prop('disabled',false);
                $('#remove-list-btn').attr('disabled',false);

                $('#clear-working-list').prop('disabled',false);
                $('#clear-working-list').attr('disabled',false);
            }

            renderReorder();

            // remove loading indicator upon export
            $('*[class^="export-full-"]').on('click',function(){
                $('#system-loading-indicator').css('display','none');
            });

            e.stopPropagation();
            e.preventDefault();
        });

        $(document).on('ready pjax:complete','#dynagrid-find-seed-list-pjax', function(e) {
            e.stopPropagation();
            e.preventDefault();
            
            $('.dropdown-trigger').dropdown();

            count = 0;
            count = $("input:checkbox.seed-inventory-seed-list-select").length;
            if(count==0){
                $('#remove-list-btn').prop('disabled',true);
                $('#remove-list-btn').attr('disabled',true);

                $('#clear-working-list').prop('disabled',true);
                $('#clear-working-list').attr('disabled',true);
            }
            else{
                $('#remove-list-btn').prop('disabled',false);
                $('#remove-list-btn').attr('disabled',false);

                $('#clear-working-list').prop('disabled',false);
                $('#clear-working-list').attr('disabled',false);
            }

            $("#search-grid-table-con-container").css('height',window.innerHeight-180);
            $("#dynagrid-find-seed-list-container").css('height',window.innerHeight-180);
            $(".find-seeds-list").css('height',window.innerHeight-180);
        });

        renderReorder();
        // select all variables
        $(document).on('click','#select-seed-list', function(e){
            var ids;
            count = 0;
            var checkedItems = retrieveCheckedItemsWL();
            checkItemCount(checkedItems.length);
        });
        
        function checkItemCount(count) {
            if(count > 0){
                $('#remove-list-btn').prop('disabled',false);
                $('#remove-list-btn').attr('disabled',false);
            }else{
                $('#remove-list-btn').prop('disabled',true);
                $('#remove-list-btn').attr('disabled',true);
            }
        }
        $(document).on('click', "#dynagrid-find-seed-list tbody tr:not(a)",function(e){ 
            var checkedItems = retrieveCheckedItemsWL();
            if(checkedItems == [] || checkedItems.length == 0){
                $('#remove-list-btn').prop('disabled',true);
                $('#remove-list-btn').attr('disabled',true);
            }  else {
                $('#remove-list-btn').prop('disabled',false);
                $('#remove-list-btn').attr('disabled',false);
            }
        });

        // validate if all required fields are specified
        $('.form-control').bind("change keyup input",function() {
            var abbrev = $('#list-abbrev').val();
            var name = $('#list-name').val();
            var display_name = $('#list-display-name').val();

            if(abbrev != '' && name != '' && display_name != ''){
                $('#create-list-btn').removeClass('disabled');
            }else{
                $('#create-list-btn').addClass('disabled');
            }
        });

        $(document).on('click', '#export-working-list-btn', function exportWorkingList (e) {
            e.preventDefault()
            e.stopPropagation()

            $('.toast').css('display','none')

            itemCount = $('#dynagrid-find-seed-list .summary').children('b').eq(1).text();

            if(itemCount == '' || parseInt(itemCount) == 0) {
                message = `<i class="material-icons yellow-text" style="margin-right: 8px;">warning</i> Working List is empty.`
            }else if (itemCount != '' && parseInt(itemCount) > 0 && parseInt(itemCount) < parseInt(thresholdValue)) { // Notif for regular export
                message = `<i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                           Exporting all data in the working list. Please wait.`
            }else { // Notif for background job export
                message = `<i class="material-icons blue-text" style="margin-right: 8px;">info</i> 
                           Refreshing page. Background job for exporting data in working list is in progress.`
            }

            Materialize.toast(message, 5000)    
            $('#system-loading-indicator').css('display','none')

            if(parseInt(itemCount) > 0) {
                // Trigger Export working list
                window.location.assign(`\${window.location.origin}$exportWorkingListUrl?itemCount=\${itemCount}`)
            }
        })
    });

    //Updates list when reordered
    function renderReorder(){
        
        $("#search-grid-table-con-container").css('height',window.innerHeight-180);
        $(".find-seeds-list").css('height',window.innerHeight-180);

        var reorderUrl = '$reorderUrl';
        $("#dynagrid-find-seed-list tbody").sortable({
            opacity: 0.325,
            tolerance: 'pointer',
            cursor: 'move',
            update: function(event, ui) {
            },
            cancel: "#dynagrid-find-seed-list td:not('[class*=drag-handle-column]')",
            revert: true,
        });

        $('#dynagrid-find-seed-list tbody').on('sortupdate',function(event,ui){

            var uiItem = $(ui.item)[0];
            var dataId = $($(uiItem).find('.sort_order_arr')[0]).attr("data-id");
            var previousRowNumber = $($(uiItem['previousElementSibling']).find('.sort_order_arr')[0]).html();
            var newRowNumber = (previousRowNumber === undefined) ? 1 : parseInt(previousRowNumber) + 1;

            var count = 1;
            $.each($('.sort_order_arr'), function(i, v){
                $(v).html(count);
                count++
            });
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: reorderUrl,
                data: {dataId:dataId, newRowNumber:newRowNumber},
                // async: false,
                success: function(data) {
                }
            });
        });
    }

    // get IDs of checked checkboxes in working list
    function retrieveCheckedItemsWL(){
        var ids = $('#dynagrid-find-seed-list').b4rCheckboxColumn("getSelectedRows");
        return ids;
    }
JS;

$this->registerJs('var itemCount='.$itemCount.'; '.$script);
?>