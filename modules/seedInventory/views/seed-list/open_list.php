<?php
/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Add from Seed List modal content
 **/

use app\models\PlatformList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use yii\web\JqueryAsset;

?>

<div>
    <?php
        $notif = '<div id="invalid-select" class="red-text text-darken-2" style = "display:none">No list selected!</div>';

        $listDataProvider = $listDataProvider;
        $dataCount = ($listDataProvider == null || $listDataProvider->allModels == null ? 0 : count($listDataProvider->allModels));

        echo $notif . Html::input(
            'text',
            'dataType',
            $dataType,
            [
                'id' => 'dataType-hidden',
                'class' => 'input-text-hidden hidden variable-filter input-text-filter'

            ]
        ) . Html::input(
            'text',
            'dataCount',
            $dataCount,
            [
                'id' => 'data-count-hidden',
                'class' => 'input-text-hidden hidden data-count'

            ]
        );

        $actionColumns = [
            [
                'label' => "Radio Button",
                'mergeHeader' => true,
                'header' => '<button type="button" class="close kv-clear-radio transparent" title="Clear selection" style="margin-left:10px;" id="select-list">×</button>',
                'headerOptions'=>['class'=>'radio-btn-row'],
                'content' => function ($model) {
                        return '
                        <input class="selected-list-select-radio saved-list-select-radio filled-in with-gap" name="list-radio-tn-grp" type="radio" id="saved-list-item-' . $model["id"] . '" />
                        <label for="saved-list-item-' . $model["id"] . '" id="label-for-radio" style="padding-left: 20px;" ></label>';
                    
                },
                'width' => '1%',
            ],
            
        ];

        $columns = [
            [
                'attribute' => 'id',
                'header' => 'Id',
                'visible' => true,
                'format' => 'raw',
            ],
            [
                'attribute' => 'display_name',
                'header' => 'Name',
                'visible' => true,
                'format' => 'raw',
            ],
            [
                'attribute' => 'abbrev',
                'header' => 'Abbrev',
                'visible' => true,
                'format' => 'raw',
            ],
            [
                'attribute' => 'item_count',
                'header' => 'Number of Items',
                'visible' => true,
                'format' => 'raw',
            ],

        ];
        $gridColumns = array_merge($actionColumns, $columns);

        $dynagrid = DynaGrid::begin([
            'columns' => $gridColumns,
            'theme' => 'panel-default',
            'showPersonalize' => true,
            'storage' => 'cookie',
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => [
                'id' => 'dynagrid-open-list',
                'dataProvider' => $listDataProvider,
                'floatHeader' =>true,
                'floatOverflowContainer'=> true,
                'showPageSummary' => false,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'dynagrid-open-list',
                    ],
                ],
                'panel' => [
                    'before' => \Yii::t('app', 'Select a saved list here.'),
                    'after' => false,
                ],
                'toolbar' => '',
            ],
            'options' => ['id'=>'dynagrid-open-list'],
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => 'Remove',
            ],
        ]);
        if (substr($dynagrid->theme, 0, 6) == 'simple') {
            $dynagrid->gridOptions['panel'] = false;
        }
        DynaGrid::end();
    ?>
</div>