<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

use kartik\select2\Select2;
use kartik\sortable\Sortable;
use app\modules\seedInventory\models\ProgramPackageTypeModel;

\yii\jui\JuiAsset::register($this);
extract($data);
$programArr = ProgramPackageTypeModel::getProgramListAll();
$packageList = ProgramPackageTypeModel::getPackageType();
$package_type_selected = explode(",", $package_type_list);
$program_id_selected = (int) $program_id;

?>



<form class="col s12" id="update-program-package-form" action="post">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="material-icons">add</i> Update Program Seed Package Type Used </h4>
            </div>
            <div class="modal-body" style="max-height: 730px; height:280px;">
                <div class="row">

                    <div class="col s12">

                        <label for="program_name"><?php echo Yii::t('app', 'Program'); ?><span style="color:red">*</span></label>
                        <?php
                        echo Select2::widget([
                            'name' => 'program',
                            'data' => $programArr,
                            'options' => [
                                'placeholder' => 'Select a program ...',
                                'id' => 'update-select-program-id2',
                                'style' => 'width:80%',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id' => 'update-select-program-id2',
                                'disabled' => true

                            ],
                            "value" => $program_id
                        ]);
                        ?>

                    </div>

                </div>

                <div class="row">

                    <div class="col s12">

                        <label for="package_type_name"><?php echo Yii::t('app', 'Package Type'); ?><span style="color:red">*</span></label>
                        <?php
                        echo Select2::widget([
                            'name' => 'package-type',
                            'data' => $packageList,
                            'options' => [
                                'placeholder' => 'Select a package type ...',
                                'id' => 'update-select-package-type-id',
                                'style' => 'width:80%',
                                'class' => 'update-select2-package-type-filter-selection2'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'id' => 'update-select-package-type-id',
                                'multiple' => true
                            ],
                            "value" => $package_type_selected
                        ]);
                        ?>
                    </div>

                </div>
            </div>

        </div>
        <div class="modal-footer">

            <a href="#" data-dismiss="modal" style="margin-right:5px;"><?php echo \Yii::t('app', 'Cancel'); ?></a>

            <?php
            echo yii\helpers\Html::submitButton('<i class="material-icons right">send</i>' . Yii::t('app', 'Submit'), [
                'class' => 'btn update-program-package-btn waves-effect',
                'disabled' => false,
                'form' => 'update-program-package-form'
            ]);
            ?>
        </div>
    </div>
</form>



<script type="text/javascript">
    $(document).ready(function() {


        $("#update-select-package-type-id").on('change', function(e) {

            if ($("#update-select-package-type-id").val() == '' ) {
                $(".update-program-package-btn").attr('disabled', true);
            } else {
                $(".update-program-package-btn").attr('disabled', false);
            }
        });

 

        $('#program-package-type-update-modal').on('show.bs.modal', function(e) {

            
        });



        $("#update-program-package-form").on('submit', function(e) {

         
            e.preventDefault();
            var packageTypeListofProgram = [];

            var program_id = $("#update-select-program-id2").val();
            var data = $('.update-select2-package-type-filter-selection2').val();


            $.ajax({
                url: '<?php echo yii\helpers\Url::toRoute(['program-package-type/update']); ?>',
                type: 'post',
                data: {
                    "program_id": program_id,
                    "package_type_list": data
                },
                dataType: 'json',
                success: function(response) {

                    if (response.success) {

                        $('#program-package-type-update-modal').modal('hide');
                        var notif = "<i class='material-icons green-text'>done</i> Successfully update record.";
                        Materialize.toast(notif, 5000);
                        $.pjax.reload({
                            container: '#seed-inventory-package-type-grid-pjax',
                            replace: true,
                            url: '<?php echo yii\helpers\Url::toRoute(['/seedInventory/configuration']); ?>',
                        });
                    } else {
                        var notif = "<i class='material-icons orange-text'>warning</i>" + response.message;
                        Materialize.toast(notif, 5000);
                    }
                }
            });


        });

    });
</script>