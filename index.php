<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

require(__DIR__ . '/vendor/autoload.php');

// load env vile
if(file_exists('config/.env')){
    $dotenv = (new Symfony\Component\Dotenv\Dotenv(true))->usePutenv();
    $dotenv->load(__DIR__.'/config/.env');
}
else{
   echo 'Missing configuration file. Please contact your administrator.';
   exit;
}

// When enabled, Yii error page shows upon encountering an error, and container logs are on trace level
defined('YII_DEBUG') || define('YII_DEBUG', strtolower(getenv('CB_DEBUG_ENABLED')) === "true");
// This variable is not set anywhere outside of this repository so it's always 'dev'
defined('YII_ENV') || define('YII_ENV', 'dev');

// Import Yii & container here, otherwise debugger will not work
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/config/container.php');

$config = require(__DIR__ . '/config/web.php');

$application = new yii\web\Application($config);
$exitCode = $application->run();
exit($exitCode);
