<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
namespace app\widgets;

use app\models\Variable;
use yii\base\Widget;

/**
 * View variable information widget
 */
class VariableInfo extends Widget
{
    public $entityLabel; // name identifier of the record
    public $id; // variable indentifier
    public $label; // label to be displayed in UI
    public $linkAttrs; // html attributes

    public function __construct (
        public Variable $variable,
        $config = []
    )
    {
        parent::__construct($config);
    }

    /**
    * Render Variable information
    */
    public function run(){

        if(!isset($entityLabel) && empty($entityLabel)){
            $response = $this->variable->getOne(intval($this->id));
            $isSuccessful = $response['success'];
            $hasData = $response['totalCount'];
            $isLabelSet = isset($response['data']['label']);

            $label = $isSuccessful && $hasData && $isLabelSet ? $response['data']['label'] : '<span class="not-set">(not set)</span>';

        }
        
        return $this->render('variable/index',[
            'value' => $label,
            'id' => $this->id,
            'linkAttrs' => $this->linkAttrs,
            'entityLabel' => $this->entityLabel
        ]);
    }
}
