<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
namespace app\widgets;

use yii\base\Widget;
use app\models\Experiment;

/**
* View experiment information widget
*/

class ExperimentInfo extends Widget
{
	public $entityLabel; // label for the widget
	public $id; // experiment indentifier
	public $label; // label to be displayed in UI
	public $linkAttrs; // html attributes

	/**
	 * Render Experiment information
	 */
	public function run(){

		return $this->render('experiment/index',[
			'id' => $this->id,
			'linkAttrs' => $this->linkAttrs,
			'entityLabel' => $this->entityLabel,
			'label' => isset($this->label) ? $this->label : $this->entityLabel
		]);
	}
}
