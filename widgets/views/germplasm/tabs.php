<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use yii\helpers\Url;

$user = \Yii::$container->get('app\models\User');
$userType = $user->isAdmin();

$viewGermplasmUrl = Url::to([
    '/germplasm/view/index', 
    'id' => $entityId,
    'program' => $program,
    'returnUrl' => Yii::$app->request->referrer
]);
/**
 * Index page that renders germplasm information
 */

?>

<ul id="view-germplasm-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Basic and additional information of germplasm') ?>">
        <a href="#" id="li-1" class="active a-tab"><?= \Yii::t('app', 'Basic') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known names to germplasm') ?>">
        <a href="#" id="li-2" class="a-tab"><?= \Yii::t('app', 'Names') ?></a>
    </li>
    <li class="tab col" title="<?= \Yii::t('app', 'Pedigree to germplasm')?>">
        <a href="#" id="li-6" class="a-tab"><?= \Yii::t('app', 'Pedigree') ?></a>
    </li>
    <li class="pull-right" id="view-more-info-tab" title="<?= \Yii::t('app', 'View More Information')?>">
        <a href="<?php echo $viewGermplasmUrl ?>" id="view-more-info-btn" class="btn btn-primary waves-effect waves-light"><?= \Yii::t('app', 'View More Information') ?></a>
    </li>
</ul>
<div id="view-entity-1" class="col s12 view-entity-content">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/germplasm/basic.php',[
        'data' => $data['basic'],
    ]);
?>
</div>
<div id="view-entity-2" class="col s12 view-entity-content hidden is-not-loaded">
<?php
    echo '<div class="progress"><div class="indeterminate"></div></div>';
?>
</div>
<div id="view-entity-6" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderAjax('@app/widgets/views/germplasm/pedigree.php',[
        'data' => $data['pedigrees'], 'entityId' => $entityId, 'entityLabel'=>$entityLabel, 'program' => $program, 'pedigreeDepth' => $pedigreeDepth
    ]);
?>
</div>

<?php

$viewGermplasmNamesInfoUrl = Url::to(['/germplasm/default/view-names-info', 'id'=>$entityId]);

$this->registerJS(<<<JS
    // Load germplasm names
    $(document).on('click', '#li-2', function(e) {
        if($('#view-entity-2').hasClass('is-not-loaded')) {
            setTimeout(function(){
                $.ajax({
                    url: '$viewGermplasmNamesInfoUrl',
                    data: {},
                    type: 'POST',
                    async: false,
                    success: function(data) {
                        $('#view-entity-2').html(data);
                    },
                    error: function(){
                        $('#view-entity-2').html('<i>There was a problem while loading record information.</i>'); 
                }
                });
            }, 300);
        }
    });
JS
);

$this->registerCss('
    #view-germplasm-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
    .disabled-tab {
        color: grey !important;
    }
    #view-more-info-tab {
        padding: 5px !important;
    }
');
?>