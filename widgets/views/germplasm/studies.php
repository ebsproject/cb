<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use yii\widgets\Pjax;
use yii\grid\GridView;

/**
 * Renders list of studies where the germplasm was used as an entry.
 */
?>

<p><?= \Yii::t('app', 'Below are the studies where the germplasm was used as an entry.') ?></p>
<a class="btn-floating waves-effect waves-light grey pull-right export-data" data-attr="study" data-label="<?= $entityLabel; ?>" id="<?= $entityId; ?>" title="Export to csv file" style="margin-top: -32px;">
    <i class="material-icons">file_download</i>
</a>

<?php Pjax::begin(['id' =>'view-germplasm-studies','enablePushState' => true]) ?>

<?= GridView::widget([
    'dataProvider' => $data,
    'id' => 'view-germplasm-tables',
    'layout' => '{summary}{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        'program',
        'year',
        'season',
        'phase',
        'study',
        'name',
        'author'
    ]
]) ?>

<?php  Pjax::end(); ?>
