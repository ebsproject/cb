<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Index page for view germplasm widget
 */
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

echo Html::a($label,
            '#',
            $linkAttrs
);

$viewGermplasmUrl = Url::to(['/germplasm/default/view-info']);

?>
<!-- modal for view germplasm -->
<div id="view-germplasm-widget-modal" class="fade modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div style="float:left"><h4><i class="fa fa-info-circle"></i> <span id="view-germplasm-label"></span></h4></div><div style="float:right"></div>
        </div>
        <div class="modal-body view-germplasm-widget-body">
            <div id="view-germplasm-widget-modal-body"></div>
        </div>

        <div class="modal-footer">
            <?= Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;' ?>
        </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
    var thisId = $id;

    // view germplasm info
    $('#view-germplasm-widget-'+thisId+',#view-germplasm-widget-'+thisId+'-more-info').on('click', function(e){
        var label = $(this).attr('data-label');
        var id = $(this).attr('data-id');

        $('#view-germplasm-label').html(label);

        $('#view-germplasm-widget-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        //load content of view germplasm information
        setTimeout(function(){ 
            $.ajax({
                url: '$viewGermplasmUrl',
                data: {
                    entity_id: id,
                    entity_type: 'germplasm',
                    entity_label: "$entityLabel"
                },
                type: 'POST',
                async: false,
                success: function(data) {
                    $('#view-germplasm-widget-modal-body').html(data);
                },
                error: function(){
                    $('#view-germplasm-widget-modal-body').html('<i>There was a problem while loading record information.</i>'); 
            }
            });
        }, 300);  
    });
JS
);

?>