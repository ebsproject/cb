<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders germplasm pedigree tree information
 */
namespace app\modules\search\models;

use yii\grid\GridView;
use app\controllers\Constants;


?>
<a class="btn-floating waves-effect waves-light grey pull-right" id="print-tree" title="Print pedigree tree."><i class="material-icons">print</i></a>
<p><?= \Yii::t('app', 'Below displays pedigree tree of the germplasm.') ?></p>
<?php
if(!empty($data)){ 
    echo '<div id="graph">';
    echo '<div class="white z-depth-3 card-panel pedigree-tree">';  
    echo SearchProduct::printTree($data);
    echo '</div></div>';
}else{
    echo '<i>No pedigree tree information.</i>';
}

$appName = Constants::APP_NAME;
$this->registerJs(<<<JS

//print pedigree tree
$(document).on('click', '#print-tree', function(e) {
    var popUpAndPrint = function()
    {
        var pedigreeTree = $('#graph');
        var height = $(window).height() - 100;
        var width = $(window).width() - 100;
        var app_name = '$appName';
        
        var printWindow = window.open('', 'PrintMap',
        'width=' + width + ',height=' + height);
        printWindow.document.writeln('<style type="text/css" media="print"> @media print{ @page {size: portrait;} .card-panel.pedigree-tree{font-family: monospace;} .pull-right{float:right;}}</style>');
        printWindow.document.writeln('<p style="font-size:20px;">Pedigree tree<br/><span style="font-size:12px;font-weight:300">Generated from ' + app_name + '</span></p>');
        printWindow.document.writeln($(pedigreeTree).html());

        printWindow.document.close();
        printWindow.print();
        printWindow.close();
    };
    setTimeout(popUpAndPrint, 100);
});
JS
);
?>

<style type="text/css">
body{
    background-color: #fff;
    margin: 10px;
}
.card-panel.pedigree-tree {
    font: 11.5px/1.4 monospace;
}
</style>