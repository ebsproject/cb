<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/** 
 * Renders derived observation data history of a germplasm
 */
use yii\grid\GridView;
?>

<p><?= \Yii::t('app', 'Below are the derived observation data history of the germplasm.') ?></p>

<?=
GridView::widget([
    'dataProvider' => $data,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        [
            'label' => '<span title="Trait observation variable">Observation</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'value' => function($data){
                return '<span title="'.$data['label'].'">'.$data['display_name'].'</span>';
            }
        ],
        [
            'label' => '<span title="Data level whether entry or plot">Data level</span>',
            'encodeLabel' => false,
            'attribute' => 'data_level'
        ],
        [
            'label' => 'No. of Observation',
            'attribute' => 'count'
        ],
        [
            'label' => '<span title="Minimum value of the observation">Min</span>',
            'encodeLabel' => false,
            'attribute' => 'min',
            'value' => function($data){
                return (isset($data['min'])) ? round($data['min'],4) : '';
            }
        ],
        [
            'label' => '<span title="Maximum value of the observation">Max</span>',
            'encodeLabel' => false,
            'attribute' => 'max',
            'value' => function($data){
                return (isset($data['max'])) ? round($data['max'],4) : '';
            }
        ],
        [
            'label' => '<span title="Average value of the observation">Avg</span>',
            'encodeLabel' => false,
            'attribute' => 'avg',
            'value' => function($data){
                return (isset($data['avg'])) ? round($data['avg'],4) : '';
            }
        ]
    ]
])
?>