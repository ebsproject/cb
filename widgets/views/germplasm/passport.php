<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders passport data of germplasm
 */

use yii\grid\GridView;
use yii\widgets\Pjax;
use app\controllers\Constants;

?>
<a class="btn-floating waves-effect waves-light grey pull-right" id="print-passport" title="Print passport data." style="margin-top: -14px;">
    <i class="material-icons">print</i>
</a>

<p><?= \Yii::t('app', 'Below displays passport data of germplasm.') ?></p>
<?php	
    if(isset($data['success']) && $data['success'] == true){
        echo '<span id="passport">';
        echo '<p id="passport-header">Passport Data of '.$entityLabel.'<br/><span style="font-size:12px;font-weight:300">Generated from '.Constants::APP_NAME.'</span></p>';

        Pjax::begin(['id' =>'view-germplasm-passport','enablePushState' => true]);
        echo GridView::widget([
            'dataProvider' => $data['output'],
            'layout' => '{items}',
            'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => ''
                ],
                'Description',
                'Value',
                'Date'
            ]
        ]);
        Pjax::end();
        echo '</div>';
    }else{
        echo '<i>'.$data['output'].'</i>';
    }
?>