<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders list of crosses where the germplasm was used as a parent
 */

use yii\grid\GridView;

?>

<p><?= \Yii::t('app', 'Below are the crosses where the germplasm was used as a parent.') ?></p>
<a class="btn-floating waves-effect waves-light grey pull-right export-data" data-attr="cross" data-label="<?= $entityLabel; ?>" id="<?= $entityId; ?>" title="Export to csv file" style="margin-top: -32px;">
    <i class="material-icons">file_download</i>
</a>
<?= GridView::widget([
    'dataProvider' => $data,
    'id' => 'view-germplasm-tables',
    'layout' => '{summary}{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        'cross_name',
        'year',
        'female',
        'male',
        'method',
        'location'
    ]
]) ?>