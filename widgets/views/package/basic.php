<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders package basic information
 */
?>

<p><?= \Yii::t('app', 'Below displays the basic and source information about the package.') ?></p>
<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //basic information
    $col1Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    $length = (isset($data['basic']) && !empty($data['basic']))? sizeof($data['basic']): 0;
    $count = 0;
    foreach ($data['basic'] as $k => $value) {
        $key = ucfirst(str_replace('_',' ', $k));
        $value = ($value == '') ? $notSet : $value;

        $col1Str = $col1Str.'<dt title="'.$key.'" style="width: 190px !important;">'.$key.'</dt><dd style="margin-left: 200px !important; word-break: break-all">'.$value.'</dd>';
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Basic information') ?></div>
        <div class="collapsible-body" style="display: block; margin-top: 15px;">
            <dl class="dl-horizontal">
                <?= $col1Str ?>
            </dl>
        </div>
    </li>
</ul>
<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //source information
    $col1Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    $length = (isset($data['source']) && !empty($data['source']))? sizeof($data['source']): 0;
    $count = 0;
    foreach ($data['source'] as $k => $value) {
        $key = ucfirst(str_replace('_',' ', $k));
        $value = ($value == '') ? $notSet : $value;

        $col1Str = $col1Str.'<dt title="'.$key.'" style="width: 190px !important;">'.$key.'</dt><dd style="margin-left: 200px !important; word-break: break-all">'.$value.'</dd>';
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Source information') ?></div>
        <div class="collapsible-body" style="display: block; margin-top: 15px;">
            <dl class="dl-horizontal">
                <?= $col1Str ?>
            </dl>
        </div>
    </li>
</ul>