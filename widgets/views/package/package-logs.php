<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders logs assigned to package
 */
use yii\grid\GridView;
?>
<?= \Yii::t('app', 'Below displays known logs to package.') ?></p>
<?= GridView::widget([
    'dataProvider' => $data,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => ''
        ],
        'packageLabel',
        [
            'attribute' => 'packageQuantity',
            'header' => 'Quantity'
        ],
        [
            'attribute' => 'packageUnit',
            'header' => 'Unit'
        ],
        [
            'attribute' => 'packageTransactionType',
            'header' => 'Transaction Type'
        ],
        [
            'attribute' => 'creator',
            'header' => 'encoder'
        ],
        [
            'attribute' => 'creationTimestamp',
            'header' => 'encoded Timestamp'
        ]
    ],
]) ?>
