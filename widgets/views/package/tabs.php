<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use yii\helpers\Url;

/**
 * Index page that renders package information
 */

?>

<ul id="view-package-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Basic information of package') ?>">
        <a href="#" id="li-1" class="active a-tab"><?= \Yii::t('app', 'Basic') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Known logs to package') ?>">
        <a href="#" id="li-2" class="a-tab"><?= \Yii::t('app', 'Usage') ?></a>
    </li>
</ul>
<div id="view-entity-1" class="col s12 view-entity-content">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/package/basic.php',[
        'data' => $data['basic'],
    ]);
?>
</div>
<div id="view-entity-2" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/package/package-logs.php',[
        'data' => $data['package-logs'],
    ]);
?>
</div>

<?php
$this->registerCss('
    #view-package-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
    .disabled-tab {
        color: grey !important;
    }
');
?>