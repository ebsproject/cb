<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Index page for view person widget
 */
use yii\helpers\Url;
use yii\helpers\Html;

echo Html::a($label,
    '#',
    $linkAttrs
);

$viewPersonUrl = Url::to(['/user/default/view-info']);
?>
<!-- modal for view person -->
<div id="view-person-widget-modal" class="fade modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div style="float:left"><h4><i class="fa fa-info-circle"></i> <span id="view-person-label"></span></h4></div><div style="float:right"></div>
        </div>
        <div class="modal-body view-person-widget-body">
            <div id="view-person-widget-modal-body"></div>
        </div>

        <div class="modal-footer">
            <?= Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;' ?>
        </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
    var thisId = $id;

    // view person info
    $('#view-person-widget-'+thisId+',#view-person-widget-'+thisId+'-more-info').on('click', function(e) {
        var label = $(this).attr('data-label');
        var id = $(this).attr('data-id');

        $('#view-person-label').html(label);
        $('#view-person-widget-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        //load content of view person information
        setTimeout(function(){
            $.ajax({
                url: '$viewPersonUrl',
                data: {
                    id: id
                },
                type: 'POST',
                async: false,
                success: function(data) {
                    $('#view-person-widget-modal-body').html(data);
                },
                error: function(){
                    $('#view-person-widget-modal-body').html('<i>There was a problem while loading record information.</i>');
                }
            });
        }, 300);
    });
JS
);

?>