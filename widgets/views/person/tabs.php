<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use yii\helpers\Url;

/**
 * Index page that renders person information
 */

?>

<ul id="view-person-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Basic information of person') ?>">
        <a href="#" id="li-1" class="active a-tab"><?= \Yii::t('app', 'Basic') ?></a>
    </li>
    <li class="tab col disabled">
        <a href="javascript:void(0);" id="li-2" class="a-tab disabled disabled-tab"><?= \Yii::t('app', 'Team') ?></a>
    </li>
</ul>
<div id="view-entity-1" class="col s12 view-entity-content">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/person/basic.php',[
        'data' => $data['basic'],
    ]);
?>
</div>

<?php
$this->registerJS(<<<JS
    $('#li-2').on('click', function(e) {
        e.preventDefault();
        return false;
    });
JS
);

$this->registerCss('
    #view-person-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
    .disabled-tab {
        color: grey !important;
    }
');
?>