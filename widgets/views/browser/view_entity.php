<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * View more information about the entity
 */
extract($data);
$notSet = '<span class="not-set">(not set)</span>';

// set display of code label
$codeLabel = isset($code['label']) ? htmlspecialchars($code['label']) : '';
$codeValue = isset($code['value']) ? htmlspecialchars($code['value']) : $notSet;
$codeValue = isset($viewUrl) && !empty($viewUrl) ? 
	'<a href="'. $viewUrl .'" title="This is the '. $codeLabel .'. Click to view more information.">
		<i class="material-icons inline-material-icon">remove_red_eye</i> '. $codeValue .
	'</a>' : 
	'<span title="'. $codeLabel .'">'. $codeValue .'</span>';

?>

<div class="browser-detail-view-viewer-panel">
	<div class="browser-detail-view-viewer-inner">
		<div class="viewer-fixed-header">
			<div class="viewer-fixed-header-code col col-md-7">
				<?= $codeValue ?>
				
			</div>		

			<div class="viewer-actions col col-md-5 no-padding-right">
				<div class="viewer-fixed-header-buttons">
					
					<button class="btn-flat grey lighten-5 black-text btn right browser-detail-view-viewer-close detail-view-close-button margin-right" title="Close panel">
						<i class="material-icons">close</i>
					</button>

					<?php
						// render header action buttons
						foreach ($buttons as $key => $value) {
							echo $value[0];
						}
					?>
				
				</div>
			</div>		

		</div>
		<div class="row margin-bottom"></div>
		<div class="viewer-body">
			<!-- First column -->
			<div class="col col-md-8">
				
				<div class="col col-md-12">

					<div class="viewer-display-name">
						<h3><?= !empty($name) ? '<span title="'.htmlspecialchars($name['label']).'">'.htmlspecialchars($name['value']) . '</span>' : $notSet ?></h3>
					</div>

					<!-- status to be displayed for smaller screens -->
					<div class="viewer-status for-small-screen">
						<span class="badge new <?= $status['statusClass'] ?>">
							<?= htmlspecialchars($status['label']) ?>
						</span>
					</div>

					<?php
						// if there are main action buttons
						if(!empty($mainButtons)){
							echo '<div class="viewer-main-buttons with-margin-bottom">';
								foreach ($mainButtons as $key => $value) {
									echo $value[0];
								}
							echo '</div>';
						}
					?>
					<div class="viewer-description viewer-detail-item">
						<div class="viewer-description viewer-label">
							Description
						</div>

						<div class="viewer-description-value viewer-value">
							<?= !empty($description) ? htmlspecialchars($description) : $notSet ?>
						</div>
					</div>

				</div>

				<div class="viewer-primary-info">

					<?php
						// primary information
						if(isset($primaryInfo) && !empty($primaryInfo)){
							
							$primaryInfoStr = '';
							foreach ($primaryInfo as $key => $value) {

								$label = htmlspecialchars($value['label']);
								$val = (!empty($value['value']) || $value['value'] == '0') ? htmlspecialchars($value['value']) : $notSet;
								$title = (!empty($value['title']) && isset($value['title'])) ? htmlspecialchars($value['title']) : '';
								$val = (!empty($value['url']) && isset($value['url'])) ? 
									'<b><a href="'.$value['url'].'">'. $val .'</a></b>' : $val;
								$attribute = (!empty($value['attribute']) && isset($value['attribute'])) ? $value['attribute'] : '';
								
								$colClass = '6';
								if(in_array($attribute, ['experiments', 'occurrences']))
									$colClass = '12';

								$detailInfo = <<<EOD
									<div class="col col-lg-$colClass">
										<div class="viewer-primary-info viewer-detail-item">
											<div class="viewer-label">
												$label
											</div>
											<div class="viewer-value" title="$title">
												$val
											</div>
										</div>
									</div>
EOD;

								$primaryInfoStr .= $detailInfo;

								if($key % 2){
									$primaryInfoStr .= '<div class="row no-margin"></div>';
								}
							}

							echo $primaryInfoStr;
						}

					?>
				</div>
				<div class="clearfix"></div>
				<!-- For status monitoring -->
				<?php
				if(!empty($monitoringInfo) && isset($monitoringInfo)){
					echo '<div class="viewer-monitoring-info">'.$monitoringInfo.'</div>';
				}
				?>
				
			</div>

			<!-- Second column -->
			<div class="col col-md-4">
				<!-- status to be displayed for wider screens -->
				<div class="viewer-status for-wide-screen">
					<span class="badge new <?= $status['statusClass'] ?>">
						<?= htmlspecialchars($status['label']) ?>
					</span>
				</div>

			<?php
				// secondary information
				if(isset($secondaryInfo) && !empty($secondaryInfo)){

					$secondaryInfoStr = '';
					foreach ($secondaryInfo as $key => $value) {
						$label = htmlspecialchars($value['label']);
						$val = !empty($value['value']) ? htmlspecialchars($value['value']) : $notSet;

						$secondaryInfoStr .= <<<EOD
							<div class="viewer-secondary-info viewer-detail-item">
								<div class="viewer-label">
									$label
								</div>
								<div class="viewer-value">
									$val
								</div>
							</div>
EOD;
					}

					echo $secondaryInfoStr;
				}

				echo '<div class="divider margin-bottom"></div>';

				// autdit information
				if(isset($auditInfo) && !empty($auditInfo)){
					foreach ($auditInfo as $key => $value) {
						if(!empty($value['value']) && isset($value['value'])){
							$label = $value['label'];
							$val = $value['value'];

							echo <<<EOD
								<div class="viewer-audit-info text-muted">
									$label $val
								</div>
EOD;
						}
					}
				}

			?>
			
			</div>
		</div>
	</div>
</div>

<?php
$this->registerJs(
	// render button dropdown
	"$('.dropdown-trigger').dropdown();"
);