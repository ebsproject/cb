<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
use app\controllers\BrowserController;

/**
 * Render category headers and panels
 */

$categoryHeaders = '';
$categoryPanels = '';

if(!empty($categories)){
	foreach ($categories as $key => $value) {
		$modelClass = $value['modelClass'];
		$modelAction = $value['modelClassAction'];
		$label = ucfirst(BrowserController::pluralize(2, $key));

		$categoryHeaders .= <<<EOD
			<li class="browser-detail-view-main-filter-item category-item-$key" 
				title="Filter by $label"
				data-category="$key" 
				data-model-class="$modelClass" 
				data-model-class-action="$modelAction">

				<span class="browser-detail-view-main-filter-item-text">
					$label
				</span>
			</li>
EOD;

		$categoryPanels .= <<<EOD
			<div class="browser-detail-view-main-filters category-panel-$key hidden">
				<div class="browser-detail-view-main-filters-group">
					<div class="browser-detail-view-main-filters-header">
						
						<span class="float-left">$label</span>
						<div class="category-panel-buttons">
							<div class="browser-detail-view-main-filters-close detail-view-close-button" title="Hide $label panel" data-category="$key">
								<i class="material-icons">close</i>
							</div>
						</div>

					</div>

					<div class="row no-margin"></div>

					<div class="categories-scrollview" id="categories-panel-$key">
						<!-- categories -->
						<!-- loading indicator -->
						<div class="category-panel-loading-indicator text-center">
							<div class="preloader-wrapper small active">
								<div class="spinner-layer spinner-green-only">
									<div class="circle-clipper left">
										<div class="circle"></div>
									</div>
									<div class="gap-patch">
										<div class="circle"></div>
									</div>
									<div class="circle-clipper right">
										<div class="circle"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
EOD;
	}
}

?>

<!-- Filter categories -->
<div class="browser-detail-view-main-filter-opts">
	<ul class="browser-detail-view-main-filter-items">
		<?= $categoryHeaders ?>
	</ul>
</div>

<!-- Category panels -->
<?= $categoryPanels ?>