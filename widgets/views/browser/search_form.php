<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

use yii\bootstrap\ActiveForm;
use app\controllers\BrowserController;
use macgyer\yii2materializecss\widgets\Button;

/**
 * Renders search form in browser detail view
 */
?>

<div class="col col-md-6">
    <?php
        // get total count label
        $totalCountLabel = BrowserController::pluralize($totalCount, 'item');
        // render search input form
        ActiveForm::begin([
            'enableClientValidation'=>false,
            'id'=>'browser-detail-view-search-form',
            'method'  => 'post',
            'options' => [
            'class' => 'browser-detail-view-search-input-form',
            'data-pjax' => true
        ],
        ]);
    ?>
    <div class="input-field no-margin">
        <input 
        id="browser-detail-view-search-input" 
        type="search" 
        class="autocomplete" 
        autocomplete="off" 
        placeholder="<?= \Yii::t('app', 'Search') ?>" 
        title="<?= \Yii::t('app','Search') ?>" <?= (empty($filter)) ? 'autofocus' : '' ?> 
        value= "<?= htmlentities($filter) ?>" 
        >

        <i class="material-icons browser-detail-view-clear-all-search-btn" title="<?= \Yii::t('app', 'Clear all') ?>">clear</i>
        <i class="material-icons browser-detail-view-help-btn" title="<?= \Yii::t('app', 'Help') ?>" data-target="#browser-detail-view-help-modal" data-toggle="modal">help</i>
        <i class="material-icons browser-detail-view-submit-btn" title="<?= \Yii::t('app', 'Search') ?>">search</i>               
    </div>
    <?php ActiveForm::end(); ?>

</div>

<div class="col col-md-6 with-margin-bottom" style="padding-right: 5px;">
    <div class="detail-view-total-count text-right detail-view-buttons">
        <div class="float-right">
            <?php
                // browser action buttons
                if(isset($buttons) && !empty($buttons)){
                    echo $buttons;
                }

                // view mode switch
                $icon = 'view_module';
                $title = 'Switch to grid view';

                // if in grid view
                if($gridMode == 'grid') {
                    $title = 'Switch to detail view';
                    $icon = 'view_list'; 
                }

                echo Button::widget([
                    'label' => '',
                    'icon' => [
                        'name' => $icon
                    ],
                    'options' => [
                        'class' => 'btn-flat pull-right grey lighten-5 black-text switch-browser-view',
                        'title' => $title,
                        'data-view' => $gridMode,
                        'data-browser_id' => $browserId
                    ],
                ]);
            ?>
        </div>
        <div class="float-right with-margin-top">
            Total <b class="total-count-summary"><?= $totalCount ?></b> 
            <span class="total-count-label"><?= $totalCountLabel ?></span>
            &nbsp;&nbsp;
        </div>
    </div>
</div>