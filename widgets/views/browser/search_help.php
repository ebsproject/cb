<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;

/*
* Renders search help content
*/

// help modal
Modal::begin([
    'id' => 'browser-detail-view-help-modal',
    'header' => '<h4><i class="fa fa-question-circle"></i> Search help</h4>',
    'footer' =>
        Html::a('Close', ['#'], [
            'id' => 'brewser-detail-view-close-search-help-modal', 
            'title' => \Yii::t('app', 'Close'),
            'data-dismiss'=> 'modal'
        ]) . '&emsp;&nbsp;',
    'closeButton' => [
        'class' => 'hidden'
    ],
    'size' =>'modal-lg',
    'options' => [
        'data-backdrop'=>'static'
    ],
]);
echo \Yii::t('app','Search records by typing what you are looking for in the search field and then pressing the enter key or clicking the search icon.');
?>
<div class="row"></div>
<div class="col col-md-6">
    <?= \Yii::t('app','You may search against below attributes:'); ?>
    <ul class="browser-default browser-detail-view-help-attributes">
    <?php 
        // render attributes that can be used in searching
        foreach ($attributes as $value) {
            echo '<li>'.\Yii::t('app',$value).'</li>';
        }
    ?>
    </ul>
</div>
<div class="col col-md-6 no-padding-right">
    Examples:
    <div class="card-panel grey lighten-4 text-justify" style="padding:10px 30px 2px 10px">
        <ul class="browser-default">
            <?php
                // render examples for searching
                foreach ($examples as $key => $value) {
                    echo ($key > 0) ? '<br>' : ''; 

                    $val = '<b>'.$value['value'].'</b>';
                    $desc = ' - '.$value['description'];

                    echo '<li>'. $val . $desc .'</li>';
                }
            ?>
        </ul>
    </div>
</div>

<?php Modal::end();