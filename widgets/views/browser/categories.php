<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use app\controllers\BrowserController;
/*
* Renders category items
*/

// pluralize the category
$categories = BrowserController::pluralize(2, $category);
$count = ' (' . $totalCount . ')';
?>

<div class="all-categories category-item" data-category="<?= $category ?>" data-id="all" data-text=''>
	<div class="category-item-inner" title="Load all <?= $categories ?>">
		All <?= $categories . $count ?>
	</div>
</div>

<?php

foreach ($data as $key => $value) {
	$dataId = $value[$id];
	$dataText = $value[$text];
	// set active color to selected item
	$activeColor = ($selectedCategoryId == $dataId) ? '#deebff' : '';

	$details = '';
	foreach ($attributes as $k => $v) {
		$isCount = (strpos($k, 'Count') !== false) ? true : false;

		$detailValue = $value[$k];
		$detailValue = ($isCount) ? '<span class="badge new">' . $detailValue . '</span>' : $detailValue;

		$details .= <<<EOD
			<div class="col col-md-6">
				<div class="category-detail-header">$v</div>
			</div>
			
			<div class="col col-md-6">
				<div class="category-detail-value">$detailValue</div>
			</div>
			<div class="clearfix"></div>

EOD;
	}

	echo <<<EOD
	<div class="category-item" data-category="$category" data-id="$dataId" data-text="$dataText" style="background-color:$activeColor">
		<div class="category-item-inner">
			<div class="category-item-expander theme-color" data-id="$dataId" title="Show/hide $category information">
				<i class="material-icons">keyboard_arrow_right</i>
			</div>
			<div class="category-item-header">
				$dataText
			</div>

			<div class="category-item-details" id="category-expander-$dataId">
				$details
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
EOD;
}
?>
