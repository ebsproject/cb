<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders experiment basic information
 */

?>

<ul class="collapsible popout collapsible-accordion col col-md-12" data-collapsible="accordion" style="margin-top:-10px">
    <?php
    //basic information
    $col1Str = '';
    $col2Str = '';
    $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
    $length = (isset($data['basic']) && !empty($data['basic']))? sizeof($data['basic']): 0;
    $count = 0;
    $dataBasic = (isset($data['basic']) && !empty($data['basic'])) ? $data['basic'] : [];
    foreach ($dataBasic as $k => $value) {
        $key = ucfirst(str_replace('_',' ', $k));
        $value = ($value == '') ? $notSet : $value;

        if($key == 'Status') {
            $experiment = \Yii::$container->get('app\models\Experiment');
            $value = $experiment->formatExperimentStatus($value);
        } else $value = $value;

        if($count < $length/2) {
            $col1Str = $col1Str.'<dt title="'.$key.'" style="width: 190px !important;">'.$key.'</dt><dd style="margin-left: 200px !important;">'.$value.'</dd>';
        } else {
            $col2Str = $col2Str.'<dt title="'.$key.'" style="width: 190px !important;">'.$key.'</dt><dd style="margin-left: 200px !important;">'.$value.'</dd>';
        }
        $count += 1;
    }
    ?>
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Basic information') ?></div>
        <div class="collapsible-body" style="display: inline-flex; margin-top: 15px; width: 100%;">
            <dl class="dl-horizontal col col-md-6">
                <?= $col1Str ?>
            </dl>
            <dl class="dl-horizontal col col-md-6">
                <?= $col2Str ?>
            </dl>
        </div>
    </li>
</ul>