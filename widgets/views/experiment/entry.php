<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders entry information about the experiment
 */

use kartik\grid\GridView;
use app\models\EntryData;

$isParentType = false;

if($crossesAct || $experimentType == 'Cross Parent Nursery'){
   $typeLabel = 'Parent Type';
   $roleLabel = 'Parent Role';
   $isParentType = true;
}else{
    $typeLabel = 'Entry Type';
    $roleLabel = 'Entry Role';
    $isParentType = false;
}
if($entryCount > 100){
    echo "<p>Note: This browser only shows the first <strong>100 of ".$entryCount."</strong> entry records.</p>";
}
?>

<?= GridView::widget([
    'dataProvider' => $data,
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3 search-seed-lot-table'],
    'striped'=>false,
    'showPageSummary'=>false,
    'columns' => $cols
]) ?>
