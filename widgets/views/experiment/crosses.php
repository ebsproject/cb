<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders crosses information about the experiment
 */

use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $data,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn'
        ],
        'crossName',
        [
            'attribute'=>'crossFemaleParent',
            'header' => '<span title="Female Parent"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
        ],
        [
            'attribute'=>'femaleParentage',
            'header' => '<span title="Female Parentage"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENTAGE').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],

        ],
        [
            'attribute'=>'crossMaleParent',
            'header' => '<span title="Male Parent">'.Yii::t('app', '<i class="fa fa-mars"></i> MALE PARENT').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
        ],
        [
            'attribute'=>'maleParentage',
            'header' => '<span title="Male Parentage"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE PARENTAGE').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
        ],
        [
            'attribute'=>'crossMethod',
            'label' => 'Method',
        ],
        'crossRemarks',
        [
            'attribute' => 'femaleParentEntryNumber',
            'label' => '<span title="Female Source Entry"><i class="fa fa-venus"></i>'.Yii::t('app', ' FEMALE PARENT ENTRY NO.').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
            'value' => function($data){
                return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute' => 'maleParentEntryNumber',
            'label' => '<span title="Male Source Entry"><i class="fa fa-mars"></i>'.Yii::t('app', ' MALE PARENT ENTRY NO.').'</span>',
            'encodeLabel' => false,
            'format' => 'raw',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
            'value' => function($data){
                if($data['crossMethod'] == 'selfing'){
                    return !empty($data['femaleParentEntryNumber']) ? $data['femaleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
                } else return !empty($data['maleParentEntryNumber']) ? $data['maleParentEntryNumber'] : '<span class="not-set">(not set)</span>';
            }
        ],
        [
            'attribute'=>'femaleParentSeedSource',
            'header' => '<span title="Female Seed Source"><i class="fa fa-venus"></i>'.Yii::t('app', 'FEMALE SEED SOURCE').'</span>',
            'headerOptions'=>['class'=>'red lighten-4'],
            'contentOptions'=>['class'=>'red lighten-5'],
        ],
        [
            'attribute'=>'maleParentSeedSource',
            'header' => '<span title="Male Seed Source"><i class="fa fa-mars"></i>'.Yii::t('app', 'MALE SEED SOURCE').'</span>',
            'headerOptions'=>['class'=>'blue lighten-4'],
            'contentOptions'=>['class'=>'blue lighten-5'],
        ]

    ],
]) ?>
