<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders entry information about the experiment
 */


use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$program = \Yii::$app->userprogram->get('id');
$exportRecordsThresholdValue = \Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'exportRecords');
$plotCount = $data['plotCount'];

$gridColumns = [
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => false,
        'noWrap' => true,
        'template' => '{export}',
        'buttons' => [
            'export' => function ($url, $data, $key) use ($plotCount){
                if($plotCount > 0 || $data['plotCount'] > 0) {
                    return Html::a('<i class="material-icons  inline-material-icon">file_download</i>',
                        '#',
                        [
                            'class' => 'export-occurrence',
                            'title' => Yii::t('app', 'Export Occurrence'),
                            'data-id' => $data['occurrenceDbId'],
                            'data-target' => '#'
                        ]
                    );
                }
            }
        ]
    ],
    [
        'attribute' => 'occurrenceName',
        'enableSorting' => false
    ],
    [
        'attribute'=>'occurrenceCode',
        'label' => '<span title="Occurrence Code">'.Yii::t('app', 'Code').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute' => 'site',
        'enableSorting' => false
    ],
    [
        'attribute' => 'field',
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentStageCode',
        'label' => '<span title="Stage Code">'.Yii::t('app', 'Stage').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentYear',
        'label' => '<span title="Experiment Year">'.Yii::t('app', 'Year').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute'=>'experimentSeason',
        'label' => '<span title="Experiment Season">'.Yii::t('app', 'Season').'</span>',
        'format' => 'raw',
        'encodeLabel'=>false,
        'enableSorting' => false
    ],
    [
        'attribute' => 'entryCount',
        'enableSorting' => false
    ],
    [
        'attribute' => 'plotCount',
        'enableSorting' => false
    ]
];

?>

<?= GridView::widget([
    'dataProvider' => $data['dataProvider'],
    'id' => 'view-occurrences-table',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => $gridColumns,
    'export' => false,
    'panel'=>[
        'heading'=>false,
        'before'=> \Yii::t('app', '<span class="new badge">NOTE</span> &nbsp;This is the browser for the created occurrences. {summary}'),
        'after' => false,
    ],
    'toggleData'=>true,
    'toolbar' => [
        ['content' => '&nbsp;'],//just for the space between summary and export button
        // 'exportConfig' => $export,
    ]
])
?>

<?php
$exportPlotsUrl = Url::to(['/occurrence/view/export-plots']);
$prepareDataForCsvDownloadUrl = Url::to(['/occurrence/view/prepare-data-for-csv-download', 'program'=>$program,'entity'=>'plot', 'experimentId'=>$id, 'tool'=>'experiment-creation']);

$this->registerJs(<<<JS
    $('.export-occurrence').off('click').on('click', function (e) {
        e.preventDefault()
        var occurrenceDbId = $(this).attr('data-id')
        if ($plotCount >= $exportRecordsThresholdValue) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '$prepareDataForCsvDownloadUrl'+'&occurrenceId='+occurrenceDbId,
                data: {},
                success: async function (ajaxResponse) {},
                error: function (xhr, error, status) {}
            })
        } else {
            let exportPlotsUrl = '$exportPlotsUrl'
            let expPlotUrl = window.location.origin + exportPlotsUrl + '?program=' + '$program' + '&id=' + occurrenceDbId

            message = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Please wait for the download to finish.'
            $('.toast').css('display','none')
            Materialize.toast(message, 5000)

            window.location.replace(expPlotUrl)

            $('#system-loading-indicator').html('')
        }
    })
JS
);

?>