<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use yii\helpers\Url;

/**
 * Index page that renders experiment information
 */
?>

<ul id="view-experiment-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Basic information of experiment') ?>">
        <a href="#" id="li-1" class="active a-tab"><?= \Yii::t('app', 'Basic') ?></a>
    </li>
    <li class="tab col " title="<?= \Yii::t('app', 'Entry information of experiment') ?>">
        <a href="#" id="li-2" class="a-tab"><?= \Yii::t('app', 'Entry') ?></a>
    </li>
    <?php if($data['crosses'] != null){?>
    <li class="tab col " title="<?= \Yii::t('app', 'Crosses information of experiment') ?>">
        <a href="#" id="li-3" class="a-tab"><?= \Yii::t('app', 'Crosses') ?></a>
    </li>
    <?php } ?>
    <li class="tab col " title="<?= \Yii::t('app', 'Occurrence information of experiment') ?>">
        <a href="#" id="li-4" class="a-tab"><?= \Yii::t('app', 'Occurrences') ?></a>
    </li>
</ul>
<div id="view-entity-1" class="col s12 view-entity-content">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/experiment/basic.php',[
        'data' => $data['basic'],
    ]);
?>
</div>
<div id="view-entity-2" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/experiment/entry.php',[
        'data' => $data['entry'],
        'cols' => $data['cols'],
        'entryCount' => $data['entryCount'],
        'crossesAct' => $crossesAct,
        'experimentType' => $experimentType
    ]);
?>
</div>
<?php if($data['crosses'] != null){?>
<div id="view-entity-3" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/experiment/crosses.php',[
        'data' => $data['crosses'],
    ]);
?>
</div>
<?php } ?>
<div id="view-entity-4" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/experiment/occurrences.php',[
        'id' => $id,
        'data' => $data['occurrences'],
    ]);
?>
</div>

<?php
$this->registerCss('
    #view-experiment-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
    .disabled-tab {
        color: grey !important;
    }
');
?>