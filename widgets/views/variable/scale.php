<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders scale information of trait (formerly "variable")
 */

use kartik\grid\GridView;

$col1Str = '';
$col2Str = '';
$notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';

foreach ($data['basic'] as $key => $val) {
    $label = $val['label'];
    $label = preg_replace('/(?<!\ )[A-Z]/', ' $0', $label); // add space before capital letter
    $label = ucfirst($label); // capitalize first letter
    $value = isset($val['value']) && $val['value'] !== '' ? $val['value'] : $notSet;

    if($key < 6){ // basic info
        $col1Str = $col1Str . '<dt title="'.$label.'">'.$label.'</dt><dd>'.$value.'</dd>';
    }else{
        $col2Str = $col2Str . '<dt title="'.$label.'">'.$label.'</dt><dd>'.$value.'</dd>';
    }
}
echo '<p>'.\Yii::t('app', 'Unit of measurement and/or allowed values the trait can hold.').'</p>';
?>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Basic information') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px">
            <dl class="dl-horizontal">
                <?= $col1Str ?>
            </dl>
        </div>
    </li>
</ul>

<ul class="collapsible popout collapsible-accordion col col-md-6" data-collapsible="accordion" style="margin-top:-10px">
    <li class="active">
        <div class="collapsible-header active"><?= \Yii::t('app', 'Additional information') ?></div>
        <div class="collapsible-body" style="display: block;margin-top:15px">
            <dl class="dl-horizontal">
                <?= $col2Str ?>
            </dl>
        </div>
    </li>
</ul>

<div class="clearfix"></div>
<?php
echo '<p><b>Scale values</b></p>';
$columns = [
    [
        'attribute' => 'orderNumber',
        'label' => '',
        'enableSorting' => false
    ],
    [
        'attribute' => 'value',
        'enableSorting' => false
    ],
    [
        'attribute' => 'description',
        'enableSorting' => false
    ],
    [
        'attribute' => 'displayName',
        'enableSorting' => false
    ]
];
echo GridView::widget([
    'pjax' => true,
    'dataProvider' => $data['values'],
    'id' => 'view-variable-info-scale-values',
    'layout'=>'{items}',
    'columns' => $columns,
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3','showSummary'=>false],
]);

?>

<style type="text/css">
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #fff;
}
</style>