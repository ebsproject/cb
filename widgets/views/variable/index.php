<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Index page for view variable widget
 */
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

if(isset($entityLabel) && !empty($entityLabel)){
	echo Html::a($entityLabel,
		'#',
		$linkAttrs
	);
} else {
	echo Html::a($value, '#!', [ 
		'id' => 'view-variable-widget-'.$id,
		'class' => 'view-variable-widget',
		'data-label' => $value,
		'data-id' => $id,
		'data-target' => '#view-variable-widget-modal',
		'data-toggle' => 'modal',
		'title'=>\Yii::t('app','View variable information')]
	);
}

$viewVariableUrl = Url::to(['/variable/default/view-info']);
?>
<!-- modal for view variable -->
<div id="view-variable-widget-modal" class="fade modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div style="float:left"><h4><i class="fa fa-info-circle"></i> <span id="view-variable-label"></span></h4></div><div style="float:right"></div>
        </div>
        <div class="modal-body view-variable-widget-body">
            <div id="view-variable-widget-modal-body"></div>
        </div>

        <div class="modal-footer">
            <?= Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;' ?>
        </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
	var thisId = $id;

	// view variable info
    $('#view-variable-widget-'+thisId+',#view-variable-widget-'+thisId+'-more-info').on('click', function(e){

		var label = $(this).attr('data-label');
		var id = $(this).attr('data-id');

    	$('#view-variable-label').html(label);

    	$('#view-variable-widget-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

    	//load content of view variable information
	    setTimeout(function(){ 
	        $.ajax({
	            url: '$viewVariableUrl',
	            data: {
	                id: id
	            },
	            type: 'POST',
	            async: false,
	            success: function(data) {
	                $('#view-variable-widget-modal-body').html(data);
	            },
	            error: function(){
	                $('#view-variable-widget-modal-body').html('<i>There was a problem while loading record information.</i>'); 
	            }
	        });
	    }, 300);  
	});
JS
);

?>