<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Renders tabs for view variable information
 */
use yii\helpers\Url;

?>
<ul id="view-variable-tabs" class="tabs view-variable-info">
    <li class="tab col s3-view-info" title="<?= \Yii::t('app', 'Basic and additional information of variable')?>" name = "view-info-general">
        <a href="#" id="li-var-1" class="active a-tab var-a-tab variable-info-tab" data-mem="view-entity-var-1"><?= \Yii::t('app', 'General') ?></a>
    </li>
    <li class="tab col s3-view-info" title="<?= \Yii::t('app', 'Propery of the variable') ?>" name = "view-info-property">
        <a href="#" id="li-var-2" class="a-tab var-a-tab variable-info-tab" data-mem="view-entity-var-2"><?= \Yii::t('app', 'Property') ?></a>
    </li>
    <li class="tab col s3-view-info" title="<?= \Yii::t('app', 'Method of the variable') ?>">
        <a href="#" id="li-var-3" class="a-tab var-a-tab variable-info-tab" data-mem="view-entity-var-3"><?= \Yii::t('app', 'Method') ?></a>
    </li>
    <li class="tab col s3-view-info" title="<?= \Yii::t('app', 'Scale of the variable') ?>">
        <a href="#" id="li-var-4" class="a-tab var-a-tab variable-info-tab" data-mem="view-entity-var-4"><?= \Yii::t('app', 'Scale') ?></a>
    </li>
</ul>

<div id="view-entity-var-1" class="col s12 view-entity-content-var">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/variable/general.php',[
        'data' => $data['general']
    ]);
?>
</div>

<div id="view-entity-var-2" class="col s12 view-entity-content-var hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/variable/property.php',[
        'data' => $data['property']
    ]);
?>
</div>

<div id="view-entity-var-3" class="col s12 view-entity-content-var hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/variable/method.php',[
        'data' => $data['method']
    ]);
?>
</div>

<div id="view-entity-var-4" class="col s12 view-entity-content-var hidden">
<?php
    echo Yii::$app->controller->renderPartial('@app/widgets/views/variable/scale.php',[
        'data' => $data['scale']
    ]);
?>
</div>

<?php

$this->registerJs(<<<JS
   $(document).on("click",".variable-info-tab", function(e){
        e.preventDefault();
    
        var obj = $(this);
        var id = obj.attr('id');
        var dataMem = obj.attr('data-mem');

        $('.var-a-tab').removeClass("active");
        $('.view-entity-content-var').addClass('hidden');

        $('#'+dataMem).removeClass('hidden');
        $('#'+id).addClass('active');
   });
JS
);
?>
<style type="text/css">
.view-entity-content-var {
    margin-top: 10px;
}
</style>