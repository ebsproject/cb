<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Index page for view seed widget
 */
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

echo Html::a($label,
            '#',
            $linkAttrs
);

$viewSeedUrl = Url::to(['/seedInventory/find-seeds/view-info']);

?>
<!-- modal for view seed -->
<div id="view-seed-widget-modal" class="fade modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div style="float:left"><h4><i class="fa fa-info-circle"></i> <span id="view-seed-label"></span></h4></div><div style="float:right"></div>
        </div>
        <div class="modal-body view-seed-widget-body">
            <div id="view-seed-widget-modal-body"></div>
        </div>

        <div class="modal-footer">
            <?= Html::a(\Yii::t('app','Close'),'#',['data-dismiss'=>'modal','class'=>'btn btn-primary waves-effect waves-light modal-close']).'&emsp;' ?>
        </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
    var thisId = $id;

    // view seed info
    $('#view-seed-widget-'+thisId+',#view-seed-widget-'+thisId+'-more-info').on('click', function(e){
        var label = $(this).attr('data-label');
        var id = $(this).attr('data-id');

        $('#view-seed-label').html(label);

        $('#view-seed-widget-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');

        //load content of view seed information
        setTimeout(function(){
            $.ajax({
                url: '$viewSeedUrl',
                data: {
                    entity_id: id,
                    entity_type: 'seed',
                    entity_label: '$entityLabel'
                },
                type: 'POST',
                async: false,
                success: function(data) {
                    $('#view-seed-widget-modal-body').html(data);
                },
                error: function(){
                    $('#view-seed-widget-modal-body').html('<i>There was a problem while loading record information.</i>'); 
            }
            });
        }, 300);
    });
JS
);

?>