<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Renders other attributes assigned to seed
 */
use yii\grid\GridView;
?>
<p><?= \Yii::t('app', 'Below displays known attributes to seed.') ?></p>
<?= GridView::widget([
    'dataProvider' => $data,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-bordered white z-depth-3'],
    'columns' => [
        [
            'attribute' => 'variableDisplayName',
            'label' => 'Attribute',
            'value' => function ($model) {
                $displayName = null;
                switch ($model['variableAbbrev']) {
                    case 'MTA_NUMBER':
                        $displayName = 'MTA Number';
                        break;
                    case 'MTA_STATUS':
                        $displayName = 'MTA Status';
                        break;
                    case 'IP_STATUS':
                        $displayName = 'IP Status';
                        break;
                    case 'IMPORT_ATTRIBUTE':
                        $displayName = 'Import Attribute';
                        break;
                    case 'ORIGIN':
                        $displayName = 'Origin';
                        break;
                    case 'SOURCE_HARV_YEAR':
                        $displayName = 'Source Harvest Year';
                        break;
                    case 'SOURCE_ORGANIZATION':
                        $displayName = 'Source Organization';
                        break;
                    case 'SOURCE_STUDY':
                        $displayName = 'Source Study';
                        break;
                }
                return $displayName;
            }
        ],
        [
            'attribute' => 'attributeDataValue',
            'label' => 'Value'
        ]
    ]
]) ?>