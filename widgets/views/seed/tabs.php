<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
use yii\helpers\Url;

$user = \Yii::$container->get('app\models\User');
$userType = $user->isAdmin();

/**
 * Index page that renders seed information
 */

?>

<ul id="view-seed-tabs" class="tabs">
    <li class="tab col " title="<?= \Yii::t('app', 'Known attributes to seed') ?>">
        <a href="#" id="li-1" class="a-tab" title="<?= \Yii::t('app', 'Available attributes of the seed') ?>"><?= \Yii::t('app', 'Attributes') ?></a>
    </li>
</ul>
<div id="view-entity-1" class="col s12 view-entity-content hidden">
<?php
    echo Yii::$app->controller->renderAjax('@app/widgets/views/seed/attributes.php',[
        'data' => $data['attributes'],
    ]);
?>
</div>

<?php
$this->registerJS(<<<JS
    $('#li-1').trigger('click');
JS
);

$this->registerCss('
    #view-seed-tabs.tabs .tab a{
        padding: 0 20px !important;
    }
    .disabled-tab {
        color: grey !important;
    }
');
?>