#!/usr/bin/env bash

do_help() {
  cat <<EOF
Usage:

  $0 [-c|-f <path>] [-o <html|xml>] [-d] [-h]


Description:

  Run Codeception unit tests.
  Specify options to run specific tests, create tests or generate reports.

  THIS MUST BE EXECUTED WITHIN A RUNNING CONTAINER.

  -h  Display help
  -c  Create test template, given relative path and file name of the class or model under test
  -f  Run specific test/s. May be a path or file.
  -o  Generate code coverage in HTML or XML after running tests
  -d  Run test in debug mode which enables output to be printed in console with codecept_debug() lines


Sample usage:

  $0                                                          Run all tests
  $0 -d                                                       Run all tests in debug mode
  $0 -c modules/occurrence/models/Plot.php                    Create test template for Plot.php
  $0 -f modules/                                              Run all tests under modules/
  $0 -f modules/harvestManager/models/HarvestDataModel.php    Run only HarvestDataModel tests
  $0 -o html                                                  Run all tests and generate HTML coverage report

EOF
  exit 0
}

do_error() {
  echo "$1"
  exit 1
}


# Generate a unit test file given a PHP file path.
# The file must exist in cb in order to create the test.
create() {
  # Get file name from arguments
  FILE=$1
  FILE_PATH=${FILE//$PHP_EXTENSION/}

  if test -z "$FILE"; then # If FILE_PATH was not provided, warn user
    printf "${F_WHITE}${B_RED}Failed to generate unit test: No FILE_PATH (TEST) provided.${RESET}\n"
  else
    if test -f "$FILE"; then # If the php file exists, create the test file
      printf "${F_LIGHT_YELLOW}Generating test file...${RESET}\n"
      php vendor/bin/codecept generate:test unit $FILE_PATH
      # Modify test file permissions
      TEST_PATH="$TESTS_DIR${FILE/.php/Test.php}"
      chmod -R 777 $TEST_PATH
    else # If the php file does not exists, warn user
      printf "${F_WHITE}${B_RED}Failed to generate unit test: File $FILE does not exist.${RESET}\n"
    fi
  fi

  exit 0
}

# Run unit tests
execute() {
  # Get file name and flags from arguments
  FILE=$1
  FILE=${FILE/.php/Test.php}
  REPORT=$2
  DEBUG=$3

  if [[ $REPORT == "$HTML" ]]; then
    REPORT='--coverage-html'
  elif [[ $REPORT == "$XML" ]]; then
    REPORT='--coverage-xml'
  else
    echo "Skipping coverage report generation"
  fi

  [[ -n $DEBUG ]] && DEBUG='--debug'

  # Run tests
  php vendor/bin/codecept run unit "$FILE" $REPORT $DEBUG

  exit 0
}

## FORMATTING
RESET='\033[0m'
## COLORS - FOREGROUND
F_LIGHT_YELLOW='\033[93m'
F_WHITE='\033[97m'
## COLORS - BACKGROUND
B_RED='\033[41m'

## OTHER VARIABLES
PHP_EXTENSION='.php'
TESTS_DIR='tests/unit/'
HTML='html'
XML='xml'

while getopts ":c:f:o:dh" opt; do
  case "$opt" in
    c) new_test=$OPTARG;;
    f) test=$OPTARG;;
	  o)
	    output=$OPTARG
	    [[ "$output" == "$HTML" || "$output" == "$XML" ]] \
	      || do_error "Coverage report type must either be 'html' or 'xml', '$output' found instead."
	    ;;
	  d) debug=true;;
	  h) do_help;;
    *) do_error "See $0 -h for options";;
  esac
done
shift $((OPTIND-1))

[[ -n "$new_test" ]] && create "$new_test"
execute "$test" "$output" "$debug"
