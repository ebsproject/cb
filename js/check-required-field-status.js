/**
 * Checks if the required fields in a gridview has been populated
 */
function checkStatus(){
  $.ajax({
      url: checkStatusUrl,
      type: 'post',
      dataType: 'json',
      data: {
          configValues: requiredFieldJson
      },
      success: function(response) {
          if(checkStatValue.includes("|")){
              var disabled = true;
              var statusArray = checkStatValue.split("|");
              for(var i =0; i<statusArray.length; i++){
                  if(response.includes(statusArray[i])){
                      disabled = false;
                      break;
                  }
              }
              if(disabled){
                  $('#next-btn').attr('disabled', 'disabled');
                  updateTabs();
              } else {
                  $('#next-btn').removeAttr('disabled');
              }
          } else {
              if(response.includes(checkStatValue)){
                  $('#next-btn').removeAttr('disabled');
              } else{
                  $('#next-btn').attr('disabled', 'disabled');
                  updateTabs();
              }
          }
      },
      error: function() {
      }
  });
}


function updateTabs(){
    let activeListToggle = document.getElementsByClassName('a-tab');
    let activeTab = null;
    for (let i = 0; i < activeListToggle.length; i++) {
        if(activeListToggle[i].className == 'active a-tab'){
            activeTab = i;
            continue;
        } else if(activeTab != null){
            if(i > activeTab){
                activeListToggle[i].href = '#';
                activeListToggle[i].className = 'a-tab disabled';
            }
        }
    }
}