/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* This contains the js scripts for the dynamic fields in the modal defined in the configuration
*/

 function getUrlVars() {
     var vars = {};
     window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
         vars[key] = value;
     });
     return vars;
 }

 function checkField(){
     var response = [];
     response['success'] = true;
     response['required'] = null;
     response['datatype'] = null;
     var tempArr = [];
     var count = 0;
     $(".required-field").each(function(){ 
         var fieldIdTemp = this.id;

         if($(this).val() == '' && (fieldIdTemp.indexOf('input') == -1)){
             response['success'] = false;
             var strArr = (this.id).split('-');
             var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
             var count = tempArr.length;
             tempArr.push((count+1)+'. '+label);
         }
     });
     response['required'] = tempArr.join("<br>");

     tempArr = [];
     count = 0;
     $(".invalid").each(function(){
         response['success'] = false;
         var strArr = (this.id).split('-');
         var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
         count = tempArr.length;
         tempArr.push((count+1)+'. '+label);
         
     });
     response['datatype'] = tempArr.join("<br>");
     return response;
 }