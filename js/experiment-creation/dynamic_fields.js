/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* This contains the js scripts for the dynamic fields defined in the configuration
*/

//This is for the dynamic fields with integer input type
$(document).on('change', '.numeric-req-input', function(e, params){
    e.preventDefault();


    var inputVal = this.value;
    key = ($(this).attr('id')).split("-")[1];
    var variable = ($(this).attr('id')).split("-")[0];

    var max = $(this).attr('max');
    var min = $(this).attr('min');
    var label = $(this).attr('data-label');
    label = label.trim();
    
    var isSaved = '';
    
    if((label != 'year' && label != 'YEAR') && (max != null && max != '')){
        //Check if input is greater than max
        if(inputVal > max){
            isSaved = false;
        }else{
            isSaved = true;
        }
    }
    
    if(label == 'year' || label == 'YEAR'){
        if (inputVal != 0) {
            
            if (inputVal.length != 4) {
                isSaved = false;
                $(this).addClass('has-error');
                $(this).find(".help-block").html('Incorrect year format. Please check.');
            }else{
                isSaved = true;
                $(this).removeClass('has-error');
                $(this).find(".help-block").html('');
            }
            var currentYear=new Date().getFullYear();
            if((inputVal < 1920) || (inputVal > currentYear))
            {
                isSaved = false;
                $(this).addClass('has-error');
                $(this).find(".help-block").html('Year value should be between 1920 to current year.');
            }else{
                isSaved = true;
                $(this).removeClass('has-error');
                $(this).find(".help-block").html('');
            }variable == 'year' || variable == 'YEAR'
        }else{
            isSaved = false;
            $(this).addClass('has-error');
            $(this).find(".help-block").html('Year cannot be empty.');
        } 
    }
    
    
    var page = '';
    var perPage = '';

    var element = document.getElementById("grid-location-id");
  
    
    //If it isn't "undefined" and it isn't "null", then it exists.
    if(typeof(element) != 'undefined' && element != null){
        if(typeof(gridId) != 'undefined' && gridId != null){
            var entryIdArray = $('#'+gridId).yiiGridView('getSelectedRows');
        }else{
            var entryIdArray = $('#grid-location-id').yiiGridView('getSelectedRows'); 
        }
    } else{
        //Changes triggers using input of the user
        if(typeof(gridId) != 'undefined' && gridId != null){
            isSaved = true;
        }else{
            //Entry List browser's id
            var entryIdArray = $('#grid-entry-list').yiiGridView('getSelectedRows');
            isSaved = true;
        }
    }
 
    if(typeof page != undefined && (typeof perPage != undefined)){
        page = getUrlVars()['page'];
        perPage = getUrlVars()['per-page'];
    }

    var dp1_page = '';
    var dp1_perPage = '';

    if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
        dp1_perPage = getUrlVars()['dp-1-per-page'];
    }

    if(typeof (getUrlVars()['dp-1-page']) != undefined){
        dp1_page = getUrlVars()['dp-1-page'];
    }
    
    if(parseInt(this.value) || parseFloat(this.value)){
        if(this.hasAttribute('step')){
            inputVal = parseFloat(this.value);
        } else {
            inputVal = parseInt(this.value);
        }
        if(max != null && max != ''){
            //Check if input is greater than max
            if((inputVal > max) && (inputVal < min)){
            
                isSaved = false;
            }else{
               
                isSaved = true;
            }
        }
        //Check minimum input
        if(min != null && min != ''){
            //Check if input is less than max
            if(inputVal < min && (max != null && max != '' && inputVal > max)){
               
                isSaved = false;
            }else if(max != null && max != '' && inputVal > max){
               
                isSaved = false;
            }else{
              
                isSaved = true;
            }
        }

        if((isSaved || isSaved == 'true') && this.value >= min){
            
            $.ajax({
                url: genericConfUpdateUrl,
                type: 'POST',
                dataType: 'json',
                cache: false,
                data:{
                    entryIdArray: [key],
                    page: page,
                    perPage: perPage,
                    dp1_page: dp1_page,
                    dp1_perPage: dp1_perPage,
                    experimentId: experimentId,
                    variable_abbrev: variable,
                    updateType: 'selected',
                    inputVal: this.value,
                    label: '',
                    isRow: true,
                    dataMemtype : $(this).attr('data-memtype')
                },
                success: function(response) {
                },
            });
        }
    }
});

//This is for the dynamic field with decimal inputs
$('.decimal-req-input').on('change',function(e, params){
    
    key = ($(this).attr('id')).split("-")[1];
    var variable = ($(this).attr('id')).split("-")[0];

    if(parseInt(this.value) || parseFloat(this.value)){
        if(this.hasAttribute('step')){
            inputVal = parseFloat(this.value);
        } else {
            inputVal = parseInt(this.value);
        }


        var page = '';
        var perPage = '';
        
        var element = document.getElementById("grid-location-id");
    
        //If it isn't "undefined" and it isn't "null", then it exists.
        if(typeof(element) != 'undefined' && element != null){
        
            var entryIdArray = $('#grid-location-id').yiiGridView('getSelectedRows');
        } else{
        
            var entryIdArray = $('#grid-entry-list').yiiGridView('getSelectedRows');    
        }

        if(typeof page != undefined && (typeof perPage != undefined)){
            page = getUrlVars()['page'];
            perPage = getUrlVars()['per-page'];
        }

        var dp1_page = '';
        var dp1_perPage = '';

        if(typeof (getUrlVars()['dp-1-per-page']) != undefined){
            dp1_perPage = getUrlVars()['dp-1-per-page'];
        }

        if(typeof (getUrlVars()['dp-1-page']) != undefined){
            dp1_page = getUrlVars()['dp-1-page'];
        }

        var max = $(this).attr('max');
        var min = $(this).attr('min');
    
        var isSaved = true;
        if(max != null && max != ''){
            //Check if input is greater than max
            if((inputVal > max) && (inputVal < min)){
            
                isSaved = false;
            }else{
                
                isSaved = true;
            }
        }
        //Check minimum input
        if(min != null && min != ''){
            //Check if input is less than max
            if(inputVal < min && (max != null && max != '' && inputVal > max)){
               
                isSaved = false;
            }else if(max != null && max != '' && inputVal > max){
               
                isSaved = false;
            }else{
               
                isSaved = true;
            }
        }
        
        if(isSaved){
            $.ajax({
                url: genericConfUpdateUrl,
                type: 'POST',
                dataType: 'json',
                data:{
                    entryIdArray: [key],
                    page: page,
                    perPage: perPage,
                    dp1_page: dp1_page,
                    dp1_perPage: dp1_perPage,
                    experimentId: experimentId,
                    variable_abbrev: variable,
                    updateType: 'selected',
                    inputVal: inputVal,
                    label: '',
                    isRow: true,
                    dataMemtype : $(this).attr('data-memtype')
                },
                success: function(response) {
                },
            }); 
        }
    }
});


 //Restric integer inputs to integer only without decimals
$(".numeric-req-input").on("keypress keyup blur paste",function (event) { 
    var max = $(this).attr('max');
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }      
});


$(".decimal-req-input").on("keypress keyup blur paste",function (event) {  
    var key = window.event ? event.keyCode : event.which;
     
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
});

function checkFields(){
    var response = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var count = 0;
    
    var arr_label = [];
    var init_fieldId = [];
    var init_fieldIdTxt = [];
    var init_field = [];
    
    var elemTest = $('.required-field').val();

    var reqVarConfig = '';

    if(typeof requiredFieldJson != 'undefined'){
        reqVarConfig = requiredFieldJson;
    }
    
    if(typeof elemTest === 'undefined' && typeof genericCheckReqVarsUrl != 'undefined'){ 
       $.ajax({
           url: genericCheckReqVarsUrl,
           type: 'POST',
           dataType: 'json',
           data: {
              reqVarConfig: reqVarConfig
           },
           cache: false,
           async: false,
           success: function(dataRes){
               if(dataRes!=null && dataRes != ''){
                  response['success'] = false;
                  tempArr = dataRes;
                  response['required'] = tempArr.join("<br>");
               }
           }
       });
       
       return response;
    }else{
        $(".kv-editable").each(function(){
            var elem = document.getElementById(this.id);
            var idTemp = (this.id).split('-cont');
            idTemp = idTemp[0];
          
            init_fieldId.push(idTemp);
            init_fieldIdTxt.push(elem.innerText);
          
            init_field[idTemp] = elem.innerText;  
        });

        $(".required-field").each(function(){
            var reqId = this.id;
            var initCheck = '';
          
            if((init_fieldId.indexOf(reqId)) > -1){
                initCheck = init_field[reqId];
            }else{
                initCheck = $(this).val();
            }
            
            if(initCheck == '' || initCheck.includes("not set") ){
                response['success'] = false;
                var strArr = (this.id).split('-');
                var $strIdArr =  $('#'+strArr[0]).attr('name');
                
                if(typeof $strIdArr === 'undefined'){
                    var fieldName = strArr;
                }else{
                    var fieldName = $strIdArr.split('_value');
                }
          
                var label = fieldName[0].replace(/[&\/\\#, +()$~%._'":*?<>{}]/g, ' '); 
    
                if(label.includes("parent type")){
                    label = 'parent role';
                }
               
                var count = tempArr.length;
               
                if(arr_label.indexOf(label) > -1){
                    
                }else{
                    tempArr.push((count+1)+'. '+label);
                    arr_label.push(label);
                }
            }
        });
 
        response['required'] = tempArr.join("<br>");
    
        tempArr = [];
        count = 0;

        $(".invalid").each(function(){
            response['success'] = false;
            var strArr = (this.id).split('-');
            var $strIdArr =  $('#'+strArr[0]).attr('name');
        
            if(typeof $strIdArr === 'undefined'){
                var fieldName = strArr;
            }else{
                var fieldName = $strIdArr.split('_value');
            }
    
            var label = fieldName[0].replace(/[&\/\\#, +()$~%._'":*?<>{}]/g, ' '); 
    
            var count = tempArr.length;
          
            if(arr_label.indexOf(label) > -1){
          
            }else{
                tempArr.push((count+1)+'. '+label);
                arr_label.push(label);
            } 
        });
        response['datatype'] = tempArr.join("<br>");

        return response;
    }
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
