/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* This contains the js scripts for the dynamic fields in the modal defined in the configuration
*/

var variable = '';
$(document).on('change','#req-vars',function(){
    variable = $(this).val();
  
    if(variable != ""){
         $.ajax({
             url: genericRenderConfigUrl,
             type: 'post',
             dataType: 'json',
             data:{
                selected: variable, 
                configVars: requiredFieldJson,
                formType: 'modal'
            },
             success: function(response){
                $('#input-field-var').html(response);
                $('.select-input').css('visibility','visible');
                $('.select-input').css('width','');
                $('.select-input').css('height','');
             },
             error: function(){
                 
             }
         });
    }else{
     $('#input-field-var').html('');
    }
});

$(document).on('click','.confirm-update-btn',function(e){
    var selectedItems= [];
    var selectedItemsCount = 0;
    var mode = $(this).attr('mode');
    var isBgUsedFlag = false;

    $.ajax({
        url: getSelectedItemsUrl,
        type: 'post',
        dataType: 'json',
        data:{
            experimentDbId : experimentId,
            action: renderedView
        },
        success: function(response) {
            if(mode == 'current-page'){
                var entryData = selectCurrentPage();
   
                if(typeof entryData['entryIds'] != undefined){
                    selectedItems = entryData['entryIds'];
                }

                if(typeof entryData['entryIdsCount'] != undefined){
                    selectedItemsCount = entryData['entryIdsCount'];
                }
            }else{
                selectedItems = response['selectedItems'];
                selectedItemsCount = response['selectedItemsCount'];
            }
            
            if((selectedItemsCount > 0 && selectedItemsCount !== 'No' && (mode == "selected" || mode == "current-page")) || mode == "all"){
                //no variable selected 
                if(typeof variable === 'undefined' || variable == null || variable == ''){
                    //hide loading indicator
                    $('#modal-content').removeClass('hidden');
                    $('#modal-progress').addClass('hidden');
                    $('.confirm-update-btn').removeClass('disabled');
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No variable selected.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }else{
                    if(mode == 'all' && response['totalCount'] == 0){
                         //hide loading indicator
                        $('#modal-content').removeClass('hidden');
                        $('#modal-progress').addClass('hidden');
                        $('.confirm-update-btn').removeClass('disabled');
                        var hasToast = $('body').hasClass('toast');
                        if(!hasToast){
                            $('.toast').css('display','none');
                            var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No entry records yet. Please import entry records.</span>";
                            Materialize.toast(notif, 5000);
                        }
                        e.preventDefault();
                        e.stopImmediatePropagation();
                    }
                    else {
                        var field = checkField();
                        if(field['success']){
                            //show loading indicator
                            $('#modal-content').removeClass('hidden');
                            $('#modal-progress').removeClass('hidden');
                            $('.confirm-update-btn').addClass('disabled');

                            if(renderedView != null && renderedView == 'specify-occurrences'){
                                var action = 'updateOccurrences';
                                var toolStep = 'occurrences';
                            }else{
                                var action = 'updateEntries';
                                var toolStep = 'entries';
                            }
                            
                            //hide close button
                            $("#close-bulk-update-btn").addClass("hidden");
                            $(".close").addClass("hidden");

                            //Notification while background job is being created
                            $.ajax({
                                url: checkThresholdCountUrl,
                                type: 'post',
                                dataType: 'json',
                                data: {
                                    action: action,
                                    toolStep: toolStep,
                                    selectedItemsCount: selectedItemsCount,
                                    method: 'update',
                                    mode: mode
                                },
                                success: function(response) {
                                    isBgUsedFlag = response;
                              
                                    if(isBgUsedFlag){
                                        $('#modal-content').html('<div class="alert ">'+ 
                                                '<div class="card-panel" style="color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">'+
                                                    '<i class="fa fa-info-circle"></i>'+
                                                      ' A background job is being created.' + 
                                                '</div></div>');
                                    }
                                }
                            });
                            
                            $.ajax({
                                url: bulkUpdateFieldsUrl,
                                type: 'post',
                                dataType: 'json',
                                data: {
                                    mode: mode,
                                    variable: variable,
                                    dataValue: field['value'],
                                    endpoint: field['endpoint'],
                                    experimentDbId: experimentId,
                                    renderedView: renderedView,
                                    targetColumn: field['targetColumn'],
                                    selectedItems: selectedItems
                                },
                                success: function(response) {
                                    endpoint = field['endpoint'] == 'entry-data' ? 'entry_data' : 'entry';
                                    variable = response['variable'];

                                    if(renderedView == 'specify-occurrences'){
                                        if(variable == 'SITE' || variable == 'FIELD'){
                                            $.pjax.reload({
                                                container: '#dynagrid-ec-site-list-pjax'
                                            }).done(function(){
                                                //Retain the selected checkbox here
                                                retainSelectedChecks();
                                            });
                                            if(variable == 'SITE'){
                                                $('#next-btn').removeAttr('disabled');
                                            }
                                        }else{
                                            $.each(response['selectedItems'], function(index, value){
                                                if(field['inputType'] == 'dropdown'){
                                                    $('#select2-' + variable +'-'+response['variableType']+ '-' + response['variableDbId'] + '-' + value + '-container').html(field['valueText']).trigger('change');
                                                }
                                                else{
                                                    $('#' + variable +'-'+response['variableType']+ '-' + response['variableDbId'] + '-' + value).val(field['value']);
                                                }
                                            });
                                        }
                                    }else{
                                        checkStatus();
                                        $.each(response['selectedItems'], function(index, value){
                                            if(field['inputType'] == 'dropdown'){
                                                $('#select2-' + variable + '-' + endpoint + '-' + value + '-container').html(field['value']);
                                            }
                                            else{
                                                $('#' + variable + '-' + endpoint + '-' + value).val(field['value']);
                                            }
                                        });
                                        if(renderedView == 'specify-entry-list') {
                                            $.pjax.reload({
                                                container: '#dynagrid-ec-entry-list-pjax',
                                                replace:false
                                            })
                                        }
                                    }
                              
                                    if(!hasToast){
                                        $('.toast').css('display','none');
                                        var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated <b>"+response['count']+"</b> items.</span>";
                                        Materialize.toast(notif, 5000);
                                        $("[class*=input-validate]").val(null).trigger("change"); 
                                        $("[class*=input-validate]").select2("val", "");
                                    }

                                    //show close button
                                    $("#close-bulk-update-btn").removeClass("hidden");
                                    $(".close").removeClass("hidden");

                                    //hide loading indicator
                                    $('#modal-content').removeClass('hidden');
                                    $('#modal-progress').addClass('hidden');
                                    $('.confirm-update-btn').removeClass('disabled');
                                },
                                error: function(e) {
                                    console.log(e);
                                }
                            });
                            
                        }
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            }
            else{//no items selected
                //hide loading indicator
                $('#modal-content').removeClass('hidden');
                $('#modal-progress').addClass('hidden');
                $('.confirm-update-btn').removeClass('disabled');
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'> No items selected.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }

        },
        error: function(e) {
            console.log(e);
        }
    });
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

//To retain the selected checks in the browser after pjax reload in the SITE tab
function retainSelectedChecks(){
    $.ajax({
        url: getSelectedItemsUrl,
        type: 'post',
        dataType:'json',
        cache: false,
        data: {
            experimentDbId:experimentId,
            action:'occurrences'
        },
        async: false,
        success: function(response) {
            //Get the total count of the dataProvider
            var dataProviderCount = '$dataProviderCount'; 
            if(response['selectedItems'].length != 0){
                occurrenceIdArray = response['selectedItems'];

                if(dataProviderCount == occurrenceIdArray.length){
                    $('#occurrence-select-all').prop('checked',true);
                    $('#occurrence-select-all').attr('checked', 'checked');
                }
           
                $.each(occurrenceIdArray, function(key,value){
                    $('#'+value).attr('checked','checked');
                    $('#'+value).prop('checked',true);
                    $("#"+value+":checkbox").parent("td").parent("tr").addClass("grey lighten-4");
                });
            }  
        }
    });
}
 //Get the entries of the current page
 function selectCurrentPage(){
    
    var page = '';
    var perPage = '';
    var entryData = '';

  
    if(typeof page != undefined && (typeof perPage != undefined)){
        page = getUrlVars()['grid-entry-list-grid-page'];
    }
    
    $.ajax({
        url: getEntriesCurrentPageUrl,
        type: 'post',
        dataType: 'json',
        data: {
            page: page,
            urlParams: window.location.href
        },
        async: false,
        cache: false,
        success: function(response) {
            entryData = response;
            var entryIdCount = response['entryIdsCount'];
        },
        error: function(response){
        }
    });
  
    return entryData;
}