/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

// This contains the basic form validations


$('.numeric-input-validate').on('change', function(e, params){
    
    var inputVal = this.value;
    key = ($(this).attr('id')).split("-")[1];
    var variable = ($(this).attr('id')).split("-")[0];
    var year_min_value = parseInt('$year_min_value');
    var year_max_value = parseInt('$year_max_value');
    
    if(year_min_value == '' || year_min_value == null){
        year_min_value = 1920;
    }

    if(year_max_value == '' || year_max_value == null){
        year_max_value = '';
    }
    
    if(variable == 'year' || variable == 'YEAR'){
        if (inputVal != 0) {
            if (inputVal.length != 4) {
                isSaved = false;
                $(this).addClass('invalid');
                $(this).attr('title', 'Incorrect year format. Please check.');
            }else{
                isSaved = true;
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
            }

            if((inputVal < year_min_value))
            {   
                isSaved = false;
                
                $(this).addClass('invalid');
                $(this).attr('title', 'Year value should start from '+ year_min_value +'.');
            }else{
                isSaved = true;
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
            }
        }else{
            isSaved = false;
            $(this).addClass('invalid');
            $(this).attr('title', 'Year cannot be empty.');
        } 
    }
});


function checkFields2(){
    var response = [];
    response['success'] = true;
    response['required'] = null;
    response['datatype'] = null;
    var tempArr = [];
    var count = 0;
    $(".required-field").each(function(){
        if($(this).val().length == 0){
            response['success'] = false;
            var strArr = (this.id).split('-');
            var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
            var count = tempArr.length;
            tempArr.push((count+1)+'. '+label);
        }
    });
    response['required'] = tempArr.join("<br>");

    tempArr = [];
    count = 0;
    $(".invalid").each(function(){
        response['success'] = false;
        var strArr = (this.id).split('-');
        var label =  $('label[for=' + strArr[0] + ']').attr("data-label");
        count = tempArr.length;
        tempArr.push((count+1)+'. '+label);
        
    });
    response['datatype'] = tempArr.join("<br>");
    return response;
}

$("input[type='number']" ).on( "change", function(){
    var min, max, value = null;
    var key = ($(this).attr('id')).split("-")[1];
    var variable = ($(this).attr('id')).split("-")[0];
    var year_min_value = parseInt('$year_min_value');
    var year_max_value = parseInt('$year_max_value'); 

    if(parseInt(this.value) || parseFloat(this.value)){
        if(this.hasAttribute('step')){
            value = parseFloat(this.value);
        } else {
            value = parseInt(this.value);
        }
        if(this.hasAttribute('min') && this.hasAttribute('max')){
            min = $(this).attr('min');
            max = $(this).attr('max');
            
            if(variable == 'year' || variable == 'YEAR'){
                if(year_min_value == '' || year_min_value == null){
                    min = 1920;
                }else{
                    min = year_min_value;
                }
             
                if(year_max_value == '' || year_max_value == null || isNaN(year_max_value)){
                    max = '';
                }else{
                    max = year_max_value;
                }
            }
          
            if((value < min || value > max) && (max != null && max != '' && max != 0)){
                $(this).addClass('invalid');
                $(this).attr('title', 'Input should be greater than '+min+' and less than '+max);
            }else if((value < min || value > max) && (max == null || max == '' || max == 0)){
                //Do nothing
            }else{
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
            }
        }
        else if(this.hasAttribute('min')){
            min = $(this).attr('min');
            if(variable == 'year' || variable == 'YEAR'){
                if(year_min_value == '' || year_min_value == null){
                    min = 1920;
                }else{
                    min = year_min_value;
                }
            }
            if(value < min){
                $(this).addClass('invalid');
                $(this).attr('title', 'Input should be greater than '+min);
            } else {
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
            }
            
        } else if(this.hasAttribute('max')){
            max = $(this).attr('max');
            if(variable == 'year' || variable == 'YEAR'){
                if(year_max_value == '' || year_max_value == null){
                    max = '';
                }else{
                    max = year_max_value;
                }
            }
            if(value > max){
                $(this).addClass('invalid');
                $(this).attr('title', 'Input should be less than '+max);
            }else {
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
            }
            
        } else{
            $(this).removeClass('invalid');
            $(this).removeAttr('title');
        }
    } else if(this.value != ''){
        // not a number
        $(this).addClass('invalid');
        $(this).attr('title', 'Input should be numeric');
    }
    
});