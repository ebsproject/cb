/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

// Notification functions
function manageNotifs(){
    notifDropdown();

    $('#notif-container').html('<ul id="notifications-dropdown" class="dropdown-content" style="display: none;"></ul>');

    renderNotifications();
    updateNotif();
}

/**
 * Initialize dropdown materialize for notifications
 */
function renderNotifications(){
    $('#seeds-search-notifs-btn').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
 * Shows new notifications
 */
function updateNotif() {
    $.getJSON(
        ssNewNotificationsUrl, 
        {
            workerName: ssWorkerNames,
            remarks: 'seed search'
        },
        function(json){
            $('#ss-notif-badge-id').html(json);

            if(parseInt(json)>0){
                $('#ss-notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#ss-notif-badge-id').html('');
                $('#ss-notif-badge-id').removeClass('notification-badge red accent-2');
            }
        }
    )
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Message: "+textStatus+" : "+errorThrown);
    });
}

/**
 * Displays the notification in dropdown UI
 */
function notifDropdown(){
    $('#seeds-search-notifs-btn').on('click',function(e){
        $.ajax({
            url: ssNotificationsUrl,
            type: 'post',
            data: {
                workerName: ssWorkerNames,
                remarks: 'working list'
            },
            async:false,
            success: function(data) {
                var pieces = data.split('||');
                var height = pieces[1];

                $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');

                data = pieces[0];
                renderNotifications();

                $('#ss-notif-badge-id').html('');
                $('#ss-notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){
                    var processId = $(this).data('process-id')
                    var workerName = $(this).data('worker-name')

                    var refPageUrl = ssRefreshBrowserUrl

                    $.pjax.reload({
                        container:'#'+ssgridId+'-pjax',
                        url: refPageUrl,
                        replace: false,
                        type: 'POST',
                        data: {
                            forceReset:true
                        }
                    });
                });
            },
            error: function(xhr, status, error) {
                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                '<i class="material-icons red toast-close-btn">close</i></button>' +
                '<ul class="collapsible toast-error grey lighten-2">' +
                '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + 
                ' </blockquote></span></div>' +
                '</li> </ul>');

                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    });
}