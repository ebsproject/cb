/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function() {
    checkboxActionsFS();
    checkDefaultValues();
});

$(document).on('ready pjax:success','#'+qResultsPjaxGridId, function(e) {
    e.stopPropagation();
    e.preventDefault();
    checkboxActionsFS();
    checkDefaultValues();
});

function checkboxActionsFS() {

    // store default values in session storage
    $.each($('.qr-default-val'), function(i, v) {
        var value = $(v).val();
        saveSelectionFS('select', value);
        $(v).removeClass('qr-default-val');
    });

    // set checkbox to checked if value is in session
    var selectedRows = getSelectionFS();
    displayTotalCountFS(selectedRows);

    // select all items in browser
    $('#'+qResultsSelectAllId).on('click',function(e){

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+qResultsSelectClass).attr('checked','checked');
            $('.'+qResultsSelectClass).prop('checked',true);
            $("."+qResultsSelectClass+":checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            saveSelectionFS('selectAll');
        }else{
            $(this).removeAttr('checked');
            $('.'+qResultsSelectClass).prop('checked',false);
            $('.'+qResultsSelectClass).removeAttr('checked');
            $("input:checkbox."+qResultsSelectClass).parent("td").parent("tr").removeClass("grey lighten-4");      
            saveSelectionFS('deselectAll');
        }
        var count = getSelectionFS().length;
        if(count > 0){
            $("#add-to-list-btn").removeAttr('disabled');
        } else {
            $('#add-to-list-btn').attr('disabled',true);
        }
    });

    $(".seed-inventory-radio-btn").on('click', function(e) {
        var current = $(this);
        var name = $('input[id='+current.prop('id')+']:checked').attr('name');
        var value = current.val();
        var exisitingGroupSelection = null;

        if (current.hasClass('isClicked')) {
            current.prop('checked', false);
            current.removeClass('isClicked');
            saveSelectionFS('deselect', value);
        } else {
            current.addClass('isClicked');
            current.prop('checked', true);
            
            $("#"+qResultsGridId+' input[name="'+name+'"]:radio:not(:checked)').each(function(index,value){
                var radioItem = $('#'+value.id);
                if(radioItem.hasClass('isClicked')){ exisitingGroupSelection = radioItem.val()}
            });
            if(exisitingGroupSelection != null){
                $('#seed-radio-id-'+exisitingGroupSelection).removeClass('isClicked');
                saveSelectionFS('deselect', exisitingGroupSelection);
            }
            saveSelectionFS('select', value);       
        }
        var count = getSelectionFS().length;
        if(count > 0){
            $("#add-to-list-btn").removeAttr('disabled');
        } else {
            $('#add-to-list-btn').attr('disabled',true);
        }
    });

    $(".seed-inventory-seedlotCode_checkbox").on('click', function(e) {
        this_check=$(this)[0];
        var checkboxObj = $(this);
        var value = $(this_check).val();

        if(!this_check.checked){
            saveSelectionFS('deselect', value);
            $('#'+qResultsSelectAllId).prop("checked", false);
            checkboxObj.parent("td").parent("tr").removeClass("grey lighten-4");
        } else {
            saveSelectionFS('select', value);
            checkboxObj.parent("td").parent("tr").addClass("grey lighten-4");

            $("#"+this_check.id).prop("checked");
            var isAllChecked = 0;
            $(".seed-inventory-seedlotCode_checkbox").each(function() {
                if (!this.checked)
                    isAllChecked = 1;
            });

            if (isAllChecked == 0) {
                $("#"+qResultsSelectAllId).prop("checked", true);
            }
        }
        var count = getSelectionFS().length;
        if(count > 0){
            $("#add-to-list-btn").removeAttr('disabled');
        } else {
            $('#add-to-list-btn').attr('disabled',true);
        }
    });

    $("#search-grid-table-con tbody tr").on('click', function(e) {
        let isMultipleSelect = $('#selectOption-switch-id').is(':checked');
        let thisRowCheck = $(this).find('input:checkbox')[0];
        let thisRowRadio = $(this).find('input:radio')[0];
        let rowId = null;
        let value = null;
        let isSelected = false;
        
        if(thisRowCheck != undefined || thisRowRadio != undefined){
            if(thisRowCheck != undefined && isMultipleSelect){
                rowId = thisRowCheck.id;
                value = $("#"+rowId).val();
                isSelected = $("#"+rowId).is(':checked')
                
                if(isSelected){
                    saveSelectionFS('deselect', value);

                    thisRowCheck.checked = true;
                    $('#'+rowId).removeAttr('checked');
                }
                else{
                    saveSelectionFS('select', value);

                    thisRowCheck.checked = true;
                    $('#'+rowId).attr('checked','checked');
                }
            }

            if(thisRowRadio != undefined && !isMultipleSelect){
                var exisitingGroupSelection = null;
                
                rowId = thisRowRadio.id;
                name = thisRowRadio.name;
                value = $("#"+rowId).val();
                isSelected = $("#"+rowId).hasClass('isClicked');
                
                if(isSelected){
                    saveSelectionFS('deselect', value);
                    $("#"+rowId).removeClass('isClicked');
                }
                else{
                    $("#"+rowId).addClass('isClicked');
                    
                    // deselect previously selected radio button
                    $("#"+qResultsGridId+' input[name="'+name+'"]:radio').each(function(index,value){
                        var radioItem = $('#'+value.id);
                        if(radioItem.hasClass('isClicked') && radioItem.val() != $("#"+rowId).val()){ exisitingGroupSelection = radioItem.val()}
                    });
                    
                    if(exisitingGroupSelection != null){
                        $('#seed-radio-id-'+exisitingGroupSelection).removeClass('isClicked');
                        $('#seed-radio-id-'+exisitingGroupSelection).parent("td").parent("tr").removeClass("grey lighten-4");
                        $('#seed-radio-id-'+exisitingGroupSelection).parent("td").parent("tr").removeClass("success1");

                        saveSelectionFS('deselect', exisitingGroupSelection);
                    }
                    saveSelectionFS('select', value);
                }
            }
        }

        if(rowId){
            if(isSelected){
                $('#' + rowId).parent("td").parent("tr").removeClass("grey lighten-4");
                $('#' + rowId).parent("td").parent("tr").removeClass("success1");
                $('#' + rowId).prop('checked',false);
            }
            else{
                $('#' + rowId).parent("td").parent("tr").addClass("grey lighten-4");
                $('#' + rowId).parent("td").parent("tr").addClass("success1");
                $('#' + rowId).prop('checked',true);
            }
        }

        var count = getSelectionFS().length;
        if(count > 0){
            $("#add-to-list-btn").removeAttr('disabled');
        } else {
            $('#add-to-list-btn').attr('disabled',true);
        }
    });
}


/**
 * Saves the selection on sessionStorage
 */
function saveSelectionFS(mode, value) {

    var currentSelection = sessionStorage.getItem(qResultsSessionStorage);

    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    if (mode == 'selectAll') {
        $('#'+qResultsGridId).find("input:checkbox."+qResultsSelectClass).each(function() {
            if ($(this).prop("checked") === true) {
                var value = $(this).val();

                if ($.inArray(value, currentSelection) === -1) {
                    currentSelection.push(value);
                }
            }
        });
    } else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    } else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    } else if (mode == 'deselectAll') {
        $('#'+qResultsGridId).find("input:checkbox."+qResultsSelectClass).each(function() {
            if ($(this).prop("checked") === false) {
                var value = $(this).val();
                var index = currentSelection.indexOf(value);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }

    displayTotalCountFS(currentSelection);
    sessionStorage.setItem(qResultsSessionStorage, JSON.stringify(currentSelection));
}

/**
 * Displays the total count summary if placeholder id is given
 */
function displayTotalCountFS(currentSelection) {
    
    if (qResultsDisplaySummaryId !== '') {
        if (currentSelection === null) {
            $('#'+qResultsDisplaySummaryId).html('');
            return true;
        }
        var totalCount = currentSelection.length;
        
        if (totalCount != 0) {
            $('#'+qResultsDisplaySummaryId).html(
                '<b><span id="selected-items-count"> '+totalCount+
                '</span></b> <span id="selected-items-text"> selected items &nbsp;</span>'
            );
        } else {
            $('#'+qResultsDisplaySummaryId).html('');
        }
    }
}

function getSelectionFS() {
    var selectedRows = sessionStorage.getItem(qResultsSessionStorage);
    return JSON.parse(selectedRows);
}

function clearSelectionFS() {
    sessionStorage.removeItem(qResultsSessionStorage);
    displayTotalCountFS(null);
}

function checkDefaultValues() {

    $.each($('.qr-default-value'), function(i, v) {
        saveSelectionFS('select', $(v).val());
    });
}