/**
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

// Check if browserId is a string. If it is a string,
// set browserIds array to an array containing browserId.
var browserIdIsString = jQuery.type(browserId) === 'string';
var browserIds = browserId;
if (browserIdIsString) browserIds = [browserId];

// Add keypress event for each browser's pageSize input field
// in the personalize grid settings modal.
for (var browserId of browserIds) {
    // trigger saving of default page size when user presses the enter button from the textfield
    $(document).on('keypress', '#pageSize-' + browserId, function (e) {
        var id = $(this).attr('id').replace('pageSize-','');
        var inputPageSize = parseInt($(this).val())
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 13) { // enter key is pressed
            $('#' + id + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');
        }
    });
}

// when personalize grid settings button is clicked
$('.personalize-grid-icon').parent().on('click', function (e) {
    for (browserId of browserIds) {
        $('#pageSize-' + browserId).val(currentDefaultPageSize); // put current default value in grid settings page size input
    }
});

// when save grid settings is clicked
$(document).on('click', '.dynagrid-submit', function (e) {
    for (browserId of browserIds) {
        if ($(this).parents('#' + browserId + '-grid-modal').length) {
            var value = $('#pageSize-' + browserId).val().trim();
            if (value == currentDefaultPageSize) continue;
            // check if value is not empty, is a number, and is an integer
            if (value != '' && !isNaN(value) && !value.includes('.')) {
                updateDefaultPageSize(value)
            }
        }
    }
});

// when remove grid settings and confirm button is clicked
$(document).on('click', '.btn-warning', function (e) {
    if ($(this).parents('.modal-content').find('.bootstrap-dialog-message').text()
        == 'Are you sure you want to delete the setting?') {    // this is the prompt for the confirm modal
        updateDefaultPageSize('-1');  // resets the value to default
    }
});

// trigger saving of default page size on personalize grid settings modal when user clicks apply button in preferences
$(document).on('click', '#apply-default-page-size-btn', function (e) {
    e.stopPropagation();
    e.preventDefault();
    for (browserId of browserIds) {
        var inputPageSize = parseInt($('#pageSize-' + browserId).val());
        updateDefaultPageSize(inputPageSize);
        $('#pageSize-' + browserId).val(inputPageSize); // put current default value in grid settings page size input
        $('#' + browserId + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');
    }
});

// save new value for data browser default page size
function updateDefaultPageSize(value) {
    value = parseInt(value);
    $.ajax({
        type: 'POST',
        url: setDefaultPageSizeUrl,
        async: false,
        data: {
            newDefaultPageSize: value
        },
        success: function () {
        },
        error: function () {
        }
    });
}


// Performs adjustments related to the unified browser size across the system 
function adjustBrowserPageSize() {
    for (browserId of browserIds) {
        var browserPageSize = $('#pageSize-' + browserId).val() === undefined ?
            null : $('#pageSize-' + browserId).val().trim();  // upon loading

        // If page sizes of browser and preferences does not match, then apply page size from preferences (to also apply it to browser cookies)
        if (browserPageSize !== null && parseInt(browserPageSize) != currentDefaultPageSize) {
            var notif = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> Applying default page size. The page will refresh.';
            Materialize.toast(notif, 2500);
            $('#pageSize-' + browserId).val(currentDefaultPageSize); // put current default value in grid settings page size input
            $('#' + browserId + '-grid-modal').find('.dynagrid-submit').eq(0).trigger('click');   // trigger click
        }
    }
}