const oper = ['+', '-', '*', '/']
const fxnOp = ['prime', '!prime', 'factor', 'between', 'min', 'max', 'each', 'copy']
const compOper = ['>', '<', '>=', '<=', '==', '!=', '=']
const valueStr = ['Random', 'Manual']


function isOper(v) {
  for (var i = 0; i < oper.length; i++) {
      if (oper[i] == v) return true
  }
  return false
}

function isCompOper(v) {
  for (var i = 0; i < compOper.length; i++) {
      if (compOper[i] == v) return true
  }
  return false
}

function isNum(v) {
  return !isNaN(parseFloat(v)) && isFinite(v)
}

function isFxnOp(v) {
  for (var i = 0; i < fxnOp.length; i++) {
      if (fxnOp[i] == v) return true
  }
  return false
}

function isAlpha(v) {
  const regExp = new RegExp(/^[a-z0-9]+$/i)
  return regExp.test(v)
}

function tokenize(str) {
    var tokens = [];
    str = str.trim()
    var s = ''

    for (var index = 0; index < str.length; index++) {
        s += str[index];
        const peek = str[index + 1]
        if (isNum(s.trim()) && !isNum(peek)) {
            tokens.push({ type: 'NUM', value: s.trim() })
            s = ''
        }
        if (s.trim() == '(' || s.trim() == ')') {
            s.trim() == '(' ? tokens.push({ type: 'LPAREN', value: s.trim() }) : tokens.push({ type: 'RPAREN', value: s.trim() })
            s = ''
        }
        if (s.trim() == '{' || s.trim() == '}') {
            s.trim() == '{' ? tokens.push({ type: 'LBRACE', value: s.trim() }) : tokens.push({ type: 'RBRACE', value: s.trim() })
            s = ''
        }
        if (isOper(s.trim()) && !isOper(peek)) {
            tokens.push({ type: 'OP', value: s.trim() })
            s = ''
        }
        
        if (isCompOper(s.trim()) && isCompOper(s.trim() + peek)) {
            tokens.push({ type: 'COMP', value: s.trim() + peek })
            s = ''
            index = index + 1
        }

        if (isCompOper(s.trim()) && !isCompOper(peek)) {
            tokens.push({ type: 'COMP', value: s.trim() })
            s = ''
        }

        if (s.trim() != '' && isFxnOp(s.trim())) {
            tokens.push({ type: 'FXN', value: s.trim() })
            s = ''
        }
        if(s.trim() != '' && s.trim() == 'if'){
            tokens.push({ type: 'COND', value: s.trim() })
            s = ''
        }
        if(s.trim() != '' && valueStr.includes(s.trim())){
            tokens.push({ type: 'BOOL', value: s.trim() })
            s = ''
        }
        if(s.trim() != '' && !isAlpha(peek)){
            tokens.push({ type: 'VAR', value: s.trim() })
            s = ''
        }

        if(s.trim() != '' && peek==undefined){
            tokens.push({ type: 'VAR', value: s.trim() })
            s = ''
        }
    }
    
    return tokens;
}

function prime(num){
    for(let i = 2, s = Math.sqrt(num); i <= s; i++){
        if(num % i === 0) return false; 
    }
    return num > 1;
}

function factor(n) {
    var factors_array = [];
    for (var x = 1; x <= Math.sqrt(Math.abs(n)); x++) {
        if (n % x === 0) {
            var z = n / x;
            if (factors_array.indexOf(z) === -1) {
                factors_array.push(z);
            }
            if (factors_array.indexOf(x) === -1) {
                factors_array.push(x);
            }
        }
    }
    return factors_array;
}

function copy(num){
    return num;
}

function between(n){
    return Array.from(Array(n), (x, index) => index + 1);
}

function min(numbers){
    return Math.min(...numbers);
}

function max(numbers){
  return Math.max(...numbers);
}

