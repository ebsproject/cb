/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contains javascript for detail browser view
 */

// data browser base url without params
var baseUrl = window.location.href.match(/^[^\#\?]+/)[0];
// id of data browser
var browserId;
// height to be lessen for the browser
var heightDiff = (heightDiff != null) ? heightDiff : 160;
// render categories url
var renderCategoriesUrl;
// selected category
var selectedCategory;
// selected category id
var selectedCategoryId;
// selected record id
var selectedRecordId;
// view item url
var viewItemUrl;
// api max items per page
var maxCountPerPage = 100;
// load more records url
var loadMoreRecordsUrl;
// current program
var program;
// id of the occurrence
var occurrenceId;

// loading indicator
var preloader = 
	'<div class="category-panel-loading-indicator text-center">' +
		'<div class="preloader-wrapper small active">' +
			'<div class="spinner-layer spinner-green-only">' +
				'<div class="circle-clipper left">' +
					'<div class="circle"></div>' +
				'</div>' +
				'<div class="gap-patch">' +
					'<div class="circle"></div>' +
				'</div>' +
				'<div class="circle-clipper right">' +
					'<div class="circle"></div>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';

// get dynamic height for the panels
resetHeight();

// reset dynamic height upon resize
$(window).resize(function(){
	resetHeight();
});

// display clear all icon on hover in input field
$("#browser-detail-view-search-form").mouseover(function(){
	if($('#browser-detail-view-search-input').val() != ''){
		$('.browser-detail-view-clear-all-search-btn').css("opacity", "0.5");
		$('.browser-detail-view-help-btn').css("display", "none");
	}
});

// display help and hide clear all upon mouseout
$("#browser-detail-view-search-form").mouseout(function(){
	$('.browser-detail-view-clear-all-search-btn').css("opacity", "0");
	$('.browser-detail-view-help-btn').css("display", "block");
});

//update display of clear all upon changing value in search field
$('#browser-detail-view-search-input').on('change keyup paste click', function () {
	if($('#browser-detail-view-search-input').val() != ''){
		$('.browser-detail-view-clear-all-search-btn').css("opacity", "0.5");
		$('.browser-detail-view-help-btn').css("display", "none");
	}else{
		$('.browser-detail-view-clear-all-search-btn').css("opacity", "0");
		$('.browser-detail-view-help-btn').css("display", "block");
	}
});

// clear all texts in search input
$(document).on('click', '.browser-detail-view-clear-all-search-btn', function(e) {
	$('#browser-detail-view-search-input').val('');
	$('.browser-detail-view-clear-all-search-btn').css("opacity", "0");
	$('.browser-detail-view-help-btn').css("display", "block");
	document.getElementById("browser-detail-view-search-input").focus();
});

// submit search
$(document).on('click', '.browser-detail-view-submit-btn', function(e) {

	$("#browser-detail-view-search-form").submit();

	e.stopPropagation();
	e.preventDefault();
});

// submit filters
$("#browser-detail-view-search-form").submit(function(e) {

	// remove selected category item
	updateUrl('selectedCategoryId', '');
	$('.category-item').css('background-color','#fff');

	var filter = $('#browser-detail-view-search-input').val().trim();

	var searchParams = updateUrl('filter', filter);

	$('.detail-view-page').remove();

	$.pjax.reload({
		url: baseUrl + '?' + searchParams,
		container: '#' + browserId + '-pjax', 
		replace:true
	});
	
	e.stopPropagation();
	e.preventDefault();

});

// upon click filter category, show panel
$(document).on('click','.browser-detail-view-main-filter-item', function(e) {
	var obj = $(this);
	var category = obj.data('category');
	var modelClass = obj.data('model-class');
	var modelClassAction = obj.data('model-class-action');

	// hide all filter category panels
	$('.browser-detail-view-main-filters').addClass('hidden');
	// display all filter items
	$('.browser-detail-view-main-filter-item').removeClass('hidden');

	obj.addClass('hidden');
	$('.category-panel-'+category).removeClass('hidden');

	var opened = $('#categories-panel-' + category).hasClass('opened');

	// if not yet opened, load categories
	if(!opened){
		// display category items
		$.ajax({
			url: renderCategoriesUrl,
			data: {
				modelClass: modelClass,
				modelClassAction: modelClassAction,
				category: category,
				selectedCategoryId: selectedCategoryId
			},
			type: 'POST',
			success: function(data) {
				$('#categories-panel-' + category).html(data);          
			},
			error: function(){
				$('#categories-panel-' + category).html('<i>There were problems while loading records.</i>');          
			}
		});
	}

	// update category upon click category panel
	var searchParams = updateUrl('category', category);

	// add flag that category was already opened
	$('#categories-panel-' + category).addClass('opened');

	e.stopPropagation();
	e.preventDefault();

});

// upon click close category panel, hide panel and display category item
$(document).on('click','.browser-detail-view-main-filters-close', function(e) {
	var obj = $(this);
	var category = obj.data('category');

	$('.category-panel-'+category).addClass('hidden');
	$('.category-item-'+category).removeClass('hidden');
	$('.category-item').css('background-color','#fff');

	// remove filters
	updateUrl('category', '');
	updateUrl('selectedCategoryId', '');

	e.stopPropagation();
	e.preventDefault();

});

// upon click a category
$(document).on('click', '.category-item', function(e) {
	var obj = $(this);
	var category = obj.data('category');
	var id = obj.data('id');
	var text = obj.data('text');

	$('#browser-detail-view-search-input').val(text);

	$('.category-item').css('background-color','transparent');
	$('.category-item').removeClass('active');

	obj.css('background-color','#deebff');

	var searchParams = updateUrl('category', category);
	var searchParams = updateUrl('selectedCategoryId', id);
	var searchParams = updateUrl('filter', text);

	$.pjax.reload({
		url: baseUrl + '?' + searchParams,
		container: '#' + browserId + '-pjax', 
		replace:true
	});
	
	e.stopPropagation();
	e.preventDefault();

});

$("document").ready(function() {
	$('.dropdown-trigger').dropdown({});
	// load category panel if there is selected category
	/*setTimeout(function() {
		$('.category-item-' + selectedCategory).trigger('click');
	},10);

	setTimeout(function() {
		if(selectedRecordId != ''){
			var row = $("tr[data-key="+selectedRecordId+"]").trigger("click");
			
			// if not in detail browser, load viewer
			if(row.length < 1){
				var obj = $('.browser-detail-view-viewer');
				var id = obj.data('id');
				var modelClass = obj.data('model-class');
				var modelClassAction = obj.data('model-class-action');
				var buttons = obj.data('buttons');
				var mainButtons = obj.data('main-action-buttons');

				var viewUrl = obj.data('view-url');
				
				loadViewer(id, modelClass, modelClassAction, buttons, mainButtons, viewUrl);
			}
		}

	},10);*/
});

// after pjax success
$('#' + browserId + '-pjax').on("pjax:success", function(e) {  
	// update total summary
	var count = $('.total-count-summary-hidden').attr('data-count');
	var label = $('.total-count-summary-hidden').attr('data-label');

	$('.total-count-summary').html(count);
	$('.total-count-label').html(label);

	// remove other pages
	$('.detail-view-page').remove();

	$('<div class="detail-view-page" data-value="2"></div>').appendTo('.browser-detail-view-container-wrapper');
	
	e.stopPropagation();
	e.preventDefault();
});

// toggle view category detail
$(document).on('click', '.category-item-expander', function(e) {
	var obj = $(this);
	var id = obj.data('id');

	$('#category-expander-' + id).slideToggle('fast');
	
	// change icon
	if ($(this).find('i').text() == 'keyboard_arrow_down'){
		$(this).find('i').text('keyboard_arrow_right');
	} else {
		$(this).find('i').text('keyboard_arrow_down');
	}

	e.stopPropagation();
	e.preventDefault();
});

// upon click on row
$(document).on('click','.browser-detail-view-row', function(e){
	var obj = $(this);
	var id = obj.data('key');
	var modelClass = obj.data('model-class');
	var modelClassAction = obj.data('model-class-action');
	var buttons = obj.data('buttons');
	var mainButtons = obj.data('main-action-buttons');

	var viewUrl = obj.data('view-url');

	$('.browser-detail-view-row').css('background-color','transparent');
	obj.css('background-color','#deebff');

	// display record information
	loadViewer(id, modelClass, modelClassAction, buttons, mainButtons, viewUrl);
	e.stopPropagation();
	e.preventDefault();

});

// close view record panel
$(document).on('click','.browser-detail-view-viewer-close', function(e){
	$('.browser-detail-view-viewer').addClass('hidden');
	$('.browser-detail-view-row').css('background-color','transparent');

	updateUrl('selectedRecordId','');
	e.stopPropagation();
	e.preventDefault();
});

// update url without loading page
function updateUrl(variable, value){
	var searchParams = new URLSearchParams(window.location.search);
	searchParams.set(variable, value);
	var searchParams = searchParams.toString();

	var newUrl = baseUrl + '?' + searchParams;
	window.history.pushState(null,null,newUrl);
	
	return searchParams;
}

// load viewer panel
function loadViewer(id, modelClass, modelClassAction, buttons, mainButtons, viewUrl){
	$('.browser-detail-view-viewer').html(preloader); 
	$.ajax({
		url: viewItemUrl,
		data: {
			id: id,
			modelClass: modelClass,
			modelClassAction: modelClassAction,
			buttons: buttons,
			mainButtons: mainButtons,
			viewUrl: viewUrl
		},
		type: 'POST',
		success: function(data) {
			$('.browser-detail-view-viewer').html(data);          
			$('.browser-detail-view-viewer').removeClass('hidden');

			updateUrl('selectedRecordId', id);

			// replace the attributes when the view record is reloaded
			setTimeout(function() {
				var obj = $('.replace-attributes');

				// get name
				var name = $('.viewer-display-name').text();
				// replace attributes
				obj.attr({
					'data-id': id,
					'data-name': name
				});

				// replace IDs in URL
				$('.replace-url-id').each(function() {
					var url =  $(this).attr('href');
					var href = new URL('http://' + url);
					
					href.searchParams.set('id', id);
					var newUrl = href.toString();
					var newUrl = newUrl.replace('http://','/');
					$(this).attr('href', newUrl);
				});

			},500);
		},
		error: function(){
			$('.browser-detail-view-viewer').html('<i>There were problems while loading records.</i>');
		}
	});
}

// load more records
$(document).on('click','.load-more-button', function(e){
	var obj = $(this);
	var page = obj.data('page');
	var filter = $('#browser-detail-view-search-input').val().trim();
	obj.html(preloader);

	$.ajax({
		url: loadMoreRecordsUrl,
		data: {
			page: page,
			filter: filter
		},
		type: 'POST',
		success: function(data) {
			$('.detail-view-page[data-value="'+ page +'"]').html(data);
			obj.remove();
		},
		error: function(){
		}
	});

	e.stopPropagation();
	e.preventDefault();
});

// set dynamic height for the browser
function resetHeight(){
	var wrapperHeight = $(window).height() - heightDiff;
	$('.browser-detail-view-panel').css('height',wrapperHeight);
	$('.browser-detail-view-container').css('height',wrapperHeight);
}

// hide loading indicator
$(document).on('click','.hide-loading',function(){
	$('#system-loading-indicator').css('display','block');
	setTimeout(function() {
		$('#system-loading-indicator').css('display','none');
	},2000);
});

// render generic web form in modal
$(document).on('click', '.render-generic-form', function(){
	var obj = $(this);
	var id = obj.data('id');
	var modelClass = obj.data('model-class');
	var modelClassAction = obj.data('model-class-action');
	var step = obj.data('step');
	var name = obj.data('name');

	$('.browser-detail-view-save-basic-info').attr('data-id', id);

	occurrenceId = id;

	// make the name print safe
	var safe = name.replace(/</g,"&lt;").replace(/>/g,"&gt;");
	// add it to the modal header as text
	$('.record-label-modal').html(safe);

	if(modelClass){
		var loading = '<div class="margin-right-big loading"><div class="progress"><div class="indeterminate"></div></div></div>';
		$('.manage-basic-info-modal-body').html(loading);

		$.ajax({
			url: renderGenericFormUrl,
			type: 'post',
			dataType: 'json',
			data:{
				id: id,
				modelClass: modelClass,
				modelClassAction: modelClassAction,
				step: step
			},
			success: function(response){
				$('.manage-basic-info-modal-body').html(response);
			},
			error: function(){
				var errorMessage = '<i>There was a problem loading the content.</i>';
				$('.manage-basic-info-modal-body').html(errorMessage);
			}
		});
	}
});

// switch browser view
$(document).on('click','.switch-browser-view',function(e){
	var obj = $(this);
	var mode = obj.attr('data-view');
	var id = obj.attr('data-browser_id');
	var icon = 'view_list';
	var view = 'grid';

	if(mode == 'grid'){
		var icon = 'view_module';
		var view = 'detail';
	}

	$(this).attr('title','Switch to ' + mode + ' view');
	$(this).attr('data-view', view);

	$(e.target).closest('i').text(icon);

	$.ajax({
		url: switchBrowserViewUrl,
		type: 'post',
		data:{
			browserId: id,
			mode: view
		},
		success: function(response){
			// refresh detail viewer to reflect changes
			$('.browser-detail-view-submit-btn').trigger('click');
			$('#' + id).attr('class','browser-' + view + '-view');

		},
		error: function(){
		}
	});

});

// validate required fields in manage basic info form
$(document).on('keypress keyup blur paste','.required-field', function (e) {
    // if required field not specified
    if($(this).val().length == 0){
        $('.browser-detail-view-save-basic-info').addClass('disabled');
    } else {
        $('.browser-detail-view-save-basic-info').removeClass('disabled');
    }
});

// save basic and data information
$(document).on('click','.browser-detail-view-save-basic-info', function(e){
	var obj = $('.render-generic-form');
	// occurrence id
	var id = occurrenceId;

	// entity
	var entity = obj.data('model-class');
	var entity = entity.toLowerCase();

    var currentPage = obj.data('current-page');

		// display loading indicator
		$('.loading').removeClass('hidden');

		$('#browser-detail-view-basic-info-form').submit();

		$('#browser-detail-view-basic-info-form').submit(function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			let formData = $("#browser-detail-view-basic-info-form").serialize();
			let actionUrl = $("#browser-detail-view-basic-info-form").attr("action");

			formData =  formData + '&' + $.param({
				['id'] : id
			});

			$.ajax({
				type:"POST",
				url: actionUrl,
				data: formData,
				success: function(data) {
					// remove initial toast
					$('.toast').css('display','none');
					var color = "green";
					var icon = "check";
					var message = "Successfully updated "+ entity +" information.";
					
					// if not successful
					if(!data){
						var color = "red";
						var icon = "close";
						var message = "There was a problem while updating record.";
					}

					// display notification
					var notif = "<i class='material-icons "+ color +"-text left'>"+ icon +"</i> <span class='white-text'>"+ message +"</span>";
					Materialize.toast(notif, 5000);
					
					$('#manage-basic-info-modal').modal('hide');
					var searchParams = new URLSearchParams(window.location.search);
					var searchParams = searchParams.toString();

                    if (currentPage === 'view-occurrence-basic') {
                        // Refresh Basic info tab of View Occurrence
                        location.reload()
                    } else {
                        // Refresh detail viewer to reflect changes
                        $.pjax.reload({
                            container: '#occurrences-grid-pjax',
                            url: '/index.php/occurrence?'+ searchParams
                        });
                    }
				},
				error: function(){
				}
			});
		});

	e.stopPropagation();
	e.preventDefault();
});

// reload viewer by record id
function reloadViewer(id){

	var row = $('.browser-detail-view-row');
	
	var modelClass = row.data('model-class');
	var modelClassAction = row.data('model-class-action');
	var buttons = row.data('buttons');
	var mainButtons = row.data('main-action-buttons');

	var viewUrl = row.data('view-url');

	loadViewer(id, modelClass, modelClassAction, buttons, mainButtons, viewUrl);

}