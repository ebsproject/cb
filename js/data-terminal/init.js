/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Quality Control Charts and Default Views
 */

//highchart containers
var stackedBar=null,   
histogram=null,
heatmap=null, histogramModal=null,heatmapModal=null;
var histogram_data, mad , median, binnedData, rawData;
var criticalMinValue,criticalMaxValue,minVal,maxVal;
var fieldmap=null;
var statusDataType;

function resetDataBrowser(){
    $('#reset-grid-id').on('click', function(e) {
        e.preventDefault();
        $.pjax.reload({
            container: '#suppress-browser-pjax',
            replace: true,
            url: varResetGridUrl
        });
    });
}

$(document).on('ready pjax:success', function(e) {
    e.preventDefault();
    resetDataBrowser();
});

$('.tabs a[href="#status-tab-id"]').click(function() {
    $('.qc-toolbar').hide();
    $('#status-tab-id').removeClass("hidden");
    renderStackedBar();

    if(isCrossStudy=='false'){
        renderHeatmap();
    }
    renderHistogram();
    if(isCrossStudy=='false'){
        renderFieldMap();
    }

});

$('.tabs a[href="#complete-tab-id"]').click(function() {
    $('.qc-toolbar').hide();
    $('#complete-tab-id').removeClass("hidden");
});

$('.tabs a[href="#suppress-tab-id"]').click(function() {
    $('.qc-toolbar').show();
    destroyStatusCharts();
});

// select observation variable in field map page 

$('#select-var-map-id').on('change', function(e) {
    e.preventDefault();

    varLabel=$('option:selected', this).attr('data-label');

    varDataLevel=$('option:selected', this).attr('data-level');
    
    varName=$('option:selected', this).attr('data-name');
    varId=$('option:selected', this).attr('data-id');
    varAbbrev=$(this).val();
    
    if(fieldmap!=null){
        fieldmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
    }

    renderFieldMap();
    if(fieldmap!=null){
        
        fieldmap.hideLoading();
    }
});

//Render graphs when a variable is selected
$('#status-select-variables').on('change', function(e) {
    e.preventDefault();
    
    varLabel=$('option:selected', this).attr('data-label');
    varDataLevel=$('option:selected', this).attr('data-level');
    varId=$('option:selected', this).attr('data-id');
    varName=$('option:selected', this).attr('data-name');
    varAbbrev=$(this).val();
    
    $.ajax({
        url: 'get-var-defaults',
        type: 'post',
        async:false,
        data: {abbrev: varAbbrev},
        success: function (data) {

            data=JSON.parse(data);
            criticalMaxValue = data['criticalMaxValue'];
            criticalMinValue = data['criticalMinValue'];
            if(criticalMaxValue=="NULL"){
                criticalMaxValue=null;
            }
            if(criticalMinValue=="NULL"){
                criticalMinValue=null;
            }
            dataType=data['var_data_type'];
            statusDataType = dataType;

            if(dataType=='string' || dataType=='time' || dataType=='character varying' || dataType=='text'){
                $("#critical-min-card-id").addClass("hidden");
                $("#critical-max-card-id").addClass("hidden");
                $("#stacked-card").toggleClass("full");
                $(".histogram-card").toggleClass("hide");
                $(".heatmap-card").toggleClass("hide");
                renderStackedBar();
            }else{
                if($('#heatmap-container').highcharts()!=undefined && heatmap!=null){
                    heatmap.destroy();
                }
                if($('#histogram-container').highcharts()!=undefined && histogram !=null){
                    histogram.destroy();
                }
                if($('#stackedbar-terminal-container').highcharts()!=undefined && stackedBar !=null){
                    stackedBar.destroy();
                }
                $("#stacked-card").removeClass("full");
                $(".heatmap-card").removeClass("hide");
                $(".histogram-card").removeClass("hide");
                
                //render charts

                renderStackedBar();
                
                if(isCrossStudy=='false'){
                    renderHeatmap();
                }

                renderHistogram();
                $("#critical-min-card-id").removeClass("hidden");
                $("#critical-max-card-id").removeClass("hidden");

            }
        }
    });

    $('#select-var-map-id').val(varAbbrev).trigger('change'); // on load, trigger change in selecting variables in the field map 

});

// Render preferred variables 
$('.tabs a[href="#pref-var-tab-id"]').click(function() {
    $("#pref-var-tab-id").html('');
    $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
    $.ajax({
        url: "var-preference",
        type: 'get',

        success: function(response) {
            if (response) {
                $("#pref-var-tab-id").html(response);
                $('#loading-div').html('');
            }
        },
        error: function() {
            $('#pref-var-tab-id').html('<div class="card-panel red"><span class="white-text">There seems to be a problem while retrieving preference data.</span></div>');
        }
    });
});

// Set default data browser 
function setDataBrowser(){
    
    $('.tabs a[href="#suppress-tab-id"]').trigger('click');
    
    if(varDataLevel=='plot'){   
        
         $( "#plot-tab-id" ).trigger( "click" );
         
    }else if(varDataLevel=='entry'){  

        $( "#entry-tab-id" ).trigger( "click" );
      
    }else if(varDataLevel=='study'){  

        $( "#study-tab-id" ).trigger( "click" );
        
    }
    
}

if(isCrossStudy=='true'){
    varDataBrowser='data-browser';
}
if(varDataBrowser=='field-map'){
    
    $('#data-browser-container-id').addClass('hidden');
    $('#field-map-container-id').removeClass('hidden');
    $('#browser-mode-btn-id').addClass("grey lighten-2"); 
    $('#map-mode-btn-id').removeClass("grey lighten-2");  

}else{

    $("#suppress-mode-btn-id").css('background-color','#9e9e9e !important;');   
    $('#suppress-mode-btn-id').attr('data-tooltip', "Click to change to field map."); 
    $('#data-browser-container-id').removeClass('hidden');
    $('#field-map-container-id').addClass('hidden');
    $('#browser-mode-btn-id').removeClass("grey lighten-2"); 
    $('#map-mode-btn-id').addClass("grey lighten-2"); 

}

if(isCrossStudy=='true'){
    $('#map-mode-btn-id').addClass('hidden');
}
$('.browser-mode-toogle').on('click', function (e) {
    e.preventDefault();
    if($(this).attr('data-tooltip')=='Data Browser'){
        $('#data-browser-container-id').removeClass('hidden');
        $('#field-map-container-id').addClass('hidden');

        $('#browser-mode-btn-id').removeClass("grey lighten-2"); 
        $('#map-mode-btn-id').addClass("grey lighten-2");     
        $('#data-browser-container-id').addClass('animated bounceOutLeft');
        
    }else{
        destroyStatusCharts();
        $('#browser-mode-btn-id').addClass("grey lighten-2"); 
        $('#map-mode-btn-id').removeClass("grey lighten-2");     
        
        $('#data-browser-container-id').addClass('hidden');
        $('#field-map-container-id').removeClass('hidden');
        $('#field-map-container-id').addClass('animated bounceOutLeft');

    }

});

$('#status-select-variables').val(varAbbrev).trigger('change');

/**
 * Set all charts in the status page to null
 * Necessary to show the texture palette in Field Map
 */
function destroyStatusCharts(){
    if ($('#heatmap-container').highcharts() != undefined && heatmap != null) {
        heatmap.destroy();
        heatmap = null;
    }

    if ($('#heatmap-modal-container').highcharts() != undefined && heatmapModal != null) {
        heatmapModal.destroy();
        heatmapModal = null;
        console.log("heree");
    }

    if ($('#histogram-container').highcharts() != undefined && histogram != null) {
        histogram.destroy();
        histogram = null;
    }

    if ($('#histogram-modal-container').highcharts() != undefined && histogramModal != null) {
        histogramModal.destroy();
        histogramModal = null;
    }
    
    if ($('#stackedbar-terminal-container').highcharts() != undefined && stackedBar!= null) {
        stackedBar.destroy();
        stackedBar = null;
    }

}

