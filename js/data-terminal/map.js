/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Quality Control Field map
 */

 var fieldmap_last_width = fieldmap_last_height = null;
 var origVarDataBrowser=varDataBrowser;
 var origVarPlotLabel=varPlotLabel;
 var origVarPalette=varPalette;

 $('#fieldmap-zoom-in').click(function() {
    fieldmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
    fieldmapChartWidth *= 1.1;
    fieldmapChartHeight *= 1.1;
    varLastWidth = fieldmapChartWidth;
    varLastHeight = fieldmapChartHeight;
    fieldmap.setSize(fieldmapChartWidth, fieldmapChartHeight,
        doAnimation = true);
    fieldmap.hideLoading();

 });

 $('#fieldmap-zoom-out').click(function() {
    fieldmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
    fieldmapChartWidth *= 0.9;
    fieldmapChartHeight *= 0.9;
    varLastWidth = fieldmapChartWidth;
    varLastHeight = fieldmapChartHeight;
    fieldmap.setSize(fieldmapChartWidth, fieldmapChartHeight,
        doAnimation = true);
    fieldmap.hideLoading();
 });

 $('#fieldmap-reset').click(function() {
    fieldmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
    fieldmapChartWidth = fieldmapOrigChartWidth;
    fieldmapChartHeight = fieldmapOrigChartHeight;
    fieldmap.setSize(fieldmapOrigChartWidth, fieldmapOrigChartHeight,
        doAnimation = true);
    fieldmap.hideLoading();
    reset();
 });

function renderMapPreferences(){

    $('.palette-option').click(function(e) {
        e.preventDefault();
        status=$(this).find('.chip-legend').text();
        status=status.toLowerCase();
        filterMap({status:status});
        $('.palette-option').removeClass('active');
        $(this).addClass('active');         

    });

    $('.legend-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });

    $('.plot-label-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });

    $('#default-palette-id').click(function(e) {
        e.preventDefault();

        if(varPalette!='default'){
            varPalette="default";
            varNullColor='#f7f7f7';

            renderFieldMap();

        }
        $(".palette-label").text("Default");
        $(".default-palette").removeClass("hidden");
        $(".texture-palette").addClass("hidden");

    });

    $('#texture-palette-id').click(function(e) {
        e.preventDefault();

        if(varPalette!='texture'){
            varPalette="texture";
            varNullColor='url(#highcharts-default-pattern-5)';

            renderFieldMap();
        }

        $(".palette-label").text("Texture");
        $(".default-palette").addClass("hidden");
        $(".texture-palette").removeClass("hidden");

    });

    $('#'+varPlotLabel+'-plot-label-id').trigger('click');
    $('#'+varPalette+'-palette-id').trigger('click');

    if(varPlotLabel=='value'){

        $(".show-plot-label").text("Value");

    }else if(varPlotLabel=='plot_code'){

        $(".show-plot-label").text("Plot code");

    }else if(varPlotLabel=='plotno'){

        $(".show-plot-label").text("Plotno");
    }

    $('#plotno-plot-label-id').click(function(e) {
        e.preventDefault();

        varPlotLabel="plotno";

        renderFieldMap();

        $(".show-plot-label").text("Plotno");

    });

    $('#plot_code-plot-label-id').click(function(e) {
        e.preventDefault();

        varPlotLabel="plot_code";

        renderFieldMap();

        $(".show-plot-label").text("Plot code");

    });
    $('#value-plot-label-id').click(function(e) {
        e.preventDefault();

        varPlotLabel="value";

        renderFieldMap();

        $(".show-plot-label").text("Value");
    });

    $('.view-map-details-collapse').sideNav({
        menuWidth: 250,
        edge: 'left',
        onOpen: function(el) {
            $("#sidenav-overlay").addClass('hidden');
            $(".show-plot-details").text("radio_button_checked");
            $('body').css('overflow','');

            if($('body').hasClass('fixed-side-nav')){
                //on open -- plot slide out, fixed left nav
                $('#plot-detail-slide-out').addClass('side-nav-overlap');
                $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
                $('#main').toggleClass('main-full');
                $('#main').removeClass('main-not-full');

                $("#map-nav-id").css('width', 'calc(100% - 265px)');
                $("#quality-control-nav").css('width','calc(100% - 265px)');

            }else{
                
                //on open -- plot slide out, no fixed left nav
                $('#plot-detail-slide-out').removeClass('side-nav-overlap');
                $('#plot-detail-slide-out').addClass('side-nav-not-overlap');

                $('#main').toggleClass('main-not-full');
                $('#main').removeClass('main-full');
                $("#map-nav-id").css('width', 'calc(100% - 265px)');

                $("#quality-control-nav").css('width','calc(100% - 265px)');
            }
        },

        onClose: function(el) {
            
            $("#sidenav-overlay").removeClass('hidden');
            $('body').css('overflow','');
            $(".show-plot-details").text("radio_button_unchecked");

            if($('body').hasClass('fixed-side-nav')){
                //on close  --plot slide out, fixed nav
                if($('#main').hasClass('main-not-full')){

                    $('#main').toggleClass('main-full');
                    $('#main').removeClass('main-not-full');
                    
                }else{
                    //--- main-not-full
                    $("#quality-control-nav").css('width','calc(100% - 265px)');
                    $("#map-nav-id").css('width', 'calc(100% - 265px)');
                }
                
            }else{
                //on close  --plot slide out, NO fixed nav
                $('#main').toggleClass('main-full');
                $('#main').removeClass('main-not-full');
                $("#quality-control-nav").css('width','100%');
                $("#map-nav-id").css('width', '100%');
            }
        }, 

        draggable: false,

    });
    $('#go-to-data-filters').on('click',function(){
        if($('body').hasClass('fixed-side-nav') ){
            if($(".show-plot-details").text()=='radio_button_checked'){
                $('#plot-detail-slide-out').addClass('side-nav-overlap');
                $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
                $('.view-map-details-collapse').trigger('click');
            }
        }else{
            if($(".show-plot-details").text()=='radio_button_checked'){
                $('#plot-detail-slide-out').addClass('side-nav-not-overlap');
                $('#plot-detail-slide-out').removeClass('side-nav-overlap');
            }
        }
    });

    $('#browser-mode-btn-id,#map-mode-btn-id,#go-to-hide-left-nav').on('click',function(){
        if($('body').hasClass('fixed-side-nav') ){
            //on mode buttons- fixed left nav
            $('#plot-detail-slide-out').addClass('side-nav-overlap');
            $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
            $('#main').toggleClass('main-full');
            $('#main').removeClass('main-not-full');
            $("#map-nav-id").css('width', 'calc(100% - 265px)');
            $("#quality-control-nav").css('width','calc(100% - 265px)');
           
        }else{
            //on mode buttons- NO fixed left nav
            if($(".show-plot-details").text()=='radio_button_checked'){
                 //-- has plot slide out
                if(!$('#main').hasClass('main-not-full')){
                    $('#main').toggleClass('main-not-full');
                    $('#main').removeClass('main-full');
                }
                
                $("#map-nav-id").css('width', 'calc(100% - 265px)');

                if($('#data-browser-container-id').hasClass("hidden")){  // // map is shown, data browser hidden
                    $("#quality-control-nav").css('width','calc(100% - 265px)');
                     //-- map mode
                }

                if($('#field-map-container-id').hasClass("hidden")){  // data browser is shown, map is hidden
                    $("#quality-control-nav").css('width','100%');
                     if($('#main').hasClass('main-not-full')){
                        $('#main').toggleClass('main-full');
                        $('#main').removeClass('main-not-full');
                    }
                    // -- browser mode
                }

            }else{
                 //-- NO plot slide out
                $('#main').toggleClass('main-full');
                $('#main').removeClass('main-not-full');
                $("#quality-control-nav").css('width','100%');
                $("#map-nav-id").css('width', '100%'); 

            }
        }
    });

    if(!$(this).hasClass('fixed-side-nav')){

        $('#plot-detail-slide-out').addClass('side-nav-overlap');
        $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
        document.getElementById('plot-detail-slide-out').removeAttribute("style");
        $('.view-map-details-collapse').sideNav('hide');
    }else{

        $('#plot-detail-slide-out').removeClass('side-nav-overlap');
        $('#plot-detail-slide-out').addClass('side-nav-not-overlap');

        $('.view-map-details-collapse').sideNav('show');
    }

    $('body').on('change',function(){
        $("#quality-control-nav").css('width','calc(100% - 265px)');
        if($(this).hasClass('fixed-side-nav')){
            
            //on body change -- fixed side nav
            $("#map-nav-id").css('width', 'calc(100% - 265px)');
            if($(".show-plot-details").text()=='radio_button_checked'){
                $('#plot-detail-slide-out').addClass('side-nav-overlap');
                $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
                if($('#main').hasClass('main-not-full')){
                    $('#main').toggleClass('main-full');
                    $('#main').removeClass('main-not-full');

                }
            }
        }else{ 
            $("#quality-control-nav").css('width','100%');
            $("#map-nav-id").css('width', '100%');
            //on body change -- no fixed side nav
            if($(".show-plot-details").text()=='radio_button_checked'){
                $('#plot-detail-slide-out').addClass('side-nav-overlap');
                $('#plot-detail-slide-out').removeClass('side-nav-not-overlap');
                if($('#main').hasClass('main-full')){
                    $('#main').toggleClass('main-not-full');
                    $('#main').removeClass('main-full');
                }else if(!$('#main').hasClass('main-not-full')){
                    $('#main').toggleClass('main-not-full');
                    $('#main').removeClass('main-full');
                } 
            }else{
                if($('#main').hasClass('main-not-full')){
                    $('#main').toggleClass('main-full');
                    $('#main').removeClass('main-not-full');
                }else if(!$('#main').hasClass('main-full')){
                    $('#main').toggleClass('main-full');
                    $('#main').removeClass('main-not-full');
                }
                
            }
        }
    });
    $(document).on('click', '#preference-toggle-id', function(e) {

        $("#pref-default-tab-id").html('');
        $('#loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');
        $.ajax({
            url: "preference",
            type: 'get',
            success: function(response) {
                if (response) {
                    $("#pref-default-tab-id").html(response);
                    $('#loading-div').html('');

                }
            },
            error: function() {
                $('#preference-body').html('<div class="card-panel red"><span class="white-text">There seems to be a problem while retrieving preference data.</span></div>');
            }
        });
    });

}

function renderPreferences(){
    if(origVarDataBrowser=='field-map'){
        $('.preference-data-browser').prop("checked",false);  

        origVarDataBrowser='field_map';
    }else{
        $('.preference-data-browser').prop("checked",true); 
        origVarDataBrowser='data_browser';
    }

    $('#plot-label-'+origVarPlotLabel).prop("checked",true);

    $('#palette-'+origVarPalette).prop("checked",true);

    $("input.preference-data-browser[type=checkbox]").on("change",function() {
        var status = $(this).prop('checked');
        if(!status){
            origVarDataBrowser='field-map';
        }else{
            origVarDataBrowser='data_browser';
        }
    });

    $('#save-preference-id').on('click', function(e) {
        e.preventDefault();
        if(!$("input.preference-data-browser[type=checkbox]").prop('checked')){
            origVarDataBrowser='field-map';
        }else{
            origVarDataBrowser='data_browser';
        }

        $.ajax({
            url: "save-preference",
            type: 'post',
            dataType: 'json',
            async:false,
            data: {
                plot_label: $("input:radio[name='plot-label']:checked").val(),
                palette: $("input:radio[name='palette']:checked").val(),
                data_browser:origVarDataBrowser
            },
            success: function(data) {
                if (!data.success) { 
                    var toastContent = $('<span>' + data.message + '</span>').add($('<button class="btn-flat toast-close-btn black-text red" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                    Materialize.toast(toastContent, 'x', 'red');
                } else {
                    varDataBrowser=data.data.data_browser;
                    varPlotLabel=data.data.plot_label;
                    varPalette=data.data.palette;

                    origVarDataBrowser=data.data.data_browser;
                    origVarPlotLabel=data.data.plot_label;
                    origVarPalette=data.data.palette;

                    $('#'+origVarPlotLabel+'-plot-label-id').trigger('click');
                    $('#'+origVarPalette+'-palette-id').trigger('click');

                    var toastContent = $('<span class="black-text"><strong>Successfully saved your preferences!</span>').add($('<button class="btn-flat toast-close-btn black-text green" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                    Materialize.toast(toastContent, 10000, 'green');
                }

            },  error: function(xhr, status, error) {

                var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;"><i class="material-icons red toast-close-btn">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                    '</li> </ul>');

                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();

            }
        });

    });
}

$('#search-plot-btn-id').click(function(e) {
    e.preventDefault();
    $('.collapsible-per-plot').collapsible('close');

    fieldmap.showLoading('<div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');

    plotno = $('#plotno').val();
    rep = $('#rep').val();
    designation = $('#designation').val();
    gid = $('#gid').val();
    x = $('#x').val();
    y = $('#y').val();

    var_params = {
        rep: rep,
        desig: designation,
        gid: gid,
        plotno: plotno
    };

    $.ajax({
        url: "field-map",
        async: false, 
        type: 'post',
        dataType: 'json',
        data: {
            transactionId: varTransactionId,
            abbrev: varAbbrev,
            level: "map-"+varDataLevel+"-level",
            params: {
                rep: rep,
                desig: designation,
                gid: gid,
                plotno: plotno
            },
            palette:varPalette
        },
        success: function(data) {

            data = JSON.parse(data);
            selectedCount = data[0].selected_count;

            layout = JSON.parse(data[0].json_agg);

            fieldmap.series[0].setData(layout, true, true, false);

            fieldmap.hideLoading();
        },
        error: function(jqXHR, exception) {

            error_log(jqXHR, exception);
        }
    });



    if ($(".highcharts-selected-label").length > 0) {
        h3lbl.destroy();
    }

    h3lbl = fieldmap.renderer.label('You selected ' + selectedCount + ' plots', 0, 40, null, null, null, null, null, 'selected-label')
    .attr({
        padding: 10,
        r: 5,
        fill: Highcharts.getOptions().colors[1],
        zIndex: 5
    })
    .css({
        color: 'white'
    })
    .add();

});

function renderFieldMap(){

    $.ajax({
        url: 'field-map',
        type: 'post',
        aync: false,
        data: {
            transactionId: varTransactionId,
            abbrev: varAbbrev,
            level: "map-"+varDataLevel+"-level",
            palette: varPalette
        },
        success: function(data) {
            data = JSON.parse(data);
            data = JSON.parse(data);
            if (data.length > 0 && data[0].json_agg != null) {
                layout = JSON.parse(data[0].json_agg);

                row = data[0].row;
                col = data[0].col;
                rowLabel = data[0].row_label;
                colLabel = data[0].col_label;
                dataType = data[0].data_type;

                if (data[0].min_val != undefined) {
                    scaleMinValue = data[0].min_val;
                    $('#min-val-id').val(scaleMinValue);
                }
                if (data[0].max_val != undefined) {
                    scaleMaxValue = data[0].max_val;
                    $('#max-val-id').val(scaleMaxValue);
                }
                if ((dataType == 'string' || dataType == 'text' || dataType == 'character varying')) {
                    $("#min-val-id").addClass("hidden");
                    $("#max-val-id").addClass("hidden");
                } else if (dataType == 'date') {
                    $("#min-val-id").addClass("hidden");
                    $("#max-val-id").addClass("hidden");
                } else if (dataType == 'time') {
                    $("#min-val-id").addClass("hidden");
                    $("#max-val-id").addClass("hidden");
                } else if (dataType == 'integer') {
                    $("#min-val-id").removeClass("hidden");
                    $("#max-val-id").removeClass("hidden");
                } else if ((dataType == 'double precision' || dataType == 'float')) {
                    $("#min-val-id").removeClass("hidden");
                    $("#max-val-id").removeClass("hidden");
                }
                var plot_count = data[0].count;
                if (row == null && col != null) {
                    row = plot_count / col;
                } else if (row != null && col == null) {
                    col = plot_count / row;
                } else if (row == null && col == null) {
                    row = 0;
                    col = 0;
                }
                x_category = [];
                y_category = [];

                for (var i = 0; i <= row; i++) {
                    x_category.push("" + i);
                }
                i = 1;
                for (var i = 0; i <= col; i++) {
                    y_category.push("" + i);
                }

                if (fieldmap == null) {
                    fieldmap = new Highcharts.chart('fieldmap-container', {
                        chart: {
                            type: 'heatmap',
                            marginTop: 100,
                            plotBorderWidth: 1,
                            height: '2000',
                            reflow: true,
                            zoomType: 'xy',
                            animation: false,
                            panKey: 'shift',
                            events: {
                                selection: selectPointsByDrag,
                                selectedpoints: selectedPoints,
                                click: unselectByClick
                            }
                        },

                        loading: {
                            hideDuration: 1000,
                            showDuration: 0
                        },
                        plotOptions: {
                            series: {

                                cursor: 'pointer',
                                allowPointSelect: true,

                            },
                        },

                        title: {
                            text: 'Plots with Collected Data for ' + varLabel,
                            align: 'left'

                        },
                        subtitle: {
                            text: '',
                            align: 'left'
                        },

                        xAxis: {
                            categories: x_category,
                            tickInterval: 1,
                            opposite: true,
                            title: {
                                enabled: true,
                                align: 'low',
                                text: '<b>'+rowLabel+'</b>',
                                useHTML:true,
                            },
                        },

                        yAxis: {
                            categories: y_category,
                            tickInterval: 1,
                            reversed: true,
                            title: {
                                align: 'high',
                                enabled: true,
                                text: '<b>'+colLabel+'</b>',
                                useHTML:true,
                            },
                        },
                        legend: {

                            enabled: false
                        },

                        tooltip: {
                            formatter: function() {
                                return 'X: ' + this.point.x + '<br>' + 'Y: ' + this.point.y + '<br> plotno:' + this.point.plotno+'<br> plot code:' + this.point.plot_code+'<br> value:' + this.point.value;
                            }
                        },

                        series: [{

                            name: 'Values for every plot',
                            borderWidth: 1,
                            data: layout,
                            allowPointSelect: true,
                            dataLabels: {
                                enabled: true,
                                color: '#000000',

                                formatter: function() {
                                    if(varPlotLabel=="plotno"){
                                        return this.point.plotno;
                                    }else if(varPlotLabel=="plot_code"){
                                        return this.point.plot_code;
                                    }else if(varPlotLabel=="value"){
                                        return this.point.value;
                                    }

                                }

                            },
                            states: {
                                select: {

                                },
                            },

                            turboThreshold: Number.MAX_VALUE,
                            events: {
                                click: function(e) {

                                    if ($(".highcharts-selected-label").length > 0) {
                                        h3lbl.destroy();
                                    }
                                    h3lbl = fieldmap.renderer.label('You clicked PLOTNO ' + e.point.plotno, 0, 40, null, null, null, null, null, 'selected-label')
                                    .attr({
                                        padding: 10,
                                        r: 5,
                                        fill: Highcharts.getOptions().colors[1],
                                        zIndex: 5
                                    }).css({
                                        color: 'white'
                                    }).add();

                                    obs_value = null;
                                    obs_collector = null;
                                    obs_coll_date = null;
                                    obs_coll_time = null;
                                    obs_remarks = null;
                                    obs_suppress_remarks = null;

                                    $('.click-to-show').addClass('hidden');
                                    $('.no-data').addClass('hidden');
                                    $('#plotno').val(e.point.plotno);
                                    $('#rep').val(e.point.rep);
                                    $('#designation').val(e.point.designation);
                                    $('#gid').val(e.point.gid);
                                    $('#x').val(e.point.x + 1);
                                    $('#y').val(e.point.y + 1);
                                    if (e.point.value !== null) {
                                        obsId = e.point.id;
                                        op_id = e.point.op_id;

                                        if (e.point.status=='suppressed') {
                                            tag = '<font class="new badge brown darken">SUPPRESSED</font>';
                                            $('#undo-discard-btn-id').removeClass('hidden');
                                        } else if (e.point.status=='updated') {
                                            tag = '<font class="new badge orange tooltipped" data-position="bottom" data-delay="50" data-tooltip="Updated">' + e.point.operational_value + '</font>';
                                        } else if (e.point.status=='new') {
                                            tag = '<font class="new badge green tooltipped" data-position="bottom" data-delay="50" data-tooltip="New">NEW</font>';
                                        }else if (e.point.status=='operational') {
                                            tag = '<font class="new badge blue tooltipped" data-position="bottom" data-delay="50" data-tooltip="Operational">OPERATIONAL</font>';
                                        }else if (e.point.status=='voided') {
                                            tag = '<font class="new badge grey tooltipped" data-position="bottom" data-delay="50" data-tooltip="Voided">Voided</font>';
                                        }else if (e.point.status=='invalid') {
                                            tag = '<font class="new badge red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Invalid">INVALID</font>';
                                        }
                                        if (e.point.is_computed) {
                                            tag += '<font class="new badge grey tooltipped" data-position="bottom" data-delay="50" data-tooltip="I am a tooltip">COMPUTED</font>';
                                        }

                                        $('#text-field-div').removeClass('hidden');
                                        $('#collector-field-div').removeClass('hidden');
                                        $('#collection_timestamp-field-div').removeClass('hidden');
                                        $('#remarks-field-div').removeClass('hidden');
                                        $('#suppress-remarks-field-div').removeClass('hidden');

                                        document.getElementById('label-id-text').innerHTML = tag + " " + varLabel;
                                        $('#text-field-id').val(e.point.value);
                                        $('#collector-field-id').val(e.point.collector);
                                        $('#collection_timestamp-field-id').val(e.point.collection_timestamp);
                                        $('#remarks-field-id').val(e.point.remarks);
                                        $('#suppress-remarks-field-id').val(e.point.suppress_remarks);

                                    } else {
                                        $('.no-data').removeClass('hidden');
                                        $('.click-to-show').addClass('hidden');
                                        $('#text-field-div').addClass('hidden');

                                        $('#collector-field-div').addClass('hidden');
                                        $('#collection_timestamp-field-div').addClass('hidden');
                                        $('#remarks-field-div').addClass('hidden');
                                        $('#suppress-remarks-field-div').addClass('hidden');

                                        $('#discard-btn-id').addClass('hidden');
                                        $('#undo-discard-btn-id').addClass('hidden');
                                    }

                                    Materialize.updateTextFields();
                                },

                            },
                        }],
                        options: {
                            responsive: true,
                            maintainAspectRatio: false
                        },

                    },function(chart) {

                        wFactor = 60;
                        hFactor = 100;

                        if (fieldmap_last_width != null) {
                            $('#fieldmap-container').highcharts().setSize(
                                fieldmap_last_width,
                                fieldmap_last_height,
                                doAnimation = true);
                        } else {

                            if (col > 5) {
                                $('#fieldmap-container').highcharts().setSize(
                                    57 * row,
                                    75 * col);
                            } else if (col == 1) {
                                $('#fieldmap-container').highcharts().setSize(
                                    60 * row,
                                    180 * col);
                            } else {

                                while (row * wFactor < $('#fieldmap-container-parent').width() || col * hFactor < ($('#fieldmap-container-parent').height() / 3)) {
                                    wFactor += 10;
                                    hFactor += 10;
                                }
                                $('#fieldmap-container').highcharts().setSize(
                                    wFactor * row,
                                    hFactor * col);

                            }
                        }

                    });

                } else {
                    fieldmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                    fieldmap.series[0].setData(layout, true, true, false);
                    fieldmap.setTitle({
                        text: 'Data Collection of ' + varLabel,
                    });
                    fieldmap.hideLoading();

                    fieldmap.redraw();
                }
                fieldmapOrigChartWidth = fieldmap.chartWidth;
                fieldmapOrigChartHeight = fieldmap.chartHeight;
                fieldmapChartWidth = fieldmapOrigChartWidth;
                fieldmapChartHeight = fieldmapOrigChartHeight;
                $('#fieldmap-btn-group2').removeClass('hidden');
                $('#fieldmap-btn-group').removeClass('hidden');

            }
        }
    });
}

function toast(chart, text) {

    if ($(".highcharts-selected-label").length > 0) {
        h3lbl.destroy();
    }
    h3lbl = fieldmap.renderer.label('You selected ' + selectedCount + ' plots', 0, 40, null, null, null, null, null, 'selected-label')
    .attr({
        padding: 10,
        r: 5,
        fill: Highcharts.getOptions().colors[1],
        zIndex: 5
    })
    .css({
        color: 'white'
    })
    .add();
}

function selectPointsByDrag(e) {

    // Select points 
    Highcharts.each(this.series, function(series) {
        Highcharts.each(series.points, function(point) {
            if (point.x >= e.xAxis[0].min && point.x <= e.xAxis[0].max &&
                point.y >= e.yAxis[0].min && point.y <= e.yAxis[0].max) {
                point.select(true, true);
            }
        });
    });

    // Fire a custom event 
    Highcharts.fireEvent(this, 'selectedpoints', {
        points: this.getSelectedPoints()
    });

    return false; // Don't zoom 
}

// The handler for a custom event, fired from selection event
function selectedPoints(e) {

    var selectedPoints = fieldmap.getSelectedPoints();
    if ($(".highcharts-selected-label").length > 0) {
        h3lbl.destroy();
    }
    h3lbl = fieldmap.renderer.label('You selected ' + selectedPoints.length + ' plots', 0, 40, null, null, null, null, null, 'selected-label')
    .attr({
        padding: 10,
        r: 5,
        fill: Highcharts.getOptions().colors[1],
        zIndex: 5
    })
    .css({
        color: 'white'
    })
    .add();
}

function unselectByClick() {
    var points = this.getSelectedPoints();
    if (points.length > 0) {
        fieldmap.each(points, function(point) {
            point.select(false);
        });
    }
}

function filterMap(params){
    
    $.ajax({
        url: "field-map",
        async: false, 
        type: 'post',
        dataType: 'json',
        data: {
            transactionId: varTransactionId,
            abbrev: varAbbrev,
            level: "map-"+varDataLevel+"-level",
            params:params,
            palette:varPalette
        },
        success: function(data) {

            data = JSON.parse(data);
            
            selectedCount = data[0].selected_count;

            layout = JSON.parse(data[0].json_agg);

            fieldmap.series[0].setData(layout, true, true, false);

            fieldmap.hideLoading();

        },
        error: function(jqXHR, exception) {

            error_log(jqXHR, exception);
        }
    });



    if ($(".highcharts-selected-label").length > 0) {
        h3lbl.destroy();
    }
    h3lbl = fieldmap.renderer.label('You selected ' + selectedCount + ' plots', 0, 40, null, null, null, null, null, 'selected-label')
    .attr({
        padding: 10,
        r: 5,
        fill: Highcharts.getOptions().colors[1],
        zIndex: 5
    })
    .css({
        color: 'white'
    })
    .add();
}
 
$('#clear-btn-id').click(function() {

    $('#plotno').val("");
    $('#rep').val("");
    $('#designation').val("");
    $('#gid').val("");
    $('#x').val("");
    $('#y').val("");

    reset();
});

function reset() {
    $('.palette-option').removeClass('active');
    renderFieldMap();
}

function error_log(jqXHR, exception) {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connected. Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (exception === 'timeout') {
        msg = 'Time out error.';
    } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error.' + jqXHR.responseText;
    }

    return msg;
}
