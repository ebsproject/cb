/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders Quality Control Observation Status
 */

renderHeatmapModal();
renderHistogramModal();

function renderHeatmapModal(){

    $('#heatmap-modal-parent').on('show.bs.modal', function (event) {

        var modal = $(this)
        modal.find('.modal-title').text('Range of Collected Data for ' + varLabel);
        heatmapModal=new Highcharts.chart('heatmap-modal-container',{
            chart: {
                type: 'heatmap',
                marginTop: 100,
                plotBorderWidth: 1,
                reflow: false,

            },
            plotOptions: {
                series: {
                    pointWidth: 20
                }
            },
            title: {
                text: ''
            },

            xAxis: {
                categories: x_category,
                tickInterval: 1,
                opposite: true
            },

            yAxis: {
                categories: y_category,
                title: null,
                tickInterval: 1,
                reversed: true,
            },

            colorAxis: {
                min: min,
                max:max,

                stops: [

                    [0, '#f96447'],
                    [0.5, '#ffffff'],
                    [1, '#4ca54c']

                ],

            },

            legend: {
                align: 'left',
                layout: 'horizontal',
                margin: 0,
                verticalAlign: 'top',
                y: -20,
                symbolHeight: 40
            },

            tooltip: {
                formatter: function () {
                    return 'X: '+ this.series.xAxis.categories[this.point.x] +'<br>'+
                    'Y: '+this.series.yAxis.categories[this.point.y]+'<br> value:'+this.point.value;
                }
            },

            series: [{
                name: 'Values for every plot',
                borderWidth: 1,
                data: heatmapLayout,
                dataLabels: {
                    enabled: true,
                    color: '#000000'
                },
               turboThreshold: Number.MAX_VALUE 
           }],
           options: {
                responsive: true,
                maintainAspectRatio: false
           }

       });

        if(col >5){

            heatmapModal.setSize( 
                57*row,
                75*col,
                doAnimation = true );
        }else if(col==1){
            heatmapModal.setSize(
                60*row,
                180*col);
        }else{
            wFactor=60;
            hFactor=100;
            while(row*wFactor<$('#heatmap_modal-parent').width()){
                wFactor+=10;
                hFactor+=10;
            }
            heatmapModal.setSize( 
                wFactor*row,
                hFactor*col,
                doAnimation = true );

        }

        heatmapmOrigChartWidth = heatmapModal.chartWidth,
        heatmapmOrigChartHeight = heatmapModal.chartHeight,
        heatmapmChartWidth = heatmapmOrigChartWidth,
        heatmapmChartHeight = heatmapmOrigChartHeight;

        $('#heatmap_modal-zoom-in').click(function () {
            heatmapModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');      
            heatmapmChartWidth *= 1.1;
            heatmapmChartHeight *= 1.1;
            heatmapModal.setSize(heatmapmChartWidth, heatmapmChartHeight);
            heatmapModal.hideLoading();
        });
        $('#heatmap_modal-zoom-out').click(function () {
            heatmapModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');      
            heatmapmChartWidth *= 0.9;
            heatmapmChartHeight *= 0.9;
            heatmapModal.setSize(heatmapmChartWidth, heatmapmChartHeight);
            heatmapModal.hideLoading();
        });
        $('#heatmap_modal-reset').click(function () {
            heatmapModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');      
            heatmapmChartWidth = heatmapmOrigChartWidth;
            heatmapmChartHeight = heatmapmOrigChartHeight;
            heatmapModal.setSize(heatmapmOrigChartWidth, heatmapmOrigChartHeight);
            heatmapModal.hideLoading();
        });

    });
}
function renderHistogramModal(){

    $('#histogram-modal-parent').on('show.bs.modal', function (event) {

        var modal = $(this);
        modal.find('.modal-title').text('Distribution Chart of ' +varLabel)
        if(mad!=null){
            histogramModal=new Highcharts.chart('histogram-modal-container',{
                chart: {
                    type: 'column',
                    margin: [100, 25, 100, 50],
                },
                title: {
                    text: ''
                },
                legend: {
                    enabled: true
                },
                tooltip: {},
                plotOptions: {
                    series: {
                        pointPadding: 0,
                        groupPadding: 0,
                        borderWidth: 0.5,
                        borderColor: 'rgba(255,255,255,0.5)'
                    }
                },
                
                xAxis: [{
                    title: {
                        text: 'Range'
                    }

                }, {
                    linkedTo: 0,
                    opposite: true,
                    gridLineWidth: 0.5,
                    gridLineColor: 'rgba(0,0,0,0.25)',
                    gridZIndex: 8,
                    tickPositions: [
                    mad.median - (mad.mad * 3),
                    mad.median - (mad.mad * 2),
                    mad.median - mad.mad,
                    mad.median,
                    mad.median + mad.mad,
                    mad.median + (mad.mad * 2),
                    mad.median + (mad.mad * 3),
                    ],
                    title: {
                        text: 'Median and MAD'
                    },
                    labels: {
                        style: {
                            color: 'rgba(0,0,0,1)',
                            fomntWeight: 'bold'
                        },
                        formatter: function() {
                            return Highcharts.numberFormat(this.value, 2);
                        }
                    },
                    plotBands:[{
                        from: min,
                        to: minVal,
                        color:'url(#highcharts-default-pattern-2)',
                        zIndex: 10,
                        id: 'plot-line-crit-min',
                        events:{
                            click: function(evt) {
                                if(isCrossStudy=='false'){
                                    filterMap({operator:'<',value: minVal, variable_id: varId});  renderHeatmap();
                                }

                                setDataBrowser();

                            },

                        }

                    },{
                        from: max,
                        to: maxVal,
                        color:'url(#highcharts-default-pattern-1)',
                        zIndex: 10,
                        id: 'plot-line-crit-max',
                        events:{
                            click: function(evt) {

                                if(isCrossStudy=='false'){
                                    filterMap({operator:'>',value: maxVal, variable_id: varId});

                                }
                                setDataBrowser();

                            },

                        }

                    },]
                }],
                yAxis: {
                    title: {
                        text: 'Frequency'
                    },
                    min: 0
                }
            });
            
                //add the data series to the chart
            histogramModal.addSeries({
                name: 'Value',
                data: binnedData
            });

            //add Median plotline
            histogramModal.xAxis[0].addPlotLine({
                value: mad.median,
                width: 1,
                color: 'rgba(0,0,0,0.5)',
                zIndex: 8
            });

            histogramModal.addSeries({
                name: 'Below minimum critical value',
                color:'url(#highcharts-default-pattern-2)',
                type:'area',
                marker: {
                    enabled: false
                },

                events: {
                    legendItemClick: function(e) {
                        if(this.visible) {
                            this.chart.xAxis[0].removePlotLine('#plot-line-crit-min');
                        }
                        else {
                            this.chart.xAxis[0].addPlotLine('#plot-line-crit-min');
                        }
                    }
                }

            });

            histogramModal.addSeries({
                name: 'Above maximum critical value',
                color:'url(#highcharts-default-pattern-1)',
                type:'area',
                marker: {
                    enabled: false
                },
                events: {

                    legendItemClick: function(e) {
                        if(this.visible) {
                            this.chart.xAxis[0].removePlotLine('#plot-line-crit-max');
                        }
                        else {
                            this.chart.xAxis[0].addPlotLine('#plot-line-crit-max');
                        }
                    }
                }
            });

            histogrammOrigChartWidth = histogramModal.chartWidth,
            histogrammOrigChartHeight = histogramModal.chartHeight,
            histogrammChartWidth = histogrammOrigChartWidth,
            histogrammChartHeight = histogrammOrigChartHeight;

            $('#histogram_modal-zoom-in').click(function () {
                histogramModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                histogrammChartWidth *= 1.1;
                histogrammChartHeight *= 1.1;
                histogramModal.setSize(histogrammChartWidth, histogrammChartHeight);
                histogramModal.hideLoading();
            });
            $('#histogram_modal-zoom-out').click(function () {
                histogramModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                histogrammChartWidth *= 0.9;
                histogrammChartHeight *= 0.9;
                histogramModal.setSize(histogrammChartWidth, histogrammChartHeight);
                histogramModal.hideLoading();
            });
            $('#histogram_modal-reset').click(function () {
                histogramModal.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                histogrammChartWidth = histogrammOrigChartWidth;
                histogrammChartHeight = histogrammOrigChartHeight;
                histogramModal.setSize(histogrammOrigChartWidth, histogrammOrigChartHeight);
                histogramModal.hideLoading();
            });
        }

    }); 
}
function renderHeatmap(){
    $.ajax({
        url: 'layout-variable',
        type: 'post',
        data: {transactionId: varTransactionId, abbrev: varAbbrev,level: varDataLevel+'-'+'level'},
        success: function (data) {              
            data=JSON.parse(data);
            
            if(data.length>0 && data[0].json_agg!=null){
                heatmapLayout=JSON.parse(data[0].json_agg);

                row=data[0].row;
                col=data[0].col;
                min=data[0].min;
                max=data[0].max;

                var plotCount=data[0].count;
                if(row==null && col!=null){
                    row=plotCount/col;
                }else if(row!=null && col==null){
                    col=plotCount/row;
                }else if(row==null && col==null){
                    row=0;
                    col=0;
                }
                x_category=[];
                y_category=[];
                if(col>row){
                    row_x=col;
                    col=row;
                    row=row_x;
                    i=1;

                }
                for(var i=1;i<=row;i++){
                    y_category.push(""+i);
                }
                i=1;
                for(var i=1;i<=col;i++){
                    x_category.push(""+i);
                }
                
                heatmap=new Highcharts.chart('heatmap-container',{
                    chart: {
                        type: 'heatmap',
                        marginTop: 130,
                        
                        plotBorderWidth: 1,
                        width:$('.heatmap-card').width(),
                        height:$('.heatmap-card').height(),
                        reflow: true,
                        animation: false,
                        className: "card"
                    },
                    plotOptions: {
                        series: {
                            pointWidth: 20
                        }
                    },

                    title: {
                        text: 'Range of collected data',
                        align: 'left',
                        marginTop:0
                    },

                    xAxis: {
                        categories: x_category,
                        tickInterval: 1,
                        opposite: true
                    },

                    yAxis: {
                        categories: y_category,
                        title: null,
                        tickInterval: 1,
                        reversed: true,
                    },

                    colorAxis: {
                        min: min,
                        max:max,

                        stops: [

                        [0, '#f96447'],
                        [0.5, '#ffffff'],
                        [1, '#4ca54c']

                        ],

                    },

                    legend: {
                        align: 'left',
                        layout: 'horizontal',
                        margin: 100,
                        verticalAlign: 'top',
                        y: -45,
                        x:200,
                        symbolHeight: 40
                    },

                    tooltip: {
                        formatter: function () {

                            return 'X: '+ this.series.xAxis.categories[this.point.x] +'<br>'+
                            'Y: '+this.series.yAxis.categories[this.point.y]+'<br> value:'+this.point.value;
                        }
                    },

                    series: [{
                        name: 'Values for every plot',
                        borderWidth: 1,
                        data: heatmapLayout,
                        dataLabels: {
                            enabled: true,
                            color: '#000000'
                        },
                        turboThreshold: Number.MAX_VALUE 
                    }],
                    options: {
                        responsive: true,
                        maintainAspectRatio: false
                    }

                });

                if(col >5){

                    $('#heatmap-container').highcharts().setSize( 
                        57*row,
                        75*col);
                }else if(col==1){
                    $('#heatmap-container').highcharts().setSize( 
                        60*row,
                        180*col);
                }else  {
                    wFactor=60;
                    hFactor=100;
                    while(row*wFactor<$('#heatmap-container-parent').width()){
                        wFactor+=10;
                        hFactor+=10;
                    }
                    $('#heatmap-container').highcharts().setSize( 
                        wFactor*row,
                        hFactor*col
                        );
                }
                
                $('#heatmap-btn-group').removeClass('hidden');
                heatmapOrigChartWidth = heatmap.chartWidth,
                heatmapOrigChartHeight = heatmap.chartHeight,
                heatmapChartWidth = heatmapOrigChartWidth,
                heatmapChartHeight = heatmapOrigChartHeight;

                $('#heatmap-zoom-in').click(function () {
                    heatmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                    heatmapChartWidth *= 1.1;
                    heatmapChartHeight *= 1.1;

                    heatmap.setSize(heatmapChartWidth, heatmapChartHeight, doAnimation = true );
                    heatmap.hideLoading();
                });
                $('#heatmap-zoom-out').click(function () {
                    heatmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                    heatmapChartWidth *= 0.9;
                    heatmapChartHeight *= 0.9;
                    heatmap.setSize(heatmapChartWidth, heatmapChartHeight, doAnimation = true );
                    heatmap.hideLoading();

                });
                $('#heatmap-reset').click(function () {
                    heatmap.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
                    heatmapChartWidth = heatmapOrigChartWidth;
                    heatmapChartHeight = heatmapOrigChartHeight;
                    heatmap.setSize(heatmapOrigChartWidth, heatmapOrigChartHeight, doAnimation = true );
                    heatmap.hideLoading();
                    
                });
                $('#heatmap-container').scrollLeft = heatmap.chartWidth;

            }
        }

    });

}
/**
 * Renders default critical values, set and save preferred critical values
 */
 function renderCriticalValues(){
    // Save and retrieve preferred and default critical values 
    $('.get-min-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        belowOrigin: true,
        alignment: 'left',
        stopPropagation: true
    });

    $('.get-max-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        belowOrigin: true,
        alignment: 'left',
        stopPropagation: true
    });

    $('.save-critical-min-button').click(function(e) {
        e.preventDefault();

        $.ajax({
            url: 'save-var-preference',
            type: 'post',
            async:false,
            data: {variable_id: varId, field: 'min', value:$('#crit-min-val-id').val()},
            success: function (data) {
                var toastContent = $('<span class="black-text"> <i class="material-icons green-text">check_circle</i>Successfully saved your preferences!</span>').add($('<button class="btn-flat toast-close-btn black-text green" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                Materialize.toast(toastContent, 5000, 'white');

            }
        });

    });

    $('.save-critical-max-button').click(function(e) {
        e.preventDefault();

        $.ajax({
            url: 'save-var-preference',
            type: 'post',
            async:false,
            data: {variable_id: varId, field: 'max', value:$('#crit-max-val-id').val()},
            success: function (data) {
                var toastContent = $('<span class="black-text"> <i class="material-icons green-text">check_circle</i>Successfully saved your preferences!</span>').add($('<button class="btn-flat toast-close-btn black-text green" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                Materialize.toast(toastContent, 5000, 'white');

            }
        });

    });

    $('#get-max-prefer-id').click(function(e) {
        e.preventDefault();

        $.ajax({
            url: 'get-var-preference',
            type: 'post',
            async:false,
            data: {variable_id: varId},
            success: function (data) {
                data=JSON.parse(data);
                maxVal=data.critical_max_val;
                
                if(maxVal!=null){
                    $('#crit-max-val-id').val(maxVal);
                    Materialize.updateTextFields();
                }else{
                    var toastContent = $('<span class=""> <i class="material-icons amber-text">warning</i>There is no saved preferred maximum critical value.</span>').add($('<button class="btn-flat toast-close-btn black-text amber" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                    Materialize.toast(toastContent, 5000, '');
                }

            }
        });

    });
    $('#get-min-prefer-id').click(function(e) {
        e.preventDefault();

        $.ajax({
            url: 'get-var-preference',
            type: 'post',
            async:false,
            data: {variable_id: varId},
            success: function (data) {
                data=JSON.parse(data);

                minVal=data.critical_min_val;
                if(minVal!=null){
                    $('#crit-min-val-id').val(parseInt(minVal));
                    Materialize.updateTextFields();
                }else{
                    var toastContent = $('<span class=""> <i class="material-icons amber-text">warning</i>There is no saved preferred minimum critical value.</span>').add($('<button class="btn-flat toast-close-btn black-text amber" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
                    Materialize.toast(toastContent, 5000, '');
                }

            }
        });

    });

    $('#save-max-default-id').click(function(e) {
        e.preventDefault();
        if(criticalMaxValue!=null){
            $('#crit-max-val-id').val(criticalMaxValue);
            Materialize.updateTextFields();
        }else{
            var toastContent = $('<span class=""> <i class="material-icons amber-text">warning</i>There is no default maximum critical value.</span>').add($('<button class="btn-flat toast-close-btn black-text amber" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
            Materialize.toast(toastContent, 5000, '');
        }
    });

    $('#save-min-default-id').click(function(e) {
        e.preventDefault();
        if(criticalMinValue!=null){
            $('#crit-min-val-id').val(criticalMinValue);
            Materialize.updateTextFields();
        }else{
            var toastContent = $('<span class=""> <i class="material-icons amber-text">warning</i>There is no default minimum critical value.</span>').add($('<button class="btn-flat toast-close-btn black-text amber" style="background-color:transparent !important;"><i class="material-icons">close</i></button>'));
            Materialize.toast(toastContent, 5000, '');
        }

    });

    
    //setup before functions

    var typingTimer;                //timer identifier
    var doneTypingInterval = 10000;  //time in ms, 5 second for example

    //on keyup, start the countdown
    $('#crit-min-val-id').on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(getMinVal(this), doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $('#crit-min-val-id').on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," render histogram
    function getMinVal (value) {
        minVal=$(value).val();
        if(minVal==''){
            minVal=null;
        }
        renderHistogram();
    }

    //on keyup, start the countdown
    $('#crit-max-val-id').on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(getMaxVal(this), doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $('#crit-max-val-id').on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," render histogram
    function getMaxVal (value) {
        maxVal=$(value).val();
        if(maxVal==''){
            maxVal=null;
        }
        renderHistogram();
    }
}
// HISTOGRAM METHODS

function renderHistogram(){
    if($('#histogram-container').highcharts()!=undefined && histogram !=null){
        histogram.destroy();
    }
    var var_model = '';

    if(varDataLevel=='plot'){

        var_model = 'Plot';
        varQueryParams ='&Plot[status]='+status+'&Plot[variable]='+varAbbrev.toLowerCase()+'&Plot['+ varAbbrev.toLowerCase() +']=';

    }else if(varDataLevel=='entry'){

        varQueryParams ='&Entry[status]='+status+'&Entry[variable]='+varAbbrev.toLowerCase()+'&Plot['+ varAbbrev.toLowerCase() +']=';
        var_model = 'Entry';

    }else{

        var_model = 'Study';
        varQueryParams ='&Study[status]='+status+'&Study[variable_id][]='+varId+'&Study[value]=';
    }
    
    $.ajax({
        url: 'variable-data',
        type: 'post',
        async:false,
        data: {transactionId: varTransactionId, abbrev: varAbbrev, level:varDataLevel+"-"+"level"},
        success: function (data) {

            data=JSON.parse(data);
            histogram_data=JSON.parse(data);

            if(histogram_data!=null && histogram_data.length>0){
                rawData=histogram_data;
                binnedData = binData(rawData);
                mad = getMad(rawData);
                if(criticalMinValue!=null){
                    $('#crit-min-val-id').val(criticalMinValue);
                    Materialize.updateTextFields();
                }
                if(criticalMaxValue!=null){
                    $('#crit-max-val-id').val(criticalMaxValue);
                    Materialize.updateTextFields();
                }

            }
        }
    });

    if(mad!=null){
        histogram= new Highcharts.chart('histogram-container',{
            chart: {
                type: 'column',
                margin: [100, 25, 100, 50],
            },
            title: {
                text: 'Distribution of '+varLabel
            },
            legend: {
                enabled: true,
                symbolWidth: 25,
                symbolHeight: 25
            },
            tooltip: {},
            plotOptions: {
                area: {
                    pointStart: Math.min.apply(null, histogram_data),
                },
                series: {
                    dataLabels: { 
                        allowOverlap: true,
                        enabled: true, 
                        inside: false,
                        overflow: 'none',
                        crop: true,
                        shape: 'callout',
                        backgroundColor:'rgba(0,0,0,0.8)',
                        borderColor: 'rgba(0,0,0,0.9)',
                        color: 'rgba(255,255,255,0.75)',
                        borderWidth: .5,
                        borderRadius: 5,
                        y: -10,
                        style: {
                            fontFamily: 'Helvetica, sans-serif',
                            fontSize: '10px',
                            fontWeight: 'normal',
                            textShadow: 'none'
                        },
                        formatter: function() {

                        }
                    },
                    pointPadding: 0,
                    groupPadding: 0,
                    borderWidth: 0.5,
                    borderColor: 'rgba(255,255,255,0.5)'
                }
            },
            xAxis: [
            {
                
                title: {
                    text: 'Range'
                }

            }, 
            {
                linkedTo: 0,
                opposite: true,
                gridLineWidth: 0.5,
                gridLineColor: 'rgba(0,0,0,0.25)',
                gridZIndex: 8,
                tickPositions: [
                mad.median - (mad.mad * 3),
                mad.median - (mad.mad * 2),
                mad.median - mad.mad,
                mad.median,
                mad.median + mad.mad,
                mad.median + (mad.mad * 2),
                mad.median + (mad.mad * 3),
                ],
                title: {
                    text: 'Median and MAD'
                },
                labels: {
                    style: {
                        color: 'rgba(0,0,0,1)',
                        fomntWeight: 'bold'
                    },
                    formatter: function() {
                        return Highcharts.numberFormat(this.value, 2);
                    }
                },
                plotBands:[
                {
                    from: min,
                    to: minVal,
                    color:'url(#highcharts-default-pattern-2)',
                    zIndex: 10,
                    id: 'plot-line-crit-min',
                    events:{
                        click: function(evt) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams+minVal+'&'+var_model+'[operator]=<', replace:false }); 
                            
                            if(isCrossStudy=='false'){
                                filterMap({operator:'<',value: minVal, variable_id: varId});
                            }
                            
                        },

                    }

                },
                {
                    from: max,
                    to: maxVal,
                    color:'url(#highcharts-default-pattern-1)',
                    zIndex: 10,
                    id: 'plot-line-crit-max',
                    events:{
                        click: function(evt) {
                            setDataBrowser();
                            
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams+maxVal+'&'+var_model+'[operator]=>', replace:false }); 
                            
                            if(isCrossStudy=='false'){
                                filterMap({operator:'>',value: maxVal, variable_id: varId});
                            }

                        },

                    }

                }]
            }],
            yAxis: {
                title: {
                    text: 'Frequency'
                },
                min: 0
            },
        });

        histogram.xAxis[0].addPlotLine({
            value: mad.median,
            width: 1,
            color: 'rgba(0,0,0,0.5)',
            zIndex: 8
        });
        histogram.addSeries({
            name: 'Value',
            data: binnedData
        });

        histogram.addSeries({
            name: 'Below minimum critical value',
            color:'url(#highcharts-default-pattern-2)',
            type:'area',
            marker: {
                enabled: false
            },

            events: {
                legendItemClick: function(e) {
                    if(this.visible) {
                        this.chart.xAxis[0].removePlotLine('#plot-line-crit-min');
                    }
                    else {
                        this.chart.xAxis[0].addPlotLine('#plot-line-crit-min');
                    }
                }
            }

        });

        histogram.addSeries({
            name: 'Above maximum critical value',
            color:'url(#highcharts-default-pattern-1)',
            type:'area',
            marker: {
                enabled: false
            },
            events: {

                legendItemClick: function(e) {
                    if(this.visible) {
                        this.chart.xAxis[0].removePlotLine('#plot-line-crit-max');
                    }
                    else {
                        this.chart.xAxis[0].addPlotLine('#plot-line-crit-max');
                    }
                }
            }
        });

        histogram.xAxis[0].addPlotLine({
            value: mad.median,
            width: 1,
            color: 'rgba(0,0,0,0.5)',
            zIndex: 8
        });

        origChartWidth = $('#histogram-container').width(),
        origChartHeight = $('#histogram-container').height(),
        chartWidth = origChartWidth,
        chartHeight = origChartHeight;
        $('#histogram-btn-group').removeClass('hidden');
        $('#histogram-zoom-in').click(function () {
            histogram.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
            chartWidth *= 1.1;
            chartHeight *= 1.1;
            histogram.setSize(chartWidth, chartHeight);
            histogram.hideLoading();
        });
        $('#histogram-zoom-out').click(function () {
            histogram.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
            chartWidth *= 0.9;
            chartHeight *= 0.9;
            histogram.setSize(chartWidth, chartHeight);
            histogram.hideLoading();
        });
        $('#histogram-reset').click(function () {
            histogram.showLoading(' <div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');
            chartWidth = origChartWidth;
            chartHeight = origChartHeight;
            histogram.setSize(origChartWidth, origChartHeight);
            histogram.hideLoading();
        });
    }else{
        $("#critical-min-card-id").addClass("hidden");
        $("#critical-max-card-id").addClass("hidden");
        $("#stacked-card").removeClass("full");

        $("#stacked-card").toggleClass("full");
        $(".histogram-card").removeClass("hide");
        $(".histogram-card").toggleClass("hide");
        $(".heatmap-card").removeClass("hide");
        $(".heatmap-card").toggleClass("hide");
    }

}

function binData(data) {

    var hData = new Array(), //the output array
    size = data.length, //how many data points
    bins = Math.round(Math.sqrt(size)); //determine how many bins we need
    bins = bins > 50 ? 50 : bins; //adjust if more than 50 cells
    max = Math.max.apply(null, data), //lowest data value
    min = Math.min.apply(null, data); //highest data value
    var range = max - min, //total range of the data
    width = range / bins, //size of the bins
    bin_bottom, //place holders for the bounds of each bin
    bin_top;

    //loop through the number of cells
    for (var i = 0; i < bins; i++) {

        //set the upper and lower limits of the current cell
        bin_bottom = min + (i * width);
        bin_top = bin_bottom + width;

        //check for and set the x value of the bin
        if (!hData[i]) {
            hData[i] = new Array();
            hData[i][0] = bin_bottom + (width / 2);
        }

        //loop through the data to see if it fits in this bin
        for (var j = 0; j < size; j++) {
            var x = data[j];

            //adjust if it's the first pass
            i == 0 && j == 0 ? bin_bottom -= 1 : bin_bottom = bin_bottom;

            //if it fits in the bin, add it
            if (x > bin_bottom && x <= bin_top) {
                !hData[i][1] ? hData[i][1] = 1 : hData[i][1]++;
            }
        }
    }
    $.each(hData, function(i, point) {
        if (typeof point[1] == 'undefined') {
            hData[i][1] = null;
        }
    });
 
    return hData;
}

//get any percentile from an array
function getPercentile(data, percentile) {

    var index = (percentile / 100) * data.length;
    var result;
    if (Math.floor(index) == index) {
        result = (data[(index - 1)] + data[index]) / 2;
    } else {
        result = data[Math.floor(index)];
    }
    return result;
}

//get the median absolute deviation
function getMad(data) {
    var median = getPercentile(data, 50);
    var devs = [];
    $.each(data, function(i, point) {
        devs.push(Math.abs(point - median));
    });
    var mad = getPercentile(devs, 50);
    var output = {};
    output.median = median;
    output.mad = mad;
    return output;
}
// STACKED BAR GRAPH METHODS

// render browser or field map when the observation status is clicked
$('.stacked-card-toggle').click(function(e) {
    e.preventDefault();
    var status=$(this).children('.card-metric-title').children('.card-metric-title').text();
    
    setDataBrowser();
    
    if(varDataLevel=='plot'){
        
        varQueryParams ='&Plot[status]='+status+'&Plot[variable]='+varAbbrev.toLowerCase()+"";

    }else if(varDataLevel=='entry'){

        varQueryParams ='&Entry[status]='+status+'&Entry[variable]='+varAbbrev.toLowerCase()+"";

    }else{

        varQueryParams ='&Study[status]='+status+'&Study[variable_id][]='+varId+"";
    }
    $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
    
    filterMap({status:status.toLowerCase()});

});

function renderStackedBar(){
    var varQueryParams='';
    var var_model='';
    if(varDataLevel=='plot'){

        varQueryParams ='&Plot[status]='+status+'&Plot[variable]='+varAbbrev.toLowerCase()+"";
        var_model = 'Plot';
    }else if(varDataLevel=='entry'){
        
        varQueryParams ='&Entry[status]='+status+'&Entry[variable]='+varAbbrev.toLowerCase()+"";
        var_model = 'Entry';
    }else{
        
        varQueryParams ='&Study[status]='+status+'&Study[variable_id][]='+varId+"";
        var_model = 'Study';
    }

    $.ajax({
        url: "status",
        type: 'post',
        data: {transactionId: varTransactionId, abbrev: varAbbrev},
        success: function (data) {
            data=JSON.parse(data);

            if(varDataLevel=='plot-level' || varDataLevel=='plot' || varDataLevel=='map-plot-level'){
                level='plots';
                operationalCount=data.operational_plot;
                unitCount=data.plot_count;
                noDataCount=data.no_data_plot;
                $("#unit-count-label").text("Plot count");
                $("#unit-value-label").text(data.count+"/"+unitCount);

            }else if(varDataLevel=='entry-level' || varDataLevel=='entry' || varDataLevel=='map-entry-level'){
                level='entries';
                operationalCount=data.operational_entry;
                unitCount=data.entry_count;
                noDataCount=data.no_data_entry;
                $("#unit-count-label").text("Entry count");
                $("#unit-value-label").text(unitCount);
                $("#unit-value-label").text(data.count+"/"+unitCount);
            }else{
                unitCount=1;
                noDataCount=data.no_data_study;
                noDataCount=data.operational_study;
                noDataCount=data.no_data_study;
                $("#unit-count-label").text("");
                $("#unit-value-label").text("");
                $("#unit-value-label").text(data.count+"/"+unitCount);
            }

            $("#new-value-label").text(data.new_valid);
            $("#invalid-value-label").text(data.invalid);
            $("#suppressed-value-label").text(data.suppress);
            $("#voided-value-label").text(data.void);
            $("#updated-value-label").text(data.existing);
            $("#operational-value-label").text(data.operational);

            stackedBar=new Highcharts.chart('stackedbar-terminal-container',{
                chart: {type: 'bar'},
                legend: {enabled: false},
                title: {text: ''},
                tooltip:{
                    formatter:function(){
                        var pcnt = (this.y / unitCount ) * 100;
                        pcnt=pcnt.toFixed(2);
                        return this.series.name+": "+pcnt +"%";
                    }
                },
                xAxis: {
                    categories: ['']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total number of collected data in the transaction.'
                    },
                    max:unitCount
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    },
                    bar:{
                        dataLabels: {
                            enabled: true,
                            allowOverlap: true,
                            formatter: function() {
                                if (this.y != 0) {
                                    return this.y ;
                                } else {
                                    return null;
                                }
                            }
                        }
                    }
                },
                series: [
                {
                    name: 'New',
                    data: [data.new_valid,],
                    color: '#81C784',
                    events: {
                        click: function (event) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            
                            filterMap({status:'new'});
                        }
                    }
                }, {
                    name: 'Invalid',
                    data: [data.invalid],
                    color: '#f45b5b',
                    cursor: 'pointer',
                    events: {
                        click: function (event) {
                            
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            
                            filterMap({status:'invalid'});
                        }
                    }
                },{
                    name: 'Updated',
                    data: [data.existing],
                    color: '#ffb74d',
                    events: {
                        click: function (event) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            
                            filterMap({status:'updated'});
                        }
                    }
                },{
                    name: 'Suppressed',
                    data: [data.suppress],
                    color: '#a1887f',
                    events: {
                        click: function (event) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            
                            filterMap({status:'suppressed'});
                        }
                    }
                },{
                    name: 'Voided',
                    data: [data.void],
                    color: '#9e9e9e',
                    events: {
                        click: function (event) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            
                            filterMap({status:'voided'});
                            
                        }
                    }

                },{
                    name: 'Operational',
                    data: [data.operational],
                    color: '#2196F3',
                    visible:isOperationalVisible,
                    events: {
                        click: function (event) {
                            setDataBrowser();
                            $.pjax.reload({container: '#'+varDataLevel+'-data-browser-pjax',url:varResetGridUrl+varQueryParams, replace:false }); 
                            filterMap({status:'operational'});
                        }
                    }

                },{
                    name: 'No data',
                    data: [(noDataCount-data.valid_count)],
                    color: '#e0e0e0',
                    events: {
                        click: function (event) {
                            $('.tabs a[href="#suppress-tab-id"]').trigger('click');
                            $('#data-browser-container-id').addClass('hidden');
                            $('#field-map-container-id').removeClass('hidden');
                            $('#suppress-mode-btn-id').attr('data-tooltip', "Browser Mode"); 
                            $('#suppress-mode-icon-id').html("grid_on"); 
                            Materialize.fadeInImage('#field-map-container-id')
                        }
                    }
                }]
            });
        }
    });
}