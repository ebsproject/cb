/* 
 * This file is part of EBS-Core Breeding.
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
 */

//Binds Quality Control Suppress Actions

var var_delete = false;
var var_form_error = false;

// Triggers the hiiden 2nd tab in the modal which is the summary 
$('#suppress-next-btn-id').on('click', function(e) {
    e.preventDefault();

    $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

    $('#filter-error-summary-id').empty();
    $('#filter-error-summary-id').removeClass('error-summary');

    // save rule in db
    $('#filter-form-id').append('<input type="hidden" name="is_suppress" value=false ></input>');
    $('#filter-form-id').append('<input type="hidden" name="dataLevel" value='+$("#suppress-var-id option:selected").attr('data-data_level')+' ></input>');
    var form = $('#filter-form-id').serializeArray();
    form.push({
        "name": "is_suppress",
        "value": false
    });

    form.push({
        "name": "data_level",
        "value": false
    });

    $.ajax({
        url: "filter",
        type: 'post',
        async: false,
        data: $('#filter-form-id').serialize(),
        success: function(data) {

            var data = $.parseJSON(data);

            if (typeof data == 'object' && !data.success) { //has error do not display summary of suppress action

                $('#filter-error-summary-id').html(data.message);
                $('#filter-error-summary-id').addClass('error-summary');
                var_form_error = true;

                $('#unsuppress-next-btn-id').removeClass("hidden");    // unsuppress button
                $('#suppress-next-btn-id').removeClass("hidden");     // suppress button

                $('.suppress-back-btn-id').addClass("hidden");   //  back button
                $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

            } else {

                $('#suppress-summary-body-id').empty();
                $('#supress-tab-1-id').removeClass('disabled');
                $('#suppress-summary-body-id').html(data.message);
                $('ul.suppress-tabs').tabs('select_tab', 'suppress-summary-tab-id');

                if (parseInt($('#suppress-total-count-id').html()) == 0) {
                    
                    $('.suppress-back-btn-id').removeClass("hidden");   //  back button
                    
                    $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                    $('#suppress-next-btn-id').addClass("hidden");     // suppress button
                    $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                }else{

                    $('.suppress-back-btn-id').removeClass("hidden");   //  back button
                    $('.suppress-btn-id').removeClass('hidden'); // suppress confirm btn

                    $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                    $('#suppress-next-btn-id').addClass("hidden");     // suppress button
                    
                }
                
            }

            $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
            $('#filter-form-id input[name=dataLevel]').remove();
            $('#filter-form-id input[name=is_suppress]').remove();
            $('.loading-div').html('');
        },
        error: function(jqXHR, exception) {
            $('.loading-div').html('');
            error_log(jqXHR, exception);
        }

    });
});

// Trigger click hidden first tab in modal
$('.suppress-back-btn-id').on('click', function(e) {
    e.preventDefault();
    $('#suppress-var-id').val($('#suppress-var-id').val()).trigger('change');
    $('.suppress-back-btn-id').addClass("hidden");   //  back button
    
    $('#unsuppress-next-btn-id').removeClass("hidden");    // unsuppress button
    $('#suppress-next-btn-id').removeClass("hidden");     // suppress button

    $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
    $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

    $('ul.suppress-tabs').tabs('select_tab', 'condition-tab-id');

});

//suppress action
$('.suppress-btn-id').on('click', function(e) {
    e.preventDefault();
    $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
    var var_delete = false;

    if (parseInt($('#suppress-total-count-id').html()) > 200) {
        $('#factorModal').modal('hide');
        krajeeDialog.confirm($('#suppress-var-id').val() + ' of ' + varStudyName + " will be suppressed. This will be sent to a background process.", function(result) {
            if (result) {
                $.ajax({
                    url: "suppress-bg",

                    type: 'post',

                    data: {
                        transactionId: varTransactionId,
                        isSuppress: true,
                        varAbbrev: $('#suppress-var-id').val(),
                        dataLevel: $("#suppress-var-id option:selected").attr('data-data_level'),
                        variableId: $("#suppress-var-id option:selected").attr('data-id')
                    },
                    error: function(xhr, status, error) {

                        var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text" style="background-color:transparent !important;"><i class="material-icons red toast-close-btn">close</i></button>' +
                            '<ul class="collapsible toast-error grey lighten-2">' +
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                            '</li> </ul>');

                        Materialize.toast(toastContent, 'x', 'red');
                        $('.toast-error').collapsible();

                    },
                    success: function(response) {

                        data = JSON.parse(response);

                        if (!data.success) {
                            var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                                '<ul class="collapsible toast-error grey lighten-2">' +
                                '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                                '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + data.message + ' </blockquote></span></div>' +
                                '</li> </ul>');

                            Materialize.toast(toastContent, 'x', 'red');
                            $('.toast-error').collapsible();
                        } else {
                            if (data.message === "in queue") {

                                var toastContent = varStudyName + " is now in queue for suppression.";

                            } else {

                                var toastContent = varStudyName + " is now being suppressed.";

                            }
                            localStorage.setItem('commit_response', toastContent);

                            window.location = browseTerminalUrl + "&ODTTransactionSearch[id]=" + varTransactionId;

                        }

                    },
                    error: function(jqXHR, exception) {

                        $('.loading-div').html('');
                        msg = error_log(jqXHR, exception);

                        var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                            '<ul class="collapsible toast-error grey lighten-2">' +
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + msg + ' </blockquote></span></div>' +
                            '</li> </ul>');

                        Materialize.toast(toastContent, 'x', 'red');
                        $('.toast-error').collapsible();
                    }
                });
            }
        });
    } else {
        $.ajax({
            url: "suppress-terminal",

            type: 'post',

            data: {
                transactionId: varTransactionId,
                isSuppress: true,
                varAbbrev: $('#suppress-var-id').val(),
                dataLevel: $("#suppress-var-id option:selected").attr('data-data_level'),
            },
            success: function(data) {
                var_delete = true;
                
                Materialize.toast('Successully suppressed data!', 5000, 'green');
                $('.suppress-back-btn-id').trigger('click');
                $('.loading-div').html('');

                if(hasPlot){
                    $.pjax.reload({container: '#plot-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                if(hasEntry){
                    $.pjax.reload({container: '#entry-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                if(hasStudy){
                    $.pjax.reload({container: '#study-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                $.pjax.reload({container: '#data-collection-browser-pjax'}); 
                $.pjax.xhr = null;
                $.pjax.reload({container: '#complete-browser-pjax'}); 
                $.pjax.xhr = null;
                $('#suppress-var-id').val($('#suppress-var-id').val()).trigger('change');

            },
            error: function(jqXHR, exception) {

                $('.loading-div').html('');
                msg = error_log(jqXHR, exception);

                var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + msg + ' </blockquote></span></div>' +
                    '</li> </ul>');

                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    }
});

// undo suppress action
$('.undo-suppress-btn-id').on('click', function(e) {
    e.preventDefault();
    $('.loading-div').html('<div class="progress"><div class="indeterminate"></div></div>');
    var var_delete = false;

    if (parseInt($('#suppress-total-count-id').html()) > 200) {
        $('#factorModal').modal('hide');
        krajeeDialog.confirm($('#suppress-var-id').val() + ' of ' + varStudyName + " will be unsuppressed. This will be sent to a background process.", function(result) {
            if (result) {
                $.ajax({
                    url: "suppress-bg",

                    type: 'post',

                    data: {
                        transactionId: varTransactionId,
                        isSuppress: false,
                        varAbbrev: $('#suppress-var-id').val(),
                        dataLevel: $("#suppress-var-id option:selected").attr('data-data_level'),
                        variableId: $("#suppress-var-id option:selected").attr('data-id')
                    },
                    error: function(xhr, status, error) {

                        var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text" style="background-color:transparent !important;"><i class="material-icons red toast-close-btn">close</i></button>' +
                            '<ul class="collapsible toast-error grey lighten-2">' +
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                            '</li> </ul>');

                        Materialize.toast(toastContent, 'x', 'red');
                        $('.toast-error').collapsible();

                    },
                    success: function(response) {

                        data = JSON.parse(response);
                        if (!data.success) {

                            var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                                '<ul class="collapsible toast-error grey lighten-2">' +
                                '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                                '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + data.message + ' </blockquote></span></div>' +
                                '</li> </ul>');

                            Materialize.toast(toastContent, 'x', 'red');
                            $('.toast-error').collapsible();
                        } else {
                            if (data.message === "in queue") {

                                var toastContent = varStudyName + " is now in queue for undo suppression.";
                            } else {

                                var toastContent = varStudyName + " is now being unsuppressed.";
                            }

                        }
                        localStorage.setItem('commit_response', toastContent);

                        window.location = browseTerminalUrl + "&ODTTransactionSearch[id]=" + varTransactionId;
                    },
                    error: function(jqXHR, exception) {

                        $('.loading-div').html('');
                        msg = error_log(jqXHR, exception);
                        var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                            '<ul class="collapsible toast-error grey lighten-2">' +
                            '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                            '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + msg + ' </blockquote></span></div>' +
                            '</li> </ul>');

                        Materialize.toast(toastContent, 'x', 'red');
                        $('.toast-error').collapsible();
                    }
                });
            }
        });
    } else {
        var var_delete = true;
        $.ajax({
            url: "unsuppress",
            dataType: 'html',
            type: 'post',
            async: false,
            data: {
                id: $(this).attr("data-id"),
                transactionId: varTransactionId,
                isSuppress: false,
                varAbbrev: $('#suppress-var-id').val(),
                dataLevel: $("#suppress-var-id option:selected").attr('data-data_level'),
                variableId: $("#suppress-var-id option:selected").attr('data-id')
            },
            success: function(data) {
                var_delete = true;
                Materialize.toast('Undo suppression successful!', 5000, 'green');
                $('.loading-div').html('');
                $('.suppress-back-btn-id').trigger('click');
                
                if(hasPlot){
                    $.pjax.reload({container: '#plot-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                if(hasEntry){
                    $.pjax.reload({container: '#entry-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                if(hasStudy){
                    $.pjax.reload({container: '#study-data-browser-pjax',});
                    $.pjax.xhr = null;
                }
                $.pjax.reload({container: '#data-collection-browser-pjax'}); 
                $.pjax.xhr = null;
                $.pjax.reload({container: '#complete-browser-pjax'}); 
                $.pjax.xhr = null;
                $('#suppress-var-id').val($('#suppress-var-id').val()).trigger('change');

            },
            error: function(jqXHR, exception) {
                $('.loading-div').html('');
                msg = error_log(jqXHR, exception);
                var toastContent = $('<span>' + 'There was an internal error with your request. <button class="btn-flat toast-action black-text toast-close-btn" style="background-color:transparent !important;"><i class="material-icons red">close</i></button>' +
                    '<ul class="collapsible toast-error grey lighten-2">' +
                    '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                    '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + msg + ' </blockquote></span></div>' +
                    '</li> </ul>');

                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();

            }
        });
        if (var_delete) {
            $(this).closest('tr').remove();
        }
    }

});

// undo suppress action
$('#unsuppress-next-btn-id').on('click', function(e) {
    e.preventDefault();

    $('.loading-div').html(' <div class="progress"><div class="indeterminate"></div></div>');

    $('#suppress-summary-body-id').empty();
    $.ajax({
        url: "get-suppress-summary",

        type: 'post',
        async: false,
        data: {
            transactionId: varTransactionId,
            var_abbrev: $('#suppress-var-id').val(),
            is_suppress: true,
            dataLevel: $("#suppress-var-id option:selected").attr('data-data_level'),
        },
        success: function(data) {

            if (data !== '') {
                $('#supress-tab-1-id').removeClass('disabled');
                $('#suppress-summary-body-id').html(data);
                $('ul.suppress-tabs').tabs('select_tab', 'suppress-summary-tab-id');
               
                if (parseInt($('#suppress-total-count-id').html()) == 0) {
                    
                    $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                    $('#suppress-next-btn-id').addClass("hidden");     // suppress button
                    $('.undo-suppress-btn-id').addClass("hidden");   // unsuppress confirm button
                    $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn

                }else{
                    
                    $('#unsuppress-next-btn-id').addClass("hidden");    // unsuppress button
                    $('#suppress-next-btn-id').addClass("hidden");     // suppress button
                    $('.undo-suppress-btn-id').removeClass("hidden");   // unsuppress confirm button
                    $('.suppress-btn-id').addClass('hidden'); // suppress confirm btn
                }
                
                $('.suppress-back-btn-id').removeClass("hidden");   //  back button
                
                $('.loading-div').html('');
            }
        },
        error: function(jqXHR, exception) {
            $('.loading-div').html('');
            msg = error_log(jqXHR, exception);
        }
    });

});

function renderForm() {

    // Stop user to press enter in textbox 
    $("input:text").keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('.browse-data-filter-id').on('click', function(e) {
        e.preventDefault();
        $('#select-var-map-id').val($('#suppress-var-id').val()).trigger('change');
        setDataBrowser();

        var param_filter = {
            status: 'suppressed'
        };
        filterMap(param_filter);

    });

}

/*END RENDER FORM*/

function changeValue() {
    
    var isValueDropDown = false;
    // Change the operator and or values when a variable is selected in the filter form

    // Add remarks when a value is inputted
    // input field is text field 
    $('input.filter-form-value').change(function(e) {
        e.preventDefault();
        
        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');



        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        $(this).parent().parent().find('textarea').val(remarks);

        isValueDropDown = false;
    });
    // input field is a dropdown 
    $('select.filter-select-value-select2').on('change', function(e) {
        e.preventDefault();


        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');



        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        tr.find('textarea').val(remarks);

        isValueDropDown = true;
    });
    // input field is a dropdown 
    $('.suppress-datepicker').on('change', function(e) {
        e.preventDefault();

        var v = $(this).val();

        var self = $(this);
        var tr = self.closest('tr');



        var remarks = tr.find('.filter-select-var :selected').val() + " " + tr.find('.filter-select-operator :selected').val() + " " + v;

        tr.find('textarea').val(remarks);

        isValueDropDown = true;
    });

    $('select.filter-select-operator').on('change', function(e) {
        e.preventDefault();
    
        var v = $(this).val();
        var self = $(this);
        var tr = self.closest('tr');
        var varDataType = $('option:selected', '#plot-suppress-rule-select2-id').attr('data-data_type');

        var fldValue;

        if (isValueDropDown) {
            fldValue = tr.find('select.filter-select-value-select2').val();
        } else if (varDataType == 'date') {
            fldValue = tr.find('.suppress-datepicker').val();
        } else {
            fldValue = tr.find('.filter-form-value').val();

        }

        var remarks = tr.find('.filter-select-var').val() + " " + v + " " + fldValue;

        tr.find('textarea').val(remarks);
    });

    $('input.filter-select-operator').change(function(e) {
        e.preventDefault();
        
        var v = $(this).val();
        var self = $(this);
        var tr = self.closest('tr');



        var fldValue;

        if (isValueDropDown) {
            fldValue = tr.find('select.filter-form-value').val();
        } else {
            fldValue = tr.find('.filter-form-value').val();
        }

        var remarks = tr.find('.filter-select-var').val() + " " + v + " " + fldValue;

        tr.find('textarea').val(remarks);
    });

    $('input.filter-select-var').change(function(e) {
        
        e.preventDefault();
        
        

        var self = $(this);
        var tr = self.closest('tr');



        var filter_data_level = tr.find('.filter-select-var :selected').attr('data_level') ;

        $(this).parent().parent().find('.filter-form-data-level').val(filter_data_level);

        isValueDropDown = false;
    });

}

//END SUPPRESS FORM 
