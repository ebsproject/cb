/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

/**
* This contains the js scripts for the dynamic fields in the modal defined in the configuration
*/

var variable = '';
$(document).on('change','#req-vars',function(){
    variable = $(this).val();
  
    if(variable != ""){
         $.ajax({
             url: genericRenderConfigUrl,
             type: 'post',
             dataType: 'json',
             data:{
                selected: variable, 
                configVars: requiredFieldJson,
                formType: 'modal'
            },
             success: function(response){
                $('#input-field-var').html(response);
                $('.select-input').css('visibility','visible');
                $('.select-input').css('width','');
                $('.select-input').css('height','');
             },
             error: function(){
                 
             }
         });
    }else{
     $('#input-field-var').html('');
    }
});

$(document).on('click','.confirm-update-btn',function(e){
    var selectedItems= [];
    var selectedItemsCount = 0;
    var mode = $(this).attr('mode');


    $.ajax({
        url: getSelectedItemsUrl,
        type: 'post',
        dataType: 'json',
        data:{
            plantingJobDbId : plantingJobId,
            sessionName: sessionName
        },
        success: function(response) {
            selectedItems = response['selectedItems'];
            selectedItemsCount = response['selectedItemsCount'];
            
            if((selectedItemsCount > 0 && selectedItemsCount !== 'No' && mode == "selected") || mode == "all"){
                // no variable selected 
                if(typeof variable === 'undefined' || variable == null || variable == ''){
                    // hide loading indicator
                    $('#modal-content').removeClass('hidden');
                    $('#modal-progress').addClass('hidden');
                    $('.confirm-update-btn').removeClass('disabled');
                    var hasToast = $('body').hasClass('toast');
                    if(!hasToast){
                        $('.toast').css('display','none');
                        var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>No variable selected.</span>";
                        Materialize.toast(notif, 5000);
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }else{
                    var field = checkField();
                    if(field['success']){

                        // show loading indicator
                        $('#modal-content').removeClass('hidden');
                        $('#modal-progress').removeClass('hidden');
                        $('.confirm-update-btn').addClass('disabled');
                        
                        $.ajax({
                            url: bulkUpdateFieldsUrl,
                            type: 'post',
                            dataType: 'json',
                            data: {
                                mode: mode,
                                variable: variable,
                                dataValue: field['value'],
                                plantingJobDbId: plantingJobId,
                                modelClass: modelClass,
                                sessionName: sessionName,
                                entityIdName: entityIdName
                            },
                            success: function(response) {
                                variable = response['variable'];

                                var variableId = response['variableDbId'];
                                $.each(response['selectedItems'], function(index, value){
                                    var type = 'identification';
                                    var fieldName = variable + '-' + type + '-' + variableId + '-' + value;

                                    if(field['inputType'] == 'dropdown'){
                                        $('#select2-' + fieldName + '-container').html(field['valueText']);
                                    }
                                    else{
                                        $('#' + fieldName).val(field['value']);
                                    }
                                });
                           
                                if(!hasToast){
                                    $('.toast').css('display','none');
                                    var s = (response['count']) > 1 ? 's' : '';
                                    var notif = "<i class='material-icons green-text left'>check</i> <span class='white-text'>Successfully updated <b>"+response['count']+"</b> item"+s+".</span>";
                                    Materialize.toast(notif, 5000);
                                    $("[class*=input-validate]").val(null).trigger("change"); 
                                    $("[class*=input-validate]").select2("val", "");
                                }

                                // hide loading indicator
                                $('#modal-content').removeClass('hidden');
                                $('#modal-progress').addClass('hidden');
                                $('.confirm-update-btn').removeClass('disabled');

                            },
                            error: function(e) {
                            }
                        });
                        
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            }
            else{ // no items selected
                // hide loading indicator
                $('#modal-content').removeClass('hidden');
                $('#modal-progress').addClass('hidden');
                $('.confirm-update-btn').removeClass('disabled');
                var hasToast = $('body').hasClass('toast');
                if(!hasToast){
                    $('.toast').css('display','none');
                    var notif = "<i class='material-icons orange-text left'>warning</i> <span class='white-text'>No items selected.</span>";
                    Materialize.toast(notif, 5000);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }

        },
        error: function(e) {
        }
    });
});
