/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

// This contains the JS fuunctions to manage notifications in the Germplasm tool

/**
 * Updates notifications
 */
function updateNotif() {
    $.getJSON(gmNewNotificationsUrl, 
        {},
        function(json){
            $('#notif-badge-id').html(json);

            if(parseInt(json)>0){
                $('#notif-badge-id').addClass('notification-badge red accent-2');
            }else{
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
            }
    })
    .fail( function(jqXHR, textStatus, errorThrown) {
        console.log("Message: "+textStatus+" : "+errorThrown);
    });
}

/**
 * Initialize dropdown materialize  for notifications
 */
function renderNotifications(){
    $('#'+gmNotifsBtnId).dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: false,
        gutter: 0,
        belowOrigin: true,
        alignment: 'right',
        stopPropagation: true
    });
}

/**
 * Renders the notification in dropdown UI
 */
function notifDropdown(){
    $('#'+gmNotifsBtnId).on('click',function(e){
        $.ajax({
            url: gmNotificationsUrl,
            type: 'post',
            data: {},
            async:false,
            success: function(data) {
    
                var pieces = data.split('||');
                var height = pieces[1];
    
                $('<style>#notifications-dropdown.dropdown-content { min-height: ' + height + 'px; }</style>').appendTo('body');
    
                data = pieces[0];
    
                renderNotifications();
    
                $('#notif-badge-id').html('');
                $('#notif-badge-id').removeClass('notification-badge red accent-2');
                $('#notifications-dropdown').html(data);

                $('.bg_notif-btn').on('click', function(e){

                    var workerName = $(this).data('worker-name')
                    var fileName = $(this).data('filename')

                    switch(workerName.toUpperCase()){
                        case 'BUILDHELIUMDATA':
                            downloadExportFile(fileName, 'helium');
                            break;
                        case 'BUILDCSVDATA':
                            downloadExportFile(fileName, 'csv');
                            break;
                        default:
                            // refresh browser
                            $.pjax.reload({
                                container: '#'+gmBrowserId+'-pjax',
                                url: gmPjaxReloadUrl
                            });

                            break;
                    }
                });
            },
            error: function(xhr, status, error) {
                var toastContent = $('<span>' + 'There was an internal error with your request.'+ 
                '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
                '<i class="material-icons red toast-close-btn">close</i></button>' +
                '<ul class="collapsible toast-error grey lighten-2">' +
                '<li><div class="collapsible-header black-text"><i class="material-icons">error_outline</i>Show error details</div>' +
                '<div class="collapsible-body grey-text" style="padding: .5rem !important;"><span><blockquote>' + xhr.responseText + ' </blockquote></span></div>' +
                '</li> </ul>');
    
                Materialize.toast(toastContent, 'x', 'red');
                $('.toast-error').collapsible();
            }
        });
    });
}

/**
 * Facilitate download of exported file
 * 
 * @param {String} text file name from notification
 * @param {String} dataFormat data format of file to be downloaded
 */
function downloadExportFile(text, dataFormat){
    var downloadDataUrl = '/index.php/occurrence/default/download-data'

    var toastContent = ''
    if(text == undefined || text == ''){
        toastContent = $('<span>' + 'The file name for the requested export file is unavailable.'+ 
        '<button class="btn-flat toast-close-btn black-text" style="background-color:transparent !important;">'+
        '<i class="material-icons red toast-close-btn">close</i></button>');

        Materialize.toast(toastContent, 'x', 'red');
    }
    else{
        var pieces = text.split('.')
        var fileName = pieces[0]

        downloadLink =  window.location.origin + `` + downloadDataUrl + `?filename=` + fileName + `&dataFormat=` + dataFormat
        window.location.href = downloadLink

        // remove loading indicator after 5 seconds
        setTimeout( function(){
            $('.progress').css('display','none');
            $('.indeterminate').css('display','none');
        }, 5000);
    }
}

/**
 * Remove error message
 */
$(document).on('click', '.toast-close-btn', function(e) {
	
	e.preventDefault()
	$(this).parent().parent().fadeOut(function() {
		$(this).remove();
	});
});