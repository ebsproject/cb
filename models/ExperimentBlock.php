<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

class ExperimentBlock extends BaseModel 
{
  /**
  * Set api endpoint
  */
  public static function apiEndPoint() {
    return 'experiment-blocks';
  }


  /**
  * Retrieve experiment blocks
  * @param $params object filter parameters
  */
  public function searchExperimentBlocks($params, $sort=''){
    $method = 'POST';
    $path = 'experiment-blocks-search'.$sort;
    
    $data = Yii::$app->api->getResponse($method, $path, json_encode($params), null, true);
    
    $result = [];

    // Check if it has a result, then retrieve menu data
    if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
      $result = $data['body']['result']['data'];
    }

    return $result; 
  }
}