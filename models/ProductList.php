<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\data\SqlDataProvider;

/**
 * Model class for List that is related to products
 */

class ProductList extends PlatformList
{
    /**
    * Load product list data provider
    *
    * @param $ids array list of product IDs
    * @param $table string table name in the database
    *
    * @return mixed array data provider and total count of products
    */
    public static function loadProductListProvider($ids = null, $table = 'product') {
        $addtlCond = '';

        if(!empty($ids)) {   // all variables will be saved from the list
            $idsStr = implode(',', $ids);
            $tableClause = 'p';
            $addtlCond = " and $tableClause.id in ($idsStr)";
        }

        $sql = "
            SELECT
                designation,
                product_type,
                generation,
                parentage
            FROM
                master.product p
            WHERE
                p.is_void = false
                {$addtlCond}
        ";

        $countSql = "SELECT count(*) FROM ({$sql}) AS a";
        $count = Yii::$app->db->createCommand($countSql)->queryScalar();

        $sql = $sql . ' limit 100';

        $sort = [
            'attributes' => ['id','designation','product_type','generation','parentage']
        ];

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'pagination' => false,
            'sort' => $sort
        ]);

        return ['dataProvider'=>$dataProvider,'count'=>$count];
    }

    /**
     * Save as new variable list
     *
     * @param $id integer list identifier
     * @param $ids array list of variable identifiers
     * @param $attributes array list attributes
     */
	public static function saveProductList($id, $ids, $attributes) {
        $userModel = new User();
        $userId = $userModel->getUserId();    // get user identifier

        extract($attributes);

        $addtlCond = '';
        $orderBy = '';
        if(!empty($ids)) {  // all variables will be saved from the list
            $idsStr = implode(',', $ids);
            $addtlCond = " and pr.id in ($idsStr)";
            $orderBy = "ORDER BY POSITION(id::text in '({$idsStr})')";
        }

        $insertStr = "INSERT INTO platform.list_member(list_id, data_id, order_number, display_value, creator_id) ";
        $sql = "
            {$insertStr}
            SELECT
                {$id},
                pr.id,
                row_number() over({$orderBy}),
                pr.designation,
                {$userId}
            FROM
                master.product pr
            WHERE
                pr.is_void = false
                {$addtlCond}
        ";
        Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * Checks if the list's abbrev and display name are unique
     *
     * @param $abbrev integer list identifier
     * @param $displayName array list of variable identifiers
     *
     * @return mixed array contains boolean for result and string for message
     */
    public static function validateProductList($abbrev, $displayName) {
        $checkIfAbbrevExistsSql = "SELECT id FROM platform.list WHERE upper(abbrev) = '{$abbrev}'";
        $abbrevExists = Yii::$app->db->createCommand($checkIfAbbrevExistsSql)->queryColumn();

        if(!empty($abbrevExists)){
            return ['result' => false, 'message' => 'Abbrev already exists'];
        }

        $checkIfDisplayNameExistsSql = "SELECT id FROM platform.list WHERE display_name = '{$displayName}'";
        $displayNameExists = Yii::$app->db->createCommand($checkIfDisplayNameExistsSql)->queryColumn();

        if(!empty($displayNameExists)){
            return ['result' => false, 'message' => 'Display name already exists'];
        }

        return ['result' => true];
    }
}