<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.scale_conversion".
 *
 * @property int $id Identifier of the record within the table
 * @property int $source_unit_id Source/base scale unit of measurement
 * @property int $target_unit_id Target/output scale unit of measurement
 * @property double $conversion_value Value to multiply to the source unit to arrive to the target unit
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ScaleConversion extends ScaleConversionBase
{


}
