<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\interfaces\models\IApi;

/**
 * This is the model class for API resource
 */

class Api implements IApi
{
    
    /**
     * Retrieve result of an API resource
     *
     * @param text $method method of the API resource
     * @param text $url path of the API resource
     * @param array $rawData post parameters of the API resource
     * @param text $filter filter parameters of the API resource
     * @param boolean $retrieveAll flag whether to retrieve all results or not
     *
     * @return array result of the API resource
     */
	public function getApiResults($method, $url, $rawData = null, $filter = '', $retrieveAll = false){

        return Yii::$app->api->getParsedResponse(
            $method,
            $url,
            $rawData,
            $filter,
            $retrieveAll
        );
    }
}