<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "seed_warehouse.seed_list".
 *
 * @property int $id Identifier of the record within the table
 * @property int $program_id Program of the seed list
 * @property int $year Year when the seed list was created
 * @property int $seed_list_number Sequence number of the seed list within the same program, year, and season
 * @property string $seed_list_name Auto-generated name of the seed list with format SL-<program abbrev>-<year>-<seed list number>
 * @property string $seed_list User-defined name of the seed list with default format SL<2-digit year><program letter code><2-digit seed list number>
 * @property string $seed_list_title Title of the seed list
 * @property int $seed_list_item_count Number of seed lots in the seed list
 * @property string $seed_list_status Status of seed list: DRAFT, COMPLETED
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log
 */
class SeedListBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seed_warehouse.seed_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['program_id', 'year', 'seed_list_number', 'seed_list_name', 'seed_list', 'seed_list_status'], 'required'],
            [['program_id', 'year', 'seed_list_number', 'seed_list_item_count', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['program_id', 'year', 'seed_list_number', 'seed_list_item_count', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['seed_list_name', 'seed_list', 'seed_list_title'], 'string', 'max' => 256],
            [['seed_list_status'], 'string', 'max' => 64],
            [['seed_list'], 'unique'],
            [['seed_list_name'], 'unique'],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'year' => 'Year',
            'seed_list_number' => 'Seed List Number',
            'seed_list_name' => 'Seed List Name',
            'seed_list' => 'Seed List',
            'seed_list_title' => 'Seed List Title',
            'seed_list_item_count' => 'Seed List Item Count',
            'seed_list_status' => 'Seed List Status',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
        ];
    }
}
