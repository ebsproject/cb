<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;
use \app\models\SeedStorage;
use app\modules\seedInventory\models\FindSeedDataBrowserModel;

/**
 * Model class for dynamic Entry data
 */
class SeedStorageSearch extends SeedStorage{
    public $label;
    public $study_name;
    public $experiment_name;
    public $experiment_year;
    public $designation;
    public $plot_code;
    public $entry_code;
    public $parentage;
    public $facility_name;
    public $volume;
    public $unit;
    public $container;
    public $harvest_year;
    // now set the rules to make those attributes safe
    public function rules()
    {
        return [ 
            // ... more stuff here
            [
                ['id',
                'label', 
                'study_name',
                'experiment_name',
                'experiment_year',
                'designation',
                'plot_code', 
                'entry_code', 
                'parentage', 
                'gid', 
                'facility_name', 
                'volume', 
                'unit', 
                'container', 
                'harvest_year',],'safe'],

                [['id',
                'label', 
                'study_name',
                'experiment_name',
                'experiment_year',
                'designation',
                'plot_code', 
                'entry_code', 
                'parentage', 
                'gid', 
                'facility_name', 
                'volume', 
                'unit', 
                'container', 
                'harvest_year',], 'default', 'value' => null]
            // ... more stuff here
        ];
    }

    /**
     * Retrieve entry data
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function search($params, $dataFilters){
        $this->load($params);
        $condStr = '';
        
   
        if($params){
            foreach ($params['SeedStorageSearch'] as $key => $value) {
                if($value!==null && $value!=''){
                    $key = strtolower($key);
                    if($key == 'label'){
                        $condStr = $condStr . " and cast(ss.label as text) ilike '%". $value ."%'";
                    } else if($key == 'study_name'){
                        $condStr = $condStr . " and cast(os.study as text) ilike '%". $value ."%'";
                    } else if($key == 'experiment_name'){
                        $condStr = $condStr . " and cast(oe.experiment_name as text) ilike '%". $value ."%'";
                    } else if($key == 'experiment_year' || $key == 'year'){
                        //$value = preg_replace("/[^0-9]/", "", $value);
                        $condStr = $condStr . " and cast(oe.year as text) ='".$value. "'";
                    } else  if($key == 'designation'){
                        $condStr = $condStr . " and cast(mp.designation as text) ilike '%". $value ."%'";
                    } else if($key == 'plot_code'){
                        $condStr = $condStr . " and cast(op.code as text) ilike '%". $value ."%'";
                    } else if($key == 'entry_code'){
                        $condStr = $condStr . " and cast(oet.entcode as text) ilike '%". $value ."%'";
                    } else if($key == 'parentage'){
                        $condStr = " and cast(mp.parentage as text) ilike '%". $value ."%'";
                    } else if($key == 'gid'){
                        $condStr = $condStr . " and cast(ss.gid as text) ='".$value. "'";
                    } else if($key == 'facility_name'){
                        $condStr = $condStr . " and cast(mf.name as text) ilike '%". $value ."%'";
                    } else if($key == 'volume'){
                        $condStr = $condStr . " and cast(ss.volume as text) ='".$value. "'";
                    }else if($key == 'unit'){
                        $condStr = $condStr . " and cast(ss.unit as text) ilike '%". $value ."%'";
                    }else if($key == 'container'){
                        $condStr = $condStr . " and cast(swc.name as text) ilike '%". $value ."%'";
                    }else if($key == 'harvest_year'){
                        $condStr = $condStr . " and cast(ss.year as text) ='".$value. "'";
                    }
                    
                    Yii::$app->session->set('condStr',$condStr);
                }
            }
        }

        if (!empty($dataFilters)) {
            foreach($dataFilters as $key => $value) {
                $abbrev = isset($value['name']) ? $value['name'] : '';
                $values = isset($value['value']) ? $value['value'] : '';

                $dataValue = '';

                if(!empty($values)){
					if(isset($value['name']) && $value['name'] =='DESIGNATION'){
                        
                        $values = explode(',',$value['value']);
                        $dataValue = "($$".implode("$$),($$",$values)."$$)";
					}
					else{
                        if (!is_array($values)) {
                            $values = array_map('intval', explode(',',$values));
                            $dataValue = 'in ('.implode(",",$values).')';
                        }
                    }

                    if($abbrev === 'YEAR'){
                        $condStr = $condStr . ' and oe.year ' . $dataValue . ' AND ss.source_experiment_id is not NULL';
                    }
        
                    if($abbrev === 'PROGRAM'){
                        $condStr = $condStr . ' and ss.program_id ' . $dataValue;
                    }

                    if($abbrev === 'EXPERIMENT_NAME'){
                        $condStr = $condStr . ' and ss.source_experiment_id ' . $dataValue;
                    }
        
                    if($abbrev === 'STUDY_NAME'){
                        $condStr = $condStr . ' and ss.source_study_id ' . $dataValue;
                    }
        
                    if($abbrev === 'PHASE'){
                        $condStr = $condStr . ' and oe.phase_id ' . $dataValue;
                    }

                    if($abbrev === 'DESIGNATION'){
	
                        $designation = $dataValue;
                        
                        $data = FindSeedDataBrowserModel::getNormalizedDesignation($designation);
    
                        if(!empty($data)){
                            $param = '(';
                            $count = 1;
                            
                            foreach($data as $key =>$value){
                                $param = $param.$value['product_id'];
                                if($count < count($data)){ $param = $param . ", "; }
                                $count = $count+1;
                            }
                            $param = $param . ")";
                            
                            $condStr = $condStr . ' and ss.product_id in '.$param;
                        }
                    }

                    if($abbrev == 'GID'){
                        $condStr = $condStr . ' and ss.gid ' . $dataValue;
                    }
    
                    if($abbrev == 'SOURCE_HARV_YEAR'){
                        $condStr = $condStr . ' and ss.year ' . $dataValue;
                    }
        
                    if($abbrev == 'CONTAINER'){
                        $condStr = $condStr . ' and ss.container_id ' . $dataValue;
                    }
    
                    if($abbrev == 'FACILITY'){
                        $condStr = $condStr . ' and ss.facility_id ' . $dataValue . ' AND mf.parent_id is NULL';
                    }
        
                    if($abbrev == 'SUB_FACILITY'){
                        $condStr = $condStr . ' and ss.facility_id ' . $dataValue . ' AND mf.parent_id is not NULL';
                    }
        
                    if($abbrev == 'PLOT_CODE'){
                        $code = "('".implode("','",$value['values'])."')"; // test for errors with '
                        
                        $condStr = $condStr . ' and op.code in '.$code;
                    }
                }
            }
        }
        $data = FindSeedDataBrowserModel::getDataProvider($condStr);
        return $data['data'];
    }
}
?>