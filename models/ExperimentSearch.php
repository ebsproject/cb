<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

use app\modules\experimentCreation\models\BrowserModel;

/**
 * This is the ActiveQuery class for [[StudySearch]].
 *
 * @see SeasonSearch
 */
class ExperimentSearch extends Experiment
{
    private $data;
    private $dynamicRules = [];

    public function __get($varName){

        if($this->data != null && isset($this->data[$varName])){
            return $this->data[$varName];
        }
    }

    public function __set($varName, $value){
       $this->data[$varName] = $value;
    }

    public function rules(){
        return $this->dynamicRules;
    }

    public function __construct(
        public BrowserModel $browserModel
    )
    {
        
    }
   
     /**
     * Search model for experiment
     *
     * @param array $params list of query parameters
     * @param string $pageParams data browser parameters
     * @param string $currentPage current page parameter
     * @param boolean $copyView flag if the browser is for entry list
     * @return array $dataProvider experiment browser data provider
     */
    public function search($params = [], $pageParams='', $currentPage = null, $copyView = false){

        $dataProvider = [];
        if(isset($params)){

            extract($params);
            
            $filters = isset($ExperimentSearch) ? $this->browserModel->formatFilters($ExperimentSearch) : [];
            $results = Experiment::getDataProvider($filters,$pageParams,$currentPage, $copyView);
            $this->dynamicRules = [[$results['attributes'], 'safe']];
            $dataProvider = $results['dataProvider'];

            if (!($this->load($params) && $this->validate())) {
                //validate results
                return $dataProvider;
            }
            
        }
        //no results found
        return $dataProvider;
    }
}