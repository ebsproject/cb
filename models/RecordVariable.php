<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/


namespace app\models;
use ChromePhp;
use Yii;

/**
 * This is the model class for table "master.record_variable".
 *
 * @property int $id Primary key of the record in the table
 * @property int $record_id Parent record ID
 * @property int $variable_id Child variable ID
 * @property bool $is_mandatory
 * @property int $order_number order_number of variable in record
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $variable_relation_id Relation of this variable with another variable
 * @property bool $is_hidden_by_def Whether variable is hidden by default or not
 */

class RecordVariable extends RecordVariableBase
{

}
