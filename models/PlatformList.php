<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\BaseModel;
use app\models\Lists;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\dataproviders\ArrayDataProvider;
use app\models\ListMember;

use app\interfaces\models\ILists;
use app\interfaces\models\IUserDashboardConfig;

/**
 * This is the model class for table "platform.list".
 *
 * @property int $id
 * @property string $abbrev
 * @property string $name
 * @property string $display_name
 * @property string $type
 * @property int $entity_id
 * @property string $description
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $record_uuid
 */


class PlatformList extends BaseModel
{
    public $name;
    public $abbrev;
    public $display_name;
    public $type;
    public $entity_id;
    public $description;
    public $remarks;
    public $creation_timestamp;
    public $creationTimestamp;
    public $creator_id;
    public $modification_timestamp;
    public $modificationTimestamp;
    public $modifier_id;
    public $notes;
    public $is_void;
    public $record_uuid;
    public $items;
    public $creatorName;
    public $modifier;
    public $access;
    public $shared_settings;
    public $formNameParam = 'MyList';
    public $permission;
    public $displayName;
    public $creator;

    public $lists;

    public function __construct(
        $config,
        ILists $lists,
        public IUserDashboardConfig $userDashboardConfig) {

        $this->lists = $lists;

        parent::__construct($config);
    }

    /**
    * Get form name parameter for data browsers
    * @return string form name parameter
    **/

    public function formName()
    {
        return $this->formNameParam;
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['abbrev', 'name', 'display_name', 'type'], 'required'],
            [['id', 'entity_id', 'creator_id', 'modifier_id'], 'integer'],
            [['abbrev', 'name', 'display_name', 'type', 'description', 'remarks', 'notes', 'record_uuid', 'creatorName', 'creator', 'creation_timestamp', 'modification_timestamp', 'modifier',  'items', 'shared_settings', 'permission', 'displayName','creationTimestamp','modificationTimestamp'], 'safe'],
            [['is_void'], 'boolean'],
        ];
    }

    /**
    *   Action to do before validation of model
    *   @return boolean if validation is successful
    **/

    public function beforeValidate()
    {

        if (parent::beforeValidate()) {

            $currentTimestamp = new Expression('NOW()');
            $userModel = new User();
            $userId = $userModel->getUserId();

            if (!isset($userId) && empty($userId)) {
                $userId = 1;
            }

            if ($this->isNewRecord) {

                $this->creation_timestamp = $currentTimestamp;
                $this->creationTimestamp = $currentTimestamp;
                $this->creator_id = $userId;
                $this->is_void = false;

            } else {

                $this->modification_timestamp = $currentTimestamp;
                $this->modificationTimestamp = $currentTimestamp;
                $this->modifier_id = $userId;

            }

            return true;

        }

        return false;

    }

    /**
    *   Validate inputted abbrev for list
    *   @param String $value Specified abbrev of the list
    *   @return boolean if abbrev is valid
    */

    public function validateAbbrev($value) {

        // check if abbrev exists;

        $params = [
            "abbrev" => (string) $value
        ];
        $response = $this->lists->searchAll($params);

        if(!isset($response) || empty($response)){
            return false;
        }

        if (isset($response['totalCount']) && !empty($response['totalCount']) && $response['totalCount'] > 0) {
            return false;
        }

        return true;
    } 

    /**
    *   Validate inputted name for list
    *   @param String $value Specified name of the list
    *   @return boolean if name is valid
    */

    public static function validateName($value) {

        // check if name exists
        $params = [
            "name" => (string) $value
        ];
        $response = Lists::searchAll($params);

        if(!isset($response) || empty($response)){
            return false;
        }

        if (isset($response['totalCount']) && !empty($response['totalCount']) &&  $response['totalCount'] > 0) {
            return false;
        }

        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params contains different filter options
     *
     * @return ArrayDataProvider data source for list browsers
     */
    public function search($params, $filters='', $pagination=true)
    {
        $this->load($params);
        $param = [];
        // build filters

        if (!empty($this->type)) {
            $param['type'] = $this->type;
        }
        if (!empty($this->abbrev)) {
            $param['abbrev'] = $this->abbrev;
        }
        if (!empty($this->name)) {
            $param['name'] = $this->name;
        }
        if (!empty($this->displayName)) {
            $param['displayName'] = $this->displayName;
        }
        if (!empty($this->description)) {
            $param['description'] = $this->description;
        }
        if (!empty($this->shared_settings)) {
            $settings = $this->shared_settings;
            if ($settings == 'shared') {
                $param['shareCount'] = 'greater than 0';
            } else if ($settings == 'not_shared') {
                $param['shareCount'] = '0';
            }
        }

        if (!empty($this->creator)) {
            $param['creator'] = $this->creator;
        }

        if (!empty($this->permission)) {
            
            $userId = \Yii::$container->get('\app\models\User')->getUserId();
            if ($this->permission == 'owned') {
                $param['creatorDbId'] = (string) $userId;
            } else {
                $param['creatorDbId'] = 'not equals ' . $userId;
            }
        }

        if(!empty($this->modifier)){
            $param['modifier'] = $this->modifier;
        }

        if(!empty($this->modificationTimestamp)){
            $param['modificationTimestamp'] = $this->modificationTimestamp;
        }

        if(!empty($this->creationTimestamp)){
            $param['creationTimestamp'] = $this->creationTimestamp;
        }
        
        if ($filters == '') {
            $filters = 'ownershipType=owned&sort=creationTimestamp:desc';
        }

        $param['subType'] = 'not equals trait protocol|not equals management%|[OR] null';
        
        $data = \Yii::$container->get('\app\models\Lists');
        $result = $data->searchAll($param, $filters);
        
        $options = [
            'key' => 'listDbId',
            'allModels' => isset($result['data']) && !empty($result['data']) ? $result['data'] : [],
        ];
        if (!$pagination) {
            $options['pagination'] = false;
        }
        return new ArrayDataProvider($options);

    }

    /**
    * @return \yii\db\ActiveQuery
    */

    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    
    public function getCreatorName()
    {
       return $this->creator->display_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getModifier()
    {
        return $this->hasOne(User::className(), ['id' => 'modifier_id']);
    }

    /**
     *  Get the items in the list, including the voided ones
     *  @return PlatformListMember records in list
    **/

    public function getItems()
    {
        return $this->hasMany(PlatformListMember::className(), ['list_id' => 'id']);
    }

    /**
     *  Get the items in the list, excluding the voided ones
     *  @return PlatformListMember records in list
     */

    public function getItemsUnvoided() 
    {
        return $this->hasMany(PlatformListMember::className(), ['list_id' => 'id'])
                    -> andOnCondition(['is_void' => false]);
    }

    /**
     *  Get the list access items for the given list
     *  @return PlatformListAccess records for given list
     */

    public function getListAccess()
    {
        return $this->hasMany(PlatformListAccess::className(), ['list_id' => 'id']);
    }

    /**
     *  Get the number of times the list is shared
     *  @return integer number of the list is shared
     */

    public function getSharedCount()
    {
         return $this->hasMany(PlatformListAccess::className(), ['list_id' => 'id'])
                    -> andOnCondition(['is_void' => false])->count();
    }

    /**
     *  Get the permission type for the list
     *  @return String permission type for list (read, read_write)
     */

    public function getPermission()
    {
        return empty($this->listAccess->permission) ? '' : $this->listAccess->permission;
    }

    /**
     * Void a list given list ID via API resource
     * 
     * @param Integer $listDbId the ID of the list to be voided
     * 
     * @return Array contains if the call is successful or not
     */
    public function voidList($listDbId) {
        $data = Yii::$app->api->getResponse('DELETE', 'lists/'.$listDbId,json_encode(''));
        if ($data['status'] == 200) {
            
            return [
                'success' => true,
                'message' => '',
            ];
        } else {
            return [
                'success' => false,
                'message' => $data['body']['metadata']['status'][0]['message']
            ];
        }
    }

    /**
     * Search for list
     *
     * @param $data array filter for searching list
     * @return $result array data for the plan template
     */
    public static function searchList($data, $retrieveAll = false){
        $result = NULL;

        $responseApi = Yii::$app->api->getResponse('POST','lists-search',json_encode($data), '', $retrieveAll);
        
        if (isset($responseApi['status']) && $responseApi['status'] == 200 && isset($responseApi['body']['result']['data'])){
            $result = $responseApi['body']['result']['data'];
        }

        return $result;
    }
}