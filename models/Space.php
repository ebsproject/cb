<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\models\Application;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "platform.space".
 *
 * @property int $id Unique identifier of the record
 * @property string $abbrev Abbreviation of the record
 * @property string $name Name of the record
 * @property array $menu_data Menu items of the system
 * @property int $order_number Hierarchy of spaces
 * @property array $team_ids Team IDs who have access to the space
 * @property string $description More information about the record
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Universally unique identifier (UUID) of the record
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid
 */
class Space
{

    /**
     * Get menu items for user
     *
     * @param $program text current program of the user
     */
    public static function getMenuItems($program){

        // check first if there is space stored in session
        if(Yii::$app->session->get('userSpace') != null){
            return Yii::$app->session->get('userSpace');
        }

        // if a collaborator program, use COLLABORATOR space
        if (stripos($program, 'COLLABORATOR') !== false){
            $program = 'COLLABORATOR';
        }

        $space = Space::getSpaceOfUser($program);

        // retrieve navigation of the user
        $data = Yii::$app->api->getResponse('GET','spaces?abbrev='.$space);

        // check if has result and retrieve menu data of the user
        if(isset($data['body']['result']['data'][0]['menuData'])){
            $result = $data['body']['result']['data'][0]['menuData'];
        }

        $mainMenuArr = [];
        $leftMenuArr = [];

        if(isset($result) && !empty($result)){ //with record
            $valArr = $result;
            $leftMenuArr = (isset($valArr['left_menu_items'])) ? $valArr['left_menu_items'] : [];
            $mainMenuArr = (isset($valArr['main_menu_items'])) ? $valArr['main_menu_items'] : [];
        }

        $mainMenuItems = array_merge($mainMenuArr, Space::getDomainLinks());

        $spaceData = [
            'mainMenuItems' => $mainMenuItems,
            'leftMenuItems' => $leftMenuArr,
        ];

        // set space to user session
        Yii::$app->session->set('userSpace', $spaceData);

        return $spaceData;
    }

    /**
     * Retrieve domain links using CS GraphQL API
     */
    public static function getDomainLinks(){

        $query = '
            query {
                findDomainList(sort: [{col: "prefix"}]){
                    content{
                        name
                        prefix
                        domaininstances{
                            context
                            mfe
                        }
                    }
                }
            }
        ';
        $domains = [];
        $response = Yii::$app->api->getGraphQlResponse('POST', $query)['findDomainList']['content'] ?? [];

        foreach($response as $key => $value){
            $domains[$key]['label'] = $value['name'];
            foreach($value['domaininstances'] as $d){
                $prefix = $value['prefix'];
                $url = $d['context'];
                $url = substr($url , -1) !== '/' && !empty($url) ? $url.'/' : $url;
                $domains[$key]['url'] = $url.($d['mfe'] && !empty($url) ? $prefix : '');
                if($prefix  == 'cs' || $prefix  == 'ba') {
                    Yii::$app->session->set(strtoupper($prefix).'_URL', $domains[$key]['url']);
                }

            }
        }
        $parameter = getenv('CS_API_URL');
        $baseURI = substr($parameter , -1) !== '/' ? $parameter.'/graphql' : $parameter.'graphql';

        $domainLinks = [
            [
                'icon'=>'apps',
                    'label' => Yii::t('app', 'Domain shortcuts'),
                    'name' => 'domain-links',
                    'items' => [
                        [
                            'image' => Yii::getAlias('@web/images/ebs-text-logo.png')
                        ],
                        [
                            'label' => '<i class="material-icons orange-text">warning</i>'. Yii::t('app', 'Unable to retrieve the domain links'),
                            'url' => $baseURI
                        ],
                    ]
            ]
        ];

        if($domains) {
            $baseItems = [
                [
                    'image' => Yii::getAlias('@web/images/ebs-text-logo.png')
                ],
                [
                    'label' => Yii::t('app', 'Go to EBS Portal'). ' <span style="font-size:140%">➜</span>',
                    'url' => Yii::$app->session->get('CS_URL')
                ],
                [
                    'label' => Yii::t('app', 'Domain shortcuts'),
                    'name' => 'domain-shortcuts',
                    'class' => 'main-menu-item-header'
                ],
            ];
            $domainLinks[0]['items'] = array_merge($baseItems, $domains);
        }

        return $domainLinks;
    }

    /**
     * Checks whether the program has access to the search tool
     *
     * @param $program String currently selected program
     * @return boolean whether or not the program has access to the search tool
     */
    public static function showSearchTool($program) {
        

        $app = new Application;
        $searchApp = $app->getAppInfoByAbbrev('SEARCH');

        if(empty($searchApp)){
            Yii::$app->session->set('showSearchTool', false);
            return false;
        }
        else if(Yii::$app->session->get('isAdmin') !== null 
            && !Yii::$app->session->get('isAdmin') 
        ){
            $userModel = new User();
            $userId = $userModel->getUserId();

            $teamParams = [
                'teamCode' => (string) $program,
                'fields' => 'team.id AS teamDbId|team.team_code AS teamCode'
            ];

            $res = Yii::$app->api->getParsedResponse(
                'GET',
                'persons/'.$userId.'/teams',
                '',
                false
            );

            if(isset($res['data']) && !empty($res['data'])){
                $role = '';
                foreach ($res['data'] as $key => $value) {
                    if(isset($value['teamCode']) && ($value['teamCode'] == $program || $value['teamCode'] == $program . '_TEAM') ){

                        $role = isset($value['personRoleCode']) ? $value['personRoleCode'] : '';
                        break;
                    }
                }

                // if collaborator, do not show search tool
                if($role == 'COLLABORATOR'){
                    Yii::$app->session->set('showSearchTool', false);
                    return false;
                }
            }
        }

        Yii::$app->session->set('showSearchTool', true);
        return true;
    }

    /**
     * Get space of user
     *
     * @param $program text current program of the user
     * @return $space text space for the user
     */
    public static function getSpaceOfUser($program){
        $program = strtoupper($program);
        $userModel = new User();
        $userId = $userModel->getUserId();
        $space = 'DEFAULT';
        $isAdmin = false;

        // check if admin user
        $userData = Yii::$app->api->getResponse('GET','persons/'.$userId);

        if(isset($userData['body']['result']['data'][0]['personType'])){
            $isAdmin = $userData['body']['result']['data'][0]['personType'];
        }

        // if admin user, use ADMIN space
        if(isset($isAdmin) && strtolower($isAdmin) == 'admin'){
            return 'ADMIN';
        }

        // check if current program has corresponding space
        $spaceData = Yii::$app->api->getResponse('GET','spaces?abbrev='.$program);

        if(isset($spaceData['body']['metadata']['pagination']['totalCount']) 
            && !empty($spaceData['body']['metadata']['pagination']['totalCount'])){
            return $program;
        }
    
        return 'DEFAULT';
    }

}
