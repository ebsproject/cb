<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table ".scale".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the scale
 * @property string $name Name identifier of the scale
 * @property string $unit Unit of the scale to measure or record a property
 * @property string $type Type of scale; categorical: ordered list of options; continuous: numerical values; discrete: unordered set of options (needs confirmation)
 * @property string $level Level of scale; nominal: discrete values; ordinal: order of values is significant; interval: values in between are significant; ratio: continuous values (needs confirmation)
 * @property string $description Description about the scale
 * @property string $display_name Name of the scale to show to users
 * @property string $remarks Additional details about the scale
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $scale_value Temporary column to hold values of scale; refer to scale_value for more correct data
 * @property string $reference Reference of the scale
 * @property string $ontology_id Crop ontology ID
 * @property int $property_id ID of the property where the scale is used
 * @property int $method_id ID of the method where the scale is used
 * @property string $bibliographical_reference Reference details about the scale
 * @property string $scale_value_model
 * @property string $scale_default_value Default value in the scale
 * @property string $min_value Lowest possible value of the variable
 * @property string $max_value Highest possible value of the variable
 * @property string $min_critical_value Critical value approaching the lower bound of the variable scale
 * @property string $max_critical_value Critical value approaching the upper bound of the variable scale
 *
 * @property PropertyMethodScale[] $PropertyMethodScales
 * @property User $creator
 * @property User $modifier
 * @property ScaleValue[] $ScaleValues
 * @property Variable[] $Variables
 */
class Scale extends ScaleBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.scale';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit', 'type', 'level', 'description', 'remarks', 'notes', 'scale_value', 'reference', 'scale_value_model', 'scale_default_value'], 'string'],
            [['type'], 'required'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['creator_id', 'modifier_id', 'property_id', 'method_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id', 'property_id', 'method_id'], 'integer'],
            [['is_void'], 'boolean'],
            [['abbrev'], 'string', 'max' => 128],
            [['name', 'display_name', 'ontology_id', 'min_value', 'max_value', 'min_critical_value', 'max_critical_value'], 'string', 'max' => 256],
            [['bibliographical_reference'], 'string', 'max' => 255]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'unit' => 'Unit',
            'type' => 'Type',
            'level' => 'Level',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'scale_value' => 'Scale Value',
            'reference' => 'Reference',
            'ontology_id' => 'Ontology ID',
            'property_id' => 'Property ID',
            'method_id' => 'Method ID',
            'bibliographical_reference' => 'Bibliographical Reference',
            'scale_value_model' => 'Scale Value Model',
            'scale_default_value' => 'Scale Default Value',
            'min_value' => 'Min Value',
            'max_value' => 'Max Value',
            'min_critical_value' => 'Min Critical Value',
            'max_critical_value' => 'Max Critical Value',
        ];
    }

    /**
     * @return \yii\db\
     */
    public function getPropertyMethodScales()
    {
        return $this->hasMany(PropertyMethodScale::className(), ['scale_id' => 'id']);
    }

    /**
     * @return \yii\db\
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\
     */
    public function getModifier()
    {
        return $this->hasOne(User::className(), ['id' => 'modifier_id']);
    }

    /**
     * @return \yii\db\
     */
    public function getScaleValues()
    {
        return $this->hasMany(ScaleValue::className(), ['scale_id' => 'id']);
    }

    /**
     * @return \yii\db\
     */
    public function getVariables()
    {
        return $this->hasMany(Variable::className(), ['scale_id' => 'id']);
    }

     /**
     * Retrieves an existing variable scale in the database
     * @param $id integer variable Id
     * @return array variable scales' record in the database
     */
    public static function getVariableScale($id){
        $method = 'GET';
        $path = 'variables'.'/'.$id.'/scales';
        
        $data = Yii::$app->api->getResponse($method, $path);
        $result = [];

        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0]['scales'][0])){
          $result = $data['body']['result']['data'][0]['scales'][0];
        }
        return $result;
      }
}
