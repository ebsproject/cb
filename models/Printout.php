<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;
use Yii;

class Printout{

    const API_RESOURCE = 'csps';
    /**
     * Retrieve reports/printouts
     * CS call: GET /api/Template/product/{productId}/program/{programId}
     * 
     * @param Integer $productId product identifier
     * @param Integer $programId optional program identifier
     * 
     * @return Array list of available templates
     */
    public function getAllTemplates($productId, $programId = 0){

        $method = 'GET';
        $path = "api/template/product/$productId";
        if($programId > 0){
            $path = $path."/program/$programId";
        }

        //TODO: set the retrieveAll param to 'true' when page size params are supported in CS call
        $response = Yii::$app->api->getResponse($method, $path, null, '', false, self::API_RESOURCE);
        
        if ($response['status'] === 500) return [ 'status' => 500 ];

        return isset($response['body']['result']['data']) ? $response['body']['result']['data'] : [];
        
    }

    /**
     * Retrieve products
     * CS call: GET /api/template/products
     * 
     * @param String optional $productName product name
     * 
     * @return Array list of available products
     */
    public function getAllProducts($productName=''){

        $method = 'GET';
        $path = "api/template/products";

        $response = Yii::$app->api->getResponse($method, $path, null, '', true, self::API_RESOURCE);

        if ($response['status'] === 500) return [ 'status' => 500 ];

        $results = isset($response['body']['result']['data'][0]['id']) ? $response['body']['result']['data'] : [];

        if(!empty($productName)){
            foreach($results as $result){
                // Sanitize first the product names to be compared against
                $name = strtolower(trim($result['name']));
                $productName = strtolower(trim($productName));

                if (str_contains($name, $productName)) {
                    return [$result];
                }
            }
        }

        
        return $results;
    }

    /**
     * Retrieve programs
     * CS call: GET /api/template/programs
     * 
     * @param String optional $programName program name
     * 
     * @return Array list of available programs
     */
    public function getAllPrograms($programName=''){

        $method = 'GET';
        $path = "api/template/programs";

        $response = Yii::$app->api->getResponse($method, $path, null, '', true, self::API_RESOURCE);

        if ($response['status'] === 500) return [ 'status' => 500 ];

        $results = isset($response['body']['result']['data'][0]['id']) ? $response['body']['result']['data'] : [];
        
        if(!empty($programName)){
            foreach($results as $result){
                // Sanitize first the program names to be compared against
                $name = strtolower(trim($result['name']));
                $programName = strtolower(trim($programName));

                if ($name == $programName) {
                    return [$result];
                }
            }
        }
        
        return $results;
    }

    /**
     * Retrieve file formats
     * CS call: GET /api/report/getformats
     * 
     * @return Array list of available file formats
     */
    public function getAllFormats () {
        $method = 'GET';
        $path = "api/report/getformats";
        $results = [ 'pdf' => 'PDF', ];

        $response = Yii::$app->api->getResponse($method, $path, null, '', true, self::API_RESOURCE);

        if ($response['status'] === 500) return [ 'status' => 500 ];

        $results = isset($response['body']) ? $response['body'] : [];

        // Repack file types into an associative array
        foreach ($results as $key => $value) {
            // Hide PNG option until CS team resolves error on generating
            // printout reports in PNG format
            if (is_int($key) && $value != 'png') {
                $results[$value] = strtoupper($value);
            }

            unset($results[$key]);
        }

        return $results;
    }

    /**
     * Retrieve parameters given the template name
     * CS call: GET /api/report/parameters
     * 
     * @return String name of the template
     */
    public function getParameters ($templateName) {
        $method = 'GET';
        $path = "api/report/parameters";
        $urlParams = "nameReport=$templateName";
        $results = [ ];

        $response = Yii::$app->api->getResponse($method, $path . "?" . $urlParams, null, '', true, self::API_RESOURCE);

        if ($response['status'] === 500) return [ 'status' => 500 ];

        $results = isset($response['body']) ? (array) $response['body'] : [];
        unset($results['result']);

        $parameters = [];
        foreach ($results as $result) {
            if(!empty($result['name'])) $parameters[] = $result['name'];
        }

        return $parameters;
    }

    /**
     * Export report/printouts
     * CS call: GET /api/template/export
     * 
     * @param String $template template name,
     * @param String $format file type pdf/xls/csv
     * @param Integer $occurrenceDbId occurrence identifier
     * @param String $fileName occurrence identifier
     */
    public function exportReport($template, $format, $occurrenceDbId, $fileName){
        Yii::debug('Exporting report: ' . json_encode(compact(
            'template',
            'format',
            'occurrenceDbId',
            'fileName'
        )), __METHOD__);

        // get dynamic parameters
        $params = $this->getParameters($template);
        $parameterName = '';

        // get ID parameter for occurrence ID while waiting for CS printouts parameter alias
        foreach($params as $param){
            // if ID is found in parameter, use as parameter name
            if(stripos($param, 'id') !== false){
                $parameterName = $param;
                break;
            }
        }

        $method = 'GET';
        $path = "api/report/export?name=$template&format=$format&parameter[$parameterName]=$occurrenceDbId";

        return Yii::$app->api->uploadFile($method, $path, $format, $fileName, self::API_RESOURCE);

    }

    /**
     * Export report/printouts
     * CS call: GET /api/template/export
     * 
     * @param String $template template name,
     * @param String $format file type pdf/xls/csv
     * @param Array $parameters array of parameters, 
     *                  where the key is the variable name,
     *                  and the value is the parameter value.
     * @param String $fileName occurrence identifier
     */
    public function generateReport($template, $format, $parameters, $fileName){

        // Assemble parameter string
        $paramString = "";
        foreach($parameters as $variableName => $paramValue) {
            $paramString .= "&parameter[$variableName]=$paramValue";
        }

        $method = 'GET';
        $path = "api/report/export?name=$template&format=$format" . $paramString;

        return Yii::$app->api->uploadFile($method, $path, $format, $fileName, self::API_RESOURCE);
    }
}
