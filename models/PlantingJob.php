<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\dataproviders\ArrayDataProvider;
use app\models\UserDashboardConfig;
use app\models\Api;

use yii\helpers\ArrayHelper;

use Yii;

/**
 * Base class for experiment.planting_job table that uses the BaseModel class
 */
class PlantingJob extends BaseModel {

    public function __construct(
        protected Api $api,
        $config = []
    ) {
        parent::__construct(
            $api,
            $config
        );
    }

    public static function apiEndPoint() {
        return 'planting-jobs';
    }

    private $data;

    public function __get($varName){
        
        if(isset($this->data[$varName])){
            return $this->data[$varName];
        }
        
    }

    public function __set($varName, $value){
        $this->data[$varName] = $value;
    }

    /**
     * Fields to be validated and included in search filters
     */
    public function rules ()
    {
        return [
            [[
                'occurrenceCount',
                'envelopeCount',
                'creatorDbId',
                'modifierDbId',
            ], 'integer'],
            [[
                'plantingJobCode',
                'experiments',
                'facilityName',
                'creator',
                'experiments'
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
                'plantingJobDueDate',
                'plantingJobStatus',
                'plantingJobType',
            ], 'safe'],
        ];
    }

    /**
     * Search planting jobs
     *
     * @param array $params filter parameters
     * @return array $dataProvider planting job records
     */
    public function search($params){
        $filters = [
            'plantingJobType' => 'packing job'
        ];
        $data = [];
        
        $dataProviderId = 'dynagrid-pim-planting-job-transactions';
        $currPage = 1;
        $paramPage = '';
        $paramSort = '';

        extract($params);
        $columnFilters = isset($PlantingJob) ? \Yii::$container->get('app\modules\experimentCreation\models\BrowserModel')->formatFilters($PlantingJob) : [];

        $filters = array_merge($filters, $columnFilters);

        array_walk($filters, function (&$value, $key) {
            if (
                $key != 'fields' &&
                $value != '' &&
                !str_contains(strtolower($value), 'equals') &&
                !str_contains(strtolower($value), '%')
            )
                $value = "equals $value";
        });

        // filter using experiment name
        if(isset($filters['experiments'])){
            $filters['experiments'] = '%'.$filters['experiments'].'%';
        }

        // get current page
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = (int) $_GET["$dataProviderId-page"];
            $paramPage = "&page=$currPage";
        }

        // get current sort
        $paramSort = 'sort=plantingJobDbId:desc';
        $sortParams = '';
        if(isset($_GET['dynagrid-pim-planting-job-transactions-sort'])) {
            $sortParams = $_GET['dynagrid-pim-planting-job-transactions-sort'];
        } else if (isset($_GET['dp-1-sort'])) {
            $sortParams = $_GET['dp-1-sort'];
        }

        // for sorting
        if (!empty($sortParams)) {
            if($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort='. $column . $order;
        }

        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $pagination = $paramLimit.$paramPage;

        // check if admin, filter by current program, else add collaborator program parameter
        // to retrieve only planting job user has access to
        $isAdmin = Yii::$app->session->get('isAdmin');
        $program = Yii::$app->userprogram->get('abbrev');
        $collaboratorProgramCond = '';

        if($isAdmin){
            $filters['programCode'] = "equals $program";
        } else {
            $collaboratorProgramCond = "&collaboratorProgramCode=$program";
        }

        // check if page is out of range
        $totalPagesArr = Yii::$app->api->getResponse('POST',
            'planting-jobs-search?'.$paramLimit.$collaboratorProgramCond,
            json_encode($filters)
        );
        
        //set default totalPages value, in case totalPages value is empty
        $totalPages = 0;
        
        if(isset($totalPagesArr['body']['metadata']['pagination']['totalPages'])){
            $totalPages = $totalPagesArr['body']['metadata']['pagination']['totalPages'];
        }

        // if out of range
        $currPage = intval($currPage);
        if($currPage > $totalPages){
            $pagination = $paramLimit.'&page=1';
        }

        // add '&' if pagination is set
        if(!empty($pagination)){
            $paramSort = '&'. $paramSort;
        }

        $output = Yii::$app->api->getParsedResponse('POST',
            'planting-jobs-search?'.$pagination.$paramSort.$collaboratorProgramCond,
            json_encode($filters)
        );

        $exportOutput = Yii::$app->api->getParsedResponse('POST',
            'planting-jobs-search?'.$paramSort.$collaboratorProgramCond,
            json_encode($filters),
            '',
            true
        );

        if ($output['success']) {
            $data = array_merge($data, [$output['data']]);
        } else {
            $data = array_merge($data, []);
        }

        if ($exportOutput['success']) {
            $data = array_merge($data, [$exportOutput['data']]);
        } else {
            $data = array_merge($data, []);
        }

        $attributes = [
            'plantingJobDbId', 'plantingJobCode', 'plantingJobStatus', 'plantingJobType', 
            'plantingJobDueDate', 'plantingJobInstructions','facilityName',
            'envelopeCount', 'occurrenceCount', 'creator', 'creationTimestamp'
        ];

        //check totalCount value. If empty set value to 0.
        $totalCount = empty($output['totalCount']) || !isset($output['totalCount']) ? 0 : $output['totalCount'];
        $dataModelsValue = empty($data) || !isset($data) ? [] : $data[0];

        $dataProvider = new ArrayDataProvider([
            'id' => 'dynagrid-pim-planting-job-transactions',
            'allModels' => $dataModelsValue,
            'key' => 'plantingJobDbId',
            'restified' => true,
            'totalCount' => $totalCount,
            'sort' => [
                'attributes' => $attributes
            ],
        ]);

        //check exportTotalCount value. If empty, set value to 0
        $exportTotalCount = empty($exportOutput['totalCount']) || !isset($exportOutput['totalCount']) ? 0 : $exportOutput['totalCount'];
        $exportModelsValue = empty($data) || !isset($data) ? [] : $data[1];

        $exportDataProvider = new ArrayDataProvider([
            'allModels' => $exportModelsValue,
            'key' => 'plantingJobDbId',
            'restified' => true,
            'totalCount' => $exportTotalCount,
            'sort' => [
                'attributes' => $attributes
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            //validate results
            return [
                $dataProvider,
                $exportDataProvider
            ];
        }

        return [
            $dataProvider,
            $exportDataProvider
        ];
    }

    /**
     * Return summary of Packing Job
     *
     * @param Integer $id Packing Job identifier
     * @return Array $summaryData Summary information
     */
    public function summary($id){

        $summaryData = Yii::$app->api->getParsedResponse('GET',
            'planting-jobs/'.$id.'/summaries'
        );

        if(isset($summaryData['data'][0]['summaries'])){
            return $summaryData['data'][0]['summaries'];
        }

        return null;
    }

    /**
     * Get class of status value
     *
     * @param Text $status Status of Planting job
     * @return Text $class Class of status
     */
    public function getStatusClass($status){
        $class = 'grey';
        if($status == 'packed') {
            $class = 'blue darken-3';
        }
        else if ($status == 'ready for packing') {
            $class = 'green darken-3';
        } else if (
            $status == 'packing' ||
            $status == 'updating entries in progress' ||
            $status == 'updating packages in progress'
        ) {
            $class = 'orange darken-2';
        } else if($status == 'packing cancelled' || $status == 'packing job failed') {
            $class = 'red';
        }

        return $class;
    }

    /**
     * Retrieve planting job tags
     *
     * @param Text $attribute Attribute to be retrieved
     * @return Aray $tags List of attribute values
     */
    public function getPlantingJobTags($attribute){
        $params = [
            'fields'=>'planting_job.'.$attribute .' as "'.$attribute.'"',    
            'distinctOn' => $attribute
        ];

        $data = $this->searchAll($params);

        return ArrayHelper::map($data['data'], $attribute, $attribute);


    }


    /**
     * Format string index to camelCase
     * 
     * @param $index string attribute index
     */
    public function formatToCamelCase($index){
        return preg_replace_callback('/_([a-z]?)/', function($match) {return strtoupper($match[1]);}, strtolower($index));
    }

    /**
     * Get processed summary for view
     *
     * @param Array $summary Planting job summary
     * @return Array $processedSummary Processed summary
     */
    public function getProcessedSummary($summary){
        $processedSummary = [];

        if(isset($summary['totalExperimentCount'])){
            $processedSummary[] = [
                'label' => 'Total number of Experiments',
                'value' => $summary['totalExperimentCount']
            ];
        }

        if(isset($summary['totalOccurrenceCount'])){
            $processedSummary[] = [
                'label' => 'Total number of Occurrences',
                'value' => $summary['totalOccurrenceCount']
            ];
        }

        if(isset($summary['totalEnvelopesCount'])){
            $processedSummary[] = [
                'label' => 'Total number of Envelopes in Packing job',
                'value' => $summary['totalEnvelopesCount']
            ];
        }

        if(isset($summary['totalReplacementCount'])){
            $processedSummary[] = [
                'label' => 'Total number of entry replacements',
                'value' =>  (empty($summary['totalReplacementCount'])) ? "0" : $summary['totalReplacementCount']
            ];
        }

        if(isset($summary['occurrenceCount'])){
            $processedSummary[] = [
                'label' => 'Number of occurrences',
                'value' => $summary['occurrenceCount']
            ];
        }

        if(isset($summary['entryCount'])){
            $processedSummary[] = [
                'label' => 'Number of entries',
                'value' => $summary['entryCount']
            ];
        }

        if(isset($summary['replacementCount'])){
            $processedSummary[] = [
                'label' => 'Number of replacements',
                'value' => $summary['replacementCount']
            ];
        }

        if(isset($summary['envelopeCount'])){
            $processedSummary[] = [
                'label' => 'Number of envelopes',
                'value' => $summary['envelopeCount']
            ];
        }

        if(isset($summary['experimentDbId'])){
            $processedSummary[] = [
                'label' => 'experimentDbId',
                'value' => $summary['experimentDbId']
            ];
        }

        return $processedSummary;
    }

    /**
     * Get processed additional data for view
     *
     * @param Array $plantingJobData Planting job additional data
     * @return Array $addtlInstructions Additional instructions
     */
    public function getProcessedAdditionalData($plantingJobData){
        $addtlInstructions[] = [
            'label' => 'Packing Job Due Date',
            'value' => isset($plantingJobData['plantingJobDueDate']) ? date("M d, Y", strtotime($plantingJobData['plantingJobDueDate'])) : ''
        ];

        $addtlInstructions[] = [
            'label' => 'Packing Job Facility',
            'value' => $plantingJobData['facilityName']
        ];

        $addtlInstructions[] = [
            'label' => 'Packing Job Instructions',
            'value' => $plantingJobData['plantingJobInstructions']
        ];

        return $addtlInstructions;
    }

    /**
     * Get processed widget options
     *
     * @param Array $data Config data
     * @return Array Data endpoing and widget options
     */
    public function getConfigForm($data){
        $variableAbbrev = isset($data['selected']) ? strtoupper($data['selected']) : '';
        $formType = isset($data['formType']) ? strtoupper($data['formType']) : '';
        $configVars = json_decode($data['configVars'], true);
        
        foreach($configVars as $value){
            if($value['variable_abbrev'] == $variableAbbrev && !(isset($value['is_hidden']) && $value['is_hidden'])){
                $config[] = $value;
            }
        }

        if(isset($config[0]['api_resource_endpoint'])){
            $dataEndpoint = $config[0]['api_resource_endpoint'];
        }

        $widgetOptions = [
            'config' => $config,
            'formType' => $formType
        ];

        return [
            'dataEndpoint' => $dataEndpoint,
            'widgetOptions' => $widgetOptions
        ];
    }

    /**
     * GET planting job's occurrence summaries and
     * return in a DataProvider format
     *
     * @param array $params parameters
     * 
     * @return ArrayDataProvider $dataProvider occurrence summary records
     */
    public function getOccurrenceSummaries ($params)
    {
        extract($params);

        Yii::debug("GETting planting job's occurrence summaries " . json_encode(['params' => $params]));

        $dataProviderId = 'planting-job-occurrence-summaries-browser';
        $sortParams = '';
        $paramSort = '';

        // get current sort
        if (
            isset($_GET["$dataProviderId-sort"]) &&
            !empty($_GET["$dataProviderId-sort"])
        ) {
            $sortParams = $_GET["$dataProviderId-sort"];

            if ($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort=' . $column . $order;
        }

        $url = "planting-jobs/$id/occurrence-summaries?$paramSort";


        $output = $this->api->getApiResults(
            'GET',
            $url,
            null,
            '',
            true
        );

        return new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'occurrenceDbId',
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'experimentDbId' => SORT_ASC,
                    'occurrenceDbId' => SORT_ASC,
                ],
                'attributes' => [
                    'experimentDbId',
                    'experimentName',
                    'occurrenceDbId',
                    'occurrenceName',
                    'entryCount',
                    'replacementCount',
                    'envelopeCount',
                ]
            ],
        ]);
    }
}