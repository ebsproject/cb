<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
// Import interfaces
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IWorker;

/**
 * This is the class for background workers
 */
class Worker implements IWorker
{
    // Models with interface
    public $backgroundJob;

    public function __construct(IBackgroundJob $backgroundJob)
    {
        $this->backgroundJob = $backgroundJob;
    }
    
    /**
     * Creates a background job record and establishes a connection to the specified worker
     * @param string $workerName - name of the worker to connect to
     * @param string $description - description of the worker
     * @param string $entity - reference to the entity where the background job is used
     * @param string $entityDbId - identifier of the entity
     * @param string $endpointEntity - reference to the entity where the background job is used
     * @param string $application - reference to the application where the background job is used
     * @param string $method - method used in the background job
     * @param array $workerData - data to pass to the worker
     * @param array $additionalParams - (optional) additional data for the background job record insertion
     * @param array $additionalWorkerParams - (optional) additional worker data
     */
    public function invoke($workerName, $description, $entity, $entityDbId, $endpointEntity, $application, $method, $workerData, $additionalParams = null, $additionalWorkerParams = null) {

        // Check if additional params is null. If null, set to empty array.
        if(!isset($additionalParams)) {
            $additionalParams = [];
        }

        // Get user ID
        $userId = Yii::$app->session->get('user.id');

        // Assemble request data for creating background job
        $records = [
            "workerName" => $workerName,
            "description" => $description,
            "message" => "[IN QUEUE] $description",
            "entity" => $entity,
            "entityDbId" => $entityDbId,
            "endpointEntity" => $endpointEntity,
            "application" => $application,
            "method" => $method,
            "jobStatus" => "IN_QUEUE",
            "startTime" => "NOW()",
            "creatorDbId" => $userId
        ];
        $records = array_merge($records, $additionalParams);
        $requestData = [
            "records" => [$records]
        ];

        // Create background job
        $result = $this->backgroundJob->create($requestData);

        // If background job creation was not successful, return error
        if(isset($result['status']) && $result['status'] != 200) {
            return [
                "success" => $result["success"],
                "status" => $result["status"],
                "message" => "Background job creation was unsuccessful: " . $result['message'],
                "backgroundJobId" => null
            ];
        }

        // Get background job id
        $backgroundJobId = isset($result["data"][0]["backgroundJobDbId"]) ? $result["data"][0]["backgroundJobDbId"] : 0;

        // Establish the connection
        try {
            // Add user Id to worker data
            $workerData['userId'] = $userId;
            // Add background job Id to worker data
            $workerData['backgroundJobId'] = $backgroundJobId;
            // Add token object to worker data
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            $workerData["tokenObject"] = [
                "token" => $token,
                "refreshToken" => $refreshToken
            ];

            // If has additional worker data parameters
            if(!is_null($additionalWorkerParams)){
                $workerData["requestData"] = $additionalWorkerParams;
            }

            // Get connection parameters
            $host = getenv('CB_RABBITMQ_HOST');
            $port = getenv('CB_RABBITMQ_PORT');
            $user = getenv('CB_RABBITMQ_USER');
            $password = getenv('CB_RABBITMQ_PASSWORD');
            // Create connection
            $connection = new AMQPStreamConnection($host, $port, $user, $password);
            // Get connection's channel
            $channel = $connection->channel();

            // Declare channel queue
            $channel->queue_declare(
                $workerName, //queue - Queue names may be up to 255 bytes of UTF-8 characters
                false,  //passive - can use this to check whether an exchange exists without modifying the server state
                true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
                false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
                false   //auto delete - queue is deleted when last consumer unsubscribes
            );

            // Create new AMQP Message object; specify json-encoded worker data as first parameter
            $msg = new AMQPMessage(
                json_encode($workerData),
                array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
            );
            // Publish the channel
            $channel->basic_publish(
                $msg,   //message 
                '', //exchange
                $workerName  //routing key (queue)
            );
            // Close the channel and connection
            $channel->close();
            $connection->close();

        }
        catch (\Exception $e) {
            Yii::error($e, __METHOD__);
            // If an exception was encountered while establishing the connection,
            // return an error, with the message of the caught exception
            return [
                "success" => false,
                "status" => null,
                "message" => "Connection failed: " . $e->getMessage(),
                "backgroundJobId" => $backgroundJobId
            ];
        }

        // Return success message and background job id
        return [
            "success" => $result["success"],
            "status" => $result["status"],
            "message" => "Connection to $workerName was successfully established.",
            "backgroundJobId" => $backgroundJobId
        ];
    }
}
