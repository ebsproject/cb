<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\ListMember;
use app\interfaces\models\ILists;
use app\interfaces\models\IListMember;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use app\models\BackgroundJob;
use app\models\TerminalTransaction;
use app\models\TerminalBackgroundProcess;

/**
 * Base class for platform.list table that uses the BaseModel class
 */
class Lists extends BaseModel implements ILists{

    public $name;
    public $abbrev;
    public $display_name;
    public $type;
    public $description;
    public $remarks;

    public $listMember;

    public function __construct(
        IListMember $listMember,
        $config=[]) {

        $this->listMember = $listMember;

        parent::__construct($config);
    }

    public static function apiEndPoint() {
        return 'lists';
    }

    /**
     * Save new members to a given list
     *
     * @param $listId integer list identifier
     * @param $members array list of members with id and displayValue information
     * @param $backgroundProcess boolean if true, creation will be sent to background process
     * @param $additionalParams array request body for processor call
     * @param $membersIdsOnly boolean indicates if the $members parameter only contains IDs
     * @param $parseMembers boolean indicates if the $members parameter still needs to be parsed or is already in the right format
     *
     * @return $response array response from API call
     */
    public function createMembers($listId, $members = [], $backgroundProcess = false, $additionalParams = [], $memberIdsOnly=false, $parseMembers=true) {
        $records = [];

        if ($parseMembers) {
            foreach ($members as $mem) {
                if ($memberIdsOnly) {
                    $temp = [
                        'id' => $mem
                    ];
                } else {
                    $temp = [
                        'id' => $mem['id']
                    ];

                    if (isset($mem['displayValue'])) {
                        $temp['displayValue'] = $mem['displayValue'];
                    }

                    if (isset($mem['remarks'])) {
                        $temp['remarks'] = $mem['remarks'];
                    }
                }

                $records[] = $temp;
            }
        } else {
            $records = $members;
        }

        $params = [
            'records' => $records
        ];

        if ($backgroundProcess) {
            $apiEndPoint = static::apiEndPoint();
            $method = 'POST';
            $path = 'processor';
            
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');

            $params = [
                'httpMethod' => 'POST',
                'endpoint' => 'v3/lists/' . $listId . '/members',
                'entity' => 'LIST_MEMBER',
                'endpointEntity' => 'LIST_MEMBER',
                'application' => 'LISTS',
                'requestData' => $params,
                'tokenObject' => [
                    "token" => $token ?? '',
                    "refreshToken" => $refreshToken ?? ''
                ]
            ];

            $params = array_merge($params, $additionalParams);
            $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($params));
        } else {
            $apiEndPoint = static::apiEndPoint();
            $method = 'POST';
            $path = $apiEndPoint . '/' . $listId . '/members';
            $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($params));
        }

        if(!isset($response) || empty($response)){
            $response = [];
        }

        return $response;
    }

    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundProcesses){
        $transactions = [];

        foreach ($backgroundProcesses as $record) {
            $status = strtolower($record['jobStatus']);

            if($status == 'done'){
                $iconClass = 'green';
                $icon = 'check';
            }else if(in_array($status, ['in_progress, in_queue'])){
                $iconClass = 'grey';
                $icon = 'hourglass_empty';
            }else if($status == 'failed'){
                $iconClass = 'red';
                $icon = 'priority_high';
            }else{
                $iconClass = 'grey';
                $icon  = 'near_me';
            }

            $entityId = $record['entityDbId'];
            $description = str_replace('.','',$record['description']);
            $message = $description . ' ' . $status . '!';
            $timeDiff = \Yii::$container->get('\app\models\BackgroundJob')->ago($record['modificationTimestamp']);

            $params = [
                'fields' => 'list.display_name as displayName | list.id as listDbId | list.abbrev as abbrev',
                'listDbId' => (string) $entityId
            ];

            $result = Lists::searchAll($params, 'limit=1', false);

            $entityName = isset($result['data'][0]['displayName']) ? $result['data'][0]['displayName'] : '';
            $abbrev = isset($result['data'][0]['abbrev']) ? $result['data'][0]['abbrev'] : '';
            $workerName = strtolower($record['workerName'] ?? '');
            $jobRemarks = $record['jobRemarks'];
            $item = '<li class="bg_notif-btn" data-transaction_id="'.$entityId.'" data-abbrev="'.$abbrev.'" data-worker-name="'.$workerName.'" data-filename="'.$jobRemarks.'">
                <a class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle small '.$iconClass.'">'.$icon.'</span>
                    '.$message.'
                </a>';

            $timeElement = '<time class="media-meta">';
            $item .= $timeElement.' <b>'.$entityName.'</b></time>'.$timeElement.$timeDiff.'</time></li>';
            $transactions[] = $item;
        }

        return implode(' ', $transactions);
    }

    /**
     * Get push notifs of User
     * @param userId - user identifier
     */
    public function getPushNotifications($userId) {

        // get new notifications
        $params = [
            'creatorDbId' => 'equals ' . $userId,
            'application' => 'LISTS',
            'distinctOn' => 'entityDbId',
            'jobStatus' => 'equals DONE|equals FAILED|equals IN_PROGRESS',
            'isSeen' => 'false'
        ];

        $filter = 'limit=5&sort=modificationTimestamp:desc';
        $result = \Yii::$container->get('\app\models\BackgroundJob')->searchAll($params, $filter, false);

        $newNotifications = isset($result['data']) ? $result['data'] : [];
        $unseenCount = count($newNotifications);
        $unseenNotifications = Lists::getBackgroundJobNotifications($newNotifications);

        // get old notifications
        $oldNotifParams = [
            'creatorDbId' => 'equals ' . $userId,
            'application' => 'LISTS',
            'distinctOn' => 'entityDbId',
            'jobStatus' => 'equals DONE|equals FAILED',
            'isSeen' => 'true'
        ];
        $oldNotifResult = \Yii::$container->get('\app\models\BackgroundJob')->searchAll($oldNotifParams, $filter, false);
        $oldNotifications = isset($oldNotifResult['data']) ? $oldNotifResult['data'] : [];
        $seenCount = count($oldNotifications);
        $seenNotifications = Lists::getBackgroundJobNotifications($oldNotifications);

        // update DONE / FAILED notifications to seen
        if(isset($newNotifications) && !empty($newNotifications) && isset($newNotifications[0]) && in_array($newNotifications[0]['jobStatus'],['FAILED','DONE'])){
            $updateParams = [
                'jobIsSeen' => 'true'
            ];
            \Yii::$container->get('\app\models\BackgroundJob')->update($newNotifications, $updateParams);
        }

        return [
            "unseenNotifications" => $unseenNotifications,
            "seenNotifications" => $seenNotifications,
            "unseenCount" => $unseenCount,
            "seenCount" => $seenCount
        ];
    }

    /**
     * Get new notifications count for user
     * @param userId - user identifier
     */
    public function getNewNotifsCount($userId) {
        $params = [
            "creatorDbId" => "" . $userId,
            "isSeen" => "false",
            'jobStatus' => 'equals DONE|equals FAILED|equals IN_PROGRESS',
            'application' => 'equals LISTS'
        ];

        $result = \Yii::$container->get('\app\models\BackgroundJob')->searchAll($params);
        if (isset($result["status"]) && !empty($result["status"]) && $result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }
        $totalCount = isset($result) && !empty($result) && isset($result["totalCount"]) ? $result["totalCount"] : 0;

        return json_encode($totalCount);
    }

    /**
     * Update seen state of background job
     * 
     * @param Integer user id
     */
    public function updateBgJobSeenState($userId) {
        $params = [
            "creatorDbId" => "" . $userId,
            "isSeen" => "false",
            'jobStatus' => 'equals DONE|equals FAILED|equals IN_PROGRESS',
            'application' => 'equals LISTS'
        ];

        $result = \Yii::$container->get('\app\models\BackgroundJob')->searchAll($params);

        if (isset($result["status"]) && !empty($result["status"]) && $result["status"] != 200) {
            return json_encode([
                "success" => false,
                "error" => "Error while retrieving background processes. Kindly contact Administrator."
            ]);
        }
        $backgroundJobs = $result['data'];
        if(isset($backgroundJobs) && count($backgroundJobs) > 0){
            $params = [
                "jobIsSeen" => 'true'
            ];

            $res = \Yii::$container->get('\app\models\BackgroundJob')->update($backgroundJobs, $params);
        }
        return true;
    }

    /**
     * Method for retrieving all the list member records from a given endpoint using the POST search method
     * 
     * @param Integer $dbId list record identifier
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllMembers($dbId, $params=null, $filters='', $retrieveAll=true){

        $apiEndPoint = static::apiEndPoint();
        $apiSearchEndpoint = 'members-search';
        $method = 'POST';
        $path = $apiEndPoint . '/' . $dbId. '/' .$apiSearchEndpoint;

        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, $retrieveAll);
    }
}