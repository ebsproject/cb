<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\models\Season;
use app\models\User;
use yii\data\ArrayDataProvider;

/**
 * This is the ActiveQuery class for [[SeasonSearch]].
 *
 * @see SeasonSearch
 */
class SeasonSearch extends Season
{
    public $creator;
    public $modifier;
    public $creationTimestamp;
    public $modificationTimestamp;

    /**
     * Search model for season
     * 
     * @param @mixed search filters
     * 
     * @return mixed data provider containing seasons
     */
    public function search ($params)
    {
        $this->load($params);
        $seasonSearchFilters = $params['SeasonSearch'];

        // Convert '1' to 'true or '0' to 'false' for isActive
        foreach ($seasonSearchFilters as $key => $value)
            if ($key == 'isActive')
                $seasonSearchFilters[$key] = ($value == '1') ?
                    'true' : 'false';

        return new ArrayDataProvider([
            'allModels' => $this->searchSeasons($seasonSearchFilters),
            'key' => 'seasonDbId',
            'sort' => [
                'attributes' => [
                    'seasonDbId',
                    'seasonCode',
                    'seasonName',
                    'description',
                    'isActive',
                    'creator',
                    'modifier',
                    'creationTimestamp',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'seasonDbId' => SORT_DESC
                ]
            ],
            'id' => 'season-data-provider'
        ]);

    }
}
