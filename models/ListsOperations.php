<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * Class for specifying different lists operations
  */
namespace app\models;

use Yii;
use app\interfaces\models\ILists;
use app\interfaces\models\IListMember;

class ListsOperations{

    public $lists;
    public $listMember;

    public function __construct(
        ILists $lists,
        IListMember $listMember
    ) {
        $this->lists = $lists;
        $this->listMember = $listMember;

    }

    /**
     * Method for merging 2 or more lists with the same type
     * 
     * @param Array $listDbIdArr array containing the list Ids
     * @param Integer $newListDbId the ID of the list that the other lists will merged to
     * @param String $mode if merging should just copy the members or move entirely to new list
     * @param Boolean $removeDuplicates
     * @return Array specifies if the operation is successful
     */
    public function mergeList($listDbIdArr, $newListDbId, $mode='copy', $removeDuplicates=false) {

        if (!empty($listDbIdArr)) {
            $listDbIdStr = implode("|equals ", $listDbIdArr);
            // check list types
            $response = $this->lists->searchAll(
                [
                    "listDbId" => "equals ".$listDbIdStr,
                    "fields" => "list.id as listDbId|list.abbrev|list.type as type"
                ]
            );

            if (!isset($response) || empty($response)){
                return [
                    "success" => false,
                    "message" => "Error encountered while retrieving list information. Kindly try again."
                ];
            }

            if (isset($response['success']) && !empty($response['success']) && isset($response['totalCount']) && !empty($response['totalCount']) && $response['success'] && $response['totalCount'] > 0) {
                $lists = $response['data'];
                $currentType = '';
                $errorType = false;
                foreach ($lists as $list) {
                    if ($currentType == '') {
                        $currentType = $list['type'];
                    } else {
                        if ($currentType != $list['type']) {
                            $errorType = true;
                            break;
                        }
                    }
                }

                if ($errorType) {
                    return [
                        "success" => false,
                        "message" => "The lists you provided have different types."
                    ];
                }

                $listAbbrevs = '';
                // retrieve list members
                foreach ($lists as $list) {
                    $listDbId = $list['listDbId'];
                    if ($newListDbId != $listDbId) {
                        $listAbbrevs .= ($listAbbrevs == '') ? $list['abbrev'] : '|'.$list['abbrev'];

                        if ($mode == 'copy') {
                            $response = $this->lists->searchAllMembers(
                                $listDbId,
                                ["fields"=>"listMember.data_id AS id|listMember.display_value AS displayValue"]
                            );

                            if (isset($response['success']) && !empty($response['success']) && isset($response['totalCount']) && !empty($response['totalCount']) && $response['success'] && $response['totalCount'] > 0) {
                                $members = $response['data'];
                                $response = $this->lists->createMembers($newListDbId, $members, false,[],true,false);
                            }
                        } else {
                            $response = $this->lists->searchAllMembers(
                                $listDbId,
                                ["fields"=>"listMember.id AS id|listMember.display_value AS displayValue"]
                            );

                            if (isset($response['success']) && !empty($response['success']) && isset($response['totalCount']) && !empty($response['totalCount']) && $response['success'] && $response['totalCount'] > 0) {
                                $members = $response['data'];
                                $memberIds = array_column($members, "id");
                                // update list ids
                                $requestData = [
                                    "listDbId" => $newListDbId
                                ];

                                $response = $this->lists->listMember->updateMany($memberIds, $requestData);
                            }
                        }
                    }
                }
    
                // remove duplicate members from list that resulted from merging
                if ($removeDuplicates) {
                    $response = $this->lists->searchAllMembers($newListDbId, ["fields"=>"listMember.id AS id"], 'duplicatesOnly=true');
    
                    if (isset($response['success']) && !empty($response['success']) && isset($response['totalCount']) && !empty($response['totalCount']) && $response['success'] && $response['totalCount'] > 0) {
                        $members = $response['data'];
                        $memberIds = array_column($members, "id");
                        $response = $this->lists->listMember->deleteMany($memberIds);
                    }
                }
    
                // update remarks
                $requestData = [
                    "remarks" => "merged from " . $listAbbrevs
                ];
                $this->lists->updateOne($newListDbId, $requestData);

                return [
                    'success' => true
                ];
            }
        }
    }

    /**
     * Split list into specified number of child lists
     * @param Integer $listId id of list to be split
     * @param Integer $childListCount number of expected child lists
     * @param String $splitOrder order of split
     * @return Array objects needed to render status of operation 
     */
    public function splitList($listId,$childListCount,$splitOrder) {
        $status = '';
        $message = '';
        $newListIds = [];
        $childListMemberIds = [];

        $parentListInfo = [];
        $param = [];
        
        // retrieve parent list
        $model = $this->lists->getOne($listId);
        
        if(!isset($model) || empty($model)){
            $status = false;
            $message = 'Error encountered while retrieving list information. Kindly try again.';

            return ['success'=>false,'message'=>$message,'targetLists'=>$newListIds, 'childListMemberIds'=>$childListMemberIds];
        }

        if ($model['totalCount'] < 1 || $model['status'] != 200) {
            $status = $model['status'];
            $message = 'Error encountered in retrieving parent list information. Kindly contact Administrator.';

            return ['success'=>$model['success'],'message'=>$message,'targetLists'=>$newListIds, 'childListMemberIds'=>$childListMemberIds];
        }
        else{
            $parentListInfo = [
                "id" => $listId,
                "abbrev" => trim($model['data']['abbrev']),
                "name" => trim($model['data']['name']),
                "displayName" => $model['data']['displayName'],
                "type" => $model['data']['type'],
                "description" => $model['data']['description'],
                "remarks" => $model['data']['remarks']
            ];   
        }
        
        $filters = $splitOrder == 'asc' ? 'sort=listMemberDbId:ASC' : 'sort=listMemberDbId:DESC';
        
        // retrieve parent list members
        $data = $this->lists->searchAllMembers($listId,null,$filters);

        if(!isset($data) || empty($data)){
            $status = false;
            $message = 'Error encountered in retrieving list members information. Kindly contact Administrator.';

            return ['success'=>false,'message'=>$message,'targetLists'=>$newListIds, 'childListMemberIds'=>$childListMemberIds];
        }
        
        $currentChildCount = $this->getChildCount($listId)['count'];
        
        if ($data['totalCount'] > 0) {
            $results = (isset($data['data'])) ? $data['data'] : [];

            $size = count($results) / $childListCount;
            $size = is_float($size) ? intval($size)+1 : $size;
            
            // split the entire array into child arrays
            $childListsMembers = array_chunk($results,$size);

            // create new lists for child lists
            $childListMemberIds[] = array_column($childListsMembers[0],'listMemberDbId');
            for($i=1;$i<count($childListsMembers);$i++){

                $currentChildCount += 1;
                $childListMemberIds[] = array_column($childListsMembers[$i],'listMemberDbId');

                $param['records'][] = [
                    "abbrev" => trim($parentListInfo['abbrev'])."_".$currentChildCount,
                    "name" => trim($parentListInfo['name'])."-".$currentChildCount,
                    "displayName" => trim($parentListInfo['displayName'])."-".$currentChildCount,
                    "type" => $parentListInfo['type'],
                    "description" => "List created from ".trim($parentListInfo['name']),
                    "remarks" => "Parent List: ".trim($parentListInfo['name'])
                ];
            }
            // create new list
            $data = $this->lists->create($param);

            if(!isset($data) || empty($data)){
                $status = false;
                $message = 'Error encountered in creating new list record. Kindly contact Administrator.';
    
                return ['success'=>false,'message'=>$message,'targetLists'=>$newListIds, 'childListMemberIds'=>$childListMemberIds];
            }

            if ($data['status'] == 200) {
                $newListIds = array_column($data['data'],'listDbId');
                array_push($newListIds,$listId);

                $message = $data['message'];
            }
            else{
                $message = 'Error in creating new child lists.';
            }
            $status = $data['status'];
        }
        else {
            $status = $data['status'] == 200 ? 400 : $data['status'];
            $message = $data['status'] == 200 ? 'List has no members.' : 'Error in retrieving list members of parent list.';
        }      
        
        $status = $status == 200 ? true : false;
        return ['success'=>$status,'message'=>$message,'targetLists'=>$newListIds,'childListMemberIds'=>$childListMemberIds];
    }

    /**
     * Update list ids of child lists
     * @param Array $childListsMembers split child lists 
     * @param Integer $childListCount number of expected child lists
     * @param String $splitOrder order of split
     * @return Array if transaction is valid, error message 
     */
    public function updateParentInfo($listsMembersIds,$newListIds){

        $status = true;
        $message = '';
        $size = 0;

        // update list id of list members in each chunk to id of newly created list
        $result = array_map(function($childListIds,$listDbId,$success=true){
                $param = [
                    "listDbId" => $listDbId
                ];
                $response = $this->listMember->updateMany($childListIds,$param);
                $success = isset($response['success']) && !empty($response['success']) ? $response['success'] && $success : $success;

                return ['response'=>$response,'success'=>$success];
        },$listsMembersIds,$newListIds);

        $size = count($newListIds);
        $status = $result[$size-1]['success'] ? true : false;
        
        if(!$status){
            $message = 'Error in updating parent information of child lists.';
        }

        return ['success'=>$status,'message'=>$message];
    }

    /**
     * Get existing child count of list
     * @param Integer $listId id of list
     * @return Array $count $status $message 
     */
    public function getChildCount($listId){
        $list = null;
        $status = true;
        $message = '';

        $list = $this->lists->getOne($listId);

        if(!isset($list) || empty($list)){
            $status = false;
            $message = 'Error encountered while retrieving list information. Kindly contact Administrator.';

            return ['status'=>$status,'message'=>$message,'count'=>0];
        }

        if ($list['totalCount'] < 1) {
            $status = $list['status'];
            $message = 'Error in retrieving list members of parent list';

            return ['status'=>$status,'message'=>$message,'count'=>0];
        }
        else{
            $name = trim($list['data']['name']);

            $response = $this->lists->searchAll(
                [
                    "remarks"=>"equals Parent List: ".$name    
                ]
            );
            
            if($response['status'] == 200){
                $status = $response['status'];
                $message = $response['message'];
                $count = $response['totalCount'];

                return ['status'=>$status,'message'=>$message,'count'=>$count];
            }
            else{
                return ['status'=>$response['status'],'message'=>$response['body']['status']['message'],'count'=>0];
            }
        }

    }

}
