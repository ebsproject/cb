<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\dataproviders\ArrayDataProvider;

/**
 * Model class for List that is related to germplasm
 */

class GermplasmList extends \yii\base\Model
{
    public $name;
    public $abbrev;
    public $display_name;
    public $type;
    public $description;
    public $remarks;

    /**
    * Load germplasm list data provider
    *
    * @param $ids array list of germplasm IDs
    *
    * @return mixed array data provider and total count of germplasm
    */
    public function loadGermplasmListProvider($ids = []) {
        $filters = [
            'germplasmDbId' => 'equals ' . implode('|', $ids)
        ];

        $response = Yii::$app->api->getResponse('POST', 'germplasm-search?', json_encode($filters));

        $data = $response['body']['result']['data'];
        $count = $response['body']['metadata']['pagination']['totalCount'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'orderNumber',
            // 'restified' => true,
            'totalCount' => $count,
            'pagination' => false
        ]);

        return ['dataProvider'=>$dataProvider,'count'=>$count];
    }

    /**
     * Save as new germplasm list
     *
     * @param $value array list values
     */
    public static function saveGermplasmList($value) {
        $request = [
            "records" => [
                $value
            ]
        ];

        $data = Yii::$app->api->getResponse('POST', 'lists', json_encode($request));

        if($data['status'] == 200) {
            $listId = $data['body']['result']['data'][0]['listDbId'];
            return ['result' => true, 'listId' => $listId];
        } else {
            return ['result' => false, 'message' => 'There seems to be a problem while saving the list.'];
        }
    }

    /**
     * Save as new variable list
     *
     * @param $listId integer list identifier
     * @param $ids array list of member identifiers
     * @param $type string type of list
     */
    public static function saveGermplasmListMembers($listId, $ids, $type) {
        $records = [];
        foreach($ids as $id) {
            $records[] = [
                'id' => $id,
                'displayValue' => $type
            ];
        }

        $request = [
            "records" => $records
        ];

        $data = Yii::$app->api->getResponse('POST', 'lists/'.$listId.'/members', json_encode($request));

        if($data['status'] == 200) {
            return ['result' => true];
        } else {
            return ['result' => false, 'message' => 'There seems to be a problem while saving the list members.'];
        }
    }
}