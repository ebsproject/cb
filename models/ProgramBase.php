<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.program".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the product development program
 * @property string $name Name identifier of the product development program
 * @property int $pipeline_id Pipeline where the product development program belongs to
 * @property string $description Description about the product development program
 * @property string $display_name Name of the product development program to show to users
 * @property string $remarks Additional details about the product development program
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $type Program type can be: VDP: variety development pipeline, CCRD: crosscutting research and development, TDP: trait development pipeline
 * @property int $level_type Level of program
 * @property string $program_status Status of program creation (DRAFT or CREATED)
 * @property bool $is_active Whether a program is enabled or disabled
 * @property string $uuid Universally unique identifier of the record
 * @property string $record_uuid Universally unique identifier of the record
 * @property array $event_log Changes made to the record
 * @property int $crop_id Refers to the crop where the program specializes or conduct experiments in
 */
class ProgramBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.program';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abbrev', 'name'], 'required'],
            [['pipeline_id', 'creator_id', 'modifier_id', 'level_type', 'crop_id'], 'default', 'value' => null],
            [['pipeline_id', 'creator_id', 'modifier_id', 'level_type', 'crop_id'], 'integer'],
            [['description', 'remarks', 'notes', 'program_status', 'uuid', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void', 'is_active'], 'boolean'],
            [['abbrev'], 'string', 'max' => 128],
            [['name', 'display_name'], 'string', 'max' => 256],
            [['type'], 'string', 'max' => 8],
            [['record_uuid'], 'unique'],
            [['abbrev'], 'unique'],
            [['name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'pipeline_id' => 'Pipeline ID',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'type' => 'Type',
            'level_type' => 'Level Type',
            'program_status' => 'Program Status',
            'is_active' => 'Is Active',
            'uuid' => 'Uuid',
            'record_uuid' => 'Record Uuid',
            'event_log' => 'Event Log',
            'crop_id' => 'Crop ID',
        ];
    }
}
