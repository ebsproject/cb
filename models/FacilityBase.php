<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "master.facility".
 *
 * @property int $id Primary key
 * @property int $place_id
 * @property int $institute_id Foreign key to the institute
 * @property string $name The name of the facility
 * @property string $facility_type The type of the facility: growth chamber, glass house, screen house, farm/
 * @property double $latitude The latitude of the facility
 * @property double $longitude The longitude of the facility
 * @property string $other_name Other name of the facility
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property bool $is_active
 * @property int $level
 * @property int $parent_id
 * @property int $object_id
 * @property string $layer_id
 * @property string $section
 * @property string $zone
 * @property string $table_id
 * @property string $warehouse_key
 * @property string $facility_qr_code QR code value of facility
 * @property string $uuid Universally unique identifier of the record
 * @property int $nuid
 */
class FacilityBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.facility';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id', 'name'], 'required'],
            [['place_id', 'institute_id', 'creator_id', 'modifier_id', 'level', 'parent_id', 'object_id', 'nuid'], 'default', 'value' => null],
            [['place_id', 'institute_id', 'creator_id', 'modifier_id', 'level', 'parent_id', 'object_id', 'nuid'], 'integer'],
            [['name', 'facility_type', 'other_name', 'remarks', 'notes', 'layer_id', 'section', 'zone', 'table_id', 'facility_qr_code', 'uuid'], 'string'],
            [['latitude', 'longitude', 'warehouse_key'], 'number'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void', 'is_active'], 'boolean'],
            [['facility_qr_code'], 'unique'],
            [['nuid'], 'unique'],
            [['institute_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterInstitute::className(), 'targetAttribute' => ['institute_id' => 'id']],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterPlace::className(), 'targetAttribute' => ['place_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Place ID',
            'institute_id' => 'Institute ID',
            'name' => 'Name',
            'facility_type' => 'Facility Type',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'other_name' => 'Other Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'is_active' => 'Is Active',
            'level' => 'Level',
            'parent_id' => 'Parent ID',
            'object_id' => 'Object ID',
            'layer_id' => 'Layer ID',
            'section' => 'Section',
            'zone' => 'Zone',
            'table_id' => 'Table ID',
            'warehouse_key' => 'Warehouse Key',
            'facility_qr_code' => 'Facility Qr Code',
            'uuid' => 'Uuid',
            'nuid' => 'Nuid',
        ];
    }
}
