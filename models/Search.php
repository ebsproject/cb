<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\models\BaseModel;

/**
 * Search for model for entity documents
 */

class Search extends BaseModel
{
    public function __construct (
        public BrowserController $browserController,
        $config = []
    )
    {
        parent::__construct($config);
    }

    /**
     * Returns document search results
     *
     * @param $seach text product catalog data browser additional query conditions
     * @param $entity text whether searching for germplasm, experiment, package, person, trait, or tool
     *
     * @return $dataProvider array product catalog data provider
     */
    public function getDataProvider($search, $entity) {
        $search = !empty($search) ? $search : '';
        $searchTrimmed = preg_replace('/[[:blank:]]+/', ' ', trim($search));
        $filterValue = '';
        $searchInDocument = false;

        if(strlen($searchTrimmed) > 1 && (
                (substr($searchTrimmed,-1) == '"' && substr($searchTrimmed,0,1) == '"') ||  // exact
                (substr($searchTrimmed,-1) == '%') ||   // starts with
                ($searchTrimmed[0] == '%') ||   // ends with
                ($searchTrimmed[0] == '%' && substr($searchTrimmed,-1) == '%') ||   // contains
                (substr($searchTrimmed,-1) == '_')  // one additional character
            )) { //with operators

            // clean search string
            $searchStr = preg_replace('|[^$A-Za-z0-9 /*.@;-]|', ' ', $searchTrimmed);
            $searchStr = preg_replace('/[[:blank:]]+/', ' ', trim($searchStr));
            $searchStr = str_replace(" "," & ",trim($searchStr));

            if(substr($searchTrimmed,-1) == '"' && substr($searchTrimmed,0,1) == '"') { // exact search
                $filterValue = str_replace('"',"",trim($searchTrimmed));
            } else {    // others search
                $filterValue = $searchTrimmed;
            }
        } else {
            $searchInDocument = true;

            // process filter
            $filterValue = $this->browserController->processDocumentFilter($searchTrimmed);
            $searchStr = $filterValue;
        }

        $dbId = '';
        $nameColumn = '';
        $columns = [];
        $columnNames = [];
        $data = [];

        $paramPage = '';
        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }
        $paramLimit = 'limit=10';

        // entity is automatically specified
        switch ($entity) {
            case 'germplasm':
                if(empty($filterValue)) {
                    break;
                }
                if($searchInDocument) {
                    $filters = [
                        "germplasmDocument" => $filterValue
                    ];
                    if (strpos($searchTrimmed, '?/?') !== false) {
                        $filters2 = [
                            "parentage" => '?/?'
                        ];
                    }
                } else {
                    $filters = [
                        "designation" => $filterValue
                    ];
                }

                $response = Yii::$app->api->getResponse('POST', 'germplasm-search?'.$paramLimit.$paramPage, json_encode($filters));
                $data = $response['body']['result']['data'];
                if(isset($filters2) && empty($data)) {
                    $response = Yii::$app->api->getResponse('POST', 'germplasm-search?'.$paramLimit.$paramPage, json_encode($filters2));
                    $data = $response['body']['result']['data'];
                }

                $dbId = 'germplasmDbId';
                $nameColumn = 'designation';
                $columns = [
                    'designation',
                    'parentage',
                    'generation',
                    'germplasmState',
                    'germplasmNameType',
                    'germplasmNormalizedName',
                    'otherNames'
                ];
                $columnNames = [
                    'Designation',
                    'Parentage',
                    'Generation',
                    'Germplasm State',
                    'Germplasm Name Type',
                    'Germplasm Normalized Name',
                    'Other Names'
                ];
                break;
            case 'experiment':
                if($searchInDocument) {
                    $filters = [
                        "experimentDocument" => $filterValue
                    ];
                } else {
                    $filters = [
                        "experimentName" => $filterValue
                    ];
                }

                $response = Yii::$app->api->getResponse('POST', 'experiments-search?'.$paramLimit.$paramPage, json_encode($filters));
                $data = $response['body']['result']['data'];

                $dbId = 'experimentDbId';
                $nameColumn = 'experimentName';
                $columns = [
                    'experimentName',
                    'experimentCode',
                    'experimentYear',
                    'experimentType',
                    'experimentSubType',
                    'experimentSubSubType',
                    'experimentDesignType',
                    'experimentStatus',
                    'plantingSeason',
                    'seasonName',
                    'seasonCode',
                    'stageName',
                    'stageCode',
                    'projectName',
                    'projectCode',
                    'steward'
                ];
                $columnNames = [
                    'Experiment Name',
                    'Experiment Code',
                    'Experiment Year',
                    'Experiment Type',
                    'Experiment Sub Type',
                    'Experiment Sub Sub Type',
                    'Experiment Design Type',
                    'Experiment Status',
                    'Planting Season',
                    'Season Name',
                    'Season Code',
                    'Stage Name',
                    'Stage Code',
                    'Project Name',
                    'Project Code',
                    'Steward'
                ];
                break;
            case 'package':
                if($searchInDocument) {
                    $filters = [
                        "packageDocument" => $filterValue
                    ];
                } else {
                    $filters = [
                        "label" => $filterValue
                    ];
                }

                $response = Yii::$app->api->getResponse('POST', 'packages-search?'.$paramLimit.$paramPage, json_encode($filters));
                $data = $response['body']['result']['data'];

                $dbId = 'packageDbId';
                $nameColumn = 'label';
                $columns = [
                    'label',
                    'designation',
                    'packageCode',
                    'quantity',
                    'seedName',
                    'harvestDate'
                ];
                $columnNames = [
                    'Label',
                    'Designation',
                    'Package Code',
                    'Quantity',
                    'Seed Name',
                    'Harvest Date'
                ];
                break;
            case 'person':
                if($searchInDocument) {
                    $filters = [
                        "personDocument" => $filterValue
                    ];
                } else {
                    $filters = [
                        "personName" => $filterValue
                    ];
                    $filters2 = [
                        "email" => $filterValue
                    ];
                }

                $response = Yii::$app->api->getResponse('POST', 'persons-search?'.$paramLimit.$paramPage, json_encode($filters));
                $data = $response['body']['result']['data'];
                if(isset($filters2) && empty($data)) {
                    $response = Yii::$app->api->getResponse('POST', 'persons-search?'.$paramLimit.$paramPage, json_encode($filters2));
                    $data = $response['body']['result']['data'];
                }

                $dbId = 'personDbId';
                $nameColumn = 'personName';
                $columns = [
                    'email',
                    'username',
                    'firstName',
                    'lastName',
                    'personType'
                ];
                $columnNames = [
                    'Email',
                    'Username',
                    'First Name',
                    'Last Name',
                    'Person Type'
                ];
                break;
                case 'trait':
                    if($searchInDocument) {
                        $filters = [
                            "variableDocument" => $filterValue
                        ];
                    } else {
                        $filters = [
                            "label" => $filterValue
                        ];
                    }
    
                    $response = Yii::$app->api->getResponse('POST', 'variables-search?'.$paramLimit.$paramPage, json_encode($filters));
                    $data = $response['body']['result']['data'];
    
                    $dbId = 'variableDbId';
                    $nameColumn = 'label';
                    $columns = [
                        'abbrev',
                        'label',
                        'name',
                        'propertyId',
                        'scaleId',
                        'dataType',
                        'type',
                        'dataLevel',
                        'status'
                    ];
                    $columnNames = [
                        'Abbrev',
                        'Label',
                        'Name',
                        'Property',
                        'Scale',
                        'Data Type',
                        'Type',
                        'Data Level',
                        'Status'
                    ];
                    break;
            case 'tool':
                if($searchInDocument) {
                    $filters = [
                        "applicationDocument" => $filterValue
                    ];
                } else {
                    $filters = [
                        "label" => $filterValue
                    ];
                }

                $response = Yii::$app->api->getResponse('POST', 'applications-search?'.$paramLimit.$paramPage, json_encode($filters));
                $data = $response['body']['result']['data'];

                $dbId = 'applicationDbId';
                $nameColumn = 'label';
                $columns = [
                    'abbrev',
                    'label',
                    'actionLabel',
                    'description'
                ];
                $columnNames = [
                    'Abbrev',
                    'Label',
                    'Action Label',
                    'Description'
                ];
                break;
            default:
                throw new \yii\web\HttpException(400, 'Please provide a valid entity!');
                break;
        }

        // add entity_id, entity_name, and entity to resulting data
        foreach($data as $key => $val) {
            $data[$key]['entityId'] = $data[$key][$dbId];
            $data[$key]['entityName'] = $data[$key][$nameColumn];
            $data[$key]['entity'] = $entity;
            $entityDescription = '';
            foreach($columns as $columnKey => $columnVal) {
                if(isset($data[$key][$columnVal]) && !is_null($data[$key][$columnVal]) && $data[$key][$columnVal] !== '') {
                    $columnValue = $data[$key][$columnVal];
                    if($columnVal == 'quantity') {
                        $columnValue = round($columnValue, 2).' '.$data[$key]['unit'];
                    }
                    $entityDescription = $entityDescription.' • <b>'.$columnNames[$columnKey].'</b>: '.$columnValue;
                }
            }
            $entityDescription = substr($entityDescription, 4); // removes ' • ' at the beginning
            $data[$key]['entityDescription'] = $entityDescription;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'entityId',
            'restified' => true,
            'totalCount' => $response['body']['metadata']['pagination']['totalCount']
        ]);

        return [
            'firstEntity'=>$entity,
            'dataProvider'=>$dataProvider,
            'searchStr'=>$searchStr
        ];
    }
}