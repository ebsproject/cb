<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;
use ChromePhp;

/**
 * This is the model class for table "platform.plan_template".
 *
 * @property int $id
 * @property string $template_name
 * @property string $mandatory_info
 * @property int $temp_file_cabinet_id
 * @property array $randomization_results
 * @property array $randomization_data_results
 * @property array $randomization_input_file
 * @property string $status
 * @property string $remarks additional details about the record
 * @property string $creation_timestamp timestamp when the record was added to the table
 * @property int $creator_id id of the user who added the record to the table
 * @property string $modification_timestamp timestamp when the record was last modified
 * @property int $modifier_id id of the user who last modified the record
 * @property string $notes additional details added by an admin; can be technical or advanced details
 * @property bool $is_void indicator whether the record is deleted (true) or not (false)
 * @property int $program_id
 * @property array $study_applied
 * @property string $design
 * @property int $entry_count
 * @property int $plot_count
 * @property int $check_count
 * @property int $repcount
 */
class PlanTemplate extends BaseModel
{
    
    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
      return 'plan-templates';
    }

    /**
     * Create record of plan template
     *
     * @param $data array data for creating the plan template
     * @return $id integer ID of the created plan template
     */
    public static function createPlanTemplate($data){
      $id = null;
      $postData['records'][] = $data;
      
      $responseApi = Yii::$app->api->getResponse('POST','plan-templates',json_encode($postData));
      
      if($responseApi['status']==200 && isset($responseApi['body']['result']['data'])){
          $id = $responseApi['body']['result']['data'][0]['planTemplateDbId'];
      }

      return $id;
    }

    /**
     * Get plan template by entity filter
     *
     * @param $data array filter for searching plan template
     * @return $result array data for the plan template
     */
    public static function getPlanTemplateByEntity($data){
        $result = NULL;

        $responseApi = Yii::$app->api->getResponse('POST', 'plan-templates-search', json_encode($data));
        
        if (isset($responseApi['status']) && $responseApi['status'] == 200 && isset($responseApi['body']['result']['data'][0])){
            $result = $responseApi['body']['result']['data'][0];
        }

        return $result;
    } 
    
    /*
     * Get plan template by id
     *
     * @param $abbrev text config abbrev
     * @return $data array config value
     */
    public static function getPlanTemplateById($id){
      $result = [];
      $data = Yii::$app->api->getResponse('GET',"plan-templates/$id");
     
      // check if has result
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
          $result = $data['body']['result']['data'][0];
      }
      
      return $result;
    }
}
