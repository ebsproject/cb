<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\models\Experiment;
use app\models\ExperimentProtocol;

/**
 * This is the model class for table "tenant.protocol".
 *
 * @property int $id Protocol ID: Database identifier of the protocol [PROT_ID]
 * @property string $protocol_code Protocol Code: Textual identifier of the protocol [PROT_CODE]
 * @property string $protocol_name Protocol Name: Full name of the protocol [PROT_NAME]
 * @property string $protocol_type Protocol type: Type of the protocol {planting, trait, sampling, genotyping, ...} [PROT_TYPE]
 * @property string|null $description Protocol Description: Additional information about the protocol [PROT_DESC]
 * @property int $program_id Program ID: Reference to the program that created the protocol
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class Protocol extends BaseModel {

    public function __construct (
        public Experiment $experiment,
        public ExperimentProtocol $experimentProtocol,
        $config = []
    ) { parent::__construct($config); }

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'protocols';
    }

    /**
     * Create records of tenant.protocol and experiment.experiment_protocol
     * 
     * @param int experimentId experiment record identifier
     * @param array protocolType type of the protocol (planting, trait)
     * 
     * @return boolean whether the records have been successfully created or not
     */
    public function createExperimentProtocols($experimentId, $protocolType){

        $experimentInfo = $this->experiment->getOne($experimentId);
        $experimentCode = $experimentInfo['data']['experimentCode'] ?? '';
        $programDbId = $experimentInfo['data']['programDbId'] ?? '';

        $protocolName = $protocolCode = '';

        $protocolCode = strtoupper($protocolType)."_PROTOCOL_".$experimentCode;
        $protocolName = ucwords(str_replace('_', ' ', strtolower($protocolCode)));
         
        
        $requestedData['records'][] = [
            'protocolName' => $protocolName,
            'protocolCode' => $protocolCode,
            'protocolType' => $protocolType,
            'programDbId' => "$programDbId",
        ];

        // insert tenant.protocol record
        $newProtocolRecord = $this->create($requestedData);
        $protocolDbId = isset($newProtocolRecord['data'][0]['protocolDbId']) ? $newProtocolRecord['data'][0]['protocolDbId'] : 0;

        if($protocolDbId){
            // This is to avoid duplicate orderNo if there are concurrent creation of Experiment Protocols
            if ($protocolType === 'planting') {
                $orderNo = 1;
            } else if ($protocolType === 'pollination') {
                $orderNo = 2;
            } else if ($protocolType === 'trait') {
                $orderNo = 3;
            } else if ($protocolType === 'management') {
                $orderNo = 4;
            } else if ($protocolType === 'harvest') {
                $orderNo = 5;
            }

            $requestedExpData['records'][] = [
                'experimentDbId' => "$experimentId",
                'protocolDbId' => "$protocolDbId",
                'orderNo' => "$orderNo",
            ];
            
            $this->experimentProtocol->create($requestedExpData);


        }
        
        return $protocolDbId;
    }

}
