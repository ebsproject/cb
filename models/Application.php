<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\models\UserDashboardConfig;
use app\interfaces\models\IApplication;
use Yii;

/**
 * This is the model class for table "platform.application".
 *
 * @property int $id Unique identifier of the record
 * @property string $abbrev Abbreviation of the record
 * @property string $label Label of the record
 * @property string $action_label Label for the action of the tool
 * @property string $icon Icon of the application
 * @property string $description More information about the record
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 *
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property PlatformApplicationAction[] $platformApplicationActions
 */
class Application extends BaseModel implements IApplication
{

    /**
     * API endpoint for applications
     */
    public static function apiEndPoint() {
        return 'applications';
    }

    /**
     * Return processed url of an application given application id
     *
     * @param $id integer application identifier
     * @return $url text processed url of an application
     */
    public function getUrlByAppId($id,$program)
    {

        $param = [
            'applicationDbId' => (string)$id
        ];

        $data = Yii::$app->api->getResponse('POST','application-actions-search?applicationDbId:ASC&limit=1', json_encode($param) );

        // check if there is a record found in application action
        if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){

            $data = $data['body']['result']['data'][0];

            $result[] = $data;
        }

        $url = '';

        //if there is record in application action
        if(isset($result[0]) && !empty($result[0])){

            if(isset($result[0]['module'])){
                $url = '/'.$result[0]['module'];
            }
            if(isset($result[0]['controller'])){
                $url .= '/'.$result[0]['controller'];
            }
            if(isset($result[0]['action'])){
                $url .= '/'.$result[0]['action'];
            }
            if(isset($result[0]['params'])){
                $params = '';

                $tempArr = json_decode($result[0]['params']);

                foreach ($tempArr as $key => $value) {
                    if($params == ''){$params = $params . '?';}
                    else{$params = $params . '&';}
                    $params = $params . $key .'=' .$value;
                }
                $url .= $params;
            }
        }

        if(empty($url)){
            $url = '#';
        }

        if(strpos($url, '?') !== false ){ //if has parameter
            $url = $url . '&program='.$program;
        }else{
            $url = $url . '?program='.$program;
        }

        return $url;
    }


    /**
     * Gets url of application by application abbreviation
     *
     * @param text $abbrev application abbreviation
     * @param text $program current program of user
     *
     * @return text $url url of application
     */
    public function getUrlByAppAbbrev($abbrev,$program){

        // check if app url is already in session
        if(Yii::$app->session->get('appUrl-'.$abbrev) != null){
            $appUrl = Yii::$app->session->get('appUrl-'.$abbrev);

            // update the program parameter based on current dashboard program filter
            return preg_replace('/program=.*/', 'program='.$program, $appUrl);
        }

        $url = null;

        //get application id by abbrev
        $data = Yii::$app->api->getResponse('GET','applications?abbrev='.$abbrev);

        if(isset($data['body']['result']['data'][0]['applicationDbId']) && isset($data['status']) && $data['status'] == 200){
            $applicationId = $data['body']['result']['data'][0]['applicationDbId'];
        }

        if(isset($applicationId) && !empty($applicationId)){
            $url = Application::getUrlByAppId($applicationId,$program);
        }

        // set url of app to session
        Yii::$app->session->set('appUrl-'.$abbrev,$url);

        return $url;
    }

    /**
     * Gets application information by id
     *
     * @param $id integer application identifier
     * @return $result object application information
     */
    public function getAppInfoById($id){
        $application = [];

        $data = Yii::$app->api->getResponse('GET','applications/'.$id);

        if(isset($data['body']['result']['data'][0]) && isset($data['status']) && $data['status'] == 200){
            $application = $data['body']['result']['data'][0];
        }

        if(isset($application) && !empty($application)){
            return $application;
        }
    }

    /**
     * Gets application information by abbrev
     *
     * @param $abbrev integer application identifier
     * @return $result object application information
     */
    public function getAppInfoByAbbrev($abbrev){

        // check if config is already in session
        if(Yii::$app->session->get('config-'.$abbrev) != null){
            return Yii::$app->session->get('config-'.$abbrev);
        }

        $application = [];
        
        $data = Yii::$app->api->getResponse('GET','applications?abbrev='.$abbrev);
        
        if(isset($data['body']['result']['data'][0]) && isset($data['status']) && $data['status'] == 200){
            $application = $data['body']['result']['data'][0];
        }

        // set config value to session
        Yii::$app->session->set('config-'.$abbrev, $application);

        return $application;
    }

    /**
     * Return module, cotntroller and action of an application given application id
     *
     * @param $id integer application identifier
     * @return $url text processed url of an application
     */
    public function getOptionsAppId($id)
    {

        $options = [];

        $param = [
            'applicationDbId' => (string)$id
        ];
        
        $data = Yii::$app->api->getResponse('POST','application-actions-search?applicationDbId:ASC&limit=1', json_encode($param) );

        // check if there is a record found in application action
        if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){

            $data = $data['body']['result']['data'][0];

            $result[] = $data;
        }

        //if there is record in application action
        if(isset($result[0]) && !empty($result[0])){

            if(isset($result[0]['module'])){
                $options['module'] = $result[0]['module'];
            }
            if(isset($result[0]['controller'])){
                $options['controller'] = $result[0]['controller'];
            }
            if(isset($result[0]['action'])){
                $options['action'] = $result[0]['action'];
            }
        }
        
        $options['iconStyle'] = 'vertical-align:middle';

        return $options;
    }

    /**
     * Retrieves all applications filter tags where user has access to
     *
     * @param $userId integer user identifier
     * @param $timestamp 
     * @return $result array list of applications tags where user has access to
     */
    public function getApplications($userId,$timestamp){

        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config

        $appIds = []; //exclude already added
        if(isset($data[0]) && !empty($data[0])){ //if there is saved data in config
            $valArr = json_decode($data[0],true);
            $appIds = (isset($valArr['tools_widget_'.$timestamp])) ? $valArr['tools_widget_'.$timestamp] : [];
        }

        $params = [];
        
        // if not admin, show tools that have access
        if(Yii::$app->session->get('isAdmin') !== true){
            $appAbbrevs = json_decode(Yii::$app->session->get('appAbbrevs'));
            array_push($appAbbrevs, 'PROFILE');

            // add search if not collaborator
            if(Yii::$app->session->get('showSearchTool')){
                array_push($appAbbrevs, 'SEARCH');
            }

            $appCond = implode('|', $appAbbrevs);

            $params = [
                'abbrev' => $appCond
            ];
        }

        // to implement access control later
        
        $method = 'POST';
        $path = 'applications-search';
        $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($params), null, true);

        $apps = isset($response['data']) ? $response['data'] : [];
        $tags = [];
        foreach ($apps as $value) {

            if(!in_array($value['applicationDbId'], $appIds)){
                $tags[] = [
                    'id' => $value['applicationDbId'],
                    'text' => $value['label']
                ];
            }
        }

        if(empty($tags)){ //if no tool
            $tags = 0;
        }
        
        return $tags;
    }

}
