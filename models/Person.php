<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use app\interfaces\models\IApi;
use app\interfaces\models\IPerson;
use app\interfaces\models\IUser;
use app\interfaces\models\IConfig;
use Yii;

/**
 * This is the model class for table "tenant.person".
 *
 * @property int $id Person ID: Database identifier of the person [PERSON_ID]
 * @property string $username Username: Short textual identifier of the person [PERSON_USERNAME]
 * @property string $first_name Person First Name: First/given name of the person [PERSON_FNAME]
 * @property string $last_name Person Last Name: Last/family name of the person  [PERSON_LNAME]
 * @property string $person_name Person Name: Full name of the person [PERSON_NAME]
 * @property string $person_type Person Type: Type of person {admin, user, person} [PERSON_TYPE]
 * @property string|null $email Email: Email address of the person, if available [PERSON_EMAIL]
 * @property int $person_role_id Person Role ID: Reference to the role that defines the privileges of the person in the system [PERSON_PROLE_ID]
 * @property string $person_status Person Status: Status of the person {active, inactive} [PERSON_STATUS]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 * @property bool|null $is_active Status of the person
 * @property string|null $person_document Sorted list of distinct lexemes which are normalized; used in search query
 */
class Person extends BaseModel implements IPerson
{
    public function __construct (
        protected IApi $api,
        protected IUser $user,
        protected IConfig $iConfig,
        $config = []
    )
    { parent::__construct($config); }

    /**
    * API endpoint for person
    */
    public static function apiEndPoint() {
        return 'persons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'first_name', 'last_name', 'person_name', 'person_type', 'person_role_id', 'person_status', 'creator_id'], 'required'],
            [['person_role_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['person_role_id', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void', 'is_active'], 'boolean'],
            [['notes', 'person_document'], 'string'],
            [['username', 'first_name', 'last_name', 'email'], 'string', 'max' => 128],
            [['person_name'], 'string', 'max' => 256],
            [['person_type'], 'string', 'max' => 16],
            [['person_status'], 'string', 'max' => 64],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetAttribute' => ['modifier_id' => 'id']],
            [['person_role_id'], 'exist', 'skipOnError' => true, 'targetAttribute' => ['person_role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'person_name' => 'Person Name',
            'person_type' => 'Person Type',
            'email' => 'Email',
            'person_role_id' => 'Person Role ID',
            'person_status' => 'Person Status',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
            'is_active' => 'Is Active',
            'person_document' => 'Person Document',
        ];
    }

    /**
     * Get logged in user person info
     */
    public function getLoggedInPersonInfo() {
        $personData = [];
        $session = Yii::$app->session;

        if($session->get('user.id') !== null){
            $personId = $session->get('user.id');
        }else{
            $personId = !empty($session->get('userId')) ? $session->get('userId') : 0;
        }

        // get logged in person information
        $data = Person::getOne($personId);
        if($data['success'] && !empty($data['data'])){
            $personData = $data['data'];
        }

        return $personData;
    }

    /**
     * Get person's role and type (whether Admin or not)
     * 
     * @param string $program Program identifier
     * 
     * @return mixed an object containing person role and type
     */
    public function getPersonRoleAndType ($program)
    {
        $userId = $this->user->getUserId();
        $role = '';
        $isAdmin = false;

        $data = $this->api->getApiResults(
            'GET',
            "persons/$userId/teams",
            '',
            false
        )['data'];

        // Determine whether person is Admin or not (User)
        $isAdmin = Yii::$app->session->get('isAdmin') !== null && Yii::$app->session->get('isAdmin');
        
        // Retrieve person's role in the team
        if (isset($data) && !empty($data)) {
            $role = '';

            foreach ($data as $key => $value) {
                $teamCode = $value['teamCode'];

                if (
                    isset($teamCode) &&
                    ($teamCode == $program ||
                    $teamCode == $program . '_TEAM') 
                ) {
                    $role = isset($value['personRoleCode']) ? $value['personRoleCode'] : '';

                    return [
                        $role,
                        $isAdmin,
                        $userId
                    ];
                }
            }
        }

        return [
            'UNDETERMINED',
            $isAdmin,
            $userId
        ];
    }

    /**
     * Retrieves all programs user have access to
     *
     * @return array list of programs where user has access to
     */
    public function getPersonPrograms(){
        $userId = $this->user->getUserId();
        $isAdmin = Yii::$app->session->get('isAdmin');
        $programs = [];

        // if not admin, retrieve only programs where user belongs to
        if(!$isAdmin){
            //check if RBAC is enabled in platform.config
            $rbacFlag = $this->iConfig->getConfigByAbbrev('ENABLE_RBAC_FLAG');
            $enableRbac = isset($rbacFlag) ? $rbacFlag : false;
            if($enableRbac){
                $data = Yii::$app->api->getParsedResponse('GET','brapi/user/external/'.$userId.'?domainPrefix=cb', apiResource: 'cs');

                if($data['success'] !== false){

                    // Verify if there's at least one(1) module under the program given the role
                    $appList = $data['data'][0]['permissions']['applications'] ?? [];
                    if(empty($appList)){
                        Yii::warning('No available modules for the user'.json_encode($data), __METHOD__);
                        return $programs;
                    }

                    $programList = $data['data'][0]['permissions']['memberOf']['programs'] ?? [];
                    $cbPrograms = Yii::$app->api->getParsedResponse('POST','programs-search', json_encode([
                        "fields" => "program.id AS programDbId|program.program_code AS programCode",
                        "programCode" => 'equals '.implode('|equals ', array_column($programList, 'code'))
                    ]), retrieveAll: true)['data'] ?? [];

                    foreach($cbPrograms as $key){
                        $programs[$key['programDbId']] = $key['programCode'];
                    }
                }else{
                    Yii::error('Error in retrieving the programs'.json_encode($data), __METHOD__);
                }
                return $programs;

            }else{
                $programs = Yii::$app->api->getParsedResponse('GET','persons/'.$userId.'/programs', null, 'sort=programDbId:asc')['data'] ?? [];
            }
        }else{
            // if admin, retrieve all programs
            $programs = Yii::$app->api->getParsedResponse('GET','programs', null)['data'] ?? [];
        }

        foreach($programs as $key){
            $programData[$key['programDbId']] = $key['programCode'];
        }
        return $programData;
    }

    /**
     * 
     * Get contact records with Person category in CRM
     * 
     * */  
    public function getPersonContacts(){
        $query = '
            query {
              findContactList (page:{number:1, size:100},filters:[{col:"category.id",val:"1"}], sort:[{col:"person.fullName", mod:ASC}]){
                totalPages
                totalElements
                number
                content {
                  id
                  person {
                    fullName
                  }
                }
              }
            }
        ';
       
        $response = Yii::$app->api->getGraphQlResponse('POST', $query)['findContactList'] ?? [];
        $results = isset($response['content']) ? $response['content'] : [];
        $totalPages = isset($response['totalPages']) ? $response['totalPages'] : 0;

        if($totalPages > 1){ //loop the request to get all data

            $addlResult = [];
            for ($i=2; $i <= $totalPages; $i++){
                //set page
                $query = '
                    query {
                      findContactList (page:{number:'.$i.', size:100},filters:[{col:"category.id",val:"1"}], sort:[{col:"person.fullName", mod:ASC}]){
                        totalPages
                        totalElements
                        number
                        content {
                          id
                          person {
                            fullName
                          }
                        }
                      }
                    }
                ';
                $response = Yii::$app->api->getGraphQlResponse('POST', $query)['findContactList'] ?? [];
                $addlResult = !empty($response) ? $response['content'] : [];
                $results = array_merge($results, $addlResult);
            }
        }

        $response = [];
        foreach($results as $rec){
            $temp['personDbId'] = $rec['id'];
            $temp['personName'] = isset($rec['person']['fullName']) ? $rec['person']['fullName'] : '(not set)';
            $response[] = $temp;
        }

        return $response;
    }

    /**
     * 
     * Get contact record from VRM via ID
     * 
     * @param integer $id record ID of contact record in CRM
     * 
     * */  
    public function getContact($id){


        $query = '
            query {
              findContactList (filters:[{col:"id",val:"'.$id.'"}]){
                totalPages
                totalElements
                number
                content {
                  id
                  person {
                    fullName
                  }
                }
              }
            }
        ';
       
        $response = Yii::$app->api->getGraphQlResponse('POST', $query)['findContactList'] ?? [];
        $results = isset($response['content']) ? $response['content'] : [];
        
        $response = [];
        if(!empty($results)){
            $response = [
                'personDbId' => $results[0]['id'],
                'personName' => isset($results[0]['person']['fullName']) ? $results[0]['person']['fullName'] : '(not set)'
            ];
        }

        return $response;
    }
}
