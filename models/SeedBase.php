<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "germplasm.seed".
 *
 * @property int $id Seed ID: Database identifier of the seed [SEED_ID]
 * @property string $seed_code Seed Code: Textual identifier of the seed [SEED_CODE]
 * @property string $seed_name Seed Name: Full name of the germplasm at the time the seed was harvested [SEED_NAME]
 * @property string|null $harvest_date Seed Harvest Date: Date when the seeds were harvested from the location of an experiment [SEED_HARVESTDATE]
 * @property string|null $harvest_method Harvest Method: Method of harvesting the plots in the location [SEED_HARVMETH]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm where the seed is referred [SEED_GERM_ID]
 * @property int|null $program_id Program ID: Reference to the program that owns the seeds [SEED_PROG_ID]
 * @property int|null $source_experiment_id Source Experiment ID: Reference to the experiment where the seed was harvested [SEED_EXPT_ID]
 * @property int|null $source_entry_id Source Entry ID: Reference to the entry where the seed was harvested [SEED_ENTRY_ID]
 * @property int|null $source_occurrence_id Source Occurrence ID: Reference to the occurrence where the seed was harvested [SEED_OCC_ID]
 * @property int|null $source_location_id Source Location ID: Location where the seed was harvested [SEED_LOC_ID]
 * @property int|null $source_plot_id Source Plot ID: Reference to the plot where the seed was harvested [SEED_PLOT_ID]
 * @property string|null $description Seed Description: Additional information about the seed [SEED_DESC]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class SeedBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'germplasm.seed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seed_code', 'seed_name', 'germplasm_id', 'creator_id'], 'required'],
            [['harvest_date', 'creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['germplasm_id', 'program_id', 'source_experiment_id', 'source_entry_id', 'source_occurrence_id', 'source_location_id', 'source_plot_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['germplasm_id', 'program_id', 'source_experiment_id', 'source_entry_id', 'source_occurrence_id', 'source_location_id', 'source_plot_id', 'creator_id', 'modifier_id'], 'integer'],
            [['description', 'notes'], 'string'],
            [['is_void'], 'boolean'],
            [['seed_code', 'harvest_method'], 'string', 'max' => 64],
            [['seed_name'], 'string', 'max' => 128],
            [['germplasm_id', 'seed_code'], 'unique', 'targetAttribute' => ['germplasm_id', 'seed_code']],
            [['seed_code'], 'unique'],
            [['source_entry_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentEntry::className(), 'targetAttribute' => ['source_entry_id' => 'id']],
            [['source_experiment_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentExperiment::className(), 'targetAttribute' => ['source_experiment_id' => 'id']],
            [['source_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentLocation::className(), 'targetAttribute' => ['source_location_id' => 'id']],
            [['source_occurrence_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentOccurrence::className(), 'targetAttribute' => ['source_occurrence_id' => 'id']],
            [['source_plot_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentPlot::className(), 'targetAttribute' => ['source_plot_id' => 'id']],
            [['germplasm_id'], 'exist', 'skipOnError' => true, 'targetClass' => GermplasmGermplasm::className(), 'targetAttribute' => ['germplasm_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seed_code' => 'Seed Code',
            'seed_name' => 'Seed Name',
            'harvest_date' => 'Harvest Date',
            'harvest_method' => 'Harvest Method',
            'germplasm_id' => 'Germplasm ID',
            'program_id' => 'Program ID',
            'source_experiment_id' => 'Source Experiment ID',
            'source_entry_id' => 'Source Entry ID',
            'source_occurrence_id' => 'Source Occurrence ID',
            'source_location_id' => 'Source Location ID',
            'source_plot_id' => 'Source Plot ID',
            'description' => 'Description',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }
}
