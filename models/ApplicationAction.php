<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.application_action".
 *
 * @property int $id Unique identifier of the record
 * @property int $application_id Application identifier
 * @property string|null $module Module of the application
 * @property string|null $controller Controller of the application
 * @property string|null $action Action of the application
 * @property string|null $params One or more parameters of the tool
 * @property string|null $description More information about the record
 * @property string|null $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string|null $modification_timestamp Timestamp when the record was last modified
 * @property int|null $modifier_id ID of the user who last modified the record
 * @property string|null $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 */
class ApplicationAction extends BaseModel
{
    /**
    * API endpoint for application_action
    */
    public static function apiEndPoint() {
        return 'application-actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'creator_id'], 'required'],
            [['application_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['application_id', 'creator_id', 'modifier_id'], 'integer'],
            [['params', 'creation_timestamp', 'modification_timestamp'], 'safe'],
            [['description', 'remarks', 'notes'], 'string'],
            [['is_void'], 'boolean'],
            [['module', 'controller', 'action'], 'string', 'max' => 256],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlatformApplication::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'module' => 'Module',
            'controller' => 'Controller',
            'action' => 'Action',
            'params' => 'Params',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
        ];
    }
}
