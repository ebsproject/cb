<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * Model Class for terminal background processes
 */

class TerminalBackgroundProcess extends BaseModel
{
    public function __construct (
        public TerminalTransaction $terminalTransaction
    )
    {

    }
    
    /**
     * Returns plural form of text
     * @param integer count given count
     * @param string text given text
     * @return string plural form of text
     */
    function pluralize($count, $text)
    {
        return $count . (($count == 1) ? (" $text") : (" ${text}s"));
    }
    
    /**
     * Returns a single number of years, months, days, hours, minutes or seconds 
     * between the current date and the provided date
     * @param string $timestamp Given timestamp
     * @return string interval of date
     */
    function ago($timestamp)
    {
        $interval = date_create('now')->diff(new \DateTime($timestamp));
        $suffix = ($interval->invert ? ' ago' : '');
        if ($interval->y >= 1) return $this->pluralize($interval->y, 'year') . $suffix;
        if ($v = $interval->m >= 1) return $this->pluralize($interval->m, 'month') . $suffix;
        if ($v = $interval->d >= 1) return $this->pluralize($interval->d, 'day') . $suffix;
        if ($v = $interval->h >= 1) return $this->pluralize($interval->h, 'hour') . $suffix;
        if ($v = $interval->i >= 1) return $this->pluralize($interval->i, 'minute') . $suffix;
        return $this->pluralize($interval->s, 'second') . $suffix;
    }
    
}