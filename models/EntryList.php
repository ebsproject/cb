<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\interfaces\models\IEntryList;
use Yii;
/**
 * This is the model class for table "experiment.entry_list".
 *
 * @property int $id Identifier of the record within the table
 * @property int $experiment_id Refers to the experiment where the entry belongs to
 * @property string $entry_class Groups related entries together
 * @property string $entry_status Status of the entry in the entry list
 * @property int $product_id Refers to the product of the entry
 * @property int $entlistno Sequence number of the entry in the list
 * @property string $entry_type Role of the entry in the list
 * @property int $author_id Steward of the entry in the entry list
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class EntryList extends BaseModel implements IEntryList{

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'entry-lists';
    }

    /**
     * Returns entry lists from a specific experiment id
     * @param experimentDbId integer entry identifier
    */
    public function getEntryListsSearch($experimentDbId){
        $method = 'POST';
        $path = 'entry-lists-search';
        $param = ["experimentDbId" => "not equals $experimentDbId"];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($param));
        $result = [];
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
          $result = $data['body']['result']['data'];
        }
        return $result;
    }

    /**
     * Retrieve entry list/s given a specific experiment ID
     *
     * @param $experimentId integer experiment record identifier
     * 
     */
    public function getEntryList($experimentId){

        $entryList = [];
        //set api request
        $method = 'POST';
        $path = 'entry-lists-search';
        $rawData = [
            'experimentDbId' => "$experimentId",
        ];

        $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData));

        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){

            $entryList = $results['body']['result']['data'][0];

        }

        return $entryList;
        
    }
    
    /**
     * Return the entry list id of the experiment, create record if does not exist
     *
     * @param Integer $experimentId record id of the experiment
     * @param Integer $toCreate flag for creating entry list
     */
    public function getEntryListId($experimentId, $toCreate=false){
        //search for entry list
        $entryList = EntryList::searchAll(['experimentDbId'=>"equals $experimentId"]);

        if($entryList['totalCount'] == 0 && $toCreate){
            //create entry list
            $experimentRecord = \Yii::$container->get('app\models\Experiment')->getExperiment($experimentId);

            //retrieve configuration
            $experimentModel = \Yii::$container->get('app\modules\experimentCreation\models\ExperimentModel');
            if(!empty($experimentRecord)){
                $configData = $experimentModel->retrieveConfiguration('specify-entry-list', $experimentRecord['programCode'], $experimentId, $experimentRecord['dataProcessDbId']);

                $entryListType = 'entry list';
                foreach($configData as $config){
                    if($config['variable_abbrev'] == 'ENTRY_LIST_TYPE' && isset($config['default'])){
                        $entryListType = $config['default'];
                    }
                }
                
                $dataProcessAbbrev = $experimentRecord['dataProcessAbbrev'];
                
                $entryListName = $experimentRecord['experimentName'];

                $requestData['records'][] = [
                    'experimentDbId' => "$experimentId",
                    'entryListName' => $entryListName,
                    'entryListStatus' => "draft",
                    'entryListType' => "$entryListType",
                    'programDbId' => $experimentRecord['programDbId']."",
                    'cropDbId' => $experimentRecord['cropDbId'].""
                ];
                $entryList = EntryList::create($requestData);
            }

           
        }

        return isset($entryList['data'][0]['entryListDbId']) ? $entryList['data'][0]['entryListDbId'] : 0;
    }

    /**
     * Checks if an entry list name exists in the database (case-sensitive)
     *
     * @param string $name entry list name
     * @param int $exceptId entry list identifier to exclude (useful for checking against oneself)
     * @return bool true if it exists, false otherwise
     */
    public function nameExists($name, $exceptId=null)
    {
        $params = [
            "fields" => "entryList.id AS entryListDbId | entryList.entry_list_name AS entryListName",
            "entryListName" => "equals $name",
        ];
        if (!empty($exceptId)) {
            $params["entryListDbId"] = "not equals $exceptId";
        }

        $result = EntryList::searchAll($params, 'limit=1', retrieveAll: false);

        return ($result['success'] && $result['totalCount'] > 0);
    }

     /**
     * Search for entry list given search parameters
     *
     * @param $params array list of search parameters
     * @return $data array list of entry list given search parameters
     */
    public static function searchRecords($params){

        $params = empty($params) ? null : $params;
        $response = Yii::$app->api->getResponse('POST', 'entry-lists-search', json_encode($params), null, true);

        $data = [];

        // check if successfully retrieved germplasm
        if(($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
          
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
        }
        
        return $data;
    }

}
