<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "experiment.entry_list_data".
 *
 * @property int $id Identifier of the record within the table
 * @property int $entry_list_id Reference to the entry list
 * @property int $variable_id Reference to the variable that describes the value of the entry list data
 * @property string $data_value Value of the entry list data
 * @property string $data_qc_code Status of the entry data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)}
 * @property int $protocol_id Reference to the protocol
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id Reference to the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id Reference to the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property string $event_log Historical transactions of the record
 */
class EntryListData extends BaseModel
{
    public static function apiEndPoint()
    {
        return 'entry-list-data';
    }

    /**
     * Returns a formatted record data used to insert into the entry list data table
     *
     * `dataValue` varies depending on the type of protocol. For trait or management, it contains `platform.list` id.
     *
     * @param int $entryListDbId entry list identifier
     * @param int $variableDbId variable identifier
     * @param string $dataValue value of the entry list data
     * @param int $protocolDbId [optional] protocol identifier
     *
     */
    public static function toRecord($entryListDbId, $variableDbId, $dataValue, $protocolDbId=null)
    {
        $record = [
            'entryListDbId' => "$entryListDbId",
            'variableDbId' => "$variableDbId",
            'dataValue' => "$dataValue",
        ];
        if (!is_null($protocolDbId)) {
            $record['protocolDbId'] = "$protocolDbId";
        }

        return $record;
    }
}