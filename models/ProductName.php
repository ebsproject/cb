<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\dataproviders\ArrayDataProvider;

class ProductName extends ProductNameBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.product_name';
    }

    /**
     * Retrieves list of product names
     *
     * @param $id integer product identifier
     *
     * @return arraydataprovider object list of product names
     */
    public static function getListOfProductNames($id){
        $sql = "
            with t as (
                select gid from master.product_gid where product_id = {$id} and is_void = false
            )
            select
            n.gid,n.nval as name, u.fname as name_type,
            case
                when g.gdate != 0 then to_date(g.gdate::text, 'YYYYMMDD')
                else null end as date,
            l.lname as location
            from
                t,
                gms.germplsm g

                left join
                    gms.location l
                on
                    locid = g.glocn,
                gms.udflds u,
                gms.names n
            where
                n.gid = t.gid
                and g.gid = n.gid
                and n.nstat = 1
                and u.fldno = n.ntype
            order by n.gid asc
        ";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => false
        ]);
    }
}