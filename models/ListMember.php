<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\IListMember;

/**
 * This is the model class for table "platform.list_member".
 *
 * @property int $id Identifier of the record within the table
 * @property int $list_id ID of the list. References to platform.list
 * @property int|null $data_id Data ID of the entity. If list type is study, data_id will refer to operational.study.id
 * @property int $order_number Order number of the member in the list
 * @property string $display_value Display value of the item member in list. Can be designation, study_name, or plot code
 * @property string|null $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string|null $modification_timestamp Timestamp when the record was last modified
 * @property int|null $modifier_id ID of the user who last modified the record
 * @property string|null $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property string|null $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 * @property string|null $search_input
 * @property bool|null $is_active
 */
class ListMember extends BaseModel implements IListMember{

    public static function apiEndPoint() {
        return 'list-members';
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param string $type list type
     * @param integer $id list ID
     * @param boolean $is_void if list items should be voided or not
     * @param array $params contains the different filter options
     * @param boolean $pagination whether the pagination of the browser should be set
     * @param boolean $isView whether the intended target for the dataprovider is the modal view or not
     * @param string $searchModel the name of the search model that is used for filtering
     *
     * @return ArrayDataProvider
     */
    public function search(string $type=null, $id, $isVoid=null, $params=null, $pagination=false, $isView=false, $searchModel='List') {
        $this->load($params);

        /** get current page number and page size */
        $paramPage = '';
        $paramLimit = (!$isView ? 'limit=10' : '');
        $dataProviderId = $isVoid==null ? 'dp-1' : ($isVoid == 'false' ? 'dp-3' : 'dp-2');

        if (isset($_GET[$dataProviderId . '-page'])){
            $paramPage = '&page=' . $_GET[$dataProviderId . '-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
        }

        $cookies = Yii::$app->request->cookies;
        $dataProviderId = isset($isVoid) ? ($isVoid == 'false' ? 'all-items-grid_' : 'final-grid_') : 'preview-grid_';
        
        if(isset($cookies[$dataProviderId])) {  // Get page size from cookies of dynagrid
            $paramLimit = $cookies->getValue($dataProviderId);
            if (!empty($paramLimit)) {
                $paramLimit = json_decode($paramLimit, true);
                $paramLimit = json_decode($paramLimit['grid'], true);
                $paramLimit = $paramLimit['page'];
                $paramLimit = 'limit=' . $paramLimit;
            }
        }

        $paramSort = 'sort=orderNumber';
        if(isset($_GET['sort']) && !empty($_GET['sort'])){
            $currSort = $_GET['sort'];
            $currSort = explode('|',$currSort);

            $finalSort = '';
            foreach($currSort as $key => $value){
                if(strpos($value,'-') !== false){
                    $value = trim(str_replace('-','',$value));
                    $value = $value . ':desc';

                    $currSort[$key] = $value;
                }
            }
            $finalSort = !empty($currSort) ? 'sort=' . implode('|',$currSort) : '';

            if(!empty($finalSort)) $paramSort = $finalSort;
        }

        if($paramLimit == []) $paramLimit = 'limit=10';

        $param = [];
        $url = !$isView ? 'lists/'.$id.'/members-search?'.$paramSort.'&'.$paramLimit.$paramPage : 'lists/'.$id.'/members-search?'.$paramSort;

        if (!is_null($isVoid)) {
            $param['isActive'] = $isVoid;
        }

        // parse filters
        if (!is_string($searchModel)) {
            throw new \yii\web\HttpException(500,"Please provide only the string name of the class!");
        }

        // check if advanced filters are set
        if (isset($_GET['Advance'])) {
            $advancedFilters = $_GET['Advance'];
            if (isset($advancedFilters['min']) && isset($advancedFilters['max'])) {
                $param['volume'] = [
                    "range" => $advancedFilters['min'] . "|" . $advancedFilters['max']
                ];
            }
            if (isset($advancedFilters['study']) && !empty($advancedFilters['study'])) {
                $studiesStr = str_replace(',', '|', $advancedFilters['study']);
                $param['study'] = $studiesStr;
            }
            if (isset($advanceFilters['year']) && !empty($advanceFilters['year'])) {
                $yearStr = str_replace(',', '|', $advancedFilters['year']);
                $param['year'] = $yearStr;
            }
            if (isset($advanceFilters['container_label']) && !empty($advanceFilters['container_label'])) {
                $labelStr = str_replace(',', '|', $advancedFilters['container_label']);
                $param['container_label'] = $labelStr;
            }
            if (isset($advanceFilters['studyName']) && !empty($advanceFilters['studyName'])) {
                $studyNamesStr = str_replace(',', '|', $advancedFilters['studyName']);
                $param['studyName'] = $studyNamesStr;
            }

            if (isset($advanceFilters['product_name']) && !empty($advanceFilters['product_name'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }

            if (isset($advanceFilters['entcode']) && !empty($advanceFilters['entcode'])) {
                $productNameStr = str_replace(',', '|', $advancedFilters['product_name']);
                $param['product_name'] = $productNameStr;
            }
        }

        $filters = isset($params[$searchModel]) ? $params[$searchModel]: null;

        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if (!empty($value)){
                    $param[$key] = $value . "%";
                }

                if ($key === 'quantity' && $value === '0'){
                    $param[$key] = $value;
                }
            }
        }

        if (empty($param)) {
            $param = '';
        }
    
        $data = Yii::$app->api->getResponse('POST',$url,json_encode($param));

        // get lists data provider
        $result = [];
        $totalCount = 0;
        if(isset($data) && !empty($data)){
            if (isset($data['status']) && $data['status'] == 200) {
                $data = $data['body'];
                $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
                $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
                $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                // build data provider
                // if isView is true, it'll only get the 1st batch of items,
                // but if not then it should continue to retrieve the rest of the items
                if($totalPages > 1 && !$isView && $paramLimit == ''){       
                    for ($i=2; $i <= $totalPages; $i++) {
                        $data = Yii::$app->api->getResponse('POST',$url.'&page='.$i, json_encode($param));                
                        if($data['status'] == 200) {
                            $data = $data['body'];
                            $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                        }
    
                        $result = array_merge($result,$addlResult);
                    }
                }
            }
        }

        $options = [
            'key' => 'listMemberDbId',
            'allModels' => $result,
            'totalCount' => $totalCount,
            'restified' => true,
            'pagination' => [ 'pageSize' => 10 ]
        ];

        return new ArrayDataProvider($options);

    }

    // required function for ManageSortsWidget implementation
    public function getColumnNameAndDatatypes() {
        $columnValues = Yii::$app->session->get("list_members-columns");
        return $columnValues;
    }
}
