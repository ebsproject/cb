<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.project".
 *
 * @property int $id Identifier of the record within the table
 * @property int $program_id Refers to the program of the project
 * @property string $abbrev Unique short name of the project
 * @property string $name Unique name of the project
 * @property string $description Description about the project
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ProjectBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['program_id', 'abbrev', 'name', 'creator_id'], 'required'],
            [['program_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['program_id', 'creator_id', 'modifier_id'], 'integer'],
            [['description', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['abbrev'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 128],
            [['abbrev'], 'unique'],
            [['name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
        ];
    }
}