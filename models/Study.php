<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;
use Yii;
use ChromePhp;
use app\models\BaseModel;
use app\dataproviders\ArrayDataProvider;
use app\models\User;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * This is the model class for table "operational.study".
 */
class Study extends StudyBase
{   
    public function formatStudyStatus($studyStatus, $isHeader=false) {
        $statuses = array_map('trim', explode(';', strtoupper($studyStatus)));
        
        $formattedStudyStatus = '';
        
        if (count($statuses) > 1) {
            $statuses = array_diff($statuses, ['CREATED']);
        }
        
        foreach ($statuses as $status) {
            switch ($status) {
                case 'CREATED':
                case 'DESIGN REQUESTED':
                case 'BACKGROUND JOB IN PROGRESS':
                case 'RANDOMIZATION IN PROGRESS':
                    $class = 'orange';
                    break;

                case 'ENTRY LIST READY':
                case 'RANDOMIZED':
                case 'LAYOUT UPLOADED':
                case 'SEED LOTS READY':
                case 'FIELD LAYOUT CREATED':
                case 'PLOTS CREATED':
                    $class = 'blue';
                    break;

                case 'ACTIVE':
                case 'STUDY CREATION':
                case 'FIELD LAYOUT FINALIZED':
                case 'ENTRY LIST CREATED':
                case 'COMMITTED STUDY':
                    $class = '';
                    break;

                case 'INCOMPLETE':
                case 'INCOMPLETE SEED LOT DATA';
                case 'NO SEED LOTS SPECIFIED';
                case 'BACKGROUND JOB ERROR';
                    $class = 'red';
                    break;

                default:
                    $class = 'grey';
            }

            if ($isHeader) {
                $formattedStudyStatus .= '<span class="new badge ' . $class . '" style="margin: 1px"><h3>' . $status . '</h3></span>';
            } else {
                $formattedStudyStatus .= '<span class="new badge ' . $class . '" style="margin: 1px">' . $status . '</span>';
            }
        }

        return $formattedStudyStatus;
    }

    /**
     * Get list of studies basic information by study ids
     * @param $studyIds array list of study identifier
     * @param $count integer total study count
     *
     * @return $result array list of stdy basic info
     */
    public static function getStudyListByIds($studyIds,$count){
        $studies = [];
        $studyIds = substr($studyIds, 1, -1);
        $studyIdsArr = str_replace(',', '|', $studyIds);

        $studyDbIdParam = [
            "studyDbId" => $studyIdsArr
        ];

        $data = Yii::$app->api->getResponse('POST','studies-search',json_encode($studyDbIdParam));
        
        // check if there are results retrieved
        if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $studies = $data['body']['result']['data'];
        }

        return new ArrayDataProvider([
            'key'=>'studyDbId',
            'allModels' => $studies,
            'pagination' => false
        ]);


    }

    /**
     * Use HTTP requests to GET, PUT, POST and DELETE data
     * 
     * @param method string request method(POST, GET, PUT, etc)
     * @param path string url path request
     * @param rawData json optional request content
     * 
     * @return array list of requested information 
     */
    public static function getApiResponse($method, $path, $rawData = null){

        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

        try {

            $client = new \GuzzleHttp\Client([
                'base_uri' => getenv('CB_API_V3_URL'), 
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Content-Type' => 'application/json'
                ]
            ]);

            $response = $client->request($method, $path, ['body' => $rawData]);

         }
         catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
         }

        return json_decode($response->getBody()->getContents(),true);
          
    }
    
    /**
     * Retrieve variable information 
     * 
     * @param id integer variable identifier
     * 
     * @return varData array list of variable information
     */
    public static function getVariableData($id){
        
        $apiResponse = Study::getApiResponse('GET', "variables/$id");
        $varData = null;
        if(isset($apiResponse['result']['data'][0]['variableDbId']) && $apiResponse['result']['data'][0]['variableDbId'] == $id){
            $varData = $apiResponse['result']['data'][0];
        }

        return $varData;
    }

}