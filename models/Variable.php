<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use yii\data\ArrayDataProvider;
use app\interfaces\models\IVariable;
use Yii;
use ChromePhp;

/**
 * This is the model class for table "master.variable".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $label
 * @property string $name
 * @property string $data_type
 * @property boolean $not_null
 * @property string $type
 * @property string $status
 * @property string $display_name
 * @property string $ontology_reference
 * @property string $bibliographical_reference
 * @property integer $property_id
 * @property integer $method_id
 * @property integer $scale_id
 * @property string $variable_set
 * @property string $synonym
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property string $description
 * @property string $default_value
 * @property string $usage
 * @property string $data_level
 * @property boolean $is_column
 * @property string $column_table
 * @property boolean $is_computed
 * @property string $member_data_type
 * @property integer $target_variable_id
 * @property string $field_prop
 * @property integer $member_variable_id
 * @property string $target_model
 * @property integer $class_variable_id
 * @property string $json_type
 * @property string $notif
 */
class Variable extends BaseModel implements IVariable
{
    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'variables';
    }
 
    /**
     * Checks if a string(haystack) starts with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function startsWith($haystack, $needle) {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }
    

    /**
     * Checks if a string(haystack) ends with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function endsWith($haystack, $needle) {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abbrev', 'label', 'name', 'data_type', 'type', 'status', 'display_name', 'usage'], 'required'],
            [['data_type', 'display_name', 'ontology_reference', 'bibliographical_reference', 'variable_set', 'synonym', 'remarks', 'notes', 'description', 'column_table', 'field_prop', 'target_model', 'json_type', 'notif'], 'string'],
            [['not_null', 'is_void', 'is_column', 'is_computed'], 'boolean'],
            [['property_id', 'method_id', 'scale_id', 'creator_id', 'modifier_id', 'target_variable_id', 'member_variable_id', 'class_variable_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['abbrev', 'label'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 256],
            [['type', 'status', 'data_level'], 'string', 'max' => 32],
            [['default_value'], 'string', 'max' => 255],
            [['usage'], 'string', 'max' => 16],
            [['member_data_type'], 'string', 'max' => 64],
            [['abbrev'], 'unique'],
            [['method_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterMethod::className(), 'targetAttribute' => ['method_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProperty::className(), 'targetAttribute' => ['property_id' => 'id']],
            [['scale_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterScale::className(), 'targetAttribute' => ['scale_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
            [['target_variable_id'], 'exist', 'skipOnError' => true, 'targetClass' => VariableBase::className(), 'targetAttribute' => ['target_variable_id' => 'id']],
            [['member_variable_id'], 'exist', 'skipOnError' => true, 'targetClass' => VariableBase::className(), 'targetAttribute' => ['member_variable_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'label' => 'Label',
            'name' => 'Name',
            'data_type' => 'Data Type',
            'not_null' => 'Not Null',
            'type' => 'Type',
            'status' => 'Status',
            'display_name' => 'Display Name',
            'ontology_reference' => 'Ontology Reference',
            'bibliographical_reference' => 'Bibliographical Reference',
            'property_id' => 'Property ID',
            'method_id' => 'Method ID',
            'scale_id' => 'Scale ID',
            'variable_set' => 'Variable Set',
            'synonym' => 'Synonym',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'description' => 'Description',
            'default_value' => 'Default Value',
            'usage' => 'Usage',
            'data_level' => 'Data Level',
            'is_column' => 'Is Column',
            'column_table' => 'Column Table',
            'is_computed' => 'Is Computed',
            'member_data_type' => 'Member Data Type',
            'target_variable_id' => 'Target Variable ID',
            'field_prop' => 'Field Prop',
            'member_variable_id' => 'Member Variable ID',
            'target_model' => 'Target Model',
            'class_variable_id' => 'Class Variable ID',
            'json_type' => 'Json Type',
            'notif' => 'Notif',
        ];
    }

    /**
     * Retrieve variable information in view variable
     *
     * @param $id integer variable identifier
     * @return $data array variable information
     */
    public function getVariableInfo($id){
        $general = [];
        $property = [];
        $method = [];
        $scale = ['basic'=>[], 'values'=>new ArrayDataProvider()];
        $scaleValues = [];
        $auditColsArr = ['description','remarks','creator','creationTimestamp','modifier','modificationTimestamp'];
        
        // general and property
        $data = Yii::$app->api->getResponse('GET', "variables/$id/properties");
        if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
            $generalObj = $data['body']['result']['data'][0];
            $generalOrder = [
                'abbrev','label','name','displayName','dataType','type','usage','dataLevel','status',
                'isComputed','ontologyReference','bibliographicalReference','synonym','notNull',
            ];
            $generalOrderCols = array_merge($generalOrder, $auditColsArr);
            $general = Variable::processOrderOfData($generalObj, $generalOrderCols);

            // if there are properties assigned
            if(isset($generalObj['properties'][0]) && !empty($generalObj['properties'][0])){
                $propertyObj = $generalObj['properties'][0];
                $propertyOrder = ['abbrev','name','displayName'];
                $propertyOrderCols = array_merge($propertyOrder, $auditColsArr);

                $property = Variable::processOrderOfData($propertyObj, $propertyOrderCols);

            }
        }

        // method
        $methodData = Yii::$app->api->getResponse('GET', "variables/$id/methods");
        if(isset($methodData['status']) && $methodData['status'] == 200 && isset($methodData['body']['result']['data'][0]['methods'][0])){
            $methodObj = $methodData['body']['result']['data'][0]['methods'][0];
            $methodObj['description'] = isset($generalObj['methodDescription']) ? $generalObj['methodDescription'] : null;
            $methodObj['name'] = isset($generalObj['methodName']) ? $generalObj['methodName'] : null;
            $methodOrder = ['name'];
            $methodOrderCols = array_merge($methodOrder,$auditColsArr);

            $method = Variable::processOrderOfData($methodObj, $methodOrderCols);
        }

        // scale
        $scaleData = Yii::$app->api->getResponse('GET', "variables/$id/scales");

        if(isset($scaleData['status']) && $scaleData['status'] == 200 && isset($scaleData['body']['result']['data'][0]['scales'][0])){
            $scalesObj = $scaleData['body']['result']['data'][0]['scales'][0];

            $scaleOrder = ['type', 'unit', 'level', 'minValue', 'maxValue'];
            $scaleOrderCols = array_merge($scaleOrder,$auditColsArr);
            $scaleBasic = Variable::processOrderOfData($scalesObj, $scaleOrderCols);

            $scaleValuesRaw = isset($scalesObj['scaleValues']) && !empty($scalesObj['scaleValues']) ? $scalesObj['scaleValues'] : [];
            $scaleValues = Variable::arraySortByColumn($scaleValuesRaw, 'orderNumber');

            $scale['basic'] = $scaleBasic;

            // scale values data provider
            $dataProvider = new ArrayDataProvider([
                'key'=>'value',
                'pagination' => false,
                'allModels' => $scaleValues,
                'sort' => [
                    'attributes' => ['orderNumber','value','description','displayName'],
                    'defaultOrder' => [
                        'orderNumber'=>SORT_ASC
                    ]
                ],
            ]);

            $scale['values'] = $dataProvider;

        }

        return [
            'general' => $general,
            'property' => $property,
            'method' => $method,
            'scale' => $scale,
        ];

    }

    /**
     * Sort array by value
     *
     * @param $arr array array to be sorted
     * @param $col text column to be sorted
     * @param $dir text directing of sorting
     */
    public function arraySortByColumn($arr, $col, $dir = SORT_ASC) {
        $sortCol = array();

        foreach ($arr as $key=> $row) {
            $sortCol[$key] = $row;
        }

        array_multisort($sortCol, $dir, $arr);

        return $sortCol;
    }

    /**
     * Process ordering of data to be displayed
     *
     * @param $arr list of attributes
     * @param $order order of display of attributes
     * @return $array ordered list of attributes
     */
    public function processOrderOfData($arr, $order){
        $count = count($order);
        $array = [];
        
        for($i = 0; $i < $count; $i++){
            if(isset($order[$i])){

                $val = $arr[$order[$i]];
                if ($order[$i] == 'notNull') {
                    $val = 'false';
                } else if ($order[$i] == 'isComputed') {
                    $val = ($val) ? 'TRUE' : 'FALSE';
                } else if (strpos($order[$i], 'Time') !== false) { // for timestamps
                    $val = date('M d, Y', strtotime($arr[$order[$i]]));
                }
                $array[] = ['label'=>$order[$i], 'value' => $val];
            }
        }

        return $array;
    }

    /**
     * Get variable label by abbrev
     *
     * @param $abbrev text variable abbreviation
     */
    public function getAttrByAbbrev($abbrev,$attr='label'){
        $sql = "select {$attr} from master.variable where abbrev = '{$abbrev}' limit 1";

        $result = Yii::$app->db->createCommand($sql)->queryColumn();

        return $result[0];
    }

    /**
     * Get variable info by abbrev
     *
     * @param $abbrev text variable abbrev
     * @return $data array variable value
     */
    public function getVariableByAbbrev($abbrev, $fields = []){
        
        $result = [];
        $isArray = is_array($abbrev);
        
        if($isArray){
            $abbrev = str_replace("'", '', implode("|equals ", $abbrev));
        }

        //set api request body
        $method = 'POST';
        $path = 'variables-search';
        $rawData = array_merge(["abbrev" => "equals $abbrev"], $fields);
        
        $data = Yii::$app->api->getResponse($method, $path, json_encode($rawData), null, true);
        
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
            $result = $isArray ? $data['body']['result']['data'] : $data['body']['result']['data'][0];
        }

        return $result;
    }

    /**
     * Retrive variable scales
     * 
     * @param $variableId varaible record identifier
     */
    public function getVariableScales($variableId){
        
        $result = [];
        
        //set api request body
        $method = 'GET';
        $path = "variables/$variableId/scales";
        $rawData = [];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($rawData));
        
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0]['scales'][0])){
            $result = $data['body']['result']['data'][0]['scales'][0];
        }

        return $result;
    }

    /**
     * Retrive tags for variable scale value - scale name pair
     * 
     * @param $scaleValues array list of scale values info
     */
    public function getVariableScaleTags($scaleValues){

        $result = [];

        if(isset($scaleValues[0]['value'])){
            foreach($scaleValues as $scaleValue){
                if((isset($scaleValue['scaleValueStatus']) && ($scaleValue['scaleValueStatus'] === 'show') || $scaleValue['scaleValueStatus'] === null)){
                    $result = array_merge([$scaleValue['value'] => $scaleValue['displayName']], $result);
                }
            }
        }

        return $result;
    }

    /**
     * Method for retrieving all the scale value records of a variable using the POST search method
     * @param Integer $variableDbId Variable identifier
     * @param Array $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllVariableScales($variableDbId, $params = null, $filters = '', $retrieveAll = true)
    {
        $method = 'POST';
        $path = 'variables/' . $variableDbId . '/scale-values-search';

        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, $retrieveAll);

    }

    /**
     * Get variable attribute by abbrev
     *
     * @param text $abbrev abbreviation of the variable
     * @param text $attribute attribute to be retrieved
     *
     * @return text variable attribute value
     */
    public function getAttributeByAbbrev($abbrev, $attribute){

        $method = 'POST';
        $path = 'variables-search';
        $varAbbrev = strtoupper($abbrev);
        $params['abbrev'] = "equals $varAbbrev";

        $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($params), 'limit=1', false);

        if(
            isset($response['data']) && 
            !empty($response['data']) && 
            isset($response['data'][0][$attribute])
        )
            return ucfirst(strtolower($response['data'][0][$attribute]));

        return null;
    }
}
