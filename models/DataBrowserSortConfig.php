<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.user_data_browser_configuration".
 *
 * @property int $id Primary key of the record in the table
 * @property int $user_id Id of the user
 * @property string $name Name identifier of the service
 * @property json $data Sort configuration
 * @property string $data_browser_id Name identifier of the data browser
 * @property string type Type of configuration (filter or sort)
 */

class DataBrowserSortConfig extends BaseModel
{
    public $name;
    public $type;
    public $data;
    public $dataBrowserId;

	/**
	 * Get table name for class
	 *
	 * @return String table name
	 */
	public static function tableName()
	{
		return 'platform.user_data_browser_configuration';
	}


    /**
     * Get the sort configurations for a given user
     *
     * @return Array $result Array of sort configurations
     */
    public function getSorts($dataBrowserId) {
        $params = '?type=sort&dataBrowserDbId='.$dataBrowserId;
        $data = Yii::$app->api->getResponse('GET', 'data-browser-configurations'.$params);

        return $data['body']['result']['data'];
    }

    /**
     * Save the sort configuration for a given user
     * @param String $name Name of sort configuration
     * @param json $data Sort configuration
     * @param String $dataBrowserId Name of data browser where sort configuration is found
     *
     * @return Boolean $result Indicates if deletion is successful
     */
    public function saveSort($name, $data, $dataBrowserId) {
        $request = [
            "records"=>[
                [
                    "name" => $name,
                    "type" => "sort",
                    "data" => $data,
                    "dataBrowserDbId" => $dataBrowserId
                ]
            ]
        ];

        $data = Yii::$app->api->getResponse('POST', 'data-browser-configurations', json_encode($request));

        return ($data['status'] == 200);
    }

    /**
     * Delete the sort configuration of a given user
     * @param Integer $sortId Data browser sort configuration identifier
     *
     * @return Boolean $result Indicates if deletion is successful
     */
    public function deleteSort($sortId) {
        $data = Yii::$app->api->getResponse('DELETE', 'data-browser-configurations/'.$sortId);

        return ($data['status'] == 200);

    }

}