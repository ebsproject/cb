<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Variable;

/**
 * VariableSearch represents the model behind the search form of `app\models\Variable`.
 */
class VariableSearch extends Variable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'property_id', 'method_id', 'scale_id', 'creator_id', 'modifier_id', 'target_variable_id', 'member_variable_id', 'class_variable_id'], 'integer'],
            [['abbrev', 'label', 'name', 'data_type', 'type', 'status', 'display_name', 'ontology_reference', 'bibliographical_reference', 'variable_set', 'synonym', 'remarks', 'creation_timestamp', 'modification_timestamp', 'notes', 'description', 'default_value', 'usage', 'data_level', 'column_table', 'member_data_type', 'field_prop', 'target_model', 'json_type', 'notif'], 'safe'],
            [['not_null', 'is_void', 'is_column', 'is_computed'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Variable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'not_null' => $this->not_null,
            'property_id' => $this->property_id,
            'method_id' => $this->method_id,
            'scale_id' => $this->scale_id,
            'creation_timestamp' => $this->creation_timestamp,
            'creator_id' => $this->creator_id,
            'modification_timestamp' => $this->modification_timestamp,
            'modifier_id' => $this->modifier_id,
            'is_void' => $this->is_void,
            'is_column' => $this->is_column,
            'is_computed' => $this->is_computed,
            'target_variable_id' => $this->target_variable_id,
            'member_variable_id' => $this->member_variable_id,
            'class_variable_id' => $this->class_variable_id,
        ]);

        $query->andFilterWhere(['ilike', 'abbrev', $this->abbrev])
            ->andFilterWhere(['ilike', 'label', $this->label])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'data_type', $this->data_type])
            ->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'display_name', $this->display_name])
            ->andFilterWhere(['ilike', 'ontology_reference', $this->ontology_reference])
            ->andFilterWhere(['ilike', 'bibliographical_reference', $this->bibliographical_reference])
            ->andFilterWhere(['ilike', 'variable_set', $this->variable_set])
            ->andFilterWhere(['ilike', 'synonym', $this->synonym])
            ->andFilterWhere(['ilike', 'remarks', $this->remarks])
            ->andFilterWhere(['ilike', 'notes', $this->notes])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'default_value', $this->default_value])
            ->andFilterWhere(['ilike', 'usage', $this->usage])
            ->andFilterWhere(['ilike', 'data_level', $this->data_level])
            ->andFilterWhere(['ilike', 'column_table', $this->column_table])
            ->andFilterWhere(['ilike', 'member_data_type', $this->member_data_type])
            ->andFilterWhere(['ilike', 'field_prop', $this->field_prop])
            ->andFilterWhere(['ilike', 'target_model', $this->target_model])
            ->andFilterWhere(['ilike', 'json_type', $this->json_type])
            ->andFilterWhere(['ilike', 'notif', $this->notif]);

        return $dataProvider;
    }
}
