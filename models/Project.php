<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.project".
 *
 * @property int $id Identifier of the record within the table
 * @property int $program_id Refers to the program of the project
 * @property string $abbrev Unique short name of the project
 * @property string $name Unique name of the project
 * @property string $description Description about the project
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class Project extends ProjectBase
{
   /**
     * Retrieves all existing projects in the database
     * 
     * @param $param array condition parameters
     * @return array projects record in the database
     */
    public static function getProjects($param){
      $method = 'POST';
      $path = 'projects-search';
      $filter = 'sort=abbrev:ASC';

      $data = Yii::$app->api->getResponse($method, $path, json_encode($param), $filter, true);
      $result = [];
       
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
        $result = $data['body']['result']['data'];
      }
      return $result;
    }

    /**
     * Retrieves an existing project in the database
     * 
     * @param $id integer project identifier
     * @return array project record in the database
     */
    public static function getProject($id){
      $method = 'GET';
      $path = 'projects'.'/'.$id;

      $data = Yii::$app->api->getResponse($method, $path);
      $result = [];
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        $result = $data['body']['result']['data'][0];
      }
      return $result;
    }
}