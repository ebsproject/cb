<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use yii\data\ArrayDataProvider;
use ChromePhp;

/**
 * This is the ActiveQuery class for [[UserSearch]].
 *
 * @see UserSearch
 */
class UserSearch extends User
{
    public $firstName;
    public $lastName;
    public $personName;
    public $personType;
    public $isActive;
    public $creator;
    public $modifier;
    public $creationTimestamp;
    public $modificationTimestamp;

    /**
     * Search model for user
     */
    public function search($params)
    {
        $this->load($params);

        foreach ($params['UserSearch'] as $key => $value) {
            // for personType: Set values to upper case
            if ($key =='personType' && $value != "") {
                $personType = strtoupper($params['UserSearch'][$key]);
                $params['UserSearch'][$key] = $personType;
            }
            
            // for isActive: Convert '1' to 'true' or '0' to 'false' for isActive
            if ($key == 'isActive') $params['UserSearch'][$key] = ($value == 1) ? 'true' : 'false';
        }

        return new ArrayDataProvider([
            'allModels' => User::searchUsers($params['UserSearch']),
            'key' => 'personDbId',
            'sort' => [
                'attributes' => [
                    'personDbId',
                    'email',
                    'username',
                    'firstName',
                    'lastName',
                    'personName',
                    'personType',
                    'isActive',
                    'creator',
                    'modifier',
                    'creationTimestamp',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'personDbId' => SORT_DESC
                ]
            ],
            'id' => 'user-data-provider'
        ]);
    }
}
