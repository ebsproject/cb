<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;
use yii\helpers\StringHelper;
use app\interfaces\models\IBaseModel;

/**
 * Model for defining all basic classes among B4R models
 */

class BaseModel extends \yii\base\Model implements IBaseModel {


    /**
     * Retrieves the corresponding api endpoint for a class
     * Any class that extends this model should define the API endpoint if it does not fit the default definition
     * Default is converting the class name into lower case
     * eg, Lists becomes lists
     */
    public static function apiEndPoint() {
        return strtolower(StringHelper::basename(get_called_class()));
    }

    /**
     * Retrieves the corresponding api search endpoint for a class
     * Any class that extends this model should define the API search endpoint if it does not fit the default definition
     * Default is apiEndpoint + -search
     * eg, lists becomes lists-search
     */
    public static function searchEndPoint() {
        return static::apiEndPoint() . '-search';
    }

    /**
     * Method for retrieving a record given its database ID
     * @param Integer $dbId the corresponding database ID
     * @return Array the record from the database
     */
    public function getOne($dbId) {
        $apiEndPoint = static::apiEndPoint();
        $method = 'GET';
        $path = $apiEndPoint . '/' . $dbId;

        $response = Yii::$app->api->getParsedResponse($method, $path);

        if (isset($response['data'][0])) {
            $response['data'] = $response['data'][0];
        }

        return $response;
    }

    /**
     * Method for retrieving all the records from a given endpoint using the GET method
     * @param String $filters specifies how the records should be filtered
     * @return Array the records from the database
     */
    public function getAll($filters='', $retrieveAll=true) {

        $apiEndPoint = static::apiEndPoint();
        $method = 'GET';
        $path = $apiEndPoint;

        return Yii::$app->api->getParsedResponse($method, $path, null, $filters, $retrieveAll);
    }

    /**
     * Method for updating a single record in the database
     * @param Integer $dbId the database Id of the record to be updated
     * @param Array $requestData contains the data to be updated
     * @return Array API response
     */
    public function updateOne($dbId, $requestData) {
        
        $apiEndPoint = static::apiEndPoint();
        $method = 'PUT';
        $path = $apiEndPoint . '/' . $dbId;

        return Yii::$app->api->getParsedResponse($method, $path, json_encode($requestData));


    }

    /**
     * Method for updating multiple records in the database
     * @param Array $dbIdsArray the database Ids of the records to be updated
     * @param Array $requestData contains the data to be updated
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @return Array API response
     */
    public function updateMany(array $dbIdsArray=null, $requestData, $backgroundProcess=false, $additionalParams=[]) {

        $apiEndPoint = static::apiEndPoint();
        if($dbIdsArray == null){
            $params = [
                "httpMethod" => "PUT",
                "endpoint" => 'v3/'.$apiEndPoint,
                "requestData" => $requestData
            ];
        } else{
            $params = [
                "httpMethod" => "PUT",
                "endpoint" => 'v3/'.$apiEndPoint,
                "dbIds" => implode("|", $dbIdsArray),
                "requestData" => $requestData
            ];
        }
        $method = 'POST';
        $path = 'broker';

        if ($backgroundProcess) {
            $path = 'processor';

            // Get token and refresh token from session
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            $params["tokenObject"] = [
                "token" => $token,
                "refreshToken" => $refreshToken
            ];

            $params = array_merge($params, $additionalParams);
        }
        
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params));


    }

    /**
     * Method for deleting a single record in the database
     * @param Integer $dbId the database Id of the record to be deleted
     * @return Array API response
     */
    public function deleteOne($dbId) {

        $apiEndPoint = static::apiEndPoint();
        $method = 'DELETE';
        $path = $apiEndPoint . '/' . $dbId;

        return Yii::$app->api->getParsedResponse($method, $path);


    }

    /**
     * Method for deleting multiple records in the database
     * @param Array $dbIdsArray the database Ids of the records to be deleted
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @return Array API response
     */
    public function deleteMany($dbIdsArray, $backgroundProcess=false, $additionalParams=[]) {

        $apiEndPoint = static::apiEndPoint();
        
        if (is_array($dbIdsArray)) {
            $dbIds = implode("|", $dbIdsArray);
        } else {
            $dbIds = $dbIdsArray;
        }

        $params = [
            "httpMethod" => "DELETE",
            "endpoint" => 'v3/'.$apiEndPoint,
            "dbIds" => $dbIds
        ];
        $method = 'POST';
        $path = 'broker';

        if ($backgroundProcess) {
            $path = 'processor';

            // Get token and refresh token from session
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            $params["tokenObject"] = [
                "token" => $token,
                "refreshToken" => $refreshToken
            ];

            $params = array_merge($params, $additionalParams);
        }

        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params));

    
    }

    /**
     * Method for creating one or more records in the database
     * @param Array $requestData contains the information about the records
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @param String $urlParams optional, url params for the request
     * @return Array API response
     */
    public function create($requestData, $backgroundProcess=false, $additionalParams=[], $urlParams='') {

        $apiEndPoint = static::apiEndPoint();
        $method = 'POST';
        $path = $apiEndPoint;
        $params = $requestData;

        // Append urlParams to path (if any)
        if($urlParams != '') {
            if (!str_contains($urlParams, '?')) $path .= '?';
            $path .= $urlParams;
        }

        if ($backgroundProcess) {
            $path = 'processor';
            // Get token and refresh token from session
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');
            $params = [
                "httpMethod" => "POST",
                "endpoint" => 'v3/'.$apiEndPoint,
                "requestData" => $requestData,
                "tokenObject" => [
                    "token" => $token,
                    "refreshToken" => $refreshToken
                ]
            ];

            $params = array_merge($params, $additionalParams);
        }
        
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params));
        
        
    }
    
    /**
     * Method for retrieving all the records from a given endpoint using the POST search method
     * @param Json $params optional request content
     * @param String $filters optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAll($params=null, $filters='', $retrieveAll=true, $escapeSlashes=false) {

        $searchEndPoint = static::searchEndPoint();
        $method = 'POST';
        $path = $searchEndPoint;

        if ($escapeSlashes) {
            $requestBody = json_encode($params,JSON_UNESCAPED_SLASHES);
            $requestBody = str_replace("\\","\\\\",$requestBody);
        } else {
            $requestBody = json_encode($params);
        }

        return Yii::$app->api->getParsedResponse($method, $path, $requestBody, $filters, $retrieveAll);


    }
}