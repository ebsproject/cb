<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * Model class for terminal transaction file. table: data_terminal.transaction_file
 */

class TerminalTransactionFile extends \yii\base\Model
{

    /**
     * Retrieves the data for a give transaction file ID
     * @param Integer $transactionFileDbId ID of the file in the transaction file table
     * @return Array contains the data in the file and the total count
     */
    public static function getData($transactionFileDbId)
    {
        $param = '';
        $data = Yii::$app->api->getResponse('GET', 'transaction-files/' . $transactionFileDbId, json_encode($param), '', true);

        if ($data['status'] == 200) {
            $data = $data['body'];
            $data = (isset($data['result']['data'])) ? $data['result']['data'] : [];
            $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
        } else {
            $data = [];
            $totalCount = 0;
        }

        return [
            'data' => $data,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Retrieves the cross data for a give transaction file ID
     * @param Integer $transactionFileDbId ID of the file in the transaction file table
     * @return Array contains the data in the file and the total count
     */
    public static function getCrossData($transactionFileDbId)
    {
        $param = '';
        $data = Yii::$app->api->getResponse('GET', 'transaction-files/' . $transactionFileDbId . '/cross-data', json_encode($param), '', true);

        if ($data['status'] == 200) {
            $data = $data['body'];
            $data = (isset($data['result']['data'])) ? $data['result']['data'] : [];
            $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
        } else {
            $data = [];
            $totalCount = 0;
        }

        return [
            'data' => $data,
            'totalCount' => $totalCount
        ];
    }
}
