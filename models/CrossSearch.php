<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

use app\modules\experimentCreation\models\BrowserModel;
use yii\data\Pagination;
use app\models\UserDashboardConfig;

/**
 * Model class for dynamic Cross data
 */
class CrossSearch extends Cross{
    
    private $data;
    private $dynamicRules = [];
    public $crossDbId;
    public $crossName;
    public $crossMethod;
    public $crossRemarks;
    public $germplasmDbId;
    public $germplasmDesignation;
    public $germplasmParentage;
    public $germplasmGeneration;
    public $entryDbId;
    public $entryNo;
    public $entryName;
    public $entryRole;
    public $experimentDbId;
    public $experimentDataProcessId;
    public $experimentTemplate;
    public $femaleEntryDbId;
    public $maleEntryDbId;
    public $creationTimestamp;
    public $creatorDbId;
    public $creator;
    public $modificationTimestamp;
    public $modifierDbId;
    public $crossFemaleParent;
    public $femaleParentSeedSource;
    public $crossMaleParent;
    public $maleParentSeedSource;
    public $femaleSourceEntry;
    public $maleSourceEntry;
    public $femaleSourcePlot;
    public $maleSourcePlot;
    public $femaleParentage;
    public $maleParentage;
    public $femaleSourcePlotDbId;
    public $maleSourcePlotDbId;
    public $femaleOccurrenceName;
    public $femaleOccurrenceCode;
    public $maleOccurrenceName;
    public $maleOccurrenceCode;
    public $femaleParentEntryNumber;
    public $maleParentEntryNumber;
    public $femaleEntryCode;
    public $maleEntryCode;
    public $femalePIEntryCode;
    public $malePIEntryCode;
    public $entryListName;
    public $harvestStatus;

    public function __construct (
        protected BrowserModel $browserModel,
        protected UserDashboardConfig $userDashboardConfig,
        $config=[]
    )
    {        
        parent::__construct($config);
    }

    public function __get($varName){
        return $this->data[$varName];
    }

    public function __set($varName, $value){
       $this->data[$varName] = $value;
    }

    public function rules(){
        return [
            [[
                'crossName', 'crossFemaleParent', 'crossMaleParent', 'crossMethod', 'crossRemarks',
                'femaleSourceEntry', 'maleSourceEntry',
                'femaleSourcePlot', 'maleSourcePlot',
                'femaleParentage', 'maleParentage',
                'femaleParentSeedSource', 'maleParentSeedSource',
                'femaleOccurrenceName', 'femaleOccurrenceCode',
                'maleOccurrenceName', 'maleOccurrenceCode',
                'femaleParentEntryNumber', 'maleParentEntryNumber',
                'femaleEntryCode', 'maleEntryCode',
                'femalePIEntryCode', 'malePIEntryCode',
                'entryListName', 'harvestStatus'
            ], 'string'],
            [[
                'femaleSourcePlotDbId', 'maleSourcePlotDbId',
            ], 'integer'],

            [$this->dynamicRules, 'safe']
        ];
        // return $this->dynamicRules;
    }

    /**
     * Search model for Cross
     * 
     * @param array $params list of current search parameters
     * @param array $paramFilter list of additional search parameters
     * @param boolean $shouldBePaginated flag for whether to paginate or not paginate data
     * @param string $pageParams for API query parameters
     * @param boolean $isForDataBrowser flag for whether data is for regular data browser or for managing crosses
     * 
     * @return ArrayDataProvider $dataProvider Cross list browser data provider and attributes
     */
    public function search ($params = null, $paramFilter = null, $shouldBePaginated = false, $pageParams = null, $isForDataBrowser = false)
    {
        $dataProviderId = 'crosses-browser';
        $dataProvider = [];

        $pagination = null;
        $paramPage = null;
        $currPage = 1;
        $paramSort = null;
        $sort = '';

        if (isset($paramFilter)) {
            extract($paramFilter);

            // Build filters
            $filters = isset($CrossSearch) ?
                $this->browserModel->formatFilters($CrossSearch) : [];
            $filters = array_map('trim', $filters);

            if(count($filters) != 0) {
                $params = array_merge($params, $filters);
            }

            // Build paramSort
            if (str_contains($sort, '-')) {
                $sort = substr($sort, 1);
                $paramSort = "sort=$sort:desc";
            } else if ($sort) {
                $paramSort = "sort=$sort:asc";
            }
            
            // get current page
            if (isset($_GET["$dataProviderId-page"])) {
                $currPage = intval($_GET["$dataProviderId-page"]);
                $paramPage = "&page=$currPage";
            }

            // Build pagination limit and page
            $paramLimit = 'limit='.$this->userDashboardConfig->getDefaultPageSizePreferences();

            $pagination = "$paramLimit$paramPage";
            
            // Build pageParams
            if (!$pageParams) {
                $pageParams = "$pagination&$paramSort";
            }

            if (!$shouldBePaginated) {
                $pagination = null;
            } else {
                $output = $this->searchAll($params, "limit=1", !isset($pagination));
                
                $totalPages = $output['totalPages'];
                // if out of range
                if ($currPage > $totalPages) {
                    $pagination = "$paramLimit&page=1";
                    $pageParams = "$pagination&$paramSort";
                    $_GET["$dataProviderId-page"] = 1; // return browser to page 1
                }
            }

            //TODO: this should be updated! include sorting and page filtering!
            $results = $this->getDataProvider($params, $pageParams, $pagination, 'SORT_DESC', $isForDataBrowser);

            $dataProvider = $results['dataProvider'];

            $this->load(Yii::$app->request->queryParams);
        
            if (!($this->load($params) && $this->validate())) {
                //validate results
                return $dataProvider;
            }
        }

        $this->load($params);

        //no results found
        return $dataProvider;
    }
}
