<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;
use app\models\UserDashboardConfig;

class TraitsSearch extends Traits
{

    /**
     * Search model for traits
     *
     * @param $params array list of parameters
     * @param $queryParams array list of query parameters
     *
     * @return $dataProvider array traits browser dataprovider
     */
    public function search($params, $queryParams=null) {
        $filters = '';

        if(!empty($queryParams)) {
            $filters = $queryParams;
            if(isset($filters['name']) && isset($filters['name']['equals'])) {
                $val = $filters['name']['equals'];
                unset($filters['name']['equals']);
                $filters['name']['like'] = $val;
            }
        }

        if(isset($params['TraitsSearch'])) {
            $tempFilters = [];
            foreach ($params['TraitsSearch'] as $key => $value) {
                if(!empty($value) && isset($value)){
                    $tempFilters[$key] = [
                        'like' => $value
                    ];
                }
            }
            if (!empty($tempFilters)) {
                if (!empty($filters)) {
                    $filters['__browser_filters__'] = $tempFilters;
                } else {
                    $filters = $tempFilters;
                }
            }
        }

        $sort = new Sort();
        $sort->attributes = ['abbrev', 'bibliographicalReference', 'dataLevel', 'dataType', 'description', 'displayName', 'isComputed', 'label', 'name', 'notNull', 'ontologyReference', 'remarks', 'status', 'synonym', 'targetModel', 'targetTable', 'type', 'usage', 'creationTimestamp', 'creator', 'modificationTimestamp', 'modifier',
            'orderNumber'=>[
                "asc" => ["orderNumber"=>SORT_ASC],
                'desc' => ["orderNumber"=>SORT_DESC]
            ]
        ];
        $sort->defaultOrder = [
            'orderNumber'=>SORT_ASC
        ];

        $paramSort = '';
        if(isset($_GET['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach($sortParams as $column) {
                if($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['orderNumber'=>SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
            $sort->setAttributeOrders($tempOrder);
            $sort->defaultOrder = $tempOrder;
        }

        $dataProviderId = 'traits-dp';
        $paramPage = '';
        $pageCount = 1;
        if (isset($_GET[$dataProviderId.'-page'])){
            $pageCount = $_GET[$dataProviderId.'-page'];
            $paramPage = '&page=' . $pageCount;
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $pageCount = $_GET['page'];
        }

        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $filters['type'] = 'observation';
        $response = Yii::$app->api->getResponse('POST', 'variables-search?'.$paramLimit.$paramPage.$paramSort, json_encode($filters));

        if ($pageCount != 1 && $response['body']['metadata']['pagination']['totalCount'] == 0) {
            $response = Yii::$app->api->getResponse('POST', 'variables-search?'.$paramLimit.$paramSort.'&page=1', json_encode($filters));
            $_GET[$dataProviderId.'-page'] = 1; // return browser to page 1
        }

        $data = $response['body']['result']['data'];

        $tempData = [];
        foreach ($data as $index => $d) {
            $tempData[$index] = $d;
            $tempData[$index]['orderNumber'] = $index;
        }
        $data = $tempData;
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'orderNumber',
            'restified' => true,
            'totalCount' => $response['body']['metadata']['pagination']['totalCount'],
            'id' => $dataProviderId
        ]);
        $dataProvider->setSort($sort);

        $this->load($params);

        // Check for Select All
        if($filters != Yii::$app->session['trait-api_filter']) {
            Yii::$app->session->set('trait-api_filter', $filters);
            Yii::$app->session->set('trait-total_results_count', $response['body']['metadata']['pagination']['totalCount']);
            Yii::$app->session->set('trait-select_all_flag', 'include mode');
            Yii::$app->session->set('trait-selected_ids', []);
        }

        return $dataProvider;
    }

    /**
     * Retrieve column names and datatypes
     *
     * @return $columnNameAndDatatypes array of objects containing the column names and their datatypes
     * for the advanced filters and multiple sort
     */
    public function getColumnNameAndDatatypes() {
        return array(
            'abbrev' => 'String',
            'label' => 'String',
            'name' => 'String',
            'displayName' => 'String',
            'dataType' => 'String',
            'type' => 'String',
            'usage' => 'String',
            'dataLevel' => 'String',
            'status' => 'String',
            'description' => 'String',
            'creator' => 'String',
            'creationTimestamp' => 'Timestamp',
            'modifier' => 'String',
            'modificationTimestamp' => 'Timestamp'
        );
    }
}