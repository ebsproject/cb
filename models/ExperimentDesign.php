<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.experiment_design".
 *
 * @property int $id Identifier of the record within the table
 * @property int $experiment_id Refers to the plot where the data is being attributed to
 * @property int $study_id Study where the experiment design belongs to
 * @property int $entry_id Refers to the entry where the plot of the experiment design value is being attributed to
 * @property int $plot_id Refers to the plot where the experiment design value is being attributed to
 * @property string $design_type Type of the experimental design
 * @property string $design_value Value of the experimental design
 * @property int $design_level_number Level number of the design value
 * @property int $steward_id Steward user of the experimental design
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ExperimentDesign extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational.experiment_design';
    }

    /**
    * Set api endpoint
    */
    public static function apiEndPoint() {
        return 'experiment-designs';
    }
}
