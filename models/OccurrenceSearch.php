<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IApi;
use app\interfaces\models\IConfig;
use app\interfaces\models\IExperiment;
use app\interfaces\models\ILocation;
use app\interfaces\models\ILocationOccurrenceGroup;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\interfaces\modules\harvestManager\models\IApiHelper;
use app\models\Occurrence;
use app\models\OccurrenceData;
use app\models\Person;
use app\models\PlantingJob;
use app\models\Plot;
use app\models\UserDashboardConfig;
use app\modules\experimentCreation\models\BrowserModel;
use app\modules\location\controllers\MonitorController;
use app\modules\occurrence\models\Manage as OccurrenceManage;
use Yii;

/**
 * Contains methods for searching an occurrence
 */
class OccurrenceSearch extends Occurrence
{
    private $data;

    public function __construct(
        public IApi $api,
        public BrowserController $browserController,
        public IConfig $configModel,
        protected BrowserModel $browserModel,
        public IDashboard $dashboard,
        public IExperiment $experimentModel,
        public ILocation $locationModel,
        public ILocationOccurrenceGroup $locationOccurrenceGroup,
        public MonitorController $monitorController,
        public OccurrenceManage $occurrenceManage,
        public OccurrenceData $occurrenceData,
        protected Person $person,
        public PlantingJob $plantingJob,
        public Plot $plot,
        public IUser $user,
        protected UserDashboardConfig $userDashboardConfig,
        public IVariable $variable,
        public IApiHelper $apiHelper,
        $config = []
    )
    {
        parent::__construct(
            $api,
            $browserController,
            $configModel,
            $dashboard,
            $experimentModel,
            $locationModel,
            $locationOccurrenceGroup,
            $monitorController,
            $occurrenceManage,
            $occurrenceData,
            $plantingJob,
            $plot,
            $user,
            $variable,
            $apiHelper,
            $config
        );
    }

    public function __get($varName)
    {
        return $this->data[$varName] ?? '';
    }

    public function __set($varName, $value)
    {
        $this->data[$varName] = $value;
    }

    /**
     * Search for occurrence
     *
     * @param $filter array list of search parameters
     * @param $category text current filter category
     * @param $selectedCategoryId integer selected category item
     * @param $page integer page in api response to be displayed
     * @param $pageParams data browser parameters
     * @param $originTab name of EC tab
     * @return $dataProvider array list of occurrences
     */
    public function search($filter = null, $category = null, $selectedCategoryId = null, $page = null, $sortArray = null, $pageParams = '', $originTab = '')
    {
        if ($category == 'experiment creation') {

            $dataProvider = [];

            if (isset($filter)) {

                $params = $filter['params'];
                $configData = $filter['configData'];

                extract($params);
                $filters = isset($OccurrenceSearch) ? $this->browserModel->formatFilters($OccurrenceSearch) : [];

                foreach ($filters as $key => $value) {
                    // process '(not set)' filter for api call
                    if ($value == '(not set)') {
                        $filters[$key] = 'null';
                    }
                }
                $results = Occurrence::getDataProvider($selectedCategoryId, $configData, $filters, false, $sortArray, $pageParams, $originTab);

                $this->dynamicRules = [[$results['attributes'], 'safe']];

                $dataProvider = $results['dataProvider'];

                if (!($this->load($params) && $this->validate())) {
                    //validate results
                    return $dataProvider;
                }

                //no results found
                return $dataProvider;
            }
        }

        $data = Occurrence::searchOccurrences($filter, $category, $selectedCategoryId, $page);
        $totalCount = $data['totalCount'];
        $dataprovider = new ArrayDataProvider([
            'allModels' => $data['dataProvider'],
            'key' => 'occurrenceDbId',
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'occurrenceDbId'
                ],
                'defaultOrder' => [
                    'occurrenceDbId' => SORT_DESC
                ],
            ]
        ]);

        return [
            'dataProvider' => $dataprovider,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Search occurrences
     *
     * @param array $params filter parameters
     * @param array $defaultFilters contains default and/or EM-specifc filters from Filter Experiments
     *
     * @return array $dataProvider occurrence records
     */
    public function searchOccurrence($params, $defaultFilters)
    {
        Yii::debug('Searching for occurrences ' . json_encode([
            'params' => $params,
            'defaultFilters' => $defaultFilters
        ]), __METHOD__);
        $makeCrosses = (isset($params['makeCrosses']) && !empty($params['makeCrosses'])) ?
            $params['makeCrosses'] : false;

        $filters = null;
        $paramLimit = '';
        $paramSort = 'sort=experimentDbId:desc|occurrenceDbId';
        $sortParams = '';
        $paramPage = '';
        $currPage = 1;
        $isOwnedCond = '';
        $ownershipCond = '';
        $dataProviderId = 'occurrences-browser';

        // default parameter, exclude draft occurrences
        if ($makeCrosses) {
            $filters['occurrenceStatus'] = 'equals planted';
            $filters['dataProcessAbbrev'] = 'equals CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS';
        }

        $filters['experimentStatus'] = 'equals completed|equals planted|equals mapped|equals created|equals planted|equals planted;crossed|equals planted;draft|equals planted;entry list created|equals planted;cross list created|equals planted;entry list created;cross list created;protocol specified;occurrences created|equals planted;entry list created;cross list created;protocol specified|equals planted;design generated|equals creation in progress';

        // include dashboard filters
        $dashboardFilters = (array) $defaultFilters;

        // format dashboard filters
        $dashboardFiltersFormatted = [];
        if (!empty($dashboardFilters)) {
            foreach ($dashboardFilters as $key => $value) {

                $attribute = str_replace('_i', 'DbI', $key);

                if (in_array($key, ['stage_id', 'season_id', 'year'])) {
                    $attribute = 'experiment' . ucfirst($attribute);
                }
                // if display only owned experiments
                if ($key == 'owned' && $value == 'true') {
                    $isOwnedCond = '&isOwned';
                }

                if ($key == 'experiment_code') {
                    $attribute = 'experimentCode';
                }
                if ($key == 'experiment_name') {
                    $attribute = 'experiment';
                }
                if ($key == 'occurrence_name') {
                    $attribute = 'occurrenceName';
                }
                if ($key == 'field_name') {
                    $attribute = 'field';
                }
                if ($key == 'location') {
                    $attribute = 'location';
                }

                if (isset($value) && $key != 'owned') {
                    if (is_array($value)) {
                        // Load array of IDs
                        $val = implode('|', $value);

                        // Retrieve values of attributes
                        if ($key == 'occurrence_name') {
                            $val = $this->getValueOfAttribute('occurrenceDbId', $val, $attribute);
                        } else if (in_array($key, ['experiment_code', 'experiment_name'])) {
                            $val = $this->getValueOfAttribute('experimentDbId', $val, $attribute);
                        } else if ($key == 'field_name') {
                            $val = $this->getValueOfAttribute('fieldDbId', $val, $attribute);
                        } else if ($key == 'location') {
                            $val = $this->getValueOfAttribute('locationDbId', $val, $attribute);
                        }

                        // Add "equals" modifier
                        $val = explode('|', $val);

                        array_walk($val, function (&$value, $key) {
                            if (
                                $value != '' &&
                                !str_contains(strtolower($value), 'equals') &&
                                !str_contains(strtolower($value), '%')
                            )
                                $value = "equals $value";
                        });

                        $val = implode('|', $val);
                    } else {
                        $val = "equals $value";
                    }

                    $dashboardFiltersFormatted[$attribute] = (string)$val;
                }
            }
        }

        $filters = array_merge($filters, $dashboardFiltersFormatted);
        extract($params);
        $columnFilters = isset($OccurrenceSearch) ? $this->browserModel->formatFilters($OccurrenceSearch) : [];

        array_walk($columnFilters, function (&$value, $key) {
            if (
                $value != '' &&
                !str_contains(strtolower($value), 'equals') &&
                !str_contains(strtolower($value), '%')
            )
                $value = "equals $value";
        });

        $filters = array_merge($filters, $columnFilters);

        // override experimentYear when both filters exist
        if (
            (isset($dashboardFiltersFormatted['experimentYear']) && isset($columnFilters['experimentYear'])) &&
            ($dashboardFiltersFormatted['experimentYear'] !== $columnFilters['experimentYear'])
        ) {
            $dashboardFilterYear = $dashboardFiltersFormatted['experimentYear'];
            $columnFilterYear = $columnFilters['experimentYear'];

            if (in_array($columnFilterYear, explode('|', $dashboardFilterYear))) {
                // use column filter if it exists in dashboard filter
                $filters['experimentYear'] = $columnFilters['experimentYear'];
            } else {
                // change condition to return 0 results
                $experimentYear = $dashboardFilterYear .'|'. $columnFilterYear;
                $filters['experimentYear'] = str_replace('|equals ','&',$experimentYear);
            }
        }

        $program = isset($program) ? $program : '';
        [ $_, $isAdmin, ] = $this->person->getPersonRoleAndType($program);

        // If User is not an Admin, Remove program condition to display occurrences shared to the program or user and add ownership condition
        if (!$isAdmin && isset($filters['programDbId'])) {
            unset($filters['programDbId']);
            $ownershipCond = "ownershipType=shared&collaboratorProgramCode=$program";
        }


        // get current sort
        if (
            isset($_GET["$dataProviderId-sort"]) &&
            !empty($_GET["$dataProviderId-sort"])
        ) {
            $sortParams = $_GET["$dataProviderId-sort"];

            if ($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort=' . $column . $order;
        }

        // get current page
        if (isset($_GET["$dataProviderId-page"])) {
            $currPage = intval($_GET["$dataProviderId-page"]);
            $paramPage = "&page=$currPage";
        }

        // Get page size from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $url = "occurrences-search?$paramLimit&$ownershipCond&$paramPage&$paramSort&$isOwnedCond";

        $output = Yii::$app->api->getParsedResponse(
            'POST',
            $url,
            json_encode($filters)
        );

        if ($currPage != 1 && $output['totalCount'] == 0) {
            $output = Yii::$app->api->getParsedResponse(
                'POST',
                "occurrences-search?$paramLimit&$ownershipCond&$paramSort&page=1&$isOwnedCond",
                json_encode($filters)
            );
            $_GET["$dataProviderId-page"] = 1; // return browser to page 1
        }

        Yii::$app->session->set('occurrence-search-occurrences-session-vars', [
            $currPage ?? null,
            $paramLimit ?? '',
            $paramPage ?? '',
            $paramSort ?? '',
            $isOwnedCond ?? '',
            $filters ?? null,
            Yii::$app->config->getAppThreshold('EXPERIMENT_MANAGER', 'selectOccurrences') ?? (int) explode('=', $paramLimit)[1],
        ]);

        $attributes = [
            'occurrenceDbId', 'programCode', 'experimentStatus', 'occurrenceStatus', 'experimentCode',
            'experiment', 'occurrenceName', 'siteCode', 'experimentStageCode',
            'field', 'location', 'experimentType', 'experimentYear', 'experimentDesignType',
            'occurrenceDesignType', 'entryCount', 'plotCount', 'experimentSeasonCode', 'experimentSteward', 'locationSteward',
            'creationTimestamp'
        ];

        $this->dynamicRules = [[$attributes, 'safe']];

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'occurrenceDbId',
            'restified' => true,
            'totalCount' => $output['totalCount'],
            'sort' => [
                'defaultOrder' => ['experimentYear' => SORT_DESC, 'creationTimestamp' => SORT_DESC],
                'attributes' => $attributes
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            //validate results
            return [
                $dataProvider,
                (int) explode('=', $paramLimit)[1],
            ];
        }

        return [
            $dataProvider,
            (int) explode('=', $paramLimit)[1],
        ];
    }

    /**
     * Retrieve the value of a given attribute
     *
     * @param string $entityDbId DB ID label of the entity
     * @param string $dbIdValue DB ID value of the entity
     * @param string $attribute name of the DB field to be retrieved
     *
     * @return string string representation of all array elements concatenated by '|'
     */
    private function getValueOfAttribute ($entityDbId, $dbIdValue, $attribute)
    {
        $params[$entityDbId] = $dbIdValue;

        $entities = Occurrence::searchAll($params, '', true);
        $entitiesVal = [];

        if (isset($entities['data'])) {
            $entitiesValues = $entities['data'];

            foreach ($entitiesValues as $v) {
                $entitiesVal[] = str_replace("'", "''", $v[$attribute]);
            }
        }

        return implode('|', $entitiesVal);
    }

    /**
     * Retrieves items for filter column of occurrence browser
     *
     * @param integer $experimentId experiment identifier
     * @param array $params list of search parameters from Select2 widget
     * @param string $column name of column whose values will be retrieved
     *
     * @return array $output data for the Select2 widget
     */
    public static function getOccurrencesFilter($experimentId, $params, $column) {
        // build filters for api call
        $fieldColumn = '';
        $distinctColumn = '';

        switch($column) {
            case 'site':
                $distinctColumn = 'site';
                $fieldColumn = 'site.geospatial_object_name AS site';
                break;
        }

        $filters = [
            'fields'=> 'occurrence.experiment_id|'.$fieldColumn,
            'distinctOn'=> $distinctColumn,
            'experiment_id'=> "equals ".$experimentId
        ];
        if(isset($params[$column])) {
            $filters[$distinctColumn] = '%'.$params[$column].'%';
        }

        $response = Yii::$app->api->getResponse('POST', 'occurrences-search', json_encode($filters));
        $data = $response['body']['result']['data'];

        // add (not set) choice
        $items = [
            [
                'id' => '(not set)',
                'text' => '(not set)'
            ]
        ];
        foreach($data as $d) {
            $items[] = [
                'id' => $d[$distinctColumn],
                'text' => $d[$distinctColumn]
            ];
        }
        $output['items'] = $items;

        return $output;

    }

    /**
     * Retrive occurrence basic info for file name by ID
     *
     * @param Integer $id Occurrence identifier
     * @return Array Occurrence information
     */
    public function getOccurrenceForFileNameById($id)
    {
        // retrieve occurrence info for CSV filename
        $occurrenceParams = [
            'fields' => 'occurrence.id AS id |'.
                'occurrence.occurrence_number AS occurrenceNumber |'.
                'experiment.experiment_code AS experimentCode |'.
                'experiment.experiment_name AS experimentName |'.
                'occurrence.occurrence_name AS occurrenceName |'.
                'occurrence.occurrence_name AS occurrenceCode |'.
                'location.location_name AS locationName |'.
                'location.location_code AS locationCode ',
            'id' => "equals $id",
        ];

        return Yii::$app->api->getParsedResponse(
            'POST',
            'occurrences-search?limit=1',
            json_encode($occurrenceParams),
            '',
            false
        );
    }
}