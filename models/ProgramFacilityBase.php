<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.program_facility".
 *
 * @property int $id Identifier of the record within the table
 * @property int $program_id Program (reference ID) linked to the facility
 * @property int $facility_id Facility (reference ID) linked to the program
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ProgramFacilityBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.program_facility';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['program_id', 'facility_id', 'creator_id'], 'required'],
            [['program_id', 'facility_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['program_id', 'facility_id', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['program_id', 'facility_id'], 'unique', 'targetAttribute' => ['program_id', 'facility_id']],
            [['facility_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterFacility::className(), 'targetAttribute' => ['facility_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'facility_id' => 'Facility ID',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
