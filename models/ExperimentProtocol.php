<?php

/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\dataproviders\ArrayDataProvider;

use Yii;

/**
 * This is the model class for table "experiment.experiment_protocol".
 *
 * @property int $id Experiment Protocol ID: Database identifier of the experiment protocol [EXPTPROT_ID]
 * @property int $experiment_id Experiment ID: Reference to the experiment that uses the protocol [EXPTPROT_EXPT_ID]
 * @property int $protocol_id Protocol ID: Reference to the protocol used by the experiment [EXPTPROT_PROT_ID]
 * @property int $order_number Experiment Protocol Order Number: Ordering of the protocol used by the experiment [EXPTPROT_ORDERNO]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class ExperimentProtocol extends BaseModel {

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'experiment-protocols';
    }

    /**
     * Retrieve list record identifier
     * Create records in tenant.protocol, experiment.experiment_protocol,
     * experiment.experiment_data and platform.list tables if list id does not exist
     * 
     * @param Integer $experimentDbId experiment identifier
     * @param String $protocolType Experiment Protocol - either trait or management
     * @param String $shouldUpdateOccurrenceProtocols flag for manage protocol mode
     * @param String $occurrenceDbIdsString list of occurrence identifiers, separated by `|`
     * @param String $listLevel EXPERIMENT | OCCURRENCE
     */
    public function getListId($experimentDbId,$protocolType='TRAITS',$shouldUpdateOccurrenceProtocols='',$occurrenceDbIdsString='',$listLevel='EXPERIMENT'){
        $experimentCode = '';

        if($protocolType == 'TRAITS'){
            $initAbbrev = 'TRAIT_PROTOCOL';
            $targetProtocol = 'trait';
            $initName = 'Trait Protocol';
            $protocolVarAbbrev = 'TRAIT_PROTOCOL_LIST_ID';
            $type = 'trait';
        }else if($protocolType == 'MANAGEMENT'){
            $initAbbrev = 'MANAGEMENT_PROTOCOL';
            $targetProtocol = 'management';
            $initName = 'Management Protocol';
            $protocolVarAbbrev = 'MANAGEMENT_PROTOCOL_LIST_ID';
            $type = 'variable';
        }

        if (empty($shouldUpdateOccurrenceProtocols) && $listLevel == 'EXPERIMENT') {
            // Experiment Creation
            $experimentInfo = \Yii::$container->get('app\models\Experiment')->getOne($experimentDbId);
            $experiment = $experimentInfo['data'] ?? [];
            $experimentCode = $experiment['experimentCode'] ?? '';

            $abbrev = $initAbbrev."_".$experimentCode;

            $listInfo = \Yii::$container->get('app\models\Lists')->searchAll(['abbrev' => "equals $abbrev"],'showWorkingList=true&limit=1', false);
            $listId = $listInfo['data'][0]['listDbId'] ?? '';
        } else {
            // Experiment Manager - Update or View occurrence-level variables
            $params = [
                'occurrenceDbId' => (string) $occurrenceDbIdsString,
                'variableAbbrev' => $protocolVarAbbrev,
            ];
            $occurrenceData = \Yii::$container->get('app\models\OccurrenceData')->searchAll($params,'limit=1',false);
            $listId = $occurrenceData['data'][0]['dataValue'] ?? '';
        }

        //no existing list
        if(empty($listId) && empty($shouldUpdateOccurrenceProtocols)){
            //Check if the protocol has been created in the tenant.protocol
            $protocolCode = strtoupper($targetProtocol)."_PROTOCOL_".$experimentCode;

            $protocol = \Yii::$container->get('app\models\Protocol')->searchAll(["protocolCode"=>"$protocolCode"]);
            $protocolDbId = $protocol['data'][0]['protocolDbId'] ?? '';

            // If protocol id is empty then create a protocol and experiment protocol record
            if(empty($protocolDbId) || $protocolDbId == 0){ 
                $protocolDbId = \Yii::$container->get('app\models\Protocol')->createExperimentProtocols($experimentDbId, $targetProtocol);
            }
            
            if($protocolDbId){
                // create list
                $name = $displayName = $experiment['experimentName']." $initName ($experimentCode)";
                $remarks = 'created using Experiment Creation tool';
                $requestData['records'][] = [
                    'abbrev' => $abbrev,
                    'name' => $name,
                    'displayName' => $displayName,
                    'remarks' => $remarks,
                    'type' => $type,
                    'listUsage' => 'working list',
                    'subType' => "$targetProtocol protocol",
                ];

                // save list basic info
                $newListInfo = \Yii::$container->get('app\models\Lists')->create($requestData);

                $listId = $newListInfo['data'][0]['listDbId'] ?? '';

                if($listId && $newListInfo['success']){
                    //add to experimentData
                    $variable = \Yii::$container->get('app\models\Variable')->searchAll(['abbrev' => 'equals '.$protocolVarAbbrev]);
                    $variableDbId = $variable['data'][0]['variableDbId'];
                    $insertData[] = [
                        'variableDbId' => "$variableDbId",
                        'dataValue' => "$listId",
                        'protocolDbId' => "$protocolDbId",
                    ];

                    \Yii::$container->get('app\models\Experiment')->createExperimentData($experimentDbId, $insertData);

                    //add list access
                    \Yii::$container->get('app\models\PlatformListAccess')->addAccess($listId, [], [$experiment['programDbId']], 'read_write');
                }
            }
        }

        return $listId;
    }
    
    /**
     * Get experiment data provider
     *
     * @param $experimentId integer experiment identifier
     * @param $isPreview boolean whether in preview page or not
     * @return mixed array data provider and total count of of variables
     */
    public static function getExperimentProtocolProvider($listId, $idList = null, $pageParams = ''){
        
        $listId = !empty($listId) ? $listId : 0;

        $listMemberDbId = '';
        // if IDs are specified
        if(!empty($idList)){
            $listMemberDbId = 'equals '.implode('|equals ', $idList);
            $listMemberInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId,["listMemberDbId"=>"$listMemberDbId"],$pageParams);
        }else{
            $listMemberInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId, null, $pageParams);
        }

        
        $listMembers = isset($listMemberInfo['data'][0]['listMemberDbId']) ? $listMemberInfo['data'] : [];

        $attributes = [
            "listMemberDbId",
            "variableDbId",
            "orderNumber",
            "abbrev",
            "label",
            "displayName",
            "remarks",
        ];

        return new ArrayDataProvider([
            'allModels' => $listMembers,
            'key' => 'orderNumber',
            'sort' => [
                'attributes' => $attributes,
                'defaultOrder' => [
                    'orderNumber' => SORT_ASC
                ]
            ],
            'pagination' => false
        ]);



    }
    
    /**
     * Retrieve variables tags
     *
     * @return $varTags 
     */

    public static function getTraitVariablesTags(){
        
        $filter = 'sort=label:ASC';

        $variableArray = \Yii::$container->get('app\models\Variable')->searchAll([
            'type'=>'observation',
            'status' => 'active',
            'dataType' => 'not ilike %json%',
            'usage' => 'not ilike %application%',
            'dataLevel' => '%plot%',
        ], $filter);

        $tags = $variableArray['data'];

        $varTags = []; // trait variable tags
        foreach ($tags as $value) {
            $abbrev = $value['abbrev'];
            $abbrevIsValid = !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'ACTUAL'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'PLANNED'))
                && !(\Yii::$container->get('app\models\Variable')->endsWith($abbrev, '_ADJ_MEAN'))
                && !(\Yii::$container->get('app\models\Variable')->endsWith($abbrev, '_ADJ_MEAN_METH'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'RANK'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'SITE_MEAN_'))
            ;			

            if((!$value['isComputed'] || $abbrev == 'FLW_CONT') && $abbrevIsValid){
                
                $varTags[$value['variableDbId']] = $value['label'].'('.$value['displayName'].')';
                
            }
        }

        return $varTags;
    }

    /**
     * Retrieve variables tags for Management Protocol
     *
     * @return $varTags 
     */
    public static function getManagementVariablesTags(){
        
        $filter = 'sort=label:ASC';

        $variableArray = \Yii::$container->get('app\models\Variable')->searchAll([
            'type'=>'metadata',
            'status' => 'active',
            'dataType' => 'not ilike %json%',
            'usage' => 'management'
        ], $filter);

        $tags = $variableArray['data'];

        $varTags = []; // trait variable tags
        foreach ($tags as $value) {
            $abbrev = $value['abbrev'];
            $abbrevIsValid = !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'ACTUAL'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'PLANNED'))
                && !(\Yii::$container->get('app\models\Variable')->endsWith($abbrev, '_ADJ_MEAN'))
                && !(\Yii::$container->get('app\models\Variable')->endsWith($abbrev, '_ADJ_MEAN_METH'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'RANK'))
                && !(\Yii::$container->get('app\models\Variable')->startsWith($abbrev, 'SITE_MEAN_'))
            ;			

            if((!$value['isComputed'] || $abbrev == 'FLW_CONT') && $abbrevIsValid){
                
                $varTags[$value['variableDbId']] = $value['label'].'('.$value['displayName'].')';
                
            }
        }
        
        return $varTags;
    }

    /**
     * Retrieve saved experiment (traits or management) protocol tags
     *
     * @param int $listId list ID of the protocol
     * @param string $protocolType TRAITS | MANAGEMENT
     * @return array $listTags list of saved list tags
     */
    public static function getExperimentProtocolTags($listId, $protocolType){

        $listId = !empty($listId) ? $listId : 0;
        
        if($protocolType == 'TRAITS'){
            $type = 'trait';
        }else if ($protocolType == 'MANAGEMENT'){
            $type = 'variable';
        }
        $searchParams = [
            'listDbId' => "not equals $listId",
            'type' => "equals $type",
            'memberCount' => 'not equals 0',
        ];
        if($protocolType == 'MANAGEMENT'){
            $searchParams['subType'] = 'management';
        }
        $tagsInfo = \Yii::$container->get('app\models\Lists')->searchAll($searchParams);

        $tags = isset($tagsInfo['data']) ? $tagsInfo['data'] : [];

        $listTags = []; // variable set tags

        foreach ($tags as $value) {
            $listTags[$value['listDbId']] = $value['name'];
        }

        return $listTags;
    }
    
    /**
     * Add new variables in the list
     *
     * @param $listId Integer list record identifier
     * @param $id Integer variable/list identifier
     * @param $type Text variable/list type 
     * @param $operation Text add/replace variables
     * @param $protocolType String Management or Traits
     * @param $experimentId Integer Experiment ID
     */
    public static function addVariables($listId, $id, $type, $operation, string $protocolType = null, $experimentId){

        $requestData = [];
        $someMsg = ' trait/s. ';
        $listId = !empty($listId) ? $listId : 0;
        $listMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId);
        $listMembers = $listMembersInfo['data'];
        $listMembersCount = $listMembersInfo['totalCount'];
        $listMembersId = array_column($listMembers, 'listMemberDbId');

        //if all traits are to be replaced
        if($operation == 'replaced'){
            \Yii::$container->get('app\models\ListMember')->deleteMany($listMembersId);
            $listMembersCount = 0;
        }

        if($protocolType == 'management'){
            $someMsg = ' management variable/s. ';
        }

        //if adding variables from a trait protocols list
        if($type == 'list'){

            if($listMembersCount == 0){//if current list is empty
                $newListMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($id);
            }
            else{
                //compare current and selected lists
                $variableDbIds = 'equals '.implode('|equals ', array_column($listMembers, 'variableDbId'));
                $newListMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($id, [
                    'variableDbId' => $variableDbIds
                ]);
                $newListMembersCount = $newListMembersInfo['totalCount'];

                $notEqualsVariableDbIds = 'not equals '.implode('|not equals ', array_column($listMembers, 'variableDbId'));

                $diffListMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($id, [
                    'variableDbId' => $notEqualsVariableDbIds
                ]);
                $diffListMembersCount = $diffListMembersInfo['totalCount'];

                if(($newListMembersCount == $listMembersCount || $newListMembersCount < $listMembersCount) && !$diffListMembersCount){
                    return "<i class='material-icons orange-text left'>warning</i> ".\Yii::t('app','Selected traits are already in the list.');
                }
                
                $newListMembersInfo = $diffListMembersInfo;

                if($diffListMembersCount){
                    $someMsg = \Yii::t('app',"$someMsg Some of the them are already in the list.");
                }
                
            }

            $newListMembers = $newListMembersInfo['data'];

            foreach($newListMembers as $newListMember){
                $requestData[] = [
                    'id' => $newListMember['variableDbId'],
                    'displayValue' => $newListMember['displayValue'],
                ];
            }
        }
        else{//if adding/replacing from the list of traits
            //check if member already exist
            $listMembers = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId, ['variableDbId' => "equals $id"]);
            if($listMembers['totalCount']){
                return "<i class='material-icons orange-text left'>warning</i> ".\Yii::t('app','The selected trait is already in the list.');   
            }

            //add variable
            $variable = \Yii::$container->get('app\models\Variable')->getOne($id);
            $requestData[] = [
                'id' => $id,
                'displayValue' => $variable['data']['label'],
            ];
        }

        $newMember = \Yii::$container->get('app\models\Lists')->createMembers($listId, $requestData);

        if($experimentId != null){
            //Check if Target level for management protocol exists in the table experiment.experiment_data
            $mgtVariable = \Yii::$container->get('app\models\Variable')->searchAll(['abbrev' => 'equals PROTOCOL_TARGET_LEVEL'])['data'][0]['variableDbId'];
            $mgtDataParam = ['variableDbId'=>"equals $mgtVariable"];
            $experimentDataRes = \Yii::$container->get('app\models\ExperimentData')->searchAllRecords($experimentId,$mgtDataParam);
            
            if(empty($experimentDataRes)){
                //By default, the management type is occurrence
                $dataRequestBodyParam[] = [
                    'variableDbId' => "$mgtVariable",
                    'dataValue' => "occurrence"
                ];
                \Yii::$container->get('app\models\Experiment')->createExperimentData($experimentId,$dataRequestBodyParam);
            }
        }
            
        $message = '';
        if($newMember['totalCount']){
            $message = "<i class='material-icons green-text left'>done</i> ".\Yii::t('app',"Successfully $operation $someMsg");
        }

        return $message;
    }

    /**
     * Bulk update remarks/trait instructions
     * 
     * @param Integer $listId list identifier
     * @param Array $listMemberId of integers list member identifier
     * @param String $remarks trait instructions
     * @param string $operation update type : selected/all
     */
    public function saveBulkRemarks($listId, $listMemberId, $remarks, $operation){

        $listId = !empty($listId) ? $listId : 0;

        if(!empty($listMemberId) && $operation == 'update-selected'){
            $idListIds = 'equals '.implode('|equals ', $listMemberId);
            $listMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId,[
                'listMemberDbId' => $idListIds,
            ]);
        }
        else{
            $listMembersInfo = \Yii::$container->get('app\models\Lists')->searchAllMembers($listId);
        }

        $listMembersId = array_column($listMembersInfo['data'], 'listMemberDbId');
        $result = \Yii::$container->get('app\models\ListMember')->updateMany($listMembersId,[
            'remarks' => "$remarks"
        ]);

        return $result;
    }

}
