<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\IApi;
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IExperiment;
use app\interfaces\models\ILocation;
use app\interfaces\models\ILocationOccurrenceGroup;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IUser;
use app\interfaces\models\IVariable;
use app\interfaces\models\IConfig;
use app\models\OccurrenceData;
use app\models\PlantingJob;
use app\models\Plot;
use app\modules\occurrence\models\Manage as OccurrenceManage;
use app\modules\location\controllers\MonitorController;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\interfaces\modules\harvestManager\models\IApiHelper;

/**
 * This is the model class for table "experiment.occurrence".
 *
 * @property int $id Occurrrence ID: Database identifier of the occurrence [OCC_ID]
 * @property string $occurrence_code Occurrence Code: Textual identifier of the occurrence [OCC_CODE]
 * @property string $occurrence_name Occurrence Name: Full name of the occurrence [OCC_NAME]
 * @property string $occurrence_status Occurrence Status: Status of the occurrence with regard to its plots {draft, mapped} [OCC_STATUS]
 * @property string|null $description Occurrence Description: Additional information about the occurrence [OCC_DESC]
 * @property int $experiment_id Experiment ID: Reference to the experiment that is represented by the occurrence [OCC_EXPT_ID]
 * @property int|null $geospatial_object_id Geospatial Object ID: Reference to the site or field (geospatial object) where the occurrence is planned to be conducted [OCC_GEO_ID]
 * @property int $creator_id
 * @property string $creation_timestamp
 * @property int|null $modifier_id
 * @property string|null $modification_timestamp
 * @property bool $is_void
 * @property string|null $notes
 * @property string|null $access_data
 * @property string|null $event_log
 * @property int|null $rep_count Number of replication
 * @property string|null $occurrence_document Sorted list of distinct lexemes which are normalized; used in search query
 */
class Occurrence extends BaseModel implements IOccurrence
{
public function __construct (
        public IApi $api,
        public BrowserController $browserController,
        public IConfig $configModel,
        public IDashboard $dashboard,
        public IExperiment $experimentModel,
        public ILocation $locationModel,
        public ILocationOccurrenceGroup $locationOccurrenceGroup,
        public MonitorController $monitorController,
        public OccurrenceManage $occurrenceManage,
        public OccurrenceData $occurrenceData,
        public PlantingJob $plantingJob,
        public Plot $plot,
        public IUser $user,
        public IVariable $variable,
        public IApiHelper $apiHelperHarvest,
        $config = []
    )
    { parent::__construct($config); }

    /* Define fields to be validated and included in search filters */
    public function rules ()
    {
        return [
            [[
                'occurrenceDbId',
                'occurrenceNumber',
                'programDbId',
                'projectDbId',
                'siteDbId',
                'fieldDbId',
                'geospatialObjectId',
                'experimentDbId',
                'experimentStageDbId',
                'experimentYear',
                'experimentSeasonDbId',
                'experimentStewardDbId',
                'dataProcessDbId',
                'repCount',
                'entryCount',
                'plotCount',
                'locationDbId',
                'locationCode',
                'creatorDbId',
                'modifierDbId',
            ], 'integer'],
            [[
                'occurrenceCode',
                'occurrenceName',
                'occurrenceDesignType',
                'programCode',
                'program',
                'projectCode',
                'project',
                'siteCode',
                'site',
                'fieldCode',
                'field',
                'geospatialObjectCode',
                'geospatialObject',
                'geospatialObjectType',
                'experimentCode',
                'experiment',
                'experimentType',
                'experimentStatus',
                'experimentStageCode',
                'experimentStage',
                'experimentSeasonCode',
                'experimentSeason',
                'experimentDesignType',
                'experimentSteward',
                'locationSteward',
                'dataProcessAbbrev',
                'location',
                'occurrenceStatus',
                'description',
                'contactPerson',
                'creator',
                'modifier',
                'occurrenceDocument',   
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
            ], 'safe'],
        ];
    }

    /**
     * API endpoint for occurrence
     */
    public static function apiEndPoint ()
    {
        return 'occurrences';
    }

    /**
     * Search for an occurrence given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $categorySelectedId integer selected category item
     * @return $data array list of occurrences given search parameters
     */
    public function searchOccurrences ($filter, $category, $categorySelectedId, $page)
    {
        $data = [];
        $totalCount = 0;
        $limit = 'limit=50';
        $page = !empty($page) ? '&page=' . $page : '';
        $sort = '&sort=occurrenceDbId:desc';
        $isOwnedCond = '';

        // default parameter, exclude draft occurrences
        $params["occurrenceStatus"] = "not equals draft";

        // if filter is set, build params
        if(!empty($filter)){
            // process filter for document
            $processedFilter = BrowserController::processDocumentFilter($filter);

            $params["occurrenceDocument"] = $processedFilter;

        }

        // get logged in user id
        
        // get dashboard filters
        $defaultFilters = $this->dashboard->getFilters();
        // include dashboard filters
        $dashboardFilters = (array) $defaultFilters;

        // format dashboard filters
        $dashboardFiltersFormatted = [];
        if(!empty($dashboardFilters)){
            foreach ($dashboardFilters as $key => $value) {
                $attribute = str_replace('_i', 'DbI', $key);

                if(in_array($key, ['stage_id','season_id','year'])){
                    $attribute= 'experiment'.ucfirst($attribute);
                }

                // if display only owned experiments
                if($key == 'owned' && $value == 'true') {
                    $isOwnedCond = '&isOwned';
                }

                if(!empty($value) && $key != 'owned') {
                    if(is_array($value)){
                        $value = implode('|', $value);
                    }
                    $dashboardFiltersFormatted[$attribute] = (string)$value;
                }
            }
        }

        $params = array_merge($params, $dashboardFiltersFormatted);

        $validCategory = in_array($category, ['experiment','location']);
        // if there is a selected category
        if(!empty($categorySelectedId) && $categorySelectedId != 'all' && $validCategory){
            $category = $category.'DbId';
            $addtlParams[$category] = (string) $categorySelectedId;

            $params = $addtlParams;
        }

        $params = empty($params) ? null : $params;

        $response = Yii::$app->api->getResponse('POST', 'occurrences-search', json_encode($params), $limit . $page . $sort . $isOwnedCond, false);

        // check if successfully retrieved occurrences
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){

            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
            $totalCount = $response['body']['metadata']['pagination']['totalCount'];
        }

        return [
            'dataProvider' => $data,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Get occurrence by ID
     *
     * @param int|string $id occurrence identifier
     * @param array $param additional parameters
     * 
     * @return $data occurrence information
     */
    public function get ($id, $param = null)
    {
        $param = $param ?? ['occurrenceDbId' => $id];
        $data = [];

        $response = $this->api->getApiResults('POST', 'occurrences-search', json_encode($param));

        // check if successfully retrieved occurrence
        if(isset($response['status']) && $response['status'] == 200 && isset($response['data'][0])){
            $data = $response['data'][0];
        }

        return $data;
    }

    /**
     * Get occurrence info for detail view
     *
     * @param $id integer occurrence identifier
     * @return $data array occurrence information
     */
    public function getDetailViewInfo ($id)
    {
        // get current program
        $program = Yii::$app->userprogram->get('abbrev');
        $data = $this->get($id,
            [
                'occurrenceDbId' => "$id",
                'fields' => 'occurrence.id AS occurrenceDbId |
                    occurrence.occurrence_code AS occurrenceCode |
                    occurrence.occurrence_name AS occurrenceName |
                    occurrence.description |
                    occurrence.experiment_id AS experimentDbId |
                    occurrence.occurrence_status AS occurrenceStatus |
                    occurrence.entry_count AS entryCount |
                    occurrence.plot_count AS plotCount |
                    experiment.experiment_name AS experiment |
                    steward.person_name AS experimentSteward |
                    experiment.experiment_type AS experimentType |
                    experiment.experiment_design_type AS experimentDesignType |
                    site.geospatial_object_name AS site |
                    field.geospatial_object_name AS field |
                    stage.stage_name AS experimentStage |
                    experiment.experiment_year AS experimentYear |
                    season.season_name AS experimentSeason |
                    project.project_name AS project |
                    creator.person_name AS creator
                ',
            ]
        );

        // main info
        $code = isset($data['occurrenceCode']) ? 
            [
                'label' => 'Occurrence code',
                'value' => $data['occurrenceCode'] 
            ]
            : '';
        $name = isset($data['occurrenceName']) ? 
            [
                'label' => 'Occurrence name',
                'value' => $data['occurrenceName']
            ]
            : '';
        $description = isset($data['description']) ? $data['description'] : '';
        $experimentId = isset($data['experimentDbId']) ? $data['experimentDbId'] : null;

        // status
        $status = isset($data['occurrenceStatus']) ? $data['occurrenceStatus'] : '';
        $statusVal = $status;

        $statusArr = [];
        $statuses = explode(';', $status);
        foreach ($statuses as $value) {
            $statusCategory = strtolower($value);
            
            if(strpos($statusCategory, 'pack') !== false){
                $statusClass = $this->plantingJob->getStatusClass(strtolower($value));
            } else {

                if( $statusCategory == 'mapped' || strpos($statusCategory, 'in progress')){
                    $statusCategory = 'in-progress';
                } else if($statusCategory == 'planted' || $statusCategory == 'crossed') {
                    $statusCategory = 'done';
                }

                $statusClass = BrowserController::getStatusClassByCategory($statusCategory);
            }

            $statusArr[] = [
                'label' => ucfirst($value),
                'statusClass' => $statusClass
            ];
        }

        // get location info
        $locOccGrpParams['occurrenceDbId'] = (string) $id;
        $locOccGrp = $this->locationOccurrenceGroup->searchAll($locOccGrpParams);
        $locationId = null;

        $data['location'] = null;
        $data['locationCode'] = null;
        if(isset($locOccGrp['data'][0]['locationDbId'])){
            $locationId = $locOccGrp['data'][0]['locationDbId'];

            $locationParams['locationDbId'] = "$locationId";
            $locationParams['fields'] = 'location.id AS locationDbId |
                location.location_name AS locationName |
                location.location_code AS locationCode |
                steward.person_name AS steward |
                location.location_type AS locationType |
                location.location_harvest_date AS harvestDate |
                location.location_planting_date AS plantingDate
            ';
            $location = $this->locationModel->searchAll($locationParams);

            if(isset($location['data'][0])){
                $locInfo = $location['data'][0];
                $data['location'] = isset($locInfo['locationName']) ? $locInfo['locationName'] : null;
                $data['locationCode'] = isset($locInfo['locationCode']) ? $locInfo['locationCode'] : null;
                $data['locationSteward'] = isset($locInfo['steward']) ? $locInfo['steward'] : null;
                $data['locationType'] = isset($locInfo['locationType']) ? $locInfo['locationType'] : null;
                $data['harvestDate'] = isset($locInfo['harvestDate']) ? $locInfo['harvestDate'] : null;
                $data['plantingDate'] = isset($locInfo['plantingDate']) ? $locInfo['plantingDate'] : null;
            }
        }

        // primary info
        $attributes = [
            [
                'label' => $this->variable->getAttributeByAbbrev('experiment_name','label'),
                'attribute' => 'experiment'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('experiment_steward','label'),
                'attribute' => 'experimentSteward'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('entry_count_cont','label'),
                'attribute' => 'entryCount',
                'title' => 'View entries',
                'url' => Url::to([
                    '/occurrence/view/entry',
                    'program' => $program, 
                    'id' => $id
                ])
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('plot_count','label'),
                'attribute' => 'plotCount',
                'title' => 'View plots',
                'url' => Url::to([
                    '/occurrence/view/plot',
                    'program' => $program, 
                    'id' => $id
                ])
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('experiment_design_type','label'),
                'attribute' => 'experimentDesignType'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('project','label'),
                'attribute' => 'projectCode'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_name','label'),
                'attribute' => 'location'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_code','label'),
                'attribute' => 'locationCode',
                // If location code is set, convert it into a hyperlink...
                // ...to Location View
                'title' => ($data['locationCode']) ? 'View location' : null,
                'url' => ($data['locationCode']) ? Url::to([
                    '/location/view/basic',
                    'program' => $program, 
                    'id' => $locationId
                ]) : null,
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_steward','label'),
                'attribute' => 'locationSteward'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_planting_date','label'),
                'attribute' => 'plantingDate'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_harvest_date','label'),
                'attribute' => 'harvestDate'
            ],
            [
                'label' => 'Packages Harvested',
                'attribute' => 'packageHarvested'
            ],
        ];

        // Retrieve total harvested plot and cross; with their url links
        $harvestRecord = $this->getPackageHarvest($id, $program);
        $data['packageHarvested'] = $harvestRecord;
        $primaryInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        // get occurrence data
        $metadata = $this->getDataByOccurrenceId($id);

        // This removes Entry and Plot Code Pattern variables
        $metadata = array_filter((array) $metadata, function ($data) {
            return strtoupper($data['variableAbbrev']) != 'ENTRY_CODE_PATTERN' && strtoupper($data['variableAbbrev']) != 'PLOT_CODE_PATTERN';
        });

        $metadataInfo = BrowserController::processMetadataDetailViewInfo($metadata);

        // merge occurrence data to basic info
        $primaryInfo = array_merge($primaryInfo, $metadataInfo);

        // secondary information
        $attributes = [
            [
                'label' => $this->variable->getAttributeByAbbrev('experiment_type','label'),
                'attribute' => 'experimentType'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('site','label'),
                'attribute' => 'site'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_type','label'),
                'attribute' => 'locationType'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('field','label'),
                'attribute' => 'field',
                'title' => 'field'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('stage','label'),
                'attribute' => 'experimentStage'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('experiment_year','label'),
                'attribute' => 'experimentYear'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('season','label'),
                'attribute' => 'experimentSeason'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('creator','label'),
                'attribute' => 'creator'
            ]
        ];

        $secondaryInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        // audit information
        $attributes = [
            [
                'label' => 'Created',
                'attribute' => 'creationTimestamp'
            ],
            [
                'label' => 'Updated',
                'attribute' => 'modificationTimestamp'
            ]
        ];

        $auditInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        return [
            'code' => $code,
            'name' => $name,
            'description' => $description,
            'status' => $statusVal,
            'statusArr' => $statusArr,
            'primaryInfo' => $primaryInfo,
            'secondaryInfo' => $secondaryInfo,
            'auditInfo' => $auditInfo,
            'locationId' => $locationId,
            'experimentId' => $experimentId
        ];


    }

    /**
     * Get occurrence status monitoring info for detail view
     *
     * @param $id integer occurrence identifier
     * @return $monitoringInfo monitoring info data values
     */
    public function getMonitoringInfo ($id)
    {
        // get location info
        $locOccGrpParams['occurrenceDbId'] = (string) $id;
        $locOccGrp = $this->locationOccurrenceGroup->searchAll($locOccGrpParams);
        $locationId = null;

        $data['location'] = null;
        $data['locationCode'] = null;
        if(isset($locOccGrp['data'][0]['locationDbId'])){
            $locationId = $locOccGrp['data'][0]['locationDbId'];
        }

        // status monitoring info
        $monitoringInfo = $this->monitorController->getLocationStatusData($locationId, $id);

        return $monitoringInfo;
    }

    /**
     * Get occurrence data by occurrence ID
     *
     * @param $id integer occurrence identifier
     * @return $data occurrence data values
     */
    public function getDataByOccurrenceId ($id)
    {
        $param = [
            'occurrenceDbId' => $id,
            // This filters out Occurrence-level protocols with non-null protocolDbId values
            'protocolDbId' => 'null',
        ];
        $data = [];

        $response = Yii::$app->api->getResponse('POST', 'occurrence-data-search', json_encode($param));

        // check if successfully retrieved occurrence data
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'][0])){
            $data = $response['body']['result']['data'];
        }

        return $data;
    }

    /**
     * Find Experiments in browser detail view catgory
     *
     * @return $results list of experiments
     */
    public function findExperimentCategories ()
    {
        $params = [];
        $method = 'POST';
        $path = 'experiments-search';
        $sort = 'sort=experimentDbId:desc';
        $isOwnedCond = '';

        // get dashboard filters
        $defaultFilters = $this->dashboard->getFilters();
        // include dashboard filters
        $dashboardFilters = (array) $defaultFilters;

        // format dashboard filters
        $params['experimentStatus'] = 'completed|created';
        if(!empty($dashboardFilters)){
            foreach ($dashboardFilters as $key => $value) {
                $attribute = str_replace('_i', 'DbI', $key);

                $attribute = ($key == 'year') ? 'experiment'.ucfirst($attribute) : $attribute;
                // if display only owned experiments
                if($key == 'owned' && $value == 'true') {
                    $isOwnedCond = '&isOwned';
                }

                if(!empty($value) && !in_array($key, ['owned', 'site_id'])) {
                    if(is_array($value)){
                        $value = implode('|', $value);
                    }
                    $params[$attribute] = (string)$value;
                }
            }
        }

        $params = empty($params) ? null : $params;

        $data = Yii::$app->api->getResponse($method, $path, json_encode($params), $sort . $isOwnedCond, true);

        $result = [];
        $totalCount = 0;
        // Check if it has a result, then retrieve retrieve experiments
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
            $totalCount = $data['body']['metadata']['pagination']['totalCount'];
        }

        // data to be displayed in view category details
        $attributes = [
            'experimentCode' => 'Code',
            'stageCode' => 'Stage',
            'experimentYear' => 'Year',
            'seasonCode' => 'Season',
            'experimentType' => 'Type',
            'occurrenceCount' => 'No. of occurrences',
            'steward' => 'Steward'
        ];

        // should always have id, text, data, total count and attributes
        return [
            'id' => 'experimentDbId',
            'text' => 'experimentName',
            'data' => $result,
            'totalCount' => $totalCount,
            'attributes' => $attributes
        ];


    }

    /**
     * Retrieve occurrence configuration by step
     *
     * @param text $step step to retrieve in config
     * @param 
     * @return array $config field configuration for the step
     */
    public function getConfig ($id, $step)
    {
        $config = [];
        // get occurrence
        $occurrence = $this->get($id);
        // get experiment
        $experimentId = isset($occurrence['experimentDbId']) ? $occurrence['experimentDbId'] : 0;
        // get experiment
        $experiment = $this->experimentModel->getExperiment($experimentId);
        // get experiment template ID
        $dataProccess = isset($experiment['dataProcessAbbrev']) ? $experiment['dataProcessAbbrev'] : 0;

        // get occurrence config by experiment template
        $configPostStr = '_OCCURRENCE_FIELDS';
        $configStr = $dataProccess . $configPostStr;
        $configData = $this->configModel->getConfigByAbbrev($configStr);

        // if no config retrieved, get default
        if(empty($configData)){
            $defaultConfigStr = 'DEFAULT' . $configPostStr;
            $configData = $this->configModel->getConfigByAbbrev($defaultConfigStr);
        }

        // if configuration was retrieved, get fields for the step
        if(!empty($configData) && isset($configData[$step]['fields'])){
            $config = $configData[$step]['fields'];
        }

        return $config;
    }

    /**
     * Save occurrence basic data
     *
     * @param integer $id occurrence identifier
     * @param array $formData occurrence info to save
     * @return boolean $result if successful or not
     */
    public function saveBasicData ($id, $formData)
    {
        $identificationParams = [];
        foreach ($formData as $key => $value) {

            // to check if identification or metadata
            $type = array_keys($value);
            $typeVal = isset($type[0]) ? $type[0] : '';
            // get variable ID and value
            $variableId = array_keys($value[$typeVal]);
            $variableIdVal = isset($variableId[0]) ? $variableId[0] : null;
            $val = $value[$typeVal][$variableIdVal];
            $val = trim($val);

            // if identification
            if($typeVal == 'identification'){
                // format variable according to api attribute text case (e.g. occurrenceName)
                $attr = ucwords(strtolower($key),'_');
                $attribute = str_replace('_','',$attr);
                $attribute = lcfirst($attribute); 

                $identificationParams[$attribute] = $val; 
            }else{ // if to save in occurrence_data
                // check if record already exists in occurrence data
                $params = [
                    "occurrenceDbId" => $id,
                    "variableDbId" => (string)$variableIdVal
                ];
                $occurrenceData = $this->occurrenceData->searchAll($params);
                $totalCount = $occurrenceData['totalCount'];

                // values parameter to be saved
                $params['dataValue'] = (string)$val;

                // if already exists, update the record
                if($totalCount > 0){
                    // get occurrence data ID
                    $occurrenceDataDbId = isset($occurrenceData['data'][0]['occurrenceDataDbId']) ? 
                        $occurrenceData['data'][0]['occurrenceDataDbId'] : null;

                    // check if value is empty, delete the record
                    if(empty($val)){
                        $this->occurrenceData->deleteOne($occurrenceDataDbId);
                    }else{
                        // if value is not empty, update the existing record
                        $param['dataValue'] = (string)$val;
                        $this->occurrenceData->updateOne($occurrenceDataDbId, $param);
                    }
                }else{
                    if(!empty($val)){
                        // if not yet existing, create new record
                        $createParams['records'] = [ $params ];
                        $this->occurrenceData->create($createParams);
                    }
                }
            }
        }

        // update occurrence information
        $occurrence = $this->updateOne($id, $identificationParams);
        // check if successful or not
        return ($occurrence['success']) ? true : false;


    }

    /**
     * Retrieve occurrence data provider
     * 
     * @param Integer $experimentDbId experiment record identifier
     * @param Array $configData configuration information
     * @param Array $params optional data parameters
     * @param Boolean $noPagination optional flag when grid view pagination should be set or not
     * @param String $pageParams data browser parameters
     * @param String $originTab name of EC tab
     */
    public function getDataProvider ($experimentDbId, $configData, $params = [], $noPagination = false, $sortArray = NULL, $pageParams = '', $originTab = '')
    {
        $params = array_merge(['experimentDbId' => "equals $experimentDbId"], $params);
        if($originTab == 'site tab') {
            $path = 'occurrences-search'.$pageParams;
            $results = Yii::$app->api->getResponse('POST', $path, json_encode($params), '', false);
            $totalCount = 0;
            if (isset($results['status']) && $results['status'] == 200) {
                $pagination = $results['body']['metadata']['pagination'];
                $totalPages = $pagination['totalPages'];
                $currentPage = $pagination['currentPage'];

                if(isset($currentPage) && $currentPage > $totalPages) {
                    $pageParams2 = str_replace("&page=$currentPage",'', $pageParams);
                    $path = 'occurrences-search'.$pageParams2;
                    $results2 = Yii::$app->api->getResponse('POST', $path, json_encode($params), '', false);
                    $_GET['page'] = 1; // return browser to page 1

                    if (isset($results2['status']) && $results2['status'] == 200) {
                        $occurrences = $results2['body']['result']['data'];
                        $totalCount = $results2['body']['metadata']['pagination']['totalCount'];
                    }
                } else {
                    $occurrences = $results['body']['result']['data'];
                    $totalCount = $results['body']['metadata']['pagination']['totalCount'];
                }
            }
        } else {    // review tab, etc.
            $occurrenceRecords = $this->searchAll($params);
            $occurrences = $occurrenceRecords['data'];
        }

        $attributes = [
            'occurrenceDbId',
            'occurrenceCode',
            'occurrenceName',
            'occurrenceDesignType',
            'programDbId',
            'programCode',
            'program',
            'projectDbId',
            'projectCode',
            'project',
            'siteDbId',
            'siteCode',
            'site',
            'fieldDbId',
            'fieldCode',
            'field',
            'geospatialObjectId',
            'geospatialObjectCode',
            'geospatialObject',
            'geospatialObjectType',
            'experimentDbId',
            'experimentCode',
            'experiment',
            'experimentType',
            'experimentStageDbId',
            'experimentStageCode',
            'experimentStage',
            'experimentYear',
            'experimentSeasonDbId',
            'experimentSeasonCode',
            'experimentSeason',
            'experimentDesignType',
            'experimentStewardDbId',
            'experimentSteward',
            'repCount',
            'entryCount',
            'plotCount',
            'locationDbId',
            'occurrenceStatus',
            'description',
            'contactPerson',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier',
            'occurrenceDocument', 
            'locationSteward',
        ];
        
        if($sortArray == null){
            $sortArray = [
                'occurrenceDbId' => SORT_DESC
            ];
        }
        $providerDetails = [
            'allModels' => $occurrences,
            'key' => 'occurrenceDbId',
            'sort' => [
                'attributes' => $attributes,
                'defaultOrder' => $sortArray
            ],
            'restified' => $originTab == 'site tab'? true: false,
            'totalCount' => $originTab == 'site tab'? $totalCount: null,
        ];

        if($noPagination){
            $providerDetails['pagination'] = false;
        }

        $dataProvider = new ArrayDataProvider($providerDetails);

        $dataProvider = [
            'dataProvider' => $dataProvider,
            'attributes' => $attributes,
        ];

        return $dataProvider;

    }

    /**
     * Retrieve plots data provider
     * 
     * @param Integer $occurrenceId occurrence record identifier
     * 
     */
    public function getPlotDataProvider ($occurrenceId, $distinct=null)
    {
        $paramLimit = 'limit=20';
        $gridName = "plot-$occurrenceId";
        $paramPage = isset($_GET["$gridName-page"]) ? '&page=' . $_GET["$gridName-page"] : '&page=1';
        $paramSort = '';
        if (isset($_GET["$gridName-sort"])){
            if(strpos($_GET["$gridName-sort"],'-') !== false){
                $sort = str_replace('-', '', $_GET["$gridName-sort"]);
                $paramSort = "&sort=$sort:DESC";
            }
            else{
                $paramSort = '&sort='.$_GET["$gridName-sort"];
            }
        }

        $plotRecords = Yii::$app->api->getParsedResponse('POST', "occurrences/$occurrenceId/plots-search?".$paramLimit.$paramPage.$paramSort, $distinct, '', false);
        $plots = $plotRecords['data'][0]['plots'] ?? [];
        
        $attributes = [
            'plotDbId',
            'entryDbId',
            'entryCode',
            'entryNumber',
            'entryName',
            'entryType',
            'entryRole',
            'entryClass',
            'entryStatus',
            'germplasmDbId',
            'plotCode',
            'plotNumber',
            'plotType',
            'rep',
            'designX',
            'designY',
            'plotOrderNumber',
            'paX',
            'paY',
            'fieldX',
            'fieldY',
            'blockNumber',
            'plotStatus',
            'plotQcCode',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier', 
        ];

        $providerDetails = [
            'allModels' => $plots,
            'key' => 'plotDbId',
            'id' => $gridName,
            'restified' => true,
            'totalCount' => $plotRecords['totalCount'],
            'sort' => [
                'attributes' => $attributes,
                'defaultOrder' => [
                    'plotNumber' => SORT_ASC
                ],
            ],
        ];

        $dataProvider = new ArrayDataProvider($providerDetails);

        $dataProvider = [
            'dataProvider' => $dataProvider,
            'attributes' => $attributes,
        ];

        return $dataProvider;
    }

    /**
     * Retrieve plot information given a specific occurrence id
     * 
     * @param Integer $occurrenceDbId occurrence record identifier
     * @param Array $params optional parameters
     * @param String $filters optional filters
     * @param Boolean $getAll Flag indicator to retrieve the plots
     * 
     */
    public function searchAllPlots ($occurrenceDbId, $params = null, $filters = '', $getAll = false)
    {
        return Yii::$app->api->getParsedResponse(
            'POST',
            "occurrences/$occurrenceDbId/plots-search",
            json_encode($params),
            $filters,
            $getAll
        );
    }

    /**
     * Retrieve plot information given a specific occurrence id
     * 
     * @param Integer $occurrenceDbId occurrence record identifier
     * @param Array $params optional parameters
     * @param String $filters optional filters
     * @param Boolean $getAll Flag indicator to retrieve the plots
     * 
     */
    public function searchPlots ($occurrenceDbId, $params = null, $filters = '', $getAll = false)
    {

        $occurrenceIdParam = ['occurrenceDbId'=>'equals ' . $occurrenceDbId];
        $searchParams = array_merge($occurrenceIdParam, $params);

        return $this->plot->searchAll($searchParams, $filters, $getAll);

    }

    /**
     * Update Experiment status
     *
     * @param integer $occurrenceDbId occurrence identifier
     */
    public function updateExperimentStatus ($occurrenceDbId)
    {
        $occurrencesParams = [
            "occurrenceDbId" => "equals $occurrenceDbId"
        ];

        // Get Occurrences info
        $occurrence = $this->searchAll($occurrencesParams, 'limit=1', false);
        $experimentDbId = (isset($occurrence['data'][0]['experimentDbId'])) ? $occurrence['data'][0]['experimentDbId'] : 0;

        $experimentParams = [
            "experimentDbId" => "equals $experimentDbId"
        ];
        // Get Experiments data
        $experimentResults = $this->searchAll($experimentParams);

        $occurrenceStatusArray = [];
        if((isset($experimentResults['data'])) && !empty($experimentResults['data'])){

            foreach ($experimentResults['data'] as $exp) {
                $occurrenceStatusArray[] = $exp['occurrenceStatus'];
            }

            // Check if the occurrences status are the same
            if (count(array_unique($occurrenceStatusArray)) <= 1) {
                // values parameter to be saved
                $occurrenceStatus = $occurrenceStatusArray[0];

                $statusArr = explode(';', $occurrenceStatus);
                $expStatusArr = [];
                
                foreach ($statusArr as $value) {
                    // exclude packing job and trait data collected statuses
                    if (strpos($value, 'pack') === false && strpos($value, 'trait data collected') === false ){
                        $expStatusArr[] = $value;
                    }
                }

                $expStatus = implode(';', $expStatusArr);

                $requestData['experimentStatus'] = $expStatus;

                // Update the status of the Experiment
                $this->experimentModel->updateOne($experimentDbId, $requestData);
            }
        }
    }

    /**
     * Get percentage of count over total count
     *
     * @param int $count actual count
     * @param int $totalCount total count
     *
     * @return float $status percentage  
     */
    public function getStatusPercentage($count, $totalCount) {
        return (!$count) ? 0 : ( $count / $totalCount) * 100;


    }

    /** 
     * Retrieve each Occurrence's parent Experiment's Type and Status and
     * associated Location ID given an Occurrence ID
     * 
     * @param array $occurrenceDbIds Currently selected Occurrence(s) from
     *                                        Experiment Manager data browser
     * 
     * @return array An array of occurrence info containing Experiment Name, Type, Status,
     *               and Location ID
    */
    public function getOccurrenceExperimentInfoAndLocationDbId ($occurrenceDbIds)
    {
        $occurrencesInfo = null;

        array_walk($occurrenceDbIds, function (&$value, $key) {
            $value = "equals $value";
        });

        $response = $this->searchAll(
            [
                'occurrenceDbId' => implode('|', $occurrenceDbIds),
            ]
        );

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        ) {
            $occurrencesInfo = [];
            $data = $response['data'];

            foreach ($data as $occurrenceRecord) {
                array_push($occurrencesInfo, [
                    'occurrenceDbId' => $occurrenceRecord['occurrenceDbId'],
                    'occurrenceName' => $occurrenceRecord['occurrenceName'],
                    'experimentDbId' => $occurrenceRecord['experimentDbId'],
                    'experimentName' => $occurrenceRecord['experiment'],
                    'experimentStatus' => $occurrenceRecord['experimentStatus'],
                    'occurrenceStatus' => $occurrenceRecord['occurrenceStatus'],
                    'experimentType' => $occurrenceRecord['experimentType'],
                    'locationDbId' => $occurrenceRecord['locationDbId'],
                ]);
            }
        }

        return $occurrencesInfo;
    }


    /** 
     * Retrieve an Occurrence's info and its parent Experiment's info
     * given an Occurrence ID
     * 
     * @param integer|string $occurrenceDbId Currently selected Occurrence from
     *                                       Experiment Manager data browser
     * 
     * @return array An array of Occurrence info and Experiment info
    */
    public function getOccurrenceExperimentInfo ($occurrenceDbId)
    {
        $occurrenceData = [];
        $response = $this->searchAll(
            [
                'occurrenceDbId' => (string) $occurrenceDbId,
            ]
        );

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'])
        ) {
            $occurrenceData = [];
            $occurrenceRecord = $response['data'][0];

            $occurrenceData = [
                'occurrenceDbId' => $occurrenceRecord['occurrenceDbId'],
                'occurrenceName' => $occurrenceRecord['occurrenceName'],
                'experimentDbId' => $occurrenceRecord['experimentDbId'],
                'experimentName' => $occurrenceRecord['experiment'],
                'experimentStatus' => $occurrenceRecord['experimentStatus'],
                'experimentType' => $occurrenceRecord['experimentType'],
                'dataProcessAbbrev' => $occurrenceRecord['dataProcessAbbrev'],
            ];
        }

        return $occurrenceData;
    }

    /**
     * Retrieves items for filter column of experiment manager browser
     *
     * @param array $params list of search parameters from Select2 widget
     * @param string $column name of column whose values will be retrieved
     *
     * @return array $output data for the Select2 widget
     */
    public function getColumnFilters($params, $column) {

        // build filters for api call
        $fieldColumn = '';
        $distinctColumn = '';
        $searchStr = '';

        if($column == 'occurrenceStatus') {
                $distinctColumn = 'occurrence_status';
                $fieldColumn = 'occurrence.'.$distinctColumn . ' as "' . $column . '"';
        }

        $filters = [
            'fields'=> $fieldColumn . ' | experiment.experiment_status as "experimentStatus"',
            'distinctOn'=> 'occurrenceStatus',
            'occurrenceStatus' => 'not equals draft',
            'experimentStatus' => 'equals completed|equals planted|equals mapped|equals created|equals planted|equals planted;crossed|equals planted;draft|equals planted;entry list created|equals planted;cross list created|equals planted;entry list created;cross list created;protocol specified;occurrences created|equals planted;entry list created;cross list created;protocol specified|equals planted;design generated'
        ];

        if(isset($params[$column])) {
            $filters[$column] = '%'.strtolower($params[$column]).'%';

            $searchStr = strtolower($params[$column]);
        }

        $res = Yii::$app->api->getParsedResponse('POST', 'occurrences-search', json_encode($filters), '', true);

        $data = $res['data'];

        $statusList = ArrayHelper::map($data, $column, $column);

        $options = [];
        $list = [];

        foreach ($statusList as $value) {
            $tempVal = explode(';',$value);

            foreach($tempVal as $val){

                if(!in_array($val,$list) && str_contains($val, $searchStr) ){ 
                    $list[$val] = $val; 

                    $options[] = [
                        'id' => $val,
                        'text' => $val
                    ];
                }
            }
        }

        $output['items'] = $options;

        return $output;

    }

    /**
     * Retrieves occurrence info in the format
     * applicable to select2 elements
     * @param requestBody - search parameters
     * @param urlParams - sort, limit, and page
     */
    public function getOccurrenceSelect2Data($requestBody = null, $urlParams = '') {
        $data = [];
        $method = 'POST';
        $endpoint = 'occurrences-search';

        $result = Yii::$app->api->getParsedResponse($method, $endpoint, $requestBody, $urlParams);
        $occurrences = isset($result['data']) ? $result['data'] : [];
        $totalCount = isset($result['totalCount']) ? $result['totalCount'] : 0;
        
        foreach($occurrences as $occurrence) {
            $data[] = [ 
                'id' => $occurrence['occurrenceDbId'],
                'text' => $occurrence['occurrenceName']
            ];
        }

        return [
            "data" => $data,
            "totalCount" => $totalCount
        ];
    }

    /**
     * Populate entry and plot count of occurrences
     *
     * @param Array $occurrenceDbIds list of IDs of the occurrence
     * @param Int $programDbId database record ID of the program
     *
     */
    public function populateOccurrenceInfo ($occurrenceDbIds, $programDbId) {
        foreach ($occurrenceDbIds as $occurrenceDbId) {
            $method = 'POST';
            $endpoint = 'occurrences';

            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/entry-count-generations", null, '');
            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/plot-count-generations", null, '');

            $data = [
                [
                    'entityDbId' => "$programDbId",
                    'entity' => 'program',
                    'permission' => 'write',
                ]
            ];
            
            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/permissions", json_encode([ 'records' => $data ]));
        }
    }


    /**
     * Populate entry and plot count of an occurrence
     *
     * @param Array $occurrenceDbIds list of IDs of the occurrence
     * @param Int $programDbId database record ID of the program
     *
     */
    public function populateSingleOccurrenceInfo ($occurrenceDbId, $programDbId) {
            $method = 'POST';
            $endpoint = 'occurrences';

            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/entry-count-generations", null, '');
            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/plot-count-generations", null, '');

            $data = [
                [
                    'entityDbId' => "$programDbId",
                    'entity' => 'program',
                    'permission' => 'write',
                ]
            ];
            
            Yii::$app->api->getParsedResponse($method, "$endpoint/$occurrenceDbId/permissions", json_encode([ 'records' => $data ]));
    }

    /**
     * Get harvest records on both cross and package
     * @param integer $occurrenceDbId occurrence identifier
     * @param integer $programDbId database record ID of the program
     */
    public function getPackageHarvest($occurrenceDbId, $program) {
        // Get Harvest Summaries
        $result = $this->apiHelperHarvest->getParsedResponse('GET', 'occurrences/'.$occurrenceDbId.'/harvest-summaries');
        $filter = "ManagementBrowserModel%5BharvestSource%5D";
        // Get total harvest record
        $result = $result['data'][0] ? $result['data'][0] : [];
        
        // Link for main HM tab for label
        $harvestRecord['urlHM'] = Url::to([
            '/harvestManager/occurrence-selection/index',
            'program' => $program, 
            'occurrenceId' => $occurrenceDbId
        ]);

        // Link and result for HM Management Tab for harvest cross
        $harvestRecord['cross'] = $result['harvestedCrosses']." / ".$result['totalCrossCount'] ?? null; 
        
        // Build link for HM management tab cross; Append dynagrid browser filter
        $harvestRecord['crossURL'] = Url::to([
            '/harvestManager/management/index',
            'program' => $program, 
            'occurrenceId' => $occurrenceDbId,
            'source' => 'crosses'
        ]);
        $harvestRecord['crossURL'] .= '&ManagementBrowserModel%5BharvestSource%5D=cross';

        // Result for HM Management Tab for harvest plots
        $harvestRecord['plot'] = $result['harvestedPlots']." / ". $result['totalPlotCount'] ?? null;
        
        // Build link for HM management tab plots; Append dynagrid browser filter
        $harvestRecord['plotURL'] = Url::to([
            '/harvestManager/management/index',
            'program' => $program, 
            'occurrenceId' => $occurrenceDbId,
            'source' => 'plots'
        ]);
        $harvestRecord['plotURL'] .= '&ManagementBrowserModel%5BharvestSource%5D=plot';

        return json_encode($harvestRecord);
    }
}