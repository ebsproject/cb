<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.seed_storage_log".
 *
 * @property int $id Primary key of the record in the table
 * @property int $seed_storage_id Seed storage of the product
 * @property int $encoder_id User who entered in the record
 * @property string $encode_timestamp Timestamp when the transaction was performed
 * @property string $transaction_type What was done to the seed storage record: deposit or withdraw
 * @property double $volume Volume deposited to or withdrawn from the seed storage record
 * @property string $unit Scale used to measure what volume was deposited or withdrawn
 * @property string $event_timestamp Timestamp when the transaction was needed
 * @property string $sender Where the deposited seeds come from
 * @property string $receiver Where the withdrawn seeds will go to
 * @property string $remarks Additional details about the seed storage log
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $seed_lot_event_id
 */
class SeedStorageLogBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational.seed_storage_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seed_storage_id', 'encoder_id', 'encode_timestamp', 'transaction_type', 'volume', 'unit', 'event_timestamp'], 'required'],
            [['seed_storage_id', 'encoder_id', 'creator_id', 'modifier_id', 'seed_lot_event_id'], 'default', 'value' => null],
            [['seed_storage_id', 'encoder_id', 'creator_id', 'modifier_id', 'seed_lot_event_id'], 'integer'],
            [['encode_timestamp', 'event_timestamp', 'creation_timestamp', 'modification_timestamp'], 'safe'],
            [['transaction_type', 'unit', 'sender', 'receiver', 'remarks', 'notes'], 'string'],
            [['volume'], 'number'],
            [['is_void'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seed_storage_id' => 'Seed Storage ID',
            'encoder_id' => 'Encoder ID',
            'encode_timestamp' => 'Encode Timestamp',
            'transaction_type' => 'Transaction Type',
            'volume' => 'Volume',
            'unit' => 'Unit',
            'event_timestamp' => 'Event Timestamp',
            'sender' => 'Sender',
            'receiver' => 'Receiver',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'seed_lot_event_id' => 'Seed Lot Event ID',
        ];
    }
}
