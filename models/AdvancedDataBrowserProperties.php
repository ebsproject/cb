<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

/**
 * Put in this class every property that a search model needs for a data browser's column filters
 * so that the AdvancedDataBrowser doesn't get too crowded. This is also done to prevent some needed
 * refactoring in the future since the dynamic properties for PHP classes gets deprecated in PHP 8.2
 * and will later result in a fatal error in PHP 9.0, according to an article that I've read. This was
 * coded in PHP 8.0 which is nearing it's EOL. Don't you just love that your fresh and shiny code
 * already needs to be refactored and also your newly created features already needs to be reimplemented
 * when you install software updates a month later? Oh boy, I do LOVE PHP SO MUCH! </3 </3 </3
 */

class AdvancedDataBrowserProperties extends \yii\base\Model {
    public $program;
    public $designation;
    public $generation;
    public $nameType;
    public $otherNames;
    public $parentage;
    public $programName;
    public $germplasmNameType;
    public $germplasmState;
    public $germplasmNormalizedName;
    public $germplasmType;
    public $germplasmCode;
    public $creator;
    public $creationTimestamp;
    public $modifier;
    public $modificationTimestamp;

    public $occurrenceStatus;
    public $experimentCode;
    public $experiment;
    public $occurrenceName;
    public $siteCode;
    public $field;
    public $location;
    public $experimentType;
    public $experimentStageCode;
    public $experimentYear;
    public $experimentSeasonCode;
    public $experimentDesignType;
    public $entryCount;
    public $plotCount;
    public $experimentSteward;
    public $locationSteward;
}