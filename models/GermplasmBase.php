<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "germplasm.germplasm".
 *
 * @property int $id Germplasm ID: Database identifier of the germplasm [GERM_ID]
 * @property string $designation Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]
 * @property string $parentage Parentage: Parentage string of the germplasm [GERM_PARENTAGE]
 * @property string $generation Generation: Stage of development of a germplasm [GERM_GENERATION]
 * @property string $germplasm_state Germplasm State: Current state of the germplasm: (progeny, fixed line, ...) [GERM_STATE]
 * @property string $germplasm_name_type Germplasm Name Type: Type of name designated to the germplasm [GERM_NAMETYPE]
 * @property int $crop_id Crop ID: Reference to the germplasm's crop [GERM_CROP_ID]
 * @property int $taxonomy_id Taxonomy ID: Reference to the taxonomy of the germplasm [GERM_TAXON_ID]
 * @property int|null $product_profile_id Product Profile ID: Reference to the product profile where the germplasm is developed [GERM_PRODPROF_ID]
 * @property string $germplasm_normalized_name Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERM_NORMNAME]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class GermplasmBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'germplasm.germplasm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['designation', 'parentage', 'generation', 'germplasm_state', 'germplasm_name_type', 'crop_id', 'taxonomy_id', 'germplasm_normalized_name', 'creator_id'], 'required'],
            [['parentage', 'notes'], 'string'],
            [['crop_id', 'taxonomy_id', 'product_profile_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['crop_id', 'taxonomy_id', 'product_profile_id', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['designation', 'germplasm_normalized_name'], 'string', 'max' => 256],
            [['generation', 'germplasm_state', 'germplasm_name_type'], 'string', 'max' => 64],
            [['designation'], 'unique'],
            [['germplasm_normalized_name'], 'unique'],
            [['product_profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => GermplasmProductProfile::className(), 'targetAttribute' => ['product_profile_id' => 'id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantCrop::className(), 'targetAttribute' => ['crop_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'designation' => 'Designation',
            'parentage' => 'Parentage',
            'generation' => 'Generation',
            'germplasm_state' => 'Germplasm State',
            'germplasm_name_type' => 'Germplasm Name Type',
            'crop_id' => 'Crop ID',
            'taxonomy_id' => 'Taxonomy ID',
            'product_profile_id' => 'Product Profile ID',
            'germplasm_normalized_name' => 'Germplasm Normalized Name',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }
}
