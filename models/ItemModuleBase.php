<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.item_module".
 *
 * @property int $id Primary key of the record in the table
 * @property int $item_id Parent item of the child module
 * @property int $module_id Child module of the parent item 
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 */
class ItemModuleBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.item_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'module_id'], 'required'],
            [['item_id', 'module_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['item_id', 'module_id', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'module_id' => 'Module ID',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
        ];
    }
}
