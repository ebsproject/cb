<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.list_member".
 *
 * @property int $id
 * @property int $list_id
 * @property int $data_id
 * @property int $order_number
 * @property string $display_value
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $record_uuid
 */
class PlatformListMemberBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.list_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_id', 'order_number', 'display_value'], 'required'],
            [['list_id', 'data_id', 'order_number', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['list_id', 'data_id', 'order_number', 'creator_id', 'modifier_id'], 'integer'],
            [['display_value', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlatformList::className(), 'targetAttribute' => ['list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'list_id' => 'List ID',
            'data_id' => 'Data ID',
            'order_number' => 'Order Number',
            'display_value' => 'Display Value',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
