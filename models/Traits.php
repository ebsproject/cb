<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

use app\interfaces\models\ITraits;

class Traits extends BaseModel implements ITraits{
    public $abbrev;
    public $bibliographicalReference;
    public $dataLevel;
    public $dataType;
    public $description;
    public $displayName;
    public $isComputed;
    public $label;
    public $name;
    public $notNull;
    public $ontologyReference;
    public $remarks;
    public $status;
    public $synonym;
    public $targetModel;
    public $targetTable;
    public $type;
    public $usage;
    public $creationTimestamp;
    public $creator;
    public $modificationTimestamp;
    public $modifier;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abbrev', 'bibliographicalReference', 'dataLevel', 'dataType', 'description', 'displayName', 'isComputed', 'label', 'name', 'notNull', 'ontologyReference', 'remarks', 'status', 'synonym', 'targetModel', 'targetTable', 'type', 'usage', 'creationTimestamp', 'creator', 'modificationTimestamp', 'modifier'], 'safe'],
        ];
    }

    /**
     * Returns the api endpoint for Traits
     *
     * @return string api endpoint for traits
     */
    public static function apiEndPoint() {
        return 'variables';
    }
}