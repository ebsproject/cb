<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use yii\web\UploadedFile;

use app\dataproviders\ArrayDataProvider;

use app\interfaces\models\IGermplasmFileUpload;
use app\interfaces\models\IUserDashboardConfig;
use app\interfaces\models\IVariable;
use app\interfaces\models\IConfig;

use app\models\Variable;
use app\models\Config;

class GermplasmFileUpload extends BaseModel implements IGermplasmFileUpload
{
    public $germplasmFileUploadDbId;
    public $programDbId;
    public $fileName;
    public $fileData;
    public $fileStatus;
    public $germplasmCount;
    public $seedCount;
    public $packageCount;
    public $errorLog;
    public $remarks;
    public $uploader;
    public $uploadTimestamp;
    public $modifier;
    public $modificationTimestamp;
    public $uploadedFile;

    public function __construct(
        public IUserDashboardConfig $userDashboardConfig,
        public IVariable $variableModel,
        public IConfig $configModel,
        UploadedFile $uploadedFile,
        $config = [])
    {
        parent::__construct($config);
        $this->uploadedFile = $uploadedFile;
    }

    public static function apiEndPoint() {
        return 'germplasm-file-uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'creator_id', 'modifier_id', 'germplasm_count', 'seed_count', 'package_count'], 'integer'],
            [['program_id', 'file_name', 'file_status','fileStatus', 'file_data', 'germplasm_count', 'seed_count', 'package_count','creation_timestamp','creator_id'], 'required'],
            [['file_name', 'file_status', 'error_log', 'remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'program', 'creationTimestamp', 'creator_id', 'program_id', 'file_name', 'file_status', 'file_data', 'germplasm_count', 'seed_count', 'package_count'], 'safe'],
            [['is_void'], 'boolean']
        ];
    }

    /**
     * Returns prefix value used for setting cookies, data provider ID, etc.
     */
    public function getPrefix()
    {
        return 'gefu-';
    }

    /**
     * Returns model name to be used by data provider
     */
    public function getModelName()
    {
        return 'GermplasmFileUpload';
    }

    /**
     * Search model for germplasm file upload
     * @param $params array list of parameters
     * @param $filters array list of additional search filters
     * 
     * @return $dataProvider array data provider for germplasm file upload browser
     */
    public function search($params, $filters){

        $attributes = [
            'germplasmFileUploadDbId',
            'programDbId',
            'programCode',
            'fileName',
            'fileData',
            'fileStatus',
            'fileUploadOrigin',
            'germplasmCount',
            'seedCount',
            'packageCount',
            'errorLog',
            'remarks',
            'uploader',
            'uploadTimestamp',
            'modifier',
            'modificationTimestamp'
        ];

        $data = [];
        $totalCount = 0;

        $browserFilters = $this->extractBrowserFilters($filters);

        $paramSort = $browserFilters['sort'];
        $paramPage = $browserFilters['page'];
        $paramLimit = $browserFilters['limit'];

        $filters = empty($browserFilters['filters']) ? '' : $browserFilters['filters'];

        $searchFilter = $paramLimit.$paramPage.$paramSort;
        $response = $this->searchAll($filters,$searchFilter,false);

        if(isset($response['data'])){
            $data = $response['data'];
            $totalCount = $response['totalCount'];
        }

        // check if need to redirect browser page
        $currPage = $browserFilters['pageCount'];
        if(intval($currPage) > 1 && count($data) == 0 && $totalCount == 0){
            // Reset pagination
            $paramPage = '&page=1';
            $getParams = \Yii::$app->request->getQueryParams();
            $gridId = $this->getPrefix().'grid';
            if (isset($getParams[$gridId . '-page'])){
                $getParams[$gridId . '-page'] = 1;
            }
            \Yii::$app->request->setQueryParams($getParams);

            $searchFilter = $paramLimit.$paramPage.$paramSort;
            $response = $this->searchAll($filters,$searchFilter,false);

            $data = $response['data'];
            $totalCount = $response['totalCount'];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'germplasmFileUploadDbId',
            'sort' => [
                'attributes' => $attributes,
                'defaultOrder' => [
                    'uploadTimestamp' => SORT_DESC,
                    'germplasmFileUploadDbId' => SORT_DESC,
                ],
            ],
            'restified' => true,
            'id' => $this->getPrefix().'grid',
            'totalCount' => $totalCount
        ]);

        return $dataProvider;
    }

    /**
     * Facilitate the extraction of browser search parameters and filters
     * 
     * @param Array $filters array of current filters
     * 
     * @return Array values to be used in searching file upload records
     */
    public function extractBrowserFilters($filters){
        
        $filters = isset($filters) && !empty($filters) ? $filters : [];

        $paramSort = '&sort=germplasmFileUploadDbId:DESC';
        $paramPage = '&page=1';
        $paramLimit = 'limit=';

        $queryParams = Yii::$app->request->getQueryParams();

        $this->load($queryParams);

        // Arrange sorting of data
        if(isset($queryParams['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $queryParams['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            foreach($sortParams as $column) {
                if($column[0] == '-') {
                    $paramSort = $paramSort . substr($column, 1) . ':DESC';
                } else {
                    $paramSort = $paramSort . $column . ':ASC';
                }
                if($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
        }

        // Arrange pagination of dataProvider
        $pageVal = 1;
        $gridPageKey = $this->getPrefix().'grid-page';
        if(isset($queryParams[$gridPageKey]) && !empty($gridPageKey)){
            $paramPage = '&page=' . $queryParams[$gridPageKey];
            $pageVal = $queryParams[$gridPageKey];
        }

        // Set page limit
        $limitVal = $this->userDashboardConfig->getDefaultPageSizePreferences();
        $paramLimit = $paramLimit.$limitVal;

        // Set browser and data filters
        if(isset($queryParams[$this->getModelName()]) && !empty($queryParams[$this->getModelName()])){
            foreach($queryParams[$this->getModelName()] as $key => $value){
                if(!empty($value)){
                    $prepend = '';

                    // if exact search value, not wildcard search
                    if(!str_contains($value, '%')){
                        $prepend = 'equals ';
                    }

                    $filters[$key] = $prepend.($key == 'fileStatus' ? strtolower($value) : $value);
                } 
            }            
        }

        return [
            'limit' => $paramLimit,
            'limitCount' => $limitVal,
            'page' => $paramPage,
            'pageCount' => $pageVal,
            'sort' => $paramSort,
            'filters' => $filters
        ];
    }

    /**
     * Facilitates retrieval of germplasm file upload status scale values
     * 
     * @return array variable configuration information
     */
    public function getFileUploadStatusValues(){
        $variableInfo = $this->variableModel->getVariableByAbbrev('GERMPLASM_FILE_UPLOAD_STATUS');
        if(!isset($variableInfo) || empty($variableInfo)){
            return [];
        }

        $scaleValuesInfo = $this->variableModel->getVariableScales($variableInfo['variableDbId']);
        if(!isset($scaleValuesInfo) || empty($scaleValuesInfo)){
            return [];
        }
            
        $scaleValues = [];
        foreach($scaleValuesInfo['scaleValues'] AS $scaleValue){
            $key = strtoupper($scaleValue["value"]);
            $scaleValues[$key] = $scaleValue["value"];
        }

        return $scaleValues;

    }

    /**
     * Build configuration abbrev based on level
     * 
     * @param String $entity entity
     * @param String $action action
     * @param String $level data level
     * @param String $levelValue data level value
     * 
     * @return $configAbbrev
     */
    public function buildConfigAbbrev($entity,$action,$level,$levelValue){
        $entity = str_replace('-','_',$entity) ?? 'germplasm';

        $level = $level ?? 'GLOBAL';
        $action = $action ?? 'UPDATE';

        $levelValue = $levelValue ?? '';
        $levelValue = $levelValue == '' || $level == 'GLOBAL' ? '' : ('_'.strtoupper($levelValue));
        
        // build configuration abbrev
        $configAbbrev = 'GM_'.strtoupper($level).($levelValue).'_'.strtoupper($entity).'_'.strtoupper($action).'_CONFIG';

        return $configAbbrev;
    }

    /**
     * Retrieve configuration values
     * 
     * @param String $entity entity of the file upload transaction (germplasm/germplasm-relation/germplasm-attribute)
     * @param String $action germplasm manager capability to be used (create/merge/update)
     * @param String $userRole user role value
     * @param String $program dashboard program value
     * @param String $cropCode crop code value for program
     * 
     * @return mixed
     */
    public function getConfigValues($entity,$action,$userRole,$program,$cropCode){
        $configValues = [];

        // retrieve level-specific config
        // if none, go up the heirarchy (role -> program -> crop -> global)
        $configOrder = ['role','program','crop','global'];
        $level = 'ROLE';
        $levelValue = $userRole;

        foreach($configOrder as $order){
            $level = $order;

            if($order == 'program'){ $levelValue = $program; }
            if($order == 'global') { $levelValue = ''; }
            if($order == 'crop'){ $levelValue = $cropCode; }    

            $configAbbrev = $this->buildConfigAbbrev(
                $entity,
                $action,
                strtoupper($level),
                strtoupper($levelValue));

            $configValues = $this->configModel->getConfigByAbbrev($configAbbrev);

            if( isset($configValues) && !empty($configValues) && 
                isset($configValues['values']) && !empty($configValues['values']) ){
                break;
            }
        }

        return [
            'level' => $level,
            'levelValue' => $levelValue,
            'configValues' => $configValues   
        ];
    }

    /**
     * Retrieve variables from configuration record matching entity, action, and level
     * 
     * @param Array $configValues configuration values
     * 
     * @return mixed
     */
    public function getFileUploadVariables($configValues){
        
        // if none, return empty
        if(!isset($configValues) || empty($configValues) || !isset($configValues['values']) || empty($configValues['values'])){
            return [];
        }

        // parse config variables
        $variables = [];
        $requiredVariables = [];

        $exists = true;

        foreach($configValues['values'] as $key=>$record){
            $variableInfo = $this->variableModel->getVariableByAbbrev($record['abbrev']);
            $exists = isset($variableInfo) && !empty($variableInfo);
    
            if($exists && !empty($record['abbrev'])){
                array_push($variables,$record['abbrev']);
            }

            if( $exists && !empty($record['usage']) && $record['usage'] == 'required'){
                array_push($requiredVariables,$record['abbrev']);
            }
        }

        return [
            'variables' => $variables,
            'requiredVariables' => $requiredVariables
        ];
    }

    /**
     * Facilitate data retrieval for uploaded file
     * 
     * @param String $fileReference reference name of uploaded file
     * 
     * @return Array array of file column headers
     */
    public function retrieveUploadedFile($fileReference){
        $success = true;
        $errorMsg = '';

        try{
            $uploadPath = realpath(Yii::$app->basePath) . '/uploads/';
            $file = $this->uploadedFile->getInstanceByName($fileReference);

            if ($file->error === 1) {
                $maxUpload = (int) (ini_get('upload_max_filesize'));
                return [
                    'success' => false,
                    'error' => 'The uploaded file exceeds the maximum filesize of ' . $maxUpload . 'MB.'
                ];
            }

            $tmpFileName = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $tmpFileName = $uploadPath . '/' . $tmpFileName;

            $file->saveAs($tmpFileName);
            $csv = new \app\components\CsvReader($tmpFileName);
            $csv->setHasHeader(true);

            $fileName = $file->name;
            
            $columnHeaders = array_map('strtoupper',$csv->getHeader());
            $content = [];

            if(isset($csv->read()[0])){
                $columnHeaders = array_map('strtoupper',array_keys($csv->read()[0]));
                foreach ($csv as $line) {
                    array_push($content,$line);
                }
            }
            else{
                $success = false;
                $errorMsg = 'Invalid file upload. Please check if file has content data.';
            }

            return [
                'success' => $success,
                'headers' => $columnHeaders,
                'fileContent' => $content,
                'fileName' => $fileName,
                'error' => $errorMsg
            ];
        }
        catch(\Exception $e){
            $errorMsg = $e->getMessage();

            if ($e->getMessage() == 'max(): Array must contain at least one element') {
                $errorMsg = 'Please check if file is not empty.';
            }

            if( strpos($e->getMessage(),'The header must be with unique string values.') !== -1){
                $errorMsg .= '. Kindly use the CSV template as reference.';
            }

            return [
                'success' => false,
                'error' => Yii::t('app', $errorMsg)
            ];
        }
    }

    /**
     * Facilitates creation of new germplasm merge file upload record
     * @param Array $paramRequest parameter body build on controller
     * 
     * @return mixed
     */
    public function saveUploadedFile($paramRequest){
        // build request body parameters
        $params = [
            "records" => [$paramRequest]
        ];

        // call API resource call
        $method = 'POST';
        $endpoint = 'germplasm-file-uploads';
        $requestBody = json_encode($params);

        // Run call
        $result = \Yii::$app->api->getResponse($method, $endpoint, $requestBody);

        if($result['status'] != 200){
            return [
                'success' => false,
                'error' => 'Error encountered creating file upload record.'
            ];
        }

        $fileUploadId = $result['body']['result']['data'][0]['germplasmFileUploadDbId'];
        
        return [
            'success' => true,
            'error' => '',
            'id' => $fileUploadId
        ];
    }
}

?>