<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;
use app\models\Germplasm;
use app\models\UserDashboardConfig;
use app\interfaces\models\IGermplasm;
use app\interfaces\models\IGermplasmSearch;

class GermplasmSearch extends Germplasm implements IGermplasmSearch
{
    const GERMPLASM_MANAGER_PERMISSIONS = [
        'GERMPLASM_SEARCH',
        'GERMPLASM_CREATE',
        'GERMPLASM_MERGE',
        'GERMPLASM_UPDATE',
        'GERMPLASM_CODING'
    ];

    public function __construct (
        public IGermplasm $germplasmModel
    )
    { }

    /**
     * Search model for germplasm
     *
     * @param $params array list of parameters
     * @param $queryParams array list of query parameters
     *
     * @return $dataProvider array germplasm browser dataprovider
     */
    public function search($params, $queryParams=null) {
        $filters = '';
        // Save search params to session 
        Yii::$app->session->set('germplasm-search-query-params', $queryParams);
        Yii::$app->session->set('germplasm-search-browser-filters', $params['GermplasmSearch'] ?? []);

        if(!empty($queryParams)) {
            $filters = $queryParams;
 
            if (isset($filters['names'])) {
                if (isset($filters['names']['equals'])) {
                    $filters['names'] = $filters['names']['equals'];
                } else {
                    unset($filters['names']);
                }
            }
        }
        if(isset($params['GermplasmSearch'])) {
            $tempFilters = [];
            foreach ($params['GermplasmSearch'] as $key => $value) {
                if(!empty($value) && isset($value)){
                    if($key == 'otherNames') {
                        $tempFilters[$key] = [
                            'like' => '%'.$value.'%'
                        ];
                    } else {
                        $tempFilters[$key] = [
                            'like' => $value
                        ];
                    }
                }
            }
            if (!empty($tempFilters)) {
                if (!empty($filters)) {
                    $filters['__browser_filters__'] = $tempFilters;
                } else {
                    $filters = $tempFilters;
                }
            }
        }

        $sort = new Sort();
        $sort->attributes = ['germplasmDbId', 'creationTimestamp', 'creator', 'designation',
            'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames',
            'parentage', 'germplasmNameType', 'germplasmState', 'germplasmNormalizedName',
            'germplasmCode', 'taxonomyName', 'cropCode', 'orderNumber'=>[
                "asc" => ["orderNumber"=>SORT_ASC],
                'desc' => ["orderNumber"=>SORT_DESC]
            ]
        ];
        $sort->defaultOrder = [
            'orderNumber'=>SORT_ASC
        ];

        $paramSort = '';
        if(isset($_GET['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach($sortParams as $column) {
                if($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            $tempOrder = ['orderNumber'=>SORT_ASC];
            $tempOrder = array_merge($tempOrder, $sortOrder);
            $sort->setAttributeOrders($tempOrder);
            $sort->defaultOrder = $tempOrder;
        }

        $dataProviderId = 'germplasm-dp';
        $paramPage = '';
        $pageCount = 1;
        if (isset($_GET[$dataProviderId.'-page'])){
            $pageCount = $_GET[$dataProviderId.'-page'];
            $paramPage = '&page=' . $pageCount;
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $pageCount = $_GET['page'];
        }

        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $filters = empty($filters)? '': $filters;

        $response = Yii::$app->api->getResponse('POST', 'germplasm-search?'.$paramLimit.$paramPage.$paramSort, json_encode($filters));

        if ($pageCount != 1 && $response['body']['metadata']['pagination']['totalCount'] == 0) {
            $response = Yii::$app->api->getResponse('POST', 'germplasm-search?'.$paramLimit.$paramSort.'&page=1', json_encode($filters));
            $_GET[$dataProviderId.'-page'] = 1; // return browser to page 1
        }

        $output = $response;
        $data = $output['body']['result']['data'];
        $tempData = [];
        foreach ($data as $index => $d) {
            $tempData[$index] = $d;
            $tempData[$index]['orderNumber'] = $index;

            $entriesInfo = $this->germplasmModel->getGermplasmEntriesCount($d['germplasmDbId']);
            $seedsInfo = $this->germplasmModel->getGermplasmSeedsCount($d['germplasmDbId']);

            $tempData[$index]['entriesCount'] = isset($entriesInfo) && !empty($entriesInfo) ? intval($entriesInfo[0]['entriesCount']) : 1;
            $tempData[$index]['seedsCount'] = isset($entriesInfo) && !empty($entriesInfo) ? intval($seedsInfo[0]['seedsCount']) : 1;
        }

        $data = $tempData;
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'orderNumber',
            'restified' => true,
            'totalCount' => $output['body']['metadata']['pagination']['totalCount'],
            'id' => $dataProviderId
        ]);
        $dataProvider->setSort($sort);

        $this->load($params);

        // Check for Select All
        if($filters != Yii::$app->session['germplasm-api_filter']) {
            Yii::$app->session->set('germplasm-api_filter', $filters);
            Yii::$app->session->set('germplasm-total_results_count', $output['body']['metadata']['pagination']['totalCount']);
            Yii::$app->session->set('germplasm-select_all_flag', 'include mode');
            Yii::$app->session->set('germplasm-selected_ids', []);
        }

        return $dataProvider;
    }

    /**
     * Retrieves germplasm information
     *
     * @param $entityId integer germplasm identifier
     *
     * @return $output array germplasm information
     */
    public static function getGermplasmById($entityId) {
        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$entityId);

        return isset($response['body']['result']['data'][0])? $response['body']['result']['data'][0] : [];


    }

    /**
     * Retrieve column names and datatypes
     *
     * @return $columnNameAndDatatypes array of objects containing the column names and their datatypes
     * for the advanced filters and multiple sort
     */
    public function getColumnNameAndDatatypes() {
        return array(
            'designation' => 'String',
            'otherNames' => 'String',
            'parentage' => 'String',
            'generation' => 'String',
            'creator' => 'String',
            'creationTimestamp' => 'Timestamp',
            'modifier' => 'String',
            'modificationTimestamp' => 'Timestamp',
            'germplasmNameType' => 'String',
            'germplasmState' => 'String',
            'germplasmNormalizedName' => 'String',
            'germplasmCode' => 'String',
            'taxonomyName' => 'String',
            'cropCode' => 'String'
        );
    }

    /**
     * Retrieve Germplasm Manager permissions
     */
    public function getGermplasmManagerPermissions()
    {
        return self::GERMPLASM_MANAGER_PERMISSIONS ?? [];
    }
}