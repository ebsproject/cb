<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\data\Sort;
use app\dataproviders\ArrayDataProvider;

use app\models\BaseModel;
use app\interfaces\models\IConfig;

/**
 * This is the class for working lists
 */

class WorkingList extends BaseModel
{
    private $configModel;

    public function __construct(IConfig $configModel){
        $this->configModel = $configModel;
    }

    /**
     * Retrieve column names and datatypes
     *
     * @return $columnNameAndDatatypes array of objects containing the column names and their datatypes
     * for the advanced filters and multiple sort
     */
    public function getColumnNameAndDatatypes() {
        // Get config from abbrev
        $config = $this->configModel->getConfigByAbbrev('WORKING_LIST_VARIABLES_CONFIG_SYSTEM_DEFAULT');
        
        $attrColumn = [
            'SEED_CODE' => 'GID',
            'GERMPLASM_NAME' => 'designation',
            'PACKAGE_LABEL' => 'label',
            'PROGRAM' => 'seedManager'
        ];
        
        $configValues = [];
        if(isset($config) && !empty($config)){
            $configValues = $config['values'];
        }

        $columns = [];

        foreach ($configValues as $keyConfig => $configVal) {
            // Check if browser columns is true
            if($configVal['browser_column'] == "true"){
                $attribStr = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($configVal['abbrev'])))));
                $attribStr = isset($attrColumn[$configVal['abbrev']]) ? $attrColumn[$configVal['abbrev']] : $attribStr;
                $columns[$attribStr] = 'String';
            }
        }

        return $columns;
    }
}