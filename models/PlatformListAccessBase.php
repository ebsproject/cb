<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.list_access".
 *
 * @property int $id Primary key of the record in the table
 * @property int $list_id ID of the list shared with other users
 * @property int $user_id ID of user where the list is shared with
 * @property string $permission Permission granted to users in accessing the list; this can be read, or read-write permission
 * @property string $remarks Additional details about the list permission
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $record_uuid
 */
class PlatformListAccessBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.list_access';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_id', 'user_id', 'permission'], 'required'],
            [['list_id', 'user_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['list_id', 'user_id', 'creator_id', 'modifier_id'], 'integer'],
            [['permission', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlatformList::className(), 'targetAttribute' => ['list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'list_id' => 'List ID',
            'user_id' => 'User ID',
            'permission' => 'Permission',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
