<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use app\models\BaseModel;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * This is the model class for table "master.product".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property string $display_name
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 *
 * @property MasterPlaceSeason[] $masterPlaceSeasons
 * @property MasterPlace[] $places
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property OperationalCrossList[] $operationalCrossLists
 * @property OperationalStudy[] $operationalStudies
 * @property WarehouseStudy[] $warehouseStudies
 */

class Product extends BaseModel
{
    public $program;
    public $creationTimestamp;
    public $creator;
    public $designation;
    public $generation;
    public $modificationTimestamp;
    public $modifier;
    public $nameType;
    public $otherNames;
    public $parentage;
    public $programName;
    public $germplasmNameType;
    public $germplasmState;
    public $germplasmNormalizedName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'season_id', 'cross_id', 'creator_id', 'modifier_id', 'gid', 'product_gid_id', 'seed_storage_id', 'organization_id', 'institute_id'], 'integer'],
            [['designation', 'name_type', 'product_type', 'parentage', 'generation'], 'required'],
            [['designation', 'name_type', 'product_type', 'mta_status', 'parentage', 'generation', 'iris_preferred_id', 'breeding_line_name', 'derivative_name', 'fixed_line_name', 'selection_method', 'notes', 'ip_rights'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'program', 'creationTimestamp', 'creator', 'germplasmNameType', 'germplasmState', 'germplasmNormalizedName', 'designation', 'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames', 'parentage', 'programName'], 'safe'],
            [['is_void'], 'boolean'],
            [['display_name', 'product_key', 'system_product_name'], 'string', 'max' => 256],
            [['ip_status'], 'string', 'max' => 32],
            [['product_status'], 'string', 'max' => 255],
            [['product_key'], 'unique'],
        ];
    }

    /**
     * Updates product given id and array of data
     *
     * @param $id integer product identifier
     * @param $data array array of values
     *
     * @return $response text return message of API call
     */
    public function updateProduct($id, $data) {
        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
        $productInfo = [];
        $productInfo['records'] = [[
            'designation' => $data['designation'],
            'generation' => $data['generation'],
            'nameType' => $data['nameType'],
            'parentage' => $data['parentage'],
            'productType' => $data['productType'],
            'programDbId' => $data['programDbId'],
            'remarks' => $data['remarks'],
            'seasonDbId' => $data['seasonDbId'],
            'year' => $data['year']
        ]];

        try {
            $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

            $client = new \GuzzleHttp\Client([
                'base_uri' => getenv('CB_API_V3_URL'),
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Content-Type' => 'application/json'
                ]
            ]);

            $response = $client->request('PUT', 'products/'.$id ,['body'=>json_encode($productInfo)]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }

        if($response->getStatusCode() == 200) {
            return true;
        } else {
            return json_encode($response->getBody()->getContents());
        }
    }
}
