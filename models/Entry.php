<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

use app\models\EntryData;
use app\models\EntryList;
use app\models\ExperimentModel;
use app\models\Experiment;
use app\models\Occurrence;


use app\dataproviders\ArrayDataProvider;

/**
 * This is the model class for table "experiment.entry".
 *
 * @property int $id Entry ID: Database identifier of the entry [ENTRY_ID]
 * @property string $entry_code Entry Code: Textual identifier of the entry in the experiment [ENTRY_CODE]
 * @property int $entry_number Entry Number: Member number of the entry in the experiment [ENTRY_NO]
 * @property string $entry_name Entry Name: Name of the germplasm as an entry in the experiment [ENTRY_NAME]
 * @property string $entry_type Entry Type: Usage of the entry in the experiment {test, check, control, filler, parent, cross} [ENTRY_TYPE]
 * @property string|null $entry_role Entry Role: Function of the entry in the experiment, which may depend on the type of the experiment {female, male, female-and-male, spatial check, performance check} [ENTRY_ROLE]
 * @property string|null $entry_class Entry Class: Grouping class of the entry, which is mainly used in data analysis [ENTRY_CLASS]
 * @property string $entry_status Entry Status: Status condition of the entry in the experiment {draft, active} [ENTRY_STATUS]
 * @property string|null $description Entry Description: Additional information about the entry in the experiment [ENTRY_DESC]
 * @property int $entry_list_id Entry List ID: Reference to the entry list where the entry is added [ENTRY_ENTLIST_ID]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm under observation/test as an entry in the experiment [ENTRY_GERM_ID]
 * @property int|null $seed_id Seed ID: Reference to the seed that will represent the germplasm as an entry in the experiment [ENTRY_SEED_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class Entry extends BaseModel {

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'entries';
    }

    public function __construct (
        public EntryList $entryList,
        public Experiment $experiment,
        public Occurrence $occurrence
    ) { }

    /**
     * Use HTTP requests to GET, PUT, POST and DELETE data
     * 
     * @param method string request method(POST, GET, PUT, etc)
     * @param path string url path request
     * @param rawData json optional request content
     * 
     * @return array list of requested information 
     */
    public function getApiResponse($method, $path, $rawData = null){

        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');

        try {

            $client = new \GuzzleHttp\Client([
                'base_uri' => getenv('CB_API_V3_URL'), 
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Content-Type' => 'application/json'
                ]
            ]);

            $response = $client->request($method, $path, ['body' => $rawData]);

         }
         catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
         }

        return json_decode($response->getBody()->getContents(),true);

    }

    /**
     * Checks if data is suppressed or not
     * 
     * @param array data to be checked
     * @return string metadata value | suppressed(S) label
     */
    public function checkIfSuppressed($data){

        $metadata = isset($data['variableValue']) ? $data['variableValue'] : NULL;

        if(isset($metadata, $data['isSuppressed']) && $data['isSuppressed']){
            $metadata = '<span class="suppressed">'.$metadata.'</span>';
        }

        return $metadata;
    }

    /**
     * Returns seed sources from a specific entry db id
     * @param entryDbId integer entry identifier
     */
    public function getSeedSources($entryDbId){
        $data = $this->getApiResponse('GET',"entries/$entryDbId/seed-sources");
        $data = isset($data['result']['data'][0]['entryDbId']) ? $data['result']['data'][0] : [];

        $result = isset($data['seedStorageLabel']) ? ['seedStorageLabel' => $data['seedStorageLabel']] : [];        
        
        if(isset($data['seedSources'])){
            $seedSources = $data['seedSources'];
            foreach ($seedSources as $value) {
                if($value['variableAbbrev'] == 'SOURCE_STUDY' || $value['variableAbbrev'] == 'SOURCE_ENTRY'){
                    $result[$value['variableAbbrev']] = $value['variableValue'];
                } 
            }
        }

        return $result;
    }

    /**
     * Retrieve entry information
     *
     * @param Integer $experimentId experiment record identifier
     * @param Array $configFields list of configuration attributes
     * @param Array $params string optional data filters
     * @param Boolean $noPagination optional flag when grid view pagination should be set or not
     * @param String $source view source of the action to perform
     * @param String $pageParams data browser parameters
     * @param Interger $currentPage data browser current page number
     */
    public function getDataProvider($experimentId, $configFields, $params = null, $noPagination = false, $source=null, $pageParams = '', $currentPage = null)
    {

        $params2 = null;
        
        if(!empty($source)){ //Planting arrangement
            $allEntryIds = [];
            $entries = [];
            extract($params);

            $entryListId = $this->entryList->getEntryListId($experimentId);
    
            //set api request
            $method = 'POST';
            $path = 'entries-search';
            if(!empty($params) && count($params) > 1){
            
                $entryRecCheck = Entry::searchAll(["fields"=>"entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId|entry.entry_number AS entryNumber|germplasm.designation|germplasm.parentage|germplasm.generation|entry.entry_class AS entryClass|entry.description|entry.entry_type AS entryType|entry.entry_role AS entryRole", "entryListDbId"=>"equals ".$entryListDbId], 'limit=1', false);
                
                $tempKeys = array_keys($params);
                foreach($tempKeys as $val){
                    
                    if(!isset($entryRecCheck['data'][0][$val])){
                        $params2[$val] = str_replace("equals ",'',$params[$val]);
                        unset($params[$val]);
                    }
                }

                $rawData = $params;
                $rawData['entryListDbId'] = "equals $entryListId";
                // build request body based on params
                $filterFields = 'entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId';
                foreach($params as $key => $val) {
                    $fieldColumn = '';
                    switch($key) {
                        case 'entryNumber':
                            $fieldColumn = 'entry.entry_number AS entryNumber';
                            break;
                        case 'seedCode':
                            $fieldColumn = 'seed.seed_code AS seedCode';
                            break;
                        case 'seedName':
                            $fieldColumn = 'seed.seed_name AS seedName';
                            break;
                        case 'designation':
                            $fieldColumn = 'germplasm.designation';
                            break;
                        case 'parentage':
                            $fieldColumn = 'germplasm.parentage';
                            break;
                        case 'generation':
                            $fieldColumn = 'germplasm.generation';
                            break;
                        case 'entryClass':
                            $fieldColumn = 'entry.entry_class AS entryClass';
                            break;
                        case 'description':
                            $fieldColumn = 'entry.description';
                            break;
                        case 'entryType':
                            $fieldColumn = 'entry.entry_type AS entryType';
                            break;
                        case 'entryRole':
                            $fieldColumn = 'entry.entry_role AS entryRole';
                            break;
                        case 'parentRole':
                            $fieldColumn = 'entry.entry_role AS parentRole';
                            break;
                        case 'packageLabel':
                            $fieldColumn = 'package.package_label AS packageLabel';
                            break;
                        case 'germplasmCode':
                            $fieldColumn = 'germplasm.germplasm_code AS germplasmCode';
                            break;
                        case 'packageQuantity':
                            $fieldColumn = 'package.package_quantity AS packageQuantity';
                            break;
                        case 'packageUnit':
                            $fieldColumn = 'package.package_unit AS packageUnit';
                            break;
                        default:
                            $fieldColumn = '';
                            break;
                    }

                    if(!empty($fieldColumn)){
                        $filterFields = $filterFields."|$fieldColumn";
                    }
                }
                $rawData['fields'] = $filterFields;
            }else{
                $rawData = ([
                    'fields' => 'entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId',
                    'entryListDbId' => "equals $entryListId"
                ]);
            }

            $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), null, true);
           
            if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                $entries = $results['body']['result']['data'];

            }

            $allEntryIds = array_column($entries, 'entryDbId');
            if($pageParams == null){
                $pageParams = '?sort=entryNumber:asc';
            } elseif (strpos($pageParams, 'sort') !== false){
                $sortStr = explode('sort', $pageParams);
                if(strpos($sortStr[1], 'entryNumber') === false){
                    $pageParams .= '|entryNumber:asc';
                }
            } else {
                $pageParams .= '&sort=entryNumber:asc';
            }

            //set api request
            $path = 'entries-search'.$pageParams;
            $rawData = ([
                'entryListDbId' => "equals $entryListId",
                'entryDbId' => !empty($allEntryIds)? 'equals '.implode('|equals ', $allEntryIds): 'equals 0'
            ]);

            $rawData['fields'] = "entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId|entry.entry_number AS entryNumber|germplasm.designation|germplasm.parentage|germplasm.generation|entry.entry_class AS entryClass|entry.description|entry.entry_type AS entryType|entry.entry_role AS entryRole";

            $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), null, false);
            
            $entryRecords = [];
            $entryRecords['totalCount'] = 0;
            if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                $entries = $results['body']['result']['data'];
                $entryRecords['totalCount'] = $results['body']['metadata']['pagination']['totalCount'];
            }
        }else{
            $entryRecords = $this->getEntries($experimentId, $params, null, $pageParams, $currentPage);
            $entries = $entryRecords['entries'];
            $allEntryIds = $entryRecords['allEntryIds'];
        }
        
        $entryDataColumns = [];

        if(!empty($entries) && !empty($configFields)){
            foreach ($configFields as $configField) {
                if(!isset($configField['is_hidden'])){
                    $index = strtolower($configField['variable_abbrev']);

                    $columnName = preg_replace_callback('/_([a-z]?)/', function($match) {return strtoupper($match[1]);}, $index);
                    
                    if(!array_key_exists($columnName, $entries[0])){
                        $entryDataColumns[] = $index;
                    }
                }
            }
            
            if(!empty($entryDataColumns)){
            
                foreach ($entries as $key => $value) {
                    $entryDataList = EntryData::getEntryDataValue($value['entryDbId']);
                  
                    foreach ($entryDataColumns as $entryDataColumn) {
                        $tempEntryDataCol = $entryDataColumn;
                        
                        if(strpos($entryDataColumn,'_') !== false){
                            $entryDataColumn = ucwords(strtolower($entryDataColumn), '_');
                            $entryDataColumn = str_replace('_', '', $entryDataColumn);
                            $entryDataColumn = lcfirst($entryDataColumn);
                        }
                      
                        if(!empty($source)){
                            if(!empty($params2)){
                              
                                if($params2[$entryDataColumn] == $entryDataList[$tempEntryDataCol]){
                                  $entries[$key][$entryDataColumn] = $entryDataList[$tempEntryDataCol];
                                }else{
                                  unset($entries[$key]);
                                }
                             }else{
                                 $entries[$key][$entryDataColumn] = isset($entryDataList[$tempEntryDataCol]) ?? '';
                             }
                        }else{
                            if(isset($entryDataList[$tempEntryDataCol])){
                                $entries[$key][$tempEntryDataCol] = $entryDataList[$tempEntryDataCol];
                            }
                        }
                        
                    }
                }
            }
        }

        $entryColumns = [
            'entryDbId',
            'entryNumber',
            'entryCode',
            'entryName',
            'entryType',
            'entryRole',
            'entryClass',
            'entryStatus',
            'description',
            'entryListDbId',
            'germplasmDbId',
            'designation',
            'germplasmCode',
            'parentage',
            'generation',
            'germplasmState',
            'seedDbId',
            'seedCode',
            'seedName',
            'sourceEntryDbId',
            'sourceOccurrenceDbId',
            'notes',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier',
            'availability',
            'repno',
            'packageLabel',
            'packageQuantity',
            'packageUnit'
        ];
        
        if(!empty($entryDataColumns)){
            $attributes = array_merge($entryColumns, $entryDataColumns);
        }else{
            $attributes = $entryColumns;
        }
        $attributes = array_diff($attributes, ['parent_type']); // Temporarily disable filtering and sorting for parent_type column in Intentional Crossing Nursery
        $sortAttributes = array_diff($attributes, ['availability']); // Disable sorting for availability column

        $providerDetails = [
            'allModels' => $entries,
            'key' => 'entryDbId',
            'restified' => true,
            'totalCount' => $entryRecords['totalCount'],
            'id' => "grid-entry-list-grid"
        ];
        
        $dataProvider = new ArrayDataProvider($providerDetails);
        
        $dataProvider = [
            'dataProvider' => $dataProvider,
            'attributes' => $attributes,
            'entryColumns' => $entryColumns,
            'entryDataColumns' => $entryDataColumns,
            'entryDbIds' => $allEntryIds
        ];
        
        return $dataProvider;

    }

     /**
     * Create simple data provider for entries
     * 
     * @param Integer $experimentId experiment record identifier
     * 
     */
    public function getSimpleDataProvider($experimentId){
        $rawData['fields'] = "entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|
            entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId|entry.entry_number AS entryNumber|
            germplasm.designation|germplasm.parentage|germplasm.generation|entry.entry_class AS entryClass|
            entry.description|entry.entry_type AS entryType|entry.entry_role AS entryRole|
            experiment.id as experimentDbId|entry.entry_name AS entryName|germplasm.germplasm_state AS germplasmState|germplasm.germplasm_type AS germplasmNameType|
            entry.entry_class AS entryClass|entry.entry_code AS entryCode";

        $rawData['experimentDbId'] = "equals ".$experimentId;

        $results = Yii::$app->api->getResponse("POST", "entries-search", json_encode($rawData), null, false);

        $entries = [];
        $totalCount = 0;
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
            $entries = $results['body']['result']['data'];
            $totalCount = $results['body']['metadata']['pagination']['totalCount'];
        }
        $providerDetails = [
            'allModels' => $entries,
            'key' => 'entryDbId',
            'restified' => true,
            'totalCount' => $totalCount,
            'id' => "grid-entry-list-grid"
        ];
        
        $dataProvider = new ArrayDataProvider($providerDetails);
        
        return $dataProvider;

    }

    /**
     * Method for retrieving all the entry data records from a given endpoint using the POST search method
     * 
     * @param Integer $dbId entry record identifier
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @param boolean $retrieveAll Flag to indicate if API will retrieve all results of by pagination
     * @return Array the records from the database
     */
    public static function searchAllData($dbId, $params=null, $filters=null, $retrieveAll=true){

        $apiEndPoint = static::apiEndPoint();
        $apiSearchEndpoint = 'data-search';
        $method = 'POST';
        $path = $apiEndPoint . '/' . $dbId. '/' .$apiSearchEndpoint;

        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, $retrieveAll);


    }

    /**
     * Create records for entry
     *
     * @param $data array data for creating the plan template
     * @return $id integer ID of the created plan template
     */
    public static function createRecords($data){
        $result = null;
        $postData['records'] = $data;
        $responseApi = Yii::$app->api->getResponse('POST','entries',json_encode($postData));
    
        if($responseApi['status']==200 && isset($responseApi['body']['result']['data'])){
            $result = $responseApi['body']['result']['data'];
        }

        return $result;
    }

    /**
     * Search for entries given search parameters
     *
     * @param array $params list of search parameters
     * @return array $data list of entries given search parameters
     */
    public static function searchRecords($params = null, $path = null){

        if($path == null){
          $path =  'entries-search';
        }
        
        $results = Yii::$app->api->getResponse('POST',$path, json_encode($params), null, true);
        
        $data = [];

        // check if successfully retrieved entries
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])){
            $data = $results['body']['result']['data'];
        }

        return $data;
    }

    /**
     * Update entry record
     *
     * @param $id record ID of the entry
     * @param $params array list of search parameters
     * @return $data array list of entries updated
     */
    public static function updateRecord($id, $params = null){
        
        $response = Yii::$app->api->getResponse('PUT', 'entries/'.$id, json_encode($params));
        
        $data = [];

        // check if successfully retrieved germplasm
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
            $data = $response['body']['result']['data'];
        }
        
        return $data;
    }

    /**
    * Retrive entries in the experiment using experiment id
    * 
    * @param integer $experimentId experiment record identifier
    * @param array $params optional array list of data filters
    * @param array $filter filter conditions
    * @param string $pageParams data browser parameters
    * @param integer $currentPage data browser current page number
    */
    public function getEntries($experimentId, $params = [], $filter = null, $pageParams = '', $currentPage = null, $retrieveAll = false){
        //get entry list id
        $entryListId = $this->entryList->getEntryListId($experimentId);

        $allEntryIds = [];
        $entries = [];

        $availability = NULL;
        if(!empty($params)){
            if(in_array("availability", array_keys($params))){
                $availability = $params['availability'];
                unset($params['availability']);
            }
        }
        //set api request
        $method = 'POST';
        $path = 'entries-search';
        if(!empty($params)){
            $rawData = array_merge(['entryListDbId' => "equals $entryListId"], $params);
            // build request body based on params
            $filterFields = 'entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.package_id AS packageDbId|entry.germplasm_id AS germplasmDbId|entry.entry_number AS entryNumber';
            foreach($params as $key => $val) {
                $fieldColumn = '';
                switch($key) {
                    case 'entryNumber':
                        $fieldColumn = '';
                        break;
                    case 'seedCode':
                        $fieldColumn = 'seed.seed_code AS seedCode';
                        break;
                    case 'seedName':
                        $fieldColumn = 'seed.seed_name AS seedName';
                        break;
                    case 'designation':
                        $fieldColumn = 'germplasm.designation';
                        break;
                    case 'germplasmCode':
                        $fieldColumn = 'germplasm.germplasm_code AS germplasmCode';
                        break;
                    case 'parentage':
                        $fieldColumn = 'germplasm.parentage';
                        break;
                    case 'generation':
                        $fieldColumn = 'germplasm.generation';
                        break;
                    case 'germplasmState':
                        $fieldColumn = 'germplasm.germplasm_state AS germplasmState';
                        break;
                    case 'entryClass':
                        $fieldColumn = 'entry.entry_class AS entryClass';
                        break;
                    case 'description':
                        $fieldColumn = 'entry.description';
                        break;
                    case 'entryType':
                        $fieldColumn = 'entry.entry_type AS entryType';
                        break;
                    case 'entryRole':
                        $fieldColumn = 'entry.entry_role AS entryRole';
                        break;
                    case 'parentRole':
                        $fieldColumn = 'entry.entry_role AS parentRole';
                        break;
                    case 'packageLabel':
                      $fieldColumn = 'package.package_label AS packageLabel';
                      break;
                    case 'packageQuantity':
                        $fieldColumn = 'package.package_quantity AS packageQuantity';
                        break;
                    case 'packageUnit':
                        $fieldColumn = 'package.package_unit AS packageUnit';
                        break;
                }

                if(!empty($fieldColumn)){
                    $filterFields = $filterFields."|$fieldColumn";
                }
            }
            $rawData = array_merge(['fields' => $filterFields], $rawData);
        }else{
            $rawData = ([
                'fields' => 'entry.entry_list_id AS entryListDbId|entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.germplasm_id AS germplasmDbId|entry.entry_number AS entryNumber
                             |germplasm.germplasm_state AS germplasmState|germplasm.germplasm_type AS germplasmNameType|entry.package_id AS packageDbId',
                'entryListDbId' => "equals $entryListId"
            ]);
        }

        //check availability filter
        if($availability != NULL){
            $filteredEntries = array();
            if($availability == 'selected'){
                $rawData['packageDbId'] = 'is not null';
                $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), 'sort=entryNumber:ASC', true);

                if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                    $filteredEntries = $results['body']['result']['data'];
                }
            } else {
                $rawData['packageDbId'] = 'is null';
                $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), 'sort=entryNumber:ASC', true);

                if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                    $entries = $results['body']['result']['data'];
                }

                //get germplasmDbIds
                $germplasmIds = array_column($entries, 'germplasmDbId');

                $experimentModel = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|program.program_code AS programCode",'experimentDbId'=>"equals ".$experimentId]);

                $filterPackages = [
                    "fields"=>"seed.id AS seedDbId|germplasm.id AS germplasmDbId|program.program_code AS programCode|package.id as \"packageDbId\"",
                    "germplasmDbId" => "equals ".implode("|equals ", $germplasmIds),
                    "programCode" => "equals ".$experimentModel['data'][0]['programCode'],
                    "distinctOn"=>"germplasmDbId",
                    "addPackageCount"=>true
                ];
                $packagesArray = Seed::searchPackageInfo($filterPackages, '',true);
                $packageInfo = $packagesArray['body']['result']['data'];
                $germplasmIdArray = array_column($packageInfo, 'germplasmDbId');

                foreach($entries as $entry){
                    $idx = array_search($entry['germplasmDbId'], $germplasmIdArray);
                    if($idx !== FALSE && isset($packageInfo[$idx]['packageCount'])) {
                        if($availability == 'select' && (int)$packageInfo[$idx]['packageCount'] > 0){    // $availability == 'select'
                            $filteredEntries[] = $entry;
                        } else if($availability == 'unavailable' && (int)$packageInfo[$idx]['packageCount'] === 0){   // $availability == 'unavailable'
                            $filteredEntries[] = $entry;
                        }
                    } else {    // $availability == 'unavailable'
                        if($availability == 'unavailable') $filteredEntries[] = $entry;
                    }
                }
            }

            $entries = $filteredEntries;
        } else {
            $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), 'sort=entryNumber:ASC', true);

            if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){
                $entries = $results['body']['result']['data'];
            }
        }

        $allEntryIds = array_column($entries, 'entryDbId');

        //set api request
        
        $rawData = ([
            'entryListDbId' => "equals $entryListId",
            'entryDbId' => !empty($allEntryIds)? 'equals '.implode('|equals ', $allEntryIds): 'equals 0'
        ]);

        if($pageParams == NULL){
            $pageParams = '?sort=entryNumber:asc';
        }else if(strpos($pageParams, 'sort') !== false){
            $sortStr = explode('sort', $pageParams);
            if(strpos($sortStr[1], 'entryNumber') === false){
                $pageParams .= '|entryNumber:asc';
            }
        } else {
            $pageParams .= '&sort=entryNumber:asc';
        }

        $path = 'entries-search'.$pageParams;
        $results = Yii::$app->api->getResponse($method, $path, json_encode($rawData), $filter, $retrieveAll);

        $totalCount = 0;
        if (isset($results['status']) && $results['status'] == 200){
            $pagination = $results['body']['metadata']['pagination'];
   
            if(isset($_GET['grid-entry-list-grid-page'])){
                $currentPage =  $_GET['grid-entry-list-grid-page'];
            }

            if(isset($currentPage) && $currentPage > $pagination['totalPages']) {
                $pageParams2 = str_replace("&page=$currentPage",'', $pageParams);
                $path = 'entries-search'.$pageParams2;
                $results2 = Yii::$app->api->getResponse($method, $path, json_encode($rawData), $filter, false);
                $_GET['grid-entry-list-grid-page'] = 1; // return browser to page 1

                if (isset($results2['status']) && $results2['status'] == 200) {
                    $entries = $results2['body']['result']['data'];
                    $totalCount = $results2['body']['metadata']['pagination']['totalCount'];
                }
            } else {
                $entries = $results['body']['result']['data'];
                $totalCount = $results['body']['metadata']['pagination']['totalCount'];
            }
        }

        return [
            'totalCount' => $totalCount,
            'entries' => $entries,
            'allEntryIds' => $allEntryIds
        ];


    }

    /**
     * Create entries from planting instruction records of the selected occurrence
     * 
     * @param Integer $occurrenceDbId occurrence record identifier
     * @param Integer $experimentDbId experiment record identifier
     * @param Integer $bgThreshold Background process limit
     * @param String $program Program Abbrev indetifier
     */
    public function addEntriesFromOccurrence($occurrenceDbId, $experimentDbId, $bgThreshold, $program){
        
        $plantInsRecordCount = Yii::$app->api->getParsedResponse('POST','occurrences/'.$occurrenceDbId.'/entries-search', json_encode(["distinctOn"=>"entryNumber"]), 'limi=1',false)['totalCount'];
        $entryListDbId = $this->entryList->getEntryListId($experimentDbId, true);
        $sourceExperiment = $this->occurrence->searchAll(['occurrenceDbId' => "equals $occurrenceDbId"]);
        $sourceExptDbId = $sourceExperiment['data'][0]['experimentDbId'];
        
        if($plantInsRecordCount > $bgThreshold){
            $token = \Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = \Yii::$app->session->get('user.refresh_token');
            //call background process for creating entries
            $addtlParams = ["description"=>"Creating of entry records","entity"=>"ENTRY","entityDbId"=>$experimentDbId,"endpointEntity"=>"ENTRY", "application"=>"EXPERIMENT_CREATION"];
            $dataArray = [
                'entity'=>'occurrence',
                'entityId' => $occurrenceDbId,  
                'defaultValues' => [
                    'entryListDbId' => $entryListDbId,
                    'entryType' => null,
                    'allowedEntryType' => null,
                    'entryClass' => null,
                    'entryRole' => null,
                    'sourceExperimentDbId' => $sourceExptDbId
                ],
                'processName' => 'create-entry-records',
                'experimentDbId' => $experimentDbId."",
                'tokenObject' => [
                    'token' => $token,
                    'refreshToken' => $refreshToken
                ],
            ];

            $create = $this->experiment->create($dataArray, true, $addtlParams);
            
            $experiment = $this->experiment->getExperiment($experimentDbId);
            $experimentStatus = $experiment['experimentStatus'];

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }
            
            if(!empty($create['data'])){
                $bgStatus = $this->experiment->formatBackgroundWorkerStatus($create['data'][0]['backgroundJobDbId']);

                if($bgStatus !== 'done' && !empty($bgStatus)){
                    
                    $updatedStatus = implode(';', $status);
                    $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                    $this->experiment->updateOne($experimentDbId, ["experimentStatus"=>"$newStatus"]);

                    //update experiment
                    Yii::$app->session->setFlash('warning', '<i class="fa-fw fa fa-warning"></i> Adding of entries '.$experiment['experimentName'].' has started.');
                    Yii::$app->response->redirect(['experimentCreation/create/index?program='.$experiment['programCode'].'&id='.$experimentDbId]);
                }
            } else {
                Yii::$app->response->redirect(['experimentCreation/create/index?program='.$program.'&id='.$experimentDbId]);
            }
        } else {

            $plantInsRecords = Yii::$app->api->getParsedResponse('POST','occurrences/'.$occurrenceDbId.'/entries-search', json_encode(["distinctOn"=>"entryNumber"]), 'sort=entryNumber',true)['data'];

            $entryRole = 'female-and-male';
            
            for($i = 0; $i < count($plantInsRecords); $i++){
                $requestData = [
                    'entryListDbId' => "$entryListDbId",
                    'entryName' => $plantInsRecords[$i]['entryName'].'',
                    'entryType' => $plantInsRecords[$i]['entryType'].'',
                    'entryClass' => 'imported',
                    'entryStatus' => $plantInsRecords[$i]['entryStatus'].'',
                    'germplasmDbId' => $plantInsRecords[$i]['germplasmDbId'].'',
                    'seedDbId' => $plantInsRecords[$i]['seedDbId'].'',
                    'notes' => $sourceExptDbId.';'.$plantInsRecords[$i]['entryDbId']
                ];
                $packageDbId = $plantInsRecords[$i]['packageDbId'];
                
                //entry role
                if(isset($plantInsRecord['entryRole'])){
                    $requestData['entryRole'] = $plantInsRecords[$i]['entryRole'];
                } else {
                    $requestData['entryRole'] = $entryRole;
                } 

                if(!empty($packageDbId)){
                    $requestData['packageDbId'] = "$packageDbId";
                }
                
                $insertList[] = $requestData; 
            }

            $createdEntries = Entry::create(['records' => $insertList]);

            return $createdEntries;

        }
        
    }

    /**
     * Update planting instruction records
     * 
     * @param Integer $experimentDbId experiment record identifier
     * @param Array $entryDbIdList list of entry ids
     * @param String $column field to be updated
     * @param String $value updated value
     */
    public static function updatePlantingInstruction($experimentDbId, $entryDbIdList, $column, $value){

        $occurrenceRecord = \Yii::$container->get('app\models\Occurrence')->searchAll(['experimentDbId' => "equals $experimentDbId"]);
        $plantingInstruction = \Yii::$container->get('app\models\PlantingInstruction');
        if($occurrenceRecord['totalCount'] == 1){
            $occurrenceDbId = $occurrenceRecord['data'][0]['occurrenceDbId'];
            $piRecords = $plantingInstruction->searchAll([
                'entryDbId'=> 'equals '.implode('|equals ', $entryDbIdList),
                'occurrenceDbId' => "equals $occurrenceDbId"
            ]);

            if($piRecords['totalCount'] > 0){
                $piDbIdList = array_column($piRecords['data'], 'plantingInstructionDbId');
                $plantingInstruction->updateMany($piDbIdList, [$column => $value]);
            }
       }
    }

}