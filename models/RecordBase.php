<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.record".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier of the record in the table
 * @property string $name Long name identifier of the record in the table
 * @property string $description Additional information about the record in the table
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $record_type
 * @property int $parent_record_id Parent record of the entity record, where all of the parent's variables are inherited from
 * @property bool $is_adjustable Indicator whether variable members of a record can be changed or not within a form view
 * @property string $display_name
 */
class RecordBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abbrev', 'name'], 'required'],
            [['description', 'remarks', 'notes', 'record_type'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['creator_id', 'modifier_id', 'parent_record_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id', 'parent_record_id'], 'integer'],
            [['is_void', 'is_adjustable'], 'boolean'],
            [['abbrev'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 256],
            [['display_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'record_type' => 'Record Type',
            'parent_record_id' => 'Parent Record ID',
            'is_adjustable' => 'Is Adjustable',
            'display_name' => 'Display Name',
        ];
    }
}
