<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.saved_list_member".
 *
 * @property int $id Primary key of the record in the table
 * @property int $saved_list_id ID of the saved list where the entity is a member of
 * @property int $entity_id ID of the entity in the saved list; this can be the product, variable, study, or seed lot records
 * @property int $order_number Ordering of the member entities within the saved list
 * @property string $remarks Additional details about the saved list members
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $data_object_id Actual id of the data object
 */
class SavedListMemberBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.saved_list_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['saved_list_id', 'entity_id'], 'required'],
            [['saved_list_id', 'entity_id', 'order_number', 'creator_id', 'modifier_id', 'data_object_id'], 'default', 'value' => null],
            [['saved_list_id', 'entity_id', 'order_number', 'creator_id', 'modifier_id', 'data_object_id'], 'integer'],
            [['remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'saved_list_id' => 'Saved List ID',
            'entity_id' => 'Entity ID',
            'order_number' => 'Order Number',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'data_object_id' => 'Data Object ID',
        ];
    }
}
