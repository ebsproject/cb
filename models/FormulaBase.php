<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table ".formula".
 *
 * @property int $id
 * @property string $formula
 * @property int $result_variable_id
 * @property int $method_id
 * @property string $data_level
 * @property string $function_name
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $formatted_formula Formatted formula from raw input formula
 * @property string $database_formula Database formula processed from raw and formatted formula
 * @property int $decimal_place No. of decimal places for floating point values
 *
 * @property Method $method
 * @property User $creator
 * @property User $modifier
 * @property Variable $resultVariable
 * @property FormulaParameter[] $FormulaParameters
 * @property Method[] $Methods
 */
class FormulaBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.formula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['formula', 'result_variable_id', 'method_id', 'data_level', 'function_name'], 'required'],
            [['formula', 'function_name', 'remarks', 'notes', 'formatted_formula', 'database_formula'], 'string'],
            [['result_variable_id', 'method_id', 'creator_id', 'modifier_id', 'decimal_place'], 'default', 'value' => null],
            [['result_variable_id', 'method_id', 'creator_id', 'modifier_id', 'decimal_place'], 'integer'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['data_level'], 'string', 'max' => 64],
            [['method_id'], 'exist', 'skipOnError' => true, 'targetClass' => Method::className(), 'targetAttribute' => ['method_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
            [['result_variable_id'], 'exist', 'skipOnError' => true, 'targetClass' => Variable::className(), 'targetAttribute' => ['result_variable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'formula' => 'Formula',
            'result_variable_id' => 'Result Variable ID',
            'method_id' => 'Method ID',
            'data_level' => 'Data Level',
            'function_name' => 'Function Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'formatted_formula' => 'Formatted Formula',
            'database_formula' => 'Database Formula',
            'decimal_place' => 'Decimal Place',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(Method::className(), ['id' => 'method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        return $this->hasOne(User::className(), ['id' => 'modifier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultVariable()
    {
        return $this->hasOne(Variable::className(), ['id' => 'result_variable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulaParameters()
    {
        return $this->hasMany(FormulaParameter::className(), ['formula_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethods()
    {
        return $this->hasMany(Method::className(), ['formula_id' => 'id']);
    }
}
