<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "germplasm.germplasm_name".
 *
 * @property int $id Germplasm Name ID: Database identifier of the germplasm name [GERMNAME_ID]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm designated with the name [GERMNAME_GERM_ID]
 * @property string $name_value Germplasm Name Value: Value of the name designated to the germplasm [GERMNAME_NAMEVAL]
 * @property string $germplasm_name_type Germplasm Name Type: Type of name designated to the germplasm [GERMNAME_NAMETYPE]
 * @property string $germplasm_name_status Germplasm Name Status: Status of the germplasm name {standard, active, deprecated, inactive, voided} [GERMNAME_NAMESTATUS]
 * @property int $creator_id
 * @property string $creation_timestamp
 * @property int|null $modifier_id
 * @property string|null $modification_timestamp
 * @property bool $is_void
 * @property string|null $notes
 * @property string|null $access_data
 * @property string|null $event_log
 * @property string $germplasm_normalized_name
 */
class GermplasmName extends GermplasmNameBase
{

     /**
     * Function for getting the normalize name of germplasm list
     * 
     * @param array $inputList array of germplasm names
     * 
     */
    public static function getStandardizedGermplasmName($inputList){

        $resultArray = [];
        foreach($inputList as $input){
            $resultArray[] = GermplasmName::normalizeTextPhp($input);
        }
      
        return $resultArray;

    }

    /**
     * Simplified germplasm name normalization
     * 
     * @param String $text string to be normalized
     */
    public static function normalizeTextPhp($text){

      // a) Remove all spaces
      $aValue = preg_replace_callback(
        "[(\s+)]",
        function ($matches) {
            return '';
        },
        $text
      );

      //b) Capitalize all letters
      $bValue = strtoupper($aValue);

      return [
        'input_product_name' => trim($text),
        'norm_input_product_name' => $bValue
      ];
    }

    /**
     * Function for normalizing germplasm name
     * Based on the stored procedure z_admin.normalize_text
     * 
     * @param string $text germplasm name
     * 
     */
    public static function normalizeText($text){

        //a) Capitalize all letters Khao­Dawk­Mali105 becomes KHAO­DAWK­MALI105
        $aValue = strtoupper($text); 

        // b) L( becomes L^( and )L becomes )^L IR64(BPH) becomes IR64 (BPH)
        // c) N( becomes N^( and )N becomes )^N IR64(5A) becomes IR64 (5A)
        $bc =  preg_replace_callback(
          '[(\()]',
          function ($matches) {
              return ' (';
          },
          $aValue
        );
        $bcValue = preg_replace_callback(
          '[(\))]',
          function ($matches) {
              return ') ';
          },
          $bc
        ); 

        //d) L. becomes L^ IR 63 SEL. becomes IR 64 SEL
        $dValue = preg_replace_callback(
          '[\.]',
          function ($matches) {
              return '';
          },
          $bcValue
        ); 

        //e) LN becomes L^N EXCEPT SLN MALI105 becomes MALI 105 but MALI-F4 IS unchanged
        $eValue = preg_replace_callback(
          "[([^\-\''\[\]\+\.]|)([a-zA-Z])([0-9])]",
          function ($matches) {
              return $matches[1].$matches[2]." ".$matches[3];
          },
          $dValue
        ); 

        //f) NL becomes N^L EXCEPT SNL B 533A-1 becomes B 533 A-1 but B 533 A-4B is unchanged
        $fValue = preg_replace_callback(
          "[([^\-\''\[\]\+\.]|)([0-9])([a-zA-Z])]",
          function ($matches) {
              return $matches[1].$matches[2]." ".$matches[3];
          },
          $eValue
        ); 

        //g) LL-LL becomes LL^LL KHAO-DAWK-MALI 105 becomes KHAO DAWK MALI 105
        $gValue = preg_replace_callback(
          "[([a-zA-Z]{2,})?(-)([a-zA-Z]{2,})]",
          function ($matches) {
              return $matches[1]." ".$matches[3];
          },
          $fValue
        ); 

        //h) ^0N becomes ^N IRTP 00123 becomes IRTP 123
        $hMatches = NULL;
        if($hMatches == NULL){
            $hValue = preg_replace_callback(
              "[(\s+)(0+)([0-9])]",
              function ($matches) {
                  return $matches[1].$matches[3];
              },
              $gValue
            ); 
        } else $hValue = $gValue;

        // i) ^^ becomes ^
        $iValue = preg_replace_callback(
          "[(\s+)]",
          function ($matches) {
              return ' ';
          },
          $hValue
        );

        // j) REMOVE LEADING OR TRAILING ^
        $jValue = trim($iValue);

        //k) ^) becomes ) and (^ becomes (
        $k1Value = preg_replace_callback(
          "[(\s+)(\))]",
          function ($matches) {
              return $matches[2];
          },
          $jValue
        );

        $kValue = preg_replace_callback(
          "[(\()(\s+)]",
          function ($matches) {
              return $matches[1];
          },
          $k1Value
        );
        
        //l) L­N becomes L^N when there is only one '–' in the name and L is not preceded by a space
        $lMatches = NULL;
        // $lCondition = preg_match('[(?<![\s])[a-zA-Z0-9]\w+\-[0-9]+]', $kValue, $lMatches);

        if($lMatches != NULL && count($lMatches) > 0){
            $lValue = str_replace('-', ' ', $kValue);
        } else $lValue = $kValue;
        
        // m) ^/ becomes / and /^ becomes /
        $m1Value = preg_replace_callback(
          "[(\s+)(\/)]",
          function ($matches) {
              return $matches[2];
          },
          $lValue
        );

        $mValue = preg_replace_callback(
          "[(\/)(\s+)]",
          function ($matches) {
              return $matches[1];
          },
          $m1Value
        );

        return [
          'input_product_name' => trim($text),
          'norm_input_product_name' => $mValue
        ];
    }

}
