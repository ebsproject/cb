<?php

namespace app\models;

use ChromePhp;
use Yii;

/**
 * This is the model class for table "experiment.entry_data".
 *
 * @property int $id Entry Data ID: Database identifier of the entry data [ENTDATA_ID]
 * @property int $entry_id Entry ID: Reference to the entry having the attribute [ENTRYDATA_ENTRY_ID]
 * @property int $variable_id Variable ID: Reference to the variable of the attribute of the entry [ENTRYDATA_VAR_ID]
 * @property string $data_value Entry Data Value: Value of the attribute of the entry [ENTRYDATA_DATAVAL]
 * @property string $data_qc_code Entry Data QC Code: Status of the entry data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [ENTRYDATA_QCCODE]
 * @property int|null $transaction_id Transaction ID: Reference to the transaction where the collected data was uploaded and validated [ENTRYDATA_TXN_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class EntryData extends BaseModel {

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'entry-data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entry_id', 'variable_id', 'data_value', 'data_qc_code', 'creator_id'], 'required'],
            [['entry_id', 'variable_id', 'transaction_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['entry_id', 'variable_id', 'transaction_id', 'creator_id', 'modifier_id'], 'integer'],
            [['data_value', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['data_qc_code'], 'string', 'max' => 8],
            [['entry_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentEntry::className(), 'targetAttribute' => ['entry_id' => 'id']],
            [['variable_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterVariable::className(), 'targetAttribute' => ['variable_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'variable_id' => 'Variable ID',
            'data_value' => 'Data Value',
            'data_qc_code' => 'Data Qc Code',
            'transaction_id' => 'Transaction ID',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }

    /**
     * Retrieve entry data variable abbrev - value pair
     * 
     * @param $entryId integer entry record identifier
     * @param $params array list of search parameters
     */
    public static function getEntryData($entryId, $params = null){

        $entryData = [];
        //set api request
        $method = 'POST';
        $path = "entries/$entryId/data-search";

        $results = Yii::$app->api->getResponse($method, $path, json_encode($params), null, true);

        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0]['data'])){

            $entryData = $results['body']['result']['data'][0]['data'];

        }

        return $entryData;
    }

    /**
     * Retrieve entry data variable abbrev and data value pair
     * 
     * @param $entryId integer entry record identifier
     * @param $params array filter parameters
     */
    public static function getEntryDataValue($entryId, $params = null){


        //set api request
        $method = 'POST';
        $path = "entries/$entryId/data-search";
        
        if(!empty($params)){
            $results = Yii::$app->api->getResponse($method, $path, json_encode($params));
        }else{
            $results = Yii::$app->api->getResponse($method, $path);
        }
        
        
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0]['data'])){

            $results = $results['body']['result']['data'][0]['data'];

        }

        return array_change_key_case(array_combine(array_column($results, 'variableAbbrev'), array_column($results, 'dataValue')), CASE_LOWER);
        

    }

    /**
    * Retrieve entry data  value pair
    * 
    * @param $entryId integer entry record identifier
    */
    public static function getEntryDataRecords($entryId){

        //set api request
        $method = 'POST';
        $path = "entries/$entryId/data-search";
   
        $results = Yii::$app->api->getResponse($method, $path);
   
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0]['data'])){

            $results = $results['body']['result']['data'][0]['data'];

        }
        return $results;
    }

    /**
    * Retrieve entry data  value pair with filter
    * 
    * @param $entryId integer entry record identifier
    * @param $configColumns array filter parameters
    */
    public static function getEntryDataRecordsFiltered($entryId, $configColumns){

        //set api request
        $method = 'POST';
        $path = "entries/$entryId/data-search";

        $params = ["fields"=>"entry_data.entry_id as entryDbId|entry_data.variable_id as variableDbId|entry_data.data_value as dataValue|variable.abbrev AS variableAbbrev", "variableAbbrev"=>"equals ".implode("|equals ", $configColumns)];

        $results = Yii::$app->api->getResponse($method, $path, json_encode($params), true);
              
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0]['data'])){

            $results = $results['body']['result']['data'][0]['data'];

        }
        return $results;
    }


    /**
     * Add entry data record
     *
     * @param $params array list of search parameters
     * @return $data array list of entry data inserted
     */
    public static function addRecord($params){
        $processedParams['records'] = $params;

        $data = [];

        if(!empty($params)){
            $response = Yii::$app->api->getParsedResponse('POST', 'entry-data', json_encode($processedParams));
     
            // check if successfully retrieved germplasm
            if(($response['status']) && $response['status'] == 200 && isset($response['data'])){
                $data = (isset($response['data'])) ? $response['data'] : [];
            }
        }
        
        return $data;
    }
      
    /**
     * Update entry data record
     *
     * @param $id record ID of the entry data
     * @param $params array list of search parameters
     * @return $data array list of entry data updated
     */
    public static function updateRecord($id, $params){
      
        $params = empty($params) ? null : $params;
        
        $response = Yii::$app->api->getResponse('PUT', 'entry-data/'.$id, json_encode($params));
        
        $data = [];
  
        // check if successfully retrieved germplasm
        if(($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
          
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
        }
        
        return $data;
    }

    /**
     * Retrieve entry data for experiment
     * 
     * @param $experimentId integer experiment record identifier
     * @param $params array list of search parameters
     */
    public static function getEntryDataByExperiment($experimentId, $params = null){

        $entryData = [];
        //set api request
        $method = 'POST';
        $path = "experiments/$experimentId/entry-data-search";

        $results = Yii::$app->api->getResponse($method, $path, json_encode($params), null, true);
        
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'][0])){

            $entryData = $results['body']['result']['data'];

        }
        return $entryData;
    }
}
