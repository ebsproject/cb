<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "germplasm.germplasm_name".
 *
 * @property int $id Germplasm Name ID: Database identifier of the germplasm name [GERMNAME_ID]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm designated with the name [GERMNAME_GERM_ID]
 * @property string $name_value Germplasm Name Value: Value of the name designated to the germplasm [GERMNAME_NAMEVAL]
 * @property string $germplasm_name_type Germplasm Name Type: Type of name designated to the germplasm [GERMNAME_NAMETYPE]
 * @property string $germplasm_name_status Germplasm Name Status: Status of the germplasm name {standard, active, deprecated, inactive, voided} [GERMNAME_NAMESTATUS]
 * @property int $creator_id
 * @property string $creation_timestamp
 * @property int|null $modifier_id
 * @property string|null $modification_timestamp
 * @property bool $is_void
 * @property string|null $notes
 * @property string|null $access_data
 * @property string|null $event_log
 * @property string $germplasm_normalized_name
 */
class GermplasmNameBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'germplasm.germplasm_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['germplasm_id', 'name_value', 'germplasm_name_type', 'germplasm_name_status', 'germplasm_normalized_name'], 'required'],
            [['germplasm_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['germplasm_id', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'access_data', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['notes', 'germplasm_normalized_name'], 'string'],
            [['name_value'], 'string', 'max' => 256],
            [['germplasm_name_type', 'germplasm_name_status'], 'string', 'max' => 64],
            [['germplasm_id'], 'exist', 'skipOnError' => true, 'targetClass' => GermplasmGermplasm::className(), 'targetAttribute' => ['germplasm_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'germplasm_id' => 'Germplasm ID',
            'name_value' => 'Name Value',
            'germplasm_name_type' => 'Germplasm Name Type',
            'germplasm_name_status' => 'Germplasm Name Status',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'access_data' => 'Access Data',
            'event_log' => 'Event Log',
            'germplasm_normalized_name' => 'Germplasm Normalized Name',
        ];
    }
}
