<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

use app\models\Entry;
use app\models\Experiment;

/**
 * This is the model class for table "experiment.planting_instruction".
 *
 * @property int $id Planting Instruction ID: Database identifier of the planting instruction [PLANTINST_ID]
 * @property string $entry_code Entry Code: Textual identifier of the entry in the planting instruction [PLANTINST_ENTRY_CODE]
 * @property int $entry_number Entry Number: Member number of the entry in the experiment, which must not change throughout the experiment [PLANTINST_ENTRY_NO]
 * @property string $entry_name Entry Name: Name of the germplasm as an entry or possibly its replacement germplasm [PLANTINST_GERM_NAME]
 * @property string $entry_type Entry Type: Usage of the entry or possibly its replacement {test, check, control, filler, parent, cross} [PLANTINST_ENTRY_TYPE]
 * @property string|null $entry_role Entry Role: Function of the germplasm as an entry or possibly its replacement germplasm {female, male, female-and-male, spatial check, performance check} [PLANTINST_ENTRY_ROLE]
 * @property string|null $entry_class Entry Class: Grouping class of the entry in plot, which is mainly used in data analysis [PLANTINST_ENTRY_CLASS]
 * @property string $entry_status Entry Status: Status condition of the entry, which may also be the replacement germplasm {draft, active, replaced} [PLANTINST_ENTRY_STATUS]
 * @property int $entry_id Entry ID: Reference to the entry or possibly the original entry of the experiment before it was replaced [PLANTINST_ENTRY_ID]
 * @property int $plot_id Plot ID: Reference to the plot where the planting instruction is performed [PLANTINST_PLOT_ID]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm of the entry in the plot or possibly its replacement [PLANTINST_GERM_ID]
 * @property int $seed_id Seed ID: Reference to the seed of the germplasm to be planted and observed in the plot [PLANTINST_SEED_ID]
 * @property int|null $package_id Package ID: Reference to the package of seeds used for planting in the plot [PLANTINST_PKG_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 * @property int|null $package_log_id Package Log ID: Reference to the package log of withdrawn materials [PLANTINST_PKGLOG_ID]
 */
class PlantingInstruction extends BaseModel {

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'planting-instructions';
    }

    public function __construct (
        public Experiment $experiment,
        public Entry $entry
    ) { }

    /**
     * Search for planting instruction records given search parameters
     *
     * @param Array $params list of search parameters
     * @param Array $filter list of filter parameters
     * @param Boolean $retrieveAll list of search parameters
     * @return Array $data list of records given search parameters
     */
    public function searchRecords($params, $filter=null, $retrieveAll=false){
        $apiEndPoint = static::apiEndPoint().'-search';
        $params = empty($params) ? null : $params;
        $response = Yii::$app->api->getParsedResponse('POST', $apiEndPoint, json_encode($params), $filter, $retrieveAll);

        $data = [];

        // check if successfully retrieved germplasm
        if(isset($response['status']) && $response['status'] == 200){
          
            $data = $response;
        }
        
        return $data;
    }

    
   
 	/**
     * Update planting instruction records when seed_ids and package_ids is not populated
     *
     * @param Array $occurrenceIds list of database id of occurrences
     * @param Integer $experimentDbId list of database id of experiment
     * @return Array list of placeholder status
     */
    public function updatePopulatedRecords($occurrenceIds ,$experimentDbId){

        $response = $this->searchRecords(['occurrenceDbId'=>'equals '.implode('|equals ', $occurrenceIds), 'packageDbId'=>'is null'], 'limit=1', false);
        $entries = $this->entry->searchAll(['experimentDbId'=>'equals '.$experimentDbId, 'packageDbId'=>'is null'], 'limit=1', false);

        if(!empty($response) && !empty($entries) && ($response['totalCount'] > 0  && $entries['totalCount'] == 0)){

            //populate the missing seed_ids and package_ids of the planting instructions
            $bgThreshold = !empty(Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences')) ? Yii::$app->config->getAppThreshold('EXPERIMENT_CREATION', 'createOccurrences') : 500;


            $experimentModel = $this->experiment->searchAll(["fields"=>"experiment.id AS experimentDbId|program.program_code AS programCode|experiment.experiment_status AS experimentStatus|experiment.experiment_name AS experimentName",'experimentDbId'=>"equals ".$experimentDbId]);

            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $refreshToken = Yii::$app->session->get('user.refresh_token');

            $requestDataParams = [
                'occurrenceIds'=>$occurrenceIds,
                'processName' => 'update-plant_inst',
                'experimentDbId' => $experimentDbId.""
            ];

            $params = [
                'httpMethod' => 'PUT',
                'endpoint' => 'v3/planting-instructions',
                'entity' => 'PLANTING_INSTRUCTION',
                'entityDbId' => ''.$experimentDbId,
                'endpointEntity' => 'PLANTING_INSTRUCTION',
                'application' => 'EXPERIMENT_CREATION',
                'dbIds' => ''.$experimentDbId,
                'descpription' => 'Update planting instruction records',
                'requestData' => $requestDataParams,
                'tokenObject' => [
                    "token" => $token,
                    "refreshToken" => $refreshToken
                ],
            ];

            $updateBgProcess = Yii::$app->api->getParsedResponse('POST', 'processor', json_encode($params));

            //Redirect to main page
            $experimentStatus = isset($experimentModel['data'][0]['experimentStatus']) ? $experimentModel['data'][0]['experimentStatus'] : '';

            if(!empty($experimentStatus)){
                $status = explode(';',$experimentStatus);
                $status = array_filter($status);
            }else{
                $status = [];
            }

            $bgStatus = $this->experiment->formatBackgroundWorkerStatus($updateBgProcess['data'][0]['backgroundJobDbId']);

            if($bgStatus !== 'done' && !empty($bgStatus)){
                $updatedStatus = implode(';', $status);
                $newStatus = !empty($updatedStatus) ? $updatedStatus.';'. $bgStatus : $bgStatus;

                $this->experiment->updateOne($experimentDbId, ["experimentStatus"=>"$newStatus"]);

                return [
                    'success' => true,
                    'message' => 'redirect'
                ];
            } else {
                return [
                    'success' => true,
                    'message' => ''
                ];
            }
        } else {
            return [
                'success' => true,
                'message' => ''
            ];
        }
    }

    /**
     * Update planting instruction records when entry record is updated
     *
     * @param Integer $entryDbId database id of the entry
     * @param Array $requestBody array containing the information to be updated
     * @return Array list of placeholder status
     */
    public function updateForEntry($entryDbId, $requestBody){

        $response = $this->searchRecords(['entryDbId'=>'equals '.$entryDbId], 'limit=1', false);
        if(!empty($response) && ($response['totalCount'] > 0 )){
            $data = $response['data'];
            $this->updateOne($data[0]['plantingInstructionDbId'], $requestBody);
        }
    }

}
