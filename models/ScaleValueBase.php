<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.scale_value".
 *
 * @property int $id Primary key of the record in the table
 * @property int $scale_id Scale where the value is associated with
 * @property string $value Allowed value in the scale
 * @property int $order_number Order of the value in the scale; -1 if order is not significant
 * @property string $description Additional information about the scale value
 * @property string $display_name Name or label of the scale value to show to users
 * @property string $remarks Additional details about the scale value
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $scale_value_status Indicates whether scale value is shown in list of options (null or show) or not (hide)
 */
class ScaleValueBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.scale_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['scale_id', 'value', 'description'], 'required'],
            [['scale_id', 'order_number', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['scale_id', 'order_number', 'creator_id', 'modifier_id'], 'integer'],
            [['value', 'description', 'remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['display_name'], 'string', 'max' => 256],
            [['scale_value_status'], 'string', 'max' => 255],
            [['scale_id', 'value'], 'unique', 'targetAttribute' => ['scale_id', 'value']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scale_id' => 'Scale ID',
            'value' => 'Value',
            'order_number' => 'Order Number',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'scale_value_status' => 'Scale Value Status',
        ];
    }
}