<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\components\GenericFormWidget;
use app\interfaces\models\IApi;
use app\models\Person;
use app\interfaces\models\IBrowser;
use app\interfaces\models\IVariable;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use Yii;

/**
 * This is the class for browser methods
 */

class Browser implements IBrowser
{

    public function __construct (
        public IApi $api,
        public GenericFormWidget $genericFormWidget,
        public Person $person,
        public IVariable $variable
    )
    { }
    
    /**
     * Build fields param with or without sorting params
     *
     * @param Array $attributes List of attributes and fields parameter
     * @param String $defaultFieldsParams Default fields parameter - the IDs to be retrieved
     * @param String $sortParams Sort parameters (e.g. sort=experimentDbId:desc|experimentName)
     * @param Array $filterParams Filter parameters (e.g. ["experimentDbId"=>"36","experimentName"=>"OYT-2021"])
     * @param String $defaultFilterFieldsParam Default filter parameter (e.g. pj.id AS plantingJobDbId)
     *
     * @return String $fields Fields parameter
     */
    public function buildFieldsParam($attributes, $defaultFieldsParams, $sortParams=null, $filterParams=null, $defaultFilterFieldsParam= null){
        $sortFieldParamsArr = $filterFieldParamsArr = [];
        // if there are sort parameters
        if(!empty($sortParams)){
            
            // get sort params
            $sortField = explode('=', $sortParams)[1];

            // get all fields in sort params
            $sortFieldsArr = explode('|', $sortField);

            $sortFieldParamsArr = [];
            // loop trough each sort params
            foreach ($sortFieldsArr as $value) {
                // get attribute
                $sortAttr = explode(':', $value)[0];

                // get fields param of sort atrribute
                if(isset($attributes[$sortAttr])){
                    $sortFieldParamsArr[] = $attributes[$sortAttr];
                }else{
                    // if there's a field not found in the attributes list
                    return null;
                }
            }

            // if sort fields parameter exist in the list of attributes
            if(!empty($sortFieldParamsArr)){
                // default fields params
                $defaultFieldParams = [$defaultFieldsParams];
                // merge sort fields params and default fields param
                $sortFieldParamsArr = array_merge($sortFieldParamsArr, $defaultFieldParams);
                // defult filter fields param 
                $sortFieldParamsArr = array_merge($sortFieldParamsArr, [$defaultFilterFieldsParam]);
            }

        } else {
            $sortFieldParamsArr = [$defaultFieldsParams, $defaultFilterFieldsParam];
        }

        // if there are filter parameters
        if(!empty($filterParams)){
            $filterFieldParamsArr = [];

            // loop through all filter params
            foreach ($filterParams as $key => $value) {
                if ($key == 'fields') {
                    continue;
                }

                // get fields param of filter atrribute
                if (isset($attributes[$key])) { // check if this key exists in the attributeMap
                    // if this attribute is a default field, skip this attribute
                    if (str_contains($defaultFieldParams[0], $attributes[$key])) {
                        continue;
                    }

                    // prep SQL field of this attribute
                    $filterFieldParamsArr[] = $attributes[$key];
                } else { // if there's a key not found in the attributes map, return null
                    return null;
                }
            }
        }

        $fieldsParamArr = array_merge($sortFieldParamsArr, $filterFieldParamsArr);

        // Remove null elements from array
        $fieldsParamArr = array_filter($fieldsParamArr, function ($field) {
            return $field !== null;
        });

        return implode('|', array_unique($fieldsParamArr));
    }

    /**
     * Build form columns
     *
     * @param Array $fields Fields to be displayed in form
     * @param Text $entity Entity to be retrieved
     */
    public function buildFormColumns($fields, $entity, $usage=null){

        $columns = [];

        foreach ($fields as $key => $field) {
            if(!(isset($field['is_hidden']) && $field['is_hidden'])){
                $column = strtoupper($field['variable_abbrev'] ?? '');

                $visible = (isset($field['is_shown']) && $field['is_shown'] == false) ? true : false;

                $defaultValue = (isset($field['default']) && !empty($field['default'])) ? $field['default'] : '';

                $varParams['abbrev'] = 'equals '.$column;
                $variableData = $this->variable->searchAll($varParams, 'limit=1', false);

                $variable = [];
                if(isset($variableData['data'][0])){
                    $variable = $variableData['data'][0];
                }

                // if variable exists
                if(!empty($variable)){

                    $header = isset($field['display_name']) && !empty($field['display_name']) ? $field['display_name'] : $variable['label'];
                    $dataType = $variable['dataType'];

                    // check if field is required
                    $isRequiredLabel = $isRequiredClass = '';
                    $isRequired = false;
                    if(isset($field['required']) && $field['required'] == 'required'){
                        $isRequiredLabel = '<span class="required" title="Required field">*</span>';
                        $isRequiredClass = ' required-field';
                        $isRequired = $field['required'];
                    }

                    // check if the field is disabled
                    $isDisabled = isset($field['disabled']) ? $field['disabled'] : false;

                    if($column == 'PLANTING_DATE' || $column == 'EMERGENCE_DATE')
                        $attribute = $column;
                    else if(isset($field['target_column']) && (!isset($field['secondary_target_column']) || empty($field['secondary_target_column']) && $field['variable_type'] !== 'metadata'))
                        $attribute = $field['target_column'];
                    else if($variable['type'] != 'identification')
                        $attribute = $column;
                    else
                        $attribute = $this->formatToCamelCase($column);

                    $data = [];
                    $variableAbbrev = $field['variable_abbrev'];

                    // Retrieve data if current field has an API resource
                    if (
                        empty(Yii::$app->session->get("em-edit-occurrence-variable-options-$variableAbbrev", [])) &&
                        (isset($field['api_resource_endpoint']) && !empty($field['api_resource_endpoint'])) &&
                        (isset($field['api_resource_method']) && !empty($field['api_resource_method']))
                    ) {
                        $response = null;

                        // If current field is Contact Persons, retrieve data via CS API and...
                        // ...preset data for contact persons (retrieved from CS-CRM user base)
                        if (
                            $variableAbbrev == 'CONTCT_PERSON_CONT'
                        ) {
                            $data = $this->person->getPersonContacts();
                        } else { // Else retrieve data via CB API
                            $response = $this->api->getApiResults(
                                $field['api_resource_method'],
                                $field['api_resource_endpoint'] . '?' . ($field['api_resource_sort'] ?? ''),
                                json_encode($field['api_resource_filter']) ?? null,
                                '',
                                true
                            );

                            if (!empty($response['data'])) {
                                $data = $response['data'];
                            }
                        }

                        Yii::$app->session->set("em-edit-occurrence-variable-options-$variableAbbrev", $data);
                    }

                    $presetDataValues = Yii::$app->session->get("em-edit-occurrence-variable-options-$variableAbbrev", []);

                    // build field value
                    $fieldValue = function ($data) use ($defaultValue, $field, $entity, $attribute, $usage, $presetDataValues) {
                        $occurrenceStatus = isset($data['occurrenceStatus']) ? $data['occurrenceStatus'] : '';
                        $variableAbbrev = $field['variable_abbrev'];

                        if ($attribute == 'field' && isset($data['siteDbId'])) {
                            $siteDbId = $data['siteDbId'];

                            $field['api_resource_filter'] = [
                                'geospatialObjectType' => 'equals field',
                                'parentGeospatialObjectDbId' => "equals $siteDbId",
                            ];

                            // If session data for this site-field is empty...
                            // ...retrieve data via API and then store to session
                            if (empty(Yii::$app->session->get("em-edit-occurrence-variable-options-FIELD-$siteDbId", []))) {
                                $response = $this->api->getApiResults(
                                    $field['api_resource_method'],
                                    $field['api_resource_endpoint'] . '?' . ($field['api_resource_sort'] ?? ''),
                                    json_encode($field['api_resource_filter']) ?? null,
                                    '',
                                    true
                                );
                                $optionsData = [];

                                if (!empty($response['data'])) {
                                    $optionsData = $response['data'];
                                }

                                Yii::$app->session->set("em-edit-occurrence-variable-options-FIELD-$siteDbId", $optionsData);
                            }

                            // Load preset data values for this site-field
                            $presetDataValues = Yii::$app->session->get("em-edit-occurrence-variable-options-FIELD-$siteDbId", []);
                        }

                        // If this occurrence is neither mapped nor planted...
                        // ...disable updating of location
                        if (
                            $attribute == 'locationName' &&
                            (!str_contains($occurrenceStatus, 'mapped') && !str_contains($occurrenceStatus, 'planted'))
                        ) {
                            return '';
                        }

                        //Default fields for site creation only
                        if ($usage == 'update occurrence' && $occurrenceStatus != 'draft') {
                            if($attribute == 'field'){
                                return isset($data['field']) ? $data['field'] : '<span class="not-set">(not set)</span>';
                            }
                            else if($attribute == 'siteDbId'){
                                return isset($data['site']) ? $data['site'] : '<span class="not-set">(not set)</span>';
                            }
                            else return isset($data[$attribute]) ? $data[$attribute] : '<span class="not-set">(not set)</span>';

                        } else {

                            // get saved value if set else default value from config

                            $field['default'] = isset($data[$attribute]) ? $data[$attribute] : $defaultValue;
                        
                            $rowId = $data[$entity.'DbId'];

                            $this->genericFormWidget->options = [
                                'config' => [$field],
                                'rowId' => $rowId,
                                'rowClass' => 'editable-field form-fields form-field-'.$attribute,
                                // Pass preset data/options if available
                                'presetData' => [ $variableAbbrev => $presetDataValues ],
                            ];

                            $formattedValue = $this->genericFormWidget->generateFields();

                            return $formattedValue['requiredFields'][0]['rawValue'];
                        }
                    };

                    $order = !empty($isRequiredLabel) ? DynaGrid::ORDER_FIX_LEFT : DynaGrid::ORDER_MIDDLE;
                }
            }

            if(isset($attribute)){
                $formattedField =
                    [
                        'attribute'=> $attribute,
                        'label' => '<span title="'.$header.'">'.Yii::t('app', $header).' '.$isRequiredLabel . '</span>',
                        'format' => 'raw',
                        'encodeLabel'=> false,
                        'vAlign' => 'middle',
                        'visible' => true,
                        'value' => $fieldValue,
                        'order' => $order
                    ];

                 // if data type is date, display datepicker as column filter
                if($dataType == 'date'){
                    $dateFilter = [
                        'filterType' => GridView::FILTER_DATE,
                        'filterInputOptions' => [
                            'autocomplete' => 'off'
                        ],
                        'filterWidgetOptions' => [
                            'pluginOptions'=> [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true,
                                'clearBtn' => true,
                                'todayHighlight' => true,
                            ],
                            'pluginEvents' => [
                                'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                            ],
                        ],
                    ];

                    $formattedField = array_merge($formattedField, $dateFilter);
                }

                $columns[] = $formattedField;
            }
        }

        return $columns;
    }

    /**
     * Format string to camel case
     *
     * @param String $text string to be converted to camel case
     * @return String text converted to camel case
     */    
    public function formatToCamelCase($index){
        return preg_replace_callback('/_([a-z]?)/', function($match) {return strtoupper($match[1]);}, strtolower($index));
    }

    /**
     * Build list of variables to form configuration
     *
     * @param Array $vars List of variable abbrevs
     * @return Array $varsConfig List of form configuration
     */
    public function buildVarsAsFormConfig($vars){
        $varsConfig = [];

        if (!empty($vars)){
            foreach ($vars as $value) {
                $varParams['abbrev'] = 'equals '.$value;
                $variableData = $this->variable->searchAll($varParams, 'limit=1', false);
                $variable = [];
                if(isset($variableData['data'][0])){
                    $variable = $variableData['data'][0];
                }
                // if variable exists
                if(!empty($variable)){
                    $varsConfig[] = [
                        'variable_abbrev' => $value,
                        'variable_type' => $variable['type']
                    ];
                }
            }
        }

        return $varsConfig;
    }
}