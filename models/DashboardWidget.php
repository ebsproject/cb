<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;
use ChromePhp;
use app\models\Config;

/**
 * This is the model class for table "platform.dashboard_widget".
 *
 * @property int $id Unique identifier of the record
 * @property string $abbrev Abbreviation of the record
 * @property string $label Label of the record
 * @property string $description More information about the record
 * @property string $icon
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 *
 * @property MasterUser $creator
 * @property MasterUser $modifier
 */
class DashboardWidget extends BaseModel
{
    /**
     * Retrieves widgets of dashboard of a user
     *
     * @param $userId integer user identifier
     * @return $widgetsArr array list of dashboard widgets
     */
    public static function getWidgets($userId){

        //get data in user dashboard config
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId, false); 

        $widgetsArrCol1 = [];
        $widgetsArrCol2 = [];
        $widgets = [];

        if(isset($result[0])){ //if there is saved configuration
            $res = json_decode($result[0]);
            $widgets = isset($res->dashboard_widgets) ? $res->dashboard_widgets : [];
        }

        if(empty($widgets)){ //if empty get default list of widgets in config

            $configModel = new Config();
            $data = $configModel->getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');
            $widgets = json_decode(json_encode($data));

        }

        $isMinimized = [];
        $customLabel = [];
        $timestamp = [];

        //for first column
        if(isset($widgets->col_1) && !empty($widgets->col_1)){
            foreach ($widgets->col_1 as $value) {
                //check if is minimized by default
                $isMinimized = (isset($value->is_minimized) && ($value->is_minimized || $value->is_minimized == "true")) ? ['is_minimized' => true] : [];
                //check if there is custom label assigned
                $customLabel = (isset($value->custom_label) && !empty($value->custom_label)) ? ['custom_label' => $value->custom_label] : [];
                //check if there is custom timestamp assigned
                $timestamp = (isset($value->timestamp) && !empty($value->timestamp)) ? ['timestamp' => $value->timestamp] : [];

                $widget = DashboardWidget::getWidgetInfo($value->widget_id);
                if(!empty($widget)){ //if widget exists
                    $widgetsArrCol1[] = array_merge($widget,$isMinimized,$customLabel,$timestamp);
                }
            }
        }

        //for second column
        if(isset($widgets->col_2) && !empty($widgets->col_2)){
            foreach ($widgets->col_2 as $value) {
                //check if is minimized by default
                $isMinimized = (isset($value->is_minimized) && ($value->is_minimized || $value->is_minimized == "true")) ? ['is_minimized' => true] : [];
                //check if the re is custom label assigned
                $customLabel = (isset($value->custom_label) && !empty($value->custom_label)) ? ['custom_label' => $value->custom_label] : [];
                //check if there is custom timestamp assigned
                $timestamp = (isset($value->timestamp) && !empty($value->timestamp)) ? ['timestamp' => $value->timestamp] : [];

                $widget = DashboardWidget::getWidgetInfo($value->widget_id);
                if(!empty($widget)){ //if widget exists
                    $widgetsArrCol2[] = array_merge($widget,$isMinimized,$customLabel,$timestamp);
                }
            }
        }

        return [
            'col_1' => $widgetsArrCol1,
            'col_2' => $widgetsArrCol2
        ];
    }

    /**
     * Get widget information 
     *
     * @param $id integer widget identifier
     * @return $widget object widget information
     */
    public static function getWidgetInfo($id){
        $widget = null; //initialize

        $param = [
            'widgetDbId' => (string)$id
        ];

        $data = Yii::$app->api->getResponse('POST','dashboard-widgets-search', json_encode($param) );

        if(isset($data['body']['result']['data'][0])){
            $widget = $data['body']['result']['data'][0];
        }

        return $widget;
    }

    /**
     * Get all widgets information 
     *
     * @return $widgets object all widgets information
     */
    public static function getAllWidgetsData(){
        $data = []; //initialize

        $data = Yii::$app->api->getResponse('POST','dashboard-widgets-search');

        if(isset($data['body']['result']['data'])){
            $data = $data['body']['result']['data'];
        }

        return $data;
    }
}
