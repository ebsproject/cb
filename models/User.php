<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use stdClass;
use Yii;
use yii\data\ArrayDataProvider;
use app\interfaces\models\IUser;

/**
 * This is the model class for table "master.user".
 *
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property integer $user_type
 * @property integer $status
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $display_name
 * @property string $salutation
 * @property string $valid_start_date
 * @property string $valid_end_date
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property boolean $is_person
 * @property string $record_uuid
 * @property string $event_log
 */

class User extends BaseModel implements IUser
{
    public $email;
    public $username;
    public $personType;
    public $person_type;
    public $firstName;
    public $first_name;
    public $lastName;
    public $last_name;
    public $personName;
    public $status;
    public $personDbId;
    public $creatorDbId;
    public $creator_id;
    public $modifierDbId;
    public $modifier_id;
    public $creationTimestamp;
    public $creation_timestamp;
    public $modification_timestamp;
    public $modificationTimestamp;
    public $creator;
    public $modifier;
    public $is_void;
    public $isActive;
    public $is_active;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant.person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username', 'personType', 'person_type', 'firstName', 'first_name', 'lastName', 'last_name'], 'required'],
            [['personType', 'person_type', 'status', 'personDbId', 'creatorDbId', 'creator_id', 'modifierDbId', 'modifier_id'], 'integer'],
            [['creationTimestamp', 'creation_timestamp', 'modificationTimestamp', 'modification_timestamp'], 'safe'],
            [['creator', 'modifier'], 'string'],
            [['is_void','isActive', 'is_active'], 'boolean'],
            [['firstName', 'first_name', 'lastName', 'last_name', 'creator', 'modifier'], 'default', 'value' => null],
            [['personType', 'person_type'], 'default', 'value' => 'user'],
            [['email', 'username', 'personName', 'person_name'], 'string', 'max' => 64],
            [['lastName', 'last_name', 'firstName', 'first_name'], 'string', 'max' => 32],
            [['email', 'username'], 'unique'],
        ];
    }

    public function getAuthKey() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    /**
     * Retrieves program where user belongs to
     *
     * @param Integer $userId User identifier
     * @return Integer $programId Program where user belongs to
     */
    public function getUserProgram($userId){
        $programId = 0; //initialize program id

        // if user ID is not set
        if(empty($userId)){
            return $programId;
        }
        
        $programs = Yii::$app->api->getResponse('GET',"persons/$userId/programs",null,'sort=programDbId:asc&limit=1');

        if($programs['status'] == 200 && isset($programs['body']['result']['data'][0]['programDbId'])){
            $programId = $programs['body']['result']['data'][0]['programDbId'];
        }

        //if no team assigned, check if user is admin
        if(!isset($result[0]) && empty($result[0])){
            $isAdmin = Yii::$app->session->get('isAdmin');

            //if admin and no assigned team, get first program
            if($isAdmin){

                $programs = Yii::$app->api->getResponse('GET',"programs",null,'sort=programDbId:asc&limit=1');

                if($programs['status'] == 200 && isset($programs['body']['result']['data'][0]['programDbId'])){
                    $programId = $programs['body']['result']['data'][0]['programDbId'];
                }
            }
        }
        
        return $programId;
    }

    /**
     * Retrieves all program filter tags where user has access to.
     * If the user is an admin, all programs are retrieved,
     * except when $ignoreAdmin is set to `true`, in which case
     * only programs which the user is a member of will be retrieved.
     *
     * @param Integer $userId integer user identifier
     * @param String $options string of url options
     * @param Boolean $ignoreAdmin (optional) whether or not the admin role will be checked.
     *      If set to `true`, only the programs which the admin user is a member of will be retrieved.
     * @return $result array list of program tags where user has access to
     */
    public function getUserPrograms($userId, $options = '', $ignoreAdmin = false){
        
        $isAdmin = Yii::$app->session->get('isAdmin');

        $userValidPrograms = Yii::$app->session->get('user_valid_programs') ?? [];
        if(!empty($userValidPrograms)){
            $body = [
                "fields" => "program.id AS programDbId|program.program_code AS programCode|program.program_name AS programName",
                "programCode" => 'equals '.implode('|equals ', $userValidPrograms)
            ];
            $programs = Yii::$app->api->getResponse('POST','programs-search', json_encode($body), retrieveAll: true);
        }elseif(!$ignoreAdmin && $isAdmin){
            $programs = Yii::$app->api->getResponse('GET','programs', null, $options);
        }else{
            // retrieve only programs where user belong to
            $programs = Yii::$app->api->getResponse('GET','persons/'.$userId.'/programs', null, $options);
        }

        if($programs['status'] == 200 && isset($programs['body']['result']['data'])){
            $programs = $programs['body']['result']['data'];
        } else {
            $programs = [];
        }

        $result = [];
        if(!empty($programs)){
            foreach ($programs as $value) {
                $result[] = [
                    'id' => $value['programDbId'],
                    'text' => $value['programName'] . ' (' . $value['programCode'] .')'
                ];
            }
        }

        if(empty($result)){ //if no program
            $result = 0;
        }
        
        return $result;
    }

    /**
     * Retrieves all teams where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeams($userId, $removeSystemGroup=false){
        
        $teams = Yii::$app->api->getResponse('GET','persons/'.$userId.'/teams');

        if($teams['status'] == 200 && isset($teams['body']['result']['data'])){
            $teams = $teams['body']['result']['data'];
        }

        $teamsArr = [];
        if(!empty($teams)){
            foreach ($teams as $value) {
                if(isset($value['teamCode'])){
                    $teamsArr[] = $value['teamCode'];
                }
            }
        }
        
        return $teamsArr;
    }
     /**
     * Retrieves all team ids where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeamsId($userId, $removeSystemGroup=false){
        
        $teams = Yii::$app->api->getResponse('GET','persons/'.$userId.'/teams');

        if($teams['status'] == 200 && isset($teams['body']['result']['data'])){
            $teams = $teams['body']['result']['data'];
        }

        $teamsArr = [];
        if(!empty($teams)){
            foreach ($teams as $value) {
                if(isset($value['teamCode'])){
                    $teamsArr[] = $value['teamDbId'];
                }
            }
        }
        
        return $teamsArr;
    }
    /**
     * Retrieves all team names where user belongs to
     *
     * @param $userId integer user identifier
     * @param $removeSystemGroup boolean if true, system group team will not be part of the results
     * @return $result array list of teams user belongs to
     */
    public function getUserTeamsName($userId, $removeSystemGroup=false){
        
        $teams = Yii::$app->api->getResponse('GET','persons/'.$userId.'/teams');

        if($teams['status'] == 200 && isset($teams['body']['result']['data'])){
            $teams = $teams['body']['result']['data'];
        }

        $teamsArr = [];
        if(!empty($teams)){
            foreach ($teams as $value) {
                if(isset($value['teamCode'])){
                    $teamsArr[] = $value['teamName'];
                }
            }
        }
        
        return $teamsArr;
    }

    /**
     * Retrieves user id of currently logged in user
     * @return $userId integer user identifier 
     */
    public function getUserId(){
        $session = Yii::$app->session;

        if($session->get('user.id') != null && $session->get('user.id') != 'null' && is_int($session->get('user.id'))){
            $userId = $session->get('user.id');
        }else if($session->get('userId') != null && $session->get('userId') != 'null'){
            $userId = $session->get('userId');
        }else if(Yii::$app->session->get('user.b4r_api_v3_token') != null){
            // get user ID by access token
            $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
            $tokenParts = explode('.', $accessToken);

            $payload = json_decode(base64_decode($tokenParts[1]),true);
    
            $email = isset($payload['http://wso2.org/claims/emailaddress']) ? $payload['http://wso2.org/claims/emailaddress'] : $payload['email'];
            $params['email'] = $email;

            $personModel = \Yii::$container->get('app\models\Person');
            $person = $personModel->searchAll($params, 'limit=1', false);
            $userId = isset($person['data'][0]['personDbId']) ? $person['data'][0]['personDbId'] : 'null';

            $session->set('user.id', $userId); 
        }else{
            return 0;
        }

        return $userId;
    }

    /**
     * Retrieves user information
     * @param $attr text attribute user wants to get
     * @return $data object list of user information
     */
    public function getUserInfo($attr){

        if($attr == 'first_name' && Yii::$app->session->get('user.first_name') !== null){
            return Yii::$app->session->get('user.first_name');
        }

        $userId = $this->getUserId();
        $personModel = \Yii::$container->get('app\models\Person');
        $personObj = $personModel->getOne($userId);
        $field = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($attr)))));

        if(isset($personObj['data'][$field])){
          return $personObj['data'][$field];

        }

        return '';
    }

    /**
     * Check if user is admin or not
     *
     * @return $userType boolean whether current logged in user is admin or not
     */
    public function isAdmin(){
        $session = Yii::$app->session;

        // retrieve from session
        if($session->get('isAdmin') != null) {

            return $session->get('isAdmin');

        }

        $userModel = new User();
        $userId = $userModel->getUserId();

        $person = User::getUser($userId);

        return (isset($person['personType']) && strtolower($person['personType']) == 'admin') ? true : false;


    }

    /**
     * Retrieves user information by id
     * @param $attr text attribute user wants to get
     * @return $data object list of user information
     */
    public function getUserInfoById($attr,$id){
        
        $sql = "select {$attr} from master.user where id = {$id}";

        $result = Yii::$app->db->CreateCommand($sql)->queryColumn();
        
        return (isset($result[0]) && !empty($result[0])) ? $result[0] : 'Unknown'; 

    }


    /**
     * Get the display name of the given user
     * @param $id integer user identifier
     * @return $displayName string display name of user
     */
    public function getDmsPersonDisplayName($id) {
        $sql = "select lname, fname, ioname from dms.persons where personid = {$id}";
        $result = Yii::$app->db->CreateCommand($sql)->queryAll();

        if(!empty($result)){
            $result = $result[0];
            return $result['lname'] . (isset($result['fname']) ? ', ' . $result['fname'] : '') . (!empty($result['ioname']) ? ' ' . substr($result['ioname'], 0, 1) . '.' : '');
        }else{
            return '';
        }
    }

    /**
     * Retrieves teams information of currently logged in user
     * @return $result array list of teams information
     */
    public function getUserTeamsInfo($userId=null){
        $result = [];
        $userId = empty($userId) ? User::getUserId() : $userId;
        $result = Yii::$app->api->getResponse('GET','persons/'.$userId.'/teams');
        $data = [];

        if($result['status'] == 200 && isset($result['body']['result']['data'])){
            $teamInfo = $result['body']['result']['data'];


            foreach ($teamInfo as $value){
                $teams[$value['teamDbId']] = $value;
            }
            
            if(!empty($teams)){
                foreach ($teams as $key => $value){

                    if(isset($value['roleDbId'])){
                
                        $searchParams = json_encode(['roleDbId' => 'equals '.$value['roleDbId']]);
                        $role = Yii::$app->api->getResponse('POST','roles-search/',$searchParams);

                        $memberCount = Yii::$app->api->getResponse('GET','teams/'.$value['teamDbId'].'/members');

                        if(isset($role['body']['result']['data'])){
                            $role = $role['body']['result']['data'][0];
                            $data[] = [
                                'abbrev' => $value['teamCode'],
                                'count' => count($memberCount['body']['result']['data'][0]['members']),
                                'id' => $value['teamDbId'], 
                                'role' => $role['personRoleName'] . ' ' . '(' . $role['personRoleCode'] . ')',
                                'team' => $value['teamName'] . ' ('.$value['teamCode'].')',
                                'teamMemberId' => $value['teamMemberDbId']
                            ];
                        }
                    }
                }
            }
        }
        
        return $data;
    }

    /**
     * Retrieves teams information of currently logged in user via CS API
     * @return $result array list of teams information
     */
    public function getUserTeamsInfoCs($userId=null){
        $result = [];
        $userId = empty($userId) ? User::getUserId() : $userId;

        $csUserId = Yii::$app->session->get('userCsId') ?? 0;

        $result = Yii::$app->api->getParsedResponse('GET','brapi/user/external/'.$userId.'?domainPrefix=cb', apiResource: 'cs');

        $csProgramsResult = Yii::$app->api->getResponse('GET','user/'.$csUserId.'/programs', apiResource: 'cs');

        $csValidPrograms = [];

        if(isset($csProgramsResult['body']['programs']) && count($csProgramsResult['body']['programs']) > 0){
            $csValidPrograms = array_column($csProgramsResult['body']['programs'], 'code');
        }

        $data = [];
        $teams = [];

        $userValidPrograms = array_values(Yii::$app->session->get('user_valid_programs'));

        if(isset($result['data'][0]['permissions']['memberOf']['units'])){
            $teamInfo = $result['data'][0]['permissions']['memberOf']['units'];

            foreach ($teamInfo as $value){

                if(strtolower($value['type']) == 'program' && in_array($value['code'],$userValidPrograms) &&  in_array($value['code'],$csValidPrograms)){
                    if(!in_array($value['id'], array_keys($teams))){
                        $teams[$value['id']] = $value;
                    }
                }
            }
            
            if(!empty($teams)){
                foreach ($teams as $key => $value){

                    $memberCount = Yii::$app->api->getResponse('GET','program/'.$value['id'].'/members', apiResource: 'cs');
                    
                    $data[] = [
                        'abbrev' => $value['code'],
                        'count' => isset($memberCount['body']['members']) ? count($memberCount['body']['members']) : 0,
                        'id' => $value['id'], 
                        'role' => $value['roles'][0],
                        'team' => $value['name'] . ' ('.$value['code'].')',
                        'teamMemberId' => $value['id']
                    ];
                }
            }
        }
       
        return $data;
    }

    /**
     * Adds user to team with the assigned role
     *
     * @param $userId integer user identifier
     * @param $teamId integer team identifier
     * @param $roleId integer role identifier
     */
    public function addUserToTeam($userId,$teamId,$roleId){
        $record = json_encode([
            'records' => [[
                'teamDbId'=>$teamId,
                'memberDbId'=>$userId,
                'roleDbId'=>$roleId,
              ]]
          ]);
        
        Yii::$app->api->getResponse('POST','team-members/',$record);
    }

    /**
     * Refreshes user's session
     */
    public function refreshUserSession(){

        $session = Yii::$app->session;
        $session->set('user.is_logged_in',true);
        $session->set('user.id',$session->get('user.id'));
        $session->set('user.email',$session->get('user.email'));
        $session->set('user.type',$session->get('user.type'));
        $session->set('user.display_name',$session->get('user.display_name'));
        $session->set('user.username',$session->get('user.username'));
        $session->set('user.start_date',$session->get('user.start_date'));
        $session->set('user.end_date',$session->get('user.end_date'));
        $session->set('user.first_name',$session->get('user.first_name'));
        $session->set('user.last_name',$session->get('user.last_name'));
    }

    /**
     * Check if user has access to program
     *
     * @param Text $program Program abbrev identifier
     * @return Boolean whether user has access to program or not
     */
    public function checkIfUserHasAccessToProgram($program){

        // get valid programs from session
        $programs = Yii::$app->session->get('user_valid_programs') ?? [];

        // return true if program is in valid list of programs
        if(in_array($program, $programs)){
            return 'true';
        }

        return 'false';
    }

    /**
    * Process user parameters
    *
    * @param object $userParams unprocessed parameters
    * @param string $method API method identifier 
    * 
    * @return object $processedParams contains record(s) of processed params
    */
    public function processUserParams ($userParams, $method=null)
    {
        $processedBodyParams = [
            'personType' => 'user',
            'isActive' => 'false',
        ];

        // Re-assign values to camelCase fields...
        // ...so that API can recognize the key-value pairs
        foreach ($userParams as $key => $value) {
            if ($key == 'person_type') {
                $processedBodyParams['personType'] = $value == 'on' ?
                    'admin' : 'user';
            } else if ($key == 'is_active') {
                $processedBodyParams['isActive'] = $value == 'on' ?
                    'true' : 'false';
            } else if ($key == 'first_name') {
                $processedBodyParams['firstName'] = $value;
            } else if ($key == 'last_name') {
                $processedBodyParams['lastName'] = $value;
            } else if ($key == 'person_name') {
                $processedBodyParams['personName'] = $value;
            } else {
                $processedBodyParams[$key] = $value;
            }
        }

        if ($method === 'POST') {
            $processedBodyParams = [
                'records' => [$processedBodyParams]
            ];
        }

        return $processedBodyParams;
    }

    /**
     * Create a user account
     *
     * @param object $params POST parameters
     * 
     * @return mixed $data created user info
     */
    public function createUser ($params)
    {
        $res = Yii::$app->api->getParsedResponse(
            'POST',
            'persons',
            json_encode($params)
        );

        $data = null;

        if (
            $res['success'] &&
            isset($res['data'])
        )
            $data = $res['data'][0];

        return $data;
    }

    /**
     * Retrieves registered users in the database given search parameters
     * 
     * @return array collection of registered users in the database
     * 
     */
    public function searchUsers($params)
    {
        $params = (empty($params) || count($params) == 0) ? new stdClass : $params;
        $response = Yii::$app->api->getResponse('POST', 'persons-search?forDataBrowser', json_encode($params), true);
        $data = null;
        
        // check if has result and retrieve menu data of the users
        if (
            isset($response['status']) &&
            $response['status'] == 200 &&
            isset($response['body']['result']['data'])
        ) {
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];

            $limit = count($data);

            // Capitalize
            for ($i = 0; $i < $limit; $i++)
                $data[$i]['personType'] = ucfirst($data[$i]['personType']);
        }

        return $data;   
    }

    /** 
     * Returns an array data provider containing seasons info
     * 
     * @return mixed data provider for users
    */
    public function getUsersArrayDataProvider ()
    {
        return new ArrayDataProvider([
            'allModels' => $this->getAllUsers(),
            'key' => 'personDbId',
            'sort' => [
                'attributes' => [
                    'personDbId',
                    'email',
                    'username',
                    'firstName',
                    'lastName',
                    'personName',
                    'personType',
                    'isActive',
                    'creator',
                    'modifier',
                    'creationTimestamp',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'personDbId' => SORT_DESC
                ]
            ],
            'id' => 'user-data-provider'
        ]);
    }

    /**
     * Retrieves registered users in the database
     * 
     * @return mixed collection of registered users in the database
     */
    public function getAllUsers ()
    {
        $res = Yii::$app->api->getParsedResponse(
            'GET',
            'persons?forDataBrowser',
            null,
            '',
            true
        );
        $data = [];

        // Check if it has data, retrieve, and then capitalize person types
        if (
            $res['success'] &&
            isset($res['data'])
        ) {
            $data = $res['data'];

            $limit = count($data);

            // Capitalize first letters
            for ($i = 0; $i < $limit; $i++)
                $data[$i]['personType'] = ucfirst($data[$i]['personType']);
        }

        return $data;
    }

    /**
     * Loads a model's props with an instance's values
     * 
     * @param integer $id unique identifier
     * 
     * @return User $model info of user
     */
    public function getInitializedUserModel ($id)
    {
        $model = $this;
        $userInstance = $this->getUser($id);

        if ($userInstance !== null) {
            $model['creationTimestamp'] = $userInstance['creationTimestamp'];
            $model['creation_timestamp'] = $userInstance['creationTimestamp'];
            $model['creator'] = $userInstance['creator'];
            $model['creatorDbId'] = $userInstance['creatorDbId'];
            $model['creator_id'] = $userInstance['creatorDbId'];
            $model['email'] = $userInstance['email'];
            $model['firstName'] = $userInstance['firstName'];
            $model['first_name'] = $userInstance['firstName'];
            $model['isActive'] = $userInstance['isActive'];
            $model['is_active'] = $userInstance['isActive'];
            $model['lastName'] = $userInstance['lastName'];
            $model['last_name'] = $userInstance['lastName'];
            $model['modificationTimestamp'] = $userInstance['modificationTimestamp'];
            $model['modification_timestamp'] = $userInstance['modificationTimestamp'];
            $model['modifier'] = $userInstance['modifier'];
            $model['modifierDbId'] = $userInstance['modifierDbId'];
            $model['modifier_id'] = $userInstance['modifierDbId'];
            $model['personDbId'] = $userInstance['personDbId'];
            $model['personName'] = $userInstance['personName'];
            $model['personType'] = $userInstance['personType'];
            $model['person_type'] = $userInstance['personType'];
            $model['username'] = $userInstance['username'];

            return $model;
        }
    }

    /**
     * Retrieves a registered user in the database
     * 
     * @param integer $id - Unique user identifier
     * 
     * @return object - User info
     */
    public function getUser ($id)
    {
        // if user ID is not set
        if(empty($id)){
            return null;
        }

        $res = Yii::$app->api->getParsedResponse(
            'GET',
            'persons/' . $id
        );
        $data = null;

        // check if has result and retrieve menu data of the user
        if (
            $res['success'] &&
            !empty($res['data'])
        ) {
            $data = $res['data'][0];

            // Capitalize
            $data['personType'] = ucfirst($data['personType']);
        } else 
            $data = null;

        return $data;
    }

    /**
     * Retrieve person information in view person
     *
     * @param $id integer person identifier
     * @return $data array person information
     *
     */
    public function getPersonInfo($id) {
        $basic = null;

        // basic
        $personData = Yii::$app->api->getResponse('GET', 'persons/' . $id);
        if(isset($personData['status']) && $personData['status'] == 200 && isset($personData['body']['result']['data'][0])) {
            $data = $personData['body']['result']['data'][0];
            $basicData = [
                'Email' => $data['email'],
                'Username' => $data['username'],
                'First Name' => $data['firstName'],
                'Last Name' => $data['lastName'],
                'Full Name' => $data['personName'],
                'Type' => ucfirst($data['personType']),
                'Is Active?' => ($data['isActive'])? 'Yes': 'No',
            ];
            $Audit = [
                'Creator' => $data['creator'],
                'Creation Timestamp' => $data['creationTimestamp'],
                'Modifier' => $data['modifier'],
                'Modification Timestamp' => $data['modificationTimestamp'],
            ];
            $basic = [
                'basic' => $basicData,
                'audit' => $Audit
            ];
        }

        return [
            'basic' => $basic
        ];
    }

    /**
     * Update a registered user's information
     *
     * @param $id integer user identifier
     * @param $params object PUT parameters
     * 
     * @return boolean
     */
    public function updateUser ($id, $params)
    {
        $res = Yii::$app->api->getParsedResponse(
            'PUT',
            'persons/' . $id,
            json_encode($params)
        );

        return $res['success'];
        
    }

    /**
     * Deletes a registered user in the database 
     *
     * @param $id integer user identifier
     * 
     * @return boolean
     */
    public function deleteUser ($id)
    {
        $res = Yii::$app->api->getParsedResponse(
            'DELETE',
            'persons/' . $id
        );

        return $res['success'];

    }

    /**
     * Checks if a given email address already exists in the database
     * @param $email string
     * @param $userId integer user identifier
     * @param $action 'create' or 'update'
     * @return boolean
     * 
     */
    public function isEmailAddressExisting($email, $userId, $action)
    {
        $params = ['email' => $email];
        $response = Yii::$app->api->getResponse('POST', 'persons-search', json_encode($params), 'forDataBrowser', false);
        $data = null;

        // check if has result and retrieve menu data of the users
        if (
            isset($response['status']) 
            && $response['status'] == 200
            && isset($response['body']['result']['data'])
        ) {
            
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];

        } else 
            $data = [];

        // check if $email matches with exactly one of the $data's email addresses
        foreach ($data as $info) {
            if (
                $action == 'update' &&
                $userId != null &&
                $info['personDbId'] != $userId &&
                $info['email'] == $email
            )
                return true;
            else if (
                $action == 'create' &&
                $info['email'] == $email
            )
                return true;
        }

        return false;   
    }
    
    /**
     * Retrieves person by email
     *
     * @param $email text email of user
     * 
     * @return $result array person information
     */
    public function getPersonInfoByEmail($email){
      $sql = "select * from tenant.person where email = '$email' and is_void = false";
      $result = Yii::$app->db->CreateCommand($sql)->queryAll();

      return !empty($result) ? $result[0] : null;
    }
}
