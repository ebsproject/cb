<?php

/*
 * This file is part of Breeding4Rice.
 *
 * Breeding4Rice is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Rice is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\account\models\ListModel;
use app\interfaces\models\IPlatformListMember;

/**
 * This is the model class for table "platform.list_member".
 *
 * @property int $id
 * @property int $list_id
 * @property int $data_id
 * @property int $order_number
 * @property string $display_value
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $record_uuid
 */

class PlatformListMember extends BaseModel implements IPlatformListMember
{
    public $creatorName;   // Name of the creator
    public $modifierName;  // Name of the user who modified the list

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['id', 'list_id', 'data_id', 'order_number', 'creator_id', 'modifier_id'], 'integer'],
            [['creatorName', 'modifierName', 'display_value', 'remarks', 'creation_timestamp', 'modification_timestamp', 'notes', 'record_uuid'], 'safe'],
            [['is_void'], 'boolean'],
        ];
    }

    
    /**
     *  Creates data provider instance with search query applied
     *  
     *  @param string $type list type
     *  @param integer $id list ID
     *  @param boolean $is_void whether to get voided or unvoided items
     *  @param array $params contains different filter options
     *  @param boolean $pagination whether the pagination of the browser should be set
     *  @param boolean $isView whether the intended target for the dataprovider is the modal view or not
     *  @param string $searchModel the name of the search model that is used for filtering
     *
     *  @return ActiveDataProvider
     */

    public function search(string $type=null, $id, $is_void, $params, $pagination=null, $isView=null, $searchModel=null)
    {
        $query = PlatformListMember::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' =>['defaultOrder'=>['order_number' => SORT_ASC,]]
        ]);
        $query->joinWith([ 'creator']);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'list_id' => $this->list_id,
            'data_id' => $this->data_id,
            'order_number' => $this->order_number,
            'creation_timestamp' => $this->creation_timestamp,
            'creator_id' => $this->creator_id,
            'modification_timestamp' => $this->modification_timestamp,
            'modifier_id' => $this->modifier_id,
            'list_member.is_void' => $this->is_void,
        ]);

        $query->andFilterWhere(['ilike', 'display_value', $this->display_value])
            ->andFilterWhere(['ilike', 'remarks', $this->remarks])
            ->andFilterWhere(['ilike', 'notes', $this->notes])
            ->andFilterWhere(['ilike', 'record_uuid', $this->record_uuid]);

        $dataProvider->sort->attributes['order_number'] = [
            'asc' => ['list_member.order_number' => SORT_ASC],  
            'desc' => ['list_member.order_number' => SORT_DESC],
        ];

        return $dataProvider;
    }

    /**
    * @return \yii\db\ActiveQuery
    */

    public function getCreatorName(){
        return $this->creator->display_name;
    }

    /**
    * @return \yii\db\ActiveQuery
    */

    public function getModifierName(){
        
        if(!empty($modifier)){
            return $this->modifier->display_name;
        }
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */

    public function getModifier()
    {
        return $this->hasOne(User::className(), ['id' => 'modifier_id']);
    }

    /**
     * Bulk update for list members; calls POST /v3/broker call
     * @param String $listMemberDbIds ID of the list members to be updated; separated by |
     * @param Integer $listId ID of the list
     * @param Array $requestData contains the attributes and values to be updated
     * @return Array the response data from API
     */
    public function bulkUpdate($listMemberDbIds, $listId, $requestData) {

        $param = [
            "httpMethod" => "PUT",
            "endpoint" => "v3/list-members",
            "dbIds" => $listMemberDbIds,
            "requestData" => $requestData
        ];

        return Yii::$app->api->getResponse('POST','broker',json_encode($param));

    }

    /**
     * Permanently removes items from a list
     * @param String $listMemberIdStr concatenated IDs of the list members to be removed
     * @return Integer number of rows affected
     */
    public function bulkDelete($listMemberIdStr) {

        $params = [
            "httpMethod" => "DELETE",
            "endpoint" => "v3/list-members",
            "dbIds" => $listMemberIdStr
        ];
        $url = 'broker';
        $response = Yii::$app->api->getResponse('POST', $url, json_encode($params));
        if (isset($response['status']) && !empty($response['status']) && $response['status'] == 200) {
            return $response['body']['metadata']['pagination']['totalCount'];
        }
        
        return 0;
    }

    /**
     * Retrieve list members using v3/lists/:id/members-search call
     * @param Integer $listDbId ID of the list
     * @param String $filters additional filters
     * @param Array $params request body
     * @return Array contains the total pages and the data
     */
    public function searchAllMembers($listDbId, $filters='', $params=null, $retrieveAll=true) {

        $url = 'lists/'.$listDbId.'/members-search';
        $params = empty($params) ? '' : $params;
        
        $response = Yii::$app->api->getResponse('POST', $url, json_encode($params), $filters, $retrieveAll);
        if (isset($response['status']) && !empty($response['status']) && $response['status'] == 200) {
            $data = $response['body'];
            $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
            $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : 0;
            $data = (isset($data['result']['data'])) ? $data['result']['data'] : [];
        } else {
            $totalPages = 0;
            $totalCount = 0;
            $data = [];
        }

        return [
            'totalPages' => $totalPages,
            'data' => $data,
            'totalCount' => $totalCount
        ];
    }

    /**
     * Updates a given list member
     * @param Integer $listMemberDbId ID of the list member
     * @param Array $requestData request body containing the attributes to be updated
     * @return Array response of the API
     */
    public function updateOne($listMemberDbId, $requestData) {
        $url = 'list-members/'.$listMemberDbId;
        return Yii::$app->api->getResponse('PUT', $url, json_encode($requestData));

    }

    /**
     * Get list members
     *
     * @param $data array filter for searching list
     * @return $result array data for the plan template
     */
    public static function getListMembers($id, $retrieveAll = false){
        $result = NULL;
        $responseApi = Yii::$app->api->getResponse('GET','lists/'.$id.'/members', null, '', $retrieveAll);
        if (isset($responseApi['status']) && $responseApi['status'] == 200 && isset($responseApi['body']['result']['data'])){
            $result = $responseApi['body']['result']['data'];
        }
        return $result;
    }
}