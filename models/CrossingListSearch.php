<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;
use ChromePhp;
use yii\data\ActiveDataProvider;

/**
* This is the ActiveQuery class for Crossing List Search
*/

class CrossingListSearch extends CrossingList
{
	public $crossname;
	public $method;
	public $female_seed_source; //Female parent entno
	public $male_seed_source; //Male parent entno
	public $remarks;
	public $female_parent;
	public $male_parent;

	public function rules(){
		return [
			[['female_entry_id','male_entry_id','creator_id'], 'required'],
			[['crossname','crossing_method_id','female_seed_source','male_seed_source','female_parent','male_parent','method','remarks',], 'default', 'value' => null],
		];
	}

	/**
	 * Search model for the temporary crosses
	 * 
	 * @param $params array list of query parameters
     * @param $experimentId integer experiment identifier
	 */
	public function search($params, $experimentId){

		$condStr = '';
		$this->load($params);
        
		foreach ($params['CrossingListSearch'] as $key => $value) {
		
			if(isset($value) && $value != ''){
				$value = trim($value);
				if($key == 'female_seed_source' || $key == 'male_seed_source'){
					if(!is_numeric($value)){
						$value = 0;
					}
	                $condStr = empty($condStr) ? " t.".$key." = ".$value : $condStr. " and t.".$key. "=".$value;
				}else{
					$condStr = empty($condStr) ? " cast(t." .$key." as text) ilike '". $value ."%'" : $condStr . " and cast(t." .$key." as text) ilike '". $value ."%'";
				}
		    }
		}
        
		return CrossingList::getDataProvider($condStr,$experimentId);
	}
}

?>
