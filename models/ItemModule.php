<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.item_module".
 *
 * @property int $id Primary key of the record in the table
 * @property int $item_id Parent item of the child module
 * @property int $module_id Child module of the parent item 
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 */
class ItemModule extends ItemModuleBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.item_module';
    }

    /**
     * Retrieves an existing item module in the database
     * @param $condParam array condition parameters 
     * @return array item module's record in the database
     */
    public static function getItemModule($param){
        $method = 'POST';
        $path = 'item-modules-search';
        
        $data = Yii::$app->api->getResponse($method, $path, json_encode($param), true);
        $result = [];
         
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
          $result = $data['body']['result']['data'][0];
        }
        return $result;
    }
}
