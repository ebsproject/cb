<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

use app\controllers\BrowserController;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\IExperiment;
use app\models\EntryList;

use app\modules\experimentCreation\models\BrowserModel;

/**
 * This is the model class for table "experiment.experiment".
 *
 * @property int $id Experiment ID: Database identifier of the experiment [EXPT_ID]
 * @property int $program_id Program ID: Reference to the program that created the experiment [EXPT_PROG_ID]
 * @property int $pipeline_id Pipeline ID: Reference to the pipeline where the experiment is subjected [EXPT_PIPE_ID]
 * @property int $stage_id Stage ID: Reference to the stage in the pipeline of the experiment [EXPT_STAGE_ID]
 * @property int|null $project_id Project ID: Reference to the project where the experiment is conceived [EXPT_PROJ_ID]
 * @property int $experiment_year Experiment Year: Year when the experiment is evaluated, normally the year when it started [EXPT_YEAR]
 * @property int $season_id Season ID: Reference to the season (cycle) when the experiment was conducted [EXPT_SEASON_ID]
 * @property string $planting_season Planting Season: Name of the planting season where the experiment is conducted, usually based on an agreed naming convention (e.g. 2020DS, 2020WS) [EXPT_PLANTINGSEASON]
 * @property string $experiment_code Experiment Code: Textual identifier of the experiment [EXPT_CODE]
 * @property string $experiment_name Experiment Name: Full name of the experiment [EXPT_NAME]
 * @property string $experiment_objective Experiment Objective: Purpose of the experiment in the context of its pipeline, stage, type, and sub-type provided by the user [EXPT_OBJECTIVE]
 * @property string $experiment_type Experiment Type: Type of the experiment {trial, nursery} [EXPT_TYPE]
 * @property string|null $experiment_sub_type Experiment Sub-Type: Sub-type of the experiment, which depends on the experiment objective or the experiment type [EXPT_SUBTYPE]
 * @property string|null $experiment_sub_sub_type Experiment Sub-Sub-Type: Sub-sub-type of the experiment, which depends on the experiment objective or  thecontext of the experiment sub-sub-type [EXPT_SUBSUBTYPE]
 * @property string|null $experiment_design_type Experiment Design: Design type of the experiment {RCBD, Augmented RCBD, P-REP, ...} [EXPT_DESIGNTYPE]
 * @property string $experiment_status Experiment Status: Status of the experiment {draft, created, active, completed} [EXPT_STATUS]
 * @property string|null $description Experiment Description: Additional information about the experiment [EXPT_DESC]
 * @property int $steward_id Steward ID: Reference to the steward (person) who manages the experiment [EXPT_PERSON_ID]
 * @property int|null $experiment_plan_id Experiment Plan ID: Reference to the experiment plan where the experiment may be based [EXPT_EXPTPLAN_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 * @property int|null $data_process_id Data process of the experiment
 * @property int $crop_id Crop ID: Reference to the experiment"s crop [EXP_CROP_ID]
 */
class Experiment extends BaseModel implements IExperiment {

    public function __construct (
        public EntryList $entryList,
        public Item $item,
        $config = []
    )
    {
        parent::__construct($config);
    }

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'experiments';
    }

    /**
     * Get experiment browser data provider
     * 
     * @param array $params optional search filters
     * @param array $pageParams data browser parameters
     * @param string $currentPage current page parameter
     * @param boolean $copyView flag if the browser is for entry list
     * @return mixed $dataProvider includes experiment attributes and data provider
     */
    public function getDataProvider($params = null, $pageParams = '', $currentPage = null, $copyView = false){

        $attributes = [
            'experimentDbId',
            'experimentYear',
            'programDbId',
            'programCode',
            'programName',
            'experimentTemplate',
            'pipelineDbId',
            'pipelineCode',
            'pipelineName',
            'stageDbId',
            'stageCode',
            'stageName',
            'projectDbId',
            'projectCode',
            'projectName',
            'seasonDbId',
            'seasonCode',
            'seasonName',
            'cropDbId',
            'cropCode',
            'cropName',
            'plantingSeason',
            'experimentCode',
            'experimentName',
            'experimentObjective',
            'experimentType',
            'experimentSubType',
            'experimentSubSubType',
            'experimentDesignType',
            'experimentStatus',
            'description',
            'dataProcessDbId',
            'dataProcessAbbrev',
            'stewardDbId',
            'steward',
            'experimentPlanDbId',
            'entryCount',
            'occurrenceCount',
            'creationTimestamp',
            'creatorDbId',
            'creator',
            'modificationTimestamp',
            'modifierDbId',
            'modifier',                       
        ];
      
        $data = Experiment::searchBrowserRecords($params, $pageParams, $copyView); 
        $experimentRecords = $data['data'];
        $allRecCount = $data['allRecCount'];

        if((isset($currentPage) && !empty($currentPage)) && $allRecCount == 0){
            $filters = str_replace('page='.$currentPage,'', $pageParams);
            $data = Experiment::searchBrowserRecords($params, $filters, $copyView); 
            $experimentRecords = $data['data'];
            $allRecCount = $data['allRecCount'];
            if($copyView){
                $_GET["dynagrid-copyexperiment-browser-page"] = 1;
            } else $_GET["dynagrid-experiment-browser-page"] = 1;
        }

        $browserName = 'dynagrid-experiment-browser';
        if($copyView){
            $browserName = 'dynagrid-copyexperiment-browser';
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $experimentRecords,
            'key' => 'experimentDbId',
            'sort' => [
                'attributes' => $attributes,
                'defaultOrder' => ['experimentYear'=>SORT_DESC, 'creationTimestamp'=>SORT_DESC],
            ],
            'restified' => true,
            'id' => $browserName,
            'totalCount' => $allRecCount
        ]);
        
        $dataProvider = [
            'dataProvider' => $dataProvider,
            'attributes' => $attributes,
        ];

        return $dataProvider;
    }

    /**
     * Format experiment statuses
     * 
     * @param array $status list of distinct statuses
     */
    public function formatExperimentStatus($status) {
        $statuses = array_map('trim', explode(';', strtoupper($status)));
  
        // If "planted" status is present display it before current status
        $formattedStatus = in_array('PLANTED', $statuses) ? '<span class="new badge green darken-3" style="margin: 1px"><b>PLANTED</b></span>' : '';
        $statuses = array_diff($statuses, ['PLANTED']);
        
        $statuses = array_filter($statuses);    // Remove empty statuses
        if($statuses){
            // Display the current status
            $currentStatus = end($statuses);
            switch ($currentStatus) {
                case 'DESIGN REQUESTED':
                case 'ENTRY LIST INCOMPLETE SEED SOURCES':
                case 'BACKGROUND JOB IN PROGRESS':
                case 'DELETION OF ENTRIES IN PROGRESS':
                case 'UPDATING OF ENTRIES IN PROGRESS':
                case 'DELETION OF PLOTS IN PROGRESS':
                case 'DELETION OF OCCURRENCES IN PROGRESS':    
                case 'RANDOMIZATION IN PROGRESS':
                case 'ADDING OF OCCURRENCES IN PROGRESS':
                case 'ADDING OF ENTRIES IN PROGRESS':
                case 'COPYING OF EXPERIMENT IN PROGRESS':
                case 'GENERATION OF OCCURRENCES IN PROGRESS':
                case 'GENERATION OF PLANTING ARRANGEMENT IN PROGRESS':
                case 'ADDING OF EXPERIMENT_BLOCKS IN PROGRESS':
                case 'UPDATING OF PLANTING_INSTRUCTIONS IN PROGRESS':
                case 'FINALIZING OF EXPERIMENT IN PROGRESS':
                case 'DELETION OF CROSSES IN PROGRESS':  
                case 'UPDATING OF ENTRY_DATA IN PROGRESS':
                    $class = 'orange';
                break;

                case 'ENTRY LIST CREATED':
                case 'CROSS LIST CREATED':
                case 'PROTOCOL SPECIFIED':
                case 'DESIGN GENERATED':
                case 'EXPERIMENT GROUP SPECIFIED':
                case 'OCCURRENCES CREATED':
                    $class = 'blue';
                break;

                case 'INCOMPLETE':
                case 'DELETION OF ENTRIES FAILED':
                case 'DELETION OF PLOTS FAILED':
                case 'DELETION OF OCCURRENCES FAILED':
                case 'UPDATING OF ENTRIES FAILED':
                case 'ADDING OF OCCURRENCES FAILED':
                case 'ADDING OF ENTRIES FAILED':
                case 'ADDING OF EXPERIMENT_BLOCKS FAILED':
                case 'BACKGROUND JOB ERROR';
                case 'UPDATING OF PLANTING_INSTRUCTIONS FAILED':
                case 'UPDATING OF CROSSES FAILED':
                case 'UPDATING OF ENTRY_DATA FAILED':
                    $class = 'red';
                break;

                case 'DELETION OF ENTRIES IN QUEUE':
                case 'UPDATING OF ENTRIES IN QUEUE':
                case 'DELETION OF OCCURRENCES IN QUEUE':
                case 'ADDING OF OCCURRENCES IN QUEUE':    
                case 'ADDING OF ENTRIES IN QUEUE':
                case 'DELETION OF PLOTS IN QUEUE':  
                case 'ADDING OF EXPERIMENT_BLOCKS IN QUEUE':  
                case 'UPDATING OF PLANTING_INSTRUCTIONS IN QUEUE':
                case 'FINALIZING OF EXPERIMENT IN QUEUE':
                case 'UPDATING OF CROSSES IN QUEUE':
                case 'UPDATING OF ENTRY_DATA IN QUEUE':
                    $class = 'yellow darken-2';
                break;

                case 'COMPLETED':
                case 'CREATED':
                case 'CROSSED':
                case 'MAPPED':
                    $class = 'green darken-3';
                break;

                default:
                    $class = 'grey';
            }
            $formattedStatus .= '<span class="new badge ' . $class . '" style="margin: 1px"><b>' . $currentStatus . '</b></span>';
        }
       
        return $formattedStatus;
    }

    /**
     * Format background worker statuses
     * 
     * @param Integer $bgId background worker identifier
     */
    public function formatBackgroundWorkerStatus($bgId){
        sleep(2);//loading buffer
        $bgName = '';
        $backgroundJob = \Yii::$container->get('app\models\BackgroundJob');
        $bgInfo = $backgroundJob->getOne($bgId)['data'];
        
        $method = $bgInfo['method'];
        $bgStatus = str_replace('_', ' ',strtolower($bgInfo['jobStatus']));
        $endpointEntity = isset($bgInfo['endpointEntity']) ? strtolower(BrowserController::pluralize(2, $bgInfo['endpointEntity'])) : '';

        switch($method){
            case 'PUT':
                $bgName = 'updating';
            break;
            case 'DELETE':
                $bgName = 'deletion';
            break;
            case 'POST':
                if($endpointEntity == 'experiments'){
                    if(str_contains(strtolower($bgInfo['description']), 'finalizing')){
                        $bgName = 'finalizing';
                        $endpointEntity = 'experiment';
                    } else {
                        $bgName = 'copying';
                        $endpointEntity = 'experiment';
                    }
                } else if($bgInfo['endpointEntity'] == 'OCCURRENCE' && $bgInfo['workerName'] == 'ExperimentCreationProcessor'){
                    $bgName = 'generation';
                    $endpointEntity = 'occurrences';
                } else {
                    $bgName = 'adding';
                }
            break;
        }

        if(!empty($bgName) && !empty($endpointEntity) && !empty($bgStatus)){
            $bgName = $bgStatus == 'done' ? $bgStatus : "$bgName of ".$endpointEntity." $bgStatus";
        }
        
        return $bgName;
    }

    /**
     * Search for experiments given search parameters
     *
     * @param array $params list of search parameters
     * @return array $data list of experiments given search parameters
     */
    public function searchRecords($params = null){
        // add condition for filtering owned experiments
        $isOwned = (isset($params['owned']) && $params['owned'] == 'true') ? '?isOwned' : '';
        // remove owned and siteDbId in POST search params
        unset($params['owned']);
        unset($params['siteDbId']);
        
        $results = Yii::$app->api->getResponse('POST', 'experiments-search'.$isOwned, json_encode($params), null, true);
        
        $data = [];

        // check if successfully retrieved experiments
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])){
            $data = $results['body']['result']['data'];
        }

        return $data;
    }

    /**
     * Search for experiments given search parameters
     *
     * @param array $params list of search parameters
     * @param string $pageParams data browser parameters
     * @param boolean $copyView flag if the browser is for entry list
     * @return array $data list of experiments given search parameters
     */
    public function searchBrowserRecords($params = null, $pageParams = '', $copyView = false){
        // add condition for filtering owned experiments
        $isOwned = (isset($params['owned']) && $params['owned'] == 'true') ? '?isOwned' : '';
        $pageParams = !empty($pageParams) && !empty($isOwned) ? '&'.str_replace('?','',$pageParams) : $pageParams;
        
        // remove owned and siteDbId in POST search params
        unset($params['owned']);
        unset($params['siteDbId']);

        if(!$copyView){
            if($pageParams == NULL){
                $pageParams = '?sort=experimentDbId:desc';
            }else if(strpos($pageParams, 'sort') !== false){
                $sortStr = explode('sort', $pageParams);
                if(strpos($sortStr[1], 'experimentDbId') === false){
                    $pageParams .= '|experimentDbId:desc';
                }
            } else {
                $pageParams .= '&sort=experimentDbId:desc';
            }
        }
        if(!$copyView){
            if(isset($params['experimentStatus']) && $params['experimentStatus'] != 'created'){ 
                $params['experimentStatus'] = '%'.$params['experimentStatus'];
            }
        }
        $path = 'experiments-search'.$isOwned.$pageParams;
        $results = Yii::$app->api->getResponse('POST', $path, json_encode($params), null, false);
        $data = [];
        
        // check if successfully retrieved experiments
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])){
            $data = $results['body']['result']['data'];
        }
        
        $totalCount = $results['body']['metadata']['pagination']['totalCount'];

        return ['data'=>$data, 'allRecCount'=>$totalCount];
    }

    /**
     * Retrieve existing experiments
     * @param filter array resource call filter
     * @param sort array resource call sort
     * @return array experiment record in the database
    */
    public function getExperiments($filter = null, $sort = null){
        $method = 'POST';
        $path = 'experiments-search';
        if(!empty($filter)){
            $data = Yii::$app->api->getResponse($method, $path, json_encode($filter), $sort, true);
        }else{
            $data = Yii::$app->api->getResponse($method, $path, null, null, true);
        }
        
        $result = [];
        
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
        }
        
        return $result;
    }
    

     /**
     * Retrieves an existing expriment in the database
     * @param $id integer experiment identifier 
     * @return array experiment record in the database
     */
    public function getExperiment($id){
      $method = 'POST';
      $path = 'experiments-search';
      $param = ['experimentDbId' => "$id"];

      $data = Yii::$app->api->getResponse($method, $path, json_encode($param), null, true);
      
      $result = null;
       
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        $result = $data['body']['result']['data'][0];
      }
      return $result;
    }
    

    /**
     * Create new records of an experiment
     * @param $params array experiment record to create
     * @return $array array containing result of the saving and the experiment id
     */
    public function createExperiment($params){
        $method = 'POST';
        $path = 'experiments';
  
        $processedParams = [
            'records' => [$params]
        ];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($processedParams));
         
        $result = ((isset($data['status'])) && $data['status'] == 200) ? true : false;
        
        $experimentId = isset($data['body']['result']['data'][0]['experimentDbId']) ? $data['body']['result']['data'][0]['experimentDbId'] : null;
      
        $errorMsg = '';
        if(!$result){
           $errorMsg = $data['body']['metadata']['status'][0]['message'];
        }

        return ['status'=>$result, 'error'=>$errorMsg,'experimentId'=>$experimentId];
    }

    /**
     * Create new records of an experiment's data
     * @param $id integer experiment ID
     * @param $params array of experiment data to save
     * @return $result boolean status of the saving
     */
    public function createExperimentData($id, $params){
        $method = 'POST';
        $path = 'experiments/'.$id.'/data';
        $processedParams = [
            'records' => $params
        ];

        $data = Yii::$app->api->getResponse($method, $path, json_encode($processedParams));
    
        $result = ((isset($data['status'])) && $data['status'] == 200) ? true : false;

        $errorMsg = '';
        if(!$result){
           $errorMsg = $data['body']['metadata']['status'][0]['message'];
        }
        return ['status'=>$result, 'error'=>$errorMsg];
    }

    /**
     * Update Experiment
     * @param $id integer experiment ID
     * @param $params array of experiment to update
     * @return $result boolean status of the saving
     */
    public function updateExperiment($id, $params){
        $method = 'PUT';
        $path = 'experiments/'.$id;
        
        $data = Yii::$app->api->getResponse($method, $path, json_encode($params));
        
        $result = ((isset($data['status'])) && $data['status'] == 200) ? true : false;

        $errorMsg = '';
        if(!$result){
           $errorMsg = $data['body']['metadata']['status'][0]['message'];
        }

        return ['status'=>$result, 'error'=>$errorMsg];
    }

    /**
     * Method for retrieving all the entry data records from a given a specific experiment id
     * 
     * @param Integer $dbId experiment record identifier
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @param Boolean $retrieveAll optional retrieve all data
     * @return Array the records from the database
     */
    public function searchAllEntries($dbId, $params = null, $filters = '', $retrieveAll = true){
        
        $entryLists = $this->entryList->searchAll(['experimentDbId' => "equals ".$dbId]);
        $entryListDbId = isset($entryLists['data'][0]['entryListDbId']) ? $entryLists['data'][0]['entryListDbId'] : 0;

        return \Yii::$container->get('app\models\Entry')->searchAll(["fields"=>"entry.id as entryDbId|entry.entry_list_id as entryListDbId",'entryListDbId' => "equals ".$entryListDbId]);

    }

    /**
    * Retrieve entry information in view experiment
    *
    * @param $id integer experiment identifier
    * @param $crossSearchModel string Cross Search Model
    * @return $data array experiment information
    */
    public function getExperimentInfo($id, $crossSearchModel = null){
        $basic = null;
        $entryData = [];
        $dataProcessAbbrev = '';
        $crosses = null;
        $entries = null;
        $entryCount = 0;

        // basic
        $filters = [
            'experimentDbId' => $id,
        ];
        $crossesActAbbrev = '';

        $packageData = Yii::$app->api->getResponse('POST', 'experiments-search', json_encode($filters));
        if(isset($packageData['status']) && $packageData['status'] == 200 && isset($packageData['body']['result']['data'][0])) {
            $data = $packageData['body']['result']['data'][0];


            $dataProcessAbbrev = $data['dataProcessAbbrev'];
            $parentActAbbrev = str_replace('_DATA_PROCESS', '', $dataProcessAbbrev);
            $crossesActAbbrev = $parentActAbbrev.'_CROSSES_ACT';

            $basicData = [
                'Status' => $data['experimentStatus'],
                'Experiment' => $data['experimentName'],
                'Experiment Code' => $data['experimentCode'],
                'Type' => $data['experimentType'],
                'Template' => $data['experimentTemplate'],
                'Project' => $data['projectCode'],
                'Season' => $data['seasonName'],
                'Planting Season' => $data['plantingSeason'],
                'Stage' => $data['stageName'] . ' ('. $data['stageCode'] . ')',
                'Crop' => $data['cropName'],
                'Year' => $data['experimentYear'],
                'Occurrence Count' => $data['occurrenceCount'],
                'Entry Count' => $data['entryCount'],
                'Program' => $data['programName'] . ' ('. $data['programCode'] . ')',
                'Pipeline' => $data['pipelineCode'],
                'Creator' => $data['creator'],
                'Creation Timestamp' => date('M d, Y', strtotime($data['creationTimestamp'])),
                'Modifier' => $data['modifier'],
                'Modification Timestamp' => date('M d, Y', strtotime($data['modificationTimestamp'])),
                'Objective' => $data['experimentObjective'],
                'Design Type' => $data['experimentDesignType'],
                'Sub Type' => $data['experimentSubType'],
                'Sub Sub Type' => $data['experimentSubSubType'],
                'Description' => $data['description'],
                'Steward' => $data['steward'],
            ];
            $basic = [
                'basic' => $basicData
            ];
        }

        // Entry
        $filters = [
            'experimentDbId' => $id,
        ];
       
        $entryData = Yii::$app->api->getResponse('POST', 'entries-search?sort=entryNumber:ASC', json_encode($filters), 'limit=100', false);
        $dataEntries = [];
        if(isset($entryData['status']) && $entryData['status'] == 200 && isset($entryData['body']['result']['data'])) {
            $dataEntries = $entryData['body']['result']['data'];
            $entryCount= $entryData['body']['metadata']['pagination']['totalCount'];
        }
        $entries = new \yii\data\ArrayDataProvider([
            'allModels' => $dataEntries,
            'pagination' => false
        ]);

        $crossesAct = $this->item->getAll("abbrev=$crossesActAbbrev")['totalCount'] > 0;
        
        if($crossesAct){
            $searchModel = $crossSearchModel; 
            $paramFilter = Yii::$app->request->queryParams;

            $mainParam = ["experimentDbId"=>"equals $id"];
            
            $crossesData = $searchModel->search($mainParam, $paramFilter, false);
            $crosses =  new \yii\data\ArrayDataProvider([
                'allModels' => $crossesData->allModels,
                'pagination' => false,
            ]);
        }
        
        // Get current program
        $program = Yii::$app->userprogram->get('abbrev');
        // Get entry columns
        $browserController = \Yii::$container->get('app\controllers\BrowserController');
        $columns = $browserController->getColumns($program, 'VIEW_OCCURRENCE_DATA', 'VIEW_ENTRY_COLUMNS');

        $cols = [];
        foreach ($columns as $key => $value) {
            // Remove germplasm info widget
            if(strtolower($value['attribute']) == 'entryname'){
                $cols[] = [
                    'label' => $value['label'] ?? '',
                    'attribute' => $value['attribute'] ?? ''
                ];
            }
            else {
                $cols[] = $value;
            }
        }

        // Get occurrences data
        $occurrences = \Yii::$container->get('app\models\Occurrence')->getDataProvider($id, null, [], true, ['occurrenceName'=>SORT_ASC]);
        $occurrenceRecords = \Yii::$container->get('app\models\Occurrence')->searchAll([
            'experimentDbId' => "equals $id",
            'fields' => 'occurrence.id AS occurrenceDbId|experiment.id AS experimentDbId',
        ],'limit=1');
        $occurrenceDbId = isset($occurrenceRecords['data'][0]) ? $occurrenceRecords['data'][0]['occurrenceDbId'] : 0;
        $occurrences['plotCount'] = \Yii::$container->get('app\models\Occurrence')->searchAllPlots($occurrenceDbId, null,'limit=1&sort=plotDbId:asc')['totalCount'];
       
        return [
            'basic' => $basic,
            'entry' => $entries,
            'entryCount' => $entryCount,
            'plotCount' => $occurrences['plotCount'] ,
            'crosses' => $crosses,
            'cols' => $cols,
            'occurrences' => $occurrences
        ];
    }

    /**
     * Retrieves experiment info in the format
     * applicable to select2 elements
     * @param requestBody - search parameters
     * @param urlParams - sort, limit, and page
     */
    public function getExperimentSelect2Data($requestBody = null, $urlParams = '') {
        $data = [];
        $method = 'POST';
        $endpoint = 'experiments-search';

        $result = Yii::$app->api->getParsedResponse($method, $endpoint, $requestBody, $urlParams);
        $experiments = isset($result['data']) ? $result['data'] : [];
        $totalCount = isset($result['totalCount']) ? $result['totalCount'] : 0;
        
        foreach($experiments as $experiment) {
            $data[] = [ 
                'id' => $experiment['experimentDbId'],
                'text' => $experiment['experimentName']
            ];
        }

        return [
            "data" => $data,
            "totalCount" => $totalCount
        ];
    }

    /**
     * Method for retrieving all the entry data records from a given a specific experiment id
     * 
     * @param Integer $dbId experiment record identifier
     * @param Json $requestBody optional request content
     * @param String $urlParams  optional sort, page or limit options
     * @param Boolean $retrieveAll optional retrieve all data
     * @return Array the records from the database
     */
    public function searchAllEntryData($dbId, $requestBody = null, $urlParams = null, $retrieveAll = true){

        $method = 'POST';
        $endpoint = "experiments/$dbId/entry-data-search";

        $result = Yii::$app->api->getParsedResponse($method, $endpoint, json_encode($requestBody), $urlParams, $retrieveAll);
        
        return $result;

    }

}
