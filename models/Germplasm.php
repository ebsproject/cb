<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

use app\interfaces\models\IGermplasm;

class Germplasm extends BaseModel implements IGermplasm
{
    public $program;
    public $creationTimestamp;
    public $creator;
    public $designation;
    public $generation;
    public $modificationTimestamp;
    public $modifier;
    public $nameType;
    public $otherNames;
    public $parentage;
    public $programName;
    public $germplasmNameType;
    public $germplasmState;
    public $germplasmNormalizedName;
    public $germplasmType;
    public $germplasmCode;
    public $taxonomyName;
    public $cropCode;

    public static function apiEndPoint() {
        return 'germplasm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'season_id', 'cross_id', 'creator_id', 'modifier_id', 'gid', 'product_gid_id', 'seed_storage_id', 'organization_id', 'institute_id'], 'integer'],
            [['designation', 'name_type', 'product_type', 'parentage', 'generation'], 'required'],
            [['designation', 'name_type', 'product_type', 'mta_status', 'parentage', 'generation', 'iris_preferred_id', 'breeding_line_name', 'derivative_name', 'fixed_line_name', 'selection_method', 'notes', 'ip_rights'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'program', 'creationTimestamp', 'creator', 'germplasmNameType', 'germplasmType', 'germplasmState', 'germplasmNormalizedName', 'germplasmCode', 'designation', 'generation', 'modificationTimestamp', 'modifier', 'nameType', 'otherNames', 'parentage', 'programName', 'taxonomyName', 'cropCode'], 'safe'],
            [['is_void'], 'boolean'],
            [['display_name', 'product_key', 'system_product_name'], 'string', 'max' => 256],
            [['ip_status'], 'string', 'max' => 32],
            [['product_status'], 'string', 'max' => 255],
            [['product_key'], 'unique'],
        ];
    }

    /**
     * Retrieves existing distinct germplasm types in the database
     *
     * @return array distinct germplasm types
     */
    public function getGermplasmState() {
        $response = Yii::$app->api->getResponse('GET', 'variables?abbrev=germplasm_state');
        $variableDbId = $response['body']['result']['data'][0]['variableDbId'];

        if(!isset($variableDbId)) {
            return [];
        }

        $response = Yii::$app->api->getResponse('GET', 'variables/'.$variableDbId.'/scales');
        $result = $response['body']['result']['data'][0]['scales'][0]['scaleValues'];

        if(!isset($result)) {
            return [];
        }

        return $result;
    }

    /**
     * Retrieves existing distinct germplasm generation in the database
     *
     * @return array distinct germplasm generation
     */
    public function getGermplasmGeneration() {
        $response = Yii::$app->api->getResponse('GET', 'variables?abbrev=generation');
        $variableDbId = $response['body']['result']['data'][0]['variableDbId'];

        if(!isset($variableDbId)) {
            return [];
        }

        $response = Yii::$app->api->getResponse('GET', 'variables/'.$variableDbId.'/scales');
        $result = $response['body']['result']['data'][0]['scales'][0]['scaleValues'];

        if(!isset($result)) {
            return [];
        }

        return $result;
    }

    /**
     * Updates germplasm given id and array of data
     *
     * @param $id integer germplasm identifier
     * @param $data array array of values
     *
     * @return $response text return message of API call
     */
    public function updateGermplasm($id, $data) {
        $response = Yii::$app->api->getResponse('PUT', 'germplasm/' . $id, json_encode($data));

        return ((isset($response['status'])) && $response['status'] == 200) ? true : false;


    }

    /**
     * Search for germplasm given search parameters
     *
     * @param $params array list of search parameters
     * @return $data array list of germplasm given search parameters
     */
    public function searchGermplasm($params){
        $params = empty($params) ? null : $params;
        $response = Yii::$app->api->getResponse('POST', 'germplasm-search', json_encode($params), null, true);
        $data = [];
        // check if successfully retrieved germplasm
        if(($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
        }
        return $data;
    }

    /** 
     * Retrieves the number of entries associated with the given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * @return Array germplasm entries count
     */
    public function getGermplasmEntriesCount($germplasmId){
        $result = 1;

        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId.'/entries-count');
        
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
            $result = $response['body']['result']['data'];
        }

        return $result;

    }

    /** 
     * Retrieves the number of seeds associated with the given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * @return Array germplasm seeds count
     */
    public function getGermplasmSeedsCount($germplasmId){
        $result = 1;

        $response = Yii::$app->api->getResponse('GET', 'germplasm/'.$germplasmId.'/seeds-count');
        
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
            $result = $response['body']['result']['data'];
        }

        return $result;
    }
}
