<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\interfaces\models\ICross;
use app\modules\experimentCreation\models\ExperimentModel;

/**
 * This is the model class for table "germplasm.cross".
 *
 * @property int $id Cross ID: Database identifier of the cross [CROSS_ID]
 * @property string $cross_name Cross Name: Name of the cross, temporarily set to the combination of the parents' names (A/B) but when marked as successfull, it is assigned with a cross name that complies with the naming convention of the organization [CROSS_NAME]
 * @property string $cross_method Cross Method: Method of crossing used in the cross {single cross, backcross, ...} [CROSS_METHOD]
 * @property int|null $germplasm_id Germplasm ID: Reference to the germplasm that is the outcome of the cross [CROSS_GERM_ID]
 * @property int|null $seed_id Seed ID: Reference to the seed that first created the germplasm of the cross [CROSS_SEED_ID]
 * @property int|null $experiment_id Experiment ID: Reference to the experiment where the cross has been made [CROSS_EXPT_ID]
 * @property int|null $entry_id Entry ID: Reference to the entry in the experiment where the cross is added as an entry [CROSS_ENTRY_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class Cross extends BaseModel implements ICross
{
    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'crosses';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        //TODO: remove when crosses have been fully refactored
        return 'operational.cross';
    }

    /**
     * Retrieves existing crosses in the experiment
     * @param String $apiEndPoint the API resource endpoint
     * @param Array $params request body parameter for the api resource
     * @param String $filters optional data filters
     * @param Boolean $retrieveAll whether or not to retrieve all pages of the result
     */
    public function searchAllData($apiEndPoint,$params, $filters = '', $retrieveAll = true){
       $method = 'POST';

       return Yii::$app->api->getParsedResponse($method, $apiEndPoint, json_encode($params), $filters, $retrieveAll);


    }

    /**
     *  Custom get all cross records
     * @param String $filters optional data filters
     * @param Boolean $retrieveAll whether or not to retrieve all pages of the result
     */
    public function getCrosses($filters, $retrieveAll = true){
        $method = 'POST';
        $path = 'crosses-search';

        $data = Yii::$app->api->getResponse($method, $path, $filters, null, $retrieveAll);
        $result = [];
        
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
          $result = $data['body']['result']['data'];
        }
        return $result;
    }

    /**
     * Retrieve cross information
     * 
     * @param array $params request body parameter for the api resource
     * @param string $filters optional data filters
     * @param array $pagination optional pagination not null if pagination is active
     * @param string $sort optional sort data by cross id SORT_ASC/SORT_DESC
     * @param boolean $isForDataBrowser flag for whether data is for regular data browser or for managing crosses
     * 
     * @return ArrayDataProvider $dataProvider Cross list browser data provider
     */
    public function getDataProvider($params = null, $filters = null, $pagination = null, $sort = 'SORT_ASC', $isForDataBrowser = false)
    {
        $dataProviderId = 'crosses-browser';
        $crossDataColumns = [];
        $params['includeParentSource'] = true;

        $output = $this->searchAll($params, $filters, !isset($pagination));
        $data = $output['data'];

        if (!empty($data)) {
            $crossDataColumns = array_keys($data[0]);

            $provider = [
                'allModels' => $data,
                'totalCount' => ($isForDataBrowser) ? $output['totalCount'] : count($data),
                'key' => 'crossDbId',
                'sort' => [
                    'attributes' => $crossDataColumns,
                    'defaultOrder' => [
                        'crossDbId' => $sort
                    ]
                ],
                'id' => $dataProviderId,
                'restified' => true,
            ];
        } else {
            $provider = [
                'allModels' => $data,
                'totalCount' => ($isForDataBrowser) ? $output['totalCount'] : count($data),
                'sort' => [
                    'attributes' => $crossDataColumns,
                ],
                'id' => $dataProviderId,
                'restified' => true,
            ];
        }

        if (!$pagination) {
            $provider['pagination'] = false;
        }

        $dataProvider = new ArrayDataProvider($provider);

        $dataProvider = [
            'dataProvider' => $dataProvider,
            'attributes' => $crossDataColumns
        ];
        return $dataProvider;
    }


    /**
     * Delete all records in germplasm.cross_parent and germplasm.cross tables
     * @param Integer $experimentDbId experiment identifier
     */
    public function deleteCrosses($experimentDbId){
        //retrieve cross parents
        $crossParents = \Yii::$container->get('app\models\CrossParent')->searchAll([
            'fields'=> 'crossParent.id AS crossParentDbId|experiment.id AS experimentDbId',
            'experimentDbId'=>"equals $experimentDbId"
        ]);

        $crossParentsDbIds = isset($crossParents['data'][0]['crossParentDbId']) ? array_column($crossParents['data'],'crossParentDbId') : [];           
        
        //delete cross parents
        \Yii::$container->get('app\models\CrossParent')->deleteMany($crossParentsDbIds);

        $crossAttributes = \Yii::$container->get('app\models\CrossAttribute')->searchAll([
            'fields'=> 'cross_attribute.id AS crossAttributeDbId|experiment.id AS experimentDbId',
            'experimentDbId'=>"equals $experimentDbId"
        ]);

        $crossAttributeDbIds = isset($crossAttributes['data'][0]['crossAttributeDbId']) ? array_column($crossAttributes['data'],'crossAttributeDbId') : [];

        //delete cross CrossAttribute
        \Yii::$container->get('app\models\CrossAttribute')->deleteMany($crossAttributeDbIds);


        //retrieve crosses
        $crosses = Cross::searchAll([
            'fields'=> 'germplasmCross.id AS crossDbId|experiment.id AS experimentDbId',
            'experimentDbId'=>"equals $experimentDbId",
        ]);
        $crossDbIds = isset($crosses['data'][0]['crossDbId']) ? array_column($crosses['data'],'crossDbId') : [];

        //delete crosses
        Cross::deleteMany($crossDbIds);
    }

    /**
     * Delete selected records in germplasm.cross_parent and germplasm.cross tables
     * @param Integer $experimentDbId experiment identifier
     * @param Array $selectedIds array of selected cross ids
     */
    public function deleteSelectedCrosses($experimentDbId, $selectedIds){
        //retrieve cross parents
        $additionalFilter = 'equals '.implode('|equals ', $selectedIds);
        $mainParam = ['experimentDbId'=>$experimentDbId];
        
        if(!empty($additionalFilter)){
            $additionalFilterArr = ["crossDbId"=>"$additionalFilter"];
            $mainParam = array_merge(['experimentDbId'=>$experimentDbId], $additionalFilterArr);
        }

        $crossAttributes = \Yii::$container->get('app\models\CrossAttribute')->searchAll($mainParam);
        $crossAttributeDbIds = isset($crossAttributes['data'][0]['crossAttributeDbId']) ? array_column($crossAttributes['data'],'crossAttributeDbId') : [];
        
        //delete cross CrossAttribute
        \Yii::$container->get('app\models\CrossAttribute')->deleteMany($crossAttributeDbIds);

        $crossParents = \Yii::$container->get('app\models\CrossParent')->searchAll($mainParam);
        $crossParentsDbIds = isset($crossParents['data'][0]['crossParentDbId']) ? array_column($crossParents['data'],'crossParentDbId') : [];
                        
        //delete cross parents
        \Yii::$container->get('app\models\CrossParent')->deleteMany($crossParentsDbIds);
  
        //delete crosses
        Cross::deleteMany($selectedIds);
    }

    /** 
     * Create self crosses record in germplasm.cross_parent and germplasm.cross tables
     * @param Array $validList list of valid cross info
     * @param Integer $experimentDbId experiment identifier
     */
    public function createSelfCrosses($validList, $experimentDbId){
        $newCrossRecordCount = 0;
        $params = [
            'experimentDbId' => "equals $experimentDbId",
            'fields' => 'entry.id AS entryDbId|germplasm.designation|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|experiment.id AS experimentDbId|entry.entry_list_id AS entryListDbId',
        ];
        if(isset($validList)){
            $params['entryDbId'] = "equals ".implode("|equals ", $validList);
        }

        $entries =  \Yii::$container->get('app\models\Entry')->searchAll($params)['data'];            
        $crossMethodVarId = \Yii::$container->get('app\models\Variable')->searchAll(['abbrev' => 'CROSS_METHOD'])['data'][0]['variableDbId'];
        $crossMethod = \Yii::$container->get('app\models\ScaleValue')->getVariableScaleValues($crossMethodVarId, ['abbrev' => 'CROSS_METHOD_SELFING'])[0]['value'];
        foreach($entries as $entry){
            $crossName = $entry['designation'].'|'.$entry['designation'];
            $entryDbId = $entry['entryDbId'];
            //create record in cross table
            $insertCrossList[] = [
                "crossName" => "$crossName",
                "crossMethod" => "$crossMethod",
                "experimentDbId" => "$experimentDbId",
                "isMethodAutofilled" => true,
                "entryListDbId" => $entry['entryListDbId'].'',
            ];
            
            $crossParent[$crossName][] = [
                'germplasmDbId' => $entry['germplasmDbId'].'',
                'seedDbId' => $entry['seedDbId'].'',
                'parentRole' => 'female-and-male',
                'orderNumber' => '1',
                'experimentDbId' => "$experimentDbId",
                'entryDbId' => $entryDbId.'',
            ];
        }

        if(!empty($insertCrossList)){
            $newCrossRecords = Cross::create(['records' => $insertCrossList]);
            $newCrossRecordCount = $newCrossRecords['totalCount'];

            foreach($insertCrossList as $key => $value){
                $crossDbId = $newCrossRecords['data'][$key]['crossDbId'].'';
                $crossName = $value['crossName'];
                $crossParent[$crossName][0]['crossDbId'] = $crossDbId;
                $insertCrossParentList[] = $crossParent[$crossName][0];
            }

            \Yii::$container->get('app\models\CrossParent')->create(['records' => $insertCrossParentList]);
            //check if there's existing cross pattern data
            $variable = \Yii::$container->get('app\models\Variable')->getVariableByAbbrev('CROSSING_PATTERN');
            $sqlData = [
                'experimentDbId'=> "equals $experimentDbId",
                'variableDbId' => "equals ".$variable['variableDbId']
            ];
            $crossAttributes = \Yii::$container->get('app\models\CrossAttribute')->searchAll($sqlData); 

            if($crossAttributes['totalCount'] > 0){
                $crossPattern = isset($crossPattern) ? $crossPattern : null;
                \Yii::$container->get('app\modules\experimentCreation\models\CrossPatternModel')->saveCrossPatternRecords($experimentDbId, $newCrossRecords, $crossPattern, $insertCrossList, $crossAttributes);

            }
        }

        return $newCrossRecordCount;
    }

}
