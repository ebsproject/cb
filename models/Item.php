<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.item".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the item
 * @property string $name Name identifier of the item
 * @property int $type Type of authorization item 10: step; 20: task; 30: activity; 40: process; 50: application
 * @property string $description Description about the item
 * @property string $display_name Name of the item to show to users
 * @property string $remarks Additional details about the item
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $item_url Next url in the process path
 * @property int $subject_entity_id Subject entity of the process item
 * @property string $process_type Type of process: operational, data
 * @property string $item_status Determines whether a process item is active or inactive
 * @property string $item_class Class of the process item
 * @property int $place_id Place where the process path is conducted
 * @property int $service_id Service mapped to the process path item
 * @property string $service_action_type Type of the service action whether accept or submit
 * @property bool $is_active
 * @property string $item_icon Group of an item
 * @property double $item_group
 * @property int $application_id Identifier of the application
 * @property string $item_usage Where the item is used. If more than one usage, separate with semi-colon.
 * @property int $nuid Numerically unique identifier of the record
 * @property string $uuid Universally unique identifier of the record
 * @property string $item_qr_code QR code value of item
 */
class Item extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.item';
    }

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
      return 'items';
    }


    /**
     * Retrieves an existing item in the database
     * @return array item record in the database
     */
    public static function getItem($abbrev){
      $method = 'GET';
      $path = 'items'.'?abbrev='.$abbrev;

      $data = Yii::$app->api->getResponse($method, $path, true);
   
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        $data = $data['body']['result']['data'][0];
      }else{
        $data = [];
      }
      return $data;
    }

    /**
     * Search item records in the database
     * @param $params array condition parameters 
     * @param $filters string filter parameters 
     * @return array list of item records
     */
    public static function searchItemRecord($params, $filters=""){
      $method = 'POST';
      $path = 'items-search';

      $data = Yii::$app->api->getResponse($method, $path, json_encode($params), $filters);
     
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        $result = $data['body']['result']['data'][0];
        $success = true;
        $totalCount = $data['body']['metadata']['pagination']['totalCount'];
      }else{
        $result = [];
        $success = false;
        $totalCount = 0;
      }
      $message = $data['body']['metadata']['status'][0]['message'];
      return [
          'success' => $success,
          'message' => $message,
          'data' => $result,
          'totalCount' => $totalCount,
          'statusCode' => $data['status'] 
      ];
    }
    
}
