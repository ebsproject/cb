<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.item".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the item
 * @property string $name Name identifier of the item
 * @property int $type Type of authorization item 10: step; 20: task; 30: activity; 40: process; 50: application
 * @property string $description Description about the item
 * @property string $display_name Name of the item to show to users
 * @property string $remarks Additional details about the item
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $item_url Next url in the process path
 * @property int $subject_entity_id Subject entity of the process item
 * @property string $process_type Type of process: operational, data
 * @property string $item_status Determines whether a process item is active or inactive
 * @property string $item_class Class of the process item
 * @property int $place_id Place where the process path is conducted
 * @property int $service_id Service mapped to the process path item
 * @property string $service_action_type Type of the service action whether accept or submit
 * @property bool $is_active
 * @property string $item_icon Group of an item
 * @property double $item_group
 * @property int $application_id Identifier of the application
 * @property string $item_usage Where the item is used. If more than one usage, separate with semi-colon.
 * @property int $nuid Numerically unique identifier of the record
 * @property string $uuid Universally unique identifier of the record
 * @property string $item_qr_code QR code value of item
 */
class ItemBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abbrev', 'name', 'type'], 'required'],
            [['type', 'creator_id', 'modifier_id', 'subject_entity_id', 'place_id', 'service_id', 'application_id', 'nuid'], 'default', 'value' => null],
            [['type', 'creator_id', 'modifier_id', 'subject_entity_id', 'place_id', 'service_id', 'application_id', 'nuid'], 'integer'],
            [['description', 'remarks', 'notes', 'item_url', 'item_status', 'uuid', 'item_qr_code'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void', 'is_active'], 'boolean'],
            [['item_group'], 'number'],
            [['abbrev', 'item_usage'], 'string', 'max' => 128],
            [['name', 'display_name', 'item_class', 'service_action_type'], 'string', 'max' => 256],
            [['process_type'], 'string', 'max' => 32],
            [['item_icon'], 'string', 'max' => 64],
            [['abbrev'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'item_url' => 'Item Url',
            'subject_entity_id' => 'Subject Entity ID',
            'process_type' => 'Process Type',
            'item_status' => 'Item Status',
            'item_class' => 'Item Class',
            'place_id' => 'Place ID',
            'service_id' => 'Service ID',
            'service_action_type' => 'Service Action Type',
            'is_active' => 'Is Active',
            'item_icon' => 'Item Icon',
            'item_group' => 'Item Group',
            'application_id' => 'Application ID',
            'item_usage' => 'Item Usage',
            'nuid' => 'Nuid',
            'uuid' => 'Uuid',
            'item_qr_code' => 'Item Qr Code',
        ];
    }
}
