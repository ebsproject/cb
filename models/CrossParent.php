<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

/**
 * This is the model class for table "germplasm.cross_parent".
 *
 * @property int $id Cross Parent ID: Database identifier of the cross parent [CPARENT_ID]
 * @property int $cross_id Cross ID: Reference to the cross record [CPARENT_CROSS_ID]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm used as a parent in a cross [CPARENT_GERM_ID]
 * @property int|null $seed_id Seed ID: Reference to the seed of the germplasm that was used as parent in a cross [CPARENT_SEED_ID]
 * @property string $parent_role Parent Role: Role of the parent in the cross {female, male} [CPARENT_PARENTROLE]
 * @property int $order_number Order Number: Ordering of parents in the cross [CPARENT_ORDERNO]
 * @property int|null $experiment_id Experiment ID: Reference to the experiment where the parent is added [CPARENT_EXPT_ID]
 * @property int|null $entry_id Entry ID: Reference to the entry in the experiment where the parent is represented [CPARENT_ENTRY_ID]
 * @property int|null $plot_id Plot ID: Reference to the specific plot in the experiment where the parent will be taken [CPARENT_PLOT_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */

class CrossParent extends BaseModel {

    public static function apiEndPoint() {
        return 'cross-parents';
    }

}
