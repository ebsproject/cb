<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.plan_config".
 *
 * @property int $id Unique identifier of the record
 * @property string $abbrev Short name identifier or abbreviation of the record
 * @property string $name Unique name identifier of the record
 * @property array $config_value json configuration value
 * @property int $rank Order to be followed in implementing the rule for the application
 * @property string $usage Where the config is used
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 */
class PlanConfig extends PlanConfigBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.plan_config';
    }

    /**
     * Get plan configuration by abbrev
     *
     * @param $abbrev text plan configuration abbrev
     * @return $data array plan configuration records
     */
    public static function getConfigByAbbrev($abbrev){

      $responseApi = Yii::$app->api->getResponse('GET', 'plan-configurations?abbrev='.$abbrev);
      $result = [];

      // check if has result
      if (isset($responseApi['status']) && $responseApi['status'] == 200 && isset($responseApi['body']['result']['data'][0])){
        $result = $responseApi['body']['result']['data'][0];
      }
    
      return $result;
    }
}
