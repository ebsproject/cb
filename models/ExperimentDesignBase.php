<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.experiment_design".
 *
 * @property int $id Identifier of the record within the table
 * @property int $experiment_id Refers to the plot where the data is being attributed to
 * @property int $plot_id Refers to the plot where the experiment design value is being attributed to
 * @property string $design_type Type of the experimental design
 * @property string $design_value Value of the experimental design
 * @property int $design_level_number Level number of the design value
 * @property int $steward_id Steward user of the experimental design
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 * @property int $temp_plot_id
 */
class ExperimentDesignBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational.experiment_design';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['experiment_id', 'design_type', 'design_value', 'design_level_number', 'steward_id', 'creator_id'], 'required'],
            [['experiment_id', 'plot_id', 'design_level_number', 'steward_id', 'creator_id', 'modifier_id', 'temp_plot_id'], 'default', 'value' => null],
            [['experiment_id', 'plot_id', 'design_level_number', 'steward_id', 'creator_id', 'modifier_id', 'temp_plot_id'], 'integer'],
            [['design_type', 'design_value', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'experiment_id' => 'Experiment ID',
            'plot_id' => 'Plot ID',
            'design_type' => 'Design Type',
            'design_value' => 'Design Value',
            'design_level_number' => 'Design Level Number',
            'steward_id' => 'Steward ID',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
            'temp_plot_id' => 'Temp Plot ID',
        ];
    }
}
