<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use app\models\BaseModel;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "master.role".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property string $description
 * @property string $display_name
 * @property integer $rank
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property string $record_uuid
 * @property string $event_log
 *
 * @property MasterItemRole[] $masterItemRoles
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property MasterTeamMember[] $masterTeamMembers
 * @property MasterUserRole[] $masterUserRoles
 */
class Role extends RoleBase
{
    
}
