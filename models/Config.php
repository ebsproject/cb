<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

use app\interfaces\models\IConfig;

/**
 * This is the model class for table "platform.config".
 */
class Config extends BaseModel implements IConfig
{

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'configurations';
    }

    /**
     * Get configurations by abbrev
     *
     * @param string $abbrev config abbrev
     * @return array data config value
     */
    public function getConfigByAbbrev($abbrev){

        // check if config is already in session
        if(Yii::$app->session->get('config-'.$abbrev) != null){
            return Yii::$app->session->get('config-'.$abbrev);
        }

        $data = Yii::$app->api->getResponse('GET',"configurations?abbrev=$abbrev", null, 'limit=1', false);
        $configValue = [];

        // check if has result
        if(isset($data['body']['result']['data'][0]['configValue'])){
            $configValue = $data['body']['result']['data'][0]['configValue'];
        }

        // set config value to session
        Yii::$app->session->set('config-'.$abbrev, $configValue);

        return $configValue;
    }
    
    /**
     * Retrieve configuration of form
     * by configuration abbreviation and step
     *
     * @param Text $abbrev Abbreviation of configuration
     * @param Text $step Step to be retrieved
     *
     * @return Array $fields Fields configuration 
     */
    public function getFormConfig($abbrev, $step=null){
        $fields = [];
        $params = 'abbrev='.$abbrev;
        $configData = $this->getAll($params, false);

        if(isset($configData['data'][0]['configValue'])){

            $value = $configData['data'][0]['configValue'];

            if(!empty($step)){
                $value = $value[$step];
            }

            $fields = isset($value['fields']) ? $value['fields'] : $value;

        }

        return $fields;
    }
}
