<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table ".method".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the method
 * @property string $name Name identifier of the method
 * @property string $description Description on how the method should be performed
 * @property string $display_name Name of the method to show to users
 * @property string $formula Formula needed to compute for a property
 * @property string $remarks Additional details
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $ontology_id Crop ontology ID
 * @property int $property_id ID of the property where the method is used
 * @property string $bibliographical_reference Reference details about the method
 * @property int $formula_id
 *
 * @property Formula[] $Formulas
 * @property Formula $formula0
 * @property User $creator
 * @property User $modifier
 * @property PropertyMethodScale[] $PropertyMethodScales
 * @property Variable[] $Variables
 */
class Method extends MethodBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'formula', 'remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['creator_id', 'modifier_id', 'property_id', 'formula_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id', 'property_id', 'formula_id'], 'integer'],
            [['is_void'], 'boolean'],
            [['abbrev'], 'string', 'max' => 128],
            [['display_name', 'ontology_id'], 'string', 'max' => 256],
            [['bibliographical_reference'], 'string', 'max' => 255],
            [['formula_id'], 'exist', 'skipOnError' => true, 'targetClass' => Formula::className(), 'targetAttribute' => ['formula_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'formula' => 'Formula',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'ontology_id' => 'Ontology ID',
            'property_id' => 'Property ID',
            'bibliographical_reference' => 'Bibliographical Reference',
            'formula_id' => 'Formula ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormulas()
    {
        return $this->hasMany(Formula::className(), ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormula0()
    {
        return $this->hasOne(Formula::className(), ['id' => 'formula_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        return $this->hasOne(User::className(), ['id' => 'modifier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyMethodScales()
    {
        return $this->hasMany(PropertyMethodScale::className(), ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariables()
    {
        return $this->hasMany(Variable::className(), ['method_id' => 'id']);
    }
}
