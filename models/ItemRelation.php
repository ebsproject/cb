<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.item_relation".
 *
 * @property int $id Primary key of the record in the table
 * @property int $parent_id Parent item which has one or more child items
 * @property int $child_id Child item of the parent item
 * @property int $order_number Order of the child in the list of children of the parent item
 * @property string $remarks Additional details
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $position
 * @property string $tooltip
 * @property string $url
 * @property string $icon
 * @property int $visible
 * @property string $task
 * @property string $options
 * @property string $title
 * @property int $root_id
 * @property string $itemrel_url
 */
class ItemRelation extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.item_relation';
    }

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'item-relations';
    }

    /**
     * Retrieves an existing item relations in the database
     * @param $param array condition parameters 
     * @return array item relations record in the database
     */
    public static function getItemRelations($param){
        $method = 'POST';
        $path = 'item-relations-search?sort=orderNo';
        
        $data = Yii::$app->api->getResponse($method, $path, json_encode($param), true);
        $result = [];

        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
          }
        return $result;
    }

}
