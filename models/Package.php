<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

class Package extends BaseModel {

    public static function apiEndPoint() {
        return 'packages';
    }

    /**
     * Retrieve package information in view package
     *
     * @param $id integer package identifier
     * @return $data array package information
     */
    public static function getPackageInfo($id){
        $basic = null;
        $packageLogs = [];

        // basic
        $packageData = Yii::$app->api->getResponse('GET', 'packages/'.$id);
        if(isset($packageData['status']) && $packageData['status'] == 200 && isset($packageData['body']['result']['data'][0])) {
            $data = $packageData['body']['result']['data'][0];
            $basicData = [
                'Seed Manager' => $data['seedManager'],
                'Seed Name' => $data['seedName'],
                'GID' => $data['GID'],
                'Designation' => $data['designation'],
                'Harvest Date' => $data['harvestDate'],
                'Package Code' => $data['packageCode'],
                'Label' => $data['label'],
                'Quantity' => round($data['quantity'], 2).' '.$data['unit'],
            ];
            $sourceData = [
                'Source Entry' => $data['sourceEntryCode'],
                'Source Entry Number' => $data['seedSourceEntryNumber'],
                'Source Plot' => $data['seedSourcePlotCode'],
                'Source Season' => $data['sourceStudySeason'],
            ];
            $basic = [
                'basic' => $basicData,
                'source' => $sourceData
            ];
        }

        // package logs
        $packageLogsData = Yii::$app->api->getResponse('GET', 'packages/'.$id.'/package-logs');
        if(isset($packageLogsData['status']) && $packageLogsData['status'] == 200 && isset($packageLogsData['body']['result']['data'])) {
            $data = $packageLogsData['body']['result']['data'];
            $packageLogs = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false
            ]);
        }

        return [
            'basic' => $basic,
            'package-logs' => $packageLogs,
        ];
    }
}
