<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.seed_storage_log".
 *
 * @property int $id Primary key of the record in the table
 * @property int $seed_storage_id Seed storage of the product
 * @property int $encoder_id User who entered in the record
 * @property string $encode_timestamp Timestamp when the transaction was performed
 * @property string $transaction_type What was done to the seed storage record: deposit or withdraw
 * @property double $volume Volume deposited to or withdrawn from the seed storage record
 * @property string $unit Scale used to measure what volume was deposited or withdrawn
 * @property string $event_timestamp Timestamp when the transaction was needed
 * @property string $sender Where the deposited seeds come from
 * @property string $receiver Where the withdrawn seeds will go to
 * @property string $remarks Additional details about the seed storage log
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $seed_lot_event_id
 */
class SeedStorageLog extends SeedStorageLogBase
{
    
}
