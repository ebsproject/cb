<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.crossing_list".
 *
 * @property int $id Identifier of the record within the table
 * @property int $experiment_id Experiment containing the cross (reference ID to operational.experiment)
 * @property int $product_id Product of the cross (reference ID to master.product)
 * @property int $female_entry_id Entry used as female in the cross (reference ID to operational.entry_list)
 * @property int $male_entry_id Entry used as male in the cross (reference ID to operational.entry_list)
 * @property int $crossing_method_id Crossing method used in the crossing of the two parents (reference ID to master.scale_value, where variable = CROSS_METHOD)
 * @property int $crossno Sequence number of the cross in the crossing list
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class CrossingListBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational.crossing_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['experiment_id', 'female_entry_id', 'male_entry_id', 'crossno', 'creator_id'], 'required'],
            [['experiment_id', 'product_id', 'female_entry_id', 'male_entry_id', 'crossing_method_id', 'crossno', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['experiment_id', 'product_id', 'female_entry_id', 'male_entry_id', 'crossing_method_id', 'crossno', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void','is_method_autofilled'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'experiment_id' => 'Experiment ID',
            'product_id' => 'Product ID',
            'female_entry_id' => 'Female Entry ID',
            'male_entry_id' => 'Male Entry ID',
            'crossing_method_id' => 'Crossing Method ID',
            'crossno' => 'Crossno',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'is_method_autofilled' => 'Is Method Auto Filled',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
