<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */ 

namespace app\models;

use Yii;
use app\models\Study;
use ChromePhp;

/**
 * This is the model class for table "seed_warehouse.seed_list".
 *
 * @property int $id Identifier of the record within the table
 * @property int $program_id Program of the seed list
 * @property int $year Year when the seed list was created
 * @property int $seed_list_number Sequence number of the seed list within the same program, year, and season
 * @property string $seed_list_name Auto-generated name of the seed list with format SL-<program abbrev>-<year>-<seed list number>
 * @property string $seed_list User-defined name of the seed list with default format SL<2-digit year><program letter code><2-digit seed list number>
 * @property string $seed_list_title Title of the seed list
 * @property int $seed_list_item_count Number of seed lots in the seed list
 * @property string $seed_list_status Status of seed list: DRAFT, COMPLETED
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log
 */
class SeedList extends SeedListBase
{
    /** 
     * Get source model for the data
     * 
     * @param $id record id of the study
     * @return array
     */
	public static function getSourceModel($id) {
		if (!empty($id)) {
			$study = Study::findOne($id);

			// for temporary entries
			if (!empty($study)) {
				if ($study->hasStudyStatus('committed study')) {
					return array(
						'entry'=>'entry',
						'plot' => 'plot',
						'grid' => 'plot',
					);
				}

				if ($study->hasStudyStatus('created')) {
					return array(
						'entry' => 'temp_entry',
						'plot' => 'temp_plot',
						'grid' => 'temp_plot',
					);
				}
			}
		}


		// default
		return array(
			'entry'=>'entry',
			'plot' => 'plot',
			'grid' => 'plot',
		);
	}

	/*
	* Function for getting the data to be exported
	* @param $id - id of the study/studies
	*
	*/
	public static function getDataArray($id, $type){

		$trialArr = array('ayt','iyt','met0','met1','met2','oyt','pyt','ryt','agr');
		$repArr = [];

		$sourceModels = SeedList::getSourceModel($id);
		$sourceEntry = $sourceModels['entry'];
		$sourcePlot = $sourceModels['plot'];

		if(in_array($type, $trialArr)){
			$repSql = "
				select distinct rep from operational.{$sourcePlot} p, operational.{$sourceEntry} e
				where e.study_id = {$id} and p.entry_id = e.id and e.is_void = false and p.is_void = false
				order by rep
			";

			$repArr = Yii::$app->db->createCommand($repSql)->queryAll();
		}

		$metaStr = "";

		$varSql = "
		select
			id,
			abbrev,
			label
		from
			master.variable v
		where
			v.abbrev in ('SOURCE_STUDY', 'SOURCE_ENTRY', 'SOURCE_PLOT_NO')
			and v.is_void = false
		";
		$result = Yii::$app->db->createCommand($varSql)->queryAll();

		foreach($result as $res){
			if($metaStr != ''){
				$metaStr = $metaStr . ',';
			}

			$abbrev = strtolower($res['abbrev']);
			$metaStr = $metaStr . ' (select ssm.value from operational.seed_storage_metadata ssm where ssm.seed_storage_id = ss.id and ssm.variable_id = ' . $res['id'] . ' and is_void = false limit 1) as ' . $abbrev;
		}

		if(!in_array($type, $trialArr)){
		$sql = "
			select
				(case when (select value from operational.study_metadata sm where sm.study_id = s.id and sm.variable_id = 484 and sm.is_void = false limit 1) is null
				then s.name else (select value from operational.study_metadata sm where sm.study_id = s.id and sm.variable_id = 484 and sm.is_void = false limit 1) end) as trial,
				e.product_name as designation,
				e.entno as entry,
				{$metaStr},
				'' as plant_source,
				p.plotno as plot,
				p.code as plot_code,
				ssl.volume as reserved_volume,
				ssl.unit
			from
				operational.seed_storage_log ssl,
				operational.seed_storage ss,
				operational.study s,
				operational.{$sourceEntry} e
			left join
				operational.{$sourcePlot} p
			on
				e.id = p.entry_id
				and p.is_void = false
			where
				s.id = {$id}
				and e.study_id = s.id
				and e.is_void = false
				and ssl.id = e.seed_storage_log_id
				and ssl.is_void = false
				and ss.id = ssl.seed_storage_id
				and ss.is_void = false
			order 
				by e.entno
			";
		}
		else{
			$repStr = '';
			$plotCodeStr = '';
			foreach($repArr as $rep){
				if($repStr != ''){
					$repStr = $repStr . ',';
				}
				if($plotCodeStr != ''){
					$plotCodeStr = $plotCodeStr . ',';
				}
				$rep = $rep['rep'];
				$repStr = $repStr . ' (select 
				p.plotno from operational.'.$sourcePlot.' p where p.entry_id = e.id and p.is_void = false
				and p.rep = '.$rep.' limit 1) as rep' . $rep;

				$plotCodeStr = $plotCodeStr . ' (select
				p.code from operational.'.$sourcePlot.' p where p.entry_id = e.id and p.is_void = false
				and p.rep = '.$rep.' limit 1) as plot_code' . $rep;
			}

			$sql = "
				select
					(case when (select value from operational.study_metadata sm where sm.study_id = s.id and sm.variable_id = 484 and sm.is_void = false limit 1) is null
					then s.name else (select value from operational.study_metadata sm where sm.study_id = s.id and sm.variable_id = 484 and sm.is_void = false limit 1) end) as trial,
					e.entno,
					e.product_name as designation,
					ss.label as seed_source,
					ssl.volume as reserved_volume,
					ssl.unit,
					{$metaStr},
					{$repStr},
					{$plotCodeStr}
				from
					operational.study s,
					operational.{$sourceEntry} e,
					operational.seed_storage_log ssl,
					operational.seed_storage ss
				where
					s.id = {$id}
					and e.study_id = s.id
					and e.is_void = false
					and ssl.id = e.seed_storage_log_id
					and ssl.is_void = false
					and ss.id = ssl.seed_storage_id
					and ss.is_void = false
				order by e.entno
			";
		}

	$dataArray = Yii::$app->db->createCommand($sql)->queryAll();

	if(!empty($dataArray) && $dataArray != NULL){

		$columns = SeedList::getCols($type,$repArr);
		$headers = array_keys($dataArray[0]);

		$columnSort = [];
		foreach($headers as $col){
			$index = array_search($col, array_column($columns, 'name'));
			$columnSort[] = $columns[$index]['header']; 
		}

		return [
			'data' => $dataArray,
			'columns' => $columnSort,
		];
	} else return NULL;

	}
    
	/**
	* Get columns for the seed list export
	* 
	* @param string $type phase of the study
	* @param array $repArr list of reps
	* 
	* @return array
	*/
	public static function getCols($type, $repArr){
	$trialArr = array('ayt','iyt','met0','met1','met2','oyt','pyt','ryt','agr');
		$basic = array(
			array(
				'name' => 'trial',
				'header' => 'TRIAL'
			)
		);
		if($type == 'pn'){
			$addl = array(
				array(
					'name' => 'plot',
					'header' => 'PLOT'
				),
				array(
					'name' => 'plot_code',
					'header' => 'PLOT CODE'
				),
				array(
					'name' => 'designation',
					'header' => 'DESIGNATION'
				),
				array(
					'name' => 'source_study',
					'header' => 'TRIAL SOURCE'
				),
				array(
					'name' => 'source_plot_no',
					'header' => 'PLOT SOURCE'
				),
				array(
					'name' => 'plant_source',
					'header' => 'PLANT SOURCE'
				),
				array(
					'name' => 'reserved_volume',
					'header' => 'RESERVED VOLUME'
				),
				array(
					'name' => 'unit',
					'header' => 'UNIT'
				),
			);
		}
		else if(in_array($type, $trialArr)){
			$addl = array(
				array(
					'name' => 'entno',
					'header' => 'ENTNO'
				),
				array(
					'name' => 'designation',
					'header' => 'DESIGNATION'
				),
				array(
					'name' => 'source_study',
					'header' => 'TRIAL SOURCE'
				),
				array(
					'name' => 'seed_source',
					'header' => 'SEED_SOURCE'
				),
				array(
					'name' => 'source_entry',
					'header' => 'ENTRY SOURCE'
				),
				array(
					'name' => 'source_plot_no',
					'header' => 'PLOT SOURCE -REP1'
				),
				array(
					'name' => 'reserved_volume',
					'header' => 'RESERVED VOLUME'
				),
				array(
					'name' => 'unit',
					'header' => 'UNIT'
				),
			);

			foreach($repArr as $rep){
				$rep = $rep['rep'];
				$addl[] = array(
					'name' => 'rep' . $rep,
					'header' => 'PLOT- REP' . $rep
				);
				$addl[] = array(
					'name' => 'plot_code' . $rep,
					'header' => 'PLOT CODE- REP' . $rep
				);
			}
		}
		else{
			$addl = array(
				array(
					'name' => 'entry',
					'header' => 'ENTRY'
				),
				array(
					'name' => 'designation',
					'header' => 'DESIGNATION',
				),
				array(
					'name' => 'source_study',
					'header' => 'TRIAL SOURCE'
				),
				array(
					'name' => 'source_entry',
					'header' => 'ENTRY SOURCE'
				),
				array(
					'name' => 'source_plot_no',
					'header' => 'PLOT SOURCE'
				),
				array(
					'name' => 'plant_source',
					'header' => 'PLANT SOURCE'
				),
				array(
					'name' => 'plot',
					'header' => 'FINAL PLOT'
				),
				array(
					'name' => 'plot_code',
					'header' => 'PLOT CODE'
				),
				array(
					'name' => 'reserved_volume',
					'header' => 'RESERVED VOLUME'
				),
				array(
					'name' => 'unit',
					'header' => 'UNIT'
				),
			);
		}
		return array_merge($basic, $addl);

	}
}
