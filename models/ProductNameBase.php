<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.product_name".
 *
 * @property int $id Primary key of the record in the table
 * @property int $product_id Product given with a particular name
 * @property string $name_type Type of product name: derivative, fixed_line, cultivar, common, others
 * @property string $value Value of the product name
 * @property string $language_code Language of the given name to the product (refer to: http://www.loc.gov/standards/iso639-2/php/code_list.php)
 * @property string $description Description about the product name (needs clarification)
 * @property string $remarks Additional details
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $product_gid_id GID of the product where the name is associated with
 * @property string $system_product_name Standardized name of product
 * @property array $event_log
 */
class ProductNameBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.product_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'name_type', 'value'], 'required'],
            [['product_id', 'creator_id', 'modifier_id', 'product_gid_id'], 'default', 'value' => null],
            [['product_id', 'creator_id', 'modifier_id', 'product_gid_id'], 'integer'],
            [['name_type', 'value', 'language_code', 'description', 'remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['system_product_name'], 'string', 'max' => 256],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProduct::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_gid_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterProductGid::className(), 'targetAttribute' => ['product_gid_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name_type' => 'Name Type',
            'value' => 'Value',
            'language_code' => 'Language Code',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'product_gid_id' => 'Product Gid ID',
            'system_product_name' => 'System Product Name',
            'event_log' => 'Event Log',
        ];
    }
}