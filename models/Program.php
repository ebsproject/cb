<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\BaseModel;
use app\interfaces\models\IProgram;
use app\interfaces\models\ICropProgram;

use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "master.program".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property integer $pipeline_id
 * @property string $description
 * @property string $display_name
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property string $type
 * @property integer $level_type
 * @property string $program_status
 * @property boolean $is_active
 * @property string $record_uuid
 * @property string $event_log
 *
 * @property MasterItemServiceTeam[] $masterItemServiceTeams
 * @property MasterProduct[] $masterProducts
 * @property MasterPipeline $pipeline
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property MasterProgramMetadata[] $masterProgramMetadatas
 * @property MasterProgramPlace[] $masterProgramPlaces
 * @property MasterProgramRelation[] $masterProgramRelations
 * @property MasterProgramRelation[] $masterProgramRelations0
 * @property MasterProgramRole[] $masterProgramRoles
 * @property MasterProgramTeam[] $masterProgramTeams
 * @property MasterProgramTvp[] $masterProgramTvps
 * @property MasterProgramVariableSet[] $masterProgramVariableSets
 * @property MasterProgramYearSequence[] $masterProgramYearSequences
 * @property MasterService[] $masterServices
 * @property OperationalCrossList[] $operationalCrossLists
 * @property OperationalSeedStorage[] $operationalSeedStorages
 * @property OperationalStudy[] $operationalStudies
 * @property OperationalStudyGroup[] $operationalStudyGroups
 * @property OperationalDataTerminalTransactionLogSeedStorage[] $operationalDataTerminalTransactionLogSeedStorages
 * @property PlatformMailMessage[] $platformMailMessages
 * @property PlatformMailMessage[] $platformMailMessages0
 * @property PlatformMailThread[] $platformMailThreads
 * @property PlatformMailThread[] $platformMailThreads0
 * @property PlatformMenuItem[] $platformMenuItems
 * @property SeedWarehouseRepackingTransaction[] $seedWarehouseRepackingTransactions
 * @property SeedWarehouseSeedList[] $seedWarehouseSeedLists
 * @property SeedWarehouseSeedTransferRequest[] $seedWarehouseSeedTransferRequests
 * @property SeedWarehouseShipment[] $seedWarehouseShipments
 * @property SeedWarehouseShipment[] $seedWarehouseShipments0
 * @property SeedWarehouseShipment[] $seedWarehouseShipments1
 * @property WarehouseStudy[] $warehouseStudies
 */
class Program extends BaseModel implements IProgram
{

    public function __construct(public ICropProgram $cropProgram)
    {
        
    }

    public static function apiEndPoint() {
        return 'programs';
    }
    /**
     * Retrieves existing programs in the database and return an array with the ID
     * as the key and program name as the value.
     * 
     * @return array collection of programs in the database
     * 
     */
    public static function getPrograms(){

        $models = Program::find()->where(['is_void'=>false])->orderBy('id ASC')->all();

        return ArrayHelper::map($models, 'id', 'name');

    }

    public static function getProgramIdByAbbrev($abbrev){
        $programRec = Program::find()->where(['abbrev'=>$abbrev])->one();
        return $programRec->id;
    }

    /**
     * Get program name and abbrev by program id
     * @param $id integer program identifier
     * @return $program text program name and abbrev
     */
    public function getProgramNameById($id){
        $program = '';
        $sql = "select
                name || ' (' || abbrev || ')' as program
            from
                master.program
            where 
                id = {$id}";

        $result = Yii::$app->db->CreateCommand($sql)->queryColumn();

        if(!empty($result) && isset($result[0])){
            $program = $result[0];
        }

        return $program;
    }

    /**
     * Get program attribute
     *
     * @param $condAttr text attribute in where condition
     * @param $condVal text program identifier
     * @param $attr text attribute to be retrieved
     *
     * @return $program text program attribute
     */
    public function getProgramAttr($condAttr = 'id', $condVal = '', $attr = 'abbrev'){
        $program = '';

        if($condAttr == 'abbrev' || $condAttr == 'program_code'){
            $condAttr = 'programCode';
        }else if($condAttr == 'id'){
            $condAttr = 'programDbId';
        }

        if($attr == 'abbrev' || $attr == 'program_code'){
            $attr = 'programCode';
        }else if($attr == 'id'){
            $attr = 'programDbId';
        }

        $params[$condAttr] = (string) $condVal;
        $programObj = Program::searchAll($params, 'limit=1', false);

        // check if there is a result
        if(isset($programObj['data'][0][$attr])){
            $program = $programObj['data'][0][$attr];
        }

        return $program;
    }

    /**
     * Get configurations by program code
     *
     * @param $programCode text program code
     * @return $data array program value
     */
    public function getProgramByProgramCode ($abbrev)
    {
      $method = 'GET';
      $path = "programs?programCode=$abbrev";
      $data = Yii::$app->api->getResponse($method, $path, true);
      $result = [];
      // check if has result
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        $result = $data['body']['result']['data'][0];
      }

      return $result;
    }

     /**
     * Get configurations by abbrev
     *
     * @param $id integer program id
     * @return $data array program record
     */
    public function getProgram($id){
        $method = 'GET';
        $path = 'programs'.'/'.$id;
  
        $data = Yii::$app->api->getResponse($method, $path, true);
        $result = [];
  
        // check if has result
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
          $result = $data['body']['result']['data'][0];
        }
  
        return $result;
    }

    /**
     * Retrieves the crop code of the given program code
     * @param String programCode program code value
     * @return String crop code value
     */
    public function getCropCode($programCode) {
        // Retrieve crop program id from the program record
        $result = $this->searchAll(
            [
                "fields" => "program.program_code AS programCode|program.crop_program_id AS cropProgramDbId",
                "programCode" => "equals $programCode"
            ],
            "limit=1"
        );
        $data =  $result['data'] ?? [];
        $programInfo = $data[0] ?? [];
        $cropProgramDbId = $programInfo['cropProgramDbId'] ?? 0;

        // Retrieve crop code from the crop program record
        $result = $this->cropProgram->searchAll(
            [
                "fields" => "cropProgram.id AS cropProgramDbId|crop.crop_code AS cropCode",
                "cropProgramDbId" => "equals $cropProgramDbId"
            ],
            "limit=1"
        );
        $data =  $result['data'] ?? [];
        $cropProgramInfo = $data[0] ?? [];
        return $cropProgramInfo['cropCode'] ?? '';
    }
}
