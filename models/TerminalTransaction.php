<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

 namespace app\models;
 use Yii;
 use app\dataproviders\ArrayDataProvider;
 use app\interfaces\models\ITerminalTransaction;

 use app\models\UserDashboardConfig;

 /**
  * Model Class for terminal transactions
  */

 class TerminalTransaction extends BaseModel implements ITerminalTransaction {

    public $transactionDbId;
    public $status;
    public $locationName;
    public $action;
    public $programCode;
    public $creator;
    public $creationTimestamp;
    public $committer;
    public $committedTimestamp;
    public $occurrence;
    public $remarks;
    public $modifier;
    public $modificationTimestamp;

    public static function apiEndPoint() {
        return 'terminal-transactions';
    }

    public function rules() {
        return [
            [['status', 'occurrence', 'locationName', 'action', 'programCode', 'creator', 'creationTimestamp', 'committer', 'committedTimestamp', 'remarks', 'modifier', 'modificationTimestamp'],'safe']

        ];
    }

    /**
     * Search functionality for data browser
     * 
     * @param Array $params contains the filter parameters
     * @return ArrayDataProvider the data provider for the browser
     */
    public function search($params=null) {
        
        $this->load($params);
        

        //build filters
        if (isset($params['TerminalTransaction'])) {
            $filters = [];
            foreach ($params['TerminalTransaction'] as $key => $value) {
                if (isset($value) && !empty($value)) {
                    if ($key == 'status') {
                        if (strtolower($value) === 'open') {
                            $filters[$key] = "not ilike committed|error in background process";
                        } elseif (strtolower($value) === 'in progress') {
                            $filters[$key] = "not ilike committed|uploaded|in queue";
                        } else {
                            $filters[$key] = $value;
                        }
                    } elseif ($key == 'occurrence') {
                        $filters['occurrenceName'] = "%".$value."%";
                    } elseif (strpos($key, 'DbId')) {
                        $filters[$key] = $value;
                    } else {
                        $filters[$key] = "%".$value."%";
                    }
                }
            }
        }
        $paramSort = '';
        if(isset($_GET['sort'])) {
            $paramSort = '&sort=';
            $sortParams = $_GET['sort'];
            $sortParams = explode('|', $sortParams);
            $countParam = count($sortParams);
            $currParam = 1;

            $sortOrder = array();

            foreach($sortParams as $column) {
                if($column[0] == '-') {
                    $sortOrder[substr($column, 1)] = SORT_DESC;
                    $paramSort = $paramSort . substr($column, 1) . ':desc';
                } else {
                    $sortOrder[$column] = SORT_ASC;
                    $paramSort = $paramSort . $column;
                }
                if($currParam < $countParam) {
                    $paramSort = $paramSort . '|';
                }
                $currParam += 1;
            }
            
        }
        $paramPage = '';
        if (isset($_GET['dp-1-page'])){
            $currentPage = $_GET['dp-1-page'];
            $paramPage = "&page=$currentPage";
        } else if (isset($_GET['page'])) {
            $currentPage = $_GET['page'];
            $paramPage = "&page=$currentPage";
        }

        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $filters["action"] = !isset($filters["action"]) || empty($filters["action"]) ? "equals file upload|equals harvest manager|equals field book|equals post harvest" : $filters["action"];
        
        // if non-admin user, display owned and shared transactions
        $program = Yii::$app->userprogram->get('abbrev');
        $personModel = Yii::$container->get('app\models\Person');
        [
            $role,
            $isAdmin,
        ] = $personModel->getPersonRoleAndType($program);

        if (!$isAdmin && isset($filters['programDbId'])){
            unset($filters['programDbId']);
        }

        $totalPages = $this->searchAll($filters, $paramLimit, false)['totalPages'];
        if(!empty($paramPage) && $currentPage > $totalPages){
            $paramPage = '';
        }
        
        $response = Yii::$app->api->getResponse('POST', 'terminal-transactions-search?'.$paramLimit.$paramPage.$paramSort, json_encode($filters));

        $output = $response;
        $data = $output['body']['result']['data'];
        $sort = [
          'attributes'=>['locationName', 'action', 'programCode', 'creator', 'creationTimestamp','committer', 'committedTimestamp', 'remarks', 'modifier', 'modificationTimestamp']
        ];

        $options = [
          'key' => 'transactionDbId',
          'allModels' => $data,
          'sort' => $sort,
          'restified' => true,
          'totalCount' => $output['body']['metadata']['pagination']['totalCount'],
          'pagination' => ['pageSize' => $defaultPageSize]
        ];

        return new ArrayDataProvider($options);

    }

    /**
     * Delete a transaction given ID
     * @param Integer $transactionDbId ID of the transaction record
     * @return Array API response
     */
    public function delete($transactionDbId=null) {
        if (empty($transactionDbId) && !empty($this->transactionDbId)) {
            $transactionDbId = $this->transactionDbId;
        }
        else { 
            $transactionDbId = 0;
        }

        $param = '';
        $url = 'terminal-transactions/'.$transactionDbId;
        return Yii::$app->api->getResponse('DELETE', $url, json_encode($param));


    }

    /**
     * Retrieves the available files given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @return Array contains the file records and total count
     */
    public function getFiles($transactionDbId=null) {
        if (empty($transactionDbId) && !empty($this->transactionDbId)) {
            $transactionDbId = $this->transactionDbId;
        }
        else { 
            $transactionDbId = 0;
        }

        $param = '';
        $url = 'terminal-transactions/'.$transactionDbId.'/files';

        $response = Yii::$app->api->getResponse('GET', $url, json_encode($param),'', true);
        if ($response['status'] == 200) {
            $data = $response['body'];
            $data = (isset($data['result']['data'])) ? $data['result']['data'] : [];
            $totalCount = (isset($data['metadata']['pagination']['totalCount'])) ? $data['metadata']['pagination']['totalCount'] : null;
        } else {
            $data = [];
            $totalCount = 0;
        }
        
        return [
            'data' => $data,
            'totalCount' => $totalCount
        ];
    }
    
    /**
     * Retrieves the most recent file given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @return Array contains the file records and total count
     */
    public function getRecentFileId($transactionDbId=null) {

        $param = '';
        $url = 'terminal-transactions/'.$transactionDbId.'/files?sort=transactionFileDbId:desc&limit=1';

        $response = Yii::$app->api->getResponse('GET', $url, json_encode($param),'', true);

        if ($response['status'] == 200) {
            $data = $response['body'];
            $id = (isset($data['result']['data'][0]['transactionFileDbId'])) ? $data['result']['data'][0]['transactionFileDbId'] : $transactionDbId;
            
        } else {
            $id = $transactionDbId;
        }

        return $id;
    }

    /**
     * Retrieves the dataset summary given transaction ID
     * @param Integer $transactionDbId ID of the transaction
     * @param Boolean $retrieveOccurrences return occurrence list or not
     * @return Array contains the variable records and basic statistics
     */
    public function getDatasetSummary($transactionDbId, $retrieveOccurrences = true) {

        $path = "terminal-transactions/$transactionDbId/dataset-summary?retrieveOccurrences=$retrieveOccurrences";
        $method = 'GET';
        return Yii::$app->api->getParsedResponse($method, $path, null, '', true);


    }

    /**
     * Method for retrieving all the dataset records in table format from a given transactionDbId using the POST search method
     * @param Integer $transactionDbId ID of the transaction
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllDatasetsTable($transactionDbId, $params=null, $filters='') {

        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionDbId.'/datasets-table-search';
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, true);


    }

    /**
     * Method for retrieving all the dataset records from a given transactionDbId using the POST search method
     * @param Integer $transactionDbId ID of the transaction
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @param Boolean retrieveAll true if to retrieve all record, false = only the default or specified limit of records
     * @return Array the records from the database
     */
    public function searchAllDatasets($transactionDbId, $params=null, $filters='', $retrieveAll = true) {

        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionDbId.'/datasets-search';
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($params), $filters, $retrieveAll);


    }

    /**
     * Method for creating one or more file records in the transaction
     * @param Integer $transactionDbId ID of the transaction
     * @param Array $requestData contains the information about the records
     * @return Array API response
     */
    public function createFiles($transactionDbId, $requestData) {

        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionDbId.'/files';
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($requestData));


    }

    /**
     * Method for computation of traits
     * @param Integer $transactionDbId ID of the transaction
     * @param Array $requestData contains the information about the records
     * @return Array API response
     */
    public function variableComputations($transactionDbId, $requestData) {

        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionDbId.'/variable-computations';
        return Yii::$app->api->getParsedResponse($method, $path, json_encode($requestData));


    }

    /**
     * Create a new dataset record for a specific and existing transaction record given a transaction ID
     *
     * @param Integer transactionId transaction identifier
     * @param Array requestBody API request body
     * @return Array API response
     */
    public function createDataset($transactionId, $requestBody)
    {
        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionId.'/dataset-table';
        $rawData = json_encode($requestBody);

        return Yii::$app->api->getParsedResponse($method, $path, $rawData);
    }

    /**
     * Validate the dataset of a specific and existing transaction record given a transaction ID
     *
     * @param Integer transactionId transaction identifier
     * @return Array API response
     */
    public function validateDataset($transactionId)
    {
        $method = 'POST';
        $path = 'terminal-transactions/'.$transactionId.'/validation-requests';

        return Yii::$app->api->getResponse($method, $path);
    }
}