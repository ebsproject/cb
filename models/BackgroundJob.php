<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;
use app\interfaces\models\IBackgroundJob;
use app\interfaces\models\IEntryList;
use app\interfaces\models\IExperiment;
use app\interfaces\models\IOccurrence;
use app\interfaces\models\IGermplasmFileUpload;
use app\interfaces\models\ILists;

/**
 * This is the model class for table "api.background_job".
 */
class BackgroundJob extends BaseModel implements IBackgroundJob {

    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'background-jobs';
    }

    public function __construct (
        public IExperiment $experiment,
        public IOccurrence $occurrence,
        public IGermplasmFileUpload $germplasmFileUpload,
        public IEntryList $entryList,
        public ILists $lists,
    )
    { }

    /**
     * Update background process records
     * @param array $records List of records of backgroundProcessDbId to be updated
     * @param array $params List of fields to be updated
     * @return array results success or error
     */
    public function update($records, $params)
    {
        // update each background process records to isSeen=true
        foreach ($records as $record) {

            $url = 'background-jobs/' . $record["backgroundJobDbId"];
       
            $data = Yii::$app->api->getResponse('PUT', $url, json_encode($params));
            
            if ($data["status"] != 200) {
                return [
                    "success" => false,
                    "error" => "Error while updating background process record. Kindly contact Administrator."
                ];
            }
        }

        return [
            "success" => true,
        ];
    }

    /**
     * Format the background job records into dropdown selections (generic version)
     * @param array backgroundJobs Array of background job records
     * @return string formatted HTML elements
     */
    public function getGenericNotifications($backgroundJobs) {
        $formatted = [];
        foreach ($backgroundJobs as $record) {
            // Set worker name data
            $workerNameData = "data-worker-name='" . $record["workerName"] . "'";
            if ($record["jobStatus"] == "DONE") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . "' data-process-id='" . $record["backgroundJobDbId"] . "' " . $workerNameData . ">
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle green small' >check</span> ". $record["message"] ."</a>";
            } else if ($record["jobStatus"] == "IN_PROGRESS" || $record["jobStatus"] == "IN_QUEUE") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . "' " . $workerNameData . ">
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >hourglass_empty</span>" . $record["message"] . "</a>";
            } else if ($record["jobStatus"] == "FAILED") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . "' " . $workerNameData . ">
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle red small' >priority_high</span>" . $record["message"] . "</a>";
            } else {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . "' " . $workerNameData . ">
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >near_me</span>" . $record["message"] . "</a>";
            }
            $ago = $this->ago($record["modificationTimestamp"]);

            // Retrieve entity details
            $entity = $record['entity'];
            $entityLower = strtolower($entity);
            $entityUCF = ucfirst($entityLower);
            $entityNoUnderscore = str_replace('_',' ',$entityUCF);
            $entityId = $record['entityDbId'];
            $entityLabel = "$entityNoUnderscore ID: $entityId";

            // For occurrence
            if($entity == 'OCCURRENCE') {
                //Retrieve the occurrence
                $params = [
                    "fields" => "occurrence.occurrence_name as occurrenceName|occurrence.id as occurrenceDbId",
                    "occurrenceDbId" => "" . $entityId
                ];

                $result = $this->occurrence->searchAll($params, '');
                $entityLabel = isset($result['data'][0]['occurrenceName']) ? $result['data'][0]['occurrenceName'] : "unknown $entityLower";
            }
            // For germplasm file upload
            if($entity == 'GERMPLASM_FILE_UPLOAD') {
                //Retrieve the occurrence
                $params = [
                    "fields" => "file_upload.file_name as fileName|file_upload.id as fileUploadId",
                    "fileUploadId" => "" . $entityId
                ];

                $result = $this->germplasmFileUpload->searchAll($params, '');
                $entityLabel = "File name: " . (isset($result['data'][0]['fileName']) ? $result['data'][0]['fileName'] : "unknown $entityLower");
            }
            // Retrieve entry list names
            if ($entity == 'ENTRY_LIST') {
                $params = [
                    'fields' => 'entryList.entry_list_name AS entryListName | entryList.id AS entryListDbId',
                    'entryListDbId' => "equals $entityId"
                ];
                $result = $this->entryList->searchAll($params);
                $entityLabel = $result['data'][0]['entryListName'] ?? "";
            }
            // Retrieve list information
            if($entity == 'LIST'){
                $result = $this->lists->searchAll(
                    [
                        "fields" => "list.abbrev AS listAbbrev | list.name AS listName | list.id AS listDbId",
                        "listDbId" => "equals $entityId"
                    ],
                    "showWorkingList=true"
                );

                $entityLabel = $result['data'][0]['listName'] ?? "";
            }

            // Append details to $msg
            $msg .= "<time class='media-meta'> <b>" . $entityLabel . "</b></time> <time class='media-meta'>" . $ago . "</time></li>";
            $formatted[] = $msg;
        }
        return implode(' ', $formatted);
    }

    /**
     * Format the background process records into dropdown selections
     * @param $backgroundProcesses Array of background process records
     * @return string formatted HTML elements
     */
    public function getNotifications($backgroundProcesses)
    {
        $formatted = [];
        foreach ($backgroundProcesses as $record) {

            $entity = strtolower($record['entity'] ?? '');
            $workerName = strtolower($record['workerName'] ?? '');
            $jobRemarks = $record['jobRemarks'];
            $description = '';

            $entityName = '';
            $abbrev = '';
            if($entity = 'experiment') {
                $result = \Yii::$container->get('app\models\Experiment')->getOne($record["entityDbId"]);
                $entityName = $result['data']['experimentName'] ?? '';
            }

            $message = $record['message'] ?? $record['description'];
            
            if ($record["jobStatus"] == "DONE") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . " ' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks'>
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle green small' >check</span> ".$message."</a>";
            } else if ($record["jobStatus"] == "IN_PROGRESS" || $record["jobStatus"] == "IN_QUEUE") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . " ' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks'>
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >hourglass_empty</span>".$message."</a>";
            } else if ($record["jobStatus"] == "FAILED") {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . " ' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks'>
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle red small' >priority_high</span> ".$message."</a>";
            } else {
                $msg = "<li class='bg_notif-btn' data-transaction_id='" . $record["entityDbId"] . " ' data-abbrev='$abbrev' data-worker-name='$workerName' data-entity='$entity' data-description='$description' data-filename='$jobRemarks'>
                <a  class='grey-text text-darken-2'><span class='material-icons icon-bg-circle grey small' >near_me</span> Experiment transaction sent for background process</a>";
            }
            $ago = $this->ago($record["modificationTimestamp"]);

            //Format: TOOL-METHOD_ENDPOINT-ROOT_ID-USER_ID
            //Retrieve the experiment ID (ROOT ID) in this case
            $transactionId = $record['entityDbId'];
            $params = [
                "fields" => "experiment.experiment_name as experimentName|experiment.id as experimentDbId",
                "experimentDbId" => "" . $transactionId
            ];

            $result = $this->experiment->searchAll($params, '');
            $experimentName = isset($result['data'][0]['experimentName']) ? $result['data'][0]['experimentName'] : '';
            if(isset($record['workerName']) && strpos(strtolower($record['workerName']),'copy') !== false && $record["jobStatus"] == "FAILED"){
                $msg .= "<time class='media-meta'> <b>" . $experimentName . "</b> set as draft and can still be worked on.</time> <time class='media-meta'>" . $ago . "</time></li>";
            } else if(isset($record['workerName']) && $record['workerName'] != '' && $record['application'] == "GERMPLASM_CATALOG"){
                $msg .= "<time class='media-meta'> <b>" . $record['jobRemarks'] . "</b> <time class='media-meta'>" . $ago . "</time></li>";
            } else $msg .= "<time class='media-meta'> <b>" . $experimentName . "</b></time> <time class='media-meta'>" . $ago . "</time></li>";
            $formatted[] = $msg;
        }
        return implode(' ', $formatted);
    }

    /**
     * Returns plural form of text. 
     * Pls note: This is slightly different from that of the pluralize function of BrowserController
     * @param integer $count given count
     * @param string $text given text
     * @return string plural form of text
     */
    function pluralize($count, $text)
    {
        return $count . (($count == 1) ? (" $text") : (" ${text}s"));
    }

    /**
     * Returns a single number of years, months, days, hours, minutes or seconds 
     * between the current date and the provided date
     * @param string $timestamp Given timestamp
     * @return string interval of date
     */
    function ago($timestamp)
    {
        $interval = date_create('now')->diff(new \DateTime($timestamp));
        $suffix = ($interval->invert ? ' ago' : '');
        if ($interval->y >= 1) return $this->pluralize($interval->y, 'year') . $suffix;
        if ($interval->m >= 1) return $this->pluralize($interval->m, 'month') . $suffix;
        if ($v = $interval->d >= 1) return $this->pluralize($interval->d, 'day') . $suffix;
        if ($v = $interval->h >= 1) return $this->pluralize($interval->h, 'hour') . $suffix;
        if ($v = $interval->i >= 1) return $this->pluralize($interval->i, 'minute') . $suffix;
        return $this->pluralize($interval->s, 'second') . $suffix;
    }

    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @param String $fieldParam Fields param when retrieving entities in background job transaction
     * @param String $filterFieldParam Field to be used when retrieving an entity
     * @param String $displayField Field to be displayed in the notifications
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundProcesses, $fieldParam, $filterFieldParam, $displayField){
        $transactions = [];
        foreach ($backgroundProcesses as $record) {
            $status = strtolower($record['jobStatus']);

            if($status == 'done'){
                $iconClass = 'green';
                $icon = 'check';
            }else if(in_array($status, ['in_progress, in_queue'])){
                $iconClass = 'grey';
                $icon = 'hourglass_empty';
            }else if($status == 'failed'){
                $iconClass = 'red';
                $icon = 'priority_high';
            }else{
                $iconClass = 'grey';
                $icon  = 'near_me';
            }

            $entityId = $record['entityDbId'];
            $message = $record['description'] . ' ' . $status . '!';
            $item = '<li class="bg_notif-btn" data-transaction_id="'.$entityId.'">
                <a class="grey-text text-darken-2">
                    <span class="material-icons icon-bg-circle small '.$iconClass.'">'.$icon.'</span>
                    '.$message.'
                </a>';

            $timeDiff = $this->ago($record['modificationTimestamp']);
            $modelClass = str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($record['entity']))));
            if ($modelClass == 'List') $modelClass = 'Lists';
            $model = '\\app\\models\\'.$modelClass;

            $params = [
                'fields' => $fieldParam,
                $filterFieldParam => (string) $entityId
            ];

            $result = $model::searchAll($params, 'limit=1', false);
            $entityName = isset($result['data'][0][$displayField]) ? $result['data'][0][$displayField] : '';

            $timeElement = '<time class="media-meta">';
            $item .= $timeElement.' <b>'.$entityName.'</b></time>'.$timeElement.$timeDiff.'</time></li>';
            $transactions[] = $item;
        }

        return implode(' ', $transactions);

    }
}
