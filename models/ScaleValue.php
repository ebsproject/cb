<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;
use app\interfaces\models\IScaleValue;

use Yii;

/**
 * This is the model class for table "master.scale_value".
 *
 * @property int $id Primary key of the record in the table
 * @property int $scale_id Scale where the value is associated with
 * @property string $value Allowed value in the scale
 * @property int $order_number Order of the value in the scale; -1 if order is not significant
 * @property string $description Additional information about the scale value
 * @property string $display_name Name or label of the scale value to show to users
 * @property string $remarks Additional details about the scale value
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $scale_value_status Indicates whether scale value is shown in list of options (null or show) or not (hide)
 */
class ScaleValue extends ScaleValueBase implements IScaleValue
{
	/** Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScaleValueBase the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'creator' => array(self::BELONGS_TO, 'User', 'creator_id'),
			'modifier' => array(self::BELONGS_TO, 'User', 'modifier_id'),
			'scale' => array(self::BELONGS_TO, 'Scale', 'scale_id'),
			'variableRelationValues' => array(self::HAS_MANY, 'VariableRelationValue', 'variable_a_scale_value_id'),
			'variableRelationValues1' => array(self::HAS_MANY, 'VariableRelationValue', 'variable_b_scale_value_id'),
		);
	}

	/**
	 * Get scale values for a given scale ID
	 *
	 * @param Integer $scaleId ID of the scale
	 * @return Array; Data containing the scale values in tags form
	 */

	public static function getScaleValuesTags($scaleId) {

		$returnVal = [];

		$sql = "
			SELECT
				value
			FROM
				master.scale_value sv 
			WHERE
				sv.scale_id = {$scaleId}
				AND sv.is_void = FALSE
		";
		$results = Yii::$app->db->createCommand($sql)->queryAll();

		foreach ($results as $result) {
			$returnVal[$result['value']] = $result['value'];
		}
		return $returnVal;
	}

	  /**
     * Retrieves an existing variable scale values in the database
     * @param $id integer variable Id
     * @return array variable scale values of a variable in the database
     */
    public static function getVariableScaleValues($id, $param){
        $method = 'POST';
        $path = 'variables'.'/'.$id.'/scale-values-search';
        $sort = 'sort=orderNumber';
        if(!empty($param)){
            $data = Yii::$app->api->getParsedResponse($method, $path, json_encode($param),$sort, true);
        }else{
            $data = Yii::$app->api->getParsedResponse($method, $path, null,$sort, true);
        }

        $result = [];

        // Check if it has a result, then retrieve menu data
        if (
          isset($data['success']) // check if property exists
          && $data['success'] // check if 'success' is true
          && isset($data['data'])
          && isset($data['data'][0]['scaleValues'])
        ) {
            $result = $data['data'][0]['scaleValues'];
        }

        return $result;
    }
   
}