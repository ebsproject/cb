<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\dataproviders\ArrayDataProvider;
use app\modules\experimentCreation\models\BrowserModel;
use Yii;

/**
 * Base class for experiment.planting_job table that uses the BaseModel class
 */
class PlantingJobOccurrence extends BaseModel {

    public function __construct(
        public BrowserModel $browserModel
    ) { }

    public static function apiEndPoint() {
        return 'planting-job-occurrences';
    }

    private $data;

    public function __get($varName){
        if(isset($this->data[$varName])){
            return $this->data[$varName];
        }
    }

    public function __set($varName, $value){
        $this->data[$varName] = $value;
    }

    /**
     * Fields to be validated and included in search filters
     */
    public function rules ()
    {
        return [
            [[
                'occurrenceEntryCount',
                'occurrencePlotCount',
                'seedsPerEnvelope',
                'envelopesPerPlot',
                'creatorDbId',
                'modifierDbId',
            ], 'integer'],
            [[
                'experimentCode',
                'experimentName',
                'occurrenceName',
                'siteCode',
                'packageUnit',
                'plotDimensions'
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp'
            ], 'safe'],
        ];
    }

    /**
     * Search planting jobs
     *
     * @param array $params filter parameters
     * @param integer $id planting job identifier
     * @return array $dataProvider planting job records
     */
    public function search($params, $id){
        $filters = [
            'plantingJobDbId' => (string) $id
        ];
        
        
        $paramLimit = 'limit=10';
        $paramPage = '';
        $paramSort = '';

        extract($params);
        $columnFilters = isset($PlantingJobOccurrence) ? $this->browserModel->formatFilters($PlantingJobOccurrence) : [];

        $filters = array_merge($filters, $columnFilters);

        array_walk($filters, function (&$value, $key) {
            if (
                $key != 'fields' &&
                $value != '' &&
                !str_contains(strtolower($value), 'equals') &&
                !str_contains(strtolower($value), '%')
            )
                $value = "equals $value";
        });

        // get current page
        $currPage = 1;
        if (isset($_GET['dp-1-page'])){
            $paramPage = '&page=' . $_GET['dp-1-page'];
            $currPage = $_GET['dp-1-page'];
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $currPage = $_GET['page'];
        }

        // get current sort
        $paramSort = 'sort=experimentDbId:desc|occurrenceDbId';
        $sortParams = '';
        if(isset($_GET['sort'])) {
            $sortParams = $_GET['sort'];
        } else if (isset($_GET['dp-1-sort'])) {
            $sortParams = $_GET['dp-1-sort'];
        }

        // for sorting
        if (!empty($sortParams)) {
            if($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort='. $column . $order;
        }

        // Get page size from cookies of dynagrid
        $cookies = Yii::$app->request->cookies;
        if(isset($cookies['planting-job-occurrences-browser_'])) {
            $paramLimit = $cookies->getValue('planting-job-occurrences-browser_');
            if(!is_array($paramLimit)){
                $paramLimit = json_decode($paramLimit, true);
                $paramLimit = json_decode($paramLimit['grid'], true);
                $paramLimit = $paramLimit['page'];
                $paramLimit = 'limit=' . $paramLimit;
            }else{
                $paramLimit = 'limit=10';
            }
        }

        $pagination = $paramLimit.$paramPage;

        // check if page is out of range
        $totalPagesArr = Yii::$app->api->getResponse('POST',
            'planting-job-occurrences-search?'.$paramLimit, 
            json_encode($filters)
        );

        if(isset($totalPagesArr['body']['metadata']['pagination']['totalPages'])){
            $totalPages = $totalPagesArr['body']['metadata']['pagination']['totalPages'];
        }

        // if out of range
        $totalPages = 0;
        $currPage = intval($currPage);
        if($currPage > $totalPages){
            $pagination = $paramLimit.'&page=1';
        }

        // add '&' if pagination is set
        if(!empty($pagination)){
            $paramSort = '&'. $paramSort;
        }

        // get all occurrences for select all
        $allResults = Yii::$app->api->getParsedResponse('POST',
            'planting-job-occurrences-search?'.$paramSort, 
            json_encode($filters),
            null,
            true
        );

        $allOccurrenceIds = [];
        if(isset($allResults['success']) && $allResults['success']){
            $occurrences = $allResults['data'];
            $allOccurrenceIds = array_column($occurrences, 'plantingJobOccurrenceDbId');
        }

        $attributes = [
            'experimentCode', 'experimentName', 'siteCode', 'occurrenceName', 
            'occurrenceEntryCount', 'occurrencePlotCount','seedsPerEnvelope',
            'plotDimensions', 'packageUnit', 'envelopesPerPlot', 'siteCode'
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allResults['data'],
            'key' => 'plantingJobOccurrenceDbId',
            'pagination' => false,
            'totalCount' => $allResults['totalCount'],
            'sort' => [
                'attributes' => $attributes
            ],
        ]);

        // store occurrence IDs to session
        $userModel = new User();
        $userId = $userModel->getUserId();

        Yii::$app->session->set("pim_$userId"."_filtered_occurrence_ids_$id", $allOccurrenceIds);

        if (!($this->load($params) && $this->validate())) {
            //validate results
            return $dataProvider;
        }

        return $dataProvider;
    }

}