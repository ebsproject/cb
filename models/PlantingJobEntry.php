<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use app\dataproviders\ArrayDataProvider;
use app\modules\experimentCreation\models\BrowserModel;
use Yii;

/**
 * Base class for experiment.planting_job_entry table that uses the BaseModel class
 */
class PlantingJobEntry extends BaseModel {

    public function __construct(
        public BrowserModel $browserModel,
        $config=[]) {

        parent::__construct($config);
    }

    public static function apiEndPoint() {
        return 'planting-job-entries';
    }

    private $data;

    public function __get($varName){
        if(isset($this->data[$varName])){
            return $this->data[$varName];
        }
    }

    public function __set($varName, $value){
        $this->data[$varName] = $value;
    }

    /**
     * Fields to be validated and included in search filters
     */
    public function rules ()
    {
        return [
            [[
                'envelopeCount',
                'packageDbId',
                'actualEntryDbId',
                'actualGermplasmDbId',
                'actualPackageDbId',                
                'creatorDbId',
                'modifierDbId',
                'entryPlotCount',
                'envelopeCount',
                'packageQuantity',
            ], 'integer'],
            [[
                'requiredPackageQuantity',
                'actualPackageReserved',
                'actualAvailableQuantity',
            ], 'double'],
            [[
                'experimentCode',
                'experimentName',
                'occurrenceName',
                'siteCode',
                'requiredPackageUnit',
                'designation',
                'parentage',
                'seedName',
                'packageLabel',
                'entryName',
                'entryCode',
                'isSufficient',
                'actualGermplasmName',
                'actualParentage',
                'actualSeedName',
                'actualPackageLabel',
                'actualPackageUnit',
            ], 'string'],
            [[
                'creationTimestamp',
                'modificationTimestamp',
                'isReplaced'
            ], 'safe'],
        ];
    }

    /**
     * Search planting jobs
     *
     * @param array $params filter parameters
     * @param integer $id planting job identifier
     * @return array $dataProvider planting job records
     */
    public function search($params, $id){
      
        $data = [];
        
        $paramPage = '';
        $paramSort = '';
        $totalPages = 0;

        extract($params);
        $columnFilters = isset($PlantingJobEntry) ? $this->browserModel->formatFilters($PlantingJobEntry) : [];

        $filters['plantingJobDbId'] = (string) $id;

        $filters = array_merge($filters, $columnFilters);

        array_walk($filters, function (&$value, $key) {
            if (
                $key != 'fields' &&
                $value != '' &&
                !str_contains(strtolower($value), 'equals') &&
                !str_contains(strtolower($value), '%')
            )
                $value = "equals $value";
        });

        $dataProviderId = 'dynagrid-pim-planting-job-entries';

        // get current sort
        $paramSort = 'sort=experimentDbId:desc|entryNumber|occurrenceDbId';
        $sortParams = '';
        if(isset($_GET['sort'])) {
            $sortParams = $_GET['sort'];
        } else if (isset($_GET[$dataProviderId.'-sort'])) {
            $sortParams = $_GET[$dataProviderId.'-sort'];
        }

        // for sorting
        if (!empty($sortParams)) {
            if($sortParams[0] == '-') {
                $order = ':desc';
                $column = substr($sortParams, 1);
            } else {
                $order = '';
                $column = $sortParams;
            }

            $paramSort = 'sort='. $column . $order;
        }

        $paramPage = '';
        $pageCount = 1;
        if (isset($_GET[$dataProviderId.'-page'])){
            $pageCount = $_GET[$dataProviderId.'-page'];
            $paramPage = '&page=' . $pageCount;
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $pageCount = $_GET['page'];
        }

        // Get page size from from the preferences
        $defaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences();
        $paramLimit = 'limit='.$defaultPageSize;

        $output = Yii::$app->api->getParsedResponse('POST',
            'planting-job-entries-search?'.$paramLimit.$paramPage.'&'.$paramSort, 
            json_encode($filters)
        );

        if ($pageCount != 1 && $output['totalCount'] == 0) {
            $output = Yii::$app->api->getParsedResponse('POST', 'planting-job-entries-search?'.$paramLimit.'&'.$paramSort.'&page=1', json_encode($filters));
            $_GET[$dataProviderId.'-page'] = 1; // return browser to page 1
        }

        $totalCount = $output['totalCount'];
        $attributes = [
            'experimentCode', 'experimentName', 'siteCode', 'occurrenceName', 'isReplaced',
            'entryCode', 'envelopeCount','requiredPackageQuantity', 'isSufficient', 'packageQuantity',
            'requiredPackageUnit', 'packageDbId', 'entryPlotCount', 'entryName', 'entryCode',
            'parentage', 'seedName', 'packageLabel', 'actualGermplasmName', 'actualParentage',
            'actualSeedName', 'actualPackageLabel', 'actualAvailableQuantity', 'actualPackageUnit',
            'actualPackageReserved',
        ];

        $dataProvider = new ArrayDataProvider([
            'id' => $dataProviderId,
            'allModels' => $output['data'],
            'key' => 'plantingJobEntryDbId',
            'restified' => true,
            'totalCount' => $totalCount,
            'sort' => [
                'attributes' => $attributes
            ],
        ]);


        $url = 'planting-job-entries-search?'.$paramSort;
        $withPaginationUrl = 'planting-job-entries-search?'.$paramLimit.$paramPage.'&'.$paramSort;

        Yii::$app->session->set('planting-job-entry-param-url-' . $id, $url);
        Yii::$app->session->set('planting-job-entry-param-url-with-pagination-' . $id, $withPaginationUrl);
        Yii::$app->session->set('planting-job-entry-param-filters-' . $id, $filters);
        Yii::$app->session->set('planting-job-entry-param-sort-' . $id, $paramSort);

        $data = [
            'data' => $dataProvider,
            'totalCount' => $totalCount
        ];

        if (!($this->load($params) && $this->validate())) {
            //validate results
            return $data;
        }

        return $data;
    }

}