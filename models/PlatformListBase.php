<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.list".
 *
 * @property int $id
 * @property string $abbrev
 * @property string $name
 * @property string $display_name
 * @property string $type
 * @property int $entity_id
 * @property string $description
 * @property string $remarks
 * @property string $creation_timestamp
 * @property int $creator_id
 * @property string $modification_timestamp
 * @property int $modifier_id
 * @property string $notes
 * @property bool $is_void
 * @property string $record_uuid
 */
class PlatformListBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abbrev', 'name'], 'required'],
            [['abbrev', 'name', 'display_name', 'type', 'description', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['entity_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['entity_id', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['abbrev'], 'unique'],
            [['name'], 'unique'],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'display_name' => 'Display Name',
            'type' => 'Type',
            'entity_id' => 'Entity ID',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
