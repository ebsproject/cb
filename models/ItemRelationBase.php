<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.item_relation".
 *
 * @property int $id Primary key of the record in the table
 * @property int $parent_id Parent item which has one or more child items
 Can be null for the root node.
 * @property int $child_id Child item of the parent item
 * @property int $order_number Order of the child in the list of children of the parent item
 * @property string $remarks Additional details
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property int $position
 * @property string $tooltip
 * @property string $url
 * @property string $icon
 * @property int $visible
 * @property string $task
 * @property string $options
 * @property string $title
 * @property int $root_id
 * @property string $itemrel_url
 */
class ItemRelationBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.item_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'child_id', 'order_number', 'creator_id', 'modifier_id', 'position', 'visible', 'root_id'], 'default', 'value' => null],
            [['parent_id', 'child_id', 'order_number', 'creator_id', 'modifier_id', 'position', 'visible', 'root_id'], 'integer'],
            [['child_id'], 'required'],
            [['remarks', 'notes', 'itemrel_url'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['tooltip', 'options'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 50],
            [['task'], 'string', 'max' => 64],
            [['title'], 'string', 'max' => 30]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'child_id' => 'Child ID',
            'order_number' => 'Order Number',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'position' => 'Position',
            'tooltip' => 'Tooltip',
            'url' => 'Url',
            'icon' => 'Icon',
            'visible' => 'Visible',
            'task' => 'Task',
            'options' => 'Options',
            'title' => 'Title',
            'root_id' => 'Root ID',
            'itemrel_url' => 'Itemrel Url',
        ];
    }
}
