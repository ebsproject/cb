<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.role".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property string $description
 * @property string $display_name
 * @property integer $rank
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property string $record_uuid
 * @property string $event_log
 *
 * @property MasterItemRole[] $masterItemRoles
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property MasterTeamMember[] $masterTeamMembers
 * @property MasterUserRole[] $masterUserRoles
 */
class RoleBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master.role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['abbrev', 'name'], 'required'],
            [['description', 'remarks', 'notes', 'record_uuid', 'event_log'], 'string'],
            [['rank', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['abbrev'], 'string', 'max' => 128],
            [['name', 'display_name'], 'string', 'max' => 256],
            [['record_uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'description' => 'Description',
            'display_name' => 'Display Name',
            'rank' => 'Rank',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'record_uuid' => 'Record Uuid',
            'event_log' => 'Event Log',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterItemRoles()
    {
        return $this->hasMany(MasterItemRole::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(MasterUser::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        return $this->hasOne(MasterUser::className(), ['id' => 'modifier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterTeamMembers()
    {
        return $this->hasMany(MasterTeamMember::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterUserRoles()
    {
        return $this->hasMany(MasterUserRole::className(), ['role_id' => 'id']);
    }
}
