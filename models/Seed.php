<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\dataproviders\ArrayDataProvider;
use Yii;

/**
 * This is the model class for table "germplasm.seed".
 *
 * @property int $id Seed ID: Database identifier of the seed [SEED_ID]
 * @property string $seed_code Seed Code: Textual identifier of the seed [SEED_CODE]
 * @property string $seed_name Seed Name: Full name of the germplasm at the time the seed was harvested [SEED_NAME]
 * @property string|null $harvest_date Seed Harvest Date: Date when the seeds were harvested from the location of an experiment [SEED_HARVESTDATE]
 * @property string|null $harvest_method Harvest Method: Method of harvesting the plots in the location [SEED_HARVMETH]
 * @property int $germplasm_id Germplasm ID: Reference to the germplasm where the seed is referred [SEED_GERM_ID]
 * @property int|null $program_id Program ID: Reference to the program that owns the seeds [SEED_PROG_ID]
 * @property int|null $source_experiment_id Source Experiment ID: Reference to the experiment where the seed was harvested [SEED_EXPT_ID]
 * @property int|null $source_entry_id Source Entry ID: Reference to the entry where the seed was harvested [SEED_ENTRY_ID]
 * @property int|null $source_occurrence_id Source Occurrence ID: Reference to the occurrence where the seed was harvested [SEED_OCC_ID]
 * @property int|null $source_location_id Source Location ID: Location where the seed was harvested [SEED_LOC_ID]
 * @property int|null $source_plot_id Source Plot ID: Reference to the plot where the seed was harvested [SEED_PLOT_ID]
 * @property string|null $description Seed Description: Additional information about the seed [SEED_DESC]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class Seed extends BaseModel
{ 

      public static function apiEndPoint() {
          return 'seeds';
      }

      public static function apiEndPointPackagesSearch() {
          return 'seed-packages-search';
      }

     /**
     * Search for seeds given search parameters
     *
     * @param $params array list of search parameters
     * @return $data array list of seeds given search parameters
     */
    public static function searchRecords($params){
      
        $params = empty($params) ? null : $params;
        $response = Yii::$app->api->getResponse('POST', 'seeds-search', json_encode($params), null, true);

        $data = [];

        // check if successfully retrieved germplasm
        if(($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
          
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
        }
        
        return $data;
    }

    /**
     * Search for seeds with existing package records
     *
     * @param json $requestBody optional request content body
     * @param string $sort optional value to sort results by field
     * @param string $dataLevel optional granularity of seed packages data : all|germplasm|seed|entry
     * @param array $pagination optional value to paginate response data : { limit => int, page => int }
     *
     * @return array list of seeds with existing package records
     */
    public static function searchPackageRecords($requestBody=null, $dataLevel='all', $sort=null, $pagination=null) {
        $method = 'POST';

        // Build query parameters
        $params = 'dataLevel='.$dataLevel;
        if ($sort != null) $params .= '&sort='.$sort;
        if ($pagination != null) {
            $params .= !empty($pagination['limit']) ? '&limit='.$pagination['limit'] : '';
            $params .= !empty($pagination['page']) ? '&page='.$pagination['page'] : '';
        }

        $path = static::apiEndPointPackagesSearch() . '?' . $params;

        try {
            if ($pagination == null) {
                $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($requestBody), retrieveAll: true);
            } else {
                $response = Yii::$app->api->getParsedResponse($method, $path, json_encode($requestBody));
            }
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            // HTTP 5xx occurred
            $response['success'] = false;
        }

        return ($response['success'] && isset($response['data'])) ? $response['data'] : [];
    }

    /**
     * Search for germplasm with packageCount and seedCount info
     *
     * @param json $requestBody optional request content body
     * @param string $limit optional sort, page or limit options 
     * @param retrieveAll boolean optional whether all of the data will be fetched or not
     * @return array list of germplasm records with package and seed count info
     */
    public static function searchPackageInfo($requestBody=null, $limit='',$retrieveAll=false) {
        
        $method = 'POST';
        $params = 'dataLevel=germplasm';
        
        $path = static::apiEndPointPackagesSearch() . '?' . $params;

        // check if successfully retrieved germplasm
        try {
            $response = Yii::$app->api->getResponse($method, $path, json_encode($requestBody), $limit, $retrieveAll);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            // HTTP 5xx occurred
            $response['success'] = false;
        }
        
        return $response;
    }

    /**
     * Retrieves list of the attributes for a given seed
     *
     * @param $seedId integer seed identifier
     * @return mixed arrayDataProvider of seed attributes
     */
    public static function getSeedAttributes($seedId){
        $response = Yii::$app->api->getResponse('GET', 'seeds/'.$seedId.'/attributes');
        $data = $response['body']['result']['data'][0]['attributes'];

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);
    }

    /**
     * Retrieve seed information in view seed
     *
     * @param $id integer seed identifier
     * @return $data array seed information
     */
    public static function getSeedInfo($id) {
        return [
            'attributes' => Seed::getSeedAttributes($id)
        ];
    }
}
