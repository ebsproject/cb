<?php

namespace app\models;

use Yii;
use app\models\Lists;
use app\modules\account\models\ListAccessSearch;

/**
 * This is the model class for table "platform.list_access".
 *
 * @property int $id Primary key of the record in the table
 * @property int $list_id ID of the list shared with other users
 * @property int $user_id ID of user where the list is shared with
 * @property string $permission Permission granted to users in accessing the list; this can be read, or read-write permission
 * @property string $remarks Additional details about the list permission
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property string $record_uuid
 */
class PlatformListAccess extends \app\models\PlatformListAccessBase
{
    /**
     * Method for giving access to specified users for a list
     * 
     * @param Integer $listId ID of the list selected
     * @param Array $userIdArray array containing the user Ids
     * @param Array $teamIdArray array containing the teams Ids
     * @param String $permission the permission of the user / program. can be read or read_write
     * @return response data from API
     */

    public function addAccess($listId, $userIdArray, $teamIdArray, $permission='read') {
        
        $url = 'lists/'.$listId.'/permissions';
        $records = [];
        $requestArr = [];
        foreach ($userIdArray as $userId) {
            $records[] = [
                'entity' => 'user',
                'entityId' => $userId,
                'permission' => $permission
            ];
        }

        foreach ($teamIdArray as $teamId) {
            $records[] = [
                'entity' => 'program',
                'entityId' => $teamId,
                'permission' => $permission
            ];
        }
        $requestArr["records"] = $records;

        return Yii::$app->api->getParsedResponse('POST', $url, json_encode($requestArr));

    }

    /**
     * Remove access from list given list access Ids
     * 
     * @param Integer $listId ID of the list
     * @param Array $accessDataArr the current access data of the list
     * @param Array $selectedUser the user Ids to be removed from the permission list
     * @param Array $selectedTeams the program ids to be removed from the permission list
     * @return Integer total count of affected records
     */
    public function revokeAccess($listId, $accessDataArr, $selectedUsers, $selectedTeams) {
        $url = 'lists/'.$listId;

        if (empty($accessDataArr)) {
            return 0;
        }

        $userAccess = isset($accessDataArr['user']) ? $accessDataArr['user'] : [];
        $programAccess = isset($accessDataArr['program']) ? $accessDataArr['program'] : [];

        foreach ($selectedUsers as $userId) {
            if (isset($userAccess[$userId])) {
                unset($userAccess[$userId]);
            }
        }

        foreach ($selectedTeams as $teamId) {
            if (isset($programAccess[$teamId])) {
                unset($programAccess[$teamId]);
            }
        }
        $newAccessData['accessData'] = [];

        if (!empty($userAccess)) {
            $newAccessData['accessData']['user'] = $userAccess;
        }

        if (!empty($programAccess)) {
            $newAccessData['accessData']['program'] = $programAccess;
        }
        $params = [
            'accessData' => $newAccessData['accessData']
        ];

        Yii::$app->api->getResponse('PUT', $url, json_encode($params));
        
        return count($userAccess) + count($programAccess);
    }

    /**
     * Updates list access for given list access ids
     * 
     * @param Array $listAccessIds array containing the list access db Ids
     * @param String $newPermission the new permission value
     * 
     * @return boolean if call was successful
     */
    public static function updateAccess($listAccessIds, $newPermission) {

        $params = [
            "permission" => $newPermission
        ];
        $jsonParams = json_encode($params);
        // update records one by one
        foreach ($listAccessIds as $listAccessDbId) {

            $url = 'list-permissions/'.$listAccessDbId;
            $response = Yii::$app->api->getResponse('PUT', $url, $jsonParams);

            if (!isset($response) || empty($response)) {
                return false;
            }

            if ($response['status'] != 200) {
                return false;
            }
        }
        return true;
    }
}
