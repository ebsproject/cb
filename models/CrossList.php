<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.cross_list".
 *
 * @property int $id Primary key of the record in the table
 * @property int $program_id Product development program where the cross list belongs to
 * @property int $place_id Place where the cross list is conducted
 * @property int $phase_id Breeding stage or phase of the cross list; default is HB
 * @property int $year Year when the cross list in conducted
 * @property int $season_id Season of the place when the cross list is conducted
 * @property int $number Sequence number of the cross list based on program, year, and season
 * @property string $name Unique name of the cross list
 * @property string $title Title of the cross list; custom name of the cross list set by the creator
 * @property string $remarks Additional details about the cross list
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void    is_void boolean NOT NULL DEFAULT false, -- 
 * @property int $cross_count Number of records in the transaction.
 * @property string $status status of the cross list transaction (draft, created, submitted, accepted, done)
 * @property int $study_id Study id
 */
class CrossList extends CrossListBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational.cross_list';
    }

   
}
