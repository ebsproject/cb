<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\Api;

/**
 * This is the model class for table "experiment.occurrence_data".
 */
class OccurrenceData extends BaseModel
{
    public $api;

    public function __construct (
        Api $api,
        $config = []
    )
    {
        $this->api = $api;

        parent::__construct($config);
    }

    /**
     * API endpoint for occurrence-data
     */
    public static function apiEndPoint()
    {
        return 'occurrence-data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['occurrence_id', 'variable_id', 'data_value', 'data_qc_code', 'creator_id'], 'required'],
            [['occurrence_id', 'variable_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['occurrence_id', 'variable_id', 'creator_id', 'modifier_id'], 'integer'],
            [['data_value', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['data_qc_code'], 'string', 'max' => 8],
            [['occurrence_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentOccurrence::className(), 'targetAttribute' => ['occurrence_id' => 'id']],
            [['variable_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterVariable::className(), 'targetAttribute' => ['variable_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'occurrence_id' => 'Occurrence ID',
            'variable_id' => 'Variable ID',
            'data_value' => 'Data Value',
            'data_qc_code' => 'Data Qc Code',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }

    /**
     * Search for a specific occurrence data record of the occurrence
     * @param interger $id Occurrence ID
     * @param array $param Request body parameter
     * @return array $result Single occurrence data record
     */
    public static function getOccurrenceDataRecord($id, $params)
    {
        $method = 'POST';
        $params['occurrenceDbId'] = (string) $id;
        $apiEndpoint = 'occurrence-data-search';
        $data = Yii::$app->api->getResponse($method, $apiEndpoint, json_encode($params), null, true);
        $result = null;

        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])) {
            $result = $data['body']['result']['data'][0];
        }

        return $result;
    }

    /**
     * Search All occurrence data records of the occurrence
     * @param interger $id Occurrence ID
     * @param array $param Request body parameter
     * @return array $result Array of experiment data records
     */
    public static function searchAllRecords($id, $params = null)
    {
        $method = 'POST';
        $apiEndpoint = 'occurrence-data-search';
        $params['occurrenceDbId'] = (string) $id;

        if (!empty($params)) {
            $data = Yii::$app->api->getResponse($method, $apiEndpoint, json_encode($params), null, true);
        } else {
            $data = Yii::$app->api->getResponse($method, $apiEndpoint, null, null, true);
        }

        $result = null;
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])) {
            $result = $data['body']['result']['data'];
        }

        return $result;
    }

    /**
     * Save Occurrence-level data
     *
     * @param array $formData Contains experiment basic information to be saved
     * @param string $occurrenceDbIdsString Record IDs of selected Occurrences
     * @param string $protocolType Protocol type identifier
    */
    public function saveOccurrenceData ($formData, $occurrenceDbIdsString, $protocolType = null)
    {
        $occurrenceDbIdsArray = explode('|', $occurrenceDbIdsString);
        $occurrenceDataArray = [];
        $result = false;

        // Rearrange formData to make it easier to access in later code
        foreach ($formData['Experiment'] as $data) {
            // Retrieve the key of this object
            $dataKey = array_keys($data)[0];

            // Retrieve the key of the sub-object of $data
            $variableDbId = array_keys($data[$dataKey])[0];
            $dataValue = $data[$dataKey][$variableDbId];

            if (gettype($variableDbId) === 'string') {
                // Retrieve the key of the innermost object
                $disabledString = $variableDbId;
                $variableDbId = array_keys($data[$dataKey][$disabledString])[0];
                $dataValue = $data[$dataKey][$disabledString][$variableDbId];
            }

            // Store to $occurrenceDataArray for later use
            array_push($occurrenceDataArray, [
                'variableDbId' => "$variableDbId",
                'dataValue' => "$dataValue",
            ]);
        }

        // Sort through Occurrence IDs to load $createOccurrenceDataParams and $updateOccurrenceDataParams
        foreach ($occurrenceDbIdsArray as $index => $occurrenceDbId) {
            $createOccurrenceDataParams = [];

            // Retrieve Experiment Code
            $params = [
                'fields' => 'occurrence.id AS occurrenceDbId | experiment.experiment_code AS experimentCode | item.abbrev AS dataProcessAbbrev',
                'occurrenceDbId' => $occurrenceDbId,
            ];

            $response = $this->api->getApiResults(
                'POST',
                'occurrences-search',
                json_encode($params),
                'limit=1',
                true
            );

            if (
                $response['success'] &&
                isset($response['data']) &&
                !empty($response['data'])
            ) {
                // This determines if the current protocol type exists in this Experiment
                $occurrenceDbId = $response['data'][0]['occurrenceDbId'];
                $experimentCode = $response['data'][0]['experimentCode'];
                $dataProcessAbbrev = $response['data'][0]['dataProcessAbbrev'];
                $params = [
                    'fields' => 'protocol.id AS protocolDbId | protocol.protocol_code AS protocolCode',
                    'protocolCode' => "$protocolType%$experimentCode",
                ];

                $response = $this->api->getApiResults(
                    'POST',
                    'protocols-search',
                    json_encode($params),
                    'limit=1&sort=protocolCode:desc',
                    true
                );

                if (
                    $response['success'] &&
                    isset($response['data'])
                ) {
                    $protocolDbId = null;


                    if (
                        !empty($response['data']) &&
                        !empty($response['data'][0]['protocolCode'])
                    ) {
                        $protocolDbId = $response['data'][0]['protocolDbId'];
    
                    }

                    // Retrieve protocol variables under this protocol ID
                    $params = [
                        'fields' => 'occurrence_data.id AS occurrenceDataDbId | occurrence_data.protocol_id AS protocolDbId | occurrence_data.occurrence_id AS occurrenceDbId',
                        'occurrenceDbId' => "$occurrenceDbId",
                        'protocolDbId' => "$protocolDbId",
                    ];

                    $response = $this->api->getApiResults(
                        'POST',
                        'occurrence-data-search',
                        json_encode($params),
                        '',
                        true
                    );

                    if (
                        $response['success'] &&
                        isset($response['data'])
                    ) {
                        $data = $response['data'];

                        // This deletes previous protocol variables
                        foreach ($data as $value) {
                            $occurrenceDataDbId = $value['occurrenceDataDbId'];

                            $this->api->getApiResults(
                                'DELETE',
                                "occurrence-data/$occurrenceDataDbId",
                            );
                        }
                    }

                    foreach ($occurrenceDataArray as $index => $occurrenceData) {
                        // Check if this variable already exists in this particular Occurrence
                        $variableDbId = $occurrenceData['variableDbId'];
                        $params = [
                            'fields' => 'occurrence_data.id AS occurrenceDataDbId | occurrence_data.occurrence_id AS occurrenceDbId | occurrence_data.variable_id AS variableDbId',
                            'occurrenceDbId' => "$occurrenceDbId",
                            'variableDbId' => "$variableDbId",
                        ];

                        $response = $this->api->getApiResults(
                            'POST',
                            'occurrence-data-search',
                            json_encode($params),
                            'limit=1&sort=occurrenceDbId:desc',
                            true
                        );

                        if (
                            $response['success'] &&
                            isset($response['data']) &&
                            ( // Validate protocol type and data process abbrevation first before allowing to store or delete occurrence data
                                (
                                    $protocolType == 'planting' &&
                                    !str_contains($dataProcessAbbrev, 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS')
                                ) ||
                                (
                                    $protocolType == 'pollination' && 
                                    (
                                        str_contains($dataProcessAbbrev, 'CROSS_PARENT_NURSERY') ||
                                        str_contains($dataProcessAbbrev, 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') ||
                                        str_contains($dataProcessAbbrev, 'GENERATION_NURSERY_DATA_PROCESS')
                                    )
                                ) ||
                                (
                                    $protocolType == 'harvest'
                                )
                            )
                        ) {
                            if (
                                empty($response['data']) &&
                                !empty($occurrenceData['dataValue'])
                            ) { // Create Occurrence data
                                if (!empty($protocolDbId)) {
                                    $occurrenceDataArray[$index]['protocolDbId'] = "$protocolDbId";
                                }

                                $occurrenceDataArray[$index]['occurrenceDbId'] = $occurrenceDbId;

                                array_push($createOccurrenceDataParams, $occurrenceDataArray[$index]);
                            } else if (
                                !empty($response['data']) &&
                                empty($occurrenceData['dataValue'])
                            ) { // Delete Occurrence data
                                $occurrenceDataDbId = $response['data'][0]['occurrenceDataDbId'];

                                $response = $this->api->getApiResults(
                                    'DELETE',
                                    "occurrence-data/$occurrenceDataDbId"
                                );
                            } else if (
                                !empty($response['data']) &&
                                !empty($occurrenceData['dataValue'])
                            ) { // Update Occurrence data
                                $occurrenceDataDbId = $response['data'][0]['occurrenceDataDbId'];
                                $dataValue = $occurrenceData['dataValue'];
                                $params = [
                                    'dataValue' => "$dataValue",
                                ];

                                $response = $this->api->getApiResults(
                                    'PUT',
                                    "occurrence-data/$occurrenceDataDbId",
                                    json_encode($params)
                                );
                            }
                        }
                    }
                }
            }

            if (!empty($createOccurrenceDataParams)) {
                $params = [
                    'records' => $createOccurrenceDataParams,
                ];

                $response = $this->api->getApiResults(
                    'POST',
                    "occurrence-data",
                    json_encode($params)
                );
            }

            $result = true;
        }

        return $result;
    }
}
