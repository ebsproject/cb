<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "experiment.occurrence".
 *
 * @property int $id Occurrrence ID: Database identifier of the occurrence [OCC_ID]
 * @property string $occurrence_code Occurrence Code: Textual identifier of the occurrence [OCC_CODE]
 * @property string $occurrence_name Occurrence Name: Full name of the occurrence [OCC_NAME]
 * @property string $occurrence_status Occurrence Status: Status of the occurrence with regard to its plots {draft, mapped} [OCC_STATUS]
 * @property string|null $description Occurrence Description: Additional information about the occurrence [OCC_DESC]
 * @property int $experiment_id Experiment ID: Reference to the experiment that is represented by the occurrence [OCC_EXPT_ID]
 * @property int|null $geospatial_object_id Geospatial Object ID: Reference to the site or field (geospatial object) where the occurrence is planned to be conducted [OCC_GEO_ID]
 * @property int $creator_id
 * @property string $creation_timestamp
 * @property int|null $modifier_id
 * @property string|null $modification_timestamp
 * @property bool $is_void
 * @property string|null $notes
 * @property string|null $access_data
 * @property string|null $event_log
 * @property int|null $rep_count Number of replication
 * @property string|null $occurrence_document Sorted list of distinct lexemes which are normalized; used in search query
 */
class OccurrenceBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experiment.occurrence';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['occurrence_code', 'occurrence_name', 'occurrence_status', 'experiment_id'], 'required'],
            [['description', 'notes', 'occurrence_document'], 'string'],
            [['experiment_id', 'geospatial_object_id', 'creator_id', 'modifier_id', 'rep_count'], 'default', 'value' => null],
            [['experiment_id', 'geospatial_object_id', 'creator_id', 'modifier_id', 'rep_count'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'access_data', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['occurrence_code', 'occurrence_status'], 'string', 'max' => 64],
            [['occurrence_name'], 'string', 'max' => 128],
            [['occurrence_code'], 'unique'],
            [['experiment_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentExperiment::className(), 'targetAttribute' => ['experiment_id' => 'id']],
            [['geospatial_object_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlaceGeospatialObject::className(), 'targetAttribute' => ['geospatial_object_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'occurrence_code' => 'Occurrence Code',
            'occurrence_name' => 'Occurrence Name',
            'occurrence_status' => 'Occurrence Status',
            'description' => 'Description',
            'experiment_id' => 'Experiment ID',
            'geospatial_object_id' => 'Geospatial Object ID',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'access_data' => 'Access Data',
            'event_log' => 'Event Log',
            'rep_count' => 'Rep Count',
            'occurrence_document' => 'Occurrence Document',
        ];
    }
}
