<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use app\interfaces\models\ICrop;
use Yii;

/**
 * This is the model class for table "master.crop".
 *
 * @property int $id Identifier of the record within the table
 * @property string $abbrev Unique short name of the crop
 * @property string $name Unique name of the crop
 * @property string $description Description about the crop
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class Crop extends BaseModel implements ICrop
{
    /**
     * Set api endpoint
     */
    public static function apiEndPoint() {
        return 'crops';
    }

    /**
     * Retrieves an existing crop in the database
     *
     * @param int $id crop identifier
     * @return array crop record in the database
     */
    public function getCrop($id) {
        $method = 'GET';
        $path = "crops/$id";

        $response = Yii::$app->api->getParsedResponse($method, $path);

        return isset($response['success']) ? $response['data'] : [];
    }

    /**
     * Retrieves an existing crop in the database
     *
     * @param string $abbrev crop code, e.g. RICE, WHEAT, MAIZE
     * @return array crop record in the database
     */
    public function getCropByAbbrev($abbrev){
        $method = 'GET';
        $path = "crops?cropCode=$abbrev";

        $data = Yii::$app->api->getResponse($method, $path);
        $result = [];
         
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
          $result = $data['body']['result']['data'][0];
        }
        return $result;
    }
}
