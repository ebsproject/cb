<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\User;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "master.season".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property string $description
 * @property string $display_name
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 *
 * @property MasterPlaceSeason[] $masterPlaceSeasons
 * @property MasterPlace[] $places
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property OperationalCrossList[] $operationalCrossLists
 * @property OperationalStudy[] $operationalStudies
 * @property WarehouseStudy[] $warehouseStudies
 */
class Season extends BaseModel
{
    public $seasonCode;
    public $seasonName;
    public $isActive;
    public $description;
    public $season_code;
    public $season_name;
    public $is_active;
    protected $user;

    public function __construct (User $user, $config=[])
    {
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * API endpoint for place
     */
    public static function apiEndPoint () {
        return 'seasons';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant.season';
    }

    public function rules()
    {
        return [
            [['seasonCode', 'season_code', 'seasonName', 'season_name', 'isActive', 'is_active'], 'required'],
            [['description', 'creator', 'modifier'], 'default', 'value' => null],
            [['description', 'creator', 'modifier'], 'string'],
            [['creationTimestamp', 'modificationTimestamp'], 'safe'],
            [['is_void', 'isActive', 'is_active'], 'boolean'],
            [['seasonCode', 'season_code'], 'string', 'max' => 128],
            [['seasonName', 'season_name'], 'default', 'value' => null],
            [['seasonName', 'season_name'], 'string', 'max' => 256],
            [['seasonCode', 'season_code'], 'unique'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne($this->user->className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        return $this->hasOne($this->user->className(), ['id' => 'modifier_id']);
    }
    /**
     * Retrieves existing seasons in the database
     *
     * @return array collection of seasons in the database
     */
    public function getSeasons ()
    {
        $res = Yii::$app->api->getParsedResponse(
            'GET',
            'seasons',
            null,
            'forDataBrowser',
            true
        );
        $data = [];

        // Check if the call was successful and data is not empty
        if (
            $res['success'] &&
            isset($res['data']) &&
            !empty($res['data'])
        )
            $data = $res['data'];

        return $data;
    }

    /**
     * Returns an array data provider containing seasons info
     *
     * @return mixed data provider for seasons
     */
    public function getSeasonsArrayDataProvider ()
    {
        return new ArrayDataProvider([
            'allModels' => $this->getSeasons(),
            'key' => 'seasonDbId',
            'sort' => [
                'attributes' => [
                    'seasonDbId',
                    'seasonCode',
                    'seasonName',
                    'description',
                    'isActive',
                    'creator',
                    'modifier',
                    'creationTimestamp',
                    'modificationTimestamp'
                ],
                'defaultOrder' => [
                    'seasonDbId'=>SORT_DESC
                ]
            ],
            'id' => 'season-data-provider'
        ]);
    }

    /**
     * Retrieves an existing season in the database
     * 
     * @param id unique season identifier
     * 
     * @return array collection of seasons in the database
     */
    public function getSeason ($id)
    {
        $res = Yii::$app->api->getParsedResponse(
            'GET',
            'seasons/' . $id
        );
        $data = null;

        // Check if it has result and retrieve menu data of the user
        if (
            $res['success'] &&
            isset($res['data']) &&
            !empty($res['data'])
        )
            $data = $res['data'][0];

        return $data;
    }

    /**
     * Retrieves existing season in the database and
     * return an array with the ID
     * as the key and program name as the value.
     * 
     * @return array collection of programs in the database
     */
    public function searchSeasons ($params)
    {
        $res = Yii::$app->api->getParsedResponse(
            'POST',
            'seasons-search',
            json_encode($params),
            'forDataBrowser',
            true
        );
        $data = [];

        if (
            $res['success'] &&
            isset($res['data']) &&
            !empty($res['data'])
        )
            $data = $res['data'];

        return $data;
    }

    /**
     * Set creator before validate
     * 
     * @return boolean
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {

            $currentTimestamp = new Expression('NOW()');

            $userId = $this->user->getUserId();

            if ($this->isNewRecord) {
                $this->creation_timestamp = $currentTimestamp;
                $this->creator_id = $userId;
            } else {
                $this->modification_timestamp = $currentTimestamp;
                $this->modifier_id = $userId;
            }
            return true;
        }
        return false;
    }

    /**
     * Check whether season is already used in a study
     * If yes, return true, else return false
     *
     * @param $id integer season identifier
     * 
     * @return $result boolean whether used in study or not
     */
    public static function checkIfUsedInStudy ($id)
    {
        $params = [
            'fields' => 'experiment.id | experiment.season_id',
            'season_id' => strval($id)
        ];

        $res = Yii::$app->api->getParsedResponse(
            'POST',
            'experiments-search',
            json_encode($params)
        );

        if (
            $res['success'] &&
            isset($res['data']) &&
            !empty($res['data'])
        )
            return true;

        return false;
    }

    /**
     * Get season attribute by season id
     * 
     * @param $id integer season identifier
     * 
     * @return $season text season attribute value
     */
    public function getRecordByIdAttribute ($id, $attribute = 'abbrev', $table = 'season')
    {
        $value = '';
        if ($table == 'place') {
            $sql = "select
                    case when display_name is null then name else display_name end
                from
                    master.{$table}
                where 
                    id = {$id}";
        } else {
            $sql = "select
                    {$attribute}
                from
                    master.{$table}
                where 
                    id = {$id}";
        }

        $result = Yii::$app->db->CreateCommand($sql)->queryColumn();

        if (!empty($result) && isset($result[0])) {
            $value = $result[0];
        }

        return $value;
    }

    /**
     * Process season parameters
     *
     * @param $params object unprocessed parameters
     * 
     * @return $processedParams object containing record(s) of processed params
     */
    public function processSeasonParams ($seasonParams, $method=null)
    {
        $processedParams = [
            'isActive' => 'false'
        ];

        // Re-assign values to camelCase fields...
        // ...so that API can recognize the key-value pairs
        foreach ($seasonParams as $key => $value) {
            if ($key == 'season_code') {
                $processedParams['seasonCode'] = $value;
            } else if ($key == 'season_name') {
                $processedParams['seasonName'] = $value;
            } else if ($key == 'is_active') {
                $processedParams['isActive'] = ($value == 'on') ?
                    'true' : 'false';
            } else {
                $processedParams[$key] = $value;
            }
        }

        if ($method === 'POST') {
            // This is the required structure of the API request body
            $processedParams = [
                'records' => [$processedParams]
            ];    
        }

        return $processedParams;
    }

    /**
     * Create season
     *
     * @param $params object POST parameters
     * 
     * @return boolean
     */
    public function createSeason ($params)
    {
        $res = Yii::$app->api->getParsedResponse(
            'POST',
            'seasons',
            json_encode($params)
        );

        if (
            isset($res['success']) &&
            $res['success']
        )
            return true;

        return false;
    }

    /**
     * Update season information
     *
     * @param $id integer season identifier
     * @param $params object PUT parameters
     * 
     * @return boolean
     */
    public function updateSeason ($id, $params)
    {
        $res = Yii::$app->api->getParsedResponse(
            'PUT',
            'seasons/' . $id,
            json_encode($params)
        );

        if (
            isset($res['success']) &&
            $res['success']
        )
            return true;

        return false;
    }

    /**
     * Delete a season
     *
     * @param $id integer season identifier
     * 
     * @return boolean
     */
    public function deleteSeason ($id)
    {
        $res = Yii::$app->api->getParsedResponse(
            'DELETE',
            'seasons/' . $id
        );

        if (
            isset($res['success']) &&
            $res['success']
        )
            return true;

        return false;
    }

    /**
     * Checks if a given season code already exists in the database
     * 
     * @param $seasonCode string
     * @param $seasonDbId integer season id
     * 
     * @return boolean
     * 
     */
    public function isSeasonCodeExisting ($seasonCode, $action, $seasonDbId =null)
    {
        $params = ['seasonCode' => $seasonCode];
        $res = Yii::$app->api->getParsedResponse(
            'POST',
            'seasons-search',
            json_encode($params),
            'forDataBrowser&limit=1',
            true
        );
        $data = [];

        // Check if has result and retrieve data of the seasons
        if (
            $res['success'] &&
            isset($res['data']) &&
            !empty($res['data'])
        )   
            $data = $res['data'];

        // Check if $seasonCode matches...
        // ...with exactly one of the $data's seasonCode
        foreach ($data as $info) {
            if ( // For Update
                $action == 'update' &&
                $seasonDbId != null &&
                $info['seasonDbId'] != $seasonDbId &&
                $info['seasonCode'] == $seasonCode
            )
                return true;
            else if ( // For Create
                $action == 'create' &&
                $info['seasonCode'] == $seasonCode
            )
                return true;
        }

        return false;   
    }
}
