<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational.experiment_data".
 *
 * @property int $id Identifier of the record within the table
 * @property int $experiment_id Refers to the experiment where the data is being attributed to
 * @property int $variable_id Reference to the variable that describes the value of the data
 * @property string $value Value of the data
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ExperimentData extends BaseModel {
    
    /**
    * Set api endpoint
    */
    public static function apiEndPoint() {
        return 'experiment-data';
    }


    /**
     * Retrieves an existing experiment data in the database
     * @param $id integer experiment identifier 
     * @param $filter object contains the filter parameters
     * @return array experiment data record in the database
    */
    public static function getExperimentData($id, $filter = null){
        $method = 'POST';
        $path = 'experiments/'.$id.'/data-search';
        
        if(!empty($filter)){
          $data = Yii::$app->api->getResponse($method, $path, json_encode($filter));
        }else{
          $data = Yii::$app->api->getResponse($method, $path);
        }
        
        $result = null;
         
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0]['data'])){
          $result = $data['body']['result']['data'][0]['data'];
        }
        return $result;
      }


    /**
     * Update experiment's data
     * @param $id integer experiment ID
     * @param $params array of experiment data to update
     * @return $result boolean status of the saving
    */
    public static function updateExperimentData($id, $params){
        $method = 'PUT';
        $path = 'experiment-data/'.$id;
        
        $data = Yii::$app->api->getResponse($method, $path, json_encode($params));
        
        $result = ((isset($data['status'])) && $data['status'] == 200) ? true : false;
        
        $errorMsg = '';
        if(!$result){
           $errorMsg = $data['body']['metadata']['status'][0]['message'];
        }

        return ['status'=>$result, 'error'=>$errorMsg];
    }

    public static function getExperimentDataRecord($id, $params){
      $method = 'POST';
      $apiEndpoint = 'experiments/'.$id.'/data-search';
      $data = Yii::$app->api->getResponse($method, $apiEndpoint, json_encode($params), null, true);
      $result = null;
      
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
        if(isset($data['body']['result']['data'][0]['data'][0])){
          $result = $data['body']['result']['data'][0]['data'][0];
        }
      }
      
      return $result;
    }

   /**
   * Search All experiment data records of the experiment
   * @param interger $id Experiment ID
   * @param array $param Request body parameter
   * @return array $result Array of experiment data records
   */
    public static function searchAllRecords($id, $params=null){
      $method = 'POST';
      $apiEndpoint = 'experiments/'.$id.'/data-search';
      
      if(!empty($params)){
        $data = Yii::$app->api->getResponse($method, $apiEndpoint, json_encode($params), null, true);
      }else{
        $data = Yii::$app->api->getResponse($method, $apiEndpoint, null, null, true);
      }
      
      $result = null;
      // Check if it has a result, then retrieve menu data
      if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
        $result = $data['body']['result']['data'][0]['data'];
      }
      return $result;
    }


}
