<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenant.stage".
 *
 */
class Stage
{
    /**
     * Retrieves existing stages in the database
     * 
     * @param $params array filter parameters
     * @return array collection of stages in the database
     * 
     */
    public static function find($params)
    {
        $data = Yii::$app->api->getResponse('POST', 'stages-search',json_encode($params),null,true);

        // check if has result and stages are retrieved
        if (isset($data['body']['result']['data'][0])) $data = $data['body']['result']['data'][0];
        else $data = null;

        return $data;
    }
}
