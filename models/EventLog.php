<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use yii\widgets\DetailView;

class EventLog extends BaseModel {

    /**
     * Load and initialize the search model object
     * Checks if the provider searchModel value is a valid class
     * @return mixed initiated model object
     */
    private static function loadSearchModel($modelName) {

        return \yii\di\Instance::ensure(
            'app\models\\'.$modelName,
            \yii\db\ActiveRecord::class
        );
    }
}