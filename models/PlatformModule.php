<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.module".
 *
 * @property int $id Primary key of the record in the table
 * @property string $abbrev Short name identifier or abbreviation of the module
 * @property string $name Name identifier of the module
 * @property string $description Description of the module
 * @property string $module_id Module ID
 * @property string $controller_id Controller ID
 * @property string $action_id Action ID
 * @property string $remarks Additional details about the module
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 * @property array $params Additional URL params
 * @property int $program_id
 */
class PlatformModule extends ModuleBase
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.module';
    }

    /**
     * Retrieves an existing module in the database
     * @param $id integer module id
     * @return array module's record in the database
     */
    public static function getModule($id){
        $method = 'GET';
        $path = 'modules'.'/'.$id;
        
        $data = Yii::$app->api->getResponse($method, $path);
        $result = [];
         
        // Check if it has a result, then retrieve menu data
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
          $result = $data['body']['result']['data'][0];
        }
        return $result;
    }
}
