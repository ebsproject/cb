<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;
use ChromePhp;
use yii\data\ActiveDataProvider;

/**
 * This is the ActiveQuery class for [[SeasonSearch]].
 *
 * @see SeasonSearch
 */
class EntryListSearch extends EntryList     
{

    public $parentage;
    public $product_name;
    public $generation;
    public $remarks;
    public $availability;
    public $seed_source;
    public $source_entry;
    public $source_study;
    public $parent_type;
    public $hybrid_role;


    public function rules()
    {
        return [
            [['experiment_id', 'entry_status', 'product_id', 'entlistno', 'entry_type', 'author_id', 'creator_id'], 'required'],
            [['availability', 'remarks', 'product_name', 'parentage', 'generation', 'experiment_id', 'product_id', 'entlistno', 'author_id', 'creator_id', 'modifier_id', 'seed_storage_log_id', 'gid','seed_source','source_study','source_entry','parent_type','hybrid_role'], 'default', 'value' => null],
            [['experiment_id', 'product_id', 'entlistno', 'author_id', 'creator_id', 'modifier_id', 'seed_storage_log_id', 'gid'], 'integer'],
            [['entry_class', 'entry_status', 'entry_type', 'remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
        ];
    }

   /**
     * Search model for study
     *
     * @param $params array list of query parameters
     * @param integer $experimentId record id of the experiment
     * @param string $exptProgram programa of the experiment
     * @param string $variableNeed specific variable needed
     * @return $dataProvider array product browser dataprovider
     */
    public function search($params, $experimentId, $exptProgram, $variableNeed=null, $requiredFields = NULL)
    {
        
        $condStr = '';
        $this->load($params);
        
        $requiredCols = [];
        $reqKeyCols = [];
        
        if($requiredFields != NULL){
             foreach ($requiredFields as $key => $value) {
            
                $varRecord = Variable::find()->where(['abbrev'=> strtoupper($value['variable_abbrev']), 'is_void'=> false])->one();
                $column = strtolower($value['variable_abbrev']);
                $checkColumn = Experiment::checkIfColumnExist('operational','entry_list',$column);    

                if(!$checkColumn){
                    $requiredCols[] = $column;
                    $reqKeyCols[$column] = $varRecord->id;
                }
            }
        }
       
        
        
        foreach ($params['EntryListSearch'] as $key => $value) {
         
            if(isset($value) && $value != ''){

                if(in_array($key, $requiredCols)){

                    if(strtolower($key) == 'parent_type'){
                        $condStr = empty($condStr) ? "cast(t.".$key." as text) = '". $value ."'"  : $condStr. " and cast(t.".$key." as text) = '". $value ."'";
                    }else{
                        $condStr = empty($condStr) ? "cast(t.".$key." as text) ilike '%". $value ."%'"  : $condStr. " and cast(t.".$key." as text) ilike '%". $value ."%'";
                    }  
                }else if($key == 'creator'){
                    $condStr = empty($condStr) ? " cast(creator.display_name as text) ilike '%". $value ."%'" : $condStr. " and cast(creator.display_name as text) ilike '%". $value ."%'";
                } elseif($key == 'modifier'){
                    $condStr = empty($condStr) ? "  cast(modifier.display_name as text) ilike '%". $value ."%'" : $condStr . " and cast(modifier.display_name as text) ilike '%". $value ."%'";
                }
                elseif($key == 'author'){
                    $condStr = empty($condStr) ? " cast(author.display_name as text) ilike '%". $value ."%'" : $condStr . " and cast(author.display_name as text) ilike '%". $value ."%'";
                }
                else if($key == 'parentage'){
                    $condStr = empty($condStr) ? " cast(t.parentage as text) ilike '%". $value ."%'" : $condStr . " and cast(t.parentage as text) ilike '%". $value ."%'";
                }
                else if($key == 'generation'){
                    $condStr = empty($condStr) ? " cast(t.generation as text) ilike '%". $value ."%'" : $condStr . " and cast(t.generation as text) ilike '%". $value ."%'";
                }
                else if($key == 'product_name'){
                    $condStr = empty($condStr) ? " cast(t.product_name as text) ilike '%". $value ."%'" : $condStr . " and cast(t.product_name as text) ilike '%". $value ."%'";
                }
                else if($key == 'gid'){
                    $condStr = empty($condStr) ?  "t.gid::text = '".$value."'" : $condStr. " and t.gid::text = '".$value."'";
                }
                else if($key == 'seed_source'){
                     $condStr = empty($condStr) ? "cast(t.seed_source as text) ilike '%". $value ."%'"  : $condStr. " and cast(t.seed_source as text) ilike '%". $value ."%'";
                }
                else if($key == 'source_study'){
                    $condStr = empty($condStr) ? " t.source_study::text = '".$value."'" : $condStr . " and t.source_study::text = '".$value."'";
                }
                else if($key == 'source_entry'){
                    $condStr = empty($condStr) ? " t.source_entry::text = '".$value."'" : $condStr . " and t.source_entry::text = '".$value."'";
                }
                else if($key == 'creation_timestamp'){
                    $dayBegin = $value.' 00:00:00.000000';
                    $dayEnd = $value.' 23:59:59.999999';
                    $condStr = empty($condStr) ? " (t.creation_timestamp between '$dayBegin' and '$dayEnd')" : $condStr . " and (t.creation_timestamp between '$dayBegin' and '$dayEnd')";
                }else if($key == 'modification_timestamp'){
                    $dayBegin = $value.' 00:00:00.000000';
                    $dayEnd = $value.' 23:59:59.999999';
                    $condStr = empty($condStr) ? " (t.modification_timestamp between '$dayBegin' and '$dayEnd')" : $condStr . " and (t.modification_timestamp between '$dayBegin' and '$dayEnd')";
                } 
                else if ($key == 'availability'){
                    $availabilityQuery = $value;
                }
                else{
                    $condStr = empty($condStr) ? " cast(t." .$key." as text) ilike '%". $value ."%'" : $condStr . " and cast(t." .$key." as text) ilike '%". $value ."%'";
                }
            }
        }
        if($variableNeed == NULL){
            return EntryList::getDataProvider($condStr, $experimentId, null, isset($availabilityQuery)? $availabilityQuery : null, $exptProgram,NULL, $requiredFields);
        } else{
            return EntryList::getDataProvider($condStr, $experimentId, null, isset($availabilityQuery)? $availabilityQuery : null, $exptProgram, $variableNeed, $requiredFields);
        }
        
    }

}