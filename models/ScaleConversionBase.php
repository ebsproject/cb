<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master.scale_conversion".
 *
 * @property int $id Identifier of the record within the table
 * @property int $source_unit_id Source/base scale unit of measurement
 * @property int $target_unit_id Target/output scale unit of measurement
 * @property double $conversion_value Value to multiply to the source unit to arrive to the target unit
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property array $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 */
class ScaleConversionBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master.scale_conversion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source_unit_id', 'target_unit_id', 'conversion_value', 'creator_id'], 'required'],
            [['source_unit_id', 'target_unit_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['source_unit_id', 'target_unit_id', 'creator_id', 'modifier_id'], 'integer'],
            [['conversion_value'], 'number'],
            [['remarks', 'notes', 'record_uuid'], 'string'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['source_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterScaleValue::className(), 'targetAttribute' => ['source_unit_id' => 'id']],
            [['target_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterScaleValue::className(), 'targetAttribute' => ['target_unit_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_unit_id' => 'Source Unit ID',
            'target_unit_id' => 'Target Unit ID',
            'conversion_value' => 'Conversion Value',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
        ];
    }
}
