<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\User;
use app\modules\experimentCreation\models\BrowserModel;

/**
 * Model class for dynamic Entry data
 */
class EntrySearch extends Entry
{

    private $data;
    private $dynamicRules = [];

    public function __get($varName)
    {
        if(isset($this->data[$varName])){
            return $this->data[$varName];
        }
    }

    public function __set($varName, $value)
    {
        $this->data[$varName] = $value;
    }

    public function rules()
    {
        // Retrieve parent's rules and append them to this class's rules
        $validationRules = parent::rules();
        $this->dynamicRules = array_merge($this->dynamicRules, $validationRules);

        return $this->dynamicRules;
    }

    /**
     * Search model for entry
     * 
     * @param array $params list of current search parameters
     * @param array $configData Configuration record
     * @param array $addtlFilter additional filter conditions
     * @param boolen $noPagination No pagination flag
     * @param string $source view source of the action to perform
     * @param String $pageParams data browser parameters
     * @param Interger $currentPage data browser current page number
     * @return array $dataProvider entry list browser data provider
     */
    public function search($params = null, $configData = null, $addtlFilter = null, $noPagination = false, $source=null, $pageParams = '',$currentPage = null)
    {
        $dataProvider = [];

        if (isset($params)) {

            extract($params);

            $filters = isset($EntrySearch) ? \Yii::$container->get('app\modules\experimentCreation\models\BrowserModel')->formatFilters($EntrySearch) : [];
            foreach($filters as $key => $value) {
                // process '(not set)' filter for api call
                if($value == '(not set)') {
                    $filters[$key] = 'null';
                }
            }
            if (!empty($addtlFilter) && empty($filters)) {
                $filters = $addtlFilter;
            }
            
            Yii::$app->session->set('experiment-entry-list-filters-' . $id, $filters);

            $results = Entry::getDataProvider($id, $configData, $filters, $noPagination, $source, $pageParams, $currentPage);
            $this->dynamicRules = [[$results['attributes'], 'safe']];

            $dataProvider = $results['dataProvider'];
            //store entry ids
            $userModel = new User();
            $userId = $userModel->getUserId();
            Yii::$app->session->set("ec_$userId"."_filtered_entry_ids_$id", $results['entryDbIds']);

            //For planting arrangement
            Yii::$app->session->set("ec"."_filtered_entry_ids_$id", $results['entryDbIds']);

            if (!($this->load($params) && $this->validate())) {
                //validate results
                return $dataProvider;
            }
        }

        //no results found
        return $dataProvider;
    }

    /**
     * Retrieves items for filter column of entry list browser
     *
     * @param integer $entryListId entry list identifier
     * @param array $params list of search parameters from Select2 widget
     * @param string $column name of column whose values will be retrieved
     * @param integer $page page number of the values that will be retrieved
     * @param boolean $designTab check if the filter will be use for design tab
     *
     * @return array $output data for the Select2 widget
     */
    public static function getEntriesFilter($entryListId, $params, $column, $page = 1, $designTab = false) {
        // build filters for api call
        $fieldColumn = '';
        $distinctColumn = '';

        switch($column) {
            case 'entryNumber':
                $distinctColumn = 'entry_number';
                $fieldColumn = 'entry.'.$distinctColumn;
                break;
            case 'seedCode':
                $distinctColumn = 'seed_code';
                $fieldColumn = 'seed.'.$distinctColumn;
                break;
            case 'seedName':
                $distinctColumn = 'seed_name';
                $fieldColumn = 'seed.'.$distinctColumn;
                break;
            case 'designation':
                $distinctColumn = 'designation';
                $fieldColumn = 'germplasm.'.$distinctColumn;
                break;
            case 'germplasmCode':
                $distinctColumn = 'germplasm_code';
                $fieldColumn = 'germplasm.'.$distinctColumn;
                break;
            case 'parentage':
                $distinctColumn = 'parentage';
                $fieldColumn = 'germplasm.'.$distinctColumn;
                break;
            case 'generation':
                $distinctColumn = 'generation';
                $fieldColumn = 'germplasm.'.$distinctColumn;
                break;
            case 'germplasmState':
                $distinctColumn = 'germplasm_state';
                $fieldColumn = 'germplasm.'.$distinctColumn;
                break;
            case 'entryClass':
                $distinctColumn = 'entry_class';
                $fieldColumn = 'entry.'.$distinctColumn;
                break;
            case 'description':
                $distinctColumn = 'description';
                $fieldColumn = 'entry.'.$distinctColumn;
                break;
            case 'entryType':
                $distinctColumn = 'entry_type';
                $fieldColumn = 'entry.'.$distinctColumn;
                break;
            case 'entryRole':
            case 'parentRole':
                $distinctColumn = 'entry_role';
                $fieldColumn = 'entry.'.$distinctColumn;
                break;
            case 'packageLabel':
                $distinctColumn = 'package_label';
                $fieldColumn = 'package.'.$distinctColumn;
                break;
            case 'packageQuantity':
                $distinctColumn = 'package_quantity';
                $fieldColumn = 'package.'.$distinctColumn;
                break;
            case 'packageUnit':
                $distinctColumn = 'package_unit';
                $fieldColumn = 'package.'.$distinctColumn;
                break;
        }

        $filters = [
            'fields'=> 'entry.entry_list_id|'.$fieldColumn,
            'distinctOn'=> $distinctColumn,
            'entry_list_id'=> "equals ".$entryListId
        ];
        if(isset($params[$column])) {
            $filters[$distinctColumn] = '%'.$params[$column].'%';
        }

        $response = Yii::$app->api->getResponse('POST', 'entries-search', json_encode($filters), 'limit=5&page='.$page);

        $totalPages = $response['body']['metadata']['pagination']['totalPages'];
        $data = $response['body']['result']['data'];

        // add (not set) choice
        $items = [];
        if($page == 1 && !$designTab) {    // Add (not set) option as first option
            $items[] = [
                'id' => '(not set)',
                'text' => '(not set)'
            ];
        }
        foreach($data as $d) {
            $items[] = [
                'id' => $d[$distinctColumn],
                'text' => $d[$distinctColumn]
            ];
        }
        $output['items'] = $items;
        $output['more'] = $page < $totalPages? true: false; // for lazy loading, indicates if there are "more pages" that can be loaded

        return $output;

    }

    // required function for ManageSortsWidget implementation
    public function getColumnNameAndDatatypes() {
        $experimentDbId = $_GET && $_GET['id'] ? $_GET['id'] : null;
        $sortOptions = [
            'entryNumber' => 'Integer',
            'seedName' => 'String',
            'packageLabel' => 'String',
            'designation' => 'String',
            'germplasmCode' => 'String',
            'parentage' => 'String',
            'generation' => 'String',
            'description' => 'String',
            'germplasmState' => 'String'
        ];

        $additionalOptions = [];
        if($experimentDbId){
            // get experiment data
            $method = 'POST';
            $path = 'experiments-search';
            $params = [ 'experimentDbId' => ''.$experimentDbId ];

            $experimentData = Yii::$app->api->getResponse($method, $path, json_encode($params), '', false);

            // get experiment type from experiment data 
            $experimentType = !empty($experimentData['body']['result']['data']) ? $experimentData['body']['result']['data'][0]['experimentType'] : '';

            if($experimentType && ($experimentType == 'Breeding Trial' || $experimentType == 'Observation')){
                $additionalOptions = [
                    'entryType' => 'String',
                    'entryClass' => 'String'
                ];
            }
            else if($experimentType && ($experimentType == 'Cross Parent Nursery' || $experimentType == 'Intentional Crossing Nursery' || $experimentType == 'Generation Nursery')){
                $additionalOptions = [
                    'parentRole'=> 'String'
                ];
            }
        }
        
        if( !empty($additionalOptions) ){
            $sortOptions = array_merge($sortOptions,$additionalOptions);
        } 
        
        return $sortOptions;
    }
}
