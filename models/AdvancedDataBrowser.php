<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

// Import Yii
use Yii;
// Import components
// Import basic models
use app\dataproviders\ArrayDataProvider;
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;
// Import other models
use app\controllers\Dashboard;
use app\models\User;
use app\models\OccurrenceSearch;

/**
 * This is the model class for API resource
 */

class AdvancedDataBrowser extends AdvancedDataBrowserProperties {

    public $modelName = 'AdvancedDataBrowser';
    public $rulesArray;
    public $defaultFilter = null;
    public $defaultSort = null;

    /**
     * Model constructor
     */
    public function __construct(
        public UserDashboardConfig $userDashboardConfig,
        public DataBrowserConfiguration $dataBrowserConfiguration,
        public Dashboard $dashboard,
        public User $user,
        public OccurrenceSearch $occurrenceSearch
        )
    {
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                $this->rulesArray,
                'safe'
            ]
        ];
    }

    /**
     * Calls the appropriate search method depending on the browser id
     *
     * @param Array $params list of parameters from Dynagrid
     * @param String $endpoint API endpoint that is used to get data for the browser
     * @param String $browserId browser identifier
     *
     * @return ArrayDataProvider $dataProvider
     */
    public function search($params, $endpoint, $browserId) {
        $dataProvider = null;

        switch ($browserId) {
            case 'dynagrid-em-occurrences':
                $dataProvider = $this->emSearch($params, $endpoint, $browserId);
                break;
            case 'dynagrid-germplasm-grid':
                $dataProvider = $this->simpleADBSearch($params, $endpoint, $browserId);
                break;
            default:    // sample
                $dataProvider = $this->simpleADBSearch($params, $endpoint, $browserId);
                break;
        }

        return $dataProvider;
    }

    /**
     * Search method for Experiment Manager
     *
     * @param Array $params list of parameters from Dynagrid
     * @param String $endpoint API endpoint that is used to get data for the browser
     * @param String $browserId browser identifier
     *
     * @return ArrayDataProvider $dataProvider
     */
    public function emSearch($params, $endpoint, $browserId) {
        // get dashboard filters
        $dashboardFilters = $this->dashboard->getFilters();
        $defaultFilters = (array) $dashboardFilters;
        $userId = $this->user->getUserId();
        $session = Yii::$app->session;
        $occurrenceTextFilters = [];

        if($session->get('dashboard-occurrence-text-filters') == NULL){

            $searchParams = [
                'userDbId' => (string)$userId,
                'dataBrowserDbId' => 'occurrences-grid',
                'type' => 'filter'
            ];

            // check if configuration already exists
            $config = $this->dataBrowserConfiguration->searchAll($searchParams);
            $occurrenceFilters = [];
            if (isset($config['totalCount']) && $config['totalCount'] > 0) {
                $occurrenceFilters = $config['data'][0]['data'];
            }
            // get text filters
            $searchParams = [
                'userDbId' => (string)$userId,
                'dataBrowserDbId' => 'occurrences-text-grid',
                'type' => 'filter'
            ];

            // check if configuration already exists
            $config = $this->dataBrowserConfiguration->searchAll($searchParams);
            $occurrenceTextFilters = [];
            if (isset($config['totalCount']) && $config['totalCount'] > 0) {
                $occurrenceTextFilters = $config['data'][0]['data'];
            }
        } else {

            $occurrenceFilters = $session->get('dashboard-occurrence-filters');
            $occurrenceTextFilters = $session->get('dashboard-occurrence-text-filters');
        }

        $defaultFilters = array_merge($defaultFilters, $occurrenceFilters);

        // retrieve browser dataProvider and export dataProvider
        $data = $this->occurrenceSearch->searchOccurrence($params, $defaultFilters);
        $dataProvider = $data[0];

        return $dataProvider;
    }

    /**
     * Search method for data browsers that have simple search() like Germplasm Search
     *
     * @param Array $params list of parameters from Dynagrid
     * @param String $endpoint API endpoint that is used to get data for the browser
     * @param String $browserId browser identifier
     *
     * @return ArrayDataProvider $dataProvider
     */
    public function simpleADBSearch($params, $endpoint, $browserId) {
        $dataProviderId = $browserId . '-adb-dp';

        $paramLimit = 'limit=' . $this->userDashboardConfig->getDefaultPageSizePreferences();

        $paramPage = '';
        $pageCount = 1;
        if (isset($_GET[$dataProviderId.'-page'])){
            $pageCount = $_GET[$dataProviderId.'-page'];
            $paramPage = '&page=' . $pageCount;
        } else if (isset($_GET['page'])) {
            $paramPage = '&page=' . $_GET['page'];
            $pageCount = $_GET['page'];
        }

        $sort = $this->formatParamsForSort($browserId);
        $paramSort = $sort['paramSort'];
        $defaultSort = $sort['defaultSort'];

        $filters = $this->formatParamsForFilter($params, $browserId);

        $response = Yii::$app->api->getResponse('POST', $endpoint.'?'.$paramLimit.$paramPage.$paramSort, json_encode($filters));

        if ($pageCount != 1 && $response['body']['metadata']['pagination']['totalCount'] == 0) {
            $response = Yii::$app->api->getResponse('POST', $endpoint.'?'.$paramLimit.$paramSort.'&page=1', json_encode($filters));
            $_GET[$dataProviderId.'-page'] = 1; // return browser to page 1
        }

        $data = $response['body']['result']['data'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'totalCount' => $response['body']['metadata']['pagination']['totalCount'],
            'restified' => true,
            'sort' => [
                'attributes' => $this->rulesArray,
                'defaultOrder' => $defaultSort,
            ],
            'id' => $dataProviderId
        ]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * Calls the appropriate sort formatter depending on the browser id
     * if there is a special need for it since the sort formatting is
     * generally the same for most (if not all) of the browsers
     *
     * @param Array $params list of parameters from Dynagrid
     * @param String $browserId browser identifier
     *
     * @return Array $sort sort parameter for API call and default sort for Data Provider
     */
    public function formatParamsForSort($browserId) {
        $sort = [
            'paramSort' => '',
            'defaultSort' => null
        ];

        if(isset($_GET['sort'])) {
            switch ($browserId) {
                case 'dynagrid-em-occurrences': // specific formatting for a specific browser id (if needed)
                    # code...
                    break;
                default:    // default - this handles single and multiple sort
                    $defaultSort = [];  // will be rebuilt so it shows up properly in the browser headers
                    $paramSort = '&sort=';
                    $sortParams = $_GET['sort'];
                    $sortParams = explode('|', $sortParams);
                    $countParam = count($sortParams);
                    $currParam = 1;

                    // Convert columns from ascending/descending format of DataProvider to ascending/descending format of API
                    foreach($sortParams as $column) {
                        if($column[0] == '-') {    // descending
                            $defaultSort[substr($column, 1)] = SORT_DESC;
                            $paramSort = $paramSort . substr($column, 1) . ':desc';
                        } else {    // ascending
                            $defaultSort[$column] = SORT_ASC;
                            $paramSort = $paramSort . $column;
                        }
                        if($currParam < $countParam) {
                            $paramSort = $paramSort . '|';
                        }
                        $currParam += 1;
                    }

                    $sort['paramSort'] = $paramSort;
                    $sort['defaultSort'] = $defaultSort;
                    break;
            }
        } else {    // Build sort params from the default sort of the Data Provider
            $defaultSort = $this->defaultSort;
            if(!is_null($defaultSort) && !empty($defaultSort)) {
                $paramSort = '&sort=';
                $count = 1;

                foreach($defaultSort as $column => $sortOrder) {
                    if($count > 1) $paramSort = $paramSort . '|';
                    $count += 1;

                    if($sortOrder == SORT_ASC) $paramSort = $paramSort . $column;
                    else $paramSort = $paramSort . $column . ':desc';
                }

                $sort['paramSort'] = $paramSort;
                $sort['defaultSort'] = $defaultSort;
            }
        }

        return $sort;
    }

    /**
     * Calls the appropriate filter formatter depending on the browser id
     *
     * @param Array $params list of parameters from Dynagrid
     * @param String $browserId browser identifier
     *
     * @return String/Object $filters filters for API call
     */
    public function formatParamsForFilter($params, $browserId) {
        $filters = '';

        switch ($browserId) {
            case 'dynagrid-em-occurrences':
                # code...
                break;
            case 'dynagrid-germplasm-grid':
                $filters = $this->useGSBrowserFormat($params);
                break;
            default:    // sample
                $filters = $this->useGSBrowserFormat($params);
                break;
        }

        return $filters;
    }

    /**
     * Formats the params based on the Format of the GS Browser
     *
     * @param Array $params list of parameters from Dynagrid
     *
     * @return String/Object $filters filters for API call
     */
    public function useGSBrowserFormat($params) {
        $filters = '';

        $defaultFilter = $this->defaultFilter;
        if(!is_null($defaultFilter) && !empty($defaultFilter)) {
            $filters = $defaultFilter;

            if (isset($filters['names'])) {
                if (isset($filters['names']['equals'])) {
                    $filters['names'] = $filters['names']['equals'];
                } else {
                    unset($filters['names']);
                }
            }
        }

        if(isset($params[$this->modelName])) {
            $tempFilters = [];
            foreach ($params[$this->modelName] as $key => $value) {
                if(!empty($value) && isset($value)){
                    if($key == 'otherNames') {
                        $tempFilters[$key] = [
                            'like' => '%'.$value.'%'
                        ];
                    } else {
                        $tempFilters[$key] = [
                            'like' => $value
                        ];
                    }
                }
            }
            if (!empty($tempFilters)) {
                if (!empty($filters)) {
                    $filters['__browser_filters__'] = $tempFilters;
                } else {
                    $filters = $tempFilters;
                }
            }
        }

        return $filters;
    }
}