<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "operational_data_terminal.randomization_transaction".
 *
 * @property int $id Primary key of the record in the table
 * @property string $type Type of transaction 
 * @property int $study_id ID of the study in the operational.study table.
 * @property string $input_file JSON format values of the inputted parameters.
 * @property int $actor_id User who performed the randomization
 * @property string $start_action_timestamp Timestamp when the randomization started;
 * @property string $end_action_timestamp Timestamp when the randomization ended; 
 * @property bool $is_successful Whether randomization is successful.
 * @property string $remarks Additional details
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not.
 * @property string $generated_input Generated input file to be used by the Web service.
 * @property string $randomization_output Generated results file of the randomization.
 * @property double $elapsed_time Duration of the randomization process in the Web Service.
 * @property string $data_results Generated data results from the randomization
 * @property int $plan_id
 */
class RandomizationTransactionBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operational_data_terminal.randomization_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'input_file', 'remarks', 'notes', 'generated_input', 'randomization_output', 'data_results'], 'string'],
            [['study_id', 'actor_id', 'start_action_timestamp', 'end_action_timestamp'], 'required'],
            [['study_id', 'actor_id', 'creator_id', 'modifier_id', 'plan_id'], 'default', 'value' => null],
            [['study_id', 'actor_id', 'creator_id', 'modifier_id', 'plan_id'], 'integer'],
            [['start_action_timestamp', 'end_action_timestamp', 'creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_successful', 'is_void'], 'boolean'],
            [['elapsed_time'], 'number']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'study_id' => 'Study ID',
            'input_file' => 'Input File',
            'actor_id' => 'Actor ID',
            'start_action_timestamp' => 'Start Action Timestamp',
            'end_action_timestamp' => 'End Action Timestamp',
            'is_successful' => 'Is Successful',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'generated_input' => 'Generated Input',
            'randomization_output' => 'Randomization Output',
            'elapsed_time' => 'Elapsed Time',
            'data_results' => 'Data Results',
            'plan_id' => 'Plan ID',
        ];
    }
}
