<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding. If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\dataproviders\ArrayDataProvider;
use app\controllers\Dashboard;
use app\controllers\BrowserController;
use app\models\LocationOccurrenceGroup;
use app\models\Variable;
use app\modules\location\controllers\MonitorController;
use app\interfaces\models\ILocation;

/**
 * Model class for table experiment.location
 */
class Location extends BaseModel implements ILocation
{
    public function __construct(
        public BrowserController $browserController,
        public Dashboard $dashboardController,
        public LocationOccurrenceGroup $locationOccurrenceGroup,
        public MonitorController $monitorController,
        public Variable $variable,
        $config = []
    )
    { parent::__construct($config); }

    /**
     * Retrieves the corresponding api endpoint for a class
     * Any class that extends this model should define the API endpoint if it does not fit the default definition
     * Default is converting the class name into lower case
     * eg, Lists becomes lists
     */
    public static function apiEndPoint() {
        return 'locations';
    }
           
    /**
     * Find Experiments in browser detail view catgory
     *
     * @return $results list of experiments
     */
    public function findCategories(){

        $method = 'POST';
        $path = 'locations-search';
        $sort = 'sort=locationName';
        $isOwnedCond = '';

        // default parameter, exclude draft locations
        $params["locationStatus"] = "not equals draft";

        // get dashboard filters
        $defaultFilters = Dashboard::getFilters();
        // include dashboard filters
        $dashboardFilters = (array) $defaultFilters;
        
        // format dashboard filters
        if(!empty($dashboardFilters)){
            foreach ($dashboardFilters as $key => $value) {
                if($key !== 'program_id'){
                    $attribute = str_replace('_i', 'DbI', $key);

                    // if display only owned experiments
                    if($key == 'owned' && $value == 'true') {
                        $isOwnedCond = '&isOwned';
                    }

                    if(!empty($value) && !in_array($key, ['owned'])) {
                        if(is_array($value)){
                            $value = implode('|', $value);
                        }
                        $params[$attribute] = (string)$value;
                    }
                }
            }
        }

        $params = empty($params) ? null : $params;

        $data = Yii::$app->api->getResponse($method, $path, json_encode($params), $sort . $isOwnedCond, true);
        
        $result = [];
        $totalCount = 0;
        // Check if it has a result, then retrieve retrieve experiments
        if (isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'])){
            $result = $data['body']['result']['data'];
            $totalCount = $data['body']['metadata']['pagination']['totalCount'];
        }

        // data to be displayed in view category details
        $attributes = [
            'locationCode' => 'Code',
            'site' => 'Site',
            'field' => 'Planting area',
            'stageCode' => 'Stage',
            'year' => 'Year',
            'seasonCode' => 'Season',
            'occurrenceCount' => 'No. of occurrences',
            'steward' => 'Location steward'
        ];

        // should always have id, text, data, total count and attributes
        return [
            'id' => 'locationDbId',
            'text' => 'locationName',
            'data' => $result,
            'totalCount' => $totalCount,
            'attributes' => $attributes
        ];


    }

    /**
     * Search for locations given search parameters
     *
     * @param Array $params list of search parameters
     * @return Array $data list of location records
     */
    public function searchRecords($params = null){
        
        $results = Yii::$app->api->getResponse('POST', 'locations-search', json_encode($params), null, true);

        $data = [];

        // check if successfully retrieved entries
        if (isset($results['status']) && $results['status'] == 200 && isset($results['body']['result']['data'])){
            $data = $results['body']['result']['data'];
        }

        return $data;
    }

    /**
     * Search for locations given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $selectedCategoryId integer selected category item
     * @return $data array list of occurrences given search parameters
     */
    public function browse($filter=null, $category=null, $selectedCategoryId=null, $page=null){

        $data = [];
        $totalCount = 0;
        $limit = 'limit=50';
        $page = !empty($page) ? '&page=' . $page : '';
        $sort = '&sort=locationDbId:desc';
        $isOwnedCond = '';


        // default parameter, exclude draft locations
        $params["locationStatus"] = "not equals draft";

        // if filter is set, build params
        if(!empty($filter)){
            // process filter for document
            $processedFilter = $this->browserController->processDocumentFilter($filter);
            
            $params["locationDocument"] = $processedFilter;

        }

        // get dashboard filters
        $defaultFilters = $this->dashboardController->getFilters();
        // include dashboard filters
        $dashboardFilters = (array) $defaultFilters;

        // format dashboard filters
        $dashboardFiltersFormatted = [];
        if(!empty($dashboardFilters)){
            foreach ($dashboardFilters as $key => $value) {
                if($key !== 'program_id'){
                    $attribute = str_replace('_i', 'DbI', $key);

                    // if display only owned experiments
                    if($key == 'owned' && $value == 'true') {
                        $isOwnedCond = '&isOwned';
                    }

                    if(!empty($value) && $key != 'owned') {
                        if(is_array($value)){
                            $value = implode('|', $value);
                        }
                        $dashboardFiltersFormatted[$attribute] = (string)$value;
                    }
                }
            }
        }

        $params = array_merge($params, $dashboardFiltersFormatted);

        $validCategory = in_array($category, ['experiment']);

        // if there is a selected category
        if(!empty($selectedCategoryId) && $selectedCategoryId != 'all' && $validCategory){

            // if category is experiment
            if($category == 'experiment'){
                // get locations based from selected experiment
                $locationDbIdCond = Location::getLocationsByExperimentId($selectedCategoryId);
            }

            if(!empty($locationDbIdCond)){
                foreach ($locationDbIdCond as $key => $value) {
                    $locationDbIdsArr[] = $value['locationDbId'];
                }

                $addtlParams['locationDbId'] = implode('|', $locationDbIdsArr);

            } else {
                $addtlParams['locationDbId'] = "0";
            }

            $params = !(empty($params)) ? array_merge($params, $addtlParams) : $addtlParams;

        }

        $params = empty($params) ? null : $params;

        $response = Yii::$app->api->getResponse('POST', 'locations-search', json_encode($params), $limit . $page . $sort . $isOwnedCond, false);

        // check if successfully retrieved occurrences
        if(isset($response['status']) && $response['status'] == 200 && isset($response['body']['result']['data'])){
            
            $data = (isset($response['body']['result']['data'])) ? $response['body']['result']['data'] : [];
            $totalCount = $response['body']['metadata']['pagination']['totalCount'];
        }

        $dataprovider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'locationDbId',
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'locationDbId'
                ],
                'defaultOrder' => [
                    'locationDbId' => SORT_DESC
                ]
            ]
        ]);

        return [
            'dataProvider' => $dataprovider,
            'totalCount' => $totalCount
        ];

    }

    /**
     * Retrieves locations given experiment ID
     *
     * @param integer $experimentId experiment identifier
     * @return array $locationsArr list of locations given experiment ID
     */
    public function getLocationsByExperimentId($experimentId){
        // get occurrences by experiment ID
        $experimentCond = [
            'experimentDbId' => $experimentId,
            'fields' => "occurrence.id as occurrenceDbId | occurrence.experiment_id as experimentDbId"
        ];

        $occurrenceModel = \Yii::$container->get('app\models\Occurrence');
        $occurrenceDbIds = $occurrenceModel->searchAll($experimentCond);
        $occurrenceDbCond = 0;
        $occurrenceDbIdsArr = [];

        // if there are occurrences retrieved
        if(isset($occurrenceDbIds['data']) && !empty($occurrenceDbIds['data'])){
            foreach ($occurrenceDbIds['data'] as $value) {
                $occurrenceDbIdsArr[] = $value['occurrenceDbId'];
            }

            $occurrenceDbCond = implode('|', $occurrenceDbIdsArr);
        }

        // build location occurrence goups condition
        $locOccGroupParams = [
            "occurrenceDbId" => (string)$occurrenceDbCond
        ];

        // retriebe locations by occurrenceDbIds
        $locationDbIds = $this->locationOccurrenceGroup->searchAll($locOccGroupParams);

        $locationsArr = [];
        // if there are locations retrieved
        if(isset($locationDbIds['data']) && !empty($locationDbIds['data'])){
            $locationsArr = $locationDbIds['data'];
        }

        return $locationsArr;
    }

    /**
     * Get location info for detail view
     *
     * @param $id integer location identifier
     * @return $data array location information
     */
    public function getDetailViewInfo($id){
        $params = [
            "locationDbId" => "equals $id"
        ];

        $data = Location::searchAll($params, 'limit=1&sort=locationDbId:asc', false);
        if(isset($data['data'][0]) && !empty($data['data'][0])){
            $data = $data['data'][0];
        }

        // main info
        $code = isset($data['locationCode']) ? 
            [
                'label' => 'Location code',
                'value' => $data['locationCode']
            ] 
            : '';
        $name = isset($data['locationName']) ? 
            [
                'label' => 'Location name',
                'value' => $data['locationName']
            ]
            : '';
        $description = isset($data['description']) ? $data['description'] : '';

        // status
        $status = isset($data['locationStatus']) ? $data['locationStatus'] : '';
        
        $statusCategory = strtolower($status);
        if( $statusCategory == 'completed'){
            $statusCategory = 'info';
        } else if( $statusCategory == 'committed' || $statusCategory == 'planted' ) {
            $statusCategory = 'done';
        } else if( $statusCategory == 'mapped' || strpos($statusCategory, 'in progress') !== false){
            $statusCategory = 'in-progress';
        }

        $statusClass = $this->browserController->getStatusClassByCategory($statusCategory);

        $status = [
            'label' => ucfirst($status),
            'statusClass' => $statusClass
        ];

        // get occurrences
        $locOccGrpParams['locationDbId'] = "equals $id";
        $locOccGrp = $this->locationOccurrenceGroup->searchAll($locOccGrpParams);

        $expArr = $occArr = [];
        $data['experiments'] = $data['occurrences'] = null;
        if(isset($locOccGrp['data']) && !empty($locOccGrp['data'])){
            $locOccGrpArr = $locOccGrp['data'];

            foreach ($locOccGrpArr as $value) {
                $occId = isset($value['occurrenceDbId']) ? $value['occurrenceDbId'] : 0;

                // get occurrence info
                $occParams['occurrenceDbId'] = "equals $occId";
                $occurrenceModel = \Yii::$container->get('app\models\Occurrence');
                $occInfo = $occurrenceModel->searchAll($occParams, 'limit=1', false);
                $occInfoData = isset($occInfo['data'][0]) ? $occInfo['data'][0] : [];

                if(isset($occInfoData['occurrenceName']) && isset($occInfoData['experiment'])){
                    $expArr[] = $occInfoData['experiment'];
                    $occArr[] = $occInfoData['occurrenceName'];
                }
            }

            // get unique experiments
            $expArr = array_unique($expArr);
            $data['experiments'] = implode(', ',$expArr);
            $data['occurrences'] = implode(', ',$occArr);
        }
        
        // primary info
        $attributes = [
            [
                'label' => $this->variable->getAttributeByAbbrev('location_code','label'), 
                'attribute' => 'locationCode'
            ],
            [
                'label' => 'Experiments', 
                'attribute' => 'experiments'
            ],
            [
                'label' => 'Occurrences', 
                'attribute' => 'occurrences'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('plot_count','label'), 
                'attribute' => 'plotCount'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_planting_date','label'),
                'attribute' => 'plantingDate'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_harvest_date','label'),
                'attribute' => 'harvestDate'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('location_steward','label'), 
                'attribute' => 'steward'
            ]
        ];

        $primaryInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        // secondary information
        $attributes = [
            [
                'label' => $this->variable->getAttributeByAbbrev('site','label'), 
                'attribute' => 'site'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('field','label'), 
                'attribute' => 'field'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('year','label'),
                'attribute' => 'year'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('season','label'),
                'attribute' => 'season'
            ],
            [
                'label' => $this->variable->getAttributeByAbbrev('creator','label'),
                'attribute' => 'creator'
            ]
        ];

        $secondaryInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        // audit information
        $attributes = [
            [
                'label' => 'Created',
                'attribute' => 'creationTimestamp'
            ],
            [
                'label' => 'Updated',
                'attribute' => 'modificationTimestamp'
            ]
        ];

        $auditInfo = $this->browserController->processDetailViewInfo($data, $attributes);

        return [
            'code' => $code,
            'name' => $name,
            'description' => $description,
            'status' => $status,
            'primaryInfo' => $primaryInfo,
            'secondaryInfo' => $secondaryInfo,
            'auditInfo' => $auditInfo
        ];


    }

    /**
     * Get location status monitoring info for detail view
     *
     * @param $id integer occurrence identifier
     * @return $monitoringInfo monitoring info data values
     */
    public function getMonitoringInfo($id){
        // status monitoring info
        $monitoringInfo = $this->monitorController->getLocationStatusData($id);

        return $monitoringInfo;
    }

    /**
     * Retrieve valid Locations when mapping an
     * Occurrence to an existing Location
     *
     * @param Integer $id Occurrence identifier
     * @param Integer $mappedLocationId Location identifier if already mapped
     * @return Array $options list of Location options
     */
    public function getValidLocationsByOccurrenceId($id, $mappedLocationId = null){
        $tags = [];

        // Get site, year and season of the occurrence
        $params = [
            'occurrenceDbId' => "equals $id"
        ];

        $occurrence = Yii::$app->api->getParsedResponse('POST',
            'occurrences-search',
            json_encode($params),
            'limit=1',
            false
        );

        $occurrenceArr = isset($occurrence['data'][0]) ? $occurrence['data'][0] : [];

        $siteId = isset($occurrenceArr['siteDbId']) ? $occurrenceArr['siteDbId'] : 0;
        $year = isset($occurrenceArr['experimentYear']) ? $occurrenceArr['experimentYear'] : 0;
        $seasonId = isset($occurrenceArr['experimentSeasonDbId']) ? $occurrenceArr['experimentSeasonDbId'] : 0;

        $locationParams = [
            'siteId' => "equals $siteId",
            'year' => "equals $year",
            'seasonId' => "equals $seasonId",
            'locationStatus' => 'not equals completed',
            'fields' => 'location.id AS id | location.location_name AS name | location.location_code AS code | location.site_id AS siteId | location.location_year AS year | location.season_id AS seasonId | location.location_status AS locationStatus'
        ];

        // if Occurrence is already mapped, add location ID to search params
        if(is_integer($mappedLocationId))
            $locationParams['id'] = "equals $mappedLocationId";

        $locations = Yii::$app->api->getParsedResponse('POST',
            'locations-search?sort=name',
            json_encode($locationParams),
            '',
            true
        );

        $options = [];
        if(isset($locations['data'])){
            $tags = $locations['data'];
            foreach ($tags as $value) {
                $options[] = [
                    'id' => (integer) $value['id'],
                    'text' => $value['name'] . ' ('.$value['code'] . ')'
                ];
            }
        }

        return $options;

    } 

    /**
     * Retrive location basic info for file name by ID 
     * 
     * @param Integer $id Location identifier
     * @return Array Location information
     */
    public function getLocationForFileNameById($id)
    {
        // retrieve location info for CSV filename
        $locationParams = [
            'fields' => 'location.id AS id |'.
                'location.location_code as locationCode |'.
                'location.location_name as locationName',
            'id' => "equals $id",
        ];

        return Yii::$app->api->getParsedResponse(
            'POST',
            'locations-search?limit=1&sort=locationCode:asc',
            json_encode($locationParams),
            '',
            false
        );
    }
}