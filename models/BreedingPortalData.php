<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

class BreedingPortalData extends BaseModel{

    /**
    * API endpoint for breeding portal data
    */
    public static function apiEndPoint() {
        return 'breeding-portal-data';
    }

    public function __construct (
    )
    { }

    /**
     * Update Transformed Data for tenant.breeding_portal_data table based on entity_value column
     */
    public function updateTransformedData() {
        $filters = [
            'fields' => 'bp_data.id AS breedingPortalDataDbId|bp_data.entity_name AS entityName|bp_data.entity_value AS entityValue',
            'entityName' => 'equals breeding_pipeline'
        ];
        $response = $this->searchAll($filters,'',false);
        $data = $response['data'];

        $breedingPipelineData = [];
        foreach ($data as $value) {
            if($value['entityName'] === 'breeding_pipeline') $breedingPipelineData = $value;
        }

        foreach ($breedingPipelineData['entityValue'] as $breedingPipeline) {
            $breedingPipelineTransformedData[] = [
                'bpid' => $breedingPipeline['BPID'],
                'shortName' => $breedingPipeline['BPShortName'],
                'longName' => $breedingPipeline['BPLongName'],
                'description' => $breedingPipeline['BPDescription'],
                'crop' => $breedingPipeline['Crop'],
                'organisation' => $breedingPipeline['Organisation']
            ];
        }

        $this->updateOne($breedingPipelineData['breedingPortalDataDbId'], ['transformedData'=>json_encode($breedingPipelineTransformedData)]);
    }
}