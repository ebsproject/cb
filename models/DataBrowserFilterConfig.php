<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.user_data_browser_configuration".
 *
 * @property int $id Primary key of the record in the table
 * @property int $user_id Id of the user
 * @property string $name Name identifier of the service
 * @property json $data Filter configuration
 * @property string $data_browser_id Name identifier of the data browser
 * @property string type Type of configuration (filter or sort)
 */

class DataBrowserFilterConfig extends BaseModel
{
    public $name;
    public $type;
    public $data;
    public $dataBrowserDbId;

    /**
     * Get table name for class
     *
     * @return String table name
     */
    public static function tableName()
    {
        return 'platform.user_data_browser_configuration';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['name', 'type','data'], 'required'],
        ];
    }


    /**
     * Get the filter configurations for a given user
     *
     * @return Array $result Array of filter configurations
     */
    public function getFilters($dataBrowserId) {

        $param = array(
            "dataBrowserDbId"=>$dataBrowserId,
            "type"=>"filter"
        );
        $url = "data-browser-configurations-search";
        $data = Yii::$app->api->getResponse('POST',$url,json_encode($param));

        $result = [];
        if ($data['status'] == 200) {
            $data = $data['body'];
            $totalPages = (isset($data['metadata']['pagination']['totalPages'])) ? $data['metadata']['pagination']['totalPages'] : null;
            $result = (isset($data['result']['data'])) ? $data['result']['data'] : [];
            // build data provider
            if($totalPages > 1){       
                for ($i=2; $i <= $totalPages; $i++) {
                    $data = Yii::$app->api->getResponse('POST',$url.'&page='.$i, json_encode($param));                
                    if($data['status'] == 200) {
                        $data = $data['body'];
                        $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                    }
                    $result = array_merge($result,$addlResult);
                }
            }
        }
        return $result;
    }

    /**
     * Save the filter configuration for a given user
     *
     * @param String $name custom name for the filters
     * @param Json $data the applied filter configuration
     * @param String $dataBrowserId the ID of the data browser
     * 
     * @return String $result String indicating if sql command executed successfully
     */
    public function saveFilter($name, $data, $dataBrowserId) {
      
        $request = [
            "records"=>[
                [
                    "name" => $name,
                    "type" => "filter",
                    "data" => $data,
                    "dataBrowserDbId" => $dataBrowserId
                ]
            ]
        ];
        $data = Yii::$app->api->getResponse('POST', 'data-browser-configurations', json_encode($request));
        
        if($data['status'] == 200) {
            return true;
        }
        return $data['status'];
    }

    /**
     * Delete the filter configuration of a given user
     *
     * @param Integer $configDbId The ID of the selected filter config to be deleted
     * @return String $result String indicating if sql command executed successfully
     */
    public function deleteFilter($configDbId) {

        $data = Yii::$app->api->getResponse('DELETE', 'data-browser-configurations/'.$configDbId);
        
        if($data['status'] == 200) {
            return true;
        }
        return $data['status'];
    }

}