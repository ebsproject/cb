<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\interfaces\models\ILocationOccurrenceGroup;

/**
 * This is the model class for table "experiment.location_occurrence_group".
 *
 * @property int $id Location Occurrence Group ID: Database identifier of the location occurrence group [LOGRP_ID]
 * @property int $location_id Location ID: Reference to the location where the occurrence is conducted [LOGRP_LOC_ID]
 * @property int $occurrence_id Occurrence ID: Reference to the occurrence that is conducted in the location [LOGRP_OCC_ID]
 * @property int $order_number Order Number: Ordering of the occurrences within the location [LOGRP_ORDERNO]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class LocationOccurrenceGroup extends BaseModel implements ILocationOccurrenceGroup
{
    /**
     * API endpoint for location occurrence group
     */
    public static function apiEndPoint() {
        return 'location-occurrence-groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['location_id', 'occurrence_id', 'order_number', 'creator_id'], 'required'],
            [['location_id', 'occurrence_id', 'order_number', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['location_id', 'occurrence_id', 'order_number', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['notes'], 'string'],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['occurrence_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExperimentOccurrence::className(), 'targetAttribute' => ['occurrence_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location_id' => 'Location ID',
            'occurrence_id' => 'Occurrence ID',
            'order_number' => 'Order Number',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }
}
