<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;
use app\models\BaseModel;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "master.team".
 *
 * @property integer $id
 * @property string $abbrev
 * @property string $name
 * @property integer $leader_id
 * @property string $type
 * @property string $description
 * @property string $display_name
 * @property string $remarks
 * @property string $creation_timestamp
 * @property integer $creator_id
 * @property string $modification_timestamp
 * @property integer $modifier_id
 * @property string $notes
 * @property boolean $is_void
 * @property string $record_uuid
 * @property string $event_log
 *
 * @property MasterCrosscuttingTeam[] $masterCrosscuttingTeams
 * @property MasterItemServiceTeam[] $masterItemServiceTeams
 * @property MasterPipelineTeam[] $masterPipelineTeams
 * @property MasterProgramTeam[] $masterProgramTeams
 * @property MasterServiceTeam[] $masterServiceTeams
 * @property MasterUser $leader
 * @property MasterUser $creator
 * @property MasterUser $modifier
 * @property MasterTeamMember[] $masterTeamMembers
 */
class Team extends BaseModel
{
    /**
     * Gets team members info by team id
     * @param $teamId integer team identifier
     * @return $result array team members info
     */
    public function getTeamMembers($teamId){
        $members = [];
        $callResult = Yii::$app->api->getResponse('GET','teams/'.$teamId.'/members');
        if($callResult['status'] == 200 && isset($callResult['body']['result']['data'][0]['members'])){
            $data = $callResult['body']['result']['data'][0]['members'];

            usort($data,function($first,$second){
                return isset($first['firstName']) ? strcmp($first['firstName'], $second['firstName']) : '';
            });

            $memberIds = array_unique(array_column($data, 'personDbId')); //get unique team ids

            foreach ($data as $key=>$value){ // curate data
                $teamMembers[$value['personDbId']] = $value;
            }

            foreach ($teamMembers as $key => $value){
                if(in_array($key,$memberIds)){
                    $members[] = [
                        'email' => $value['email'],
                        'full_name' => $value['firstName'].' '.$value['lastName'],
                        'member_id' => $value['personDbId'],
                        'role' => $value['role'],
                        'role_abbrev' => $value['roleCode']
                    ];
                }
            }
        }
        return $members;
    }

    /**
     * Gets team members info by team id via CS API
     * @param $teamId integer team identifier
     * @return $result array team members info
     */
    public function getTeamMembersCs($teamId){
        $members = [];

        $callResult = Yii::$app->api->getResponse('GET','program/'.$teamId.'/members', apiResource: 'cs');

        if(isset($callResult['body']['members'])){
            $data = $callResult['body']['members'];

            usort($data,function($first,$second){
                return isset($first['name']) ? strcmp($first['name'], $second['name']) : '';
            });

            $memberIds = array_unique(array_column($data, 'userId')); //get unique team ids

            foreach ($data as $key=>$value){ // curate data
                $teamMembers[$value['userId']] = $value;
            }

            foreach ($teamMembers as $key => $value){
                if(in_array($key,$memberIds)){
                    
                    $members[] = [
                        'email' => $value['username'],
                        'full_name' => $value['name'],
                        'member_id' => $value['userId'],
                        'role' => isset($value['roles'][0]) ? $value['roles'][0] : "NA",
                        'role_abbrev' => isset($value['roles'][0]) ? $value['roles'][0] : "NA",
                    ];
                }
            }
        }
        
        return $members;
    }

    /**
     * Returns tags for all teams
     *
     * @param $teamIds array list of excluded team ids
     * @return $tags array list of team tags
     */
    public static function getAllTeamTags($teamIds){
        $teams = [];
        $teamsCond = [];
        
        $params = [
            "teamCode" => "not equals SG",
        ];

        // if already member from other teams
        if(!empty($teamIds)){
            $teamsCond = implode('| not equals ', $teamIds);
            $teamsParams = [
                "teamDbId" => "not equals " . $teamsCond
            ];
            $params = array_merge($params, $teamsParams);
        }

        $callResult = Yii::$app->api->getResponse('POST', 'teams-search?sort=teamName', json_encode($params));

        if($callResult['status'] == 200 && isset($callResult['body']['result']['data'])){
            $data = $callResult['body']['result']['data'];

            foreach ($data as $value){
                $teams[$value['teamDbId']] = $value['teamName'];
            } 
        }

        return $teams;
        
    }

    /**
     * Returns tags for all roles
     *
     * @return $tags array list of role tags
     */
    public static function getAllRoleTags(){
        $roles = [];
        $postParams = [
           "personRoleCode" => "equals TM|equals COLLABORATOR"
        ];
        $callResult = Yii::$app->api->getResponse('POST', 'roles-search?sort=personRoleName', json_encode($postParams));

        if($callResult['status'] == 200 && isset($callResult['body']['result']['data'])){
            $data = $callResult['body']['result']['data'];
        
            foreach ($data as $value){
                $roles[$value['roleDbId']] = $value['personRoleName'];
            }
        }

        return $roles;
    }
}
