<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "place.geospatial_object".
 *
 * @property int $id Geospatial Object ID: Database identifier of the geospatial object [GEO_ID]
 * @property string $geospatial_object_code Geospatial Object Code: Textual identifier of the geospatial object [GEO_CODE]
 * @property string $geospatial_object_name Geospatial Object Name: Full name of the geospatial object [GEO_NAME]
 * @property string $geospatial_object_type Geospatial Object Type: Type of geospatial object {site, field, planting_area} [GEO_TYPE]
 * @property string $geospatial_object_subtype Geospatial Object Subtype: Subtype of the geospatial object {country, region, province, district, breeding location, international agricultural research center, national agricultural research center, etc.} [GEO_SUBTYPE]
 * @property string|null $geospatial_coordinates Geospatial Object Coordinates: Set of coordinates that determines the location of the geospatial object, which can form a central point (one pair of latitude-longitude) or a shape/polygon (three or more pairs of latitude-longitude) [GEO_COORDS]
 * @property float|null $altitude Geospatial Object Altitude: Distance of the place from the Earth's sea/ground level [GEO_ALTITUDE]
 * @property string|null $description Geospatial Object Description: Additional information about the geospatial object [GEO_DESC]
 * @property int|null $parent_geospatial_object_id Geospatial Object Parent ID: Reference to the parent geospatial object of the geospatial object, which depends on its type [GEO_PARENT_GEO_ID]
 * @property int|null $root_geospatial_object_id Geospatial Object Root ID: Reference to the root geospatial object of the geospatial object [GEO_ROOT_GEO_ID]
 * @property int $creator_id Creator ID: Reference to the person who created the record [CPERSON]
 * @property string $creation_timestamp Creation Timestamp: Timestamp when the record was created [CTSTAMP]
 * @property int|null $modifier_id Modifier ID: Reference to the person who last modified the record [MPERSON]
 * @property string|null $modification_timestamp Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]
 * @property bool $is_void Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]
 * @property string|null $notes NOTES: Technical details about the record [ISVOID]
 * @property string|null $event_log
 */
class GeospatialObject extends BaseModel
{
    /**
     * API endpoint for geospatial object
     */
    public static function apiEndPoint() {
        return 'geospatial-objects';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place.geospatial_object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['geospatial_object_code', 'geospatial_object_name', 'geospatial_object_type', 'geospatial_object_subtype', 'creator_id'], 'required'],
            [['geospatial_coordinates', 'description', 'notes'], 'string'],
            [['altitude'], 'number'],
            [['parent_geospatial_object_id', 'root_geospatial_object_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['parent_geospatial_object_id', 'root_geospatial_object_id', 'creator_id', 'modifier_id'], 'integer'],
            [['creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['geospatial_object_code', 'geospatial_object_type', 'geospatial_object_subtype'], 'string', 'max' => 64],
            [['geospatial_object_name'], 'string', 'max' => 128],
            [['geospatial_object_code'], 'unique'],
            [['parent_geospatial_object_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeospatialObject::className(), 'targetAttribute' => ['parent_geospatial_object_id' => 'id']],
            [['root_geospatial_object_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeospatialObject::className(), 'targetAttribute' => ['root_geospatial_object_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'geospatial_object_code' => 'Geospatial Object Code',
            'geospatial_object_name' => 'Geospatial Object Name',
            'geospatial_object_type' => 'Geospatial Object Type',
            'geospatial_object_subtype' => 'Geospatial Object Subtype',
            'geospatial_coordinates' => 'Geospatial Coordinates',
            'altitude' => 'Altitude',
            'description' => 'Description',
            'parent_geospatial_object_id' => 'Parent Geospatial Object ID',
            'root_geospatial_object_id' => 'Root Geospatial Object ID',
            'creator_id' => 'Creator ID',
            'creation_timestamp' => 'Creation Timestamp',
            'modifier_id' => 'Modifier ID',
            'modification_timestamp' => 'Modification Timestamp',
            'is_void' => 'Is Void',
            'notes' => 'Notes',
            'event_log' => 'Event Log',
        ];
    }
}
