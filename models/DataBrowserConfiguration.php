<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\models;

use Yii;

use app\models\User;

/**
 * This is the model class for table "platform.user_data_browser_configuration".
 *
 * @property int $id Identifier of the record within the table
 * @property int $user_id Id of the user who owns the filter
 * @property string $name Name of the filter
 * @property string $data Filter data in json format
 * @property string $data_browser_id Data browser which contains the filter
 * @property string|null $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string|null $modification_timestamp Timestamp when the record was last modified
 * @property int|null $modifier_id ID of the user who last modified the record
 * @property string|null $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted (true) or not (false)
 * @property string|null $event_log Historical transactions of the record
 * @property string $record_uuid Universally unique identifier (UUID) of the record
 * @property string|null $type
 */
class DataBrowserConfiguration extends BaseModel
{
    /**
     * API endpoint for user data browser configurations
     */
    public static function apiEndPoint() {
        return 'data-browser-configurations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'data', 'data_browser_id', 'creator_id'], 'required'],
            [['user_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['user_id', 'creator_id', 'modifier_id'], 'integer'],
            [['name', 'data_browser_id', 'remarks', 'notes', 'record_uuid', 'type'], 'string'],
            [['data', 'creation_timestamp', 'modification_timestamp', 'event_log'], 'safe'],
            [['is_void'], 'boolean'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TenantPerson::className(), 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'data' => 'Data',
            'data_browser_id' => 'Data Browser ID',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
            'event_log' => 'Event Log',
            'record_uuid' => 'Record Uuid',
            'type' => 'Type',
        ];
    }

    /**
     * Save Data browser configuration
     *
     * @param text $type type of configuration
     * @param integer $userId user identifier
     * @param array $dataBrowserId data browser identifier
     * @param array $data configuration to be saved
     */
    public function saveConfig($type, $userId, $dataBrowserId, $data) {

        if(empty($type)){
            $type='filter';
        }
        $searchParams = [
            'userDbId' => (string)$userId,
            'dataBrowserDbId' => $dataBrowserId,
            'type' => $type
        ];

        // check if configuration already exists
        $config = $this->searchAll($searchParams);

        // name of configuration
        $name = $dataBrowserId . ' ' . $type;
        
        $requestData = [
            'name' => $name,
            'type' => $type,
            'data' => $data,
            'dataBrowserDbId' => $dataBrowserId
        ];

        // if no config yet
        if(isset($config['totalCount']) && $config['totalCount'] == 0){
            
            if(!empty($data)){
                $request = [
                    'records'=>[
                        $requestData
                    ]
                ];

                DataBrowserConfiguration::create($request);
            }

        }else{
            // if there is an existing config, update record
            if(isset($config['data'][0]['configDbId'])){
                $configDbId = $config['data'][0]['configDbId'];

                DataBrowserConfiguration::updateOne($configDbId, $requestData);
            }
        }
    }

    /**
     * Get data browser configuration for occurrences browser
     *
     * @param Integer $userId User Identifier
     * @param Array $dashboardFilters Dashboard data filters
     *
     * @return Array Occurrence filter values and texts 
     */
    public function getDataBrowserConfig($userId, $dashboardFilters){
        $occurrenceTextFilters = [];
        // process dashboard filters
        $searchParams = [
            'userDbId' => (string)$userId,
            'dataBrowserDbId' => 'occurrences-grid',
            'type' => 'filter'
        ];

        // check if configuration already exists
        $config = DataBrowserConfiguration::searchAll($searchParams);
        $occurrenceFilters = [];
        if (isset($config['totalCount']) && $config['totalCount'] > 0) {
            $occurrenceFilters = $config['data'][0]['data'];
        }

        // get text filters
        $searchParams = [
            'userDbId' => (string)$userId,
            'dataBrowserDbId' => 'occurrences-text-grid',
            'type' => 'filter'
        ];

        // check if configuration already exists
        $config = DataBrowserConfiguration::searchAll($searchParams);
        $occurrenceFilters = [];
        if (isset($config['totalCount']) && $config['totalCount'] > 0) {
            $occurrenceTextFilters = $config['data'][0]['data'];
        }

        return [
            'occurrenceFilters' => $occurrenceFilters,
            'occurrenceTextFilters' => $occurrenceTextFilters
        ];
    }

    /**
     * Get data browser settings of all session browsers
     *
     * @return Array settings for each data browser
     */
    public function getDataBrowserSettings($name) {

        $userModel = new User();
        $personId = $userModel->getUserId(); //get user id

        // Get data browser grid settings from db
        $dataBrowserSettings = $this->searchAll([
            'userDbId' => "equals $personId",
            'name' => "equals $name",
            'dataBrowserDbId' => 'equals dynagrid-settings'
        ]);

        // If data browser grid settings does not exist on db, create a blank one
        if($dataBrowserSettings['totalCount'] == 0) {
            $this->create([
                'records' => [
                    [
                        'name' => $name,
                        'dataBrowserDbId' => 'dynagrid-settings',
                        'data' => json_encode([]),
                        'type' => 'config'
                    ]
                ]
            ]);

            // Retrieve the settings again
            $dataBrowserSettings = $this->searchAll([
                'userDbId' => "equals $personId",
                'name' => "equals $name",
                'dataBrowserDbId' => 'equals dynagrid-settings'
            ]);
        }

        return $dataBrowserSettings;
    }

    /**
     * Update data browser page size for all session browsers. Apply to session and store to db
     *
     * @param Integer $pageSize Page size
     */
    public function updateDataBrowserPageSize($pageSize) {
        // Get data browser grid settings from db
        $dataBrowserSettings = $this->getDataBrowserSettings('dynagrid-settings-of-all-session-browsers');
        $browserSettingConfigDbId = $dataBrowserSettings['data'][0]['configDbId'];
        $currentBrowserSetting = $dataBrowserSettings['data'][0]['data'];

        // Iterate through each setting and save to $newBrowserSetting
        $newBrowserSetting = [];
        foreach ($currentBrowserSetting as $browserId => $browserSetting) {
            if(is_null($browserSetting) || $browserSetting == '[]') continue;

            // Parse browser setting
            $setting = json_decode($browserSetting, true);
            $grid = json_decode($setting['grid'], true);

            // Change page size of grid setting
            $grid['page'] = "$pageSize";

            // Rebuild browser setting
            $grid = json_encode($grid);
            $setting['grid'] = $grid;
            $newSetting = json_encode($setting);

            // Apply data browser grid settings to session
            Yii::$app->session->set($browserId, $newSetting);

            $newBrowserSetting[$browserId] = $newSetting;
        }

        // Save new browser settings
        $this->updateOne($browserSettingConfigDbId, ['data'=>$newBrowserSetting]);
    }

    /**
     * Saves data browser settings of a single browser or multiple browsers (e.g. on the same page) to the database
     *
     * @param array<string> $browserIds browser identifier
     */
    public function saveDataBrowserSettings($browserIds) {
        // Get data browser grid settings from session
        $newBrowserSetting = [];
        foreach ($browserIds as $browserId) {
            $browserId = "{$browserId}_";
            $newBrowserSetting[$browserId] = Yii::$app->session[$browserId];
        }

        // Get data browser grid settings from db
        $dataBrowserSettings = $this->getDataBrowserSettings('dynagrid-settings-of-all-session-browsers');
        $browserSettingConfigDbId = $dataBrowserSettings['data'][0]['configDbId'];
        $currentBrowserSetting = $dataBrowserSettings['data'][0]['data'];

        // Add/Replace new settings to old settings
        $newBrowserSetting = array_merge($currentBrowserSetting, $newBrowserSetting);

        // Save new browser settings
        $this->updateOne($browserSettingConfigDbId, ['data'=>$newBrowserSetting]);
    }

    /**
     * Load data browser grid settings from db to session
     */
    public function loadDataBrowserSettings() {
        // Get data browser grid settings from db
        $dataBrowserSettings = $this->getDataBrowserSettings('dynagrid-settings-of-all-session-browsers');
        $currentBrowserSetting = $dataBrowserSettings['data'][0]['data'];

        // Load data browser grid settings to session
        foreach ($currentBrowserSetting as $browserId => $browserSetting) {
            Yii::$app->session->set($browserId, $browserSetting);
        }
    }

    /**
     * Process current data browser state once a controller action is called
     *
     * @param String $browserId browser identifier
     * @param String $baseUrl Base url of the current page of the browser
     *                  This is used for the saving of browser states which rely on url parameters.
     *                  Value should come from `Url::current();` but without the parameters.
     *                  Sample: '/index.php/advancedDataBrowser/default/index'
     * @param Array $urlParams Expected base url parameters of the current page of the browser.
     *                  Specifically the parameters on the Controller Action which excludes the filters
     *                  Sample: ['program' => $program]
     * @param Array $nonBrowserParamsToExclude Parameters that, if present, should not be saved on the data browser state
     *                  If these parameters are present, then the whole logic of saving and applying of the previous
     *                  browser state are skipped. Sample of these parameters are the DbId filters which are applied
     *                  when the user clicks a notification. The array can accept strings or array values.
     *                  Sample: ['apiColumn', 'EntitySearch' => ['entityDbId']]
     */
    public function processCurrentDataBrowserState($browserId, $baseUrl = null, $urlParams = [], $nonBrowserParamsToExclude = []) {
        // Get current url value
        $currentUrl = \Yii::$container->get('yii\helpers\Url')->current();

        // If the url has a param that should be excluded, then don't do anything
        $currentParams = explode('?', $currentUrl)[1];
        parse_str($currentParams, $currentParams);
        foreach($nonBrowserParamsToExclude as $excludedKey => $excludedVal) {
            if(gettype($excludedVal) == 'string') {
                if(isset($currentParams[$excludedVal])) return;
            } else if (gettype($excludedVal) == 'array') {
                if(isset($currentParams[$excludedKey])) {
                    foreach($excludedVal as $ex) {
                        if(isset($currentParams[$excludedKey][$ex])) return;
                    }
                }
            }
        }

        if(is_null($baseUrl)) $baseUrl = explode('?', $currentUrl)[0];

        // Get Data Browser state
        $browserState = $this->getDataBrowserState($browserId);
        if(is_null($browserState)) {
            $browserState = [
                'base' => $baseUrl,
                'default' => '',
                'recent' => $baseUrl . '?' . http_build_query($currentParams),
                'savedStates' => []
            ];
        }

        // Get parameters and url parameters that was previously applied (found in recent url from db)
        $previousParams = explode('?', $browserState['recent'])[1];
        parse_str($previousParams, $previousParams);
        $previousUrlParams = [];
        foreach($urlParams as $param => $paramVal) {
            $previousUrlParams[$param] = $previousParams[$param];
        }

        // Check if user is newly logged in, apply the last filters and sort applied (thru url) from db
        $savedFilterAndSortAlreadyLoaded = \Yii::$app->cbSession->get($browserId . 'savedFilterAndSortAlreadyLoaded');
        if(!$savedFilterAndSortAlreadyLoaded || is_null($savedFilterAndSortAlreadyLoaded)) {
            \Yii::$app->cbSession->set($browserId . 'savedFilterAndSortAlreadyLoaded', true);

            // Also only apply if both of the base urls (current and in db) are not the same,
            // and the current parameters and url parameters don't have the same value; Else there is no need to apply this
            if($baseUrl != $browserState['base'] && $currentParams != $urlParams) {
                // If current url parameters are the same as previous url parameters, then apply recent
                // Else, apply base url with current url parameters
                if($urlParams == $previousUrlParams) $redirectUrl = $browserState['recent'];
                else $redirectUrl = $browserState['base'] . '?' . http_build_query($urlParams);

                // redirect
                \Yii::$app->getResponse()->redirect($redirectUrl);
            }
        }

        // Check if user navigates to Data Browser Page from another page or tool
        if($baseUrl == $browserState['base'] && $currentParams == $urlParams && $urlParams == $previousUrlParams) {
            // If there was a previously applied filter and sort parameters, apply them using the recent url from db
            if($currentParams != $previousParams) {
                // Get recent url value
                $redirectUrl = $browserState['recent'];

                // redirect
                \Yii::$app->getResponse()->redirect($redirectUrl);
            }
        }

        // Save current url as recent url
        $browserState['recent'] = $currentUrl;
        $this->saveDataBrowserState($browserId, $browserState);
    }

    /**
     * Saves data browser state to the database
     *
     * @param String $browserId browser identifier
     * @param Object $browserState contains the data browser state to be saved to db
     */
    public function saveDataBrowserState($browserId, $browserState) {
        // Get data browser grid settings from db
        $dataBrowserState = $this->getDataBrowserSettings('dynagrid-state-of-all-session-browsers');
        $browserSettingConfigDbId = $dataBrowserState['data'][0]['configDbId'];
        $currentBrowserStates = $dataBrowserState['data'][0]['data'];

        // Add/Replace new settings to old settings
        $newBrowserState = array_merge($currentBrowserStates, [$browserId => $browserState]);

        // Save new browser settings
        $this->updateOne($browserSettingConfigDbId, ['data'=>$newBrowserState]);
    }

    /**
     * Get data browser state
     * @param String $browserId browser identifier
     */
    public function getDataBrowserState($browserId) {
        // Get data browser state from db
        $dataBrowserState = $this->getDataBrowserSettings('dynagrid-state-of-all-session-browsers');
        $currentBrowserSetting = $dataBrowserState['data'][0]['data'];

        return $currentBrowserSetting[$browserId] ?? null;
    }
}
