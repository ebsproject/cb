<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "platform.plan_config".
 *
 * @property int $id Unique identifier of the record
 * @property string $abbrev Short name identifier or abbreviation of the record
 * @property string $name Unique name identifier of the record
 * @property array $config_value json configuration value
 * @property int $rank Order to be followed in implementing the rule for the application
 * @property string $usage Where the config is used
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 */
class PlanConfigBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform.plan_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abbrev', 'name', 'config_value', 'usage'], 'required'],
            [['config_value', 'creation_timestamp', 'modification_timestamp'], 'safe'],
            [['rank', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['rank', 'creator_id', 'modifier_id'], 'integer'],
            [['remarks'], 'string'],
            [['is_void'], 'boolean'],
            [['abbrev', 'usage'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 255],
            [['notes'], 'string', 'max' => 600],
            [['abbrev'], 'unique'],
            [['name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abbrev' => 'Abbrev',
            'name' => 'Name',
            'config_value' => 'Config Value',
            'rank' => 'Rank',
            'usage' => 'Usage',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
        ];
    }
}
