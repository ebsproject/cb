#!/bin/bash

CB_UI_COVERAGE_XML=$1

# checks if there is coverage file in argument
if [ -z "$CB_UI_COVERAGE_XML" ]
then
    printf "Code coverage file not entered. Try again.\n";
    exit 1
fi

lineCount=$(wc -l < ${CB_UI_COVERAGE_XML})
overallMetrics=$((lineCount-2))
coverageStr=$(sed -n ${overallMetrics}p ${CB_UI_COVERAGE_XML})

IFS=" "
read -ra coverageStrArr <<< "$coverageStr"

for i in "${coverageStrArr[@]}"
do
    case $i in
        "methods"*)
            methodStr=$i
            ;;
        "coveredmethods"*)
            coveredMethodStr=$i
            ;;
        "statements"*)
            lineStr=$i
            ;;
        "coveredstatements"*)
            coveredLineStr=$i
            ;;
    esac
done

IFS="\""
read -ra methodArr <<< "$methodStr"
read -ra coveredMethodArr <<< "$coveredMethodStr"
read -ra lineArr <<< "$lineStr"
read -ra coveredLineArr <<< "$coveredLineStr"

lastIndex=$((${#methodArr[@]}-1))

methods=${methodArr[$lastIndex]}
coveredMethods=${coveredMethodArr[$lastIndex]}
lines=${lineArr[$lastIndex]}
coveredLines=${coveredLineArr[$lastIndex]}

methodCoverage=$(echo "(($coveredMethods/$methods)*100)" | bc -l)
lineCoverage=$(echo "(($coveredLines/$lines)*100)" | bc -l)

printf "Total Method Coverage: %.2f%%\\\\nTotal Line Coverage: %.2f%%" $methodCoverage $lineCoverage