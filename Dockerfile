# This file is part of EBS-Core Breeding.
#
# EBS-Core Breeding is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# EBS-Core Breeding is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 
FROM php:8.0.28-apache-buster AS builder

LABEL "MAINTAINER"="j.lagare@irri.org/e.tenorio@irri.org"

RUN apt-get update && apt-get install -y libzip-dev libpng-dev libpq-dev git && docker-php-ext-install zip gd sockets

WORKDIR /var/www/html

COPY composer.json .
COPY composer.json /root/.composer/composer.json
COPY ./run.sh .

RUN curl https://getcomposer.org/download/2.1.8/composer.phar --output composer.phar && php composer.phar config --global --auth github-oauth.github.com e3fa89bd8d5023bab2c9f619bf9bf079afb6fa57 \
    && php composer.phar install --prefer-source --no-interaction

FROM php:8.0.28-apache-buster AS base

RUN apt-get update && apt-get install -y libzip-dev libpng-dev libpq-dev git && docker-php-ext-install zip gd sockets && pecl install pcov && docker-php-ext-enable pcov && docker-php-ext-configure opcache --enable-opcache && docker-php-ext-install opcache

ENV APACHE_CONF_DIR=/etc/apache2 \
    PHP_CONF_DIR=/usr/local/etc/php/conf.d \
    PHP_DATA_DIR=/var/log/apache2

# Environment variables
ENV BA_API2_URL=http://ba-api2:80/
ENV BSO_API_URL=http://bso-api:8080/
ENV CB_ADMIN_EMAIL='admin@example.com'
ENV CB_API_V3_URL=http://cb-api:3000/v3/
ENV CB_DATA_BROWSER_DEFAULT_PAGE_SIZE=50
ENV CB_DATA_BROWSER_MAX_PAGE_SIZE=100
ENV CB_DEBUG_ENABLED=FALSE
ENV CB_RABBITMQ_HOST=rabbitmq
ENV CB_RABBITMQ_PASSWORD=zDrRsxtFMK
ENV CB_RABBITMQ_PORT=5672
ENV CB_RABBITMQ_USER=ebsrabbitmq
ENV CB_REDIS_HOST=cb-redis
ENV CB_REDIS_PORT=6379
ENV CB_SG_AUTH_URL=https://sg.ebsproject.org:9443/oauth2/authorize
ENV CB_SG_CLIENT_ID=xxxx
ENV CB_SG_CLIENT_SECRET=xxxx
ENV CB_SG_REDIRECT_URI=https://cb.local/index.php/auth/default/verify
ENV CB_SG_TOKEN_URL=https://sg.ebsproject.org:9443/oauth2/token
ENV CB_UNDER_MAINTENANCE=FALSE
ENV CB_URL=https://cb.local/
ENV CB_VALID_EMAIL_DOMAINS=irri.org|gmail.com|yahoo.com|cgiar.org|cimmyt.org|cornell.edu
ENV CS_API_URL=https://csapi.local/
ENV CS_PS_URL=http://cs-ps/
ENV EBS_SG_AF_API_URL=http://ebs-sg-af:8080/graphql
ENV IDSITE=xxxx
ENV MATOMO_ENABLED=FALSE
ENV MATOMO_URL=xxxx

# PHP Environment Variables
ENV PHP_OPCACHE_REVALIDATE_FREQ=0 \
    PHP_OPCACHE_MAX_ACCELERATED_FILES=10000 \
    PHP_OPCACHE_MEMORY_CONSUMPTION=192 \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE=10 \
    PHP_OPCACHE_INTERNED_STRINGS_BUFFER=16 \
    PHP_MAX_INPUT_VARS=10000

ARG STDOUT=/proc/1/fd/1
ARG STDERR=/proc/1/fd/2

# Redirect apache logs for docker
RUN ln -sf $STDOUT $PHP_DATA_DIR/access.log \
    && ln -sf $STDERR $PHP_DATA_DIR/error.log \
    && ln -sf $STDOUT $PHP_DATA_DIR/other_vhosts_access.log

# PHP configurations for file uploads and opcache
RUN echo "file_uploads = On\n" \
    "memory_limit = 512M\n" \
    "upload_max_filesize = 25M\n" \
    "post_max_size = 0\n" \
    "max_input_vars = ${PHP_MAX_INPUT_VARS}\n" \
    > /usr/local/etc/php/conf.d/uploads.ini && \
    echo "opcache.enable=1\n" \
    "opcache.enable_cli=1\n" \
    "opcache.revalidate_freq=${PHP_OPCACHE_REVALIDATE_FREQ}\n" \
    "opcache.max_accelerated_files=${PHP_OPCACHE_MAX_ACCELERATED_FILES}\n" \
    "opcache.memory_consumption=${PHP_OPCACHE_MEMORY_CONSUMPTION}\n" \
    "opcache.max_wasted_percentage=${PHP_OPCACHE_MAX_WASTED_PERCENTAGE}\n" \
    "opcache.interned_strings_buffer=${PHP_OPCACHE_INTERNED_STRINGS_BUFFER}\n" \
    > /usr/local/etc/php/conf.d/opcache.ini

COPY --from=builder /var/www/html/vendor vendor/
COPY --chown=www-data:www-data . .

RUN chmod a+x ./run.sh


CMD ["./run.sh"]