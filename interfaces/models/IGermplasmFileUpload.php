<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface IGermplasmFileUpload extends IBaseModel {

    /**
     * Search model for germplasm file upload
     * @param $params array list of parameters
     * @param $filters array list of additional search filters
     * 
     * @return $dataProvider array data provider for germplasm file upload browser
     */
    public function search($params, $filters);

    /**
     * Facilitate the extraction of browser search parameters and filters
     * 
     * @return Array values to be used in searching file upload records
     */
    public function extractBrowserFilters($filters);

    /**
     * Facilitates retrieval of germplasm file upload status scale values
     * 
     * @return array variable configuration information
     */
    public function getFileUploadStatusValues();

    /**
     * Build configuration abbrev based on level
     * 
     * @param String $entity entity
     * @param String $action action
     * @param String $level data level
     * @param String $levelValue data level value
     * 
     * @return $configAbbrev
     */
    public function buildConfigAbbrev($entity,$action,$level,$levelValue);

    /**
     * Retrieve configuration values
     * 
     * @param String $entity entity of the file upload transaction (germplasm/germplasm-relation/germplasm-attribute)
     * @param String $action germplasm manager capability to be used (create/merge/update)
     * @param String $userRole user role value
     * @param String $program dashboard program value
     * @param String $cropCode crop code value for program
     * 
     * @return mixed
     */
    public function getConfigValues($entity,$action,$userRole,$program,$cropCode);

    /**
     * Retrieve variables from configuration record matching entity, action, and level
     * 
     * @param Array $configValues configuration values
     * 
     * @return mixed
     */
    public function getFileUploadVariables($configValues);

    /**
     * Facilitates creation of new germplasm merge file upload record
     * @param Array $paramRequest parameter body build on controller
     * 
     * @return mixed
     */
    public function saveUploadedFile($paramRequest);

}
?>