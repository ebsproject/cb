<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

use app\dataproviders\ArrayDataProvider;

interface ISeedTransfer extends IBaseModel {

    /**
     * Returns a unique identifier for a seed transfer browser
     */
    public function getDataBrowserId();

    /**
     * Returns model name to be used by seed transfer browser
     */
    public function getModelName();

    /**
     * Search model for seed transfer
     *
     * @param $params array search fields
     * @param $filters array limit, sort, page options
     *
     * @return ArrayDataProvider $dataProvider data provider for seed transfer browser
     */
    public function search($params=null, $filters='');

    /**
     * Returns data filters for searching
     *
     * @param array $params user browser request data
     */
    public function getSearchParams($params);

    /**
     * Returns sort, page, and limit filters for searching
     *
     * @param array $params user browser request data
     */
    public function getSearchFilters($params);

}
