<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface IPlatformListMember extends IBaseModel {

    /**
     * Retrieve list members using v3/lists/:id/members-search call
     * @param Integer $listDbId ID of the list
     * @param String $filters additional filters
     * @param Array $params request body
     * @param Boolean $retrieveAll check if needs to retrieve all data
     * @return Array contains the total pages and the data
     */
    public function searchAllMembers($listDbId, $filters='', $params=null, $retrieveAll=true);

    /**
     * Permanently removes items from a list
     * @param String $listMemberIdStr concatenated IDs of the list members to be removed
     * @return Integer number of rows affected
     */
    public function bulkDelete($listMemberIdStr);

    /**
     * Bulk update for list members; calls POST /v3/broker call
     * @param String $listMemberDbIds ID of the list members to be updated; separated by |
     * @param Integer $listId ID of the list
     * @param Array $requestData contains the attributes and values to be updated
     * @return Array the response data from API
     */
    public function bulkUpdate($listMemberDbIds, $listId, $requestData);
}