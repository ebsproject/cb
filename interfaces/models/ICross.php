<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface ICross extends IBaseModel
{
    /**
     * Retrieves existing crosses in the experiment
     * @param String $apiEndPoint the API resource endpoint
     * @param Array $params request body parameter for the api resource
     * @param String $filters optional data filters
     * @param Boolean $retrieveAll whether or not to retrieve all pages of the result
     */
    public function searchAllData($apiEndPoint,$params, $filters = '', $retrieveAll = true);
 
    /**
     *  Custom get all cross records
     * @param String $filters optional data filters
     * @param Boolean $retrieveAll whether or not to retrieve all pages of the result
     */
     public function getCrosses($filters, $retrieveAll = true);
 
    /**
     * Retrieve cross information
    * 
    * @param Array $params request body parameter for the api resource
    * @param String $filters optional data filters
    * @param Array $pagination optional pagination not null if pagination is active
    * @param String $sort optional sort data by cross id SORT_ASC/SORT_DESC 
    */
    public function getDataProvider($params = null, $filters = null, $pagination = null, $sort = 'SORT_ASC');
 
 
    /**
     * Delete all records in germplasm.cross_parent and germplasm.cross tables
     * @param Integer $experimentDbId experiment identifier
     */
    public function deleteCrosses($experimentDbId);
 
    /**
     * Delete selected records in germplasm.cross_parent and germplasm.cross tables
     * @param Integer $experimentDbId experiment identifier
     * @param Array $selectedIds array of selected cross ids
     */
    public function deleteSelectedCrosses($experimentDbId, $selectedIds);
 
    /** 
     * Create self crosses record in germplasm.cross_parent and germplasm.cross tables
     * @param Array $validList list of valid cross info
     * @param Integer $experimentDbId experiment identifier
     */
    public function createSelfCrosses($validList, $experimentDbId);
}
