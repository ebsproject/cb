<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface IApplication
{
    /**
     * Return processed url of an application given application id
     *
     * @param $id integer application identifier
     * @return $url text processed url of an application
     */
    public function getUrlByAppId($id,$program);


    /**
     * Gets url of application by application abbreviation
     *
     * @param text $abbrev application abbreviation
     * @param text $program current program of user
     *
     * @return text $url url of application
     */
    public function getUrlByAppAbbrev($abbrev,$program);

    /**
     * Gets application information by id
     *
     * @param $id integer application identifier
     * @return $result object application information
     */
    public function getAppInfoById($id);

    /**
     * Gets application information by abbrev
     *
     * @param $abbrev integer application identifier
     * @return $result object application information
     */
    public function getAppInfoByAbbrev($abbrev);

    /**
     * Return module, cotntroller and action of an application given application id
     *
     * @param $id integer application identifier
     * @return $url text processed url of an application
     */
    public function getOptionsAppId($id);

    /**
     * Retrieves all applications filter tags where user has access to
     *
     * @param $userId integer user identifier
     * @param $timestamp 
     * @return $result array list of applications tags where user has access to
     */
    public function getApplications($userId,$timestamp);

}
