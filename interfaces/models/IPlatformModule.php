<?php
/**
 * This is the interface for PlatformModule class.
 */

namespace app\interfaces\models;

interface IPlatformModule {

    /**
     * Retrieves an existing module in the database
     * @param $id integer module id
     * @return array module's record in the database
     */
    public function getModule($id);

}

?>