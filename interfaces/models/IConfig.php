<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\interfaces\models;


interface IConfig
{
    /**
     * Get configurations by abbrev
     *
     * @param string $abbrev config abbrev
     * @return array data config value
     */
    public function getConfigByAbbrev($abbrev);

    /**
     * Retrieve configuration of form
     * by configuration abbreviation and step
     *
     * @param Text $abbrev Abbreviation of configuration
     * @param Text $step Step to be retrieved
     *
     * @return Array $fields Fields configuration 
     */
    public function getFormConfig($abbrev, $step=null);
    
}