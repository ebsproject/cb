<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface ILocation extends IBaseModel
{
    /**
     * Find Experiments in browser detail view catgory
     *
     * @return $results list of experiments
     */
    public function findCategories();

    /**
     * Search for locations given search parameters
     *
     * @param Array $params list of search parameters
     * @return Array $data list of location records
     */
    public function searchRecords($params = null);

    /**
     * Search for locations given search parameters
     *
     * @param $filter search parameters
     * @param $category text current filter category
     * @param $selectedCategoryId integer selected category item
     * @return $data array list of occurrences given search parameters
     */
    public function browse($filter=null, $category=null, $selectedCategoryId=null, $page=null);

    /**
     * Retrieves locations given experiment ID
     *
     * @param integer $experimentId experiment identifier
     * @return array $locationsArr list of locations given experiment ID
     */
    public function getLocationsByExperimentId($experimentId);

    /**
     * Get location info for detail view
     *
     * @param $id integer location identifier
     * @return $data array location information
     */
    public function getDetailViewInfo($id);

    /**
     * Retrieve valid Locations when mapping an
     * Occurrence to an existing Location
     *
     * @param Integer $id Occurrence identifier
     * @return Array $options list of Location options
     */
    public function getValidLocationsByOccurrenceId($id);
}
