<?php
/**
 * This is the interface for Program class.
 */

namespace app\interfaces\models;

interface IProgram extends IBaseModel {

    /**
     * Get program name and abbrev by program id
     * @param $id integer program identifier
     * @return $program text program name and abbrev
     */
    public function getProgramNameById($id);

    /**
     * Get program attribute
     *
     * @param $condAttr text attribute in where condition
     * @param $condVal text program identifier
     * @param $attr text attribute to be retrieved
     *
     * @return $program text program attribute
     */
    public function getProgramAttr($condAttr = 'id', $condVal = '', $attr = 'abbrev');

    /**
     * Get configurations by program code
     *
     * @param $programCode text program program code
     * @return $data array program value
     */
    public function getProgramByProgramCode($programCode);

     /**
     * Get configurations by id
     *
     * @param $id integer program id
     * @return $data array program record
     */
    public function getProgram($id);

    /**
     * Retrieves the crop code of the given program code
     * @param String programCode program code value
     * @return String crop code value
     */
    public function getCropCode($programCode);

}

?>