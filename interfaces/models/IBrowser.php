<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface IBrowser
{

    /**
     * Format string to camel case
     *
     * @param String $text string to be converted to camel case
     * @return String text converted to camel case
     */
    public function formatToCamelCase($text);

    /**
     * Build form columns
     *
     * @param Array $fields Fields to be displayed in form
     * @param Text $entity Entity to be retrieved
     */
    public function buildFormColumns($fields, $entity);

    /**
     * Build list of variables to form configuration
     *
     * @param Array $vars List of variable abbrevs
     */
    public function buildVarsAsFormConfig($vars);
}
