<?php
/**
 * This is the interface for ItemModule class.
 */

namespace app\interfaces\models;

interface IItemModule {

    /**
     * Retrieves an existing item module in the database
     * @param $condParam array condition parameters 
     * @return array item module's record in the database
     */
    public function getItemModule($param);

}

?>