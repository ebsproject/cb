<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\interfaces\models;

/**
 * Model for defining all basic classes among B4R models
 */

interface IBaseModel
{
    /**
     * Method for retrieving a record given its database ID
     * @param Integer $dbId the corresponding database ID
     * @return Array the record from the database
     */
    public function getOne($dbId);

    /**
     * Method for retrieving all the records from a given endpoint using the GET method
     * @param String $filters specifies how the records should be filtered
     * @return Array the records from the database
     */
    public function getAll($filters='', $retrieveAll=true);

    /**
     * Method for updating a single record in the database
     * @param Integer $dbId the database Id of the record to be updated
     * @param Array $requestData contains the data to be updated
     * @return Array API response
     */
    public function updateOne($dbId, $requestData);

    /**
     * Method for updating multiple records in the database
     * @param Array $dbIdsArray the database Ids of the records to be updated
     * @param Array $requestData contains the data to be updated
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @return Array API response
     */
    public function updateMany(array $dbIdsArray, $requestData, $backgroundProcess=false, $additionalParams=[]);

    /**
     * Method for deleting a single record in the database
     * @param Integer $dbId the database Id of the record to be deleted
     * @return Array API response
     */
    public function deleteOne($dbId);

    /**
     * Method for deleting multiple records in the database
     * @param Array $dbIdsArray the database Ids of the records to be deleted
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @return Array API response
     */
    public function deleteMany($dbIdsArray, $backgroundProcess=false, $additionalParams=[]);

    /**
     * Method for creating one or more records in the database
     * @param Array $requestData contains the information about the records
     * @param Boolean $backgroundProcess flag to know if the request should use processor call; optional and defaults to false
     * @param Array $additionalParams optional, contains additional request body for processor
     * @param String $urlParams optional, url params for the request
     * @return Array API response
     */
    public function create($requestData, $backgroundProcess=false, $additionalParams=[], $urlParams='');
    
    /**
     * Method for retrieving all the records from a given endpoint using the POST search method
     * @param Json $params optional request content
     * @param String $filters optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAll($params=null, $filters='', $retrieveAll=true, $escapeSlashes=false);
}