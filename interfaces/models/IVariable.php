<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\interfaces\models;


interface IVariable extends IBaseModel
{

    /**
     * Checks if a string(haystack) starts with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function startsWith($haystack, $needle);
    
    /**
     * Checks if a string(haystack) ends with a specific string(needle)
     * 
     * @param string haystack string to search in
     * @param string needle string to check
     * @reference https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public function endsWith($haystack, $needle);

    /**
     * @inheritdoc
     */
    public function rules();

    /**
     * @inheritdoc
     */
    public function attributeLabels();

    /**
     * Retrieve variable information in view variable
     *
     * @param $id integer variable identifier
     * @return $data array variable information
     */
    public function getVariableInfo($id);

    /**
     * Sort array by value
     *
     * @param $arr array array to be sorted
     * @param $col text column to be sorted
     * @param $dir text directing of sorting
     */
    public function arraySortByColumn($arr, $col, $dir = SORT_ASC);

    /**
     * Process ordering of data to be displayed
     *
     * @param $arr list of attributes
     * @param $order order of display of attributes
     * @return $array ordered list of attributes
     */
    public function processOrderOfData($arr, $order);

    /**
     * Get variable label by abbrev
     *
     * @param $abbrev text variable abbreviation
     */
    public function getAttrByAbbrev($abbrev,$attr='label');

    /**
     * Get variable info by abbrev
     *
     * @param $abbrev text variable abbrev
     * @return $data array variable value
     */
    public function getVariableByAbbrev($abbrev, $fields = []);

    /**
     * Retrive variable scales
     * 
     * @param $variableId varaible record identifier
     */
    public function getVariableScales($variableId);

    /**
     * Retrive tags for variable scale value - scale name pair
     * 
     * @param $scaleValues array list of scale values info
     */
    public function getVariableScaleTags($scaleValues);

    /**
     * Method for retrieving all the scale value records of a variable using the POST search method
     * @param Integer $variableDbId Variable identifier
     * @param Array $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @return Array the records from the database
     */
    public function searchAllVariableScales($variableDbId, $params = null, $filters = '', $retrieveAll = true);

    /**
     * Get variable attribute by abbrev
     *
     * @param text $abbrev abbreviation of the variable
     * @param text $attribute attribute to be retrieved
     *
     * @return text variable attribute value
     */
    public function getAttributeByAbbrev($abbrev, $attribute);
}