<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\interfaces\models;

interface IExperiment extends IBaseModel
{
    /**
     * Get experiment browser data provider
     * 
     * @param array $params optional search filters
     * @param array $pageParams data browser parameters
     * @param string $currentPage current page parameter
     * @param boolean $copyView flag if the browser is for entry list
     * @return mixed $dataProvider includes experiment attributes and data provider
     */
    public function getDataProvider($params = null, $pageParams = '', $currentPage = null, $copyView = false);

    /**
     * Format experiment statuses
     * 
     * @param array $status list of distinct statuses
     */
    public function formatExperimentStatus($status);

    /**
     * Format background worker statuses
     * 
     * @param Integer $bgId background worker identifier
     */
    public function formatBackgroundWorkerStatus($bgId);

    /**
     * Search for experiments given search parameters
     *
     * @param array $params list of search parameters
     * @return array $data list of experiments given search parameters
     */
    public function searchRecords($params = null);

    /**
     * Search for experiments given search parameters
     *
     * @param array $params list of search parameters
     * @param string $pageParams data browser parameters
     * @param boolean $copyView flag if the browser is for entry list
     * @return array $data list of experiments given search parameters
     */
    public function searchBrowserRecords($params = null, $pageParams = '', $copyView = false);

    /**
     * Retrieve existing experiments
     * @param filter array resource call filter
     * @param sort array resource call sort
     * @return array experiment record in the database
    */
    public function getExperiments($filter = null, $sort = null);
    

     /**
     * Retrieves an existing expriment in the database
     * @param $id integer experiment identifier 
     * @return array experiment record in the database
     */
    public function getExperiment($id);
    

    /**
     * Create new records of an experiment
     * @param $params array experiment record to create
     * @return $array array containing result of the saving and the experiment id
     */
    public function createExperiment($params);

    /**
     * Create new records of an experiment's data
     * @param $id integer experiment ID
     * @param $params array of experiment data to save
     * @return $result boolean status of the saving
     */
    public function createExperimentData($id, $params);

    /**
     * Update Experiment
     * @param $id integer experiment ID
     * @param $params array of experiment to update
     * @return $result boolean status of the saving
     */
    public function updateExperiment($id, $params);

    /**
     * Method for retrieving all the entry data records from a given a specific experiment id
     * 
     * @param Integer $dbId experiment record identifier
     * @param Json $params optional request content
     * @param String $filters  optional sort, page or limit options
     * @param Boolean $retrieveAll optional retrieve all data
     * @return Array the records from the database
     */
    public function searchAllEntries($dbId, $params = null, $filters = '', $retrieveAll = true);

    /**
    * Retrieve entry information in view experiment
    *
    * @param $id integer experiment identifier
    * @param $crossSearchModel string Cross Search Model
    * @return $data array experiment information
    */
    public function getExperimentInfo($id, $crossSearchModel = null);

    /**
     * Retrieves experiment info in the format
     * applicable to select2 elements
     * @param requestBody - search parameters
     * @param urlParams - sort, limit, and page
     */
    public function getExperimentSelect2Data($requestBody = null, $urlParams = '');
}
