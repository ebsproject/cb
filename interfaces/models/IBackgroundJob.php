<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\interfaces\models;

interface IBackgroundJob extends IBaseModel
{
    /**
     * Update background process records
     * @param array $records List of records of backgroundProcessDbId to be updated
     * @param array $params List of fields to be updated
     * @return array results success or error
     */
    public function update($records, $params);

    /**
     * Format the background job records into dropdown selections (generic version)
     * @param array backgroundJobs Array of background job records
     * @return string formatted HTML elements
     */
    public function getGenericNotifications($backgroundJobs);

    /**
     * Format the background process records into dropdown selections
     * @param $backgroundProcesses Array of background process records
     * @return string formatted HTML elements
     */
    public function getNotifications($backgroundProcesses);

    /**
     * Returns plural form of text. 
     * Pls note: This is slightly different from that of the pluralize function of BrowserController
     * @param integer $count given count
     * @param string $text given text
     * @return string plural form of text
     */
    function pluralize($count, $text);

    /**
     * Returns a single number of years, months, days, hours, minutes or seconds 
     * between the current date and the provided date
     * @param string $timestamp Given timestamp
     * @return string interval of date
     */
    function ago($timestamp);

    /**
     * Format the background process records into dropdown selections
     *
     * @param Array $backgroundProcesses List of background process records
     * @param String $fieldParam Fields param when retrieving entities in background job transaction
     * @param String $filterFieldParam Field to be used when retrieving an entity
     * @param String $displayField Field to be displayed in the notifications
     * @return String $transactions Formatted HTML elements for transactions
     */
    public function getBackgroundJobNotifications($backgroundProcesses, $fieldParam, $filterFieldParam, $displayField);
}