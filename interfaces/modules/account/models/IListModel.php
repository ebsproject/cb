<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\interfaces\modules\account\models;


interface IListModel
{
    /**
     * Validate if split is possible or valid
     * @param Integer $listId id of list to be split
     * @param Integer $childListCount number of expected child lists
     * @param String $splitOrder order of split
     * @return Array if transaction is valid, error message 
     */
    public function validateSplit($listId,$listType,$childListCount,$splitOrder,$listMemberCount);

    /**
     * Update list info
     * @param Integer $listId ID of the list
     * @param String $processRemarks remarks identifying the update
     * @param Integer $listMemberId id of list member
     */
    public function updateListInfo($listId,$processRemarks,$listMemberId=null);

    /**
    * Get all user options for share list feature
    * @param integer $listId ID of list
    * @param integer $userId ID of user
    * @param integer $teamId ID of team
    * @param string $return if return should be tags
    * @return array of users
    **/
    public function getUsers($listId, $userId, $teamId=null,$return='tags');

    /**
    * Get all team options for share list feature
    * @return array containing the team ID and team name
    **/
    public function getTeams();

    /**
     * Validates the inputted names/abbrev for adding to list
     * @param Array $inputList contains the list of names
     * @param Integer $id ID of the list
     * @param String $listType the type of the list
     * @param String $program abbrev of the program
     * @return Array return the needed data for rendering the view file 
     */
    public function validateInputList($inputList, $id, $listType, $program);

    /**
    * Validate input for creating lists with type Trait
    * Inputs for trait lists will be the name of the trait
    * @param array $inputListSplitArr array containing the input values
    * @return array if list is valid
    **/
    public function validateTraitInput($inputListSplitArr);

    /**
    * Validate input for creating lists with type Germplasm
    * @param array $inputListSplitArr array of input values
    * @return array if list is valid and notification message
    **/    
    public function validateDesignationInput($inputListSplitArr);

    /**
     * Processes the data for inserting list members to a list
     * @param Integer $id ID of the list
     * @param String $listType type of the list
     * @return String notification message
     */
    public function addMembersByInput($id, $listType);
}
