<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace app\interfaces\modules\account\models;


interface IListMemberModel
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param string $type list type
     * @param integer $id list ID
     * @param boolean $is_void if list items should be voided or not
     * @param array $params contains the different filter options
     * @param boolean $pagination whether the pagination of the browser should be set
     * @param boolean $isView whether the intended target for the dataprovider is the modal view or not
     * @param string $searchModel the name of the search model that is used for filtering
     *
     * @return ArrayDataProvider
     */
    public function search(string $type=null, $id, $isVoid=null, $params=null, $pagination=false, $isView=false, $searchModel='List');
}
