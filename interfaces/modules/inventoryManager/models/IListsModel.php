<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for IListsModel class.
 */

namespace app\interfaces\modules\inventoryManager\models;

use app\interfaces\models\ILists;

interface IListsModel extends ILists {
    /**
     * Retreves data to be displayed in the lists browser
     * @param Array params browser parameters
     * @return ArrayDataProvider browser data provider object
     */
    public function search($params);

    /**
     * Assemble the URL parameters for 
     * pagination and sorting of the browser
     * @param Boolean resetPage to reset pagination or not
     * @return String URL parameters in string format
     */
    public function assembleUrlParameters($resetPage = false);

    /**
     * Assembles the browser filters for API retrieval
     * @param Array params query parameters
     * @return Array containing filters for API retrieval
     */
    public function assembleBrowserFilters($params);

    /**
     * Retrieves the list abbrev given its ID
     * @param Integer listDbId list identifier
     * @return String list abbrev
     */
    public function getListAbbrev($listDbId);
}