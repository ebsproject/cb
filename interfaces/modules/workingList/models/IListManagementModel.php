<?php
/**
 * This is the interface for WorkingListModel class
 */

namespace app\interfaces\modules\workingList\models;

interface IListManagementModel {

    /**
     * Update order of list items given sort configuration
     * 
     * @param String $sort sort configuration
     * @param Integer $listId list identifier
     * 
     * @return Integer update process status
     */
    public function updateListItemOrder($sort,$listId);

    /**
     * Remove list items
     * 
     * @param Integer $listItemIds list item ID
     * @param Boolean $bgprocess background process 
     * 
     * @return Integer update process status
     */
    public function removeItems($listItemIds,$bgprocess);  

    /**
     * Build export file columns
     * 
     * @param Array $variables variable array
     * 
     * @return Array columns
     */
    public function buildExportColumns($variables);

    /**
     * Build export CSV file content
     * 
     * @param Array $columns csv file columns
     * @param Array $listItems working list items
     * 
     * @return Array csv content
     */
    public function buildCSVContent($columns,$listItems);

    /**
     * Get ATTRIB_EQUIV values
     */
    public function getAttribEquiv();
}
?>