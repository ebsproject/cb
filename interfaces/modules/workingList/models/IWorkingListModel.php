<?php
/**
 * This is the interface for WorkingListModel class
 */

namespace app\interfaces\modules\workingList\models;

interface IWorkingListModel {

    /**
     * Retrieves working list record given the user id and list type
     * 
     * @param Integer $userId user identifier
     * @param String $listType type of working list
     * 
     * @return Array working list information
     */
    public function getWorkingList($userId,$listType);

    /**
     * Create new working list record
     * 
     * @param Integer $userId user identifier
     * @param String $listType type of working list
     * 
     * @return Integer status of creating new working list
     */
    public function createWorkingList($userId,$listType);

    /**
     * Retrieve variable configuration record
     * 
     * @param String $programCode program code value
     * 
     * @return Array config-based variable information
     */
    public function getVariableConfiguration($programCode);

    /**
     * Validate variables if existing in database
     * 
     * @param Array $variables config-based variable information
     * 
     * @return Array validated variables
     */
    public function validateVariables($variables);

    /**
     * Build browser columns from config-based variables
     * 
     * @param Array variable information
     * 
     * @return Array browser columns
     */
    public function buildBrowserColumns($variables);

    /**
     * Main search function for Working List members
     * 
     * @param Onject $param search parameters
     * @param Integer $listId working list identifier
     * @param String $listType type of working list
     * 
     * @return mixed data provider for main working list browser
     */
    public function search($params,$listId,$listType);

    /**
     * Build filters for retrieving data provider
     * 
     * @param String $gridId browser identifier
     * @param Boolean $reset if need to reset data, default valu is FALSE
     * 
     * @return String filter parameter
     */
    public function assembleFilters($gridId,$reset=false);

}
?>