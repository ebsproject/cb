<?php
/**
 * This is the interface for FindSeedListModel class
 */

namespace app\interfaces\modules\seedInventory\models;

interface IFindSeedListModel {

    /**
     * Add items to platform.list_member
     * @param table name list id user id
     */
    public function addListItems($tableName, $listId, $userId);

    /**
     * Check if the temp table is existing
     *
     * @param string $tableName name of the temp table
     *
     * @return boolean
     */
    public function checkIfTableExistWithoutCol($tableName,$userId);

    /**
     * Check seeds if in working list
     *
     * @param mixed selected ids user id
     */
    public function validateSeedIds($selectedIds, $userId,$existingWorkingList);

    /**
     * Retrieve package label and id
     * @param Array list items
     * @return Array package data
     */
    public function retrievePackageDataById($newListItems);

    /**
     * Insert values to the temporary table created
     * @param Array list items
     * @param Integer list id
     * @return Boolean insert status
     */
    public function insertSelectedItemsToWorkingList($newListItems,$listId);

    /**
     * Insert selected items using a background job
     * @param Integer list id
     * @param Array search parameters 
     */
    public function insertItemsInBackground($listId,$searchParams);

    /**
     * Insert values to the temporary table created
     * @param Insert values to the temporary table created
     * @param text $selectedIds selected studies
     */
    public function insertTempValuesFromSeedList($newListItems,$listId);

    /**
     * Builds the prerequisite parameter for the processor
     * @param array $data contains the request data for the endpoint
     * @param integer $listId ID of the list
     * @param integer $totalCount total count of the records to be inserted
     */
    public function insertProcessorWithPrereq($data, $listId, $totalCount);

    /**
     * processor implementation for inserting package records to list
     * @param boolean api call status
     */
    public function insertProcessor($newItemRecords=[],$listId=null,$prereq=[],$totalCount=0);

    /**
     * Delete values to the temporary table created
     * @param array $selectedRemoveIds selected studies
     */
    public function deleteTempValuesFromSeedList($selectedRemoveIds, $userId,$itemCount);

    /**
     * Get working list browser data provider
     * @return mixed
     */
    public function getListDataProvider($userId);

    /**
    * Retrieve saved lists of a certain type for working list
    * @param mixed user id list type
    * @param array data provider items count
    **/
    public function getSavedSeedListDataProvider($userId,$listType);

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function getSavedListById($id);

    /**
    * Retrieve saved list information
    * @param mixed list id
    * @param array list data
    **/
    public function getSavedListData($id);

    /**
    * Retrieve saved list items by id
    * @param mixed list id
    * @param array list items
    **/
    public function getSavedSeedListById($id);

    /**
     * Save list basic information
     * @param array list info
     * @return mixed api call return
     */
    public function saveListBasicInformation($listInfo);

    /**
     * Removes the package IDs in the array if existing already in working list
     * @param Integer $listDbId ID of the working list
     * @param Array $idArray contains the package db ids
     * @return Array array of the package Ids that have been filtered out
     */
    public function removeDuplicates($listDbId, $idArray);

    /**
     * Retrieve working list items
     * @param integer user id
     * @return array data total count
     */
    public function retrieveWorkingList($userId);

    /**
     * Clear working list items
     * @param integer user id
     * @return boolean status
     */
    public function clearWorkingList($userId);

    /**
     * update list record information
     * @param mixed id params
     * @return array mixed
     */
    public function updateListRecord($listDbId,$params);

    /**
     * Create working list
     * @param integer user id
     * @return array working list
     */
    public function createWorkingList($userId);

    /**
     * Saves the items from the working list to a final list
     * @param Array $data contains the request data with the list ID and list name
     * @return Array contains whether the operation is succesfull with a notification message
     */
    public function saveListItems($data, $userId);

    /**
     * Retrieves the total count of the items in a working list
     * @return Integer total record count
     */
    public function getItemsCount();

}
?>