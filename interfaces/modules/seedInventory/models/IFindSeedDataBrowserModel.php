<?php
/**
 * This is the interface for FindSeedDataBrowserModel class
 */

namespace app\interfaces\modules\seedInventory\models;

interface IFindSeedDataBrowserModel {

    /**
    *   Set dynamic columns
    * @param $config array 
    */
    public function setColumnsForFiltering($config=null);

    /**
     * Retrieve columns from configuration dataset
     * @return array Array of configuration
     */
    public function getColumns();

    /**
     * Check and sort variables with existing mariable information in the database
     * @param array config variables
     * @return array config variables
     */
    public function validateColumnConfigVariables($records);

    /**
     * Get search DataProvider
     * @return mixed
     */
    public function getDataProvider($condition, $condStr=null, $columns=null, $inputListOrder=false, $browserFilters=[], $getAllResponse=false, $getParamsOnly=false);
    
    /**
     * Retrieve program code
     * @param array program value
     * @return string program code
     */
    public function getProgramCode($programIds);

    /**
     * Process variable filter values
     * @param array passed filters
     * @return array variable filter
     */
    public function processFilters($passedFilters);

    /**
     * Retrieve entry data
     * 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function search($params, $dataFilters, $inputListOrder);

    /**
     * Retrieve search dquery parameters
     * needed in query builder 
     * @param $params array list of current search parameters
     * @return $dataProvider array storage data provider
     */
    public function getSearchData($params, $dataFilters, $inputListOrder);
} 
?>