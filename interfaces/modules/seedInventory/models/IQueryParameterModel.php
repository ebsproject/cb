<?
/**
 * This is the interface for QueryParameterModel class
 */

namespace app\interfaces\modules\seedInventory\models;

interface IQueryParameterModel {

    /**
     * Validate query parameter
     * @param mixed name type data
     * @return array api call return
     */
    public function getQueryParam($name,$type,$data);

    /**
     * Get the query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsDataProvider($userId, $toolId);

    /**
     * Save query parameters
     *
     * @return Array $result Array of query parameters
     */
    public function saveQueryParam($queryParamRecord);

    /**
     * Get the query parameters by id
     *
     * @return Array $result Array of query parameters
     */
    public function getQueryParamsById($paramId,$userId,$appTool);

}