<?php
/**
 * This is the interface for WorkingList class
 */

namespace app\interfaces\modules\seedInventory\models;

interface IWorkingList {

    /**
     * Retrievs the working list for find seeds
     * @param $userId integer ID of the current user
     * @return array contains the attribute of the working list
     */
    public function getWorkingList($userId);

    /**
     * Retrievs the current working list member
     * @param integer integer ID of the current working list
     * @return array contains list member data
     */
    public function getWorkingListMembers($listDbId);

    /**
     * Search method for find seeds working lsit
     * @param $listDbId integer ID of the working list
     */
    public function search($listDbId);

}
?>