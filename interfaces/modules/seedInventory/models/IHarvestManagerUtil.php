<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for variable filter component
 */

namespace app\interfaces\modules\seedInventory\models;

interface IHarvestManagerUtil
{   
    /**
     * Retrieve variable label that matches the field name
     * @param fieldName - name of field from api
     * @return label - retrieved variable label matching the field name
     *               - if no variable matched the field name, the field name is return as label
     */
    public function getDisplayName($fieldName);

    /**
     * Retrieve location details
     */
    public function getLocationDetails($locationId,$experimentId);

    /**
     * Generate Query Parameter Filter Fields
     */
    public function generateFilterFields($userId, $variableFilters, $isBasic);

    /**
     * Retrieve display text of parameter's initial value
     *
     */
    public function getFilterValuesById($filter);

    /**
     * Retrieve data for variable filters
     *
     * @param String $q Select2 search text
     * @param Integer $userId user current user identifier
     * @param Array $variableFilters list of variable filters
     * @param Array $defaultFilters list of default search filters
     * @param Integer $limit number of displayed options
     * @param Integer $page page number
     * 
     * @return Array $data list of filtered data
     */
    public function getVariableFilterData($q, $userId, $id, $variableFilters, $defaultFilters, $limit, $page);

    /**
     * Retrieve source experiment of location
     */
    public function getLocationSourceExperiment($locationId);

    /**
     * Process variable filter values
     * @param array passed filters
     * @return array variable filter
     */
    public function processFilters($passedFilters);

    /**
     * Returns generated Select2 html fields
     *
     * @param name variable id data source elemenet value required string label size
     */
    public function generateHTMLElements($name, $varId, $data, $val, $reqStr, $label, $size, $removeFxn, $initVal, $inputType);
}