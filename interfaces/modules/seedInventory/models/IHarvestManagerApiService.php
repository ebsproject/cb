<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for variable filter component
 */

namespace app\interfaces\modules\seedInventory\models;

interface IHarvestManagerApiService
{
    /**
     * Retrieve harvest manager configuration
     */
    public function getConfiguration($abbrev);

    /**
     * Retrieve plot browser configuration
     */
    public function getPlotConfiguration($abbrev);

    /**
     * Retrieve harvest manager configuration
     */
    public function getPackageLabelConfiguration();

    /**
     * Retrieve programs accessible to user/person
     */
    public function getUserPrograms($limit, $page);

    /**
     * Retrieve all programs where the current user belongs to
     */
    public function getAllUserPrograms();

    /**
     * Retrieve experiments
     */
    public function getExperiments($requestBody, $limit, $page);

    /**
     * Retrieve occurrences
     */
    public function getOccurrences($requestBody);

    /**
     * Retrieve occurrences with pagination
     * @param array requestBody - API request body
     * @param integer limit - limit of records to retrieve
     * @param integer page - page of records to retrieve
     */
    public function getOccurrencesPaginated($requestBody, $limit, $page);

    /**
     * Retrieve location occurrence groups
     */
    public function getLocationOccurrenceGroups($requestBody);

    /**
     * Retrieve locations by location ids
     */
    public function getLocationsByIds($requestBody);

    /**
     * Retrieve locations
     */
    public function getLocations($requestBody, $limit, $page);

    /**
     * Retrieve a program using its id
     */
    public function getProgramById($programId);

    public function getProgramInfoById($programId);

    /**
     * Retrieve a experiment using its id
     */
    public function getExperimentNameById($experimentId);

    /**
     * Retrieve a location using its id
     */
    public function getLocationNameById($locationId);

    /**
     * Retrieve plots of a location per page
     */
    public function getPlotsByLocation($locationId,$requestBody,$queryParams);

    /**
     * Retrieve packages of a location per page
     */
    public function getPackagesByLocation($locationId,$requestBody,$queryParams);

    /**
     * Retrieve all plots of a location
     */
    public function getAllPlotsByLocation($locationId,$requestBody,$urlParams=null);

    /**
     * Retrieve harvest data of a location per page
     */
    public function getHarvestDataByLocation($locationId,$requestBody,$queryParams,$dataLevel='plot');

    /**
     * Retrieve all harvest data of a location
     */
    public function getAllHarvestDataByLocation($locationId,$requestBody,$urlParams=null,$dataLevel='plot');

    /**
     * Retrieve a plots by plot db ids
     */
    public function getPlotsByPlotDbId($plotDbIdArray);

    /**
     * Retrieve plots with status IN_QUEUE or IN_PROGRESS
     */
    public function getPlotsHarvestStatusReady($locationId,$experimentId);

    /**
     * Retrieve plots with status DONE
     */
    public function getPlotsHarvestStatusProcessed($locationId,$experimentId);

    /**
     * Retrieve plots with status IN_QUEUE, IN_PROGRESS, or DONE
     */
    public function getPlotsHarvestStatusTotal($locationId,$experimentId);

    /**
     * Retrieve a package by package db ids
     */
    public function getPackageByPackageDbId($packageDbId);

    /**
     * Delete a package and update notes
     */
    public function deletePackage($packageDbId,$notes);

    /**
     * Retrieve stage of location
     */
    public function getLocationStage($locationId);

    /**
     * Retrieve terminal transaction id of a location (if transaction exists)
     */
    public function getTerminalTransactionId($locationId,$userId,$any=null);

    /**
     * Create terminal transaction for a location and return its transaction id     
     */
    public function createTerminalTransaction($locationId);

    /**
     * Adds dataset to transaction
     * @param transactonId - transaction id
     * @param plotIds - array containing plot ids
     * @param variableAbbrevArray - array containing variable abbrevs
     * @param values - array containing dataset values
     * @return boolean - whether or not the datasets were successfully added     
     */
    public function addDataSetToTransaction($transactionId,$plotIds,$variableAbbrevArray,$values,$dataLevel);

    /**
     * Deletes transactions if there is no transaction dataset recor
     * @param transactionDbId transaction DB identifier
     */
    public function deleteTransaction($transactionDbId);

    /**
     * Update Package Quantity and Unit
     * @param packageDbId - id of package
     * @param packageFields - fields to update
     * @param values - values of field to update
     * return true if successfull , false if not     
     */
    public function updatePackageData($packageDbId,$packageFields,$values);

    /**
     * Retrieve datasets of transaction
     * @param transactionId - id of transaction
     * @return datasets     
     */
    public function getTransactionDataSets($transactionId);

    /** 
     * This function retreives packages
     * return $data
     */
    public function getPackages($locationId,$filters);

    /**
     * This method retrieves the variable id given an abbrev
     * @param $abbrev - variable abbrev
     * @return $variableDbId - variable  id
     */
    public function getVariableIdByAbbrev($abbrev);

    public function getProgramCropCode($programDbId);

    /**
     * This method retrieves the variable details
     * @param $variableDbId - variable id
     * @return $variableDetails - array containing variable detials     
     */
    public function getVariableDetails($variableDbId);
    
    /**
     * retrieves location occurence group where supplied location id belongs to
     * @param locationDbId
     * @return locatonOccurrenceGroups - array containing location occurrence group
     */
    public function getLocationOccurrenceGroupByLocationId($locationDbId);

    /**
     * retrieves location harvest summary 
     * @param locationDbId
     * @return locatonOccurrenceGroups - array containing location occurrence group
     */
    public function getLocationHarvestSummaries($locationDbId,$experimentId);

    /**
     * retrieves Occurrence info of the supplied occurrence id
     * @param occurenceDbId
     * @return occurrences - array containing occurence
     */
    public function getOccurrenceById($occurrenceDbId);

    /**
     * retrieves Occurrence info of the supplied occurrence id
     * @param occurenceDbId
     * @return occurrences - array containing occurence
     */
    public function getExperimentById($experimentDbId);

    /**
     * Creates a seed record given the seed info
     * @param seedInfo
     * @return seedDbId - id of created seed record     
     */
    public function createSeedRecord($seedInfo);

    /**
     * get seed record associated with given plot id
     * @param sourcePlotDbId
     * @return seedDbId
     */
    public function getSeedDbId($sourcePlotDbId);

    /**
     * get seed record with given germplasm id
     * @param germplasmDbId
     * @return seedDbId
     */
    public function getSeedDbIdGivenGermplasm($germplasmDbId);

    /**
     * get seed id of seed with the given seed code
     * @param seedCode
     * @return seedDbId
     */
    public function getSeedDbIdBySeedCode($seedCode);

    /**
     * Creates a package record given the package info
     * @param packageInfo - array
     * @return packageDbId - id of created package record     
     */
    public function createPackageRecord($packageInfo);

    /**
     * Retrieve package id associated with the given seed id
     * @param seedDbId 
     * @return packageDbId 
     */
    public function getPackageDbId($seedDbId);

    /**
     * Delete a dataset given its ID     
     */
    public function deleteDataset($datasetId);

    /**
     * Retrieve dataset id using plot id and variable id
     */
    public function getDatasetId($plotDbId,$variableDbId,$transactionDbId);

    /**
     * Retrieve datasets of plot
     */
    public function getDatasets($plotDbId,$variableDbIds,$transactionDbId);


     /**
     * Retrieve dataset ids using plot id
     */
    public function getDatasetIdsOfPlot($plotDbId,$transactionDbId);

    /**
     * Retrieve information of germplasm given its ID     
     */
    public function getGermplasmInfo($sourceGID);

    /**
     * Retrieve germplasm ID given its designation
     */
    public function getGermplasmId($designation);

    /**
     * Retrieve germplasm names
     */
    public function getGermplasmNames($desigStr);

    /**
     * Retrieve germplasm names
     */
    public function getGermplasmNamesByGermplasmId($germplasmDbId);


    /**
     * Create germplasm and germplasm name record    
     */
    public function createGermplasmRecord($record);

    /**
     * Update germplasm record     
     */
    public function updateGermplasmRecord($germplasmDbId,$requestBody);
    
    /**
     * Retrieve a package unit     
     */
    public function getPackageUnit();

    /**
     * Delete seed given seed id
     * @param plotDbId
     */
    public function deleteSeeds($seedDbId);

    /**
     * Delete germplasm record given germplasmDbId
     * @param plotDbId
     */
    public function deleteGermplasmRecord($germplasmDbId);

    /**
     * Delete germplasm record given germplasmDbId
     * @param plotDbId
     */
    public function deleteGermplasmNames($germplasmNamesDbId);

    /**
     * Get sourceplot dbid  given ploId
     * @param plotDbId
     */
    public function getSourcePlotDbId($plotDbId);

    public function bulkDeleteCommitted($dbIds);

    /** 
     * Commit Transaction
     */
    public function commitTransaction($transactionId, $datasetCount);

    /**
     * Retrieve plot data of given plot
     * @param plotDbId - plot identifier
     * @param variableDbIds - variable ids of data to retrieve
     * @return - array containing cross data records
     */
    public function getPlotData($plotDbId, $variableDbIds);

    /**
     * Retrieve cross data of given cross
     * @param crossDbId - cross identifier
     * @param variableDbIds - variable ids of data to retrieve
     * @return - array containing cross data records
     */
    public function getCrossData($crossDbId, $variableDbIds);

    /**
     * Retrieve plot data of given plot
     * @param plotDbId
     */
    public function getEntries($seedDbId);

    /**
     * Get backround job info
     * @param id - id of the background job
     */
    public function getBackgroundJob($id);

    /**
     * Search backround process by id
     * @param id - id of the background process
     */
    public function backgroundProcessesSearchById($id);

    /**
     * Search backround process by id
     * @param id - id of the background process
     */
    public function backgroundProcessesSearchByUser($userId);

    /**
     * Function for bulk operations using processor
     * @param httpMethod - HTTP method to be used
     * @param endpoint - endpoint where bulk operation will be performed
     * @param dbIds - array containing database identifiers of records included in the bulk operation
     * @param entity - the entity where the record belongs (eg. germplasm)
     * @param entityDbId - entity identifier
     * @param endpointEntity - the specific endpoint in the entity (eg. seed, package, etc.)
     * @param application - the application performing the bulk operation (eg. Harvest Manager)
     * @param optional - optional data for the bulk operations (eg. dependents, requestData)
     */
    public function processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional = []);

    /**
     * Processor function
     */
    public function processorOperation($requestBody);

    /** 
     * 
     * function for bulk operations using broker
     */
    public function bulkBuilder($dbIds,$endpoint,$method,$requestData=null);

    /**
     * Broker function
     */
    public function bulkOperation($requestBody);

    /**
     * get all data of havrest manager filter
     */
    public function getFilterData($filter);

    /**
     * Retrieve packages of seed
     */
    public function seedPackagesSearch($seedDbId,$requestBody=null);

    /**
     * Retrieve entries of seed
     */
    public function seedEntriesSearch($seedDbId,$requestBody=null);

    /**
     * Retrieve germplasm names of a germplasm
     */
    public function germplasmNamesSearch($germplasmDbId,$requestBody=null);

    /**
     * Retrieve seed records
     */
    public function seedsSearch($requestBody = null,$queryParams = null);

    /**
     * Retrieve seed records
     */
    public function packagesSearch($requestBody = null,$queryParams = null);

    /**
     * Retrieve germplasmName records
     */
    public function germplasmNamesSearchAll($requestBody = null,$queryParams = null);
}