<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for transaction
 */

namespace app\interfaces\modules\dataCollection\models;

interface ITransaction
{
    /**
     * Retrieve variable given the variableDbId
     * @param array $param search parameters
     * @return array variable record
     */
    public function getVariable($param);

    /**
     * Deletes dataset records
     * @param Integer $transactionDbId Transaction identifier
     * @return array Contains success and/or error message
     */
    function deleteDataset($transactionDbId);

    /**
     * Creates file record
     * @param Integer $transactionDbId Transaction identifier
     * @param String $status Committed or invalid
     * @return array Contains success and/or error message
     */
    function createTransactionFile($transactionDbId, $status);
    /**
     * Commits transaction dataset records
     * @param Integer $transactionDbId Transaction identifier
     * @return array Contains success and/or error message
     */
    function commitPlotData($transactionDbId);
    /**
     * Suppress or unsuppress records
     * @param Array $params parameters for suppressing or unsuppressing records
     * @return Json Contains success and/or error message
     */
    public function suppressRecord($params);

    /**
     * Renders summary of data on suppress/unsuppress action
     * @param Array $params parameters for suppressing or unsuppressing records
     * @return Html _suppress_summary view
     */
    public function getSuppressRecordSummary($params);

    /**
     * Renders summary of data on suppress/unsuppress action for suppression rules
     * @param Integer $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $suppress true or false
     * @param String $dataLevel plot
     * @return Html  _suppress_summary view
     */
    public function getSuppressRuleSummary($variableId, $transactionId, $suppress, $dataLevel);
    /**
     * Suppress data by rules
     * @param Integer $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $suppress true or false
     * @param String $dataLevel plot
     * @return Json success and/or error message
     */
    public function suppressByRule($variableId, $transactionId, $suppress, $dataLevel);
    /**
     * Suppress data by rules
     * @param Integer $variableId target variable ID to be suppressed
     * @param Integer $transactionId transaction ID
     * @param String $dataLevel plot
     * @return Json success and/or error message 
     */
    public function createSuppressRules($variableDbId, $transactionDbId, $dataUnit);

    /**
     * Get variable for each data unit
     * @param Array $datasetVariables List of all variables in the transaction
     * @return Array variables for each data unit
     */
    public function getDatasetVariables($datasetVariables);

    /**
     * Inserts datasets into the transaction in the data terminal
     * @param Integer transactionId transaction identifier
     * @param String variableAbbrev trait variable
     * @param String value trait value
     * @param Array recordIds array of record ids to update
     * @param String idType type of ids (plot/cross)
     * @return Boolean if creation of records is successful or not
     */
    public function insertDataset($transactionId, $variableAbbrev, $value, $recordIds, $idType = 'plot');
}