<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for field layout
 */

namespace app\interfaces\modules\harvestManager\models;

interface ICreationModel
{
    /**
     * Generates buttons for the creation module actions (commit, create, notifs)
     * @param boolean twoBrowsers whether or not two browsers are to be rendered
     * @return string html button elements
     */
    public function getActionButtons($twoBrowsers = false);

    /**
     * Determines which browser data must be loaded
     * @param string pageUrl current page URL string
     * @param array postParams $_POST parameters of the request
     * @return object containing loadPlots and loadCrosses boolean values
     */
    public function determineDataToLoad($pageUrl, $postParams);

    /**
     * Renders harvest data columns (harvest date and harvest method)
     * in the creation tab browsers.
     *
     * This method is responsible for rendering the specified harvest data column
     * with the provided terminal and committed values. It also takes into account
     * the model object from the data provider and whether the column is required.
     *
     * @param string $columnName The name of the column to render.
     * @param string $terminal The terminal (uncommitted) value for the variable.
     * @param string $committed The committed value for the variable.
     * @param object $model The model object from the data provider.
     * @param bool $required Whether the column is required.
     * @return string The rendered HTML for the harvest data column.
     */
    public function renderHarvestDataColumn($columnName, $terminal, $committed, $model, $required);

    /**
     * Renders the numeric variable column in the creation tab browsers.
     * @param string terminalMethod uncommitted harvest method
     * @param string committedMethod committed harvest method
     * @param object methodNumVarComat method numvar compatibility array
     * @param object numVars field names of each numeric variable abbrev
     */
    public function renderNumericVariableColumn($terminalMethod, $committedMethod, $methodNumVarCompat, $numVars, $model);

    /**
     * Determines if the plot or cross is valid depending on its available
     * harvest data (committed and uncommitted) and provides the appropriate remarks
     * based on the validity.
     * @param object browserConfig browser configurations
     * @param object record plot or cross record to be checked
     */
    public function harvestDataIsValid($browserConfig, $record);

    /**
     * Retrieves the creation summary and returns a data provider
     * containing the summary for the creation summary modal.
     * @param integer occurrenceId occurrence identifier
     * @param boolean harvestCross whether or not crosses will be harvested as well
     */
    public function getSummaryDataProvider($occurrenceId, $harvestCross);

    /**
     * Commits harvest data
     * @param integer occurrenceId occurrence identifier
     * @return array committedDataset commit response and dataset count check
     */
    public function commitHarvestData($occurrenceId);

    /** 
     * Build Harvest Data columns
     * 
     * @param string dataLevel data level of records to be processed ("plot", "cross")
     * @param integer occurrence ID occurrence record identifier
     * 
     * @return mixed harvest data columns
     */
    public function buildHarvestDataColumns($dataLevel, $occurrenceId);

    /**
     * Build Harvest Remarks column values for a given record
     * 
     * @param object record data record to be processed
     * 
     * @return object record with harvestRemarks values
     */
    public function buildHarvestRemarksColumn($record);

    /**
     * Extract supported harvest data from required data config
     * 
     * @param object harvest data requirements config
     * 
     * @return array supported data information
     */
    public function getSupportedHarvestData($harvestReqConfig);

    /**
     * Generate Creation tab browser columns based on harvest data requirements columns
     * 
     * @return array array of databrowser columns
     */
    public function generateBowserColumns();
}
