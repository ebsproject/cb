<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for field layout
 */

namespace app\interfaces\modules\harvestManager\models;

interface IApiHelper
{
    /**
     * Sends a request to the API.
     * This is used for API calls that do not have a model class.
     * (eg. locations/:id/harvest-plot-data-search)
     * @param string method HTTP method of the request
     * @param string endpoint API endpoint 
     * @param array requestBody array containing body parameters
     * @param array queryParams array containing URL parameters
     * @return array the result of the request
     */
    public function sendRequest($method, $endpoint, $requestBody = null, $queryParams = null);

    /**
     * Sends a request to the API (single page).
     * This is used for API calls that do not
     * have a model class (eg. locations/:id/harvest-plot-data-search)
     * @param string method HTTP method of the request
     * @param string endpoint API endpoint 
     * @param array requestBody array containing body parameters
     * @param array queryParams array containing URL parameters
     * @return array the result of the request
     */
    public function sendRequestSinglePage($method, $endpoint, $requestBody = null, $queryParams = null);

    /**
    * Use HTTP requests to GET, PUT, POST and DELETE data
    * 
    * @param string method request method (POST, GET, PUT, etc)
    * @param string path url path request
    * @param array rawData (optional) request content
    * @param string filter (optional) sort, page or limit options 
    * @param boolean retrieveAll (optional) whether all of the data will be fetched or not
    *
    * @return array list of requested information
    */
    public function getParsedResponse($method, $path, $rawData = null, $filter = '', $retrieveAll = false);

    /**
     * Function for bulk operations using processor
     * @param string httpMethod HTTP method to be used
     * @param string endpoint endpoint where bulk operation will be performed
     * @param array dbIds array containing database identifiers of records included in the bulk operation
     * @param string entity the entity where the record belongs (eg. germplasm)
     * @param integer entityDbId entity identifier
     * @param string endpointEntity the specific endpoint in the entity (eg. seed, package, etc.)
     * @param string application the application performing the bulk operation (eg. Harvest Manager)
     * @param string optional optional data for the bulk operations (eg. dependents, requestData)
     * @return object result of the API access
     */
    public function processorBuilder($httpMethod, $endpoint, $dbIds, $description, $entity, $entityDbId, $endpointEntity, $application, $optional = []);

    /**
     * Send request to the processor endpoint
     * @param object requestBody object containing request body parameters
     * @return object result of the API access
     */
    public function processorOperation($requestBody);
}