<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for field layout
 */

namespace app\interfaces\modules\harvestManager\models;

interface IOccurrenceDetailsModel
{
    /**
     * Retrieve variable display name that matches the field name
     * @param string fieldName name of field from api
     * @return string displayName retrieved variable display name matching the field name. If no variable matched the field name, the field name is returned as display name
     */
    public function getDisplayName($fieldName);

    /**
     * Retrieves the location id associated with the occurrence
     * @param integer occurenceId occurrence identifier
     */
    public function getLocationIdOfOccurrence($occurrenceId);

    /**
     * Retrieves the experiment id association with the occurrence
     * @param integer occurenceId occurrence identifier
     */
    public function getExperimentIdOfOccurrence($occurrenceId);
    
    /**
     * Get the harvest summary given the occurrence
     * @param integer $occurrenceId occurrence identifier
     */
    public function getHarvestSummary($occurrenceId);

    /**
     * Retrieve occurrence details
     * @param integer occurrenceId occurrence identifier
     */
    public function getOccurrenceDetails($occurrenceId);
}