<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Model class for field layout
 */

namespace app\interfaces\modules\harvestManager\models;

interface IPlotBrowserModel
{
    /**
     * Retrieve plot records to display
     * @param object params browser parameters
     * @param integer experimentId experiment identifier
     * @param integer  occurrenceId occurrence identifier
     * @param integer locationId location identifier
     * @return object containing data provider and array of germplasm states
     */
    public function search($params, $experimentId, $occurrenceId, $locationId);

    /**
     * Retrieves plots of the current occurrence in the given location
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function getPlotRecords($occurrenceId, $requestBody, $urlParams);

    /**
     * Retrieve plots given the request body and url parameters
     * @param integer occurrenceId occurrence identifier
     * @param object requestBody column filters
     * @param array urlParams url params for sorting, pagination, and page size
     */
    public function searchAllPlots($occurrenceId, $requestBody = null, $urlParams = null);

    /**
     * Retrieves the distinct values for the specified field of the plots
     * given the occurrence id
     * @param integer occurrenceId occurrence identifier
     * @param string field name of the field for which distinct values will be retrieved
     */
    public function getDistinctField($occurrenceId, $field);

    /**
     * Assemble browser filters
     * @param object params parameters passed by data browser
     * @param integer occurrenceId occurrence identifier
     * @return object columnFilters browser filters in API request body format
     */
    public function assembleBrowserFilters($params, $occurrenceId);
}