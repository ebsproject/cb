<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for IMaleListSearch class.
 */

namespace app\interfaces\models;
namespace app\interfaces\modules\experimentCreation\models;

interface IMaleListSearch {

     /**
     * Search model for male parents
     *
     * @param Array $params list of query parameters
     * @param String $filters set text of existing filters
     * @param String $currentPage current page parameter
     * 
     * @return Array $dataProvider male browser data provider
     */
    public function search($params, $filters = '', $currentPage = null);
}

?>