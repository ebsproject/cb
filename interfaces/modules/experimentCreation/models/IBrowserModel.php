<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for IBrowserModel class.
 */

namespace app\interfaces\models;
namespace app\interfaces\modules\experimentCreation\models;

interface IBrowserModel {
    /**
     * Updates the status of the experiment after background process is done
     * @param Array $experimentStatus Status of the experiment
     * @param Array $excludedStatusesArr List of Experiment that should not be included in the update
     * @param String $method Experiment step of the process
     * @param Integer $entityDbId background job entity ID
     */
    public function updateNotifExperimentStatus($experimentStatus,$excludedStatusesArr,$method,$entityDbId);
    
    /**
     * Update the experiment the experiment status from the background job as it changes
     * @param Integer @userId User ID 
     */
    public function updateExptBackgroundStatus($userId);
    
    /** 
     * For a partial match you can iterate the array and use a string search function like strpos().
     * @param Array $arr String array
     * @param String $keyword keyword to search
    */  
    public function partialArraySearch($arr, $keyword);

    /**
     * Status update of the background job for the occurrence step
     * @param Integer $id Experiment ID
     * @param Array $result Api call for the background job endpoint
     */
    public function bgOccurrenceStatusUpdate($id, $result);

    /**
     * Status update of the background job for the generation of plot records
     * @param Integer $id Experiment ID
     * @param Array $result Api call for the background job endpoint
     */
    public function plotRecordStatusUpdate($id, $result);
    
    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllInProgresStatus($experimentStatus);

    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllInQueueStatus($experimentStatus);

    /**
     * Remove all in-progress status
     * @param string $experimentStatus Current experiment status
     */
    public function unsetAllFailedStatus($experimentStatus);

    /**
     * Update the experiment status once the background process is done
     * @param integer $entityDbId Experiment ID
     * @param string $method Experiment step of the process
     */
    public function updateExperimentFinalStatus($entityDbId, $method);

}

?>