<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for IProtocolModel class.
 */

namespace app\interfaces\models;
namespace app\interfaces\modules\experimentCreation\models;

interface IProtocolModel {

    /**
	 * Retrieve planting protocol data
	 * 
	 * @param Integer $id experiment identifier
	 * @param String $program program code
	 * @param Integer $processId process identifier
	 * @param Array $model list of experiment info
	 * @param Integer $dashProgram dashboard program id
	 * 
	 * @return Array $data planting protocol data
	 */
    public function getPlantingProtocolData($id, $program, $processId, $model, $dashProgram);
    
    /**
	 * Retrieve plot types
	 * 
	 * @param Integer $id experiment identifier
	 * @param String $program program identifier
	 * @param Array $postData list of posted data
	 * 
	 * @return Array $data plot type data
	 */
    public function getPlotType($id, $program, $postData);

	/**
     * Retrieve protocol data stored in the experiment data
     * 
     * @param Integer $id experiment identifier
     * @param String $program program code
     * @param Integer $processId process identifier
     * @param Array $model list of experiment info
     * @param String Protocol abbrev to save
     * @return Array $data planting protocol data
     */
    public function getExperimentProtocolData($id, $program, $processId, $model, $protocolAbbrev);

	/**
	 * Auto compute of plot fields
	 * 
	 * @param Array $postData list of posted data
	 *
	 * @return Integer computed value
	 */
    public function computePlotFieldValue($postData);

    /**
	 * Retrieve plot variable values
	 * 
	 * @param Integer $id experiment identifier
	 * @param Array $defaultVariables list of default variables
	 */
	public function getPlotVariableValue($id, $defaultVariables);

	/**
	 * Validate if required protocol values were properly filled
	 * 
	 * @param Integer $id experiment identifier
	 * @param String $program program code
	 * @param Integer $processId process identifier
	 * @param Array $protocolChildren list of protocol sub tabs
	 */
	public function validateProtocol($id, $program, $processId, $protocolChildren);

	/**
	 * Retrieve plot field filter tags
	 * 
	 * @param Integer $id experiment identifier
	 * @param Array $postData list of posted data
	 */
	public function getPlotFieldFilterTag($id, $postData);

}

?>