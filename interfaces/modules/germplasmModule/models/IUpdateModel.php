<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for ICreateModel class.
 */

namespace app\interfaces\modules\germplasmModule\models;

interface IUpdateModel {

    /**
     * Returns key reference CONST value for variables
     *
     * @return string KEY_VARIABLES value
     */
    public function getKeyVariables();

    /**
     * Returns key reference CONST value for required variables
     *
     * @returns string KEY_VARIABLES_REQUIRED value
     */
    public function getKeyVariablesRequired();

    /**
     * Returns key reference CONST value for ACTION variables
     *
     * @returns string ACTION value
     */
    public function getModelAction();

    /**
     * Returns key reference CONST value for LEVEL variables
     *
     * @returns string LEVEL value
     */
    public function getModelLevel();

    /**
     * Returns key reference CONST value for TEMPLATE_FILE_NAME variables
     *
     * @returns string TEMPLATE_FILE_NAME value
     */
    public function getTemplateFileName();
}
?>