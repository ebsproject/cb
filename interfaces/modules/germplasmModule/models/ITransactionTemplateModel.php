<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for ConflictReportModel class.
 */

namespace app\interfaces\modules\germplasmModule\models;

interface ITransactionTemplateModel {

    /**
     * Returns key reference CONST value for entity germplasm
     *
     * @return string KEY_VARIABLES value
     */
    public function getEntityGermplasm();

    /**
     * Returns key reference CONST value for entity germplasm_attribute
     *
     * @return string KEY_VARIABLES value
     */
    public function getEntityGermplasmAttribute();

    /**
     * Returns key reference CONST value for template type germplasm
     *
     * @return string KEY_VARIABLES value
     */
    public function getGermplasmTemplate();

    /**
     * Returns key reference CONST value for template type germplasm_attribute
     *
     * @return string KEY_VARIABLES value
     */
    public function getGermplasmAttributeTemplate();

    /**
     * Returns key reference CONST value for germplasm module action update
     *
     * @return string KEY_VARIABLES value
     */
    public function getActionUpdate();

    /**
     * Returns key reference CONST value for selected template type
     *
     * @return string KEY_VARIABLES value
     */
    public function getTypeString($type = null);

    /**
     * Retrieve template type options configuration
     */
    public function getTypeOptionsConfig();

    /**
     * Retrieve template option contents
     * 
     * @param String $action germplasm module used
     * @param String $entity data level used
     * 
     * @return Array
     */
    public function getTemplateOptions($action,$entity = null);

    /**
     * Retrieve template headers
     * 
     * @param String $entity data level
     * @param String $action germplasm module used
     * @param String $userRole user role
     * @param String $program dashboard program
     * @param String $cropCode crop code for dashboard program
     * @param String $templateType template type selected
     * 
     * @return mixed
     */
    public function getTemplateHeaders($entity,$action,$userRole,$program,$cropCode,$templateType);

    /**
     * Retrieve items from selected saved list
     * 
     * @param String $listId uique list identifier
     * @param String $templateType selected template type
     * @param Array $headers extracted fiel headers
     * @param String $action germplasm module used
     * 
     * @return mixed
     */
    public function getListMemberData($listId, $templateType, $headers, $action);
}
?>