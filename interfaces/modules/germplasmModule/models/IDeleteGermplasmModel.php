<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the interface for IDeleteGermplasmModel class.
 */

namespace app\interfaces\modules\germplasmModule\models;

interface IDeleteGermplasmModel {

    /**
     * Delete Germplasm record
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasm($germplasmId);

    /**
     * Delete Germplasm name record/s for a given germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmName($germplasmId);

    /**
     * Delete Germplasm relation record/s for a germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmRelation($germplasmId);

    /**
     * Delete Germplasm attribute record/s for a germplasm
     * 
     * @param Integer $germplasmId germplasm record identifier
     * 
     * @return Boolean $success deletion process status
     */
    public function deleteGermplasmAttribute($germplasmId);
}
?>