#!/bin/bash

## Create directories
mkdir -p /var/www/html/uploads && \
chmod 777 -R /var/www/html/uploads && \
mkdir -p /var/www/html/analytics/input && \
mkdir -p /var/www/html/analytics/output && \
chmod 777 -R /var/www/html/analytics/input/ && \
chmod 777 -R /var/www/html/analytics/output && \
mkdir -p /var/www/html/analytics/cb-trial-designs && \
chmod 777 -R /var/www/html/analytics/cb-trial-designs && \
mkdir -p /var/www/html/temp/qrcode && \
chmod 777 -R /var/www/html/temp/qrcode && \
chmod 777 -R /var/www/html/runtime && \
chmod 777 -R /var/www/html/assets && \
mkdir -p /var/www/html/files/data_export && \
mkdir -p /var/www/html/files/attachments && \
chmod 777 -R /var/www/html/files

## Load env file
(cd config;printenv | egrep 'BA_API2_URL|BSO_API_URL|CB_ADMIN_EMAIL|CB_API_V3_URL|CB_DATA_BROWSER_DEFAULT_PAGE_SIZE|CB_DATA_BROWSER_MAX_PAGE_SIZE|CB_DEBUG_ENABLED|CB_RABBITMQ_HOST|CB_RABBITMQ_PASSWORD|CB_RABBITMQ_PORT|CB_RABBITMQ_USER|CB_REDIS_HOST|CB_REDIS_PORT|CB_SG_AUTH_URL|CB_SG_CLIENT_ID|CB_SG_CLIENT_SECRET|CB_SG_REDIRECT_URI|CB_SG_TOKEN_URL|CB_UNDER_MAINTENANCE|CB_URL|CB_VALID_EMAIL_DOMAINS|CS_API_URL|CS_PS_URL|EBS_SG_AF_API_URL|IDSITE|MATOMO_ENABLED|MATOMO_URL' > .env)

## Start apache
apachectl -D FOREGROUND