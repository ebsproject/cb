<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UserDashboardConfig;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
    * Displays error page
    *
    */
    public function actionFault()
    {
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();

            $this->view->params['statusCode'] = $statusCode;
            $this->view->params['message'] = $message;
            $this->view->params['name'] = $name;
            $this->layout = 'error';
            
            return $this->render('error');
         } else { 
            $this->layout = 'error'; 
            return $this->render('error'); 
        } 
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){

        $isUnderMaintenance = getenv('CB_UNDER_MAINTENANCE');

        //if site is under maintenance
        if($isUnderMaintenance == 'true' || $isUnderMaintenance == 'TRUE'){
            $this->layout = 'under_maintenance';
        }else{
            $this->layout = 'homepage';
        }

        return $this->render('homepage');
    }
    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays sample page.
     *
     * @return string
     */
    public function actionSample($tool,$vertical=null)
    {
        // return;
        $view = ($vertical == 't') ? 'vertical' : 'index';
        return $this->render($view, compact('tool'));
    }

    /**
     * Saves theme color to session
     *
     */
    public function actionSavethemecolor(){
        $themeColor = Yii::$app->request->post('theme_color');
        $darken = Yii::$app->request->post('darken');

        UserDashboardConfig::saveLayoutPreferences('themeColor',$themeColor);
        UserDashboardConfig::saveLayoutPreferences('darken',$darken);
    }

    /**
     * Get layout preferences from session
     *
     */
    public static function getLayout(){
        
        $layout = UserDashboardConfig::getLayoutPreferences();
        if(!empty($layout)){
            extract($layout);
        }

        $themeColor = (isset($themeColor) && !empty($themeColor)) ? $themeColor : 'teal';
        $darken = (isset($darken) && !empty($darken)) ? 'darken-'.$darken : '';
        $bodyClass = ((!empty($showSideNav) && $showSideNav == 'true') || !isset($showSideNav)) ? ' fixed-side-nav show-side-nav' : '';
        
        $showSideNav = (!empty($showSideNav) && isset($showSideNav)) ? $showSideNav : 'true';
        $sideNavClass = ((!empty($showSideNav) && $showSideNav == 'true') || !isset($showSideNav)) ? ' fixed ' : '';
        $sideNavSkin = (!empty($sideNavSkin) && isset($sideNavSkin)) ? $sideNavSkin : 'light';
        Yii::$app->session->set('sideNavSkin',$sideNavSkin);

        $sideNavSkin = (isset($sideNavSkin) && !empty($sideNavSkin) && $sideNavSkin == 'dark') ? ' dark-side-nav ' : '';
        $fontSize = (isset($fontSize) && !empty($fontSize)) ? $fontSize : 'normal';
        $language = (isset($language) && !empty($language)) ? $language : 'en';

        Yii::$app->session->set('showSideNav',$showSideNav);
        Yii::$app->session->set('fontSize',$fontSize);
        Yii::$app->session->set('language',$language);
        
        return compact('themeColor','darken','bodyClass','sideNavClass','sideNavSkin','fontSize','language');
    }
}
