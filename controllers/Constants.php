<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\controllers;

/**
 * This contains the constants in the application.
 *
 */
class Constants {
    const B4R_API_ERROR_MSG = 'Could not connect to API.';
    const APP_NAME = 'EBS-Core Breeding';
    const DOMAIN_NAME = 'Core Breeding';
    const JIRA_SERVICE_DESK_URL = 'https://ebsproject.atlassian.net/servicedesk/customer/';
    const EBS_ADMIN_ROLE = 'Admin';
    const CB_ADMIN_ROLE = 'CB Admin';
}