<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Api;

class StatusController extends Controller
{
    /**
     * Get Core Breeding's healthcheck status
     *
     * Sample Response:
     * {
     *  "success": true,
     *  "message": "Request has been successfully completed.",
     *  "data": [
     *    {
     *      "status": "OK",
     *      "responseTime": "0.004683s",
     *      "timestamp": "Thu Oct 12 2023 5:44:00 AM",
     *      "checks": {
     *          "database": "OK"
     *      }
     *    }
     *  ],
     *  "totalCount": null,
     *  "status": 200,
     *  "totalPages": 1
     * }
     */
    public function actionIndex ()
    {   
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        return json_encode((new Api)->getApiResults('GET', 'status'));
    }
}
