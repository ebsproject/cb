<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
namespace app\controllers;

use Yii;
use app\interfaces\controllers\IDashboard;
use app\models\UserDashboardConfig;
use app\models\User;

/**
 * Contains methods for system dashboard
 */

class Dashboard implements IDashboard
{

	/**
	 * Retrieves data filters saved in user dashboard config
	 *
	 * @param $attr text attribute that user wants to retrieve (i.e program_id, phase_id, etc.)
	 * @return $data mixed saved data filters 
	 */
	public function getFilters($attr=null){
		//get user id
		$userModel = new User();
		$userId = $userModel->getUserId();

		$userDashboardConfigModel = \Yii::$container->get('app\models\UserDashboardConfig');
		return $userDashboardConfigModel->getDashboardFilters($userId,$attr);
	}

	/**
	 * Get url with filter and access token parameters
	 */
	public function getFilterUrl(){
		$filters = Dashboard::getFilters();
		$filterStr = '';

		if(is_array($filters)){
			$filters = (object) $filters;
		}

		$program = Yii::$app->userprogram->get('abbrev'); //current program abbrev

		if(!isset($filters->program_id) || $filters->program_id == null || $filters->program_id == ''){
 			$filterStr = 'program_id=000&module='.$program;
		} else{
			$filterStr = 'program_id='.$filters->program_id.'&module='.$program;
		}
		$tempArr = [];
		foreach($filters as $key=>$value){
				if($value != null && $value != ''){
					if($value != null && $key != 'program_id'){
						$valStr = implode(',',$value);
						$tempArr[] = $key.'=='.$valStr;
					}
				}
		}
		$tempStr = 'none';
		if(!empty($tempArr)){
			$tempStr = implode('|', $tempArr);
		}
		$userModel = new User();
		$userId = $userModel->getUserId();

		//process access token
		$token = Yii::$app->session->get('accessToken')."|".microtime(true); //get access token and current time
		$key='b4rapp';
		$encrypted=openssl_encrypt($token,"AES-128-ECB",$key);
		//end process access token

		return $filterStr."&userId=".$userId.'&accessToken='.$encrypted."&defaultFilters=".$tempStr;


	}

	/**
	 * Populate recently used tools in user config
	 */
	public function populateRecentlyUsed(){
		//get user id
		$userModel = new User();
		$userId = $userModel->getUserId();
		$action = Yii::$app->controller->action->id;
		$module = Yii::$app->controller->module->id;
		$controller = Yii::$app->controller->id;
		$params = null;//$_GET;

		UserDashboardConfig::populateRecentlyUsed($userId,$module,$controller,$action,$params);
	}

	/**
	 * Get formatted time interval between two dates
	 * 
	 * @param $datetime1 timestamp first date
	 * @param $datetime2 timestamp second date
	 * @param @int text formatted date interval
	 */
	public function getFormattedTimeInterval($datetime1,$datetime2){
		$interval = $datetime1->diff($datetime2);
		//initialize time interval labels
		$minLabel = '';
		$hourLabel = '';
		$dayLabel = '';
		$monthLabel = '';

		$minLabel = ($interval->i > 1) ? ' '.\Yii::t('app','minutes') : ' '.\Yii::t('app','minute');
		$int = $interval->format('%i'.$minLabel);

		if($interval->h != 0 || $interval->h != '0'){ //if hour is set
			$hourLabel = ($interval->h > 1) ? ' '.\Yii::t('app','hours') : ' '.\Yii::t('app','hour');
			$int = $interval->format('%h'.$hourLabel.' %i'.$minLabel);
		}

		if($interval->d != 0 || $interval->d != '0'){//if day is set
			$dayLabel = ($interval->d > 1) ? ' '.\Yii::t('app','days') : ' '.\Yii::t('app','day');

			$h = ($interval->h == 0 || $interval->h == '0') ? '' : ' %h'; //if hour is not set
				
			$int = $interval->format('%d'.$dayLabel.$h.$hourLabel);
		}

		if($interval->m != 0 || $interval->m != '0'){ //if month is set
			$monthLabel = ($interval->m > 1) ? ' '.\Yii::t('app','months') : ' '.\Yii::t('app','month');

			$int = $interval->format('%m'.$monthLabel.' %d'.$dayLabel);
		}

		return $int;
	}


	/**
	 * Reset saved URL in session
	 * 
	 */
	public function resetSavedUrl($program){
		//reset current URL for EBS-Core Breeding
		$userModel = new User();
		$userId = $userModel->getUserId();

		Yii::$app->session['userId-'.$userId.'-program-'.$program] = null;
	}

	/**
	 * Return URL for Yii1
	 *  
	 */
	public function getCurrentUrl($origUrl, $actionLink, $refreshUrlSession = false){
		$userModel = new User();
		$userId = $userModel->getUserId();

		$program = Yii::$app->userprogram->get('abbrev'); //current program

		//checks if there's already saved URL in the session, if none default is the original action
		if(isset(Yii::$app->session['userId-'.$userId.'-program-'.$program][$actionLink.'?program='.$program]) && Yii::$app->session['userId-'.$userId.'-program-'.$program][$actionLink.'?program='.$program] != null && !$refreshUrlSession){
			$retUrl = Yii::$app->session['userId-'.$userId.'-program-'.$program][$actionLink.'?program='.$program];
		} else { 
			$filterStr = Dashboard::getFilterUrl(); //get the saved filters
			$initialParamStr = '?';

			// if there is specified initial url parameters
			if (strpos($origUrl, '?') !== false) {
			    $initialParamStr = '&';
			}

			$retUrl = $origUrl.$initialParamStr.$filterStr;
		}

		return $retUrl;
	}

	/**
	 * Checks if there is an existing saved filter values
	 * @param $filters currently applied data filters
	 * @return filter name if exists
	 */
	public function getSavedFilterByValues($filters){
		$userModel = new User();
		$userId = $userModel->getUserId();
		$result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
        
        if(isset($result[0]) && !empty($result[0])){ //with record
            $data = json_decode($result[0],true);
        }

        $currFltr = (array) $filters;
        if(isset($data['saved_dashboard_filters'])){ //if there are existing saved filters
	        foreach ($data['saved_dashboard_filters'] as $key => $value) {
	        	if($currFltr == $value){ //if there is an existing saved data filter
	        		$len = strlen($key);
	        		if($len > 14){
	        			return ['raw'=>$key,'name'=>substr($key, 0, 14) . '...'];
	        		}
	        		return ['raw'=>$key,'name'=>$key];
	        	}
			}
		}

		return null;
	}
}