<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\controllers;

use app\models\Config;
use app\interfaces\controllers\IDashboard;
use app\interfaces\models\IApi;
use app\interfaces\models\IPerson;
use app\interfaces\models\IUser;
use app\models\DataBrowserConfiguration;
use app\modules\occurrence\models\FileExport;

use app\widgets\GermplasmInfo;
use yii\web\Controller;
use Yii;

/**
 * Contains methods for data browsers
 */

class BrowserController extends Controller
{
    public function __construct ($id, $module,
        protected IApi $api,
        protected Config $configModel,
        protected IDashboard $dashboard,
        protected IPerson $person,
        protected IUser $user,
        $config = []
    )
    { parent::__construct($id, $module, $config); }

  /**
   * Process document filter
   *
   * @param $filter filter to be processed for document column
   * @return $processedFilter process filter for document column
   */
  public function processDocumentFilter($filter){
    // excluded characters
    $preg = '|[^$A-Za-z0-9 /*.@;-]|';
    $searchFilter = preg_replace($preg, ' ', trim($filter));

    // replace spaces to '&'' for tsvector query
    $processedFilter = preg_replace('/[[:blank:]]+/', ' ', trim($searchFilter));
    $processedFilter = str_replace(" "," & ",$processedFilter);

    return $processedFilter;
  }

  /**
   * Get category items by entity
   *
   * @return view category items
   */
  public function actionRenderCategories(){
    // model where to retrieve category items
    $modelClass = isset($_POST['modelClass']) ? $_POST['modelClass'] : '';
    // action in model to retrieve category items
    $modelClassAction = isset($_POST['modelClassAction']) ? $_POST['modelClassAction'] : '';
    // selected category
    $category = isset($_POST['category']) ? $_POST['category'] : '';
    // selected category ID
    $selectedCategoryId = isset($_POST['selectedCategoryId']) ? $_POST['selectedCategoryId'] : 0;

    $model = "\\app\\models\\".$modelClass;
    $data = $model::$modelClassAction();

    // records to be displayed in category panel
    $results = isset($data['data']) ? $data['data'] : [];
    // total count of category items
    $totalCount = isset($data['totalCount']) ? $data['totalCount'] : 0;
    // attribute name to be retrieved as id of the category item
    $id = isset($data['id']) ? $data['id'] : 'id';
    // attribute name to retrieved as text of the category item
    $text = isset($data['text']) ? $data['text'] : 'name';
    // attributes to be displayed in view more details about the category item
    $attributes = isset($data['attributes']) ? $data['attributes'] : [];

    return Yii::$app->controller->renderPartial('@app/widgets/views/browser/categories.php',[
      'data' => $results,
      'totalCount' => $totalCount,
      'id' => $id,
      'text' => $text,
      'attributes' => $attributes,
      'category' => $category,
      'selectedCategoryId' => $selectedCategoryId
    ]);
  }

  /**
   * View record information
   *
   * @return view record information
   */
  public function actionViewItem(){
    // model where to retrieve item info
    $modelClass = isset($_POST['modelClass']) ? $_POST['modelClass'] : '';
    // action in model class to retrieve info
    $modelClassAction = isset($_POST['modelClassAction']) ? $_POST['modelClassAction'] : '';
    // ID of the record to be retrieved
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    // butons to be rendered
    $buttons = isset($_POST['buttons']) ? $_POST['buttons'] : [];
    // main action butons to be rendered
    $mainButtons = isset($_POST['mainButtons']) ? $_POST['mainButtons'] : [];
    // view more info about the record url
    $viewUrl = isset($_POST['viewUrl']) ? $_POST['viewUrl'] : '';

    $modelInstance = \Yii::$container->get("\\app\\models\\".$modelClass);
    $data = $modelInstance->$modelClassAction($id);

    return Yii::$app->controller->renderAjax('@app/widgets/views/browser/view_entity.php',[
      'id' => $id,
      'data' => $data,
      'buttons' => $buttons,
      'mainButtons' => $mainButtons,
      'viewUrl' => $viewUrl
    ]);
  }

  /**
   * Pluralize a word if quantity is not one.
   *
   * @param $quantity int Number of items
   * @param $singular string  Singular form of word
   * @param $plural string Plural form of word; function will attempt to deduce plural form from singular if not provided
   * @return string Pluralized word if quantity is not one, otherwise singular
   */
  public static function pluralize($quantity, $singular, $plural=null) {
    if($quantity==1 || !strlen($singular)) return $singular;
    if($plural!==null) return $plural;

    $lastLetter = strtolower($singular[strlen($singular)-1]);
    switch($lastLetter) {
      case 'y':
        return substr($singular,0,-1).'ies';
      case 's':
        return $singular.'es';
      default:
        return $singular.'s';
    }
  }

  /**
   * Process detail view information
   * The format of attribbutes should be
   * [ [l$abel, $attribute, $title] ]
   * 
   * @param $data array record information to be processed
   * @param $attributes array attributes to be processed
   * @return $result array processed record information
   */
  public function processDetailViewInfo($data, $attributes){
    $result = [];

    if(isset($data) && !empty($data)){
      $title = $url = [];
      foreach ($attributes as $value) {
        $attribute = $value['attribute'];
        $label = isset($value['label']) ? $value['label'] : ucfirst($attribute);
        $val = isset($data[$attribute]) ? $data[$attribute] : null;
        $title = isset($value['title']) ? ['title' => $value['title']] : [];
        $url = isset($value['url']) ? ['url' => $value['url']] : [];

        // for timestamps
        if( (strpos($attribute, 'Timestamp') !== false || strpos($attribute, 'Date') !== false) && !empty($val)){
          if($attribute == 'modificationTimestamp'){
            // format modification timestamp
            $start = new \DateTime($val);
            $end = new \DateTime();

            $val = $this->dashboard->getFormattedTimeInterval($start, $end) . ' ago';
          }else{
            // format date
            $val = date("F j, Y", strtotime($val));
          }
        } else if( (strpos($attribute, 'Count') !== false ) ){
          $val = number_format($val);
        }

        $reqCols = [
          'attribute' => $attribute,
          'label' => $label,
          'value' => (string) $val
        ];

        $result[] = array_merge($reqCols, $title, $url);
          
      }
    }

    return $result;
  }

  /**
   * Process data values for detail view
   *
   * @param $data array list of variable and values to be processed
   * @return $attributes array list of processed variable and values
   */
  public static function processMetadataDetailViewInfo($data){
    $attributes = [];

    if(!empty($data)){
      foreach ($data as $value) {

        $label = isset($value['variableLabel']) ? $value['variableLabel'] : $value['label'];
        $value = $value['dataValue'];

        $attributes[] = [
          'label' => ucfirst(strtolower($label)),
          'value' => $value
        ];
      }
    }

    return $attributes;
  }

  /**
   * Get status category
   * NOTE: Refer to getStatusClassByCategory(string) for details on status classes
   *
   * @param string $status status value
   * 
   * @return string status category
   */
  public static function getStatusCategory ($status)
  {
    $status = strtolower($status);

    // Add more statuses here for future reference and validation
    $statusCategoryMap = [
      'cancellation in progress' => 'in-progress',
      'packed' => 'info',
      'packing cancelled' => 'invalid',
      'packing job failed' => 'invalid',
      'ready for packing' => 'done',
      'submission in progress' => 'in-progress',
      'updating entries in progress' => 'in-progress',
      'updating packages in progress' => 'in-progress',
    ];

    // This will return a 'grey' CSS status class
    if (!isset($statusCategoryMap[$status])) {
      return 'none';
    }

    return $statusCategoryMap[$status];
  }

  /**
   * Get status class by category
   *
   * @param $statusCategory status category
   * @return $class text class name
   */
  public static function getStatusClassByCategory($statusCategory){

    switch ($statusCategory) {
      case 'in-progress':
        $class = 'amber darken-3';
        break;
      case 'done':
        $class = 'green darken-3';
        break;
      case 'invalid':
        $class = 'red darken-1 white-text';
        break;
      case 'info':
        $class = 'blue darken-2 white-text';
        break;

      default:
        $class = 'grey';
        break;
    }

    return $class;
  }

  /**
   * Get class by Data collection QC code
   *
   * @param $code plot data QC code
   * @return $class, $description array class name and description of QC code
   */
  public static function getClassByQcCode($code){

    $code = strtoupper(trim($code));
    
    switch ($code) {
      case 'B': // data is erroneous or not accepted
        $class = 'red darken-1 white-text';
        $description = 'Data is erroneous or not accepted';
        break;
      case 'M': // data is not found
        $class = 'blue darken-2 white-text';
        $description = 'Data is missing';
        break;
      case 'S': // data is excluded from the analysis
        $class = 'grey white-text';
        $description = 'Data is suppressed';
        break;
      case 'G': // data is accepted and included in analysis
        $class = 'green darken-3 white-text';
        $description = 'Data is accepted and included in the analysis';
        break;

      default:
        $class = 'grey white-text';
        $description = '';
        break;
    }

    return [
      'class' => $class,
      'description' => $description
    ];
  }

  /**
   * Render generic web form
   */
  public function actionRenderGenericForm(){
    // record ID
    $id = isset($_POST['id']) ? $_POST['id'] : '';
    // model where to retrieve the field configuration
    $modelClass = isset($_POST['modelClass']) ? $_POST['modelClass'] : '';
    // model action to retrieve the field configuraton
    $modelClassAction = isset($_POST['modelClassAction']) ? $_POST['modelClassAction'] : '';
    // step to retrieve in the json configuration
    $step = isset($_POST['step']) ? $_POST['step'] : '';

    // get entity
    $entity = strtolower($modelClass);
    $pluralizedEntity = $this->pluralize(2, $entity);

    // retieve configuration
    $modelInstance = \Yii::$container->get("\\app\\models\\".$modelClass);
    $data = $modelInstance->$modelClassAction($id, $step);

    $widgetOptions = [
      'mainModel' => $modelClass,
      'config' => $data,
      'mainApiResourceMethod' => 'POST',
      'mainApiResourceFilter' => [$entity.'DbId' => "$id"],
      'mainApiResource' => $pluralizedEntity.'-search',
      'dataApiResource' => $entity.'-data-search',
      'noInstructions' => 'true'
    ];

    // below folder structure should be followed
    $module = strtolower($modelClass);
    $viewFile = '@app/modules/'.$module.'/views/manage/'.$step.'.php';
    
    $htmlData = $this->renderAjax($viewFile,[
      'widgetOptions' => $widgetOptions
    ]);

    return json_encode($htmlData);
  }

  /**
   * Save user's browser view mode whether 'grid' or 'detail'
   */
  public function actionSaveViewMode(){
    $dataBrowserId = $_POST['browserId'];
    $mode = $_POST['mode'];
    $name = $dataBrowserId . ' view mode';

    // get user ID
    $userModel = new User();
    $userId = $userModel->getUserId();

    $searchParams = [
      'userDbId' => (string)$userId,
      'dataBrowserDbId' => $dataBrowserId,
      'type' => 'view'
    ];
    
    // check if configuration already exists
    $config = DataBrowserConfiguration::searchAll($searchParams);

    $data['view'] = $mode;
    $requestData = [
      'name' => $name,
      'type' => 'view',
      'data' => $data,
      'dataBrowserDbId' => $dataBrowserId
    ];

    // if no config yet
    if(isset($config['totalCount']) && $config['totalCount'] == 0){
      $request = [
        'records'=>[
          $requestData
        ]
      ];

      DataBrowserConfiguration::create($request);

    }else{
      // if there is an existing config, update record

      if(isset($config['data'][0]['configDbId'])){
        $configDbId = $config['data'][0]['configDbId'];

        DataBrowserConfiguration::updateOne($configDbId, $requestData);
      }
    }
  }

    /**
     * Get columns given config abbrev
     *
     * @param string $program program identifier
     * @param string $abbrev abbreviation of a configuration
     * @param string $configValueKey key for an array of config value
     * @param array $excludedAttributes array of excluded variables from retrieval
     * @param string $mode determinant for packaging columns
     * @param string $headerMode determinant for using which type of header to use
     * 
     * @return mixed array of data browser columns
     */
    public function getColumns ($program, $abbrev, $configValueKey = '', $excludedAttributes = [], $mode = 'view', $headerMode = 'label')
    {
        $response = null;
        $configValue = [];
        $program = $program ?? $_SESSION['program'];
        $modifier = "_$program";
        $downloadedFileHeaders = [];
        $downloadedFileAttributes = [];
        [
            $role,
            $isAdmin,
        ] = $this->person->getPersonRoleAndType($program);

        if (strtoupper($role) == 'COLLABORATOR')
            $modifier = '_COLLABORATOR';


        $newAbbrev = "$abbrev$modifier";
        
        Yii::info('Get columns from configuration ' . json_encode(['program'=>$program,
            'configAbbrev'=>$newAbbrev]), __METHOD__);

        try {
            $method = 'GET';
            $path = "configurations?abbrev=$newAbbrev&limit=1";

            $response = $this->api->getApiResults(
                $method,
                $path,
                null,
                '',
                true
            );
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'][0])
        ) {
            $configValue = $response['data'][0]['configValue'][$configValueKey];
        } else if (empty($response['data'][0])) { // Get DEFAULT configuration
            $modifier = "_DEFAULT";
            $newAbbrev = "$abbrev$modifier";

            try {
                $method = 'GET';
                $path = "configurations?abbrev=$newAbbrev&limit=1";
    
                $response = $this->api->getApiResults(
                    $method,
                    $path,
                    null,
                    '',
                    true
                );
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $response = $e->getResponse();
            }

            if (empty($response['data']))
                return [
                    [],
                    [],
                ];

            $configValue = $response['data'][0]['configValue'][$configValueKey];
        }

        // Filter out excluded attributes from the config
        foreach ($excludedAttributes as $key => $value) {
            $configValue = array_filter($configValue, function ($e) use ($value) {
                return $e['attribute'] != $value;
            });
        }

        foreach ($configValue as $key => $value) {
            // If config value has no attribute or abbrev index keys, skip.
            if (!isset($value['attribute']) || !isset($value['abbrev'])) {
              continue;
            }
            if (strtolower($headerMode) == 'label') {
                $abbrev = $value['abbrev'];
                $label = 'MISSING_LABEL';

                // Retrieve label given abbrev
                $response = $this->api->getApiResults(
                    'POST',
                    "variables-search",
                    json_encode([
                        'fields' => '
                            variable.id AS variableDbId |
                            variable.abbrev AS abbrev |
                            variable.label AS label
                        ',
                        'abbrev' => $abbrev,
                    ]),
                    '&limit=1',
                    true
                );

                // Set label
                $label = (isset($response['data'][0]['label'])) ? $response['data'][0]['label'] : $label;
                $configValue[$key]['label'] = $label;
                unset($configValue[$key]['abbrev']);
            }

            // Check if column has View Germplasm Info settings
            if (isset($value['hasViewGermplasmInfo'])) {
                $id = $value['hasViewGermplasmInfo']['id'];
                $label = $value['hasViewGermplasmInfo']['label'];

                $configValue[$key] = array_merge(
                    $configValue[$key],
                    ['value' => function ($model) use ($id, $label) {
                        $linkAttrsTemp = [
                            'title' => \Yii::t('app', 'View Germplasm information'),
                            'data-label' => $model[$label],
                            'data-id' => $model[$id],
                            'data-target' => '#view-germplasm-widget-modal',
                            'data-toggle' => 'modal'
                        ];

                        $linkNameId = ['id' => 'view-germplasm-widget-' . $model[$id]];
                        $linkAttrsName = array_merge($linkNameId, $linkAttrsTemp);

                        return GermplasmInfo::widget([
                            'id' => $model[$id],
                            'linkAttrs' => $linkAttrsName,
                            'entityLabel' => $model[$label]
                        ]);
                    }]
                );

                unset($configValue[$key]['hasViewGermplasmInfo']);
            }

            if (strtolower($mode) == 'download') {
                array_push($downloadedFileAttributes, $configValue[$key]['attribute']);
                array_push($downloadedFileHeaders, strtoupper($configValue[$key][$headerMode]));
            }
        }

        if ($mode == 'view') return $configValue;
        else if ($mode == 'download')
            return [
                $downloadedFileAttributes,
                $downloadedFileHeaders,
            ];
    }

    /**
     * Get file name in download file by entity ID
     * 
     * @param Integer $id file name entity indentifier
     * @param Text $feature donwload feature identifier saved in config
     * @param Text $fileFormat file format identifier
     * @param Text $entity entity identifier (e.g. occcurrence, location, experiment)
     * 
     * @return Text $fileName processed file name from config
     * 
     */
    public function getFileNameFromConfigByEntityId($id, $feature, $fileFormat=FileExport::CSV, $entity='occurrence')
    {
        // get attribute and suffix to be used in file name from config
        [$fileNameAttribute, $fileNameSuffix] = Yii::$app->config->getFileNameAttributeAndSuffix($feature);

        if($entity == 'location'){
            // get location info for file name 
            $location = \Yii::$container->get('app\models\Location');
            $response = $location->getLocationForFileNameById($id);
        } else {
            // get occurrence info for file name
            $occurrence = \Yii::$container->get('app\models\OccurrenceSearch');
            $response = $occurrence->getOccurrenceForFileNameById($id);
        }

        // redirect to previous page if data retrieval fails
        if(empty($response['success'])){
            $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl)->send();
            exit;
        }

        // get file name by file format, timestamp, file name attribute and suffix
        $fileName = isset($response['data'][0][$fileNameAttribute]) ? $response['data'][0][$fileNameAttribute] : $fileNameAttribute;
        $toExport = new FileExport(fileFormat: $fileFormat, time: time(), fileName: $fileName, suffix: $fileNameSuffix);

        // if attribute has experiment, use occurrence number in file name
        if(strpos($fileNameAttribute,'experiment') !== false){
            $occurrenceNumber = isset($response['data'][0]['occurrenceNumber']) ? $response['data'][0]['occurrenceNumber'] : 0 ;
            $fileName = $toExport->getFileNames(
                $fileName,
                $toExport->formatSerialCodes([ $occurrenceNumber ])
            )[0];
        } else{
            $fileName = $toExport->getFileName();
        }

        return $fileName;
    }
}