<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register model interfaces of the Harvest module here
 */

// register IApiHelper interface
// When a class depends on the IApiHelper,
// ApiHelper will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IApiHelper', 'app\modules\harvestManager\models\ApiHelper');

// register ICreationModel interface
// When a class depends on the ICreationModel,
// CreationModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\ICreationModel', 'app\modules\harvestManager\models\CreationModel');

// register ICrossBrowserModel interface
// When a class depends on the ICrossBrowserModel,
// CrossBrowserModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\ICrossBrowserModel', 'app\modules\harvestManager\models\CrossBrowserModel');

// register IHarvestManagerModel interface
// When a class depends on the IHarvestManagerModel,
// HarvestManagerModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IHarvestManagerModel', 'app\modules\harvestManager\models\HarvestManagerModel');

// register IOccurrenceDetailsModel interface
// When a class depends on the IOccurrenceDetailsModel,
// OccurrenceDetailsModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel', 'app\modules\harvestManager\models\OccurrenceDetailsModel');

// register IPlotBrowserModel interface
// When a class depends on the IPlotBrowserModel,
// PlotBrowserModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IPlotBrowserModel', 'app\modules\harvestManager\models\PlotBrowserModel');

?>