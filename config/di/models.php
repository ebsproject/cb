<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register general model interfaces here
 */

// register IApi interface
// When a class depends on the IApi,
// Api will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IApi', 'app\models\Api');

// register IApplication interface
// When a class depends on the IApplication,
// Application will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IApplication', 'app\models\Application');

// register IBackgroundJob interface
// When a class depends on the IBackgroundJob,
// BackgroundJob will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IBackgroundJob', 'app\models\BackgroundJob');

// register ICross interface
// When a class depends on the ICross,
// Cross will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ICross', 'app\models\Cross');

// register IItemModule interface
// When a class depends on the IItemModule,
// ItemModule will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IItemModule', 'app\models\ItemModule');

// register IOccurrence interface
// When a class depends on the IOccurrence,
// Occurrence will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\IOccurrence',
    'app\models\Occurrence'
);

// register IExperiment interface
// When a class depends on the IExperiment,
// Experiment will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\IExperiment',
    'app\models\Experiment'
);

// register ILocation interface
// When a class depends on the ILocation,
// Location will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\ILocation',
    'app\models\Location'
);

// register IPlatformModule interface
// When a class depends on the IPlatformModule,
// PlatformModule will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IPlatformModule', 'app\models\PlatformModule');

// register IPlot interface
// When a class depends on the IPlot,
// Plot will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IPlot', 'app\models\Plot');

// register IUser interface
// When a class depends on the IUser,
// User will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IUser', 'app\models\User');

// register IWorker interface
// When a class depends on the IWorker,
// Worker will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IWorker', 'app\models\Worker');

// register IConfig interface
// When a class depends on the IConfig,
// Config will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IConfig', 'app\models\Config');

// register IProgram interface
// When a class depends on the IProgram,
// Program will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IProgram', 'app\models\Program');

// register ITerminalTransaction interface
// When a class depends on the ITerminalTransaction,
// TerminalTransaction will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITerminalTransaction', 'app\models\TerminalTransaction');

// register ITransactionDataset interface
// When a class depends on the ITransactionDataset,
// TransactionDataset will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITransactionDataset', 'app\models\TransactionDataset');

// register ITransaction interface
// When a class depends on the ITransaction,
// Transaction will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\dataCollection\models\ITransaction', 'app\modules\dataCollection\models\Transaction');

// register IVariable interface
// When a class depends on the IVariable,
// Variable will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IVariable', 'app\models\Variable');

// register alias 'user' to class User
Yii::$container->set(
    'user',
    'app\models\User'
);

//register ILists interface
Yii::$container->set('app\interfaces\models\ILists', 'app\models\Lists');

//register IListMember interface
Yii::$container->set('app\interfaces\models\IListMember', 'app\models\ListMember');

//register IPlatformListMember interface
Yii::$container->set('app\interfaces\models\IPlatformListMember', 'app\models\PlatformListMember');

//register ILocationOccurrenceGroup interface
Yii::$container->set('app\interfaces\models\ILocationOccurrenceGroup', 'app\models\LocationOccurrenceGroup');

// register IPlantingJob interface
// When a class depends on the IPlantingJob,
// PlantingJob will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IPlantingJob', 'app\models\PlantingJob');


// register IBrowser interface
// When a class depends on the IBrowser,
// Browser will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IBrowser', 'app\models\IBrowser');

// register IPerson interface
// When a class depends on the IPerson,
// Person will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IPerson', 'app\models\Person');

// register ITenantProgram interface
// When a class depends on the ITenantProgram,
// TenantProgram will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITenantProgram', 'app\models\TenantProgram');

// register ITenantTeam interface
// When a class depends on the ITenantTeam,
// TenantTeam will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITenantTeam', 'app\models\TenantTeam');

// register IGermplasm interface
// When a class depends on the IGermplasm,
// Germplasm will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IGermplasm', 'app\models\Germplasm');

// register IGermplasmSearch interface
// When a class depends on the IGermplasmSearch,
// GermplasmSearch will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IGermplasmSearch', 'app\models\GermplasmSearch');

// register IGermplasmFileUpload interface
// When a class depends on the IGermplasmFileUpload,
// GermplasmFileUpload will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IGermplasmFileUpload', 'app\models\GermplasmFileUpload');

// register ITraits interface
// When a class depends on the ITraits,
// Traits will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITraits', 'app\models\Traits');

// register ICropProgram interface
// When a class depends on the ICropProgram,
// Traits will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ICropProgram', 'app\models\CropProgram');

// register IUserDashboardConfig interface
// When a class depends on the IUserDashboardConfig,
// UserDashboardConfig will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IUserDashboardConfig', 'app\models\UserDashboardConfig');

// register IndicesSearch interface
// When a class depends on the IndicesSearch,
// IndicesSearch will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IIndicesSearch', 'app\models\IndicesSearch');

?>