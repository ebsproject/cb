<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register model interfaces of the Working List module here
 */

// register IWorkingListModel interface
// When a class depends on the IWorkingListModel,
// WorkingListModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\workingList\models\IWorkingListModel', 'app\modules\workingList\models\WorkingListModel');

// register IWorkingListModel interface
// When a class depends on the IListManagemenModel,
// ListManagemenModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\workingList\models\IListManagemenModel', 'app\modules\workingList\models\ListManagemenModel');
?>