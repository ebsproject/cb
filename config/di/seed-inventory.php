<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register model interfaces of the Seed Inventory module here
 */

// register IHarvestManagerApiService interface
// When a class depends on the IHarvestManagerApiService,
// HarvestManagerApiService will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IHarvestManagerApiService', 'app\modules\seedInventory\models\HarvestManagerApiService');

// register IHarvestManagerUtil interface
// When a class depends on the IHarvestManagerUtil,
// HarvestManagerUtil will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IHarvestManagerUtil', 'app\modules\seedInventory\models\HarvestManagerUtil');

// register IVariableFilterComponent interface
// When a class depends on the IVariableFilterComponent,
// VariableFilterComponent will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IVariableFilterComponent', 'app\modules\seedInventory\models\VariableFilterComponent');

// register IQueryParameterModel interface
// When a class depends on the IQueryParameterModel,
// QueryParameterModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IQueryParameterModel', 'app\modules\seedInventory\models\QueryParameterModel');

// register IFindSeedDataBrowserModel interface
// When a class depends on the IFindSeedDataBrowserModel,
// QueryParameterModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IFindSeedDataBrowserModel', 'app\modules\seedInventory\models\FindSeedDataBrowserModel');

// register IFindSeedListModel interface
// When a class depends on the IFindSeedListModel,
// QueryParameterModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IFindSeedListModel', 'app\modules\seedInventory\models\FindSeedListModel');

// register IWorkingList interface
// When a class depends on the IWorkingList,
// QueryParameterModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IWorkingList', 'app\modules\seedInventory\models\WorkingList');
?>