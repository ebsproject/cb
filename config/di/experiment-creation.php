<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register model interfaces of the Experiment Creation module here
 */

// register IProtocolModel interface
// When a class depends on the IProtocolModel,
// ProtocolModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\experimentCreation\models\IProtocolModel', 'app\modules\experimentCreation\models\ProtocolModel');


// register IBrowserModel interface
// When a class depends on the IBrowserModel,
// BrowserModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\experimentCreation\models\IBrowserModel', 'app\modules\experimentCreation\models\BrowserModel');

// register IEntryOrderModel interface
// When a class depends on the IEntryOrderModel,
// EntryOrderModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\experimentCreation\models\IEntryOrderModel', 'app\modules\experimentCreation\models\EntryOrderModel');

?>