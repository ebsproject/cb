<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register model interfaces of the Germplasm module here
 */

// register ICreateModel interface
// When a class depends on the ICreateModel,
// CreateModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\ICreateModel', 'app\modules\germplasm\models\CreateModel');

// register IDeleteGermplasmModel interface
// When a class depends on the IDeleteGermplasmModel,
// IDeleteGermplasmModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\IDeleteGermplasmModel', 'app\modules\germplasm\models\DeleteGermplasmModel');

// register IMergeModel interface
// When a class depends on the IMergeModel,
// MergeModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\IMergeModel', 'app\modules\germplasm\models\MergeModel');

// register IUpdateModel interface
// When a class depends on the IUpdateModel,
// UpdateModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\IUpdateModel', 'app\modules\germplasm\models\UpdateModel');

// register IConflictReportModel interface
// When a class depends on the IConflictReportModel,
// MergeModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\IConflictReportModel', 'app\modules\germplasm\models\ConflictReportModel');

// register ITransactionTemplateModel interface
// When a class depends on the ITransactionTemplateModel,
// TransactionTemplateModel will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\germplasmModule\models\ITransactionTemplateModel', 'app\modules\germplasm\models\TransactionTemplateModel');

?>