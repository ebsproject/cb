<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Cross Manager module here
 */

// register IEntryList interface
// When a class depends on the IEntryList,
// EntryList will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IEntryList', 'app\models\EntryList');

// register ICrop interface
// When a class depends on the ICrop,
// Crop will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ICrop', 'app\models\Crop');

// register IScaleValue interface
// When a class depends on the IScaleValue,
// ScaleValue will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IScaleValue', 'app\models\ScaleValue');

// register IOccurrence interface
// When a class depends on the IOccurrence,
// Occurrence will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IOccurrence', 'app\models\Occurrence');

?>