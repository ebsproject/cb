<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

$params = require(__DIR__ . '/params.php');

$redisHost = getenv('CB_REDIS_HOST');
$redisPort = getenv('CB_REDIS_PORT');

$logFormat = function ($message) {
    list($text, $level, $category, $timestamp) = $message;
    $level = strtoupper(yii\log\Logger::getLevelName($level));

    // https://www.yiiframework.com/doc/api/2.0/yii-log-target#getMessagePrefix()-detail
    $request = Yii::$app->getRequest();
    $ip = $request instanceof yii\web\Request ? $request->getUserIP() : '-';

    $user = \Yii::$container->get('app\models\User');
    $userID = $user ? $user->getUserId() : '-';

    /* @var $session \yii\web\Session */
    $session = Yii::$app->has('session', true) ? Yii::$app->get('session') : null;
    $sessionID = $session && $session->getIsActive() ? $session->getId() : '-';

    return "$level [CB-UI][$ip][$userID][$sessionID]";
};

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'gearman' => [
            'class' => 'micmorozov\yii2\gearman\GearmanController',
            'gearmanComponent' => 'gearman',
        ],
    ],
    'components' => [
        'gearman' => [
            'class' => 'micmorozov\yii2\gearman\GearmanComponent',
            'servers' => [
                ['host' => getenv('CB_GEARMAN_HOST'), 'port' =>getenv('CB_GEARMAN_PORT')],
            ],
            'user' => 'www-data',
        ],
        'redis' => [
            'class'=>'yii\redis\Connection',
            'hostname' =>  $redisHost, // the swarm service name
            'port' => $redisPort,
            'database' => 0,
        ],
        'session'       => [
            'class' => 'yii\redis\Session',
        ],
        'cache'         => [
            'class' => 'yii\redis\Cache',
        ],

        'request' => [
            'cookieValidationKey' => '4fsB=>g_aLx>UHnCOdi1Nj*1Q|5j^S',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/fault',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'flushInterval' => 1,
            // If YII_DEBUG is on, each log message will be appended with at most 3 levels of the call stack at which
            // the log message is recorded; and if YII_DEBUG is off, no call stack information will be included.
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                // https://github.com/codemix/yii2-streamlog
                [
                    'class' => 'codemix\streamlog\Target',
                    // 'url' => 'php://stdout',
                    'url' => 'php://stderr',  // https://github.com/docker-library/php/issues/811#issuecomment-481029692
                    'logVars' => [],
                    'levels' => YII_DEBUG ? ['info', 'trace'] : ['info'],
                    'except' => ['yii\*'],
                    'exportInterval' => 1,
                    // 'prefixString' => '[yii] ',
                    'prefix' => $logFormat,
                ],
                [
                    'class' => 'codemix\streamlog\Target',
                    'url' => 'php://stderr',
                    'logVars' => [],
                    'levels' => ['error', 'warning'],
                    'except' => [],
                    'exportInterval' => 1,
                    // 'prefixString' => '[yii] ',
                    'prefix' => $logFormat,
                ],
            ],
        ],
        // 'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
        'enablePrettyUrl' => true,
        // 'showScriptName' => false,
        'enableStrictParsing' => false,
        'rules' => [
            'gii'=>'gii',
            'gii/<controller:\w+>'=>'gii/<controller>',
            'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
            '<module>/<controller>/<id:\d+>' => '<module>/<controller>/view',
            
        ],
        ],
        'userprogram' => [
            'class' => 'app\components\UserProgram',
        ],
        'api' => [
            'class' => 'app\components\Api',
        ],
        'config' => [
            'class' => 'app\components\Config',
        ],
        'access' => [
            'class' => 'app\components\Access',
        ],
        'cbSession' => [
            'class' => 'app\components\CBSession',
        ],
    ],
    'params' => $params,
    'modules' =>  [
        'download' => [
            'class' => 'app\modules\download\DownloadModule',
        ],
        'package' => [
            'class' => 'app\modules\package\PackageModule',
        ],
        'printouts' => [
            'class' => 'app\modules\printouts\PrintoutsModule',
        ],
        'traits' => [
            'class' => 'app\modules\traits\TraitsModule',
        ],
        'user' => [
            'class' => 'app\modules\user\UserModule',
        ],
        'userFilter' => [
            'class' => '\app\modules\userFilter\UserFilterModule'
        ],
        'userSort' => [
            'class' => '\app\modules\userSort\UserSortModule'
        ],
        'season' => [
            'class' => '\app\modules\season\SeasonModule'
        ],
        'dataCollection' => [
            'class' => '\app\modules\dataCollection\DataCollectionModule'
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
            'minPageSize' => 1,
            'maxPageSize' => getenv('CB_DATA_BROWSER_MAX_PAGE_SIZE'),
            'defaultPageSize' => getenv('CB_DATA_BROWSER_DEFAULT_PAGE_SIZE'),
            'dynaGridOptions' => [
                'iconPersonalize' => '<i class="material-icons personalize-grid-icon">settings</i>',
                'deleteButtonOptions' => [
                    'label' => 'Remove'
                ],
                'submitButtonOptions' => [
                    'label' => 'Apply'
                ]
            ]
        ],
        'advancedDataBrowser' => [
            'class' => 'app\modules\advancedDataBrowser\AdvancedDataBrowserModule',
        ],
        'search' => [
            'class' => 'app\modules\search\SearchModule',
        ],
        'account' => [
            'class' => 'app\modules\account\AccountModule',
        ],
        'dashboard' => [
            'class' => 'app\modules\dashboard\DashboardModule',
        ],
        'auth' => [
            'class' => 'app\modules\auth\AuthModule',
        ],
        'experimentCreation' => [
            'class' => 'app\modules\experimentCreation\ExperimentCreationModule',
        ],
        'seedInventory' => [
            'class' => 'app\modules\seedInventory\SeedInventoryModule',
        ],
        'variable' => [
            'class' => 'app\modules\variable\VariableModule'
        ],
        'germplasm' => [
            'class' => 'app\modules\germplasm\GermplasmModule',
        ],
        'query' => [
            'class' => 'app\modules\query\QueryModule',
        ],
        'occurrence' => [
            'class' => 'app\modules\occurrence\OccurrenceModule',
        ],
        'location' => [
            'class' => 'app\modules\location\LocationModule',
        ],
        'plantingInstruction' => [
            'class' => 'app\modules\plantingInstruction\PlantingInstructionModule',
        ],
        'harvestManager' => [
            'class' => 'app\modules\harvestManager\HarvestManagerModule',
        ],
        'crossManager' => [
            'class' => 'app\modules\crossManager\CrossManager',
        ],
        'inventoryManager' => [
            'class' => 'app\modules\inventoryManager\InventoryManagerModule',
        ],
        'fileUpload' => [
            'class' => 'app\modules\fileUpload\FileUploadModule',
        ],
        'workingList' => [
            'class' => 'app\modules\workingList\WorkingListModule',
        ],
        'breedingProgramManager' => [
            'class' => 'app\modules\breedingProgramManager\breedingProgramManagerModule',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
    //    'allowedIPs' => YII_DEBUG ? ['127.0.0.1', '::1','*'] : [],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [ // setting materializecss templates
                    'materializecss' => '@vendor/macgyer/yii2-materializecss/src/gii-templates/generators/crud/materializecss',
                ]
            ]
        ],
    ];
}

return $config;
