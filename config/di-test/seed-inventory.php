<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Seed Inventory module here
 */

// register alias 'plotBrowserModel' to class PlotBrowserModel
Yii::$container->set('plotBrowserModelSI', 'app\modules\seedInventory\models\PlotBrowserModel');

// register alias 'harvestManagerModel' to class HarvestManagerModel
Yii::$container->set('harvestManagerModelSI', 'app\modules\seedInventory\models\HarvestManagerModel');

// register alias 'harvestManagerUtil' to class HarvestManagerUtil
Yii::$container->set('harvestManagerUtil', 'app\modules\seedInventory\models\HarvestManagerUtil');

// register alias 'seedlotManagementBrowserModel' to class SeedlotManagementBrowserModel
Yii::$container->set('seedlotManagementBrowserModel', 'app\modules\seedInventory\models\SeedlotManagementBrowserModel');

// register IHarvestManagerApiService interface
// When a class depends on the IHarvestManagerApiService,
// HarvestManagerApiServiceMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IHarvestManagerApiService', 'app\tests\mocks\modules\seedInventory\models\HarvestManagerApiServiceMock');

// register IHarvestManagerUtil interface
// When a class depends on the IHarvestManagerUtil,
// HarvestManagerUtilMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IHarvestManagerUtil', 'app\tests\mocks\modules\seedInventory\models\HarvestManagerUtilMock');

// register IVariableFilterComponent interface
// When a class depends on the IVariableFilterComponent,
// VariableFilterComponent will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\seedInventory\models\IVariableFilterComponent', 'app\modules\seedInventory\models\VariableFilterComponent');

// register IQueryParameterModel interface
// When a class depends on the IQueryParameterModel,
// QueryParameterModel will be instantiated as the dependency
Yii::$container->set('queryParameterModel', 'app\modules\seedInventory\models\QueryParameterModel');
Yii::$container->set('app\interfaces\modules\seedInventory\models\IQueryParameterModel', 'app\tests\mocks\modules\seedInventory\models\QueryParameterModelMock');

// register alias 'findSeedDataBrowserModel' to class FindSeedDataBrowserModel
Yii::$container->set('findSeedDataBrowserModel', 'app\modules\seedInventory\models\FindSeedDataBrowserModel');

// register alias 'variableFilterComponent' to class VariableFilterComponent
Yii::$container->set('variableFilterComponent', 'app\modules\seedInventory\models\VariableFilterComponent');
Yii::$container->set('app\interfaces\modules\seedInventory\models\IVariableFilterComponent', 'app\tests\mocks\modules\seedInventory\models\VariableFilterComponentMock');

// register alias 'seedSearchListModel' to class FindSeedListModel
Yii::$container->set('seedSearchListModel', 'app\modules\seedInventory\models\FindSeedListModel');

// register alias 'workingListModel' to class WorkingList
Yii::$container->set('workingListModel', 'app\modules\seedInventory\models\WorkingList');
?>