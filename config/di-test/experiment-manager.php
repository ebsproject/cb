<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Occurrence module (Experiment Manager) here
 */

// Register alias "fieldLayout" to class "app\modules\location\models\FieldLayout"
Yii::$container->set('fieldLayout','app\modules\location\models\FieldLayout');

// register alias 'locationPlot' to class LocationPlot
Yii::$container->set('locationPlot', 'app\modules\occurrence\models\LocationPlot');

// register alias 'manageModel' to class Manage
Yii::$container->set('manageModel', 'app\modules\occurrence\models\Manage');

// register alias 'locationPlotModel' to class LocationPlot
Yii::$container->set('locationPlotModel', 'app\modules\occurrence\models\LocationPlot');
?>