<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Cross Manager module here
 */

// register alias 'crossListModel' to class CrossListModel
Yii::$container->set('crossListModel', 'app\modules\crossManager\models\CrossListModel');

// register alias 'crossManagerParentModel' to class ParentModel
Yii::$container->set('crossManagerParentModel', 'app\modules\crossManager\models\ParentModel');

// register alias 'crossManagerCrossModel' to class CrossModel
Yii::$container->set('crossManagerCrossModel', 'app\modules\crossManager\models\CrossModel');

// register alias 'crossManagerCrossParentModel' to class CrossParentModel
Yii::$container->set('crossManagerCrossParentModel', 'app\modules\crossManager\models\CrossParentModel');

// register alias 'basicInfoModel' to class BasicInfoModel
Yii::$container->set('basicInfoModel', 'app\modules\crossManager\models\BasicInfoModel');

// register alias 'entryListMock' to class EntryList
Yii::$container->set('entryListMock','app\tests\mocks\models\EntryListMock');

// register IEntryList interface
// When a class depends on the IEntryList,
// EntryList will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IEntryList', 'app\tests\mocks\models\EntryListMock');

// register ICrop interface
// When a class depends on the ICrop,
// Crop will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ICrop', 'app\tests\mocks\models\CropMock');

// register IScaleValue interface
// When a class depends on the IScaleValue,
// ScaleValue will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IScaleValue', 'app\tests\mocks\models\ScaleValueMock');

// register IOccurrence interface
// When a class depends on the IOccurrence,
// Occurrence will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IOccurrence', 'app\tests\mocks\models\OccurrenceMock');

?>