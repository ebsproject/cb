<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Experiment Creation module here
 */

// Register alias "formModel" to class "app\modules\experimentCreation\models\FormModel"
Yii::$container->set('formModel','app\modules\experimentCreation\models\FormModel');

// Register alias "browserModel" to class "app\modules\experimentCreation\models\BrowserModel"
Yii::$container->set('browserModel','app\modules\experimentCreation\models\BrowserModel');

// Register alias "entryOrderModel" to class "app\modules\experimentCreation\models\EntryOrderModel"
Yii::$container->set('entryOrderModel','app\modules\experimentCreation\models\EntryOrderModel');

// register alias 'packageModel' to class 'app\modules\experimentCreation\models\PackageModel'
Yii::$container->set('packageModel', 'app\modules\experimentCreation\models\PackageModel');

// register alias 'paEntriesModel' to class 'app\modules\experimentCreation\models\PAEntriesMoodel'
Yii::$container->set('paEntriesModel', 'app\modules\experimentCreation\models\PAEntriesModel');
?>