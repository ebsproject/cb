<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Germplasm module here
 */

// Register alias "germplasmCreateModel" to class "app\modules\germplasm\models\CreateModel"
Yii::$container->set('germplasmCreateModel','app\modules\germplasm\models\CreateModel');

Yii::$container->set('app\interfaces\modules\germplasmModule\models\ICreateModel','app\tests\mocks\modules\germplasmModule\models\CreateModelMock');

// Register alias "viewBrowserModel" to class "app\modules\germplasm\models\ViewBrowserModel"
Yii::$container->set('viewBrowserModel','app\modules\germplasm\models\ViewBrowserModel');

// Register alias "errorLogModel" to class "app\modules\germplasm\models\ErrorLogModel"
Yii::$container->set('errorLogModel','app\modules\germplasm\models\ErrorLogModel');

// Register alias "deleteGermplasmModel" to class "app\modules\germplasm\models\DeleteGermplasmModel"
Yii::$container->set('deleteGermplasmModel','app\modules\germplasm\models\DeleteGermplasmModel');

// Register alias "viewFileUploadModel" to class "app\modules\germplasm\models\ViewFileUploadModel"
Yii::$container->set('viewFileUploadModel','app\modules\germplasm\models\ViewFileUploadModel');

// Register alias "germplasmMergeModel" to class "app\modules\germplasm\models\MergeModel"
Yii::$container->set('germplasmMergeModel','app\modules\germplasm\models\MergeModel');

// Register alias "germplasmUpdateModel" to class "app\modules\germplasm\models\UpdateModel"
Yii::$container->set('germplasmUpdateModel','app\modules\germplasm\models\UpdateModel');

// Register alias "conflictReportModel" to class "app\modules\germplasm\models\ConflictReportModel"
Yii::$container->set('conflictReportModel','app\modules\germplasm\models\ConflictReportModel');

// Register alias "transactionTemplateModel" to class "app\modules\germplasm\models\TransactionTemplateModel"
Yii::$container->set('transactionTemplateModel','app\modules\germplasm\models\TransactionTemplateModel');