<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Harvest Manager module here
 */

// register alias 'apiHelper' to class ApiHelper
Yii::$container->set('apiHelper', 'app\modules\harvestManager\models\ApiHelper');
// register alias 'apiHelperMock' to class ApiHelperMock
Yii::$container->set('apiHelperMock', 'app\tests\mocks\modules\harvestManager\models\ApiHelperMock');

// register alias 'creationCrossBrowserModel' to class CreationCrossBrowserModel
Yii::$container->set('creationCrossBrowserModel', 'app\modules\harvestManager\models\CreationCrossBrowserModel');

// register alias 'creationModel' to class CreationModel
Yii::$container->set('creationModel', 'app\modules\harvestManager\models\CreationModel');
// register alias 'creationModelMock' to class CreationModelMock
Yii::$container->set('creationModelMock', 'app\tests\mocks\modules\harvestManager\models\CreationModelMock');

// register alias 'creationPlotBrowserModel' to class CreationPlotBrowserModel
Yii::$container->set('creationPlotBrowserModel', 'app\modules\harvestManager\models\CreationPlotBrowserModel');

// register alias 'crossBrowserModel' to class CrossBrowserModel
Yii::$container->set('crossBrowserModel', 'app\modules\harvestManager\models\CrossBrowserModel');
// register alias 'crossBrowserModelMock' to class CrossBrowserModelMock
Yii::$container->set('crossBrowserModelMock', 'app\tests\mocks\modules\harvestManager\models\CrossBrowserModelMock');

// register alias 'harvestDataModel' to class HarvestDataModel
Yii::$container->set('harvestDataModel', 'app\modules\harvestManager\models\HarvestDataModel');
// register alias 'harvestDataModelMock' to class HarvestDataModelMock
Yii::$container->set('harvestDataModelMock', 'app\tests\mocks\modules\harvestManager\models\HarvestDataModelMock');

// register alias 'harvestManagerModel' to class HarvestManagerModel
Yii::$container->set('harvestManagerModel', 'app\modules\harvestManager\models\HarvestManagerModel');
// register alias 'harvestManagerModelMock' to class HarvestManagerModelMock
Yii::$container->set('harvestManagerModelMock', 'app\tests\mocks\modules\harvestManager\models\HarvestManagerModelMock');

// register alias 'managementBrowserModel' to class ManagementBrowserModel
Yii::$container->set('managementBrowserModel', 'app\modules\harvestManager\models\ManagementBrowserModel');

// register alias 'occurrenceDetailsModel' to class OccurrenceDetailsModel
Yii::$container->set('occurrenceDetailsModel', 'app\modules\harvestManager\models\OccurrenceDetailsModel');
// register alias 'occurrenceDetailsModelMock' to class OccurrenceDetailsModelMock
Yii::$container->set('occurrenceDetailsModelMock', 'app\tests\mocks\modules\harvestManager\models\OccurrenceDetailsModelMock');

// register alias 'plotBrowserModel' to class PlotBrowserModel
Yii::$container->set('plotBrowserModel', 'app\modules\harvestManager\models\PlotBrowserModel');
// register alias 'plotBrowserModelMock' to class PlotBrowserModelMock
Yii::$container->set('plotBrowserModelMock', 'app\tests\mocks\modules\harvestManager\models\PlotBrowserModelMock');

// register alias 'searchParametersModel' to class OccurrenceDetailsModel
Yii::$container->set('searchParametersModel', 'app\modules\harvestManager\models\SearchParametersModel');
// register alias 'searchParametersModelMock' to class OccurrenceDetailsModelMock
Yii::$container->set('searchParametersModelMock', 'app\tests\mocks\modules\harvestManager\models\SearchParametersModelMock');

// register IApiHelper interface
// When a class depends on the IApiHelper,
// ApiHelper will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IApiHelper', 'app\tests\mocks\modules\harvestManager\models\ApiHelperMock');

// register ICreationModel interface
// When a class depends on the ICreationModel,
// CreationModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\ICreationModel', 'app\tests\mocks\modules\harvestManager\models\CreationModelMock');

// register ICrossBrowserModel interface
// When a class depends on the ICrossBrowserModel,
// CrossBrowserModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\ICrossBrowserModel', 'app\tests\mocks\modules\harvestManager\models\CrossBrowserModelMock');

// register IHarvestManagerModel interface
// When a class depends on the IHarvestManagerModel,
// HarvestManagerModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IHarvestManagerModel', 'app\tests\mocks\modules\harvestManager\models\HarvestManagerModelMock');

// register IOccurrenceDetailsModel interface
// When a class depends on the IOccurrenceDetailsModel,
// OccurrenceDetailsModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IOccurrenceDetailsModel', 'app\tests\mocks\modules\harvestManager\models\OccurrenceDetailsModelMock');

// register IPlotBrowserModel interface
// When a class depends on the IPlotBrowserModel,
// PlotBrowserModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\harvestManager\models\IPlotBrowserModel', 'app\tests\mocks\modules\harvestManager\models\PlotBrowserModelMock');

?>