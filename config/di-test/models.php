<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register general mock model interfaces here
 */

// register alias 'backgroundJob' to class BackgroundJob
Yii::$container->set('backgroundJob', 'app\models\BackgroundJob');

// register alias 'config' to class Config
Yii::$container->set('config', 'app\models\Config');
// register alias 'configMock' to class configMock
Yii::$container->set('configMock', 'app\tests\mocks\models\ConfigMock');

// register alias 'crop' to class Crop
Yii::$container->set('crop', 'app\models\Crop');
// register alias 'cropMock' to class CropMock
Yii::$container->set('cropMock', 'app\tests\mocks\models\CropMock');

// register alias 'cropProgramModel' to class CropProgram
Yii::$container->set('cropProgramModel', 'app\models\CropProgram');
Yii::$container->set('app\interfaces\models\ICropProgram', 'app\tests\mocks\models\CropProgramMock');

// register ICross interface
// When a class depends on the ICross,
// CrossMock will be instantiated as the dependency
Yii::$container->set('cross', 'app\models\cross');
Yii::$container->set('crossMock', 'app\tests\mocks\models\CrossMock');
Yii::$container->set('app\interfaces\models\ICross', 'app\tests\mocks\models\CrossMock');

// register alias 'experiment' to class Experiment
Yii::$container->set('experiment', 'app\models\Experiment');
// register alias 'experiment' to class ExperimentMock
Yii::$container->set('experimentMock', 'app\tests\mocks\models\ExperimentMock');

// register alias 'experimentProtocol' to class ExperimentProtocol
Yii::$container->set('experimentProtocol', 'app\models\ExperimentProtocol');

// register alias 'occurrence' to class occurrence
Yii::$container->set('occurrence', 'app\models\occurrence');
// register alias 'occurrence' to class occurrenceMock
Yii::$container->set('occurrenceMock', 'app\tests\mocks\models\occurrenceMock');

// register alias 'program' to class Program
Yii::$container->set('program', 'app\models\Program');
// register alias 'programMock' to class ProgramMock
Yii::$container->set('programMock', 'app\tests\mocks\models\ProgramMock');

// register alias 'terminalTransaction' to class TerminalTransaction
Yii::$container->set('terminalTransaction', 'app\models\TerminalTransaction');
// register alias 'terminalTransactionMock' to class TerminalTransactionMock
Yii::$container->set('terminalTransactionMock', 'app\tests\mocks\models\TerminalTransactionMock');

// register alias 'germplasm' to class Germplasm
Yii::$container->set('germplasm', 'app\models\Germplasm');
// register alias 'germplasmSearch' to class GermplasmSearch
Yii::$container->set('germplasmSearch', 'app\models\GermplasmSearch');

// register alias 'traits' to class Traits
Yii::$container->set('traits', 'app\models\Traits');

// register alias 'transactionDataset' to class TransactionDataset
Yii::$container->set('transactionDataset', 'app\models\TransactionDataset');
// register alias 'transactionDatasetMock' to class TransactionDatasetMock
Yii::$container->set('transactionDatasetMock', 'app\tests\mocks\models\TransactionDatasetMock');

// register alias 'user' to class User
Yii::$container->set('user', 'app\models\User');
// register alias 'userMock' to class UserMock
Yii::$container->set('userMock', 'app\tests\mocks\models\UserMock');

// register IApi interface
// When a class depends on the IApi,
// ApiMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IApi', 'app\tests\mocks\models\ApiMock');

// register IApplication interface
// When a class depends on the IApplication,
// ApplicationMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IApplication', 'app\tests\mocks\models\ApplicationMock');

// register IBackgroundJob interface
// When a class depends on the IBackgroundJob,
// BackgroundJobMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IBackgroundJob', 'app\tests\mocks\models\BackgroundJobMock');

// register alias 'occurrenceModel' to class Occurrence
Yii::$container->set(
    'occurrenceModel',
    'app\models\Occurrence'
);

// register IOccurrence interface
// When a class depends on the IOccurrence,
// OccurrenceMock will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\IOccurrence',
    'app\tests\mocks\models\OccurrenceMock'
);

// register alias 'occurrenceSearchModel' to class OccurrenceSearch
Yii::$container->set('occurrenceSearchModel', 'app\models\OccurrenceSearch');

// register IExperiment interface
// When a class depends on the IExperiment,
// ExperimentMock will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\IExperiment',
    'app\tests\mocks\models\ExperimentMock'
);

// register ILocation interface
// When a class depends on the ILocation,
// LocationMock will be instantiated as the dependency
Yii::$container->set(
    'app\interfaces\models\ILocation',
    'app\tests\mocks\models\LocationMock'
);

// register IUser interface
// When a class depends on the IUser,
// UserMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IUser', 'app\tests\mocks\models\UserMock');

// register IWorker interface
// When a class depends on the IWorker,
// WorkerMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IWorker', 'app\tests\mocks\models\WorkerMock');

// register IConfig interface
// When a class depends on the IProgram,
// ConfigMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IConfig', 'app\tests\mocks\models\ConfigMock');
Yii::$container->set('configModelMock', 'app\tests\mocks\models\ConfigMock');

// register IProgram interface
// When a class depends on the IProgram,
// ProgramMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IProgram', 'app\tests\mocks\models\ProgramMock');

// register ITerminalTransaction interface
// When a class depends on the ITerminalTransaction,
// TerminalTransactionMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITerminalTransaction', 'app\tests\mocks\models\TerminalTransactionMock');

// register ITransactionDataset interface
// When a class depends on the ITransactionDataset,
// TransactionDatasetMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\ITransactionDataset', 'app\tests\mocks\models\TransactionDatasetMock');

// register ITransaction interface
// When a class depends on the ITransaction,
// Transaction will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\dataCollection\models\ITransaction', 'app\tests\mocks\modules\dataCollection\models\TransactionMock');

// register alias 'transaction' to class Transaction
Yii::$container->set('transaction', 'app\modules\dataCollection\models\Transaction');

// register IVariable interface
// When a class depends on the IVariable,
// VariableMock will be instantiated as the dependency
Yii::$container->set('variable', 'app\models\Variable');
Yii::$container->set('variableMock', 'app\tests\mocks\models\VariableMock');
Yii::$container->set('app\interfaces\models\IVariable', 'app\tests\mocks\models\VariableMock');

// for Lists.php model dependencies
Yii::$container->set('listsOperations', 'app\models\ListsOperations');
Yii::$container->set('listsModel', 'app\models\Lists');
Yii::$container->set('app\interfaces\models\ILists', 'app\tests\mocks\models\ListsMock');
Yii::$container->set('app\interfaces\models\IListMember', 'app\tests\mocks\models\ListMemberMock');

//register IPlatformListMember interface
Yii::$container->set('app\interfaces\models\IPlatformListMember', 'app\tests\mocks\models\PlatformListMemberMock');

//register ILocationOccurrenceGroup interface
Yii::$container->set('app\interfaces\models\ILocationOccurrenceGroup', 'app\tests\mocks\models\LocationOccurrenceGroupMock');

// register IPlantingJob interface
// When a class depends on the IPlantingJob,
// PlantingJobMock will be instantiated as the dependency
Yii::$container->set('plantingJob', 'app\models\PlantingJob');
Yii::$container->set('app\interfaces\models\IPlantingJob', 'app\tests\mocks\models\PlantingJobMock');

// register alias 'plantingJobOccurrence' to class PlantingJobOccurrence
Yii::$container->set('plantingJobOccurrence', 'app\models\PlantingJobOccurrence');

// register alias 'plantingJobEntry' to class PlantingJobEntry
Yii::$container->set('plantingJobEntry', 'app\models\PlantingJobEntry');

// register IPlot interface
// When a class depends on the IPlot,
// PlotMock will be instantiated as the dependency
Yii::$container->set('plot', 'app\models\Plot');
Yii::$container->set('plotMock', 'app\tests\mocks\models\PlotMock');
Yii::$container->set('app\interfaces\models\IPlot', 'app\tests\mocks\models\PlotMock');

// register alias 'browser' to class Browser
Yii::$container->set('browser', 'app\models\Browser');

// register alias 'person' to class Person
Yii::$container->set('person', 'app\models\Person');
Yii::$container->set('app\interfaces\models\IPerson', 'app\tests\mocks\models\PersonMock');

// register alias 'tenantTeam' to class TenantTeam
Yii::$container->set('tenantTeam', 'app\models\TenantTeam');
Yii::$container->set('app\interfaces\models\ITenantTeam', 'app\tests\mocks\models\TenantTeamMock');

// register alias 'tenantProgram' to class TenantProgram
Yii::$container->set('tenantProgram', 'app\models\TenantProgram');
Yii::$container->set('app\interfaces\models\ITenantProgram', 'app\tests\mocks\models\TenantProgramMock');

// register alias 'germplasm' to class Germplasm
Yii::$container->set('germplasm', 'app\models\Germplasm');
Yii::$container->set('app\interfaces\models\IGermplasm', 'app\tests\mocks\models\GermplasmMock');

// register IGermplasmSearch interface
// When a class depends on the IGermplasmSearch,
// GermplasmSearchMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IGermplasmSearch', 'app\tests\mocks\models\GermplasmSearchMock');

// register alias 'germplasmFileUpload' to class GermplasmFileUpload
Yii::$container->set('germplasmFileUpload', 'app\models\GermplasmFileUpload');

// register IGermplasmFileUpload interface
// When a class depends on the IGermplasmFileUpload,
// GermplasmFileUploadMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IGermplasmFileUpload', 'app\tests\mocks\models\GermplasmFileUploadMock');

// register alias 'germplasmFileUploadMock' to class GermplasmFileUploadMock
Yii::$container->set('germplasmFileUploadMock', 'app\tests\mocks\models\GermplasmFileUploadMock');

// register alias 'seed' to class Seed
Yii::$container->set('seed', 'app\models\Seed');

// register alias 'traits' to class Traits
Yii::$container->set('app\interfaces\models\ITraits', 'app\tests\mocks\models\TraitsMock');

// register alias 'CrossManagerOccurrenceModel' to class crossManager\models\OccurrenceModel
Yii::$container->set('crossManagerOccurrenceModel', 'app\modules\crossManager\models\OccurrenceModel');

// register IUserDashboardConfig interface
// When a class depends on the IUserDashboardConfig,
// UserDashboardConfigMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\models\IUserDashboardConfig', 'app\tests\mocks\models\UserDashboardConfigMock');

// Register alias 'userDashboardConfigMock' to mock class 'app\tests\mocks\models\UserDashboardConfigMock'
Yii::$container->set('userDashboardConfigMock', 'app\tests\mocks\models\UserDashboardConfigMock');

// Register alias 'userDashboardConfig' to mock class 'app\models\UserDashboardConfig'
Yii::$container->set('userDashboardConfig', 'app\models\UserDashboardConfig');

// Register alias 'removedListMemberModel to class module\account\models\removeListMemberModel
Yii::$container->set('removedListMemberModel', 'app\modules\account\models\RemovedListMemberModel');

// Register alias 'templateModel to class module\workingList\models\templateModel
Yii::$container->set('templateModel', 'app\modules\workingList\models\TemplateModel');

// Register alias 'indicesSearch to class app\models\IndicesSearch
Yii::$container->set('indicesSearchModel', 'app\models\IndicesSearch');
Yii::$container->set('app\interfaces\models\IIndicesSearch', 'app\tests\mocks\models\IndicesSearchMock');

// Register alias 'fileUploadErrorLogModel' to class app\modules\fileUpload\models\ErrorLogModel
Yii::$container->set('fileUploadErrorLogModel', 'app\modules\fileUpload\models\ErrorLogModel');

// Register alias 'fileUploadSummaryModel' to class app\modules\fileUpload\models\SummaryModel
Yii::$container->set('fileUploadSummaryModel', 'app\modules\fileUpload\models\SummaryModel');

// Register alias 'fileUploadFileUploadHelper' to class app\modules\fileUpload\models\FileUploadHelper
Yii::$container->set('fileUploadFileUploadHelper', 'app\modules\fileUpload\models\FileUploadHelper');
