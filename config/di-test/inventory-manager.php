<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Register mock model interfaces of the Inventory Manager module here
 */

// ALIASES ============================

// Register alias "inventoryCreateModel" to class "app\modules\inventoryManager\models\CreateModel"
Yii::$container->set('inventoryCreateModel','app\modules\inventoryManager\models\CreateModel');

// Register alias "inventoryFileUpload" to class "app\modules\inventoryManager\models\InventoryFileUpload"
Yii::$container->set('inventoryFileUpload','app\modules\inventoryManager\models\InventoryFileUpload');

// Register alias "inventoryTemplatesModel" to class "app\modules\inventoryManager\models\TemplatesModel"
Yii::$container->set('inventoryTemplatesModel','app\modules\inventoryManager\models\TemplatesModel');

// Register alias "inventoryListsModel" to class "app\modules\inventoryManager\models\TemplatesModel"
Yii::$container->set('inventoryListsModel','app\modules\inventoryManager\models\ListsModel');

// Register alias "inventorySeedTransferModel" to class "app\modules\inventoryManager\models\SeedTransferModel"
Yii::$container->set('inventorySeedTransferModel','app\modules\inventoryManager\models\SeedTransferModel');


// INTERFACES ============================

// register IListsModel interface
// When a class depends on the IListsModel,
// ListsModelMock will be instantiated as the dependency
Yii::$container->set('app\interfaces\modules\inventoryManager\models\IListsModel', 'app\tests\mocks\modules\inventoryManager\models\ListsModelMock');
