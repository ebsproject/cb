<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\dataproviders;

use yii\helpers\ArrayHelper;

/**
 * Class ArrayDataProvider
 */
class ArrayDataProvider extends \yii\data\ArrayDataProvider
{
    public $restified = false;
    public $disableDefaultSort = false;
    public $totalCount;

    /** @inheritdoc */
    protected function sortModels($models, $sort)
    {
        if (!$this->disableDefaultSort) {
            $orders = $sort->getOrders();
            if (!empty($orders)) {
                $flatModels = $this->flattenModels($models);
                if (is_array($flatModels) && count($flatModels) > 0 && is_string($flatModels[0])) {
                    ArrayHelper::multisort(
                        $flatModels,
                        array_keys($orders),
                        array_values($orders),
                        SORT_NATURAL | SORT_FLAG_CASE
                    );
                    $models = $this->unflattenModels($flatModels);
                }
            }
        }

        return $models;
    }
    
    protected function prepareModels()
    {
        if (($models = $this->allModels) === null) {
            return [];
        }

        if (($sort = $this->getSort()) !== false) {
            $models = $this->sortModels($models, $sort);
        }

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();

            if (!$this->restified) {
                if ($pagination->getPageSize() > 0) {
                    $models = array_slice($models, $pagination->getOffset(), $pagination->getLimit(), true);
                }
            }
        }

        return $models;
    }

    protected function prepareTotalCount()
    {
        if (isset($this->totalCount) && !empty($this->totalCount)) {
            return $this->totalCount;
        } else {
            return is_array($this->allModels) ? count($this->allModels) : 0;
        }
    }

    /**
     * Transform models' traits to contain only non-array values.
     * NOTE: Only use this when an array of models has different data structures
     * as it is currently O(n^2).
     *
     * @param array $models data browser's array of models and traits data
     * @return array models data with each trait having only one value
     */
    private function flattenModels($models)
    {
        $flatModels = [];
        foreach ($models as $modelsIdx => $modelsData) {
            foreach ($modelsData as $modelIdx => $modelData) {
                $modelData = $modelData['dataValue'] ?? $modelData;
                $flatModels[$modelsIdx][$modelIdx] = $modelData;
            }
        }
        return $flatModels;
    }
}