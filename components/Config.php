<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Contains methods for managing config settings
 */
class Config extends Component
{

    /**
     * Retrieve a feature's threshold value for background processing
     * 
     * @param tool string
     * @param feature string
     *
     * @return int threshold value of the given feature
     */
    public function getAppThreshold($tool, $feature, $sort = null)
    {
        $response = null;

        try {
            $method = 'GET';
            $abbrev = $tool . '_BG_PROCESSING_THRESHOLD';
            $path = 'configurations?abbrev=' . $abbrev . '&limit=1' . $sort;
            $response = Yii::$app->api->getParsedResponse(
                $method,
                $path,
                null,
                '',
                true
            );
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'][0])
        ) {
            $configValue = $response['data'][0]['configValue'];
            if (isset($configValue[$feature])) {
                return intval($configValue[$feature]['size']);
            }
        }

        return null;
    }

    /**
     * Retrieves attribute and suffix to be used in file name from config
     * 
     * @param string $feature Download feature identifier
     * @return array attribute and suffux values
     */
    public function getFileNameAttributeAndSuffix($feature){
        $config = null;
        $suffix = 'File'; // default suffix value if no value in config
        $attribute = 'No_Config'; // default attribute if no value in config

        $method = 'GET';
        $abbrev = 'DOWNLOAD_FILE_NAME';
        $path = 'configurations?abbrev=' . $abbrev . '&limit=1';

        $response = Yii::$app->api->getParsedResponse(
            $method,
            $path,
            null,
            '',
            true
        );

        if (
            $response['success'] &&
            isset($response['data']) &&
            !empty($response['data'][0]['configValue'])
        ) {

            // get config by download feature
            $config = isset($response['data'][0]['configValue'][$feature]) ? 
                $response['data'][0]['configValue'][$feature] : null;

            // if not config for the feature, use DEFAULT config
            $config = empty($config) && isset($response['data'][0]['configValue']['DEFAULT']) ?
                $response['data'][0]['configValue']['DEFAULT'] : $config;
            
            $attribute = isset($config['attribute']) ? $config['attribute'] : null;
            $suffix = isset($config['suffix']) ? $config['suffix'] : 'File';
        }

        return [
            $attribute,
            $suffix
        ];
    }
}
