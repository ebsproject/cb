<?php
/* 
* This file is part of Breeding4Rice.
* Breeding4Rice is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Rice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Rice.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Renders the generic input form
 */

$fieldCount = count($generatedFields);

if(strtolower($formType) === 'modal'){
                        
    for($i=0; $i < $fieldCount; $i++){
        echo $generatedFields[$i]['value'];
    }
}else if(isset($columnClass) && !empty($columnClass)){
    // if with custom class
    echo '<div class="'. $columnClass .' parent-box box-1">';
        for($i=0; $i < $fieldCount; $i++){
            echo $generatedFields[$i]['value'];
        }
    echo '</div>';
}
else{//double panel
?>
<?php
    if(count($defaultFields) > 0){
        $notSet = '<span class="not-set">'. \Yii::t('app', '(not set)').'</span>';
?>  
    <div class="col-md-12 main-container"> 
       <ul class="collapsible collapsible-filter active" id="default-values">
           <li id="default-config">
              <div class="collapsible-header">Default Values</div>
              <div class="collapsible-body" id="default-values-body">
                  <div class="row" id="default-values-container" > 
                      <div class="col-md-12 container-body">
                         <div class = "row" id = "default-value-details" style="overflow-x:auto;">
                             <?php 
                                echo "<table class='table table-borderless'>";
                                echo "<thead><tr>";
                                foreach($defaultFields as $defaultVal){
                                    $value = empty($defaultVal['value']) ? $notSet : $defaultVal['value'];
                                    echo '
                                        <th title="'.$defaultVal['label'].'">'.$defaultVal['label'].'</th>
                                    ';
                                }
                                echo "</tr></thead><tr>";
                                foreach($defaultFields as $defaultVal){
                                    $value = empty($defaultVal['value']) ? $notSet : $defaultVal['value'];
                                    echo '
                                        <td>'.$value.'</td>
                                    ';
                                }

                                echo "</tr></table>";
                              ?>
                         </div>
                      </div>
                  </div>
              </div>
           </li>
       </ul> 
    </div>
<?php
    } // End of if condition
    echo '<div class="col col-sm-12 view-entity-content"><div class="main-panel">';
?>

<div class="col-md-6 s6 parent-box box-1">
    <div id="basic-info1" class="basic-info1">
        <?php
            //For the hidden default fields to be included in the saving of the input form
            for($i=0; $i < count($hiddenDefaultFields); $i++){
                echo $hiddenDefaultFields[$i]['value'];
            }
            if(isset($targetStep) && !empty($targetStep)){
                if($fieldCount > 4){
                    $countFields = ceil($fieldCount / 2);
                }else{
                    $countFields = $fieldCount;
                }
            }else{
                $countFields = ceil($fieldCount / 2);
            }
                            
            for($i=0; $i < $countFields; $i++){
                echo $generatedFields[$i]['value'];
            }
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<div class="col-md-6 s6 parent-box box-2">
    <div id="basic-info2">
        <?php
            for($i=$countFields; $i < $fieldCount; $i++){
                echo $generatedFields[$i]['value'];
            }
        
        ?>
    </div>
</div>
<div class="clearfix"></div>
</div></div>
<?php
    } // End of if condition
?>

<style>
   #default-values-body{
    padding-top:0px;
   }
   #default-values-container{
    margin-bottom:10px;
   }
   .container-body{
    padding-right: 0px;
   }
   #default-value-details{
    margin-left:5px; 
    margin-top:10px;
    margin-bottom: -10px;
    padding-top:5px; 
    padding-right:25px; 
    padding-left:10px;
    background-color: white; 
   }
   .main-container{
      margin-top: -15px;
   }
  .table-borderless > tbody > tr > td,
  .table-borderless > tbody > tr > th,
  .table-borderless > tfoot > tr > td,
  .table-borderless > tfoot > tr > th,
  .table-borderless > thead > tr > td,
  .table-borderless > thead > tr > th,
  .table-borderless > thead {
    border: none;
  }
</style>