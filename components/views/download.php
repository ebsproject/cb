<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Url;

// URLs
$renderDownloadModalUrl = Url::toRoute(['/download/default/render-modal-body']);
$downloadUrl = Url::toRoute(['/download/default/download']);
$setSelectedOccurrenceIdsUrl = Url::to(['/download/default/set-selected-occurrence-ids']);

$this->registerJs(<<< JS
var program = '$program'
var selectedIds = $occurrenceIds
var checkboxSessionStorage = '$checkboxSessionStorage'
var setSelectedOccurrenceIdsUrl = '$setSelectedOccurrenceIdsUrl'

$(document).ready(function () {
    refreshDownload()
})

// reload grid on pjax
$(document).off('ready pjax:success').on('ready pjax:success', function(e) {
    e.stopPropagation();
    e.preventDefault();
    refreshDownload();
});

function refreshDownload(){
    // Entry point button click event
    $(`.download-widget-entry-point-button`).off('click').on('click', function (e) {
        let selectedIds = JSON.parse(sessionStorage.getItem(checkboxSessionStorage))

        // If no occurrences were selected, display error
        if (!selectedIds.length) {
            const message = 'No occurrences selected. Please select at least one and try again.'

            let notif = "<i class='material-icons red-text left'>close</i> No occurrence selected. Please select at least one and try again."
            Materialize.toast(notif,5000)
            
            // Stop the loading from the html link
            window.stop()
            
            // Remove loading indicator
            $('#system-loading-indicator').html('')

            return
        }

        // Extract only the occurrence IDs
        selectedIds = selectedIds.map(element => element.id)

        const returnUrl = $(this).data('return-url') ?? ''
        const entities = $(this).data('entities') ?? ''
        const filenameAbbrev = $(this).data('filename-abbrev') ?? ''
        const isInDataBrowserPage = $(this).data('is-in-data-browser-page') ?? true
        const sourceUrl = $(this).data('return-url') ?? ''

        // Set file name abbrev for retrieval of file name later
        $('.download-widget-download-button').data('filename-abbrev', filenameAbbrev)
        $('.download-widget-download-button').data('entities', entities)

        // Store selected occurrence ids in session storage first...
        // ...then proceed to retrieve from session and render the contents of the download modal
        $.ajax({
            type: 'POST',
            url: setSelectedOccurrenceIdsUrl,
            data: { selectedOccurrenceIds: selectedIds },
            success: function () {
                // Render the contents of the download modal
                let renderDownloadModal = '$renderDownloadModalUrl'
                        + '?program=' + program
                        + '&entities=' + entities
                        + '&isInDataBrowserPage=' + isInDataBrowserPage
                        + '&returnUrl=' + returnUrl
                        + '&functionalityName=' + filenameAbbrev;

                window.location.assign(renderDownloadModal);
            },
            error: function (jqXHR, textStatus, errorThrown) {}
        })
    })
}

JS);

Yii::$app->view->registerCss('
    i.download-widget-modal-icon {
        vertical-align: -20%;
    }
');