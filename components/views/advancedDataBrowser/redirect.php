<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * This page just redirects the user to the data browser with previously applied
 * filter and sort. I know that this is a super roundabout way but the following
 * Yii methods just don't want to work inside a (Yii) Widget for some reason:
 *  - Yii::$app->getResponse()->redirect($redirectUrl);
 *  - $this->redirect($redirectUrl);
 * So I was forced to redirect the user manually using JS. Which works!
 */

$this->registerJs(<<<JS
    // redirect
    var notif = '<i class="material-icons blue-text" style="margin-right: 8px;">info</i> $message The page will refresh.';
    Materialize.toast(notif, 2500);
    window.location.href = window.location.origin + '$redirectUrl';
JS
);
?>