<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Index page for view germplasm widget
 */

// Import Yii helpers
use yii\helpers\Url;
use yii\helpers\Html;

// Import kartik shenanigans
use kartik\dynagrid\DynaGrid;

// Import models
use app\models\UserDashboardConfig;
use app\models\DataBrowserConfiguration;

// Format attributes to be passed to DynaGrid here
// Format $gridOptions
if(isset($gridOptions) && !is_null($gridOptions)) {
    $gridOptions = array_replace_recursive($gridOptions, [
        'dataProvider' => $dataProvider,
        'filterModel' => $filterModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ]);
} else {    // Default value
    $gridOptions = [
        'rowOptions' => [
            'class' => 'adb-row'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $filterModel,
        'showPageSummary'=>false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>$browserId.'-pjax',
                // 'enablePushState'=>false,
                // 'enableReplaceState'=>true
            ],
            'beforeGrid'=>'',
            'afterGrid'=>''
        ],
        'responsiveWrap'=>false,
        'panel' => [
            'heading'=>false,
            'before' => $browserDescription.' {summary} ',
            'after' => false,
        ],
        'floatHeader' => true,
        'floatOverflowContainer'=> true,
        'toolbar' => [
            '{dynagrid}'
        ],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
    ];
}

?>

<div id="<?= $browserId ?>-adb-container">
    <!-- data browser -->
    <?=
        DynaGrid::widget([
            'options' => [
                'id'=>$browserId,
            ],
            'columns' => $columns,
            'storage' => DynaGrid::TYPE_SESSION,
            'theme' => 'simple-default',
            'showPersonalize' => $showPersonalize ?? true,
            'showFilter' => false,
            'showSort' => false,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'gridOptions' => $gridOptions,
            'submitButtonOptions' => [
                'icon' => 'glyphicon glyphicon-ok',
            ],
            'deleteButtonOptions' => [
                'icon' => 'glyphicon glyphicon-remove',
                'label' => 'Remove',
            ],
        ])
    ?>
</div>

<?php

$setDefaultPageSizeUrl = Url::to(['/dashboard/default/set-default-page-size']);
$currentDefaultPageSize = UserDashboardConfig::getDefaultPageSizePreferences('default');
(new DataBrowserConfiguration())->saveDataBrowserSettings([$browserId]);

// variables for change page size
Yii::$app->view->registerJs("
    var setDefaultPageSizeUrl = '".$setDefaultPageSizeUrl."',
    browserId = '".$browserId."',
    currentDefaultPageSize = '".$currentDefaultPageSize."'
    ;",
\yii\web\View::POS_HEAD);

// js file for data browsers
$this->registerJsFile("@web/js/data-browser.js", [
    'depends' => ['app\assets\AppAsset'],
    'position'=>\yii\web\View::POS_END
]);

$this->registerJs(<<<JS
    adbRefresh();

    $(document).ready(function() {
        // Adjust browser page size, if the page size was changed in another browser
        adjustBrowserPageSize();
        adbRefresh();
    });

    function adbRefresh() {
        var maxHeight = ($(window).height() - 300);
        $(".kv-grid-wrapper").css("height",maxHeight);
    }
JS
);

?>

<style type="text/css">
    div.empty {
        visibility: hidden
    }
</style>