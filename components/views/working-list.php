<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

// Urls
$getWorkingListCountUrl = Url::to(['list-management/get-working-list-count']);

/**
 * View page for rendering working list widget access button
 */
echo Html::a('<i class="material-icons">shopping_cart</i>
    <span id="notif-badge-id" class="notification-badge red accent-2">'.$itemCount.'</span>',
    '#',
    [
        'class' => 'working-list-btn btn btn-default',
        'title' => Yii::t('app', 'Working List'),
        'data-sourceTool' => $sourceTool,
        'data-returnUrl' => $returnUrl,
        'data-listType' => $listType
    ]
);

$workingListUrl = Url::toRoute(['/workingList/default/index']);

$script = <<< JS

let itemsCount = '$itemCount'

$("document").ready(function(){
    
    // error logs button click event
    $(document).on('click', '.working-list-btn', function(e) {
        let returnUrl = '$returnUrl';
        let sourceTool = $(this).attr('data-sourceTool');
        let listType = $(this).attr('data-listType');

        // Build template download url
        let workingList = '$workingListUrl'
            + '?sourceTool=' + sourceTool
            + '&listType=' + listType
            + '&returnUrl=' + returnUrl;

        window.location.assign(workingList);
    });
});


function renderListItemCount(){
    $('#notif-badge-id').html(''+itemsCount).trigger('change');
}
   
   
/**
* Show and update working list count in notification
* Call this function in order to update WL count notification
*
*/
function updateWorkingListItemCount(){
    $.ajax({
        url: '$getWorkingListCountUrl',
        type: 'POST',
        dataType: 'json',
        success: function(response){
            itemsCount = response['totalCount'] ?? 0;

            $('#notif-badge-id').html(response['totalCount']);
            $('#notif-badge-id').removeClass('notification-badge red accent-2').addClass('notification-badge red accent-2');
        },
        error: function(){
            showMessage('There was a problem in retrieving Working List. Please try again later or file a report for assistance.', 'error');
        }
    });
}  

JS;

$this->registerJs($script);

// Sets css for notification
$this->registerCss('
    .notification-badge {
        font-family: "Poppins", sans-serif;
        position: relative;
        right: 0px;
        top: -20px;
        color: #ffffff;
        background-color: #3f51b5;
        margin: 0 -.8em;
        border-radius: 50%;
        padding: 2px 5px;
    }
');