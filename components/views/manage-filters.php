<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering manage filters UI
 */

echo Html::a('<i class="glyphicon glyphicon-filter"></i>', '#', ['class' => 'filter-action-button btn btn-default', 'title'=>\Yii::t('app','Manage filters')]);

// filter action modal
Modal::begin([
    'id' => 'filter-action-modal',
    'header' => '<h4><i class="glyphicon glyphicon-filter"></i> '.\Yii::t('app','Filter Management'). '</h4>',
    'footer' =>
        Html::a('Cancel', ['#'], ['id' => 'filter-action-modal-cancel-btn', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'.
        Html::a(\Yii::t('app','Apply').'&nbsp; <i class="material-icons">check</i>','#',['id' => 'filter-action-modal-apply-btn', 'class'=>'btn btn-default', 'data-dismiss'=> 'modal']).'&emsp;&emsp;&nbsp;',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="filter-action-modal-body"></div>';
Modal::end();

$renderFilterModal = Url::to(['/userFilter/data-browser-filter/index']);

$btnClass = (isset($btnClass) && !empty($btnClass)) ? $btnClass : 'filter-action-button';

$this->registerJs(<<<JS

	$(document).on('click', '.$btnClass', function(e) {
	    var columnNames = JSON.parse('$columnNames');
	    var filterResetGridUrl = "$resetGridUrl";

	    //load content of view more information
	    setTimeout(function(){
	        $.ajax({
	            url: '$renderFilterModal',
	            data: {
	                columnNames: columnNames,
	                dataBrowserId: '$dataBrowserId',
	                columnNameAndDatatypes: JSON.parse('$columnNameAndDataTypes'),
	                browserUrl: filterResetGridUrl,
	                browserModel: '$searchModel',
	                browserPjaxId: '#$gridId-pjax'
	            },
	            type: 'POST',
	            async: true,
	            success: function(data) {
	                $('.filter-action-modal-body').html(data);
	                $('#filter-action-modal').modal('show');
	            },
	            error: function(){
	                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading filter configuration. Please try again.';
	                Materialize.toast(notif, 5000);
	            }
	        });
	    }, 300);
	});

JS
);

Yii::$app->view->registerCss('
	.btn {
		float: none !important
	}
');