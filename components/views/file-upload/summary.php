<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering summary UI
 */

echo Html::a('<i class="material-icons">visibility</i>',
    '#',
    [
        'class' => 'file-upload-summary-btn',
        'title' => Yii::t('app', 'View Summary'),
        'data-id' => $fileDbId,
        'data-abbrev' => $configAbbrevPrefix
    ]
);

$summaryUrl = Url::toRoute(['/fileUpload/default/summary']);

$script = <<< JS

$("document").ready(function(){

    // error logs button click event
    $(document).on('click', '.file-upload-summary-btn', function(e) {
        let selectedId = $(this).attr('data-id');
        let returnUrl = encodeURIComponent('$returnUrl');
        let sourceToolName = encodeURIComponent('$sourceToolName');
        let configAbbrevPrefix = $(this).attr('data-abbrev');

        // Build template download url
        let summaryUrl = '$summaryUrl'
            + '?fileDbId=' + selectedId
            + '&returnUrl=' + returnUrl
            + '&sourceToolName=' + sourceToolName
            + '&configAbbrevPrefix=' + configAbbrevPrefix;

        window.location.assign(summaryUrl);
    });
});
    
JS;

$this->registerJs($script);