<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering file data download button
 */

echo Html::a('<i class="material-icons">file_download</i>',
    '#',
    [
        'id' => 'file-upload-dl-fdata-btn-' . $fileDbId,
        'class' => 'file-upload-dl-fdata-btn hide-loading',
        'title' => Yii::t('app', 'Download file'),
        'data-id' => $fileDbId,
        'data-abbrev' => $configAbbrevPrefix
    ]
);

$downloadCsvUrl = Url::toRoute(['/fileUpload/default/download-file-data-csv']);

$script = <<< JS

$("document").ready(function(){

    // error logs button click event
    $(document).on('click', '#file-upload-dl-fdata-btn-$fileDbId', function(e) {
        selectedId = $(this).attr('data-id');

        // Build template download url
        let downloadCsvUrl = '$downloadCsvUrl'
                + '?fileDbId=' + selectedId
                + '&configAbbrevPrefix=$configAbbrevPrefix'
                + '&sourceToolName=$sourceToolName';

        // Build complete URL
        let csvFileUrl = window.location.origin + downloadCsvUrl;

        window.location.assign(csvFileUrl);
    });

    // display and hide loading indicator
    $(document).on('click','.hide-loading',function(){
        $('#system-loading-indicator').css('display','block');
        setTimeout(function() {
            $('#system-loading-indicator').css('display','none');
        },3000);
    });
});
    
JS;

$this->registerJs($script);