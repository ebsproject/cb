<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering error logs UI
 */

echo Html::a('<i class="material-icons">assignment_late</i>',
    '#',
    [
        'class' => 'file-upload-error-logs-btn',
        'title' => Yii::t('app', 'View Error Logs'),
        'data-id' => $fileDbId
    ]
);

$errorLogsUrl = Url::toRoute(['/fileUpload/default/error-logs']);

$script = <<< JS

$("document").ready(function(){

    // error logs button click event
    $(document).on('click', '.file-upload-error-logs-btn', function(e) {
        selectedId = $(this).attr('data-id');
        returnUrl = encodeURIComponent('$returnUrl');
        sourceToolName = encodeURIComponent('$sourceToolName');

        // Build template download url
        let errorLogsUrl = '$errorLogsUrl'
            + '?fileDbId=' + selectedId
            + '&returnUrl=' + returnUrl
            + '&sourceToolName=' + sourceToolName;

        window.location.assign(errorLogsUrl);
    });
});
    
JS;

$this->registerJs($script);