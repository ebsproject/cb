<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Yii;

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

use app\modules\account\controllers\ListController;

/**
 * View page for rendering printouts UI
 */

// Build button style from array
$buttonStyleStr = implode(";", array_map(
    function ($v, $k) {
        return $k . ':' . $v;
    },
    $buttonStyle,
    array_keys($buttonStyle)
)) . ";";

// Build printouts button
echo Html::a(
    '<i class="material-icons">print</i>',
    '#',
    [
        'class' => 'printouts-button btn btn-default', 'title' => \Yii::t('app', 'Printouts'),
        'style' => $buttonStyleStr
    ]
);

// printouts modal
Modal::begin([
    'id' => 'printouts-modal',
    'header' => '<h4><i class="material-icons printouts-modal-icon">print</i> ' . \Yii::t('app', 'Printouts') . '</h4>',
    'footer' =>
    Html::a(
        'Cancel',
        '#',
        [
            'id' => 'printouts-modal-cancel-btn'
        ]
    ) .  '&emsp;' .
        Html::a(
            \Yii::t('app', 'Download'),
            '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light printouts-btn hide-loading disabled',
                'id' => 'printouts-modal-download-button'
            ]
        ) .  '&emsp;',
    'options' => [
        'data-backdrop' => 'static',
        'style' => 'z-index: 1040;'
    ],
    'closeButton' => [
        'class' => 'hidden'
    ],
]);
echo '<div class="printouts-modal-body"></div>';
Modal::end();

// Variables
$renderPrintoutsModal = Url::toRoute(['/printouts/default/render-printouts-modal']);
$getSelectedFromSessionUrl = Url::to([$entityValueSource]);

$entityDbIdsJSON = isset($entityDbIds) && !empty($entityDbIds) ? json_encode($entityDbIds) ?? " " : " ";

$sessionEntityValues = [];
if (isset($entityType) && !empty($entityType)) {
    $sessionEntityValues = Yii::$app->session[$entityType . "-selected_ids"] ?? [];
    $entityDbIdsJSON = json_encode($sessionEntityValues);
}

$script = <<< JS
var product = '$product';
var program = '$program';
var entityName = '$entityName';
var entityDisplayName = '$entityDisplayName';
var entityDbIds = $entityDbIdsJSON ??  " ";
var checkboxSessionStorage = '$checkboxSessionStorage';

var entityType = '$entityType';
var entityValueSource = '$getSelectedFromSessionUrl';

$("document").ready(function(){
    // printouts button click event
    $(document).off('click', '.printouts-button').on('click', '.printouts-button', function(e) {
        // Progress bar
        $('.printouts-modal-body').html(
            '<em><p>Connecting to the printouts service...</p></em>'
            + '<div class="progress"><div class="indeterminate"></div></div>');
        $('#printouts-modal').modal('show');
        // Disable download button by default
        $('#printouts-modal-download-button').addClass('disabled');

        // If checkboxSessionStorage is set, retrieve and parse
        var selected = ""
        if(entityType){
            getEntitySessionValues(entityValueSource)
            sessionStorage.setItem(checkboxSessionStorage,JSON.stringify(entityDbIds))
        }

        // Switch source to entryDbIds when not in sessionCheckboxSelectedIds
        if (checkboxSessionStorage != undefined && checkboxSessionStorage != null && checkboxSessionStorage != '') {
            selected = JSON.parse(sessionStorage.getItem(checkboxSessionStorage));
            
            if(typeof selected == 'string'){
                selected = JSON.parse(selected);
            }

            // If selected items are of Object type
            if (selected && typeof selected[0] == 'object') {
                selected = selected.map(element => element.id)
            }

            entityDbIds = selected !== null ? selected : [];
        }

        // If no occurrences were selected, display error
        if (entityDbIds.length == 0) {
            var message = 'No <strong>' + entityName + '</strong> record was selected. Please select at least one and try again.'
            
            $('.printouts-modal-body').html('<p>' + message + '</p>');
            $('#printouts-modal').modal('show');
            return;
        }

        // Render the contents of the printouts modal
        $.ajax({
            url: '$renderPrintoutsModal',
            data: {
                entityDbIds: JSON.stringify(entityDbIds),
                product: product,
                program: program,
                entityName: entityName,
                entityDisplayName: entityDisplayName,
            },
            type: 'POST',
            async: true,
            success: function(html) {
                $('.printouts-modal-body').html(html);
            },
            error: function(){
                $('.printouts-modal-body').html('<p>There was a problem while loading. Please try again later.</p>');
            }
        });
    });

    // cancel button click event
    $(document).on('click', '#printouts-modal-cancel-btn', function(e) {
        // Close the modal
        $("#printouts-modal.in").modal('hide');
    });
});

// retrieve entity sessionfrom other source
function getEntitySessionValues(url){

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            type: 'germplasm'
        },
        async: false,
        dataType: 'json',
        success: function(response) {
            let results = JSON.parse(response);
            entityDbIds = results.currentSelectedIds ?? [];
        },
        error: function(){
            var notif = "<i class='material-icons red-text'>close</i> There was a problem while loading content.";
            Materialize.toast(notif,5000);
        }
    });
}
    
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
	.printouts-btn {
		float: none !important
	}

    i.printouts-modal-icon {
        vertical-align: -20%;
    }
');
