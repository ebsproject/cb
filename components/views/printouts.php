<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering printouts UI
 */

echo Html::a(
    '<i class="material-icons">print</i>',
    '#',
    [
        'class' => 'printouts-button btn btn-default', 'title'=>\Yii::t('app','Printouts')
    ]
);

// printouts modal
Modal::begin([
    'id' => 'printouts-modal',
    'header' => '<h4><i class="material-icons printouts-modal-icon">print</i> ' . \Yii::t('app','Printouts') . '</h4>',
    'footer' =>
        Html::a(
            'Cancel',
            '#',
            [
                'id' => 'printouts-modal-cancel-btn',
                'data-dismiss'=> 'modal'
            ]
        ) .  '&emsp;' .
        Html::a(\Yii::t('app', 'Download'), '#',
            [
                'class' => 'btn btn-primary waves-effect waves-light printouts-btn hide-loading disabled',
                'id' => 'printouts-modal-download-button'
            ]
        ) .  '&emsp;',
    'options' => [
        'data-backdrop'=>'static',
        'style'=>'z-index: 1040;'
    ]
]);
echo '<div class="printouts-modal-body"></div>';
Modal::end();

// Variables
$renderPrintoutsModal = Url::toRoute(['/printouts/default/index']);
$selectedIdsJSON = json_encode($occurrenceIds) ?? [];

$script = <<< JS
var product = '$product';
var program = '$program';
var entity = '$entity';
var selectedIds = $selectedIdsJSON;
var checkboxSessionStorage = '$checkboxSessionStorage';

$("document").ready(function(){

    // printouts button click event
    $(document).on('click', '.printouts-button', function(e) {
        // Add loading indicator
        $('.printouts-modal-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#printouts-modal').modal('show');
        // Disable download button by default
        $('#printouts-modal-download-button').addClass('disabled');

        // If checkboxSessionStorage is set, retrieve and parse
        if (checkboxSessionStorage != '') {
            var selected = JSON.parse(sessionStorage.getItem(checkboxSessionStorage));

            // If selected items are of Object type
            if (selected && typeof selected[0] == 'object') {
                selected = selected.map(element => element.id)
            }

            selectedIds = selected !== null ? selected : [];
        }

        // If no occurrences were selected, display error
        if (selectedIds.length == 0) {
            var message = 'No ' + entity + ' selected. Please select at least one ' + entity + ' and try again.'
            var crossListMsg = 'No valid cross list selected. Please select at least one cross list with CROSS LIST SPECIFIED or FINALIZED status and try again.'
            
            message = entity == 'cross list' ? crossListMsg : message;
            $('.printouts-modal-body').html('<p>' + message + '</p>');
            $('#printouts-modal').modal('show');
            return;
        }

        // Render the contents of the printouts modal
        $.ajax({
            url: '$renderPrintoutsModal',
            data: {
                selectedIds: JSON.stringify(selectedIds),
                product: product,
                program: program,
                entity: entity,
            },
            type: 'POST',
            async: true,
            success: function(html) {
                $('.printouts-modal-body').html(html);
            },
            error: function(){
                $('.printouts-modal-body').html('<p>There was a problem while loading. Please try again later.</p>');
            }
        });
    });
});
    
JS;

$this->registerJs($script);

Yii::$app->view->registerCss('
	.printouts-btn {
		float: none !important
	}

    i.printouts-modal-icon {
        vertical-align: -20%;
    }
');