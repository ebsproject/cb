<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * View page for rendering manage sorts UI
 */

echo Html::a('<i class="glyphicon glyphicon-sort"></i>', '#', ['class' => 'sort-action-button btn btn-default', 'title'=>\Yii::t('app','Manage sorts')]);

// sort action modal
Modal::begin([
    'id' => 'sort-action-modal',
    'header' => '<h4><i class="glyphicon glyphicon-sort"></i> '.\Yii::t('app','Sort Management'). '</h4>',
    'footer' =>
        Html::a('Cancel', ['#'], ['id' => 'sort-action-modal-cancel-btn', 'data-dismiss'=> 'modal']) . '&emsp;&emsp;'.
        Html::a(\Yii::t('app','Apply'),'#',['id' => 'sort-action-modal-apply-btn', 'class'=>'btn manage-sorts-btn btn-default', 'data-dismiss'=> 'modal']).'&emsp;&emsp;&nbsp;',
    'size' =>'modal-lg',
    'options' => ['data-backdrop'=>'static','style'=>'z-index: 9999999;']
]);
echo '<div class="sort-action-modal-body"></div>';
Modal::end();

$renderSortModal = Url::to(['/userSort/data-browser-sort/index']);

$btnClass = (isset($btnClass) && !empty($btnClass)) ? $btnClass : 'sort-action-button';

$this->registerJs(<<<JS

	$(document).on('click', '.$btnClass', function(e) {
	    var columnNames = JSON.parse('$columnNames');
	    var sortResetGridUrl = "$resetGridUrl";

	    //load content of view more information
	    setTimeout(function(){
	        $.ajax({
	            url: '$renderSortModal',
	            data: {
	                columnNames: columnNames,
	                dataBrowserId: '$dataBrowserId',
	                browserUrl: sortResetGridUrl,
	                browserModel: '$searchModel',
	                browserPjaxId: '#$gridId-pjax'
	            },
	            type: 'POST',
	            async: true,
	            success: function(data) {
	                $('.sort-action-modal-body').html(data);
	                $('#sort-action-modal').modal('show');
	            },
	            error: function(){
	                var notif = '<i class="material-icons orange-text">warning</i> There was a problem while loading sort configuration. Please try again.';
	                Materialize.toast(notif, 5000);
	            }
	        });
	    }, 300);
	});

JS
);

Yii::$app->view->registerCss('
	.manage-sorts-btn {
		float: none !important
	}
');