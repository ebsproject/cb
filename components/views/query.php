<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * View file for the query widget tool
 */

$accordion = '';
if (!empty($currentlyAppliedFilters)) {
    $accordion = 'active';
}
?>

<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
<li class="active">
        <div class="progress find-loading-progress hidden" style="margin:0px;padding:0px;">
            <div class="indeterminate"></div>
        </div>
    <div class="collapsible-header collapsible-query-header active"><em class="material-icons">search</em>Query Parameters</div>
    <div class="collapsible-body" style="display: block;">
        <?php 
            $form = ActiveForm::begin([
                'action' => '',
                'enableClientValidation' => false,
                'id' => $formId
            ]);
        ?>
        <?php
            echo $basicFlds;
        ?>

        <div class="row" style="margin:0">
            <ul id="addl-query-acc" class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="li-addl active">
                <div class="collapsible-header collapsible-query-header <?php echo $accordion; ?>"><em class="material-icons">list</em>Additional Search Parameters</div>
                <div class="collapsible-body" style="display: block;">
                    <div class="additional-filter-container">
                        <div class="container-query">
                            <div style="max-height:100%">
                                <ul id="additional-query-list">
                                </ul>
                            </div>
                        </div>
                        <button id="query-add-condition-btn" class="btn btn-default"><span>Add condition</span></button>
                        <button id="query-input-list-btn" disabled class="btn btn-default"><span>Input List</span></button>
                    </div>
                </div>
            </li>
            </ul>
        </div>
        <div class="row action-btns" style="margin:0" id="action-btns">
            <button id="query-find-btn" style="float:right !important;" class="waves-effect waves-light btn light-green darken-3 pull-right" title="Find" style="margin-right:13px;" data-pjax="">Find</button>
            <button id="query-clear-all-btn" style="float:right !important; margin-right:5px; background-color: white !important; color: gray" class="btn btn-default">
                <span>Reset</span>
            </button>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</li>
</ul>

<!-- Input List Modal -->
<div id="query-input-list-modal" class="fade modal in" role="dialog" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><em class="material-icons">format_list_numbered</em> Input List </h4>
            </div>
            <div class="modal-body" id="query-input-list-body">
            </div>
            <div class="modal-footer">
                <div class="row">
                <?php
                    echo Html::a('Cancel', ['#'], ['id'=>'cancel-save', 'data-dismiss'=>'modal']) . '&emsp;&emsp;'
                    .Html::a('Confirm',
                        '#',
                        [
                            'class' => 'btn btn-primary waves-effect waves-light', 
                            'url' => '#',
                            'id' => 'query-confirm-list-btn'
                        ]
                    ) . '&emsp;';
                ?>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
$getInputFldUrl = Url::to(['/query/default/get-select-field']);
$savedListsUrl = Url::to(['/query/default/render-lists']);

$this->registerJs(<<<JS
    var getInputFldUrl = "$getInputFldUrl";
    var findUrl = "$findUrl";
    var renderSavedListsUrl = "$savedListsUrl";
    var totalCount = "$totalCount";
    var addlFlds = JSON.parse('$addlFlds');
    var addlFldsDataType = JSON.parse('$addlFldsDataType');
    var addlFldsOptions = JSON.parse('$addlFldsOptions');
    var addlFldsCurrent = $addlFldsCurrent;
    var gridId = "$gridId";
    var columnsSelected = {};
    var columnsSelectedCount = 0;
    var addlColumnsCount = 0;
    var selectOptionData = [];
    var inputListOptionData = [];
    var currentInputListCount = 0;
    var entity = "$entity";
    var tool = "$tool";
    var formId = "$formId";
    var clearSelectionCb = "$clearSelectionCb";
    var searchLimit = 5000;
    var searchCharMin = "$searchCharMin" ?? 3;

    $(document).ready(function() {
        buildSelectOptionData();
        // build current filters
        buildCurrentFilters();
        updateInputListBtn();
        
        if (totalCount > 0) {
            $('.collapsible').collapsible('close', 0);
        }
    });

    // define available operators for given data type
    var columnConditionOptions = {
        'string': [
            'equals',
            'not equals',
            'like',
            'not like',
            'is null',
            'is not null',
            'contains'
        ],
        'number': [
            'equals',
            'not equals',
            'less than',
            'less than or equal to',
            'greater than',
            'greater than or equal to',
            'range',
            'is null',
            'is not null'
        ],
        'timestamp': [
            'equals',
            'not equals',
            'range'
        ]
    };

    /**
     * Functions definitions
     */
    function buildSelectOptionData() {
        selectOptionData = [];
        inputListOptionData = [];
        addlColumnsCount = 0;
        // build select option
        for (let fldIndex of Object.keys(addlFlds)) {
            var fldObj = addlFlds[fldIndex];
            
            selectOptionData.push({
                id: fldObj.abbrev,
                text: fldObj.label
            });

            columnsSelected[fldObj.abbrev] = false;
            addlColumnsCount += 1;

            var fldOptions = addlFldsOptions[fldObj.abbrev];
            if (fldOptions['allow_filter_list'] == 'true') {
                inputListOptionData.push({
                    id: fldObj.abbrev,
                    text: fldObj.label
                });
            }
        }
    }

    function updateDisabledColumn() {
        $('#additional-query-list li').each(function(listIndex, listElement) {
            var selectElement = $(listElement).find('.query-column-name-select');
            inputListOptionData = [];
            for (let fldIndex of Object.keys(addlFlds)) {
                var columnObj = addlFlds[fldIndex];
                var fldOptions = addlFldsOptions[columnObj.abbrev];
                if (columnsSelected[columnObj.abbrev] == true) {
                    selectElement.children('[value="'+columnObj.abbrev+'"]').prop('disabled', true);
                } else {
                    selectElement.children('[value="'+columnObj.abbrev+'"]').prop('disabled', false);
                    if (fldOptions['allow_filter_list'] == 'true') {
                        inputListOptionData.push({
                            id: columnObj.abbrev,
                            text: columnObj.label
                        });
                    }
                }
            }
            updateInputListBtn();
        });
    }

    function updateInputListBtn() {
        if (inputListOptionData.length > 0) {
            $('#query-input-list-btn').removeAttr("disabled");
        } else {
            $('#query-input-list-btn').attr("disabled", "disabled");
        }
    }

    function addOperatorsOptions (params) {
        var currentElement = params['element'];
        // Remove condition and values to the right if they exist
        if($(currentElement).parent().next().length) {
            $(currentElement).parent().nextAll().remove();
        }

        var selectedColumnName = $(currentElement).select2('data')[0].id;
        var selectedKey = addlFldsOptions[selectedColumnName]['fieldName'];
        var selectedColumnInputFld =  addlFldsOptions[selectedColumnName]['input_field'];

        $(currentElement).parents().eq(1).append('<div class="col-md-3">'
            +'<select name="Query['+selectedKey+'][operator]" class="query-condition-select" id="operator-'+selectedColumnName+'">'
            +'</select>'
        +'</div>');
        
        var listElement = $('#additional-query-list');
        var listItemIndex = currentElement.parents().eq(3).index();
        var columnConditionSelectElement = listElement.children().eq(listItemIndex).find('.query-condition-select');
        var selectedColumnDataType = addlFldsDataType[selectedColumnName];
        var conditionOptions = columnConditionOptions[selectedColumnDataType];
        
        var selectData = [];
        for (var colCondition of conditionOptions) {
            selectData.push({
                id: colCondition,
                text: colCondition
            });
        }

        columnConditionSelectElement.select2({
            data: selectData
        });

        var currentValue = '';
        if (typeof params['currentFilterObj'] !== 'undefined') {
            var currentFilterObj = params['currentFilterObj'];
            // select current operator
            for (let operator of Object.keys(currentFilterObj)) {
                columnConditionSelectElement.val(operator).trigger('change');
                currentValue = currentFilterObj[operator];
                break;
            }
        } else {
            if (selectedColumnDataType == 'string' && selectedColumnInputFld != 'selection') {
                columnConditionSelectElement.val('equals').trigger('change');
            }
        }

        addInputValueField({
            'element': columnConditionSelectElement,
            'columnName': selectedColumnName,
            'currentValue': currentValue
        });
    }

    function addInputValueField (params) {
        var currentElement = params['element'];
        var columnName = params['columnName'];
        var currentValue = '';
        // Remove input values to the right if they exist
        if ($(currentElement).parent().next().length) {
            $(currentElement).parent().nextAll().remove();
        }

        var selectedOperator = $(currentElement).select2('data')[0].id;
        var selectedFldOptions = addlFldsOptions[columnName];

        // check if there's a current value available
        if (typeof params['currentValue'] !== 'undefined') {
            currentValue = params['currentValue'];
        }

        // // No input field need if chosen = is null or is not null
        if (selectedOperator != 'is null' && selectedOperator != 'is not null') {
            var selectedColumnInputFld = selectedFldOptions['input_field'];
            if ((selectedOperator == 'equals' || selectedOperator == 'not equals') && selectedColumnInputFld == 'selection') {
                // get input field
                $.ajax({
                    url: getInputFldUrl,
                    data: {
                        columnName: columnName,
                        selectedOperator: selectedOperator,
                        selectedFldOptions: selectedFldOptions,
                        entity: entity,
                        currentValue: currentValue
                    },
                    type: 'POST',
                    // dataType: '0',
                    success: function(response) {
                        $(currentElement).parents().eq(1).append(response);
                    }
                });
            } else {
                var selectedKey = selectedFldOptions['fieldName'];
                var selectedAbbrev = selectedFldOptions['variable_abbrev'];
                var inputType = 'text';
                var selectedColumnDataType = addlFldsDataType[columnName];
                var instruction = '';
                if (['less than', 'less than or equal to', 'greater than', 'greater than or equal to'].includes(selectedOperator)) {
                    inputType = 'number';
                } else {
                    if (currentValue != '') {
                        currentValue = currentValue.replace('|', ',');
                    }
                    if (selectedColumnDataType == 'string') {
                        instruction = 'Separate values with comma (,) eg. value1, value2';
                    } else if (selectedColumnDataType == 'number' && selectedOperator == 'range') {
                        instruction = 'Input range of values separated with dash (-) e.g 2-10'
                    }
                }
                $(currentElement).parents().eq(1).append('<div class="col-md-5">'
                    +'<input name="Query['+columnName.toLowerCase()+']" data-variable-abbrev="'+selectedAbbrev+'" data-column-name="'+selectedKey+'" id="query-fld-'+columnName+'" class= "query-flds query-additional-flds" value="'+currentValue+'" placeholder="" type="'+inputType+'">'
                    + instruction
                +'</div>');
            }
        } else {
            var selectedKey = selectedFldOptions['fieldName'];
            var selectedAbbrev = selectedFldOptions['variable_abbrev'];
            $(currentElement).parents().eq(1).append('<div class="col-md-5">'
                +'<input style="display:none" name="Query['+columnName.toLowerCase()+']" data-variable-abbrev="'+selectedAbbrev+'" data-column-name="'+selectedKey+'" id="query-fld-'+columnName+'" class= "query-flds query-additional-flds" placeholder="" type="text">'
            +'</div>');
        }
    }

    function buildCurrentFilters() {
        for (let columnIndex of Object.keys(addlFldsCurrent)) {
            var filterObj = addlFldsCurrent[columnIndex];
            var fldOptions = addlFldsOptions[columnIndex];
            columnsSelectedCount += 1;

            $('#additional-query-list'). append('<li>'
                +'<div class="container query-view-container">'
                    +'<div class="row query-row">'
                        +'<div class="col-md-1">'
                            +'<button class="btn btn-danger remove-addl-query"><i class="material-icons">remove</i></button>'
                        +'</div>'
                        +'<div class="col-md-3">'
                            +'<select class="query-column-name-select">'
                            +'</select>'
                        +'</div>'
                    +'</div>'
                +'</div>'
            +'</li>');

            // Automatically selects the last element because it is newly appended
            var listElement = $('#additional-query-list');
            var listItemIndex = listElement.children().length - 1;
            var selectElement = listElement.children().eq(listItemIndex).find('.query-column-name-select');
            var currentOperator = '';
            var currentValue = '';
            for (let operator of Object.keys(filterObj)) {
                currentOperator = operator;
                currentValue = filterObj[operator];
            }
            if (currentOperator == 'list') {
                selectElement.select2({
                    data: [{
                        'id': fldOptions['variable_abbrev'],
                        'text': fldOptions['label']
                    }],
                    disabled: true
                });
                // Auto select current filter
                selectElement.val(columnIndex).trigger('change');
                columnsSelected[columnIndex] = true;

                // disable selection options
                updateDisabledColumn();

                // disabled add button if all columns have been selected
                if (columnsSelectedCount == addlColumnsCount) {
                    $('#query-add-condition-btn').prop('disabled', true);
                }
                // Remove condition and values to the right if they exist
                if($(selectElement).parent().next().length) {
                    $(selectElement).parent().nextAll().remove();
                }

                var selectedColumnName = $(selectElement).select2('data')[0].id;
                var selectedKey = addlFldsOptions[selectedColumnName]['fieldName'];

                $(selectElement).parents().eq(1).append('<div class="col-md-3">'
                    +'<select name="Query['+selectedKey+'][operator]" class="query-condition-select" id="operator-'+selectedColumnName+'">'
                    +'</select>'
                +'</div>');
                var listElement = $('#additional-query-list');
                var listItemIndex = selectElement.parents().eq(3).index();
                var columnConditionSelectElement = listElement.children().eq(listItemIndex).find('.query-condition-select');

                columnConditionSelectElement.select2({
                    data: [{
                        'id': 'list',
                        'text': 'input list'
                    }],
                    disabled: true
                });
                // Remove input values to the right if they exist
                if ($(columnConditionSelectElement).parent().next().length) {
                    $(columnConditionSelectElement).parent().nextAll().remove();
                }

                var selectedOperator = $(columnConditionSelectElement).select2('data')[0].id;
                var selectedAbbrev = fldOptions['variable_abbrev'];
                var currentListCount = countCurrentList(currentValue);
                $(columnConditionSelectElement).parents().eq(1).append('<div class="col-md-5">'
                    +'<a href="#" class="list-count" data-abbrev="'+selectedAbbrev+'" style="font-size: 15px;">Total Items: <b><span id="list-count-'+selectedAbbrev+'">'+currentListCount+'</span></b> <i class="material-icons" style="font-size: 15px;">create</i></a>'
                    +'<textarea style="display:none" name="Query['+selectedColumnName.toLowerCase()+']" data-variable-abbrev="'+selectedAbbrev+'" data-column-name="'+selectedKey+'" id="query-fld-'+selectedColumnName+'" class= "query-flds query-additional-flds" value="" placeholder="" type="text"></textarea>'
                +'</div>');

                $('#query-fld-'+selectedColumnName).val(currentValue).trigger('change');
            } else {
                selectElement.select2({
                    data: selectOptionData,
                });
                
                var selectedNew = false;

                // Auto select current filter
                selectElement.val(columnIndex).trigger('change');
                columnsSelected[columnIndex] = true;

                // disable selection options
                updateDisabledColumn();

                // disabled add button if all columns have been selected
                if (columnsSelectedCount == addlColumnsCount) {
                    $('#query-add-condition-btn').prop('disabled', true);
                }

                // add operators options
                addOperatorsOptions({
                    'element': selectElement,
                    'currentFilterObj': filterObj
                });
            }
        }
        listCountAction();
    }

    function listCountAction() {
        $('.list-count').on('click', function(e) {
            e.preventDefault();
            var selectedAbbrev = $(this).attr('data-abbrev');
            var currentValue = $('#query-fld-'+selectedAbbrev).val();
            $('#query-input-list-modal').modal('show');
            $('#query-input-list-body').html('<div class="row">'
                +'<div class="col-md-7">'
                    +'<select id="query-column-name-list">'
                    +'</select><div class="row"></div>'
                    + '<div id="query-saved-lists"><div class="progress"><div class="indeterminate"></div></div></div>'
                +'</div>'
                +'<div class="col-md-5">'
                    + '<div id="loading-area"></div>'
                    + '<textarea class="form-control input-filter variable-filter" id="query-txt-area" rows="20" spellcheck="false" style="position: relative; background-color: white; line-height: 20px;"></textarea>'
                    + '<p style="color: red;">Note: Search is only limited for the first '+searchLimit+' items. The rest will be excluded from search.</p>'
                    +'<input type="text" style="display:none" id="current-list-count"/>'
                +'</div>'
                +'</div>');
            
                
            var fldOptions = addlFldsOptions[selectedAbbrev];
            var selectElement = $('#query-column-name-list');
            selectElement.select2({
                data: [{
                    'id': selectedAbbrev,
                    'text': fldOptions['label']
                }],
                width: '100%',
                disabled:true
            });
            selectElement.val(selectedAbbrev).trigger('change');
            var listType = fldOptions['list_type'];
            var listField = fldOptions['list_filter_field'];
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: renderSavedListsUrl,
                    async: false,
                    data: {
                        type: listType,
                        field: listField,
                        addlFldsOptions: addlFldsOptions
                    },
                    success: function(response) {
                        $('#query-saved-lists').html(response);
                    }
                });
            }, 1000);
            var inputValues = currentValue.split('|').join('\\n');
            inputValues = inputValues.replace(/equals /g, '');
            $('#query-txt-area').val(inputValues).trigger('change');
        });
    }
    function countCurrentList(value, splitter='|') {
        var valueArr = value.split(splitter);
        valueArr = valueArr.map(e => e.trim()).filter(e => e !== '' && e !==',');
        return valueArr.length;
    }

    /** End of function definition */

    /** Trigger events */
    $('#query-add-condition-btn').on('click', function(e) {
        e.preventDefault();
        columnsSelectedCount += 1;

        // add condition row
        $('#additional-query-list'). append('<li>'
            +'<div class="container query-view-container">'
                +'<div class="row query-row">'
                    +'<div class="col-md-1">'
                        +'<button class="btn btn-danger remove-addl-query"><i class="material-icons">remove</i></button>'
                    +'</div>'
                    +'<div class="col-md-3">'
                        +'<select class="query-column-name-select">'
                        +'</select>'
                    +'</div>'
                +'</div>'
            +'</div>'
        +'</li>');

        // Automatically selects the last element because it is newly appended
        var listElement = $('#additional-query-list');
        var listItemIndex = listElement.children().length - 1;
        var selectElement = listElement.children().eq(listItemIndex).find('.query-column-name-select');
        
        selectElement.select2({
            data: selectOptionData,
        });
        
        var selectedNew = false;
        // Auto select first non-disabled option
        for (let fldIndex of Object.keys(addlFlds)) {
            var columnObj = addlFlds[fldIndex];

            if (columnsSelected[columnObj.abbrev] == false) {
                selectElement.val(columnObj.abbrev).trigger('change');
                columnsSelected[columnObj.abbrev] = true;
                break;
            }
        }

        // disable selection options
        updateDisabledColumn();

        // disabled add button if all columns have been selected
        if (columnsSelectedCount == addlColumnsCount) {
            $('#query-add-condition-btn').prop('disabled', true);
        }

        // // add operators options
        addOperatorsOptions({
            'element': selectElement
        });
    });

    // filter by input list
    $('#query-input-list-btn').on('click', function(e){
        e.preventDefault();

        // $('#query-input-list-body').html('<div class="progress"><div class="indeterminate"></div></div>');
        $('#query-input-list-modal').modal('show');
        $('#query-input-list-body').html('<div class="row">'
            +'<div class="col-md-7">'
                +'<select id="query-column-name-list">'
                +'</select><div class="row"></div>'
                + '<div id="query-saved-lists"><div class="progress"><div class="indeterminate"></div></div></div>'
            +'</div>'
            +'<div class="col-md-5">'
                + '<div id="loading-area"></div>'
                + '<textarea class="form-control input-filter variable-filter" id="query-txt-area" rows="20" spellcheck="false" style="position: relative; background-color: white; line-height: 20px;"></textarea>'
                + '<p style="color: red;">Note: Search is only limited for the first '+searchLimit+' items. The rest will be excluded from search.</p>'
                +'<input type="text" style="display:none" id="current-list-count"/>'
            +'</div>'
            +'</div>');

        var selectElement = $('#query-column-name-list');
        selectElement.select2({
            data: inputListOptionData,
            width: '100%'
        });

        var fldOptions = addlFldsOptions[selectElement.val()];
        var listType = fldOptions['list_type'];
        var listField = fldOptions['list_filter_field'];
        setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: renderSavedListsUrl,
                async: false,
                data: {
                    type: listType,
                    field: listField,
                    addlFldsOptions: addlFldsOptions
                },
                success: function(response) {
                    $('#query-saved-lists').html(response);
                }
            });
        }, 1000);

        $('#query-column-name-list').on('change', function() {
            $('#query-txt-area').val("");
        });
    });

    /**
     * Confirm adding of input list as filter
     * Adds the  row for the query
     */
    $('#query-confirm-list-btn').on('click', function(e) {
        e.preventDefault();
        var value = $('#query-txt-area').val();
        var columnName = $('#query-column-name-list').val();
        var fldOptions = addlFldsOptions[columnName];
        
        if (value == '') {
            Materialize.toast("<i class='material-icons orange-text'>warning</i> Invalid input value", 3000);
            $('#query-txt-area').focus();
            return false;
        } else {
            // if text area already exists, change the value
            if ($('#query-fld-'+columnName).length > 0) {
                $('#query-fld-'+columnName).val(value).trigger('change');
                var currentCount = countCurrentList(value, '\\n');
                $('#list-count-'+columnName).html(currentCount);
            } else {
                // add condition row
                $('#additional-query-list').append('<li>'
                    +'<div class="container query-view-container">'
                        +'<div class="row query-row">'
                            +'<div class="col-md-1">'
                                +'<button class="btn btn-danger remove-addl-query"><i class="material-icons">remove</i></button>'
                            +'</div>'
                            +'<div class="col-md-3">'
                                +'<select class="query-column-name-select">'
                                +'</select>'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</li>');

                // Automatically selects the last element because it is newly appended
                var listElement = $('#additional-query-list');
                var listItemIndex = listElement.children().length - 1;
                var selectElement = listElement.children().eq(listItemIndex).find('.query-column-name-select');
                selectElement.select2({
                    data: [{
                        'id': fldOptions['variable_abbrev'],
                        'text': fldOptions['label']
                    }],
                    disabled: true
                });
                
                if (columnsSelected[fldOptions['variable_abbrev']] == false) {
                    columnsSelected[fldOptions['variable_abbrev']] = true;
                }
                updateDisabledColumn();
                // disabled add button if all columns have been selected
                if (columnsSelectedCount == addlColumnsCount) {
                    $('#query-add-condition-btn').prop('disabled', true);
                }

                // Remove condition and values to the right if they exist
                if($(selectElement).parent().next().length) {
                    $(selectElement).parent().nextAll().remove();
                }

                var selectedColumnName = $(selectElement).select2('data')[0].id;
                var selectedKey = addlFldsOptions[selectedColumnName]['fieldName'];

                $(selectElement).parents().eq(1).append('<div class="col-md-3">'
                    +'<select name="Query['+selectedKey+'][operator]" class="query-condition-select" id="operator-'+selectedColumnName+'">'
                    +'</select>'
                +'</div>');
                var listElement = $('#additional-query-list');
                var listItemIndex = selectElement.parents().eq(3).index();
                var columnConditionSelectElement = listElement.children().eq(listItemIndex).find('.query-condition-select');

                columnConditionSelectElement.select2({
                    data: [{
                        'id': 'list',
                        'text': 'input list'
                    }],
                    disabled: true
                });
                // Remove input values to the right if they exist
                if ($(columnConditionSelectElement).parent().next().length) {
                    $(columnConditionSelectElement).parent().nextAll().remove();
                }

                var selectedOperator = $(columnConditionSelectElement).select2('data')[0].id;
                var selectedAbbrev = fldOptions['variable_abbrev'];
                var value = value.replace(/,/g, '\\n');
                var currentListCount = countCurrentList(value, '\\n');
                $(columnConditionSelectElement).parents().eq(1).append('<div class="col-md-5">'
                    +'<a href="#" class="list-count" data-abbrev="'+selectedAbbrev+'" style="font-size: 15px;">Total Items: <b><span id="list-count-'+selectedAbbrev+'">'+currentListCount+'</span></b> <i class="material-icons" style="font-size: 15px;">create</i></a>'
                    +'<textarea style="display:none" name="Query['+selectedColumnName.toLowerCase()+']" data-variable-abbrev="'+selectedAbbrev+'" data-column-name="'+selectedKey+'" id="query-fld-'+selectedColumnName+'" class= "query-flds query-additional-flds" value="" placeholder="" type="text"></textarea>'
                +'</div>');

                $('#query-fld-'+selectedColumnName).val(value).trigger('change');
                
                listCountAction();
            }
        }
        $('#query-input-list-modal').modal('hide');
    });

    $('#additional-query-list').on('select2:selecting', '.query-column-name-select', function(e) {
        var currentlySelectedColumn = $(this).select2('data')[0].id;
        columnsSelected[currentlySelectedColumn] =  false;
        updateDisabledColumn();
    });

    $('#additional-query-list').on('select2:open', '.query-column-name-select', function(e) {
        var currentlySelectedColumn = $(this).select2('data')[0].id;
        $(this).children('[value="'+currentlySelectedColumn+'"]').prop('disabled',false);
    });

    $('#additional-query-list').on('select2:select', '.query-column-name-select', function(e) {
        var currentlySelectedColumn = $(this).select2('data')[0].id;
        columnsSelected[currentlySelectedColumn] = true;
        addOperatorsOptions({'element':$(this)});
        updateDisabledColumn();
    });

    $('#additional-query-list').on('select2:select', '.query-condition-select', function(e) {
        var currentNameElement = $(this).parent().prev().find('.query-column-name-select');
        var columnName = currentNameElement.select2('data')[0].id;
        var currentOperator = $(this).select2('data')[0].id;

        addInputValueField({
            'element': $(this),
            'columnName': columnName
        });
    });
    $('#additional-query-list').keypress(function(e){
        if (e.which == 13){
            e.preventDefault();
        }
    });

    $('#additional-query-list').on('click', '.remove-addl-query', function(e) {
        e.preventDefault();
        if (e.which == 13) {
            return false;
        }
        columnsSelectedCount -= 1;
        var columnName = $(this).parent().next().find('select').select2('data')[0].id;
        
        $('#query-add-condition-btn').prop('disabled', false);
        columnsSelected[columnName] = false;
        updateDisabledColumn();
        $($(this).parents().eq(3)).remove();
    });

    $('#query-clear-all-btn').on('click', function(e) {
        e.preventDefault();
        $('#query-add-condition-btn').prop('disabled', false);
        columnsSelectedCount = 0;
        $('#additional-query-list').empty();
        buildSelectOptionData();
        $('#query-input-list-btn').removeAttr("disabled");

        if (clearSelectionCb == "true") {
            if(typeof $('#'+gridId).b4rCheckboxColumn === "function") $('#'+gridId).b4rCheckboxColumn("clearSelection");
        }

        $.ajax({
            url: findUrl,
            data: {'reset': true},
            type: 'POST',
            success: function(response) {
                $.pjax.reload({
                    url: findUrl,
                    container: '#'+gridId+'-pjax', 
                    replace:true
                });
                $.each($('.query-basic-flds'), function(){
                    $(this).val('').trigger('change');
                });
            }
        });
    });

    $(document).on('click', '#query-find-btn', function(e) {
        e.preventDefault();
        var data = {};
        var invalid = false;
        var germplasmNameInputIsInvalid = false;
        $(".find-loading-progress").removeClass("hidden");
        localStorage.setItem('exemptedFromQuerySearchValues', '');
        // validate fields & build query data
        $.each($('.query-flds'), function(i,v) {
            var currentFld = $(this);
            var operatorFldId = '#operator-'+currentFld.attr('data-variable-abbrev');
            var selectedOperator = 'equals';
            var columnName = currentFld.attr('data-column-name');
            var selectedColumnDataType = addlFldsDataType[currentFld.attr('data-variable-abbrev')];

            // check if operator is available. if not, default is equals.
            if ($(operatorFldId).length > 0) {
                selectedOperator = $(operatorFldId).val();
            }

            if (selectedOperator == 'is not null' || selectedOperator == 'is null') {
                data[columnName] = {};
                data[columnName][selectedOperator] = "";
            } else if (selectedOperator == 'list') {
                var fldValue = currentFld.val();
                var inputListValue = fldValue.split('\\n');
                var exemptedValues = inputListValue.slice(searchLimit);
                var showExempted = false;
                if(exemptedValues.length > 1) showExempted = true;
                inputListValue = inputListValue.slice(0,searchLimit);
                inputListValue = inputListValue.join('\\n');
                exemptedValues = exemptedValues.join('\\n');
                localStorage.setItem('showExemptedFromQuerySearch', showExempted);
                var currentExempted = localStorage.getItem('exemptedFromQuerySearchValues');
                if(currentExempted.trim().length > 0) currentExempted = currentExempted + '\\n';
                localStorage.setItem('exemptedFromQuerySearchValues', currentExempted + exemptedValues);
                var inputValues = inputListValue.replace(/,/g, '\\n');
                inputValues = 'equals ' + inputValues.replace(/\\n/g, '|equals ');

                if (inputValues !== '' && inputValues !== null){
                    data[columnName] = {};
                    data[columnName][selectedOperator] = inputValues;
                }
            }
            else {
                var fldValue = currentFld.val();
                if (Array.isArray(fldValue)) {
                    var tempValue = '';
                    for (val of fldValue) {
                        if (tempValue !== '') {
                            tempValue += '|';
                        }
                        tempValue += val;
                    }
                    fldValue = tempValue;
                } else {
                    if (fldValue !== '' && fldValue !== null) {
                        if (selectedColumnDataType == 'timestamp'){}
                        else if (selectedColumnDataType == 'number') {
                            if (selectedOperator == 'range') {
                                var fldValueSplit = fldValue.split('-');
                                var tempVal = '';
                                fldValueSplit = fldValueSplit.map(e => e.trim()).filter(e => e !== '' && e !==',');
                                if (fldValueSplit.length > 2) {
                                    Materialize.toast("<i class='material-icons orange-text'>warning</i> Invalid input for range", 3000);
                                    invalid = true;
                                }
                                if(isNaN(fldValueSplit[0]) || isNaN(fldValueSplit[1])) {    // Both numbers should be valid
                                    Materialize.toast("<i class='material-icons orange-text'>warning</i> Both values should be a valid number for each of the "+columnName+" range input.", 3000);
                                    invalid = true;
                                } else {
                                    var newRange1 = Number(fldValueSplit[0]);
                                    var newRange2 = Number(fldValueSplit[1]);
                                    if(newRange1 >= newRange2) {    // First value should only be less than the second value
                                        Materialize.toast("<i class='material-icons orange-text'>warning</i> The first value should be less than the second value for each of the "+columnName+" range input.", 3000);
                                        invalid = true;
                                    } else {
                                        fldValue = newRange1 + "|" + newRange2;
                                    }
                                }
                            } else {
                                var fldValueSplit = fldValue.split(',');
                                var tempVal = '';
                                fldValueSplit = fldValueSplit.map(e => e.trim()).filter(e => e !== '' && e !==',');
                                for (val of fldValueSplit) {
                                    if (isNaN(val)) {
                                        Materialize.toast("<i class='material-icons orange-text'>warning</i> Only numbers are allowed for "+columnName+" field.", 3000);
                                        invalid = true;
                                        break;
                                    }
                                    if (tempVal != '') {
                                        tempVal += '|';
                                    }
                                    tempVal += val;
                                }
                                fldValue = tempVal;
                            }
                        }
                        else {
                            var fldValueSplit = fldValue.split(',');
                            var tempVal = '';
                            fldValueSplit = fldValueSplit.map(e => e.trim()).filter(e => e !== '' && e !==',');
                            for (val of fldValueSplit) {
                                if (val.replaceAll('%','').trim() == '') continue;

                                // Set minimum 3 characters in search for germplasm and trait browsers
                                if(tool == 'germplasm_browser' || tool == 'trait_browser') {
                                    // Check if there are at least 3 characters (excluding the % and _ wildcards) for each input
                                    if(val.replaceAll('%','').replaceAll('_','').length < searchCharMin) {
                                        germplasmNameInputIsInvalid = true
                                    }
                                }

                                if (tempVal != '') {
                                    tempVal += '|';
                                }

                                if (columnName == 'otherNames') {
                                    val = '%' + val + '%';
                                }
                                tempVal += val;
                            }
                            fldValue = tempVal;
                        }
                    }
                }

                if (fldValue !== '' && fldValue !== null){
                    data[columnName] = {};
                    data[columnName][selectedOperator] = fldValue;
                }
            }
        });

        if (Object.keys(data).length == 0 || typeof data === "undefined") {
            Materialize.toast("<i class='material-icons orange-text'>warning</i> Please input at least one query parameter", 3000);
            invalid = true;
            
            $(".find-loading-progress").addClass("hidden");
            return false;
        }

        if (germplasmNameInputIsInvalid) {
            Materialize.toast("<i class='material-icons orange-text'>warning</i> A minimum of "+ searchCharMin +" characters (excluding wildcards) are required for all text inputs", 3000);
            invalid = true;

            $(".find-loading-progress").addClass("hidden");
            return false;
        }

        if (!invalid) {
            $.ajax({
                url: findUrl,
                data: {'Query': data},
                type: 'POST',
                success: function(response) {
                    $.pjax.reload({
                        // url: url,
                        container: '#'+gridId+'-pjax', 
                        // replace:true
                    });
                    $(".find-loading-progress").addClass("hidden");
                    $('.collapsible').collapsible('close', 0);
                }
            });
        }
    });
JS
);

$this->registerCss('
    .container-query {
        background-color: white;
        height: 250px;
        position: relative;
        overflow-y: scroll;
        margin: 5px 0;
    }
    .query-column-name-select, .query-column-name-list{
        display: block;
    }
    .remove-addl-query {
        padding: 0 10px;
    }
    .query-view-container {
        padding: 5px 5px !important;
        margin: 5px 0;
    }
    .query-row {
        margin-bottom: 5px;
    }
    .query-row > .col {
        padding: 0 5px;
    }
    .query-row > .col > .select2 {
        margin: 3px 0;
    }
    .query-row > .col > .advanced-filter-column-value-input {
        margin: -8px 0;
    }
    .query-condition-select {
        display: block;
    }
    .collapsible-query-header {
        cursor: pointer !important;
    }
    .modal-dialog.modal-xl{
        width: 90% !important;
    }
');
?>