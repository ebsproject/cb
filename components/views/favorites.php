<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Render favorites widget
 */
use yii\helpers\Html;
use yii\helpers\Url;

$url = Url::to(['/dashboard/default/markunmarkfavorites']);
$id = 'favorite-a-tool-icon-'.$module.$controller.$action;
echo '&nbsp;'.Html::a('<i class="material-icons" style="font-size:80%;'.$iconStyle.'">star</i>', '#!', [
	'id'=>$id,
    'class'=>$class . ' favorite-a-tool-icon',
    'title'=>$title,
    'style'=>$style,
    'data-appid'=> $appId,
    'data-classname' => $id,
    'data-infavorites'=>$inFavorites
]);

$this->registerJs(<<<JS
	//add or remove to favorites

	$(document).on('click', '.favorite-a-tool-icon', function(){

		var obj = $(this);
		var app_id = obj.data('appid');
		var el = document.getElementById(obj[0]['id']);

		$.ajax({
			url: '$url',
			type: 'POST',
			async: true,
			data: {
				app_id: app_id
			},
			success: function(data) {
				if(data){ //unmark to favorite
					el.setAttribute("style","opacity:0.5;color:#fff;");
					el.setAttribute("title","Mark as favorite");
				}else{ //mark as favorite
					el.setAttribute('style','color:#f9a825 !important;opacity:1;');
					el.setAttribute('title','Unmark as favorite');
				}     	
			},
			error: function(){
			}
		});
	});
JS
);

?>