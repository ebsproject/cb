<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 namespace app\components;

 use Yii;
 use yii\base\Widget;
 use yii\helpers\ArrayHelper;
 use kartik\widgets\Select2;
 use kartik\date\DatePicker;

 use app\models\Config;
 use app\models\CropProgram;
 use app\models\Person;
 use app\models\Program;
 use app\models\Scale;
 use app\models\ScaleValue;
 use app\interfaces\models\IVariable;
 use app\models\User;
 use yii\helpers\Html;
 


 /**
  * This is the generic form of input forms
  */
 class GenericFormWidget extends Widget{
   public $variableAbbrevsArr = [];
   public $options = [];
   public $processType = '';

    public function __construct (
        public Config $configModel,
        public CropProgram $cropProgram,
        public Program $program,
        public IVariable $variable,
        public Person $person,
        $config = []
    ) { parent::__construct($config); }

  /**
   * Initialize the widget
   */
  public function init(){
    parent::init();
  }

  /**
   * Render the UI
   */
  public function run(){
     $generatedFieldsHolder = $this->generateFields();

     extract($generatedFieldsHolder);

     $formType = isset($formType) ? $formType : 'tab-form';
     $columnClass = isset($columnClass) ? $columnClass : null;

     return $this->render('generic-form',[
       'generatedFields' => $requiredFields,
       'formType' => $formType,
       'defaultFields' => $defaultFields,
       'hiddenDefaultFields' => $hiddenDefaultFields,
       'targetStep' => $targetStep,
       'columnClass' => $columnClass
     ]);
  }

  /**
   * Generate the fields
   */
  public function generateFields(){
    $isViewOnly = '';
    $previewOnly = '';
    $configTag = '';
    extract($this->options);
    $variableFields = [];
    $column = '';
    $rawValue = '';
    $errorVariables = [];
    $formType = isset($formType) ? $formType : 'tab-form';
    $defaultFields = [];
    $hiddenDefaultFields = [];
    //Target tab or step in the process i.e. protocol
    $targetStep = isset($targetStep) && !empty($targetStep) ? $targetStep : null;
    // If with custom column class
    $columnClass = isset($columnClass) && !empty($columnClass) ? $columnClass : null;
        
    // When source of variable abbrev is from the configuration
    if(empty($variableAbbrevsArr)){ 
        $variableAbbrevsArr = $config;
    }

    $userModel = new User();
    $userId = $userModel->getUserId();  

    // Default parameters
    $programId = $program ?? Yii::$app->userprogram->get('id');
    $rowId = isset($rowId) ? "-$rowId" : '';
    foreach($variableAbbrevsArr as $variable){
        $abbrev = isset($variable['variable_abbrev']) ? $variable['variable_abbrev'] : $variable;
        $varRecord = $this->variable->getVariableByAbbrev(strtoupper($abbrev));
        $variableArr = [];
        $additionalFilter = '';
        $tempVal2 = '';
        $hiddenClass = '';
        $defaultPattern = false;
        $shouldLoadDefaultValue = true;

        if (isset($hasMultipleSelectedOccurrences) && $hasMultipleSelectedOccurrences) $shouldLoadDefaultValue = false;
        
        $variableArr['id'] = $varRecord['variableDbId'] ?? '';
        $variableArr['label'] = ucwords(strtolower($variable['field_label'] ?? $varRecord['name'] ?? ''));
        $variableArr['target_model'] = $varRecord['targetModel'] ?? '';

        //get the scale of the variable
        if(!empty($varRecord['scaleDbId'])){
            $scaleModel =Scale::getVariableScale($varRecord['variableDbId'] ?? '');
            $scaleUnit = !empty($scaleModel['unit']) ? $scaleModel['unit'] : '';
        }

        $configUnit = (isset($variable['unit']) && !empty($variable['unit'])) ? '('.$variable['unit'].')' : '';
        $unit = !empty($scaleUnit) ? '('.$scaleUnit.')' : $configUnit;

        if(isset($configTag) && $configTag == 'plotTypeFields' && !empty($configUnit)){
            $unit = $configUnit;
        }
      
        $reqStr = isset($variable['required']) ? ' <span class="required">*</span>':'';
        $header = isset($variable['display_name']) && !empty($variable['display_name']) ? $variable['display_name'] : $variableArr['label'];
        $labelStr = ucwords(strtolower($header)).' '.$unit.$reqStr;

        $fieldDescription = !empty($varRecord['description']) ? $varRecord['description'] : $variableArr['label'];
      
        $allowedValues = false;
        if(isset($variable['allowed_values']) && count($variable['allowed_values']) > 0){
            $allowedValues = true;
            foreach($variable['allowed_values'] as $allowedVal){
                if(empty($additionalFilter)){
                    $additionalFilter = "equals ".$allowedVal;
                }else{
                    $additionalFilter = $additionalFilter . "|". "equals ".$allowedVal;
                }
            }
        }
        
        //disabled for update
        $disableLabel = '';
        $notAllowed = '';
        $isViewOnly = isset($isViewOnly) ? $isViewOnly : null; 
        $previewOnly = isset($previewOnly) ? $previewOnly : null;
        $shouldUpdateOccurrenceProtocols = isset($shouldUpdateOccurrenceProtocols) ? $shouldUpdateOccurrenceProtocols : null;
        $mainApiResourceFilter = isset($mainApiResourceFilter) ? $mainApiResourceFilter : []; 

        if((isset($variable['disabled']) && $variable['disabled']) || $isViewOnly === '1' || $previewOnly === '1'){
            $disabled="readonly";
            $disableLabel = "['disabled']";
            $notAllowed = " not-allowed";
            $disableStyle = 'disabled';
        }else{
            $disabled = false;
            $disableStyle = '';
        }

        $allowNewVal = isset($variable['allow_new_val']) ? $variable['allow_new_val'] : false;

        $withOptions = false;

        $targetColumn = isset($variable['target_column']) ? $variable['target_column'] : '';
        $apiResourceMethod = isset($variable['api_resource_method']) ? $variable['api_resource_method'] : '';
        $apiResourceEndpoint = isset($variable['api_resource_endpoint']) ? $variable['api_resource_endpoint'] : '';
        $apiResourceFilter = isset($variable['api_resource_filter']) ? $variable['api_resource_filter'] : '';
        $apiResourceSort = isset($variable['api_resource_sort']) ? $variable['api_resource_sort'] : '';

        $targetValue = isset($variable['target_value']) && !empty($variable['target_value']) ? $variable['target_value'] : 'abbrev';
        // if variable is FIELD, set target value to geospatialObjectName instead of scaleName
        // (note that this is a temp solution since there is a need to modify all the configurations with FIELD variable)
        $targetValue = ($abbrev == 'FIELD') ? 'geospatialObjectName' : $targetValue;
        $secondaryTargetValue = isset($variable['secondary_target_value']) && !empty($variable['secondary_target_value']) ? $variable['secondary_target_value'] : '';
        $secondaryTargetColumn = isset($variable['secondary_target_column']) ? $variable['secondary_target_column'] : '';
        $variableType = isset($variable['variable_type']) ? $variable['variable_type'] : ($varRecord['type'] ?? '');
        
        if($formType == 'MODAL'){
            $varId = 'modal'.'-'.($varRecord['abbrev'] ?? '').'-'.$variableType.'-'.($varRecord['variableDbId'] ?? '').$rowId;
        }else{
            $varId = ($varRecord['abbrev'] ?? '').'-'.$variableType.'-'.($varRecord['variableDbId'] ?? '').$rowId;
        }

        $label = Html::label($labelStr, ($varRecord['abbrev'] ?? ''), ['for'=>$varId,'data-toggle'=>$fieldDescription, 'class'=> 'control-label','data-label'=>ucwords(strtolower($variableArr['label']))]);

        if(empty($targetColumn)){ 
            $column = ucwords(strtolower($abbrev),'_');
            $column = str_replace('_','',$column);
            $column = lcfirst($column); 
        }else{
            $column = $targetColumn;
        }
        
        $variableArr['source_value'] = $variableType;

        $mainModel = isset($mainModel) ? $mainModel : '';

        $name = ''.$mainModel.''.'['.($varRecord['abbrev'] ?? '').']['.$variableArr['source_value'].']'.$disableLabel.'['.($variable['api_field'] ?? $varRecord['variableDbId'] ?? '').']';
        
        $requiredStr = isset($variable['required']) ? ' required-field':'';
        $numericRequiredClass = isset($variable['required']) ? 'numeric-req-input':'';
        $rowClass = isset($rowClass) && !empty($rowClass) ? $rowClass : 'input-validate';
        $data = [];
        // If no api_resource_method or api_resource_endpoint is specified then the $column is column in the main target table
        if(empty($apiResourceMethod) || empty($apiResourceEndpoint)){

            //Check if variable has scale values
            if(!$withOptions && isset($varRecord['scaleDbId']) && !empty($varRecord['scaleDbId'])){
                $scaleParam = null;
                if($allowedValues){
                    $additionalFilterArr = ["$targetValue" => "$additionalFilter"];
                    if(!empty($targetValue)){
                        $scaleParam = ArrayHelper::merge($scaleParam,$additionalFilterArr);
                    }
                }
                
                $scaleValues = ScaleValue::getVariableScaleValues(($varRecord['variableDbId'] ?? ''), $scaleParam);
                
                $data = [];
                foreach ($scaleValues as $key => $scaleVal) {
                  $data[$scaleVal['value']] = isset($scaleVal['displayName']) ? $scaleVal['displayName'] : $scaleVal['value'];
                }

                if(count($scaleValues) > 0){
                    $withOptions =true;
                }
            }
        }else{

          $withOptions = true;
          if(!empty($additionalFilter) && !empty($targetValue)){
              //This part will be used as additional filter for stages 
              if(empty($apiResourceFilter)){
                  $apiResourceFilter = ["$targetValue" => "$additionalFilter"];
              }else{
                  $additionalFilterArr = ["$targetValue" => "$additionalFilter"];
                  $apiResourceFilter = ArrayHelper::map($apiResourceFilter,$additionalFilterArr, $targetValue);
              }
          }
          // This is for the variables with select2 options
          if($apiResourceMethod == 'GET'){
             $path = $apiResourceEndpoint.'?'.$apiResourceSort;
          }else{
             $path = $apiResourceEndpoint;
          }

          if(isset($presetData) && isset($presetData[$abbrev]) && !empty($presetData[$abbrev])){
            $data = $presetData[$abbrev];
          } else {
            if(!empty($apiResourceMethod) && !empty($apiResourceEndpoint)){
              if(!empty($apiResourceFilter)){
                $result = Yii::$app->api->getResponse($apiResourceMethod, $path, json_encode($apiResourceFilter), '', true);
              }else{
                $apiResourceSort = !empty($apiResourceSort) ? '?'.$apiResourceSort : '';

                if ($column == 'contactPerson') {
                    // If the target column is Contact Person...
                    // ...retrieve person records from CS-CRM
                    $result = [ 
                        'status' => 200,
                        'body' => [ 'result' => [ 'data' => $this->person->getPersonContacts(), ], ],
                    ];
                } else {
                    $result = Yii::$app->api->getResponse($apiResourceMethod, $path, json_encode($apiResourceFilter), null, true);
                }
              }
            }
          
              if (isset($result['status']) && $result['status'] == 200 && isset($result['body']['result']['data'])){
                  $data = $result['body']['result']['data'];
              }
          }
          $dataTargetColumn = !empty($secondaryTargetColumn) ? $secondaryTargetColumn : $targetColumn;
          
          //if there's additional information in display name
          $appendDisplay = isset($variable['append_to_value']) ? $variable['append_to_value'] : null;
          $displayData = null;
          // if with secondary attribute to display in dropdown options; format is MAIN OPTION (SECONDARY OPTION)          
          if(isset($secondaryTargetValue) && !empty($secondaryTargetValue)){
            $secondaryData = [];
            foreach ($data as $v) {
              $mainOption = isset($v[$targetValue]) ? $v[$targetValue] : '';
              $secondaryOption = isset($v[$secondaryTargetValue]) ? ' (' . $v[$secondaryTargetValue] .')' : '';

              $secondaryData[$v[$dataTargetColumn]] = $mainOption . $secondaryOption;
            }
            $data = $secondaryData;
          } else {

            if($appendDisplay != null && isset($data[0][$appendDisplay])){

                $origData = ArrayHelper::map($data, $dataTargetColumn, $targetValue);
                $tempArray = [];
                foreach($data as $displayVal){
                    $tempArray[$displayVal[$dataTargetColumn]] = $displayVal[$targetValue]." ".$displayVal[$appendDisplay];
                }
                $data = $tempArray;
                
            } else {
                $data = ArrayHelper::map($data, $dataTargetColumn, $targetValue);
                $origData = $data;
            }
          }

          if($allowNewVal && isset($variable['default']) && !in_array($variable['default'], $data)){
              $data[$variable['default']] = $variable['default'];
          }
        }

        $resourceFilter = $mainApiResourceFilter ?? null;

        // Check if a record exists in the given main API resource
        if(isset($mainApiSource) && !empty($mainApiSource) && $mainApiSource == 'metadata'){
            $variableId = ($varRecord['variableDbId'] ?? '');

            if(isset($mainApiResourceFilter) && !empty($variableId)){
                if ($isViewOnly || $shouldUpdateOccurrenceProtocols) {
                    $mainApiResourceFilter['variableDbId'] = "$variableId";
                } else {
                    $mainApiResourceFilter = ['variableDbId' => "$variableId"];
                }
            }
        }

        $mainData = [];

        if(!empty($mainApiResourceMethod) && !empty($mainApiResourceEndpoint) && isset($mainApiResourceFilter)){
            $mainData = Yii::$app->api->getResponse($mainApiResourceMethod, $mainApiResourceEndpoint, json_encode($mainApiResourceFilter), null, false);
        }

        // if identification and main API source is set (e.g. experiments-search)
        if(!empty($mainApiResource) && $variableType == 'identification'){
            $mainData = Yii::$app->api->getResponse($mainApiResourceMethod, $mainApiResource, json_encode($resourceFilter), null, false);
        }

        // if metadata and data API source is set (e.g. experiments-data-search)
        if(!empty($dataApiResource) && $variableType == 'metadata'){
            $variableId = ($varRecord['variableDbId'] ?? '');
            $variableFilter = ["variableDbId" => "$variableId"];
            $filter = array_merge($resourceFilter, $variableFilter);
            
            $mainData = Yii::$app->api->getResponse($mainApiResourceMethod, $dataApiResource, json_encode($filter), null, false);
        }
        
        // For Single Update Occurrence-level Protocols
        // This prevents uneditable plot type fields from loading...
        // ...selected Occurrence's existing plot type field values
        if (
            isset($configTag) && 
            $configTag == 'plotTypeFields' &&
            $shouldUpdateOccurrenceProtocols &&
            !$hasMultipleSelectedOccurrences &&
            $variable['disabled']
        ) {
            $mainData = [];
        }

        $mainResult = [];
        // Check if it has a result, then retrieve menu data
        if (
            isset($mainData['status']) &&
            $mainData['status'] == 200 &&
            isset($mainData['body']['result']['data'][0]) &&
            $shouldLoadDefaultValue
        ){
            if($variableType == 'metadata' || $variableType == 'observation'){

                $mainResult = $mainData['body']['result']['data'][0]['data'][0] ?? $mainData['body']['result']['data'][0];

                // if format of api is {entity}-data-search
                if (empty($mainResult)) {
                    $mainResult = $mainData['body']['result']['data'][0];
                }
                $column = 'dataValue';
            }else{
                $mainResult = $mainData['body']['result']['data'][0];
            }
            //Default passed parameter
            if(($varRecord['abbrev'] ?? '') == 'EXPERIMENT_TYPE'){
                $variableArr['default'] = ucwords($mainResult[$column]);
            }else{
                if(!empty($mainResult[$column])){
                    $variableArr['default'] = $mainResult[$column];
                    
                    //Get the ids from the $data array
                    $data_key_values = array_keys($data);

                    //Check if the default or retrieved value exists in the array keys of data
                    if(!in_array($variableArr['default'],$data_key_values)){
                        $varTemp = $variableArr['default'];
                        $data = ArrayHelper::merge($data,["$varTemp"=>"$varTemp"]);
                    }
                }else{
                    $variableArr['default'] = $variable['default'] ?? '';
                }
            }

            if(($varRecord['abbrev'] ?? '') == 'STAGE'){
               
                $tempData = [];
                $tempValuesArr = array_values($origData);
                $keysArr = array_keys($data);

                $dataValuesArr = array_values($data);
                for($i=0; $i<count($variable['allowed_values']); $i++){

                    $index = array_search($variable['allowed_values'][$i], $tempValuesArr);
                    if($index !== false){
                        if($mainResult[$column] == $keysArr[$index]){
                            $variableArr['default'] = $i.'-'.$mainResult[$column];
                        }
                        $tempData[$i.'-'.$keysArr[$index]] = $data[$keysArr[$index]];
                    }
                }
                $data = $tempData;
            }

        } else {
          if (
            (!isset($variable['default']) || !$variable['default']) &&
            $variable['variable_abbrev'] === 'PLANTING_INSTRUCTIONS' ||
            $variable['variable_abbrev'] === 'POLLINATION_INSTRUCTIONS' ||
            $variable['variable_abbrev'] === 'HARVEST_INSTRUCTIONS'
          ) {
            $variable['default'] = '';
          }
    
          if(isset($variable['default']) && $variable['default'] === true) {
            $variable['default'] = 'true';
          }

          if(isset($variable['default']) && $variable['default'] === false) {
            $variable['default'] = 'false';
          }
          //Else get the default value defined in the config if there's such
          if(isset($variable['default'])){

              if(!empty($data)){
                  if(array_key_exists($variable['default'],$data)){
                      $variableArr['default'] = $variable['default'];
                  } else {
                      if(in_array($variable['default'],$data)){
                          $key = array_search($variable['default'],$data);
                          $variableArr['default'] = $key;
                      }
                  }
              }else{
                  $variableArr['default'] = $variable['default'];
              } 
          }

          $type = isset($type) ? $type : null;

          if(isset($varRecord['abbrev'])){
            //Default passed parameter 
            if(($varRecord['abbrev'] ?? '') == 'EXPERIMENT_TEMPLATE'){ 
                $variableArr['default'] = $templateId; 
            } 
            //Default passed parameter 
            if(($varRecord['abbrev'] ?? '') == 'EXPERIMENT_TYPE'){ 
                $variableArr['default'] = ucwords($type); 
            } 

            if(($varRecord['abbrev'] ?? '') == 'PROGRAM'){
                $variableArr['default'] = $programId;
            }
            
            if(($varRecord['abbrev'] ?? '') == 'EXPERIMENT_YEAR'){
                $variableArr['default'] = date('Y');
            }

            if(($varRecord['abbrev'] ?? '') == 'EXPERIMENT_STEWARD'){
                $variableArr['default'] = $userId;
            }

            if(($varRecord['abbrev'] ?? '') == 'CROP'){
                $program = $this->program->getProgram($programId);
                
                $cropProgramId = $program['cropProgramDbId'];

                $programCrop = $this->cropProgram->searchAll(['cropProgramDbId'=>"$cropProgramId"]);

                $variableArr['default'] = $programCrop['data'][0]['cropDbId'];
            }

            if(in_array(($varRecord['abbrev'] ?? ''),['EXPERIMENT_NAME','ENTRY_LIST_NAME'])){
                $patternConfig = [];
                $patternString = '';

                $configAbbrev = '';
                switch($varRecord['abbrev']){
                    case 'EXPERIMENT_NAME':
                        $configAbbrev = 'EXPERIMENT_NAME_CONFIG';
                        break;
                    case 'ENTRY_LIST_NAME':
                        $configAbbrev = 'CM_NAME_PATTERN_ENTRY_LIST_NAME';
                        break;
                }

                $patternConfig = $this->configModel->getConfigByAbbrev($configAbbrev);

                if(isset($patternConfig['pattern']) && !empty($patternConfig['pattern'])){
                  foreach($patternConfig['pattern'] as $pattern){
                      if($pattern['value'] != 'COUNTER'){
                          $patternString .= $pattern['value'];
                      } else {
                          $patternString .= str_repeat('X', $pattern['digits']);
                      }
                  }
                }
                $defaultPlaceholder = 'Auto-generated:'.$patternString;
                $defaultPattern = true;
            }
            
            if(($varRecord['abbrev'] ?? '') == 'STAGE'){
               
                $tempData = [];
                $tempValuesArrName = array_values($data);
                $tempValuesArr = array_values($origData);
                $keysArr = array_keys($data);

                $dataValuesArr = array_values($data);
                for($i=0; $i<count($variable['allowed_values']); $i++){
                    $index = array_search($variable['allowed_values'][$i], $tempValuesArr);
                    if($index !== false){
                        $tempData[$i.'-'.$keysArr[$index]] = $tempValuesArrName[$index];
                    }
                }
            
                $data = $tempData;
            }

            if(($varRecord['abbrev'] ?? '') == 'LOCATION_NAME'){
                $defaultPlaceholder = 'Auto-generated: SITE_CODE-YEAR-SEASON_CODE-XXX';
                $defaultPattern = true;
            }
          }
        }
  
        $helpBlock = '<p class="help-block help-block-error"></p>';

        if($withOptions){

            $allowClear = (isset($variable['required']) && strtolower($variable['required']) == 'required') ? false : true;
            $value = $rawValue = Select2::widget([
                'name' => $name,
                'id' => $varId,
                'data' => $data,
                'value' => (isset($variableArr['default']) && !empty($variableArr['default'])) ? $variableArr['default'] : null,
                'options' => [
                    'placeholder' => 'Select a value',
                    'tags' => true,
                    'disabled'=> $disabled,
                    'class'=>'editable-field select2-form-fields select2-'.$rowClass.$requiredStr,
                    'data-mem' => ($varRecord['abbrev'] ?? ''),
                    'target-column' => $targetColumn,
                ],
                'pluginOptions' => [
                    'allowClear' => $allowClear,
                    'tags' => $allowNewVal,
                ]
            ]);
            $hidden = '';
            $isIncludeForm = isset($variable['include_form']) ? $variable['include_form'] : false;

            if($disabled && !$isIncludeForm && $isViewOnly !== '1' && $previewOnly !== '1'){
                $hidden = Html::input('text',$name, !empty($variableArr['default']) ? $variableArr['default']:null, ['id'=>$varId.'-hidden','class'=>'select2-form-fields select2-'.$rowClass.'-hidden hidden']);
                $hiddenClass = 'hidden';
                $tempVal2 = isset($variableArr['default']) && isset($data[$variableArr['default']]) ? $data[$variableArr['default']] : '';
            }

            $value = '<div class="my-container">'.$value.'</div>';   
            $tempVal = '<div class="control-label row col-md-12 '.$hiddenClass.'" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$hidden.$helpBlock, ['class'=>'control-group field-'.$varId, 'title'=>$fieldDescription]).'</div>';
          
            $variableArr['value']= $tempVal;
            $variableArr['rawValue']= $rawValue;
        } 
        $dataType = $variable['data_type'] ?? $varRecord['dataType'] ?? '';
        if(!isset($variableArr['value'])){
            if(in_array($dataType, ['integer', 'smallint', 'numeric', 'bigint'])){
                $min = NULL;
                $max = NULL;
                if(isset($variable['min_value'])){
                    $min = $variable['min_value'];
                }
                if(isset($variable['max_value'])){
                    $max = $variable['max_value'];
                }
          
                $scale = Scale::getVariableScale(($varRecord['variableDbId'] ?? ''));
                
                if(empty($min)){
                    $min = (isset($scale['minValue']) && !empty($scale['minValue']))? $scale['minValue'] : 1;
                }
                if(empty($max)){
                    $max = (isset($scale['maxValue']) && !empty($scale['maxValue']))? $scale['maxValue'] : '';
                }
                
                if(($varRecord['abbrev'] ?? '') == 'YEAR'){
                    $variableArr['default'] = !empty($variableArr['default']) ? $variableArr['default']:date("Y");
                }
                
                $computed = isset($variable['computed']) ? $variable['computed'] : '';

                $value = $rawValue = Html::input('number', $name, $variableArr['default'], ['id'=>$varId,'class'=> $numericRequiredClass. ' editable-field numeric-'.$rowClass.' '.$requiredStr.' '.$computed.' '.$disableStyle,'readonly'=>$disabled, 'min'=>$min, 'max'=>$max, 'step'=>'any']);

                $value = '<div class="my-container">'.$value.'</div>';
                $tempVal = '<div class="control-label row col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId, 'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;
            }
            else if(in_array($dataType, ['float'])){
                $min = NULL;
                $max = NULL;
                if(isset($variable['min_value'])){
                    $min = $variable['min_value'];
                }
                if(isset($variable['max_value'])){
                    $max = $variable['max_value'];
                }
          
                $scale = Scale::getVariableScale(($varRecord['variableDbId'] ?? ''));
                
                if(empty($min)){
                    $min = (isset($scale['minValue']) && !empty($scale['minValue'])) ? $scale['minValue'] : 0;
                }
                if(empty($max)){
                    $max = (isset($scale['maxValue']) && !empty($scale['maxValue']))? $scale['maxValue'] : '';
                }
                
                $computed = isset($variable['computed']) ? $variable['computed'] : '';

                $value = $rawValue = Html::input('number', $name, $variableArr['default'], ['id'=>$varId,'min'=>$min, 'max'=>$max, 'step'=>'any','class'=>'editable-field float-'.$rowClass.' '.$requiredStr.' '.$computed.' '.$disableStyle,'readonly'=>$disabled, 'target-column' => $targetColumn]);
                $value = '<div class="my-container">'.$value.'</div>';
                $tempVal = '<div class="control-label row col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId, 'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;          
            } 
            else if(in_array($dataType, ['name', 'character varying', 'polygon'])){
              
                if($defaultPattern && (in_array(($varRecord['abbrev'] ?? ''), ['EXPERIMENT_NAME','ENTRY_LIST_NAME','LOCATION_NAME']))){
                    $disableStyle = 'disabled';
                    $toggle = '<div class="row switch" >
                        <em>Auto-generate name</em> &nbsp;&nbsp;
                        <label>
                            No
                            <input type="checkbox" id="generate-'.$varId.'" class="default-namegen-switch" name="DefaultName['.$varId.']." checked>
                            <span class="lever"></span>
                            Yes
                        </label>
                    </div>';
                    $valueTemp = $rawValue = Html::input('text', $name, null, ['id'=>$varId, 'placeholder'=>$defaultPlaceholder,'class'=>'editable-field text-'.$rowClass.' generated-pattern '.$requiredStr.' '.$disableStyle,'readonly'=>'readonly', 'target-column' => $targetColumn]);
                } else {
                    $toggle = '';
                    $valueTemp = $rawValue = Html::input('text', $name, $variableArr['default'] ?? '', ['id'=>$varId, 'class'=>'text-'.$rowClass.' '.$requiredStr.' '.$disableStyle,'readonly'=>$disabled, 'target-column' => $targetColumn]);
                }

                $value = '<div class="my-container'.$notAllowed.'">'.$valueTemp.$toggle.'</div>';
                $isIncludeForm = isset($variable['include_form']) ? $variable['include_form'] : false;
                if($disabled && !$isIncludeForm){
                    $hiddenClass = 'hidden';
                    $tempVal2 =  '<p style="color:gray;">'.$variableArr['default'].'</p>';
                }

                $tempVal = '<div class="control-label row col-md-12 '.$hiddenClass.'" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId, 'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;

            }
            else if(in_array($dataType, ['boolean'])){
                $arraySet = ['id'=>$varId,'value'=>'0', 'class'=>'editable-field boolean-'.$rowClass.' '.$requiredStr, 'target-column' => $targetColumn];
                
                if(!empty($variableArr['default'])){
                    if(in_array($variableArr['default'], [1, 'true', TRUE])){
                        $arraySet = ['checked'=>'checked', 'value'=>'1','id'=>$varId, 'class'=>'boolean-'.$rowClass.' '.$requiredStr.' '.$disabled, 'target-column' => $targetColumn];
                    } 
                }

                $value = $rawValue = Html::input('checkbox', $name, $variableArr['default'],$arraySet );
                $span = Html::tag('span', '', ['class'=>'lever ']);
                $labelName = Html::label($labelStr, ($varRecord['abbrev'] ?? ''),['for'=>$varId, 'data-label'=>ucwords(strtolower($variableArr['label'])),'class'=>'cbx-label', 'style'=>'font-weight: bold;margin-left:-2px;']);
                $label2 = Html::tag('label',$value.$span);
              
                $label = $labelName;
                $value = $label2;
                $tempVal = '<div class="control-label row col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group row switch ', 'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;          

            } else if(in_array($dataType, ['time', 'timestamp'])){
                $value = $rawValue = Html::input('text', $name, $variableArr['default'], ['id'=>$varId, 'class'=>'editable-field time-'.$rowClass.' timepicker '.$requiredStr.' '.$disabled, 'target-column' => $targetColumn]);
                $label = $label;
                $value = '<div class="my-container">'.$value.'</div>';
                $tempVal = '<div class="control-label col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId, 'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;          

            } else if(in_array($dataType, ['date'])){

                $value = $rawValue = DatePicker::widget([
                  'name' => $name,
                  'id' => $varId,                
                  'value' => $variableArr['default'],
                  'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                    'clearBtn' => true,
                    'startDate' => '1970-01-01',
                  ],
                  'pluginEvents' => [
                    'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                  ],
                  'options' => [
                    'autocomplete' => 'off',
                    'target-column' => $targetColumn,
                    'class' => 'time-'.$rowClass.' datepicker '.$requiredStr.' editable-field '.$disabled,
                  ]
                ]);

                $label = $label;
                $value = '<div class="my-container">'.$value.'</div>';
                $tempVal = '<div class="control-label row col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId,'title'=>$fieldDescription]).'</div>';
                
                $variableArr['value']= $tempVal;
                $variableArr['rawValue']= $rawValue;          
                
            } else if(in_array($dataType, ['json', 'text'])){

                $defaultValue = isset($variableArr['default']) ? $variableArr['default'] : '';
                if($disabled == 'readonly'){
                    $value = $rawValue = Html::textarea($name, $defaultValue, ['id'=>$varId, 'rows'=>'2','class'=>'form-control editable-field text-'.$rowClass.' '.$requiredStr.' '.$disableStyle,'readonly'=>$disabled, 'target-column' => $targetColumn, 'style' => 'min-width: 150px', 'type'=>'textarea']);
                }else{
                    $value = $rawValue = Html::textarea($name, $defaultValue, ['id'=>$varId, 'rows'=>'2','class'=>'form-control editable-field text-'.$rowClass.' '.$requiredStr.' '. 'blue-border-focus', 'target-column' => $targetColumn, 'style' => 'min-width: 150px', 'type'=>'textarea']);
                }
                
                $label = $label;
                $value = '<div class="my-container'.$notAllowed.'">'.$value.'</div>';
                $tempVal = '<div class="control-label row  col-md-12" style="margin-bottom:10px;padding-left:5px;">'.Html::tag('div',$label.$value.$helpBlock, ['class'=>'control-group field-'.$varId ,'title'=>$fieldDescription]).'</div>';
                $variableArr['value']= $tempVal;          
                $variableArr['rawValue']= $rawValue;          
          }
        }
        //Separate those with default values and disabled
        if($disabled && $targetStep != 'protocol'){

            //To check if to include in the input form or in the default fields
            if(!isset($variable['include_form']) || !$variable['include_form']){ 
                $variableArr['isDisabled'] = true;
                $variableArr['value']= $tempVal2;  
                $defaultFields[] = $variableArr;

                $variableArr['value']= $tempVal; 
                $hiddenDefaultFields[] = $variableArr;
            }else{
                $variableFields[] = $variableArr; 
                $variableArr['value']= $tempVal; 
            }
        }else{
            $variableArr['value']= $tempVal;  
            $variableFields[] = $variableArr;
        }
      } // End of foreach
    
      //Check the length of defaultFields
      if(sizeof($defaultFields) == 1 ){
          $variableFields = ArrayHelper::merge($defaultFields,$variableFields);
      }
      
      if(count($errorVariables) > 0){
            return [
                "message"=> "Missing variables: <b>".implode(',  ',$errorVariables)."</b>. Please contact system administrator to create the variable/s.",
                "requiredFields"=> [],
                "defaultFields" => [],
                "hiddenDefaultFields" => [],
                "targetStep" => $targetStep,
                "columnClass" => $columnClass
            ];
      } else{
            return [
                "message" => "success",
                "requiredFields" => $variableFields,
                "formType" => $formType,
                "defaultFields" => $defaultFields,
                "hiddenDefaultFields" => $hiddenDefaultFields,
                "targetStep" => $targetStep,
                "columnClass" => $columnClass
            ];
      }
   }
 } 