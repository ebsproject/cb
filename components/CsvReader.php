<?php
/* 
* This file is part of EBS-Core Breeding. 
* 
* EBS-Core Breeding is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* 
* EBS-Core Breeding is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
* GNU General Public License for more details. 
* 
* You should have received a copy of the GNU General Public License 
* along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

namespace app\components;

use LogicException;

/**
 * Class CsvReader
 *
 * Reading CSV file with SplFileObject.
 * Define a header or manage the header line from CSV file.
 * Returns rows in arrays with header values as array keys.
 *
 * If a header is defined then for each row we will check that
 * the number of columns corresponds to the number of header elements.
 * If this does not match then the line is considered invalid.
 * An invalid line in a CSV file will be returned as an array
 * comprising a single null field unless using skipInvalid.
 *
 * Use the safeMode so that the reader still returns invalid lines.
 */
class CsvReader implements \Iterator, \Countable
{
    /**
     * @var array;
     */
    private $data;
    /**
     * @var bool
     */
    private $hasHeader = false;
    /**
     * @var array
     */
    private $header = [];
    /**
     * @var int
     */
    private $countHeader = 0;
    /**
     * @var string
     */
    private $filename;
    /**
     * @var \SplFileObject
     */
    private $file;
    /**
     * @var int
     */
    private $count = -1;
    /**
     * @var array
     */
    private $invalids = [];
    /**
     * @var bool
     */
    private $skipInvalid = false;
    /**
     * @var bool
     */
    private $safeMode = false;
    /**
     * CsvReader constructor.
     *
     * Example Usage:
     * 
     *     $csv=new \app\components\CsvReader($uploadPath . '/' . $fileName);
     *     $csv->setHasHeader(true);
     *     foreach ($csv as $line) {
     *         ...
     *     }           
     *
     * 
     * @param $filename Name of the file
     * @param string $enclosure Enclosure of values
     * @param string $escape Escape of values
     */
    public function __construct($filename, $enclosure = '"', $escape = '\\')
    {
        $this->filename = $filename;
        $this->file = new \SplFileObject($this->filename, 'r');
        $delimiter = $this->getDelimiter();

        $this->file->setFlags(
            \SplFileObject::READ_CSV
                | \SplFileObject::READ_AHEAD
                | \SplFileObject::SKIP_EMPTY
                | \SplFileObject::DROP_NEW_LINE
        );

        $this->file->setCsvControl($delimiter, $enclosure, $escape);
    }
    /**
     * Get the delimiter using the first 2 lines of the file
     * @param  integer $checkLines Number of lines to check, default=2
     * @return string delimiter
     */
    public function getDelimiter($checkLines = 2)
    {

        $delimiters = array(
            ',',
            "\t",
            ';',
            '|',
            ':'
        );
        $results = array();
        $i = 0;
        while ($this->file->valid() && $i <= $checkLines) {
            $line = $this->file->fgets();

            foreach ($delimiters as $delimiter) {
                $regExp = '/[' . $delimiter . ']/';
                $fields = preg_split($regExp, $line);
                if (count($fields) > 1) {
                    if (!empty($results[$delimiter])) {
                        $results[$delimiter]++;
                    } else {
                        $results[$delimiter] = 1;
                    }
                }
            }
            $i++;
        }

        if (empty($results)) {
            throw new LogicException('No delimiter found in file.');
        }

        $results = array_keys($results, max($results));
        return $results[0];
    }
    /**
     * {@inheritdoc}
     */
    public function __destruct()
    {
        unset($this->file);
    }
    /**
     * Checks column headers
     * @return object header
     */
    private function computeHeader()
    {
        if ($this->countHeader === 0 && $this->hasHeader === true) {
            $currentPosition = $this->file->key();
            $this->file->seek(0);
            $this->header = $this->file->current();
            $this->file->seek($currentPosition);
        }
        
        if ($this->countHeader === 0 && count($this->header) > 0) {            
            $header = array_unique(array_filter($this->header, fn($value) => is_string($value) && $value !== ''));
            $this->header = array_filter($this->header);
            if ($this->header !== $header) {
                $headersNotUnique = $this->array_not_unique($this->header);

                if (empty($headersNotUnique)) {
                    throw new \Exception(\Yii::t('app', 'One or more column names of the header are empty.'));
                } else {
                    throw new \Exception(\Yii::t('app', 'The header must be with unique string values. Remove duplicates: ') . '<b style="overflow-wrap: break-word">' . implode( ', ' , array_unique($headersNotUnique)) . '</b>');
                }
            }
            $this->header = $header;
            
            $this->countHeader = count($this->header);
        }
    }
    /**
     * Converts current line to array with header values as array keys
     */
    private function buildData()
    {
        if ($this->file->valid() === false) {
            return;
        }
        $this->data = [];
        $line = $this->combineHeader();

        if ($line == null) {
            throw new LogicException("Header count does not match the row count.");
        }

        $this->data = $line;
    }
    /**
     * Combine header and values
     * @return array key valued pair
     */
    private function combineHeader()
    {
        $this->computeHeader();
        if ($this->hasHeader === true && $this->file->key() === 0) {
            $this->file->next();
        }
        $line = $this->file->current();
        if(!$line) return null;
        if ($this->safeMode === false) {
            try {
                $this->ensureIsValid($line);
            } catch (LogicException $e) {
                $this->invalids[$this->key()] = $e->getMessage();
                if ($this->skipInvalid === true) {
                    $this->file->next();
                    $this->buildData();
                }
                return null;
            }
        }
        if ($this->countHeader > 0) {
            $line = array_slice(array_pad($line, $this->countHeader, null), 0, $this->countHeader);
            $line = array_combine(array_map('strtolower', $this->header), $line);
        }
        return $line;
    }
    /**
     * Returns true if the file contains at least one invalid line. False otherwise.
     *
     * @return bool
     */
    public function check()
    {
        $this->read();
        return (bool) count($this->invalids) > 0;
    }
    /**
     * Ensures columns matches the column headers
     * @param $line current row
     */
    private function ensureIsValid($line)
    {
        if ($this->countHeader > 0) {
            $countColumn = count($line);
            if ($this->countHeader > $countColumn) {
                throw new LogicException(
                    sprintf(
                        'The header has %d elements. Found %d columns on line %d.',
                        $this->countHeader,
                        $countColumn,
                        $this->file->key()
                    )
                );
            }
        }
    }
    /**
     * Read the entire file
     *
     * @return array rows
     */
    public function read()
    {
        $rows = [];
        $this->file->rewind();
        while ($this->file->eof() === false) {
            $this->file->next();
            $this->buildData();
            $rows[] = $this->data;
        }
        $this->file->rewind();
        return $rows;
    }
    /**
     * Return count of columns
     * @return int column count
     */
    public function count()
    {
        if (-1 === $this->count) {
            $this->rewind();
            $count = iterator_count($this->file);
            if ($count > 0 && $this->hasHeader === true) {
                $count--;
            }
            $this->count = $count;
            $this->rewind();
        }
        return $this->count;
    }
    /**
     * Return the first row
     *
     * @return array current row
     */
    public function first()
    {
        return $this->rewind()->current();
    }
    /**
     * Move to specified line
     *
     * @param int $position Pointer position
     * @return $this current row
     */
    public function move($position)
    {
        $this->file->seek($position);
        return $this;
    }
    /**
     * Rewind iterator to the first element
     * @return $this current column
     */
    public function rewind()
    {
        if ($this->file) {
            $this->file->rewind();
        }
        $this->buildData();
        return $this;
    }
    /**
     * Return the current row
     *
     * @return array current row
     */
    public function current()
    {
        $this->buildData();
        return $this->data;
    }
    /**
     * Return the key of the current row
     *
     * @return int current key
     */
    public function key()
    {
        return $this->file->key();
    }
    /**
     * Return the next row
     *
     * @return array current row
     */
    public function next()
    {
        $this->file->next();
        $this->buildData();
        return $this->data;
    }
    /**
     * Check if current position is valid
     * @return bool true if valid, false if invalid
     */
    public function valid()
    {
        return $this->file->valid();
    }

    /**
     * Set header if initialized
     * @param boolean $hasHeader true if header exists, false otherwise
     */
    public function setHasHeader($hasHeader)
    {
        $this->hasHeader = $hasHeader;
        return $this;
    }
    /**
     * Set header
     * @param array $header Column headers
     */
    public function setHeader(array $header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * Initialize skip invalid
     * @param boolean $skipInvalid If true, skip invalid rows, otherwise, it triggers to return error on line number
     */
    public function setSkipInvalid($skipInvalid)
    {
        $this->skipInvalid = $skipInvalid;
        return $this;
    }
    /**
     * Initialize safe mode
     * @param bool $safeMode If true, returns invalid lines
     */
    public function setSafeMode($safeMode)
    {
        $this->safeMode = $safeMode;
        return $this;
    }
    /**
     * Return invalid lines
     * @return array error message that includes line number
     */
    public function getInvalids()
    {
        return $this->invalids;
    }
    /**
     * Return column headers
     * @return array row of column headers
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Return duplicate headers
     * @param $rawArray column headers
     * @return array row of column headers
     */
    function array_not_unique($rawArray)
    {
        $headersNotUnique = array();
        natcasesort($rawArray);
        reset($rawArray);

        $oldKey   = NULL;
        $oldValue = NULL;
        foreach ($rawArray as $key => $value) {
            if ($value === NULL) {
                continue;
            }
            if (strcasecmp($oldValue, $value) === 0) {
                $headersNotUnique[$oldKey] = $oldValue;
                $headersNotUnique[$key]     = $value;
            }
            $oldValue = $value;
            $oldKey   = $key;
        }
        return $headersNotUnique;
    }
}
