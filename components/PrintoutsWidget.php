<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use yii\base\Widget;
use Yii;

/**
 * Widget for Printouts. Renders the UI for displaying available printouts templates.
 */
class PrintoutsWidget extends Widget {
	public $product; // string name of the product for which reports will be retrieved
	public $program; // string code of the program for which reports will be retrieved
	public $occurrenceIds; // array containing occurrence ids for which reports will be generated
	public $checkboxSessionStorage; // string name of the session storage variable containing selection of occurrences
	public $entity = 'occurrence'; // string name of the entity used: occurrence or cross list level

	/**
	 * Initialize the widget
	 * Check if required attributes are specified
	 */
	public function init() {
		parent::init();

		// Check if occurrenceIds, product, and program were provided
		if(!isset($this->occurrenceIds) || !isset($this->product) || !isset($this->program)) {
			throw new \yii\web\HttpException(500, 'Missing required field. Required fields are: $occurrenceIds, $product, and $program');
		}

		// If occurrenceIds array is empty, require checkboxSessionStorage
		if(empty($this->occurrenceIds) && empty($this->checkboxSessionStorage)) {
			throw new \yii\web\HttpException(500, 'Missing value for $checkboxSessionStorage. Value is required when $occurrenceIds array is empty.');
		}
	}

	/**
	 * Run the widget by rendering the button and other elements
	 */
	public function run() {

		return $this->render('printouts', [
			'product' => $this->product,
			'program' => $this->program,
			'occurrenceIds' => $this->occurrenceIds,
			'checkboxSessionStorage' => $this->checkboxSessionStorage,
			'entity' => $this->entity,
		]);
	}
}