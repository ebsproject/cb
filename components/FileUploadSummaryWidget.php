<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

/**
 * Widget for File Upload Summary. Renders the UI for displaying file upload error logs.
 */
class FileUploadSummaryWidget extends FileUploadWidget {
	/**
	 * Run the widget by rendering the button and other elements
	 */
	public function run() {
		return $this->render('file-upload/summary', [
			'sourceToolName' => $this->sourceToolName,
			'fileDbId' => $this->fileDbId,
			'returnUrl' => $this->returnUrl,
			'configAbbrevPrefix' => $this->configAbbrevPrefix,
			'entity' => $this->entity
		]);
	}
}