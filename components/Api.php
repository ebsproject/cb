<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use Yii;
use yii\base\Component;
use GuzzleHttp\Client;

/**
 * Contains methods for accessing API
 */
class Api extends Component{

    /**
    * Use HTTP requests to GET, PUT, POST and DELETE data
    *
    * @param string method request method (POST, GET, PUT, etc)
    * @param string path url path request
    * @param object rawData  optional request content
    * @param string filter optional sort, page or limit options 
    * @param boolean retrieveAll optional whether all of the data will be fetched or not
    * @param string apiResource api domain prefix - cb/bso/cs/csps
    *
    * @return array list of requested information
    */
    public function getResponse($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb'){

        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
        $requestError = false;
        $errorType = 'API';

        // set filters if any
        $path = (strstr($path,'?') && $filter !== '') ? "$path&$filter" : (($filter !== '') ? "$path?$filter" : $path);
        
        switch ($apiResource) {
            case 'bso':
                $parameter = 'BSO_API_URL';
                break;
            case 'cs':
                $parameter = 'CS_API_URL';
                break;
            case 'csps':
                $parameter = 'CS_PS_URL';
                break;
            default:
                $parameter = 'CB_API_V3_URL';
        }

        $baseURI = getenv($parameter);

        if(empty($baseURI)){
            $errorMessage = Yii::t('app', "Environment variable for $parameter not found. Please try again or file a report for assistance.");
            Yii::$app->session->setFlash('error', $errorMessage);
            Yii::error( "Environment variable for $parameter not found", __METHOD__);
        } else {

            try{

                $client = new Client([
                    'base_uri' => $baseURI,
                    'headers' => [
                        'Authorization' => 'Bearer ' . $accessToken,
                        'Content-Type' => 'application/json'
                    ]
                ]);

                $response = $client->request($method, $path, ['body' => $rawData]);
                
            }
            catch (\GuzzleHttp\Exception\ClientException $e){
                $response = $e->getResponse();
            }
            catch (\GuzzleHttp\Exception\ConnectException $e){
                $errorType = 'connection';
                $requestError = true;
                $errorResponse = $e->getMessage();
            }
            catch(\GuzzleHttp\Exception\RequestException $e){
                $errorType = 'request';
                $requestError = true;
                $errorResponse = $e->getMessage();
            }
            catch (\GuzzleHttp\Exception\ServerException $e){
                $errorType = 'server';
                $requestError = true;
                $errorResponse = $e->getMessage();
            }

            // if there were errors encountered
            if($requestError){
                $errorMessage = Yii::t('app', "A $errorType error occurred while loading the page. Please try again or file a report for assistance.");
                Yii::$app->session->setFlash('error', $errorMessage);
                Yii::error($errorResponse . ' ' . json_encode(['method'=>$method, 'path'=>$path, 'content'=>$rawData]), __METHOD__);

                return [
                    'status' => 500,
                    'body' => [
                        'metadata' => [
                            'pagination' => [
                                'pageSize' => null,
                                'totalCount' => null,
                                'currentPage'=> null,
                                'totalPages'=> null
                            ],
                            'status' => [[
                                'message' => $errorMessage,
                                'messageType' => 'Internal',
                            ]]
                        ],
                        'result' => [
                            'data' => [],
                        ],
                    ]
                ];
            }else{
                $status =  $response->getStatusCode();
                $responseBody = json_decode($response->getBody()->getContents(),true);

                if($status == 200 && $retrieveAll){
                    $totalPages = $responseBody['metadata']['pagination']['totalPages'] ?? null;
                    $results = $responseBody['result']['data'] ?? [];

                    // build data array
                    if($totalPages > 1){
                        //set page
                        if(!strpos($path,'page=')){
                            $path = strpos($path,'?') ? "$path&page=" : $path = "$path?page=";
                        }
                        
                        $addlResult = [];
                        for ($i=2; $i <= $totalPages; $i++){
                            $data = Yii::$app->api->getResponse($method, $path.$i, $rawData, '', false, $apiResource);

                            if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
                                $data = $data['body'];
                                $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                            }

                            $results = array_merge($results, $addlResult);
                        }
                    }

                    $responseBody['result']['data'] = $results;
                }

                return [
                    'status'=> $status,
                    'body'=> $responseBody
                ];
            }
        }
    }
    
    /**
    * Use HTTP requests to GET, PUT, POST and DELETE data
    * 
    * @param method string request method (POST, GET, PUT, etc)
    * @param path string url path request
    * @param rawData json optional request content
    * @param filter string optional sort, page or limit options 
    * @param retrieveAll boolean optional whether all of the data will be fetched or not
    * @param apiResource string api domain prefix - cb/bso/cs/csps
    *
    * @return array list of requested information
    */
    public function getParsedResponse($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb') {

        $data = Yii::$app->api->getResponse($method, $path, $rawData, $filter, $retrieveAll, $apiResource);
        $result = [];
        $message = $data['body']['metadata']['status'][0]['message'] ?? 'Internal';
        $statusCode = $data['status'] ?? 500;
        if (isset($data['status']) && $data['status'] == 200) {
          $data = $data['body'];
          $result = isset($data['result']['data']) ? $data['result']['data'] : [];
          $success = true;
          $totalCount = $data['metadata']['pagination']['totalCount'];
          $totalPages = $data['metadata']['pagination']['totalPages'];
        } else{
            $success = false;
            $totalCount = 0;
            $totalPages = 0;
        }

        return [
            'success' => $success,
            'message' => $message,
            'data' => $result,
            'totalCount' => $totalCount,
            'status' => $statusCode,
            'totalPages' => $totalPages
        ];
    }

    /**
    * Store API response in a file
    * 
    * @param method string request method (POST/GET)
    * @param path string url path request
    * @param format string file format 
    * @param fileName string file name 
    * @param apiResource string instance name
    *
    * @return filePath string where the file was successfully stored
    */
    public function uploadFile($method, $path, $format, $fileName, $apiResource = 'cb'){

        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
        $requestError = false;
        
        switch ($apiResource) {
            case 'bso': 
                $parameter = 'BSO_API_URL';
                break;
            case 'cs':
                $parameter = 'CS_API_URL';
                break;
            case 'csps':
                $parameter = 'CS_PS_URL';
                break;
            default:
                $parameter = 'CB_API_V3_URL';
        }

        $baseURI = getenv($parameter);

        try{
            $client = new Client([
                'base_uri' => $baseURI, 
                'headers' => [
                    'Authorization' => "Bearer $accessToken",
                    'Content-Type' => "application/$format",
                    'Accept' => "application/$format",
                ]
            ]);

            $filePath = Yii::getAlias('@webroot').'/files/'.$fileName;
            $resource = fopen($filePath, 'w');
            $client->request($method, $path, ['sink' => $resource]);
    
            return $filePath;
        }
        catch (\GuzzleHttp\Exception\ClientException $e){
            $response = $e->getResponse();
        }
        catch (\GuzzleHttp\Exception\ConnectException $e){
            $errorType = 'connection';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }
        catch(\GuzzleHttp\Exception\RequestException $e){
            $errorType = 'request';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }
        catch (\GuzzleHttp\Exception\ServerException $e){
            $errorType = 'server';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }

        // if there were errors encountered
        if($requestError){
            $errorMessage = Yii::t('app', "A $errorType error occurred while loading the page. Please try again or file a report for assistance.");
            Yii::$app->session->setFlash('error', $errorMessage);
            Yii::error($errorResponse);

            return [
                'status' => 500,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'pageSize' => null,
                            'totalCount' => null,
                            'currentPage'=> null,
                            'totalPages'=> null
                        ],
                        'status' => [[
                            'message' => $errorMessage,
                            'messageType' => 'Internal',
                        ]]
                    ],
                    'result' => [
                        'data' => [],
                    ],
                ]
            ];
        }
    }

    /**
    * Use HTTP requests to GET, PUT, POST and DELETE data
    * 
    * @param method string request method (POST, GET, PUT, etc)
    * @param path string url path request
    * @param rawData json optional request content
    * @param filter string optional sort, page or limit options 
    * @param retrieveAll boolean optional whether all of the data will be fetched or not
    * @param apiResource string source where the data will come from
    * @param limit integer limit of the records to be fetched, should be multiples of 100
    *
    * @return array list of requested information
    */
    public function getResponseWithLimit($method, $path, $rawData = null, $filter = '', $retrieveAll = false, $apiResource = 'cb', $limit=100){
        
        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
        $requestError = false;
        // set filters if any
        $path = (strstr($path,'?') && $filter !== '') ? "$path&$filter" : (($filter !== '') ? "$path?$filter" : $path);
        
        switch ($apiResource) {
            case 'bso': 
                $parameter = 'BSO_API_URL';
                break;
            case 'cs':
                $parameter = 'CS_API_URL';
                break;
            case 'csps':
                $parameter = 'CS_PS_URL';
                break;
            default:
                $parameter = 'CB_API_V3_URL';
        }

        $baseURI = getenv($parameter);

        try{

            $client = new Client([
                'base_uri' => $baseURI, 
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Content-Type' => 'application/json'
                ]
            ]);

            $response = $client->request($method, $path, ['body' => $rawData]);
            
        }
        catch (\GuzzleHttp\Exception\ClientException $e){
            $response = $e->getResponse();
        }
        catch (\GuzzleHttp\Exception\ConnectException $e){
            $errorType = 'connection';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }
        catch(\GuzzleHttp\Exception\RequestException $e){
            $errorType = 'request';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }
        catch (\GuzzleHttp\Exception\ServerException $e){
            $errorType = 'server';
            $requestError = true;
            $errorResponse = $e->getMessage();
        }

        // if there were errors encountered
        if($requestError){
            $errorMessage = Yii::t('app', "A $errorType error occurred while loading the page. Please try again or file a report for assistance.");
            Yii::$app->session->setFlash('error', $errorMessage);
            Yii::error($errorResponse);

            return [
                'status' => 500,
                'body' => [
                    'metadata' => [
                        'pagination' => [
                            'pageSize' => null,
                            'totalCount' => null,
                            'currentPage'=> null,
                            'totalPages'=> null
                        ],
                        'status' => [[
                            'message' => $errorMessage,
                            'messageType' => 'Internal',
                        ]]
                    ],
                    'result' => [
                        'data' => [],
                    ],
                ]
            ];
        }else{
            $status =  $response->getStatusCode();
            $responseBody = json_decode($response->getBody()->getContents(),true);

            if($status == 200){
                if($retrieveAll){
                    $totalPages = (isset($responseBody['metadata']['pagination']['totalPages'])) ? $responseBody['metadata']['pagination']['totalPages'] : null;
                    $results = (isset($responseBody['result']['data'])) ? $responseBody['result']['data'] : [];

                    // build data array
                    if($totalPages > 1){
                        //set page
                        if(!strpos($path,'page=')){
                            $path = strpos($path,'?') ? "$path&page=" : $path = "$path?page=";
                        }
                        
                        $addlResult = [];
                        for ($i=2; $i <= $totalPages; $i++){
                            if(count($results) < $limit){
                                $data = Yii::$app->api->getResponse($method, $path.$i, $rawData, '', false, $apiResource);

                                if(isset($data['status']) && $data['status'] == 200 && isset($data['body']['result']['data'][0])){
                                    $data = $data['body'];
                                    $addlResult = (isset($data['result']['data'])) ? $data['result']['data'] : [];
                                }

                                $results = array_merge($results, $addlResult);
                            } else break;
                        }
                    }

                    $responseBody['result']['data'] = $results;
                }
            }

            return [
                'status'=> $status,
                'body'=> $responseBody
            ];
        }
    }

    /**
     * Retrieve GraphQL response
     *
     * @param string method request method (POST, GET)
     * @param string query graphQL query
     * @param string apiResource source where the data will come from (default: cs)
     */
    public function getGraphQlResponse($method, $query, $apiResource = 'cs'){
        $accessToken = Yii::$app->session->get('user.b4r_api_v3_token');
        switch ($apiResource) {
            case 'bso':
                $parameter = getenv('BSO_API_URL');
                break;
            case 'cs':
                $parameter = getenv('CS_API_URL');
                break;
            case 'csps':
                $parameter = getenv('CS_PS_URL');
                break;
            default:
                $parameter = getenv('CB_API_V3_URL');
        }
        $baseURI = substr($parameter , -1) !== '/' ? $parameter.'/graphql' : $parameter.'graphql';

        if(empty($parameter)){
            $errorMessage = Yii::t('app', "Environment variable for $apiResource not found. Please try again or file a report for assistance.");
            Yii::$app->session->setFlash('error', $errorMessage);
            Yii::error( "Environment variable for $apiResource not found", __METHOD__);
        } else {
            try{

                $client = new Client();
                $response = $client->request($method, $baseURI,
                [
                    'headers' => [
                      'Authorization' => 'Bearer ' . $accessToken,
                      'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode(['query' => $query])
                  ]
                );

                return json_decode($response->getBody()->getContents(), true)['data'] ?? [];

            }
            catch (\GuzzleHttp\Exception\ClientException $e){
                $response = $e->getResponse();
                Yii::error($e->getMessage() ?? '', __METHOD__);
            }
            catch (\GuzzleHttp\Exception\ConnectException $e){
                Yii::error($e->getMessage() ?? '', __METHOD__);
            }
        }
    }
}
