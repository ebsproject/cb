<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Config;
use app\models\User;
use yii\helpers\Url;
use app\models\Program;

/**
 * Get user access
 * Usage: Yii::$app->access->renderAccess();
 */
class Access extends Component
{ 	
	/**
	* Combined check for access data and rbac permissions
	* @param rbacAction string name of the action element
	* @param tool string abbrev of the current tool
	* @param creatorDbId integer id of the creator of record; used for grid actions
	* @param accessData json permission data for record
	* @param elementType string if action is read or write
	* @param usage string if usage is for elements or rendering page
	* @param checkAccessData enforce checking access data parameter
	* @return string use to hide the elements based on permission
	* 
	* */
 	public function renderAccess($rbacAction, $tool, $creatorDbId=null, $accessData=null, $elementType=null, $usage=null, $checkAccessData=false) {

		$render = false;
 		$user = new User();
		$userId = $user->getUserId();
		$rbacDisabled = false;

		// store flag if RBAC is disabled
		if(Yii::$app->session->get('enableRbac') !== null && !Yii::$app->session->get('enableRbac')){
 			$rbacDisabled = true;
 		}

		if(isset($accessData) && $accessData != null){
			//check access data
			$programId = Yii::$app->userprogram->get('id');

			if($elementType == 'write'){
				if(isset($accessData['program'][$programId]['permission'])){
					// if permission for the program is set and permission is write, return true else false
					if($accessData['program'][$programId]['permission'] == 'write'){
						$render = true;
					} else {
						$render = false;
					}
				}
				if(isset($accessData['person'][$userId]['permission'])){
					// if permission for the user is set and permission is write, return true else false
					if($accessData['person'][$userId]['permission'] == 'write'){
						$render = true;
					} else {
						$render = false;
					}
				}
			}
		}

		// if user is the creator of the record, give full access
		if($creatorDbId == $userId){
			$render = true;
			return $render;
		}

		// if accessed via URL and user has no access at data-level permission
		// OR if accessed via URL, check data-level permission and access data is null, return no access
		if(($usage == 'url' && !$render && $accessData !== null) || $usage == 'url' && $accessData == null && $checkAccessData){
			Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
			Yii::$app->response->redirect(Url::base().'/index.php');

          	return false;
		} else if(!$render && $accessData !== null){
			// permission is not write
			return false;
		} else if($rbacDisabled && $render && $accessData !== null){
			// if RBAC is disabled, return data-level permission, else do checking of UI-level permission
			return $render;
		}
 		
 		if(Yii::$app->session->get('enableRbac') !== null && !Yii::$app->session->get('enableRbac')){
 			return true;
 		}

     	$programAbbr = Yii::$app->userprogram->get('abbrev');

 		$permissions = null;

 		if(Yii::$app->session->get('currentPermissions') !== null){
 			$currentPermission = Yii::$app->session->get('currentPermissions');
 			if($currentPermission['tool'] == $tool && $currentPermission['program'] == $programAbbr){
 				$permissions = $currentPermission['permission'];
 			}
 		}

 		if($permissions == null){
 		 	
	 		$rbacPermissions = Yii::$app->session->get('userPermissions')[$programAbbr] ?? []; //user permissions

	 		if(!$rbacPermissions){ //false, meaning no user profile retrieved due to error; disable rbac
	 			Yii::$app->session->set('enableRbac', false);
	 			return true;
	 		} elseif(!empty($rbacPermissions)){  //if there's user profile retrieved
		       	$index = array_search($tool, array_column($rbacPermissions, 'appAbbrev'));
		        $permissions = $rbacPermissions[$index]['permissions'];

				$setCurrentPermissions = [
					'tool' => $tool,
					'program' => $programAbbr,
					'permission' => $permissions
				];

			     // store product permissions to session
	 		 	Yii::$app->session->set('currentPermissions', $setCurrentPermissions);
 		 	} else { //if no permission for the user; empty = no permission
 		 		if($usage == 'url'){
 		 			
			          Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
			          Yii::$app->response->redirect(Url::base().'/index.php');
 		 		}
 		 		return false;
 		 	}
 		}
        
		$render = false;
		if($permissions != null){
			if(in_array($rbacAction, $permissions)){
				$render = true;
			}
		}
		
		if(!$render && $usage == 'url'){
	          Yii::$app->session->setFlash('error', "You have no permission to the page you are trying to access. Please contact your data administrator.");
	          // Go back to the homepage
	          Yii::$app->response->redirect(Url::base().'/index.php');
		}
		return $render;
	}
}