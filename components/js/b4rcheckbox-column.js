/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

 /**
  * contains functions for B4RCheckboxColumn class
  */

$(document).ready(function() {
    checkboxActions();
});

$(document).on('ready pjax:success','#'+pjaxGridId, function(e) {
    e.stopPropagation();
    e.preventDefault();
    checkboxActions();
});

/**
 * Contains events for clicking checkboxes
 */
function checkboxActions() {
    // store default values in session storage
    $.each(JSON.parse(defaultValues), function(i, v) {
        saveSelection('select', v.toString());
    });


    // set checkbox to checked if value is in session
    var selectedRows = sessionStorage.getItem(sessionStorageName);
    selectedRows = JSON.parse(selectedRows);

    displayTotalCount(selectedRows);

    if (type == 'checkbox') {
        var inputs = "input:checkbox."+selectClass;
    } else {
        var inputs = ".b4rradiobtn";
    }
    var $grid = $('#'+gridId);

    $('#'+gridId).find(inputs).each(function() {
        var value = $(this).val();
        if ($.inArray(value, selectedRows) !== -1) {
            $(this).prop('checked',true);
            $(this).addClass('isClicked');
        }
    });

    if ($grid.find(inputs).length != 0) {
        var all = $grid.find(inputs).length == $grid.find(inputs + ":checked").length;
        $('#'+selectAllId).prop('checked', all).change();
    }
    
    // select all items in browser
    $('#'+selectAllId).on('click',function(e){

        if($(this).prop("checked") === true)  {
            $(this).attr('checked','checked');
            $('.'+selectClass).attr('checked','checked');
            $('.'+selectClass).prop('checked',true);
            $("."+selectClass+":checkbox").parent("td").parent("tr").addClass("grey lighten-4");
            saveSelection('selectAll');
        }else{
            $(this).removeAttr('checked');
            $('.'+selectClass).prop('checked',false);
            $('.'+selectClass).removeAttr('checked');
            $("input:checkbox."+selectClass).parent("td").parent("tr").removeClass("grey lighten-4");      
            saveSelection('deselectAll');
        }
    });

    if (type == 'checkbox') {
        // click row in browser
        $("#"+gridId+ " tbody tr:not(a)").on('click',function(e){
            var thisRow=$(this).find('input:checkbox')[0];
            var value = $(thisRow).val();

            if(thisRow.checked){
                saveSelection('deselect', value);
                thisRow.checked=false;
                $('#'+selectAllId).prop("checked", false);
                $(this).removeClass("grey lighten-4");
            } else {
                saveSelection('select', value);
                $(this).addClass("grey lighten-4");
                thisRow.checked=true;
                $("#"+thisRow.id).prop("checked");

                var isAllChecked = 0;
                $("."+selectClass).each(function() {
                    if (!this.checked)
                        isAllChecked = 1;
                });

                if (isAllChecked == 0) {
                    $("#"+selectAllId).prop("checked", true);
                }
            }
        });
    } else {
        $("#"+gridId+" .b4rradiobtn").on('click', function(e) {
            var current = $(this);
            var name = $('input[id='+current.prop('id')+']:checked').attr('name');
            var value = current.val();
            var exisitingGroupSelection = null;

            if (current.hasClass('isClicked')) {
                current.prop('checked', false);
                current.removeClass('isClicked');
                saveSelection('deselect', value);
            } else {
                current.addClass('isClicked');
                current.prop('checked', true);
                
                $("#"+gridId+' input[name="'+name+'"]:radio:not(:checked)').each(function(index,value){
                    var radioItem = $('#'+value.id);
                    if(radioItem.hasClass('isClicked')){ exisitingGroupSelection = radioItem.val()}
                });
                if(exisitingGroupSelection != null){
                    $('#'+exisitingGroupSelection).removeClass('isClicked');
                    saveSelection('deselect', exisitingGroupSelection);
                }
                saveSelection('select', value);       
            }

        });
    }
}

/**
 * Saves the selection on sessionStorage
 */
function saveSelection(mode, value) {

    var currentSelection = sessionStorage.getItem(sessionStorageName);

    if (currentSelection == null) {
        currentSelection = [];
    } else {
        currentSelection = JSON.parse(currentSelection);
    }

    if (mode == 'selectAll') {
        $('#'+gridId).find("input:checkbox."+selectClass).each(function() {
            if ($(this).prop("checked") === true) {
                var value = $(this).val();

                if ($.inArray(value, currentSelection) === -1) {
                    currentSelection.push(value);
                }
            }
        });
    } else if (mode == 'select') {
        if ($.inArray(value, currentSelection) === -1) {
            currentSelection.push(value);
        }
    } else if (mode == 'deselect') {
        var index = currentSelection.indexOf(value);
        if (index !== -1) {
            currentSelection.splice(index,1);
        }
    } else if (mode == 'deselectAll') {
        $('#'+gridId).find("input:checkbox."+selectClass).each(function() {
            if ($(this).prop("checked") === false) {
                var value = $(this).val();
                var index = currentSelection.indexOf(value);
                if (index !== -1) {
                    currentSelection.splice(index,1);
                }
            }
        });
    }

    displayTotalCount(currentSelection);
    sessionStorage.setItem(sessionStorageName, JSON.stringify(currentSelection));
}

/**
 * Displays the total count summary if placeholder id is given
 */
function displayTotalCount(currentSelection) {
    
    if (displaySummaryId !== '') {
        if (currentSelection === null) {
            $('#'+displaySummaryId).html('');
            return true;
        }
        var totalCount = currentSelection.length;
        
        if (totalCount != 0) {
            $('#'+displaySummaryId).html(
                '<b><span id="selected-items-count"> '+totalCount+
                '</span></b> <span id="selected-items-text"> selected items &nbsp;</span>'
            );
        } else {
            $('#'+displaySummaryId).html('');
        }
    }
}

/**
 * Contains different methods for manipulating checkboxes
 * 
 * @function getSelectedRows retrieves currently selected rows in a grid
 */

(function ($) {
    $.fn.b4rCheckboxColumn = function (method) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    }

    var methods = {
        getSelectedRows: function() {
            var selectedRows = sessionStorage.getItem(sessionStorageName);
            return JSON.parse(selectedRows);
        },

        clearSelection: function() {
            sessionStorage.removeItem(sessionStorageName);
        }
    }
})(window.jQuery);