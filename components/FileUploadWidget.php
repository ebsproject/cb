<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use yii\base\Widget;

/**
 * Parent Widget class for File Upload widgets.
 */
class FileUploadWidget extends Widget {

	public $fileDbId; // integer file upload identifier
	public $returnUrl; // string return URL
	public $configAbbrevPrefix; // prefix for the configurations abbrev
	public $sourceToolName; // name of the tool displaying the widget
	public $entity; //entity; germplasm, seed or package

	/**
	 * Initialize the widget
	 * Check if required attributes are specified
	 */
	public function init() {
		parent::init();

		// Check if fileDbId was provided
		if(!isset($this->sourceToolName) || !isset($this->fileDbId) || !isset($this->returnUrl))
		{
			throw new \yii\web\HttpException(500, 'Missing required field. Required fields are: $sourceToolName, $fileDbId, and $returnUrl');
		}
	}
}