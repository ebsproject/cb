<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use Yii;
use yii\base\Component;
use app\models\UserDashboardConfig;
use app\models\Person;
use app\models\Program;
use app\models\CropProgram;

/**
 * Get current program of user
 * Usage: Yii::$app->userprogram->get('abbrev');
 */
class UserProgram extends Component
{
	/**
	 * Get current program of user
	 */
	public function get($attr='abbrev'){
		$program = null;
		$programModel = new Program(new CropProgram());
		
		if(isset($_GET['program']) && !empty($_GET['program'])){ //if in url parameter
			$program = strtoupper($_GET['program']);
		}else{ //if not in url, check in user session
			$userDashboardConfig = \Yii::$container->get('app\models\UserDashboardConfig');
			
			$program = $userDashboardConfig->getUserProgram();
		}

		switch ($attr) {
			case 'id':
				if(!is_numeric($program)){
					$program = $programModel->getProgramAttr('abbrev',$program,'id');
				}
				break;

			case 'abbrev':
				if(is_numeric($program)){
					$program = $programModel->getProgramAttr('id',$program,'abbrev');
				}
				break;
			
			case 'name':
				if(is_numeric($program)){
					$program = $programModel->getProgramAttr('id',$program,'name');
				}else{
					$program = $programModel->getProgramAttr('abbrev',$program,'name');
				}
				break;
		}

		if(empty($program)){
			$program = 0;
		}

		return $program;
	}

	/**
	 * Verify program of user upon login
	 * 
	 **/
	public function verify(){
		$programModel = new Program(new CropProgram());
		$person = \Yii::$container->get('app\models\Person');
		
		// get program code values where user have access to
		$programs = $person->getPersonPrograms();

		// if no valid programs, return null
		if(empty($programs)){
			return null;
		}

		// store valid programs to session
		Yii::$app->session->set('user_valid_programs', $programs);

		// get program from the config
		$userDashboardConfig = \Yii::$container->get('app\models\UserDashboardConfig');
		$programIdFromConfig = $userDashboardConfig->getUserProgram(true);
		$firstProgram = $programs[array_key_first($programs)] ?? '';

		// if no saved program in config, return first program
		if(empty($programIdFromConfig)){
			return $firstProgram;
		}

		// get program abbrev of program from config
		$programCode = $programModel->getProgramAttr('id', $programIdFromConfig, 'program_code');
		
		// if program from config is in the list of valid programs
		if(in_array($programCode, $programs)){
			return $programCode;
		}

		// if program abbrev from the config is not valid program, return first valid program
		return $firstProgram;

	}
 
}