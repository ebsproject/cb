<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * Favorites widget
 */
namespace app\components;

use yii\base\Widget;
use app\models\UserDashboardConfig;

class FavoritesWidget extends Widget{
    public $module; //module of app
    public $controller; //controller of app
    public $action; //action of app
    public $params; //params of app
    public $options; //css and class
    public $class; //class of button
    public $style; //style of button
    public $iconStyle;

    public function init(){
        parent::init();
        
        if(isset($this->options['class'])){
            $this->class = $this->options['class'];
        }

        if(isset($this->options['style'])){
            $this->style = $this->options['style'];
        }
    }

    public function run(){
        $appId = null;
        $inFav = true;
        //check whether already marked as favorite
        $inFavorites = UserDashboardConfig::checkIfInFavorites($this->module,$this->controller,$this->action,$this->params);

        if(is_string($inFavorites) && !is_bool($inFavorites)){ //if application does not exist in db
            return '<small><i>App not in db</i></small>';
        }
        
        if(is_array($inFavorites) && $inFavorites['inFavorites']!==false){//if marked as favorite
            $this->style = $this->style . ' color:#f9a825 !important;opacity:1;';
            $appId = $inFavorites['appId'];
        }

        if(is_array($inFavorites) && $inFavorites['inFavorites']!==true){//if not marked as favorite
            $appId = $inFavorites['appId'];
            $inFav = false;
        }

        if(!$inFavorites && !empty($inFavorites)){//not in favorites
            return '<small><i>App not in db</i></small>';
        }

        //render favorites button
        return $this->render('favorites',[
            'class'=>$this->class,
            'style'=>$this->style,
            'module'=>$this->module,
            'controller'=>$this->controller,
            'action'=>$this->action,
            'params'=>$this->params,
            'appId'=>$appId,
            'inFavorites' => ($inFav) ? 'true' : 'false',
            'title' => ($inFav) ? 'Unmark as favorite' : 'Mark as favorite',
            'iconStyle' => $this->iconStyle
        ]);
    }
}



?>