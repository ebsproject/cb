<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use app\controllers\Dashboard;
use app\models\Config;
use app\models\Program;
use app\models\Variable;
use app\modules\query\models\QueryModel;

use yii\base\Widget;
use yii\helpers\Url;
use Yii;

/**
 * Query tool feature that allows filtering of values with basic and advanced parameters.
 * Parameters are defined in a json configuration object
 */

class QueryWidget extends Widget {
    public $configAbbrev;
    public $gridId;
    public $formId='query-form';
    public $sessionName;
    public $currentlyAppliedFilters = [];
    public $url;
    public $totalCount=0;
    public $clearSelectionCb;
    public $tool;
    public $searchCharMin;

    /**
     * Initialize the widget
     */
    public function init() {
        parent::init();
    }

    /**
     * Run the widget by rendering the query tool UI
     * Also checks and assigns the attributes
     */
    public function run() {
        
        // check if query session is available
        if (isset(Yii::$app->session[$this->sessionName]) && !empty(Yii::$app->session[$this->sessionName])) {
            $this->currentlyAppliedFilters = Yii::$app->session[$this->sessionName];
        }
        // get configuration fields
        $config = new Config();
        $config = $config->getConfigByAbbrev($this->configAbbrev);
        $config = json_decode(json_encode($config));
        if(empty($config)) {
            $config = (object) ['fields' => (object) ['basic'=>[], 'additional'=>[]]];
        }
        $basicFlds = $this->generateBasicFields($config, $this->configAbbrev);
        $addlFldsData = $this->getAdditionalColumns($config, $this->configAbbrev);
        $addlFlds = $addlFldsData['addlFldsArr'];
        $addlFldsDataType = $addlFldsData['addFldsDataType'];
        $addlFldsOptions = $addlFldsData['addlFldsOptions'];
        $addlFldsCurrent = $addlFldsData['addlFldsCurrent'];

        return $this->render('query', [
            'basicFlds' => $basicFlds,
            'addlFlds' => json_encode($addlFlds),
            'addlFldsDataType' => json_encode($addlFldsDataType),
            'addlFldsOptions' => json_encode($addlFldsOptions),
            'addlFldsCurrent' => json_encode($addlFldsCurrent),
            'currentlyAppliedFilters' => !empty($this->currentlyAppliedFilters) ? json_encode($this->currentlyAppliedFilters) : "",
            'configAbbrev' => $this->configAbbrev,
            'gridId' => $this->gridId,
            'formId' => $this->formId,
            'findUrl' => $this->url,
            'totalCount' => $this->totalCount,
            'clearSelectionCb' => $this->clearSelectionCb,
            'tool' => $this->tool,
            'entity' => $this->configAbbrev,
            'searchCharMin' => $this->searchCharMin
        ]);
    }

    /**
     * Generates the basic fields parameters
     * 
     * @param Object $config the configuration value for the given entity
     * @param String $entity the specified entit. ie PRODUCT
     * @return String $htmlFldsStr string in HTML format
     */
    private function generateBasicFields ($config, $entity) {
        $dataUrl = Url::to(['/query/default/get-filter-data']);
        $basicFlds = $config->fields->basic;
        $htmlFlds = [];
        $fldCount = count($basicFlds) > 0? count($basicFlds): 1;
        $fldDivider = $fldCount > 6 ? 6 : $fldCount;
        $colDiv = 's'.floor(12 / $fldDivider);

        $counter = 0;
        $isTagEnded = false;
        foreach ($basicFlds as $fld) {
            
            $defaultValue = $this->getCurrentValue($fld);

            // Set default value for crop select2 field in Germplasm Search using current Program
            if($this->tool == 'germplasm_browser' && $fld->fieldName == 'cropCode' && $defaultValue == '') {
                $programDbId = Yii::$app->userprogram->get('id');
                $crops = \Yii::$container->get('app\models\Crop')->searchAll(['programDbId' => "equals $programDbId"]);
                $defaultValue = [];
                foreach ($crops['data'] as $crop) {
                    $defaultValue[] = $crop['cropCode'];
                }
                $defaultValue = implode('|', $defaultValue);
            }
            $tempStr = QueryModel::generateHtmlElements($fld, $colDiv, $dataUrl, $entity, 'basic', true, $defaultValue);

            if ($counter == 0) {
                $tempStr = '<div class="row">' . $tempStr;
                $isTagEnded = false;
            }
            if ($counter == 5) {
                $isTagEnded = true;
                $tempStr .= '</div>';
                $counter = 0;
            }else {
                $counter++;
            }
            $htmlFlds[$fld->order_number] = $tempStr;
        }

        $htmlFldsStr = implode(' ', $htmlFlds);
        if (!$isTagEnded) {
            $htmlFldsStr .= '</div>';
        }
        return $htmlFldsStr;
    }

    /**
     * Retrieve the current value of the field in session
     * 
     * @param Object $fld the current selected field
     */
    private function getCurrentValue($fld) {
        $currentlyAppliedFilters = $this->currentlyAppliedFilters;
        $defaultValue = '';

        // $defaultValue = $fld->default_value;
        $currentFilterKey = $fld->fieldName;
        if (isset($currentlyAppliedFilters[$currentFilterKey]) && !empty($currentlyAppliedFilters[$currentFilterKey])) {
            foreach ($currentlyAppliedFilters[$currentFilterKey] as $val) {
                $defaultValue = $val;
                break;
            }
            unset($currentlyAppliedFilters[$currentFilterKey]);
            $this->currentlyAppliedFilters = $currentlyAppliedFilters;
        }
        return $defaultValue;
    }
    
    /**
     * Retrieves the additional fields
     * 
     * @param Object $config the configuration value for the given entity
     * @param String $entity the specified entity, ie PRODUCT
     * @return Array contains the attributes for the additional fields
     */
    private function getAdditionalColumns ($config, $entity) {
        
        $addlFldsObj = $config->fields->additional;
        $addlFldsArr = [];
        $addlFldsDataType = [];
        $addlFldsOptions = [];
        $addlFldsCurrent = [];
        $currentlyAppliedFilters = $this->currentlyAppliedFilters;

        foreach ($addlFldsObj as $fld) {
            $fldName = $fld->fieldName;
            if (isset($currentlyAppliedFilters[$fldName]) && !empty($currentlyAppliedFilters[$fldName])) {
                $addlFldsCurrent[$fld->variable_abbrev] = $currentlyAppliedFilters[$fldName];
            }

            // get data type from variable
            $variable = new Variable();
            $variableObj = $variable->getVariableByAbbrev($fld->variable_abbrev);
            $dataType = array_key_exists('data_type',$variableObj) ? $variableObj['data_type'] : 'string';
            $dataType = QueryModel::getStandardDataType($dataType);

            $addlFldsDataType[$fld->variable_abbrev] = $dataType;
            $addlFldsOptions[$fld->variable_abbrev] = $fld;
            
            $addlFldsArr[$fld->order_number] = [
                'label' => $fld->label,
                'abbrev' => $fld->variable_abbrev,
            ];
        }
        return [
            'addlFldsArr' => $addlFldsArr,
            'addFldsDataType' => $addlFldsDataType,
            'addlFldsOptions' => $addlFldsOptions,
            'addlFldsCurrent' => $addlFldsCurrent
        ];
    }
}