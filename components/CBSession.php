<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use yii\base\Component;
use app\models\DataBrowserConfiguration;
use app\models\User;

/**
 * Contains methods for accessing CBSession
 */
class CBSession extends Component{

    /**
     * Class constructor
     */
    public function __construct(
        public DataBrowserConfiguration $dataBrowserConfiguration,
        public User $user
    ) {
        parent::init();
    }

    /**
     * Retrieves the main session variable (cb-session-variables) 
     * for the user as stored in the database.
     * If no record is found, returned id is 0,
     * and returned data is an empty array.
     */
    private function getMainSessionVar() {
        // Get user id
        $userDbId = $this->user->getUserId();
        // Retrieve from database
        $result = $this->dataBrowserConfiguration->searchAll(
            [
                "dataBrowserDbId" => "equals cb-session-variables",
                "userDbId" => "equals $userDbId"
            ],
            'limit=1',
            true
        );

        // Parse result
        $data = $result["data"] ?? [];
        $mainSessionVarRecord = $data[0] ?? [];

        // Return id and data
        return [
            "configDbId" => $mainSessionVarRecord["configDbId"] ?? 0,
            "data" => $mainSessionVarRecord["data"] ?? []
        ];
    }

    /**
     * Retrives the given variable name from the session.
     * @param String $varName name of the session variable
     * @return Any value associated with the given variable name (null if not set)
     */
    public function get($varName) {
        // Get main session var
        $mainSessVar = $this->getMainSessionVar();
        $mainSessVarData = $mainSessVar["data"];

        return $mainSessVarData[$varName] ?? null;
    }

    /**
     * Sets the variable name given a value.
     * @param String $varName name of the session variable
     * @param Any $varValue value to be set for the variable
     */
    public function set($varName, $varValue) {
        // Get main session var
        $mainSessVar = $this->getMainSessionVar();
        $mainSessDbId = $mainSessVar["configDbId"];
        $mainSessVarData = $mainSessVar["data"];

        // Set new value for given var name
        if (!isset($varValue)) {
            // If varValue is null and data includes the variable name,
            // unset from the session data.
            if(isset($mainSessVarData[$varName])) unset($mainSessVarData[$varName]);
        }
        else {
            // Else, add varValue to session data.
            $mainSessVarData[$varName] = $varValue;
        }

        // If the main session variable does not exist yet,
        // create the variable and add the data.
        if ($mainSessDbId == 0) {
            // Create new
            $result = $this->dataBrowserConfiguration->create([
                "records" => [
                    [
                        "dataBrowserDbId" => "cb-session-variables",
                        "name" => "cb-all-session-variables",
                        "type" => "config",
                        "data" => $mainSessVarData
                    ]
                ]
            ]);
        }
        // If the main session variable exists, update the data.
        else {
            // Update existing
            $result = $this->dataBrowserConfiguration->updateOne($mainSessDbId, [
                "data" => $mainSessVarData
            ]);
        }
    }

    /**
     * Removes all session variables
     */
    public function flushSession() {
        // Get main session var
        $mainSessVar = $this->getMainSessionVar();
        $mainSessDbId = $mainSessVar["configDbId"];

        if($mainSessDbId != 0){
            // Set data to empty array
            $result = $this->dataBrowserConfiguration->updateOne($mainSessDbId, [
                "data" => []
            ]);
        }
    }

}