<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use yii\base\Widget;

/**
 * Widget for Working List viewer page. Renders the UI for displaying the working list items.
 */
class WorkingListWidget extends Widget {

	public $returnUrl; // string return URL
	public $listType; // string list type
	public $sourceTool; // string name for source tool
	public $itemCount; // current list item count

	/**
	 * Initialize the listId
	 * Check if required attributes are specified
	 */
	public function init() {
		parent::init();

		// Check if fileDbId was provided
		if(!isset($this->listType) || !isset($this->returnUrl) || !isset($this->sourceTool))
		{
			throw new \yii\web\HttpException(500, 'Missing required field. Required fields are: $listId and $returnUrl and $sourceTool');
		}
	}

	/**
	 * Run the widget by rendering the button and other elements
	 */
	public function run() {

		return $this->render('working-list', [
			'returnUrl' => $this->returnUrl,
			'listType' => $this->listType,
			'sourceTool' => $this->sourceTool,
			'itemCount' => $this->itemCount
		]);
	}
}