<?php
/* 
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Contains methods for unit testing
 */
class UnitTest extends Component{
	
	/**
	* Authenticate using access token
	* @return $userId integer user identifier
	*/
	public function authenticate(){

		// retrieve access token
		$accessToken = getenv('CB_UNIT_TESTING_ACCESS_TOKEN');
		Yii::$app->session->set('user.b4r_api_v3_token',$accessToken);

		// decode access token to retrieve user id
		$tokenObj = (json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $accessToken)[1])))));

		$userId = isset($tokenObj->userId) ? $tokenObj->userId : 1;
		Yii::$app->session->set('user.id',$userId);

		return $userId;
	}
}