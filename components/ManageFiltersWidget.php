<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use yii\base\Widget;
use Yii;

/**
 * Advanced filter widget for data browsers. Renders the UI for displaying saved filters and advanced filter options
 */
class ManageFiltersWidget extends Widget {
	public $columnNames; // array containing the column names you want to enable advanced filtering.
	public $columnNameAndDataTypes; // array containing the columns names with its specified data types
	public $searchModel; // search model used in the data browser
	public $searchModelObject; // initialized search model object
	public $gridId; // ID of the dynagrid
	public $dataBrowserId; // ID of the data browser in the database
	public $resetGridUrl; // base URL of the data browser
	public $btnClass='filter-action-button'; // button class to be used to initiate the click action

	/**
	 * Initialize the widget
	 * Check if required attributes are specified
	 * Get searchModelObject and columnNameAndDataTypes values
	 */
	public function init() {
		parent::init();

		if (empty($this->searchModel) || empty($this->gridId) || empty($this->resetGridUrl)) {
			throw new \yii\web\HttpException(500, 'Missing required field. Required fields are: $searchModel, $gridId, and $resetGridUrl');
		}
		if(empty($this->dataBrowserId)) {
			$this->dataBrowserId = $this->gridId;
		}
		$this->searchModelObject = $this->loadSearchModel();

		if (method_exists($this->searchModelObject, 'getColumnNameAndDatatypes')) {
			$this->columnNameAndDataTypes = $this->searchModelObject->getColumnNameAndDatatypes();
			$this->columnNames = array_keys($this->columnNameAndDataTypes);
		} else {
			throw new \yii\web\HttpException(500, 'Missing required method getColumnNameAndDatatypes() in model');
		}
	}

	/**
	 * Run the widget by rendering the button and other elements
	 */
	public function run() {

		return $this->render('manage-filters', [
			'columnNames' => json_encode($this->columnNames),
			'columnNameAndDataTypes' => json_encode($this->columnNameAndDataTypes),
			'searchModel' => $this->searchModel,
			'dataBrowserId' => $this->dataBrowserId,
			'gridId' => $this->gridId,
			'btnClass' => $this->btnClass,
			'resetGridUrl' => $this->resetGridUrl
		]);
	}

	/**
	 * Load and initialize the search model object
	 * Checks if the provider searchModel value is a valid class
	 * @return the initiated model object
	 */
	private function loadSearchModel() {

		return \yii\di\Instance::ensure(
			'app\models\\'.$this->searchModel,
			\yii\base\Model::class
		);
	}
}