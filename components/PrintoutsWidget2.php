<?php

/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;

use yii\base\Widget;
use Yii;

/**
 * Widget for Printouts (version 2).
 * Renders the UI for displaying available printouts templates.
 */
class PrintoutsWidget2 extends Widget
{
	public $entityName; // string - name of the entity used
	public $entityDisplayName; // string - name of the entity used
	public $entityDbIds; // integer array - database ids of the entity records
	public $entityType; // string - type of entity to be processed; i.e. germplasm
	public $entityValueSource; // string - url of alternate source of selected records
	public $checkboxSessionStorage; // string name of the session storage variable containing selection of entity records
	public $product; // string - name of the product for which reports will be retrieved
	public $program; // string - code of the program for which reports will be retrieved

	public $buttonStyle; // array

	/**
	 * Initialize the widget
	 * Check if required attributes are specified
	 */
	public function init()
	{
		parent::init();

		// Check if occurrenceIds, product, and program were provided
		if (
			!isset($this->entityName)
			|| !isset($this->entityDisplayName)
			|| !isset($this->entityDbIds)
			|| !isset($this->product)
			|| !isset($this->program)
		) {
			throw new \yii\web\HttpException(500, 'Missing required field. Required fields are: $entityName, $entityDisplayName, $entityDbIds, $product, and $program');
		}

		// If entityDbIds array is empty, require checkboxSessionStorage
		if (empty($this->entityDbIds) && empty($this->checkboxSessionStorage) && empty($this->entityType)) {
			throw new \yii\web\HttpException(500, 'Missing value for $checkboxSessionStorage. Value is required when $entityDbIds array is empty.');
		}
	}

	/**
	 * Run the widget by rendering the button and other elements
	 */
	public function run()
	{

		return $this->render('printouts2', [
			'product' => $this->product,
			'program' => $this->program,
			'entityName' => $this->entityName,
			'entityDisplayName' => $this->entityDisplayName,
			'entityDbIds' => $this->entityDbIds,
			'entityType' => $this->entityType,
			'checkboxSessionStorage' => $this->checkboxSessionStorage,
			'buttonStyle' => $this->buttonStyle,
			'entityValueSource' => $this->entityValueSource
		]);
	}
}
