<?php
/*
* This file is part of EBS-Core Breeding.
* EBS-Core Breeding is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* EBS-Core Breeding is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>
*/
namespace app\components;

// Import Yii helpers
use yii\base\Widget;
use yii\helpers\Url;

// Import base GridView and DynaGrid models
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

/**
 * Advance Data Browser widget
 */
class AdvancedDataBrowserWidget extends Widget {

    /** START: Unique Public properties from Kartik widgets and their parents found in /vendor
     */

        // class GridView extends YiiGridView
        public $moduleId = null;
        public $krajeeDialogSettings = null;
        public $layout = null;
        public $itemLabelSingle = null;
        public $itemLabelPlural = null;
        public $itemLabelFew = null;
        public $itemLabelMany = null;
        public $itemLabelAccusative = null;
        public $panelTemplate = null;
        public $panelHeadingTemplate = null;
        public $panelFooterTemplate = null;
        public $panelBeforeTemplate = null;
        public $panelAfterTemplate = null;
        public $panelPrefix = null;
        public $panel = null;
        public $beforeHeader = null;
        public $afterHeader = null;
        public $beforeFooter = null;
        public $afterFooter = null;
        public $toolbar = null;
        public $toolbarContainerOptions = null;
        public $replaceTags = null;
        public $dataColumnClass = null;
        public $footerRowOptions = null;
        public $captionOptions = null;
        public $tableOptions = null;
        public $pjax = null;
        public $pjaxSettings = null;
        public $persistResize = null;
        public $hideResizeMobile = null;
        public $resizableColumns = null;
        public $resizeStorageKey = null;
        public $resizableColumnsOptions = null;
        public $bordered = null;
        public $bootstrap = null;
        public $fuKartik = null;
        public $ih8php = null;
        public $condensed = null;
        public $striped = null;
        public $responsiveWrap = null;
        public $responsive = null;
        public $floatHeader = null;
        public $hover = null;
        public $floatOverflowContainer = null;
        public $floatHeaderOptions = null;
        public $perfectScrollbar = null;
        public $perfectScrollbarOptions = null;
        public $showPageSummary = null;
        public $pageSummaryPosition = null;
        public $pageSummaryContainer = null;
        public $pageSummaryRowOptions = null;
        public $defaultPagination = null;
        public $toggleData = null;
        public $toggleDataOptions = null;
        public $toggleDataContainer = null;
        public $exportContainer = null;
        public $export = null;
        public $exportConfig = null;
        public $exportConversions = null;
        public $autoXlFormat = null;
        public $containerOptions = null;
        public $hashExportConfig = null;

        // class (Yii)GridView extends BaseListView
        public $caption = null;
        public $options = null;
        public $headerRowOptions = null;
        public $rowOptions = null;
        public $beforeRow = null;
        public $afterRow = null;
        public $showHeader = null;
        public $showFooter = null;
        public $placeFooterAfterBody = null;
        public $showOnEmpty = null;
        public $formatter = null;
        public $columns = null;
        public $emptyCell = null;
        public $filterModel = null;
        public $filterUrl = null;
        public $filterSelector = null;
        public $filterPosition = null;
        public $filterRowOptions = null;
        public $filterErrorSummaryOptions = null;
        public $filterErrorOptions = null;
        public $filterOnFocusOut = null;

        // class BaseListView extends Widget
        public $dataProvider = null;
        public $pager = null;
        public $sorter = null;
        public $summary = null;
        public $summaryOptions = null;
        public $emptyText = null;
        public $emptyTextOptions = null;

        // class DynaGrid extends (kartik)Widget
        public $storage = null;
        public $theme = null;
        public $userSpecific = null;
        public $dbUpdateNameOnly = null;
        public $showPersonalize = null;
        public $showFilter = null;
        public $showSort = null;
        public $enableMultiSort = null;
        public $allowPageSetting = null;
        public $allowThemeSetting = null;
        public $allowFilterSetting = null;
        public $allowSortSetting = null;
        public $gridOptions = null;
        public $matchPanelStyle = null;
        public $toggleButtonGrid = null;
        public $toggleButtonFilter = null;
        public $toggleButtonSort = null;
        public $sortableOptions = null;
        public $sortableHeader = null;
        public $submitMessage = null;
        public $deleteMessage = null;
        public $messageOptions = null;
        public $deleteConfirmation = null;
        public $submitButtonOptions = null;
        public $resetButtonOptions = null;
        public $deleteButtonOptions = null;
        public $iconPersonalize = null;
        public $iconFilter = null;
        public $iconSort = null;
        public $iconConfirm = null;
        public $iconRemove = null;
        public $iconVisibleColumn = null;
        public $iconHiddenColumn = null;
        public $iconSortableSeparator = null;

     /*
    * END: Public properties from Kartik widgets and their parents found in /vendor */

    // GridView other properties
    public $behaviors = null;
    public $id = null;
    public $view = null;
    public $viewPath = null;

    // Custom Public Properties of AdvancedDataBrowser
    public $mode = null;
    public $browserId = null;
    public $endpoint = null;
    public $browserDescription = null;
    /**
     * Base url of the current page of the browser. This is used for the saving of browser
     * states which rely on url parameters
     * Value should come from `Url::current();` but without the extra parameters
     * Sample: '/index.php/advancedDataBrowser/default/index?program=IRSEA'
     */
    public $baseUrl = null;
    /**
     * Default filter that will be sent to the API and WILL NOT SHOW UP on the browser column
     * you can think of this as the filter that is generated from the query tool
     * Sample: ['columnName1'=>'filter', 'columnName2'=>['operator' => 'filter']]
     */
    public $defaultFilter = null;
    /**
     * Default sort for Data Provider
     * Sample: ['columnName1'=>SORT_ASC, 'columnName2'=>SORT_DESC]
     */
    public $defaultSort = null;

    /**
     * Initialize the widget
     */
    public function init() {
        parent::init();
    }

    /**
     * Render a Data Browser
     */
    public function run() {
        /*  TODO: Refactor so that the Advanced Data Browser Widget will have a built-in management for data browser states
            if(!isset($this->baseUrl)) {
                throw new \yii\base\ErrorException(\Yii::t('app', '$baseUrl is a required parameter for saving and loading browser states'));
            }

            // Get current url value
            $currentUrl = Url::current();
            if(is_null($this->baseUrl)) $this->baseUrl = explode('?', $currentUrl)[0] . '?program=' . \Yii::$app->userprogram->get('abbrev');

            // Get Data Browser state
            $browserState = \Yii::$container->get('app\models\DataBrowserConfiguration')->getDataBrowserState($this->browserId);
            if(is_null($browserState)) {
                $browserState = [
                    'base' => $this->baseUrl,
                    'default' => '',
                    'recent' => $this->baseUrl,
                    'savedStates' => []
                ];
            }

            // Check if user is newly logged in, apply the last filters and sort applied (thru url) from db
            $savedFilterAndSortAlreadyLoaded = \Yii::$app->cbSession->get($this->browserId . 'savedFilterAndSortAlreadyLoaded');
            if(!$savedFilterAndSortAlreadyLoaded || is_null($savedFilterAndSortAlreadyLoaded)) {
                \Yii::$app->cbSession->set($this->browserId . 'savedFilterAndSortAlreadyLoaded', true);

                // Also only apply if the current url, base url, and recent url don't have the same value; else there is no need to apply this
                if(!($currentUrl == $browserState['base'] && $currentUrl == $browserState['recent'])) {
                    // Get recent url value
                    $redirectUrl = $browserState['recent'];

                    // redirect
                    return $this->render('advancedDataBrowser/redirect',[   // this view file just redirects the user. Nothing to see here. Move along...
                        'redirectUrl' => $redirectUrl,
                        'message' => 'Applying saved data browser state.'
                    ]);
                }
            }

            // Check if user navigates to Data Browser Page from another page or tool
            if($currentUrl == $browserState['base']) {
                // If there was a previously applied filter and sort parameters, apply them
                if($currentUrl != $browserState['recent']) {
                    // Get recent url value
                    $redirectUrl = $browserState['recent'];

                    // redirect
                    return $this->render('advancedDataBrowser/redirect',[   // this view file just redirects the user. Nothing to see here. Move along...
                        'redirectUrl' => $redirectUrl,
                        'message' => 'Applying the last data browser state.'
                    ]);
                }
            }

            // If user, wants to reset the filter and sort parameters, the use the base url from db to reset them
            if(isset($_GET['reset']) && $_GET['reset'] == 'true') {
                // Save base url as recent url
                $browserState['recent'] = $browserState['base'];
                \Yii::$container->get('app\models\DataBrowserConfiguration')->saveDataBrowserState($this->browserId, $browserState);

                // Get base url value
                $redirectUrl = $browserState['base'];

                // redirect
                return $this->render('advancedDataBrowser/redirect',[   // this view file just redirects the user. Nothing to see here. Move along...
                    'redirectUrl' => $redirectUrl,
                    'message' => 'Resetting data browser state.'
                ]);
            }

            // Save current url as recent url
            $browserState['recent'] = $currentUrl;
            \Yii::$container->get('app\models\DataBrowserConfiguration')->saveDataBrowserState($this->browserId, $browserState);
        */

        if($this->mode == 'GridView') {
            $props = [
                'moduleId' => $this->moduleId,
                'krajeeDialogSettings' => $this->krajeeDialogSettings,
                'layout' => $this->layout,
                'itemLabelSingle' => $this->itemLabelSingle,
                'itemLabelPlural' => $this->itemLabelPlural,
                'itemLabelFew' => $this->itemLabelFew,
                'itemLabelMany' => $this->itemLabelMany,
                'itemLabelAccusative' => $this->itemLabelAccusative,
                'panelTemplate' => $this->panelTemplate,
                'panelHeadingTemplate' => $this->panelHeadingTemplate,
                'panelFooterTemplate' => $this->panelFooterTemplate,
                'panelBeforeTemplate' => $this->panelBeforeTemplate,
                'panelAfterTemplate' => $this->panelAfterTemplate,
                'panelPrefix' => $this->panelPrefix,
                'panel' => $this->panel,
                'beforeHeader' => $this->beforeHeader,
                'afterHeader' => $this->afterHeader,
                'beforeFooter' => $this->beforeFooter,
                'afterFooter' => $this->afterFooter,
                'toolbar' => $this->toolbar,
                'toolbarContainerOptions' => $this->toolbarContainerOptions,
                'replaceTags' => $this->replaceTags,
                'dataColumnClass' => $this->dataColumnClass,
                'footerRowOptions' => $this->footerRowOptions,
                'captionOptions' => $this->captionOptions,
                'tableOptions' => $this->tableOptions,
                'pjax' => $this->pjax,
                'pjaxSettings' => $this->pjaxSettings,
                'resizableColumns' => $this->resizableColumns,
                'hideResizeMobile' => $this->hideResizeMobile,
                'resizableColumnsOptions' => $this->resizableColumnsOptions,
                'persistResize' => $this->persistResize,
                'resizeStorageKey' => $this->resizeStorageKey,
                'bootstrap' => $this->bootstrap,
                'bordered' => $this->bordered,
                'striped' => $this->striped,
                'condensed' => $this->condensed,
                'responsive' => $this->responsive,
                'responsiveWrap' => $this->responsiveWrap,
                'hover' => $this->hover,
                'floatHeader' => $this->floatHeader,
                'floatOverflowContainer' => $this->floatOverflowContainer,
                'floatHeaderOptions' => $this->floatHeaderOptions,
                'perfectScrollbar' => $this->perfectScrollbar,
                'perfectScrollbarOptions' => $this->perfectScrollbarOptions,
                'showPageSummary' => $this->showPageSummary,
                'pageSummaryPosition' => $this->pageSummaryPosition,
                'pageSummaryContainer' => $this->pageSummaryContainer,
                'pageSummaryRowOptions' => $this->pageSummaryRowOptions,
                'defaultPagination' => $this->defaultPagination,
                'toggleData' => $this->toggleData,
                'toggleDataOptions' => $this->toggleDataOptions,
                'toggleDataContainer' => $this->toggleDataContainer,
                'exportContainer' => $this->exportContainer,
                'export' => $this->export,
                'exportConfig' => $this->exportConfig,
                'exportConversions' => $this->exportConversions,
                'autoXlFormat' => $this->autoXlFormat,
                'containerOptions' => $this->containerOptions,
                'hashExportConfig' => $this->hashExportConfig,
                'dataColumnClass' => $this->dataColumnClass,
                'caption' => $this->caption,
                'captionOptions' => $this->captionOptions,
                'tableOptions' => $this->tableOptions,
                'options' => $this->options,
                'headerRowOptions' => $this->headerRowOptions,
                'footerRowOptions' => $this->footerRowOptions,
                'rowOptions' => $this->rowOptions,
                'beforeRow' => $this->beforeRow,
                'afterRow' => $this->afterRow,
                'showHeader' => $this->showHeader,
                'showFooter' => $this->showFooter,
                'placeFooterAfterBody' => $this->placeFooterAfterBody,
                'showOnEmpty' => $this->showOnEmpty,
                'formatter' => $this->formatter,
                'columns' => $this->columns,
                'emptyCell' => $this->emptyCell,
                'filterModel' => $this->filterModel,
                'filterUrl' => $this->filterUrl,
                'filterSelector' => $this->filterSelector,
                'filterPosition' => $this->filterPosition,
                'filterRowOptions' => $this->filterRowOptions,
                'filterErrorSummaryOptions' => $this->filterErrorSummaryOptions,
                'filterErrorOptions' => $this->filterErrorOptions,
                'filterOnFocusOut' => $this->filterOnFocusOut,
                'layout' => $this->layout,
                'options' => $this->options,
                'dataProvider' => $this->dataProvider,
                'pager' => $this->pager,
                'sorter' => $this->sorter,
                'summary' => $this->summary,
                'summaryOptions' => $this->summaryOptions,
                'showOnEmpty' => $this->showOnEmpty,
                'emptyText' => $this->emptyText,
                'emptyTextOptions' => $this->emptyTextOptions,
                'layout' => $this->layout,
                'behaviors' => $this->behaviors,
                'id' => $this->id,
                'view' => $this->view,
                'viewPath' => $this->viewPath,
            ];

            $props = array_filter($props, fn($value) => !is_null($value));

            return GridView::widget($props);
        } else if ($this->mode == 'DynaGrid') {
            $props = [
                'moduleId' => $this->moduleId,
                'storage' => $this->storage,
                'theme' => $this->theme,
                'userSpecific' => $this->userSpecific,
                'dbUpdateNameOnly' => $this->dbUpdateNameOnly,
                'showPersonalize' => $this->showPersonalize,
                'showFilter' => $this->showFilter,
                'showSort' => $this->showSort,
                'enableMultiSort' => $this->enableMultiSort,
                'allowPageSetting' => $this->allowPageSetting,
                'allowThemeSetting' => $this->allowThemeSetting,
                'allowFilterSetting' => $this->allowFilterSetting,
                'allowSortSetting' => $this->allowSortSetting,
                'gridOptions' => $this->gridOptions,
                'matchPanelStyle' => $this->matchPanelStyle,
                'toggleButtonGrid' => $this->toggleButtonGrid,
                'toggleButtonFilter' => $this->toggleButtonFilter,
                'toggleButtonSort' => $this->toggleButtonSort,
                'options' => $this->options,
                'sortableOptions' => $this->sortableOptions,
                'sortableHeader' => $this->sortableHeader,
                'columns' => $this->columns,
                'submitMessage' => $this->submitMessage,
                'deleteMessage' => $this->deleteMessage,
                'messageOptions' => $this->messageOptions,
                'deleteConfirmation' => $this->deleteConfirmation,
                'krajeeDialogSettings' => $this->krajeeDialogSettings,
                'submitButtonOptions' => $this->submitButtonOptions,
                'resetButtonOptions' => $this->resetButtonOptions,
                'deleteButtonOptions' => $this->deleteButtonOptions,
                'iconPersonalize' => $this->iconPersonalize,
                'iconFilter' => $this->iconFilter,
                'iconSort' => $this->iconSort,
                'iconConfirm' => $this->iconConfirm,
                'iconRemove' => $this->iconRemove,
                'iconVisibleColumn' => $this->iconVisibleColumn,
                'iconHiddenColumn' => $this->iconHiddenColumn,
                'iconSortableSeparator' => $this->iconSortableSeparator,
            ];

            $props = array_filter($props, fn($value) => !is_null($value));

            return DynaGrid::widget($props);
        }

        if(!isset($this->browserId)) {
            throw new \yii\base\ErrorException(\Yii::t('app', '$browserId is a required parameter'));
        }

        if(!isset($this->columns)) {
            throw new \yii\base\ErrorException(\Yii::t('app', '$columns is a required parameter'));
        }

        // Build rules for column filters based on $columns
        $filterModel = \Yii::$container->get('app\models\AdvancedDataBrowser');
        $filterColumns = [];
        foreach($this->columns as $column) {
            if(isset($column['attribute'])) $filterColumns[] = $column['attribute'];
        }
        $filterModel->rulesArray = $filterColumns;

        $filterModel->defaultFilter = $this->defaultFilter;
        $filterModel->defaultSort = $this->defaultSort;

        // Get Data
        $params = \Yii::$app->request->queryParams;
        $dataProvider = $filterModel->search($params, $this->endpoint, $this->browserId);

        return $this->render('advancedDataBrowser/index',[
            'browserId' => $this->browserId,
            'columns' => $this->columns,
            'browserDescription' => $this->browserDescription,
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
            // Other attributes supplied by the Dev to be passed to the Dynagrid
            'showPersonalize' => $this->showPersonalize,
            'gridOptions' => $this->gridOptions,
        ]);
    }
}
