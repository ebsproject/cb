<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 namespace app\components;
 use Yii;
 use Closure;
 use yii\helpers\Html;
 use yii\base\Widget;
 use kartik\export\ExportMenu;

/**
 * Custom widget for export menu
 */

 class B4RExportMenu extends ExportMenu {

    public $retrieveAll=false;
    public $apiEndpoint;
    public $apiFilters;
    public $apiRequestBody;

    public function generateBody() {

        $this->_endRow = 0;
        $columns = $this->getVisibleColumns();

        if ($this->retrieveAll && !empty($this->apiEndpoint)) {
            $response = Yii::$app->api->getParsedResponse('POST', $this->apiEndpoint, json_encode($this->apiRequestBody), $this->apiFilters, true);
            $models = array_values($response['data']);
            $this->_provider->setModels($models);
        } else {
            $models = array_values($this->_provider->getModels());
        }

        if (count($columns) == 0) {
            $cell = $this->setOutCellValue($this->_objWorksheet, 'A1', $this->emptyText);
            $model = reset($models);
            $this->raiseEvent('onRenderDataCell', [$cell, $this->emptyText, $model, null, 0, $this]);
            return 0;
        }
        // do not execute multiple COUNT(*) queries
        $totalCount = $this->_provider->getTotalCount();
        $this->findGroupedColumn();
        while (!empty($models)) {
            $keys = $this->_provider->getKeys();
            foreach ($models as $index => $model) {
                $key = $keys[$index];
                $this->generateRow($model, $key, $this->_endRow);
                $this->_endRow++;
                if ($index === $totalCount) {
                    //a little hack to generate last grouped footer
                    $this->checkGroupedRow($model, $models[0], $key, $this->_endRow);
                } elseif (isset($models[$index + 1])) {
                    $this->checkGroupedRow($model, $models[$index + 1], $key, $this->_endRow);
                }
                if (!is_null($this->_groupedRow)) {
                    $this->_endRow++;
                    $this->_objWorksheet->fromArray($this->_groupedRow, null, 'A' . ($this->_endRow + 1), true);
                    $cell = 'A' . ($this->_endRow + 1) . ':' . self::columnName(count($columns)) . ($this->_endRow + 1);
                    $this->_objWorksheet->getStyle($cell)->applyFromArray($this->groupedRowStyle);
                    $this->_groupedRow = null;
                }
            }
            if ($this->_provider->pagination) {
                $this->_provider->pagination->page++;
                $this->_provider->refresh();
                $this->_provider->setTotalCount($totalCount);
                $models = $this->_provider->getModels();
            } else {
                $models = [];
            }
        }
        $this->generateBox();
        return $this->_endRow;
    }
 }