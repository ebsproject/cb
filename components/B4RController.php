<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EBS-Core Breeding.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Parent controller of EBS-Core Breeding
 */
namespace app\components;

use yii\web\Controller;
use Yii;
use yii\helpers\Url;
use app\controllers\SiteController;
use app\models\User;
use app\models\Program;

class B4RController extends Controller{
	
	/**
	 * Parent action to be performed in the system
	 */
	public function beforeAction($action)
	{
		$session = Yii::$app->session;
		$action = Yii::$app->controller->action->id;
		$controller = Yii::$app->controller->id;
		$hasAccess = 'true';
		$programWithAccess = null;

		$isUnderMaintenance = getenv('CB_UNDER_MAINTENANCE');

		//if site is under maintenance, log out the user
		if($isUnderMaintenance == 'true' || $isUnderMaintenance == 'TRUE'){
			$session->set('user.is_logged_in', NULL);
		}

		$program = isset($_GET['program']) ? $_GET['program'] : null;

		// Check if the token is not yet expired
		$isTokenExpired = \Yii::$container->get('app\controllers\CheckAccessToken')->authenticate();

		//if no access, redirect to home page
		if(($session->get('user.is_logged_in') == NULL || !$session->get('user.is_logged_in')) && $action != 'login' && $action != 'verify' && $controller != 'bug-report'){
			Yii::$app->session->setFlash('error',\Yii::t('app','Sorry, you are not allowed to access this page.'));
			Yii::$app->response->redirect(Url::base().'/index.php');
		}
		else if ($isTokenExpired) {
			
			Yii::$app->user->logout();
        
			$session = Yii::$app->session;
			
			$session->destroy(); // Destroy all application session data
			
			Yii::$app->session->setFlash('error', \Yii::t('app', 'Your session has expired. Please log in.'));
            
			// Go back to the homepage
			return $this->goHome();
		}
		else{

			$userModel = new User();
			$isAdmin = $userModel->isAdmin(); // check if admin user or not

			if(is_numeric($program)){ //check if value uses program ID
				$programRecord =  \Yii::$container->get('app\models\Program')->getProgram($program);
				$program = $programRecord['programCode'] ?? $program;

			}
			if(!empty($program) && !$isAdmin){

				$hasAccess = $userModel->checkIfUserHasAccessToProgram($program);

				$programWithAccess = (!empty($hasAccess['program'])) ? $hasAccess['program'] : null;
			}

			// if user does not have access to program
			if($hasAccess == 'true' || empty($program)){
				Yii::$app->language  = 'en'; //set language
			}else{
				$message = \Yii::t('app',"You do not have access to <b>$program</b> program.");
				Yii::$app->getSession()->setFlash('error',\Yii::t('app', $message));

				if(!empty($programWithAccess)){
					Yii::$app->getResponse()->redirect(Url::base().'/index.php?program='.$programWithAccess);
				}else{
					Yii::$app->getResponse()->redirect(Url::base().'/index.php');
				}

				Yii::$app->getResponse()->send();
				exit;
			}

			return parent::beforeAction($action);
		}
	}
}