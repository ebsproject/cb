<?php
/*
 * This file is part of EBS-Core Breeding.
 *
 * EBS-Core Breeding is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EBS-Core Breeding is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\components;
use Yii;
use Closure;
use yii\helpers\Html;
use kartik\grid\DataColumn as DataColumn;

/**
 * Customized class for rendering checkboxes in a dynagrid browser
 */

class B4RCheckboxColumn extends DataColumn {

    public $name;
    public $selectAllId;
    public $checkboxOptions = [];
    public $displaySummaryId;
    public $pjaxGridId;
    public $type;
    private $defaultValues = [];
    private $sessionStorageName;

    public function init() {

        parent::init();

        if (empty($this->name)){
            $this->name = $this->grid->id . '_select';
        }

        if (substr_compare($this->name, '[]', -2, 2)) {
            $this->name .= '[]';
        }

        if (empty($this->selectAllId)) {
            $this->selectAllId = $this->grid->id . '_select_all';
        }

        if (empty($this->type)) {
            $this->type = 'checkbox';
        }

        if (empty($this->pjaxGridId)) {
            if (isset($this->grid->pjaxSettings['options']['id'])) {
                $this->pjaxGridId = $this->grid->pjaxSettings['options']['id'];
            } else {
                $this->pjaxGridId = $this->grid->id . '-pjax';
            }
        }

        $this->sessionStorageName = $this->grid->id . '_checkbox_storage';
        

        $this->registerClientScript();
    }


    /**
     * Renders the header cell content.
     * The default implementation simply renders [[header]].
     * This method may be overridden to customize the rendering of the header cell.
     * @return string the rendering result
     */
    protected function renderHeaderCellContent()
    {
        return Html::checkbox($this->getHeaderCheckBoxName(), false, [
            'id' => $this->selectAllId,
            'class' => 'filled-in',
        ]) . '<label for="'.$this->selectAllId.'"></label>';
    }

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $hide = false;
        if ($this->checkboxOptions instanceof Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
        }
        
        if (!isset($options['value'])) {
            $options['value'] = is_array($key) ? Json::encode($key) : $key;
        }

        if (!isset($options['class'])) {
            $options['class'] = $this->grid->id . '_select';
        }

        if (!isset($options['id'])) {
            $options['id'] = $this->grid->id . '_select_' . $index;
        }

        if (!isset($options['checked'])) {
            $options['checked'] = false;
        }

        if ($options['checked']) {
            $this->defaultValues[] = $options['value'];
        }

        if (isset($options['name'])) {
            $this->name = $options['name'];
        }

        if (isset($options['hide'])) {
            $hide = $options['hide'];
            unset($options['hide']);
        }

        if ($this->type == 'radio') {
            $options['class'] .= ' with-gap b4rradiobtn';

            if ($options['checked']) {
                $options['class'] .= ' isClicked';
            }
            return Html::radio($this->name, $options['checked'], $options) 
                . '<label for="'.$options['id'].'"></label>';
        } else {
            $options['class'] .= ' filled-in b4rcheckbox';

            if (!$hide) {
                return Html::checkbox($this->name, $options['checked'], $options) 
                    . '<label for="'.$options['id'].'"></label>';
            }
        }
    }

    /**
     * Returns header checkbox name.
     * @return string header checkbox name
     * @since 2.0.8
     */
    protected function getHeaderCheckBoxName()
    {
        $name = $this->name;
        if (substr_compare($name, '[]', -2, 2) === 0) {
            $name = substr($name, 0, -2);
        }
        if (substr_compare($name, ']', -1, 1) === 0) {
            $name = substr($name, 0, -1) . '_all]';
        } else {
            $name .= '_all';
        }

        return $name;
    }

    /**
     * Registers the needed JavaScript.
     * @since 2.0.8
     */
    public function registerClientScript()
    {
        $selectAllId = $this->selectAllId;
        $selectClass = 'b4rcheckbox';
        $gridId = $this->grid->id;
        $sessionStorageName = $this->sessionStorageName;
        $displaySummaryId = (empty($this->displaySummaryId)) ? '' : $this->displaySummaryId;
        $defaultValues = json_encode($this->defaultValues);
        $pjaxGridId = $this->pjaxGridId;
        $type = $this->type;

        Yii::$app->controller->view->registerJs(<<<JS
            var selectAllId = '$selectAllId';
            var selectClass = '$selectClass';
            var gridId = '$gridId';
            var sessionStorageName = '$sessionStorageName';
            var displaySummaryId = '$displaySummaryId';
            var defaultValues = '$defaultValues';
            var pjaxGridId = '$pjaxGridId';
            var type = '$type';
        JS, 
        yii\web\View::POS_BEGIN);

        Yii::$app->controller->view->registerJsFile(
            '@web/components/js/b4rcheckbox-column.js',[
                'depends' => ['app\assets\AppAsset'],
                'position'=>\yii\web\View::POS_END
            ]
        );
    }
}



